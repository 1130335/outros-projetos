/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author Sergio
 */
public class Ligacao {

	private InetAddress IPdestino;
	private int porto;
	private String Nome;
	private boolean estadoEnvia;
	private boolean estadoRecebe;
	private Socket sock;

	public Ligacao() {
		estadoEnvia = true;
		estadoRecebe = true;
	}

	/**
	 * @return the IPdestino
	 */
	public InetAddress getIPdestino() {
		return IPdestino;
	}

	/**
	 * @param IPdestino the IPdestino to set
	 */
	public void setIPdestino(InetAddress IPdestino) {
		this.IPdestino = IPdestino;
	}

	/**
	 * @return the porto
	 */
	public int getPorto() {
		return porto;
	}

	/**
	 * @param porto the porto to set
	 */
	public void setPorto(int porto) {
		this.porto = porto;
	}

	/**
	 * @return the Nome
	 */
	public String getNome() {
		return Nome;
	}

	/**
	 * @param Nome the Nome to set
	 */
	public void setNome(String Nome) {
		this.Nome = Nome;
	}

	/**
	 * @return the estadoEnvia
	 */
	public boolean getEstadoEnvia() {
		return estadoEnvia;
	}

	/**
	 * @param estadoEnvia the estadoEnvia to set
	 */
	public void setEstadoEnvia() {
		if (estadoEnvia == true) {
			this.estadoEnvia = false;
		} else {
			this.estadoEnvia = true;
		}

	}

	/**
	 * @return the estadoRecebe
	 */
	public boolean getEstadoRecebe() {
		return estadoRecebe;
	}

	/**
	 * @param estadoRecebe the estadoRecebe to set
	 */
	public void setEstadoRecebe() {
		if (estadoRecebe == true) {
			this.estadoRecebe = false;
		} else {
			this.estadoRecebe = true;
		}
	}

	/**
	 * @return the sock
	 */
	public Socket getSock() {
		return sock;
	}

	/**
	 * @param sock the sock to set
	 */
	public void setSock(Socket sock) {
		this.sock = sock;
	}
}
