package rcomp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Classe responsável pela retransmissão da mensagem de um cliente para todos os
 * outros a si ligados. Baseada na classe fornecida na PL11-P.
 *
 * @author 1130335
 */
public class Servidor {

	/* Número máximo de clientes */
	public static int MAX_CLI = 100;
	/* Nome do servidor */
	public static String myName;
	/* Vetor com (sockets de) clientes ligados e espaço para novos clientes */
	public static Socket[] cliSock = new Socket[MAX_CLI];
	/* Lista com nicknames dos clientes */
	public static List<String> cliNicks = new ArrayList<>();
	/* Objecto que permite escrever as mensagens */
	public static DataOutputStream[] sOut = new DataOutputStream[MAX_CLI];
	/* Indica quais os sockets que contêm clientes ligados e espaços livres para novos clientes */
	public static Boolean[] cliLigados = new Boolean[MAX_CLI];
	/* Semáforo para exclusão múltipla para acesso ao vetor acima */
	public static Semaphore exclMult = new Semaphore(1);
	/* Socket do servidor */
	private static ServerSocket sock;

	public static void main(String args[]) throws Exception {
		/* Regista o nome do servidor */
		myName = args[0];

		try {
			sock = new ServerSocket(40000);
		} catch (IOException io) {
			System.out.println("O porto local está ocupado.\n");
			System.exit(1);
		}

		for (int i = 0; i < MAX_CLI; i++) {
			cliLigados[i] = false;
		}

		while (true) {
			respondeDisponivel();
			try {
				exclMult.acquire();
				for (int i = 0; i < MAX_CLI; i++) {

					/* Vai procurar um socket onde não haja nenhum cliente ligago */
					if (!cliLigados[i]) {
						break;
					}

					exclMult.release();
					cliSock[i] = sock.accept();
					exclMult.acquire();
					cliLigados[i] = true;
					sOut[i] = new DataOutputStream(cliSock[i].
						getOutputStream());
					exclMult.release();
					new Thread(new Chat_Server(i)).start();
				}

			} catch (InterruptedException ex) {
				System.out.
					println("Não foi possível adquirir o lock no semáforo.\n");
			}
		}
	}

	static void respondeDisponivel() {

		try {

			//Open a random port to send the package
			DatagramSocket c;
			c = new DatagramSocket(40001);
			//Wait for a response
			byte[] recvBuf = new byte[15000];

			DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);

			c.receive(receivePacket);

			//Check if the message is correct
			String message = new String(receivePacket.getData()).trim();

			if (message.equals("DISPONIVEL?")) {

				byte[] sendData = "DISPONIVEL".getBytes();

				try {
					receivePacket.setData(sendData);
					receivePacket.setLength(sendData.length);
					c.send(receivePacket);
				} catch (Exception e) {
				}

			}

			//Close the port!
			c.close();

		} catch (IOException ex) {
		}
	}

	static class Chat_Server implements Runnable {

		/* Número do cliente */
		int myNum;
		/* Objecto que permite ler as mensagens */
		private DataInputStream sIn;

		public Chat_Server(int numCli) {
			myNum = numCli;
		}

		@Override
		public void run() {
			int numChars;
			byte[] data = new byte[300];
			try {
				sIn = new DataInputStream(Servidor.cliSock[myNum].
					getInputStream());
				/* Começa pelo registo do nickname de um novo cliente */
				numChars = sIn.read();
				sIn.read(data, 0, numChars);
				String registo = new String(data, "UTF-8");
				//verifica se a mensagem se trata de um registo ou nao
				if (verificaNovoCliente(registo)) {
					//se se tratar de um registo a mensagem não é enviada para os restantes clientes
				} else {
					//caso a mensagem nao se trate de um registo, a mensagem é enviada para os outros clientes
					while (true) {
						numChars = sIn.read();
						/* O cliente sai quando enviar uma linha vazia */
						if (numChars == 0) {
							break;
						}
						sIn.read(data, 0, numChars);

						Servidor.exclMult.acquire();
						for (int i = 0; i < Servidor.MAX_CLI; i++) {
							if (Servidor.cliLigados[i]) {
								Servidor.sOut[i].write(numChars);
								Servidor.sOut[i].write(data, 0, numChars);
							}
						}
						Servidor.exclMult.release();
					}
				}
				/* Quando o cliente quiser sair do chat */
				Servidor.exclMult.acquire();
				Servidor.sOut[myNum].write(numChars);
				Servidor.cliLigados[myNum] = false;
				Servidor.cliSock[myNum].close();
				Servidor.cliNicks.remove(myNum);
				Servidor.exclMult.release();

			} catch (IOException ex) {
				System.out.println("IOException\n");
			} catch (InterruptedException ex) {
				System.out.println("Não foi possível fazer lock no semáforo.\n");
			}
		}

		private boolean verificaNovoCliente(String registo) throws IOException {
			String aviso = null;
			byte[] data = new byte[300];
			if (registo.substring(11).equalsIgnoreCase("#registar: ")) {
				//verifica se a lista de nicknames registados no servidor contem o nickname do cliente que pretende registar
				if (!Servidor.cliNicks.contains(registo.substring(10, 27))) {
					//se a lista nao conter o nickname,adiciona o nickname à lista e informa o cliente do sucesso
					aviso = "SUCESSO";
					Servidor.sOut[myNum].write(aviso.length());
					data = aviso.getBytes();
					Servidor.sOut[myNum].write(data, 0, aviso.length());
					Servidor.cliNicks.add(myNum, registo.substring(10, 27));

				} else {
					//se a lista conter já o nickname informa o cliente do insucesso do registo,fecha a ligaçao
					aviso = "INSUCESSO";
					Servidor.sOut[myNum].write(aviso.length());
					data = aviso.getBytes();
					Servidor.sOut[myNum].write(data, 0, aviso.length());
					Servidor.cliLigados[myNum] = false;
					Servidor.cliSock[myNum].close();
				}
				return true;
			}
			return false;
		}
	}
}
