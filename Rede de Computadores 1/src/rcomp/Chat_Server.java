package rcomp;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Classe servidora para cada cliente ligado.
 *
 * @author 1130335
 */
public class Chat_Server implements Runnable {

	/* Número do cliente */
	int myNum;
	/* Objecto que permite ler as mensagens */
	private DataInputStream sIn;

	public Chat_Server(int numCli) {
		myNum = numCli;
	}

	@Override
	public void run() {
		int numChars;
		byte[] data = new byte[300];
		try {
			sIn = new DataInputStream(Servidor.cliSock[myNum].getInputStream());
			/* Começa pelo registo do nickname de um novo cliente */
			numChars = sIn.read();
			sIn.read(data, 0, numChars);
			String registo = new String(data, "UTF-8");
			//verifica se a mensagem se trata de um registo ou nao
			if (verificaNovoCliente(registo)) {
				//se se tratar de um registo a mensagem não é enviada para os restantes clientes
			} else {
				//caso a mensagem nao se trate de um registo, a mensagem é enviada para os outros clientes
				while (true) {
					numChars = sIn.read();
					/* O cliente sai quando enviar uma linha vazia */
					if (numChars == 0) {
						break;
					}
					sIn.read(data, 0, numChars);

					Servidor.exclMult.acquire();
					for (int i = 0; i < Servidor.MAX_CLI; i++) {
						if (Servidor.cliLigados[i]) {
							Servidor.sOut[i].write(numChars);
							Servidor.sOut[i].write(data, 0, numChars);
						}
					}
					Servidor.exclMult.release();
				}
			}
			/* Quando o cliente quiser sair do chat */
			Servidor.exclMult.acquire();
			Servidor.sOut[myNum].write(numChars);
			Servidor.cliLigados[myNum] = false;
			Servidor.cliSock[myNum].close();
			Servidor.cliNicks.remove(myNum);
			Servidor.exclMult.release();

		} catch (IOException ex) {
			System.out.println("IOException\n");
		} catch (InterruptedException ex) {
			System.out.println("Não foi possível fazer lock no semáforo.\n");
		}
	}

	private boolean verificaNovoCliente(String registo) throws IOException {
		String aviso = null;
		byte[] data = new byte[300];
		if (registo.substring(11).equalsIgnoreCase("#registar: ")) {
			//verifica se a lista de nicknames registados no servidor contem o nickname do cliente que pretende registar
			if (!Servidor.cliNicks.contains(registo.substring(10, 27))) {
				//se a lista nao conter o nickname,adiciona o nickname à lista e informa o cliente do sucesso
				aviso = "SUCESSO";
				Servidor.sOut[myNum].write(aviso.length());
				data = aviso.getBytes();
				Servidor.sOut[myNum].write(data, 0, aviso.length());
				Servidor.cliNicks.add(myNum, registo.substring(10, 27));

			} else {
				//se a lista conter já o nickname informa o cliente do insucesso do registo,fecha a ligaçao
				aviso = "INSUCESSO";
				Servidor.sOut[myNum].write(aviso.length());
				data = aviso.getBytes();
				Servidor.sOut[myNum].write(data, 0, aviso.length());
				Servidor.cliLigados[myNum] = false;
				Servidor.cliSock[myNum].close();
			}
			return true;
		}
		return false;
	}
}
