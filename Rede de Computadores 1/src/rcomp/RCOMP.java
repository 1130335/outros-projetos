/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;
import utils.Utils;

/**
 *
 * @author Sergio
 */
public class RCOMP {

	public static void main(String args[]) throws Exception {
		List<Ligacao> listaPesquisar = null;
		List<Ligacao> listaDisponiveis = null;
		List<Ligacao> listaLigados = null;
		String nick = null;
		nick = insereNickname();
		listaPesquisar = lerTxt();
		Disponiveis disponiveis = new Disponiveis(listaPesquisar, listaDisponiveis);
		String frase = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		disponiveis.pesquisar();
		listaDisponiveis = disponiveis.getM_listaDisponiveis();
		do {
			frase = in.readLine();
			if (frase.compareTo("sair") == 0) {
				fecharSocketsLigados(listaLigados);
				break;
			}
			do {
				menu(listaLigados, listaDisponiveis,
					 nick, disponiveis);
				frase = in.readLine();
			} while (frase.equalsIgnoreCase("menu"));

			for (Ligacao l : listaLigados) {

				byte[] data = new byte[300];

				DataOutputStream sOut = new DataOutputStream(l.getSock().
					getOutputStream());

				Thread serverConn = new Thread(new Tcp_chat_cli_con(l.getSock()));
				serverConn.start();

				//while (true) {
				frase = "(" + nick + ") " + frase;
				data = frase.getBytes();
				sOut.write((byte) frase.length());
				sOut.write(data, 0, (byte) frase.length());
				//}

				serverConn.join();
			}

		} while (!frase.equalsIgnoreCase("sair"));
		fecharSocketsLigados(listaLigados);
	}

	static void menu(List<Ligacao> listaLigados, List<Ligacao> listaDisponiveis,
					 String nick, Disponiveis d) throws IOException, InterruptedException {
		int escolha;
		apresentaLigados(listaLigados);
		do {
			d.pesquisar();
			listaDisponiveis = d.getM_listaDisponiveis();
			System.out.println("\"1. Alterar o estado ENVIA a servidor(es)");
			System.out.println("2. Alterar o estado RECEBE a um servidor(es)");
			System.out.println("3. Desligar servidor(es)");
			System.out.println("4. Ligar a servidor disponivel");
			System.out.println("0. Voltar para o chat");

			escolha = Integer.parseInt(Utils.readLineFromConsole("Opção: "));

			switch (escolha) {
				case 1:
					alterarEnvia(listaLigados);
					break;

				case 2:
					alterarRecebe(listaLigados);
					break;

				case 3:
					terminarLigacao(listaLigados);
					break;

				case 4:
					ligarServidor(listaLigados, listaDisponiveis, d, nick);
					break;

				default:
					System.out.println("Digite uma opcao valida");
			}
		} while (escolha != 0);

	}

	static String insereNickname() {
		String aux;
		do {
			aux = Utils.readLineFromConsole("Insira nickname: ");
		} while (aux.length() > 15);
		return aux;
	}

	static void apresentaDisponiveis(List<Ligacao> listaDisponiveis) {
		int i = 0;
		for (Ligacao l : listaDisponiveis) {
			System.out.printf("Indice: %s, %s\n", i, l.toString());
			i++;
		}
	}

	static void apresentaLigados(List<Ligacao> listaLigados) {
		int i = 0;
		for (Ligacao l : listaLigados) {
			System.out.printf("Indice: %s, %s\n", i, l.toString());
			i++;
		}
	}

	static void apresentaPorPesquisar(List<Ligacao> listaPesquisar) {
		int i = 0;
		for (Ligacao l : listaPesquisar) {
			System.out.printf("Indice: %s, %s\n", i, l.toString());
			i++;
		}
	}

	static void alterarRecebe(List<Ligacao> listaLigados) {
		apresentaLigados(listaLigados);
		int lido = Integer.parseInt(Utils.
			readLineFromConsole("Digite o indíce da ligação que pretende alterar o estado 'Recebe'"));
		listaLigados.get(lido).setEstadoRecebe();
	}

	static void alterarEnvia(List<Ligacao> listaLigados) {
		apresentaLigados(listaLigados);
		int lido = Integer.parseInt(Utils.
			readLineFromConsole("Digite o indíce da ligação que pretende alterar o estado 'Envia'"));
		listaLigados.get(lido).setEstadoEnvia();
	}

	static void terminarLigacao(List<Ligacao> listaLigados) throws IOException {
		apresentaLigados(listaLigados);
		int lido = Integer.parseInt(Utils.
			readLineFromConsole("Digite o indíce da ligação que pretende terminar ligação"));
		listaLigados.get(lido).getSock().close();
		listaLigados.remove(lido);
	}

	static void fecharSocketsLigados(List<Ligacao> listaLigados) throws IOException {
		for (Ligacao l : listaLigados) {
			l.getSock().close();
			listaLigados.remove(l);

		}
	}

	/*liga ao servidorcom base nas ligaçoes disponiveis e escolhidas pelo cliente*/
	static void ligarServidor(List<Ligacao> listaLigados,
							  List<Ligacao> listaDisponiveis, Disponiveis d,
							  String nick) throws IOException, InterruptedException {
		d.pesquisar();
		//MELHORAR PARA APENAS APRESNTAR OS QUE AINDA NAO ESTAO LIGADOS
		apresentaDisponiveis(listaDisponiveis);
		int lido = Integer.parseInt(Utils.
			readLineFromConsole("Digite o indíce do servidor que pretende ligar"));
		Ligacao l = listaDisponiveis.get(lido);

		String frase;
		byte[] data = new byte[300];
		DataOutputStream sOut = new DataOutputStream(l.getSock().
			getOutputStream());

		//ENVIAR NICK PARA O SERVIDOR REGISTAR
		frase = "#registar: " + nick;
		data = frase.getBytes();
		sOut.write((byte) frase.length());
		sOut.write(data, 0, (byte) frase.length());
		//caso na funçao recebePermissaoLigacao a resposta do servidor for sucesso o cliente obtem ligaçao
		if (recebePermissaoLigacao(l.getSock())) {
			System.out.println("Ligado com sucesso");
		} else {
			System.out.println("Insucesso");
		}
	}
	/*responsavel por saber a resposta do servidor, se consegue ligar ou nao*/

	static boolean recebePermissaoLigacao(Socket s) throws IOException {

		int nChars;
		byte[] data = new byte[300];
		String frase;
		DataInputStream sIn;

		sIn = new DataInputStream(s.getInputStream());

		nChars = sIn.read();
		sIn.read(data, 0, nChars);
		frase = new String(data, 0, nChars);
		if (frase.equalsIgnoreCase("sucesso")) {
			return true;
		}

		return false;
	}

	static List<Ligacao> lerTxt() throws FileNotFoundException {
		String txt = Utils.
			readLineFromConsole("Digite o nome do ficheiro,com a informação do servidor:");
		File f = new File(txt);
		Scanner fTxt = new Scanner(f);
		List<Ligacao> ips = null;

		while (fTxt.hasNextLine()) {
			Ligacao l = null;
			String linha = fTxt.nextLine();

			String partes[] = linha.split(" ");

			l.setNome(partes[0]);

			ips.add(l);
		}

		fTxt.close();

		return ips;
	}
}
