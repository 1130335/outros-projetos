/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class Disponiveis {

	private List<Ligacao> m_listaPesquisar;
	private List<Ligacao> m_listaDisponiveis;

	public Disponiveis(List<Ligacao> listaPesquisar,
					   List<Ligacao> listaDisponiveis) {
		m_listaDisponiveis = listaDisponiveis;
		m_listaPesquisar = listaPesquisar;

	}

	public void pesquisar() {
		// Find the server using UDP broadcast

		try {
			for (Ligacao l : m_listaPesquisar) {

				//Open a random port to send the package
				DatagramSocket c;
				c = new DatagramSocket();
				//c.setBroadcast(true);
				byte[] sendData = "DISPONIVEL?".getBytes();

				try {
					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.
																   getByName(l.
																	   getNome()), 40001);
					c.send(sendPacket);
				} catch (Exception e) {
				}

				//Wait for a response
				byte[] recvBuf = new byte[15000];

				DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);

				c.receive(receivePacket);

				//Check if the message is correct
				String message = new String(receivePacket.getData()).trim();

				if (message.equals("DISPONIVEL")) {
					Ligacao liga = new Ligacao();
					Socket soc = new Socket(receivePacket.getAddress(), 40000);
					liga.setIPdestino(receivePacket.getAddress());
					liga.setSock(soc);
					liga.setNome(l.getNome());

					m_listaDisponiveis.add(liga);
				}

				//Close the port!
				c.close();
			}
		} catch (IOException ex) {
		}

	}

	/**
	 * @return the m_listaDisponiveis
	 */
	public List<Ligacao> getM_listaDisponiveis() {
		return m_listaDisponiveis;
	}

	/**
	 * @param m_listaDisponiveis the m_listaDisponiveis to set
	 */
	public void setM_listaDisponiveis(
		List<Ligacao> m_listaDisponiveis) {
		this.m_listaDisponiveis = m_listaDisponiveis;
	}
}
