/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Vehicles.Energy;
import Model.Vehicles.Gasoline;
import Model.Project;
import Model.Vehicles.Gear;
import Model.Vehicles.Motorization;
import Model.Vehicles.Throttle;
import Model.Vehicles.Vehicle;
import Model.Vehicles.VelocityLimit;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Helder
 */
public class P05_AddVehicleToProjectControllerTest {

    public P05_AddVehicleToProjectControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    /**
//     * Test of validateAndImportNewVehicleToProject method, of class
// P05_AddVehicleToProjectController.
//     */
//    @Test
//    public void testValidateAndImportNewVehicleToProject() {
//        System.out.println("importNewVehicleToProject");
//        ArrayList<Double> gears = new ArrayList<>();
//        gears.add(3.2);
//        gears.add(2.5);
//        gears.add(1.2);
//        gears.add(0.8);
//        Vehicle car = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Gasoline(),
//                100, 30, 1.3, 15, 0.01, 0.5, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
//        Project proj = new Project(null, "projeto1", "without description");
//        Vehicle newVehicle_DifName = new Vehicle("carr", "toyota xpto", Vehicle.VehicleType.CAR, new Gasoline(),
//                1555, 30, 1.3, 15, 0.01, 0.5, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
//        Vehicle newVehicle_SameNameButDifAttributes = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Gasoline(),
//                1555, 30, 1.3, 15, 0.01, 0.5, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
//        Vehicle newVehicle_SameNameAndAttrib = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Gasoline(),
//                100, 30, 1.3, 15, 0.01, 0.5, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
//        
//        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
//        instance.setActiveProject(proj);
//        proj.addVehicleToListOfV(car);
//        boolean expResult = true;
//        boolean result = instance.validateAndImportNewVehicleToProject(newVehicle_DifName);
//        assertEquals(expResult, result);
//    }

    /**
     * Test of splitByUnderScore method, of class
     * P05_AddVehicleToProjectController.
     */
    @Test
    public void testSplitByUnderScore() {
        System.out.println("splitByUnderScore");
        String str = "carro_5";
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        String expResult = "carro";
        String result = instance.splitByUnderScore(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfLastReplica method, of class
     * P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetNumberOfLastReplica() {
        System.out.println("retirarNumeroDString2");
        String str = "car5ro_2";
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        int expResult = 2;
        int result = instance.getNumberOfLastReplica(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of getActiveProject method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetActiveProject() {
        System.out.println("getActiveProject");
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        Project expResult = null;
        Project result = instance.getActiveProject();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setActiveProject method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testSetActiveProject() {
        System.out.println("setActiveProject");
        Project activeProject = null;
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        instance.setActiveProject(activeProject);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedProjectIntoActivProj method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testSetSelectedProjectIntoActivProj() {
        System.out.println("setSelectedProjectIntoActivProj");
        String name = "";
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        Project expResult = null;
        Project result = instance.setSelectedProjectIntoActivProj(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRepository method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetRepository() {
        System.out.println("getRepository");
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        ProjectRepositoryDB expResult = new ProjectRepositoryDB();
        ProjectRepositoryDB result = instance.getRepository();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfProjInRepository method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetNumberOfProjInRepository() {
        System.out.println("getNumberOfProjInRepository");
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        int expResult = 0;
        int result = instance.getNumberOfProjInRepository();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

//    /**
//     * Test of getStringInsideQuotationMark method, of class P05_AddVehicleToProjectController.
//     */
//    @Test
//    public void testGetStringInsideQuotationMark() {
//        System.out.println("getStringInsideQuotationMark");
//        String textXML = "\"text\"";
//        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
//        String expResult = "text";
//        String result = instance.getStringInsideQuotationMark(textXML);
//        assertEquals(expResult, result);
//    }

    /**
     * Test of getTempNewListofVehicles method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetTempNewListofVehicles() {
        System.out.println("getTempNewListofVehicles");
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        List<Vehicle> expResult = null;
        List<Vehicle> result = instance.getTempNewListofVehicles();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTempNewListofVehicles method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testSetTempNewListofVehicles() {
        System.out.println("setTempNewListofVehicles");
        List<Vehicle> tempNewListofVehicles = null;
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        instance.setTempNewListofVehicles(tempNewListofVehicles);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentProjects method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testGetCurrentProjects() {
        System.out.println("getCurrentProjects");
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        List<String> expResult = null;
        List<String> result = instance.getCurrentProjects();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validatesVehicleToImportAndName method, of class P05_AddVehicleToProjectController.
     */
    @Test
    public void testValidatesVehicleToImportAndName() {
        System.out.println("validatesVehicleToImportAndName");
        Vehicle newVehicle = null;
        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
        Vehicle expResult = null;
        Vehicle result = instance.validatesVehicleToImportAndName(newVehicle);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

//    /**
//     * Test of importedNewVehiclesList method, of class P05_AddVehicleToProjectController.
//     */
//    @Test
//    public void testImportedNewVehiclesList() {
//        System.out.println("importedNewVehiclesList");
//        File xmlFile = null;
//        P05_AddVehicleToProjectController instance = new P05_AddVehicleToProjectController();
//        boolean expResult = false;
//        boolean result = instance.importedNewVehiclesList(xmlFile);
//        assertEquals(expResult, result);
//    }

}
