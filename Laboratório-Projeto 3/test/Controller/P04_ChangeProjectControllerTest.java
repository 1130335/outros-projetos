/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import Repository.Oracle.ProjectRepositoryDB;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class P04_ChangeProjectControllerTest {
    
    public P04_ChangeProjectControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of changeProjectNameAndDescription method, of class P04_ChangeProjectController.
     */
    @Test
    public void testChangeProjectNameAndDescription() {
        System.out.println("changeProjectNameAndDescription");
        String newName = "abc";
        String newDescription = "abcd";
        MainController mc = new MainController();
        P04_ChangeProjectController instance = new P04_ChangeProjectController();
        instance.changeProjectNameAndDescription(newName, newDescription, mc);
    }
    
}
