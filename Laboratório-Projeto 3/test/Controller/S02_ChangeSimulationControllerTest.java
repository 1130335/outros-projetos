/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Repository.Oracle.ProjectRepositoryDB;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class S02_ChangeSimulationControllerTest {
    
    public S02_ChangeSimulationControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of changeSimulation method, of class S02_ChangeSimulationController.
     */
    @Test
    public void testChangeSimulation() {
        System.out.println("changeSimulation");
        String newName = "s2";
        String newDescription = "d2";
        MainController mc = new MainController();
        S02_ChangeSimulationController instance = new S02_ChangeSimulationController();
        instance.changeSimulation(newName, newDescription, mc);
    }

    
    
}
