/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import Model.RoadNetwork;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class P03_CopyProjectControllerTest {

    public P03_CopyProjectControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of copyProject method, of class P03_CopyProjectController.
     */
    @Test
    public void testCopyProject() {
        System.out.println("copyProject");
        String newName = "P04";
        String oldName = "P01";
        String description = "P04 description";
        MainController mcTest = new MainController();
        P03_CopyProjectController instance = new P03_CopyProjectController();
        instance.copyProject(newName, oldName, description, mcTest);
        ProjectRepositoryDB pr = instance.getRepository();
        Project p04 = pr.getProjectByName(newName);
        assertEquals(p04.getName(), "P04");
    }

    /**
     * Test of getCurrentProjects method, of class P03_CopyProjectController.
     */
    @Test
    public void testGetCurrentProjects() {
        System.out.println("getCurrentProjects");
        P03_CopyProjectController instance = new P03_CopyProjectController();
        RoadNetwork rn = new RoadNetwork();
        Project p01 = new Project(rn, "P01", "D01");
        Project p02 = new Project(rn, "P02", "D02");
        Project p03 = new Project(rn, "P03", "D03");
        List<String> expResult = new ArrayList<>();
        expResult.add(p01.getName());
        expResult.add(p02.getName());
        expResult.add(p03.getName());
        ProjectRepositoryDB pr = instance.getRepository();
        pr.addProject(p01);
        pr.addProject(p02);
        pr.addProject(p03);
        List<String> result = instance.getCurrentProjects();
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyName method, of class P03_CopyProjectController.
     */
    @Test
    public void testVerifyName() {
        System.out.println("verifyName");
        String name = "NewName";
        P03_CopyProjectController instance = new P03_CopyProjectController();
        boolean expResult = true;
        boolean result = instance.verifyName(name);
        assertEquals(expResult, result);
    }

}
