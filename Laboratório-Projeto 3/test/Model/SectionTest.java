/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.Vertex;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author André
 */
public class SectionTest {
    
    public SectionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBeginningNode method, of class Section.
     */
    @Test
    public void testGetSetBeginningNode() {
        System.out.println("getBeginningNode");
        Section instance = new Section();
        Node node = new Node();
        instance.setBeginningNode(node);
        Node expResult = node;
        Node result = instance.getBeginningNode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndingNode method, of class Section.
     */
    @Test
    public void testGetSetEndingNode() {
        System.out.println("getEndingNode");
        Section instance = new Section();
        Node node = new Node();
        instance.setEndingNode(node);
        Node expResult = node;
        Node result = instance.getEndingNode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTipology method, of class Section.
     */
    @Test
    public void testGetSetTipology() {
        System.out.println("getTipology");
        Section instance = new Section();
        Section.Typology expResult = Section.Typology.REGULAR_ROAD;
        instance.setTypology(expResult);
        Section.Typology result = instance.getTipology();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDirection method, of class Section.
     */
    @Test
    public void testGetSetDirection() {
        System.out.println("getDirection");
        Section instance = new Section();
        Section.Direction expResult = Section.Direction.DIRECT;
        instance.setDirection(expResult);
        Section.Direction result = instance.getDirection();
        assertEquals(expResult, result);
    }

    /**
     * Test of getToll method, of class Section.
     */
    @Test
    public void testGetSetToll() {
        System.out.println("getToll");
        Section instance = new Section();
        int expResult = 0;
        instance.setToll(expResult);
        int result = instance.getToll();
        assertEquals(expResult, result);
    }

    /**
     * Test of getWindDirection method, of class Section.
     */
    @Test
    public void testGetSetWindDirection() {
        System.out.println("getWindDirection");
        Section instance = new Section();
        double expResult = 0.0;
        instance.setWindDirection(expResult);
        double result = instance.getWindDirection();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getWindSpeed method, of class Section.
     */
    @Test
    public void testGetSetWindSpeed() {
        System.out.println("getWindSpeed");
        Section instance = new Section();
        double expResult = 0.0;
        instance.setWindSpeed(expResult);
        double result = instance.getWindSpeed();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getSequenceOfSegments method, of class Section.
     */
    @Test
    public void testGetSetSequenceOfSegments() {
        System.out.println("getSequenceOfSegments");
        Section instance = new Section();
        Segment s1 = new Segment();
        Segment s2 = new Segment();
        List<Segment> expResult = new ArrayList<>();
        expResult.add(s1);
        expResult.add(s2);
        instance.setSequenceOfSegments(expResult);
        List<Segment> result = instance.getSequenceOfSegments();
        assertEquals(expResult, result);
    }

    /**
     * Test of addSegment method, of class Section.
     */
    @Test
    public void testAddGetSegment() {
        System.out.println("addSegment");
        Segment segment = new Segment();
        Section instance = new Section();
        instance.addSegment(segment);
        assertEquals(instance.getSegmentByIndex(0), segment);
    }

    /**
     * Test of removeSegment method, of class Section.
     */
    @Test
    public void testRemoveSegment() {
        System.out.println("removeSegment");
        int segmentIndex = 0;
        Section instance = new Section();
        Segment s1 = new Segment();
        Segment s2= new Segment();
        instance.addSegment(s1);
        instance.addSegment(s2);
        boolean expResult = true;
        boolean result = instance.removeSegment(segmentIndex);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBeginningNode method, of class Section.
     */
    @Test
    public void testGetBeginningNode() {
        System.out.println("getBeginningNode");
        Section instance = new Section();
        Node expResult = null;
        Node result = instance.getBeginningNode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEndingNode method, of class Section.
     */
    @Test
    public void testGetEndingNode() {
        System.out.println("getEndingNode");
        Section instance = new Section();
        Node expResult = null;
        Node result = instance.getEndingNode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTipology method, of class Section.
     */
    @Test
    public void testGetTipology() {
        System.out.println("getTipology");
        Section instance = new Section();
        Section.Typology expResult = null;
        Section.Typology result = instance.getTipology();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDirection method, of class Section.
     */
    @Test
    public void testGetDirection() {
        System.out.println("getDirection");
        Section instance = new Section();
        Section.Direction expResult = null;
        Section.Direction result = instance.getDirection();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getToll method, of class Section.
     */
    @Test
    public void testGetToll() {
        System.out.println("getToll");
        Section instance = new Section();
        int expResult = 0;
        int result = instance.getToll();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getWindDirection method, of class Section.
     */
    @Test
    public void testGetWindDirection() {
        System.out.println("getWindDirection");
        Section instance = new Section();
        double expResult = 0.0;
        double result = instance.getWindDirection();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getWindSpeed method, of class Section.
     */
    @Test
    public void testGetWindSpeed() {
        System.out.println("getWindSpeed");
        Section instance = new Section();
        double expResult = 0.0;
        double result = instance.getWindSpeed();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSequenceOfSegments method, of class Section.
     */
    @Test
    public void testGetSequenceOfSegments() {
        System.out.println("getSequenceOfSegments");
        Section instance = new Section();
        List<Segment> expResult = null;
        List<Segment> result = instance.getSequenceOfSegments();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBeginningNode method, of class Section.
     */
    @Test
    public void testSetBeginningNode() {
        System.out.println("setBeginningNode");
        Node beginningNode = null;
        Section instance = new Section();
        instance.setBeginningNode(beginningNode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEndingNode method, of class Section.
     */
    @Test
    public void testSetEndingNode() {
        System.out.println("setEndingNode");
        Node endingNode = null;
        Section instance = new Section();
        instance.setEndingNode(endingNode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTypology method, of class Section.
     */
    @Test
    public void testSetTypology() {
        System.out.println("setTypology");
        Section.Typology typology = null;
        Section instance = new Section();
        instance.setTypology(typology);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDirection method, of class Section.
     */
    @Test
    public void testSetDirection() {
        System.out.println("setDirection");
        Section.Direction direction = null;
        Section instance = new Section();
        instance.setDirection(direction);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setToll method, of class Section.
     */
    @Test
    public void testSetToll() {
        System.out.println("setToll");
        int toll = 0;
        Section instance = new Section();
        instance.setToll(toll);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setWindDirection method, of class Section.
     */
    @Test
    public void testSetWindDirection() {
        System.out.println("setWindDirection");
        double windDirection = 0.0;
        Section instance = new Section();
        instance.setWindDirection(windDirection);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setWindSpeed method, of class Section.
     */
    @Test
    public void testSetWindSpeed() {
        System.out.println("setWindSpeed");
        double windSpeed = 0.0;
        Section instance = new Section();
        instance.setWindSpeed(windSpeed);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSequenceOfSegments method, of class Section.
     */
    @Test
    public void testSetSequenceOfSegments() {
        System.out.println("setSequenceOfSegments");
        List<Segment> sequenceOfSegments = null;
        Section instance = new Section();
        instance.setSequenceOfSegments(sequenceOfSegments);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of listSequenceOfSegments method, of class Section.
     */
    @Test
    public void testListSequenceOfSegments() {
        System.out.println("listSequenceOfSegments");
        Section instance = new Section();
        String expResult = "";
        String result = instance.listSequenceOfSegments();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Section.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Section instance = new Section();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addSegment method, of class Section.
     */
    @Test
    public void testAddSegment() {
        System.out.println("addSegment");
        Segment segment = null;
        Section instance = new Section();
        instance.addSegment(segment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSegmentByIndex method, of class Section.
     */
    @Test
    public void testGetSegmentByIndex() {
        System.out.println("getSegmentByIndex");
        int segmentIndex = 0;
        Section instance = new Section();
        Segment expResult = null;
        Segment result = instance.getSegmentByIndex(segmentIndex);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEndpoints method, of class Section.
     */
    @Test
    public void testGetEndpoints() {
        System.out.println("getEndpoints");
        Section instance = new Section();
        Node[] expResult = null;
        Node[] result = instance.getEndpoints();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSectionEdge method, of class Section.
     */
    @Test
    public void testGetSectionEdge() {
        System.out.println("getSectionEdge");
        Graph<Node, Section> roadmap1 = new Graph<>(true);

        Node n1 = new Node();
        Node n2 = new Node();
        roadmap1.insertVertex(n1);
        roadmap1.insertVertex(n2);
        Section instance = new Section();
        Section sec1 = new Section(instance);
        sec1.setBeginningNode(n1);
        sec1.setEndingNode(n2);
        
        Segment seg1 = new Segment();
        seg1.setLength(6);
        sec1.getSequenceOfSegments().add(seg1);
        
        roadmap1.insertEdge(n1, n2, sec1, 0);
        
        Section expResult = sec1;
        Section result = instance.getSectionEdge(roadmap1.getVertex(n1), roadmap1.getVertex(n1));
        assertEquals(expResult, result);
    }

    /**
     * Test of getEdge method, of class Section.
     */
    @Test
    public void testGetEdge() {
        System.out.println("getEdge");
        Vertex<Node, Section> vorig = null;
        Vertex<Node, Section> vdest = null;
        Section instance = new Section();
        Edge<Node, Section> expResult = null;
        Edge<Node, Section> result = instance.getEdge(vorig, vdest);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSectionEdge2 method, of class Section.
     */
    @Test
    public void testGetSectionEdge2() {
        System.out.println("getSectionEdge2");
        //Graph<Node, Section> roadmap1 = new Graph<>(true);
        RoadNetwork roadmap1 = new RoadNetwork();
        Node n1 = new Node();
        Node n2 = new Node();
//        roadmap1.insertVertex(n1);
//        roadmap1.insertVertex(n2);
        Section sec1 = new Section();
        sec1.setBeginningNode(n1);
        sec1.setEndingNode(n2);
        
        Segment seg1 = new Segment();
        seg1.setLength(6);
        sec1.getSequenceOfSegments().add(seg1);
        
  //      roadmap1.insertEdge(n1, n2, sec1, 0);
        
        //Edge result = roadmap1.getVertex(n1).getOutgoing().get(roadmap1.getVertex(n2));
        System.out.println("Existe Edge?" + roadmap1.existsConnection(n1, n2));//return vorig.getOutgoing().get(vdest);
        Section instance = new Section(sec1);
        Section expResult = sec1;
        Section result = sec1;
        //Section result = Section.getSectionEdge2(n1, n2);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRoad method, of class Section.
     */
    @Test
    public void testGetRoad() {
        System.out.println("getRoad");
        Section instance = new Section();
        instance.setRoad("A01");
        String expResult = "A01";
        String result = instance.getRoad();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRoad method, of class Section.
     */
    @Test
    public void testSetRoad() {
        System.out.println("setRoad");
        String road = "A01";
        Section instance = new Section();
        instance.setRoad(road);
        String expResult = "A01";
        String result = instance.getRoad();
        assertEquals(expResult, result);
    }
    
}
