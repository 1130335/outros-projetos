
package Model;

import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class SimulationTest {
    
    private Simulation instance = new Simulation();
    
    public SimulationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getId method, of class Simulation.
     */
    @Test
    public void testGetSetName() {
        System.out.println("getId");
        String expResult = "Simulation1";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription method, of class Simulation.
     */
    @Test
    public void testGetSetDescription() {
        System.out.println("getDescription");
        String expResult = "Description1";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTrafficList method, of class Simulation.
     */
    @Test
    public void testGetSetTrafficList() {
        System.out.println("getTrafficList");
        ArrayList<TrafficPattern> expResult = new ArrayList<>();
        TrafficPattern a = new TrafficPattern("1", "2", "vehicle1", 5, "segundo");
        expResult.add(a);
        instance.setTrafficList(expResult);
        ArrayList<TrafficPattern> result = instance.getTrafficList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getActiveProject method, of class Simulation.
     */
    @Test
    public void testGetSetActiveProject() {
        System.out.println("getActiveProject");
        Project expResult = new Project();
        instance.setActiveProject(expResult);
        Project result = instance.getActiveProject();
        assertEquals(expResult, result);
    }

    

    /**
     * Test of addTrafficPattern method, of class Simulation.
     */
    @Test
    public void testAddTrafficPattern() {
        System.out.println("addTrafficPattern");
        TrafficPattern tp = new TrafficPattern("1", "2", "vehicle1", 5, "segundo");
        instance.addTrafficPattern(tp);
    }

    
    
}
