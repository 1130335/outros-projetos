/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Helder
 */
public class ProjectTest {
    
    public ProjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRoadNetwork and setRoadNetwork methods, of class Project.
     */
    @Test
    public void testGetSetRoadNetwork() {
        System.out.println("getRoadNetwork");
        Project instance = new Project(new RoadNetwork(), "Name", "Description");
        RoadNetwork expResult = new RoadNetwork();
        instance.setRoadNetwork(expResult);
        RoadNetwork result = instance.getRoadNetwork();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName and setName methods, of class Project.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Project instance = new Project(new RoadNetwork(), "Name", "Description");
        String expResult = "NewName";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription and setDescription methods, of class Project.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Project instance = new Project(new RoadNetwork(), "Name", "Description");
        String expResult = "NewDescription";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of isActive method, of class Project.
     */
    @Test
    public void testIsActive() {
        System.out.println("isActive");
        Project instance = new Project(new RoadNetwork(), "Name", "Description");
        boolean expResult = true;
        instance.setActive();
        boolean result = instance.isActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListOfVehicles method, of class Project.
     */
    @Test
    public void testGetListOfVehicles() {
        System.out.println("getListOfVehicles");
        Vehicle v1 = new Vehicle();
        Project instance = new Project(null, "without name", "without description");
        instance.addVehicleToListOfV(v1);
        List<Vehicle> expResult = new ArrayList<>();
        expResult.add(v1);
        List<Vehicle> result = instance.getListOfVehicles();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListOfVehicles method, of class Project.
     */
    @Test
    public void testSetListOfVehicles() {
        System.out.println("setListOfVehicles");
        Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle();
        Project instance = new Project(null, "without name", "without description");
        instance.addVehicleToListOfV(v1);
        
        List<Vehicle> listOfVehicles2 = new ArrayList<>();
        listOfVehicles2.add(v2);
        instance.setListOfVehicles(listOfVehicles2);
        
        List <Vehicle> result = instance.getListOfVehicles();
        List <Vehicle> expResult = listOfVehicles2;
        assertEquals(expResult, result);
    }

    /**
     * Test of addVehicleToListOfV method, of class Project.
     */
    @Test
    public void testAddVehicleToListOfV() {
        System.out.println("addVehicleToListOfV");
        Vehicle v = new Vehicle();
        Project instance = new Project(null, "without name", "without description");
        boolean expResult = true;
        boolean result = instance.addVehicleToListOfV(v);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeVehicleToListOfV method, of class Project.
     */
    @Test
    public void testRemoveVehicleToListOfV() {
        System.out.println("removeVehicleToListOfV");
        Vehicle v = new Vehicle();
        Project instance = new Project(null, "without name", "without description");
        instance.addVehicleToListOfV(v);
        boolean expResult = true;
        boolean result = instance.removeVehicleToListOfV(v);
        assertEquals(expResult, result);
    }
    
}
