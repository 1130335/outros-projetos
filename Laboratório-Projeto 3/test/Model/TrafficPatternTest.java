/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.Vehicles.Vehicle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class TrafficPatternTest {
    
    private TrafficPattern instance = new TrafficPattern();
    
    public TrafficPatternTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getBeginNode method, of class TrafficPattern.
     */
    @Test
    public void testGetSetBeginNode() {
        System.out.println("getBeginNode");
        String expResult = "1";
        instance.setBeginNodeId(expResult);
        String result = instance.getBeginNodeId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndNode method, of class TrafficPattern.
     */
    @Test
    public void testGetSetEndNode() {
        System.out.println("getEndNode");
        String expResult = "2";
        instance.setEndNodeId(expResult);
        String result = instance.getEndNodeId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getVehicle method, of class TrafficPattern.
     */
    @Test
    public void testGetSetVehicle() {
        System.out.println("getVehicle");
        String expResult = "vehicle 1";
        instance.setVehicle(expResult);
        String  result = instance.getVehicle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getArrivalRate method, of class TrafficPattern.
     */
    @Test
    public void testGetSetArrivalRate() {
        System.out.println("getArrivalRate");
        int expResult = 10;
        instance.setArrivalRate(expResult);
        int result = instance.getArrivalRate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTimeUnit method, of class TrafficPattern.
     */
    @Test
    public void testGetSetTimeUnit() {
        System.out.println("getTimeUnit");
        String expResult = "m";
        instance.setTimeUnit(expResult);
        String result = instance.getTimeUnit();
        assertEquals(expResult, result);
    }


    /**
     * Test of toString method, of class TrafficPattern.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TrafficPattern teste = new TrafficPattern("1", "2", "vehicle", 15, "m");
        String expResult = "TrafficPattern{" + "beginNodeId=" + "1" + ", endNodeId=" + "2" 
                + ", vehicleId=" + "vehicle" + ", arrivalRate=" + 15 + ", timeUnit=" + "m" + '}';
        String result = teste.toString();
        assertEquals(expResult, result);
    }



}
