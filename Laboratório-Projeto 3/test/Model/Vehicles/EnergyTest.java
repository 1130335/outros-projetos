/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class EnergyTest {
    
    public EnergyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getMinRpm method, of class Energy.
     */
    @Test
    public void testGetSetMinRpm() {
        System.out.println("getMinRpm");
        Energy instance = new Energy();
        double expResult = 2;
        instance.setMinRpm(expResult);
        double result = instance.getMinRpm();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getMaxRpm method, of class Energy.
     */
    @Test
    public void testGetSetMaxRpm() {
        System.out.println("getMaxRpm");
        Energy instance = new Energy();
        double expResult = 3;
        instance.setMaxRpm(expResult);
        double result = instance.getMaxRpm();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFinalDriveRatio method, of class Energy.
     */
    @Test
    public void testGetSetFinalDriveRatio() {
        System.out.println("getFinalDriveRatio");
        Energy instance = new Energy();
        double expResult = 2;
        instance.setFinalDriveRatio(expResult);
        double result = instance.getFinalDriveRatio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getGearList method, of class Energy.
     */
    @Test
    public void testGetSetGearList() {
        System.out.println("getGearList");
        Energy instance = new Energy();
        ArrayList<Gear> expResult = new ArrayList<>();
        expResult.add(new Gear("01", 1));
        instance.setGearList(expResult);
        ArrayList<Gear> result = instance.getGearList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getThrottleList method, of class Energy.
     */
    @Test
    public void testGetSetThrottleList() {
        System.out.println("getThrottleList");
        Energy instance = new Energy();
        ArrayList<Throttle> expResult = new ArrayList<>();
        expResult.add(new Throttle());
        instance.setThrottleList(expResult);
        ArrayList<Throttle> result = instance.getThrottleList();
        assertEquals(expResult, result);
    }

   
    
}
