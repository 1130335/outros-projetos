/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class GearTest {
    
    public GearTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getGearID method, of class Gear.
     */
    @Test
    public void testGetSetGearID() {
        System.out.println("getGearID");
        Gear instance = new Gear();
        String expResult = "01";
        instance.setGearID(expResult);
        String result = instance.getGearID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRatio method, of class Gear.
     */
    @Test
    public void testGetSetRatio() {
        System.out.println("getRatio");
        Gear instance = new Gear();
        double expResult = 2;
        instance.setRatio(expResult);
        double result = instance.getRatio();
        assertEquals(expResult, result, 0.0);
    }

   

    
    
}
