/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class RegimeTest {
    
    public RegimeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getTorque method, of class Regime.
     */
    @Test
    public void testGetSetTorque() {
        System.out.println("getTorque");
        Regime instance = new Regime();
        double expResult = 5;
        instance.setTorque(expResult);
        double result = instance.getTorque();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getRpmLow method, of class Regime.
     */
    @Test
    public void testGetSetRpmLow() {
        System.out.println("getRpmLow");
        Regime instance = new Regime();
        double expResult = 2;
        instance.setRpmLow(expResult);
        double result = instance.getRpmLow();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getRpmHigh method, of class Regime.
     */
    @Test
    public void testGetSetRpmHigh() {
        System.out.println("getRpmHigh");
        Regime instance = new Regime();
        double expResult = 4;
        instance.setRpmHigh(expResult);
        double result = instance.getRpmHigh();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getSfc method, of class Regime.
     */
    @Test
    public void testGetSetSfc() {
        System.out.println("getSfc");
        Regime instance = new Regime();
        double expResult = 8;
        instance.setSfc(expResult);
        double result = instance.getSfc();
        assertEquals(expResult, result, 0.0);
    }

    
}
