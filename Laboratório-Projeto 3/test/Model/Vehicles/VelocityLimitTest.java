/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import Model.Section;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class VelocityLimitTest {
    
    public VelocityLimitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getSectionType method, of class VelocityLimit.
     */
    @Test
    public void testGetSetSectionType() {
        System.out.println("getSectionType");
        VelocityLimit instance = new VelocityLimit();
        Section.Typology expResult = Section.Typology.CONTROLLED_ACCESS_HIGHWAY;
        instance.setSectionType(expResult);
        Section.Typology result = instance.getSectionType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSpeedLimit method, of class VelocityLimit.
     */
    @Test
    public void testGetSetSpeedLimit() {
        System.out.println("getSpeedLimit");
        VelocityLimit instance = new VelocityLimit();
        double expResult = 100;
        instance.setSpeedLimit(expResult);
        double result = instance.getSpeedLimit();
        assertEquals(expResult, result, 0.0);
    }


    
}
