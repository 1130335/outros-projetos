/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class ThrottleTest {
    
    public ThrottleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getId method, of class Throttle.
     */
    @Test
    public void testGetSetId() {
        System.out.println("getId");
        Throttle instance = new Throttle();
        String expResult = "25";
        instance.setId(expResult);
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of getRegimeList method, of class Throttle.
     */
    @Test
    public void testGetSetRegimeList() {
        System.out.println("getRegimeList");
        Throttle instance = new Throttle();
        ArrayList<Regime> expResult = new ArrayList<>();
        expResult.add(new Regime(2, 2, 2, 2));
        instance.setRegimeList(expResult);
        ArrayList<Regime> result = instance.getRegimeList();
        assertEquals(expResult, result);
    }
    
}
