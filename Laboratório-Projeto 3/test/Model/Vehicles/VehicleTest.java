/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import Model.Section;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class VehicleTest {
    private Vehicle instance = new Vehicle();
    
    
    public VehicleTest() {

    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getType method, of class Vehicle.
     */
    @Test
    public void testGetSetType() {
        System.out.println("getType");
        Vehicle.VehicleType expResult = Vehicle.VehicleType.CAR;
        instance.setType(expResult);
        Vehicle.VehicleType result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMotorization method, of class Vehicle.
     */
    @Test
    public void testGetSetMotorization() {
        System.out.println("getMotorization");
        Motorization expResult = new Gasoline();
        instance.setMotorization(expResult);
        Motorization result = instance.getMotorization();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMass method, of class Vehicle.
     */
    @Test
    public void testGetSetMass() {
        System.out.println("getMass");
        double expResult = 2400;
        instance.setMass(expResult);
        double result = instance.getMass();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLoad method, of class Vehicle.
     */
    @Test
    public void testGetSetLoad() {
        System.out.println("getLoad");
        double expResult = 420;
        instance.setLoad(expResult);
        double result = instance.getLoad();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getDragCoefficient method, of class Vehicle.
     */
    @Test
    public void testGetSetDragCoefficient() {
        System.out.println("getDragCoefficient");
        double expResult = 0.38;
        instance.setDragCoefficient(expResult);
        double result = instance.getDragCoefficient();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getName method, of class Vehicle.
     */
    @Test
    public void testGetSetName() {
        System.out.println("getName");
        String expResult = "Dummy01";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription method, of class Vehicle.
     */
    @Test
    public void testGetSetDescription() {
        System.out.println("getDescription");
        String expResult = "Dummy teste vehicle 01";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRrc method, of class Vehicle.
     */
    @Test
    public void testGetSetRrc() {
        System.out.println("getRrc");
        double expResult = 0.01;
        instance.setRrc(expResult);
        double result = instance.getRrc();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getWheelSize method, of class Vehicle.
     */
    @Test
    public void testGetSetWheelSize() {
        System.out.println("getWheelSize");
        double expResult = 0.7;
        instance.setWheelSize(expResult);
        double result = instance.getWheelSize();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getEnergy method, of class Vehicle.
     */
    @Test
    public void testGetSetEnergy() {
        System.out.println("getEnergy");
        
        ArrayList<Gear> gearList = new ArrayList<>();
        gearList.add(new Gear("01", 3.5));
        gearList.add(new Gear("02", 2.5));
        gearList.add(new Gear("03", 1.9));
        gearList.add(new Gear("04", 1.2));
        gearList.add(new Gear("05", 0.8));
        ArrayList<Throttle> throttleList = new ArrayList<>();
        ArrayList<Regime> regimeList = new ArrayList<>();
        regimeList.add(new Regime(105, 1000, 2499, 8.2));
        regimeList.add(new Regime(115, 2500, 3999, 6.2));
        regimeList.add(new Regime(100, 4000, 5500, 10.2));
        throttleList.add(new Throttle("25", regimeList));
        ArrayList<Regime> regimeList2 = new ArrayList<>();
        regimeList2.add(new Regime(135, 1000, 2499, 5.2));
        regimeList2.add(new Regime(150, 2500, 3999, 3.2));
        regimeList2.add(new Regime(130, 4000, 5500, 8.2));
        throttleList.add(new Throttle("50", regimeList2));
        ArrayList<Regime> regimeList3 = new ArrayList<>();
        regimeList3.add(new Regime(220, 1000, 2499, 2.2));
        regimeList3.add(new Regime(250, 2500, 3999, 1.2));
        regimeList3.add(new Regime(190, 4000, 5500, 4.2));
        throttleList.add(new Throttle("100", regimeList3));
        Energy expResult = new Energy(1000, 5500, 2.6, gearList, throttleList, 0);
        
        instance.setEnergy(expResult);
        Energy result = instance.getEnergy();
        assertEquals(expResult, result);
    }


    /**
     * Test of getLimitList method, of class Vehicle.
     */
    @Test
    public void testGetSetLimitList() {
        System.out.println("getLimitList");
        List<VelocityLimit> expResult = new ArrayList<>();
        expResult.add(new VelocityLimit(Section.Typology.CONTROLLED_ACCESS_HIGHWAY, 100));
        instance.setLimitList(expResult);
        List<VelocityLimit> result = instance.getLimitList();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Vehicle.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Vehicle{" + "type=" + instance.getType() + ", motorization=" 
                + instance.getMotorization() + ", name=" + instance.getName() + ", description=" + instance.getDescription() + '}';
        String result = instance.toString();
        assertEquals(expResult, result);
    }


    /**
     * Test of equals method, of class Vehicle.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        ArrayList<VelocityLimit> limitList = new ArrayList<>();
        limitList.add(new VelocityLimit(Section.Typology.CONTROLLED_ACCESS_HIGHWAY, 100));
        ArrayList<Gear> gearList = new ArrayList<>();
        gearList.add(new Gear("01", 3.5));
        gearList.add(new Gear("02", 2.5));
        gearList.add(new Gear("03", 1.9));
        gearList.add(new Gear("04", 1.2));
        gearList.add(new Gear("05", 0.8));
        ArrayList<Throttle> throttleList = new ArrayList<>();
        ArrayList<Regime> regimeList = new ArrayList<>();
        regimeList.add(new Regime(105, 1000, 2499, 8.2));
        regimeList.add(new Regime(115, 2500, 3999, 6.2));
        regimeList.add(new Regime(100, 4000, 5500, 10.2));
        throttleList.add(new Throttle("25", regimeList));
        ArrayList<Regime> regimeList2 = new ArrayList<>();
        regimeList2.add(new Regime(135, 1000, 2499, 5.2));
        regimeList2.add(new Regime(150, 2500, 3999, 3.2));
        regimeList2.add(new Regime(130, 4000, 5500, 8.2));
        throttleList.add(new Throttle("50", regimeList2));
        ArrayList<Regime> regimeList3 = new ArrayList<>();
        regimeList3.add(new Regime(220, 1000, 2499, 2.2));
        regimeList3.add(new Regime(250, 2500, 3999, 1.2));
        regimeList3.add(new Regime(190, 4000, 5500, 4.2));
        throttleList.add(new Throttle("100", regimeList3));
        Energy energy = new Energy(1000, 5500, 2.6, gearList, throttleList, 0);
        Vehicle obj = new Vehicle();
                
                //new Vehicle("car1", "toyota1", Vehicle.VehicleType.CAR, new Gasoline(), 2400, 420, 0.38, 2.4, 0.01, 0.7, limitList, energy);
        Vehicle inst = new Vehicle();
        boolean expResult = true;
        boolean result = inst.equals(obj);
        assertEquals(expResult, result);
    }

    
}
