/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author André
 */
public class NodeTest {
    
    public NodeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRoadsIn method, of class Node.
     */
    @Test
    public void testGetRoadsIn() {
        System.out.println("getRoadsIn");
        List<Section>roadsIn = new ArrayList<>();
        Section s1 = new Section();
        Section s2 = new Section();
        roadsIn.add(s2);roadsIn.add(s1);
        
        Node instance = new Node(roadsIn,null);
        
        List<Section> expResult = roadsIn;
        List<Section> result = instance.getRoadsIn();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRoadsOut method, of class Node.
     */
    @Test
    public void testGetRoadsOut() {
        System.out.println("getRoadsOut");
        List<Section>roadsOut = new ArrayList<>();
        Section s1 = new Section();
        Section s2 = new Section();
        roadsOut.add(s2);roadsOut.add(s1);
        
        Node instance = new Node(null,roadsOut);
        
        List<Section> expResult = roadsOut;
        List<Section> result = instance.getRoadsOut();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Node.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Node instance = new Node("n1");
        String expResult = "n1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
