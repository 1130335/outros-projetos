package Model;

import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Helder
 */
public class SegmentTest {

    Segment s = new Segment(1, 2.0, 3.0, 4.0, 5.0, 6, 7.0, 8.0);

    public SegmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSegm_index method, of class Segment.
     */
    @Test
    public void testGetSegm_index() {
        System.out.println("getSegm_index");
        Segment instance = new Segment(s);
        int expResult = 1;
        int result = instance.getSegm_index();
        assertEquals(expResult, result);
        
        Section sec = new Section();
        Segment s2 = new Segment();
        sec.addSegment(s2);        
        int expResult2 = 1;
        int result2 = instance.getSegm_index();
        assertEquals(expResult2, result2);

    }

    /**
     * Test of setSegm_index method, of class Segment.
     */
    @Test
    public void testSetSegm_index() {
        System.out.println("setSegm_index");
        int segm_index = 3;
        Segment instance = new Segment(s);
        instance.setSegm_index(segm_index);
        int expResult = 3;
        int result = instance.getSegm_index();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInitialHeigh method, of class Segment.
     */
    @Test
    public void testGetInitialHeigh() {
        System.out.println("getInitialHeigh");
        Segment instance = new Segment(s);
        double expResult = 2.0;
        double result = instance.getInitialHeigh();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setInitialHeigh method, of class Segment.
     */
    @Test
    public void testSetInitialHeigh() {
        System.out.println("setInitialHeigh");
        double initialHeigh = 1.0;
        Segment instance = new Segment(s);
        instance.setInitialHeigh(initialHeigh);
        double expResult = 1.0;
        double result = instance.getInitialHeigh();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getSlope method, of class Segment.
     */
    @Test
    public void testGetSlope() {
        System.out.println("getSlope");
        Segment instance = new Segment(s);
        double expResult = 3.0;
        double result = instance.getSlope();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setSlope method, of class Segment.
     */
    @Test
    public void testSetSlope() {
        System.out.println("setSlope");
        double slope = 1.0;
        Segment instance = new Segment(s);
        instance.setSlope(slope);
        double expResult = 1.0;
        double result = instance.getSlope();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLength method, of class Segment.
     */
    @Test
    public void testGetLength() {
        System.out.println("getLength");
        Segment instance = new Segment(s);
        double expResult = 4.0;
        double result = instance.getLength();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setLength method, of class Segment.
     */
    @Test
    public void testSetLength() {
        System.out.println("setLength");
        double length = 1.0;
        Segment instance = new Segment();
        instance.setLength(length);
        double expResult = 1.0;
        double result = instance.getLength();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getRollResistCoef method, of class Segment.
     */
    @Test
    public void testGetRollResistCoef() {
        System.out.println("getRollResistCoef");
        Segment instance = new Segment(s);
        double expResult = 5.0;
        double result = instance.getRollResistCoef();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setRollResistCoef method, of class Segment.
     */
    @Test
    public void testSetRollResistCoef() {
        System.out.println("setRollResistCoef");
        double rollResistCoef = 1.0;
        Segment instance = new Segment(s);
        instance.setRollResistCoef(rollResistCoef);
        double expResult = 1.0;
        double result = instance.getRollResistCoef();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getMaxNumOfVehicles method, of class Segment.
     */
    @Test
    public void testGetMaxNumOfVehicles() {
        System.out.println("getMaxNumOfVehicles");
        Segment instance = new Segment(s);
        int expResult = 6;
        int result = instance.getMaxNumOfVehicles();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMaxNumOfVehicles method, of class Segment.
     */
    @Test
    public void testSetMaxNumOfVehicles() {
        System.out.println("setMaxNumOfVehicles");
        int maxNumOfVehicles = 1;
        Segment instance = new Segment(s);
        instance.setMaxNumOfVehicles(maxNumOfVehicles);
        int expResult = 1;
        int result = instance.getMaxNumOfVehicles();
        assertEquals(expResult, result);
    }

        /**
     * Test of getMinVelocity method, of class Segment.
     */
    @Test
    public void testGetMinVelocity() {
        System.out.println("getMinVelocity");
        Segment instance = new Segment(s);
        double expResult = 7.0;
        double result = instance.getMinVelocity();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setMinVelocity method, of class Segment.
     */
    @Test
    public void testSetMinVelocity() {
        System.out.println("setMinVelocity");
        double minVelocity = 1.0;
        Segment instance = new Segment();
        instance.setMinVelocity(minVelocity);
        double expResult = 1.0;
        double result = instance.getMinVelocity();
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of getMaxVelocity method, of class Segment.
     */
    @Test
    public void testGetMaxVelocity() {
        System.out.println("getMaxVelocity");
        Segment instance = new Segment(s);
        double expResult = 8.0;
        double result = instance.getMaxVelocity();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setMaxVelocity method, of class Segment.
     */
    @Test
    public void testSetMaxVelocity() {
        System.out.println("setMaxVelocity");
        double maxVelocity = 1.0;
        Segment instance = new Segment();
        instance.setMaxVelocity(maxVelocity);
        double expResult = 1.0;
        double result = instance.getMaxVelocity();
        assertEquals(expResult, result, 0.0);
    }
    
    
    /**
     * Test of getListOfVehicles method, of class Segment.
     */
    @Test
    public void testGetListOfVehicles() {
        System.out.println("getListOfVehicles");
        Vehicle v1 = new Vehicle();
        Segment instance = new Segment(s);
        instance.addVehicleToListOfV(v1);
        List<Vehicle> expResult = new ArrayList<>();
        expResult.add(v1);
        List<Vehicle> result = instance.getListOfVehicles();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListOfVehicles method, of class Segment.
     */
    @Test
    public void testSetListOfVehicles() {
        System.out.println("setListOfVehicles");
        Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle();
        Segment instance = new Segment(s);
        instance.addVehicleToListOfV(v1);
        List<Vehicle> lista2 = new ArrayList<>();
        lista2.add(v2);
        instance.setListOfVehicles(lista2);
        
        List<Vehicle> result = instance.getListOfVehicles();
        List<Vehicle> expResult = lista2;
        assertEquals(expResult, result);
    }

    /**
     * Test of toStringComplete method, of class Segment.
     */
    @Test
    public void testToStringComplete() {
        System.out.println("toStringComplete");
        Segment instance = new Segment(s);
        String expResult = "Segment 1 has initial heigh 2.0, slope 3.0, length 4.0, rolling resistance coefficient 5.0, maximum number of vehicles 6, minimum velocity of 7.0, maximum velocity of 8.0.";
        String result = instance.toStringComplete();
        assertEquals(expResult, result);
    }

    /**
     * Test of addVehicleToListOfV method, of class Segment.
     */
    @Test
    public void testAddVehicleToListOfV() {
        System.out.println("addVehicleToListOfV");
        Vehicle v = new Vehicle();
        Segment instance = new Segment(s);
        boolean expResult = true;
        boolean result = instance.addVehicleToListOfV(v);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeVehicleToListOfV method, of class Segment.
     */
    @Test
    public void testRemoveVehicleToListOfV() {
        System.out.println("removeVehicleToListOfV");
        Vehicle v = new Vehicle();
        Segment instance = new Segment();
        instance.addVehicleToListOfV(v);
        boolean expResult = true;
        boolean result = instance.removeVehicleToListOfV(v);
        assertEquals(expResult, result);
    }
}
