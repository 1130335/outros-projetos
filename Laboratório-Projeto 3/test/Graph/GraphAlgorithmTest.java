/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import static Graph.GraphAlgorithm.calculateEnergy;
import static Graph.GraphAlgorithm.calculateGravitationalForce;
import static Graph.GraphAlgorithm.calculateRelativeVelo;
import static Graph.GraphAlgorithm.calculateVelocVehicle_inMperSecoWithWind;
import Model.Vehicles.Energy;
import Model.Node;
import Model.Section;
import Model.Segment;
import Model.Vehicles.Diesel;
import Model.Vehicles.Electric;
import Model.Vehicles.Gasoline;
import Model.Vehicles.Gear;
import Model.Vehicles.Motorization;
import Model.Vehicles.Regime;
import Model.Vehicles.Throttle;
import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Helder
 */
public class GraphAlgorithmTest {
        Graph<Node, Section> roadmap = new Graph<>(true);
        
        Node n1 = new Node("n1");
        Node n2 = new Node("n2");
        Node n3 = new Node("n3");
        Node n4 = new Node("n4");
        Node n5 = new Node("n5");

        Section sec1 = new Section();
        Section sec2 = new Section();
        Section sec3 = new Section();
        Section sec4 = new Section();
        Section sec5 = new Section();
        Section sec6 = new Section();
        
        Segment seg1 = new Segment();
        Segment seg2 = new Segment();
        Segment seg3 = new Segment();
        Segment seg4 = new Segment();
        Segment seg5 = new Segment();
        Segment seg6 = new Segment();
                
    public GraphAlgorithmTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calculateFastestPath method, of class GraphAlgorithm.
     */
    @Test
    public void testCalculateFastestPath() {
        System.out.println("calculateFastestPath");

        Graph<Node, Section> roadmap1 = new Graph<>(true);

        Node n1 = new Node();
        Node n2 = new Node();
        Node n3 = new Node();
        Node n4 = new Node();

        roadmap1.insertVertex(n1);
        roadmap1.insertVertex(n2);
        roadmap1.insertVertex(n3);
        roadmap1.insertVertex(n4);

        Section sec1 = new Section();
        Section sec2 = new Section();
        Section sec3 = new Section();
        Section sec4 = new Section();

        Segment seg1 = new Segment();
        Segment seg2 = new Segment();
        Segment seg3 = new Segment();
        Segment seg4 = new Segment();

        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg2.setLength(1);
        seg2.setMaxVelocity(40);

        seg3.setLength(20);
        seg3.setMaxVelocity(40);
        seg4.setLength(1);
        seg4.setMaxVelocity(40);

        sec1.addSegment(seg1);
        sec2.addSegment(seg2);
        sec3.addSegment(seg3);
        sec4.addSegment(seg4);

        Vertex<Node, Section> v1 = roadmap1.insertVertex(n1);
        Vertex<Node, Section> v2 = roadmap1.insertVertex(n2);
        Vertex<Node, Section> v3 = roadmap1.insertVertex(n3);
        Vertex<Node, Section> v4 = roadmap1.insertVertex(n4);

        System.out.println(roadmap1.numVertices());

        roadmap1.insertEdge(n1, n2, sec1, 0);
        roadmap1.insertEdge(n2, n3, sec2, 0);
        roadmap1.insertEdge(n1, n4, sec3, 0);
        roadmap1.insertEdge(n4, n3, sec4, 0);

        System.out.println(roadmap1.numEdges());

        if (roadmap1.getEdge(v1, v2) != null) {
            System.out.println("Existe edge");
            System.out.println(roadmap1.getEdge(v1, v2));
        }

        Edge edge = roadmap1.getEdge(roadmap1.getVertex(n1), roadmap1.getVertex(n2));
        System.out.println("Edge: " + edge.getElement().toString());
        Section sec_x = (Section) edge.getElement();

        System.out.println("N.º de segmentos de Edge:" + sec_x.getSequenceOfSegments().size());
        System.out.println("Segmento: " + sec_x.getSegmentByIndex(0).toStringComplete());

        double tempoMin = GraphAlgorithm.calculateFastestPath(roadmap1, n1, n3);
        System.out.println("Caminho mais rápido: " + tempoMin);
        
        double expResult = 990.0;
        double result = GraphAlgorithm.calculateFastestPath(roadmap1, n1, n3);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calculateTime method, of class GraphAlgorithm.
     */
    @Test
    public void testCalculateTime() {
        System.out.println("calculateTime");
        double v = 40.0;
        double d = 20.0;
        double expResult = 1800.0;
        double result = GraphAlgorithm.calculateTime(v, d);
        System.out.println("Calculate time "+result);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of convertRPM_to_RadPerSec method, of class GraphAlgorithm.
     */
    @Test
    public void testConvertRPM_to_RadPerSec() {
        System.out.println("convertRPM_to_RadPerSec");
        double value_InRPM = 1.0;
        double expResult = (2 * Math.PI) / 60;  //1 RPM = 2π rad.min−1 = 2π/60 rad.s-1
        double result = GraphAlgorithm.convertRPM_to_RadPerSec(value_InRPM);
        System.out.println("Convert RPM "+result);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of convertMPerSec_to_KmPerH method, of class GraphAlgorithm.
     */
    @Test
    public void testConvertMPerSec_to_KmPerH() {
        System.out.println("convertMPerSec_to_KmPerH");
        double value_InMPerSec = 1.0;
        double expResult = 3.6;
        double result = GraphAlgorithm.convertMPerSec_to_KmPerH(value_InMPerSec);
        System.out.println("Convert to Km/h "+result);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calcMaxLinearVelocOfVehicle_inMperSec method, of class
     * GraphAlgorithm.
     */
    @Test
    public void testCalcMaxLinearVelocOfVehicle_inMperSec() {
        System.out.println("calcMaxLinearVelocOfVehicle_inMperSec");
        Energy vehiEnergy = new Energy();
        vehiEnergy.setMaxRpm(5500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.25));
        gear_list.add(new Gear("g4",0.9));
        vehiEnergy.setGearList(gear_list);
        Vehicle v = new Vehicle();
        v.setWheelSize(0.5);
        v.setEnergy(vehiEnergy);

        System.out.println("radiusOfTire = v.getWheelSize() / 2: " + (v.getWheelSize() / 2));
        System.out.println("engineSpeed_InRotationsPerSec = v.getEnergy().getMaxRpm()/60: "
                + (v.getEnergy().getMaxRpm()) / 60);
        System.out.println("finalDriveRatio = v.getEnergy().getFinalDriveRatio():"
                + v.getEnergy().getFinalDriveRatio()); //(no unit)
        System.out.println("kthGearRatio = v.getEnergy().getGears().get(v.getEnergy().getGears().size() - 1): "
                + v.getEnergy().getGearList().get(v.getEnergy().getGearList().size() - 1).getRatio()); //gear da última mudança
        double expResult = 61.54;
        double result = GraphAlgorithm.calcMaxLinearVelocOfVehicle_inMperSec(v);
        System.out.println("Max Linear Velocity of car "+result);
        assertEquals(expResult, result, 0.01);
    }

    /**
     * Test of fastestPath method, of class GraphAlgorithm.
     */
    @Test
    public void testFastestPath() {
        System.out.println("fastestPath");

        Graph<Node, Section> roadmap1 = new Graph<>(true);
        
        roadmap1.insertVertex(n1);
        roadmap1.insertVertex(n2);
        roadmap1.insertVertex(n3);
        roadmap1.insertVertex(n4);

        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg2.setLength(1);
        seg2.setMaxVelocity(40);

        seg3.setLength(20);
        seg3.setMaxVelocity(40);
        seg4.setLength(1);
        seg4.setMaxVelocity(40);

        sec1.addSegment(seg1);
        sec2.addSegment(seg2);
        sec3.addSegment(seg3);
        sec4.addSegment(seg4);

        roadmap1.insertEdge(n1, n2, sec1, 0);
        roadmap1.insertEdge(n2, n3, sec2, 0);
        roadmap1.insertEdge(n1, n4, sec3, 0);
        roadmap1.insertEdge(n4, n3, sec4, 0);
        Node orig = n1;
        Node dest = n3;
        ArrayList<Node> expResult = new ArrayList<>();
        expResult.add(n1);
        expResult.add(n2);
        expResult.add(n3);
        ArrayList<Node> result = GraphAlgorithm.fastestPath(roadmap1, orig, dest);
        System.out.println("FASTEST PATH: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of completeFastestPath method, of class GraphAlgorithm.
     */
    @Test
    public void testFastestPathForVehicle() {
        System.out.println("completeFastestPath");
        
        roadmap.insertVertex(n1);
        roadmap.insertVertex(n2);
        roadmap.insertVertex(n3);
        roadmap.insertVertex(n4);
        roadmap.insertVertex(n5);
        
        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg2.setLength(1);
        seg2.setMaxVelocity(40);

        seg3.setLength(20);
        seg3.setMaxVelocity(40);
        seg4.setLength(1);
        seg4.setMaxVelocity(40);
        
        seg5.setLength(10);
        seg5.setMaxVelocity(40);
        seg6.setLength(1.1);
        seg6.setMaxVelocity(120);

        sec1.addSegment(seg1);
        sec2.addSegment(seg2);
        sec3.addSegment(seg3);
        sec4.addSegment(seg4);
        sec5.addSegment(seg5);
        sec6.addSegment(seg6);
        
        roadmap.insertEdge(n1, n2, sec1, 0);
        roadmap.insertEdge(n2, n3, sec2, 0);
        roadmap.insertEdge(n1, n4, sec3, 0);
        roadmap.insertEdge(n4, n3, sec4, 0);
        roadmap.insertEdge(n1, n5, sec5, 0);
        roadmap.insertEdge(n5, n3, sec6, 0);                
        
        Node orig = n1;
        Node dest = n3;  
        
        Vehicle tractor = new Vehicle();
        Energy vehiEnergy = new Energy();
        vehiEnergy.setMaxRpm(3500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.75));
        gear_list.add(new Gear("g4",1.586));
        vehiEnergy.setGearList(gear_list);
        tractor.setWheelSize(0.25);
        tractor.setEnergy(vehiEnergy);
        ///Another vehicle for testing
        Vehicle car = new Vehicle();
        Energy vehiEnergy2 = new Energy();
        vehiEnergy2.setMaxRpm(5500);
        vehiEnergy2.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list2 = new ArrayList();
        gear_list2.add(new Gear("g1",3.5));
        gear_list2.add(new Gear("g1",2.5));
        gear_list2.add(new Gear("g3",1.75));
        gear_list2.add(new Gear("g4",1.586));
        vehiEnergy2.setGearList(gear_list2);
        car.setWheelSize(0.5);
        car.setEnergy(vehiEnergy2);        
        
        ArrayList<Node> expResult = new ArrayList<>();
        expResult.add(n1);
        expResult.add(n5);
        expResult.add(n3);
        ArrayList<Node> result = GraphAlgorithm.fastestPathForVehicle(roadmap, orig, dest, car);
        System.out.println("FASTEST PATH with vehicle: " + result);
        assertEquals(expResult, result);
        
        ArrayList<Node> expResult2 = new ArrayList<>();
        expResult2.add(n1);
        expResult2.add(n2);
        expResult2.add(n3);
        ArrayList<Node> result2 = GraphAlgorithm.fastestPathForVehicle(roadmap, orig, dest, tractor);
        System.out.println("FASTEST PATH with vehicle: " + result2);
        assertEquals(expResult2, result2);
    }

    
    @Test
    public void testTheoreticalMostEnergyEfficientPath1(){
        System.out.println("EfficientPath");
        
        roadmap.insertVertex(n1);
        roadmap.insertVertex(n2);
        roadmap.insertVertex(n3);
        roadmap.insertVertex(n4);
        roadmap.insertVertex(n5);
        
        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg1.setRollResistCoef(23);
        seg1.setSlope(2);
        
        
        seg2.setLength(1);
        seg2.setMaxVelocity(40);
        seg2.setRollResistCoef(25);
        seg2.setSlope(3);

        seg3.setLength(20);
        seg3.setMaxVelocity(40); 
        seg3.setRollResistCoef(27);
        seg3.setSlope(4);
        
        seg4.setLength(1);
        seg4.setMaxVelocity(40);
        seg4.setRollResistCoef(29);
        seg4.setSlope(6);
        
        seg5.setLength(10);
        seg5.setMaxVelocity(40);
        seg5.setRollResistCoef(12);
        seg5.setSlope(9);
        
        seg6.setLength(1.1);
        seg6.setMaxVelocity(120);
        seg6.setRollResistCoef(27);
        seg6.setSlope(2);

        sec1.setWindDirection(30);
        sec1.setWindSpeed(40);
        sec1.addSegment(seg1);
        
        sec2.setWindDirection(32);
        sec2.setWindSpeed(20);
        sec2.addSegment(seg2);
        
        sec3.setWindDirection(50);
        sec3.setWindSpeed(80);
        sec3.addSegment(seg3);
        
        sec1.setWindDirection(10);
        sec1.setWindSpeed(60);
        sec4.addSegment(seg4);
        
        sec5.setWindDirection(20);
        sec5.setWindSpeed(90);
        sec5.addSegment(seg5);
        
        sec6.setWindDirection(30);
        sec6.setWindSpeed(40);
        sec6.addSegment(seg6);
        
        roadmap.insertEdge(n1, n2, sec1, 0);
        roadmap.insertEdge(n2, n3, sec2, 0);
        roadmap.insertEdge(n1, n4, sec3, 0);
        roadmap.insertEdge(n4, n3, sec4, 0);
        roadmap.insertEdge(n1, n5, sec5, 0);
        roadmap.insertEdge(n5, n3, sec6, 0);                
        
        Node orig = n1;
        Node dest = n3;  
        
        Vehicle tractor = new Vehicle();
        Energy vehiEnergy = new Energy();
        vehiEnergy.setMaxRpm(3500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.75));
        gear_list.add(new Gear("g4",1.586));
        vehiEnergy.setGearList(gear_list);
        tractor.setWheelSize(0.25);
        tractor.setEnergy(vehiEnergy);
        tractor.setMass(43);
        tractor.setDragCoefficient(10);
        tractor.setFrontalArea(3);
        tractor.setWheelSize(1);
        tractor.setEnergy(vehiEnergy);
        tractor.setRrc(10);
        
        
        ///Another vehicle for testing
        Vehicle car = new Vehicle();
        Energy vehiEnergy2 = new Energy();
        vehiEnergy2.setMaxRpm(5500);
        vehiEnergy2.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list2 = new ArrayList();
        gear_list2.add(new Gear("g1",3.5));
        gear_list2.add(new Gear("g1",2.5));
        gear_list2.add(new Gear("g3",1.75));
        gear_list2.add(new Gear("g4",1.586));
        vehiEnergy2.setGearList(gear_list2);
        car.setMass(13);
        car.setDragCoefficient(23);
        car.setFrontalArea(2);
        car.setWheelSize(0.5);
        car.setEnergy(vehiEnergy2);
        car.setRrc(12);
        
       double expResult=1117021;
        double res1 = GraphAlgorithm.theoreticalMostEnergyEfficientPath2(roadmap, orig, dest, car);
        System.out.println("Energy of most EFFICIENT PATH for the car: " + res1);
        assertEquals(expResult, res1,0.9);
//        
//        double expResult2 = 917531;
//        
//        double res2 = GraphAlgorithm.theoreticalMostEnergyEfficientPath2(roadmap, orig, dest, tractor);
//        System.out.println("EFFICIENT  PATH for the tractor: " + res2);
//        assertEquals(expResult2, res2,0.9);
    }
    
   
     @Test
    public void testTheoreticalMostEnergyEfficientPath3(){
        System.out.println("EfficientPath");
        
        roadmap.insertVertex(n1);
        roadmap.insertVertex(n2);
        roadmap.insertVertex(n3);
        roadmap.insertVertex(n4);
        roadmap.insertVertex(n5);
        
        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg1.setRollResistCoef(23);
        seg1.setSlope(2);
        
        
        seg2.setLength(1);
        seg2.setMaxVelocity(40);
        seg2.setRollResistCoef(25);
        seg2.setSlope(3);

        seg3.setLength(20);
        seg3.setMaxVelocity(40); 
        seg3.setRollResistCoef(27);
        seg3.setSlope(4);
        
        seg4.setLength(1);
        seg4.setMaxVelocity(40);
        seg4.setRollResistCoef(29);
        seg4.setSlope(6);
        
        seg5.setLength(10);
        seg5.setMaxVelocity(40);
        seg5.setRollResistCoef(12);
        seg5.setSlope(9);
        
        seg6.setLength(1.1);
        seg6.setMaxVelocity(120);
        seg6.setRollResistCoef(27);
        seg6.setSlope(2);

        sec1.setWindDirection(30);
        sec1.setWindSpeed(40);
        sec1.addSegment(seg1);
        
        sec2.setWindDirection(32);
        sec2.setWindSpeed(20);
        sec2.addSegment(seg2);
        
        sec3.setWindDirection(50);
        sec3.setWindSpeed(80);
        sec3.addSegment(seg3);
        
        sec1.setWindDirection(10);
        sec1.setWindSpeed(60);
        sec4.addSegment(seg4);
        
        sec5.setWindDirection(20);
        sec5.setWindSpeed(90);
        sec5.addSegment(seg5);
        
        sec6.setWindDirection(30);
        sec6.setWindSpeed(40);
        sec6.addSegment(seg6);
        
        roadmap.insertEdge(n1, n2, sec1, 0);
        roadmap.insertEdge(n2, n3, sec2, 0);
        roadmap.insertEdge(n1, n4, sec3, 0);
        roadmap.insertEdge(n4, n3, sec4, 0);
        roadmap.insertEdge(n1, n5, sec5, 0);
        roadmap.insertEdge(n5, n3, sec6, 0);                
        
        Node orig = n1;
        Node dest = n3;  
        
        Vehicle tractor = new Vehicle();
        Energy vehiEnergy = new Energy();
        vehiEnergy.setMaxRpm(3500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.75));
        gear_list.add(new Gear("g4",1.586));
        vehiEnergy.setGearList(gear_list);
        tractor.setWheelSize(0.25);
        tractor.setEnergy(vehiEnergy);
        tractor.setMass(43);
        tractor.setDragCoefficient(10);
        tractor.setFrontalArea(3);
        tractor.setWheelSize(1);
        tractor.setEnergy(vehiEnergy);
        tractor.setRrc(10);
        
        
        ///Another vehicle for testing
        Vehicle car = new Vehicle();
        Energy vehiEnergy2 = new Energy();
        vehiEnergy2.setMaxRpm(5500);
        vehiEnergy2.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list2 = new ArrayList();
        gear_list2.add(new Gear("g1",3.5));
        gear_list2.add(new Gear("g1",2.5));
        gear_list2.add(new Gear("g3",1.75));
        gear_list2.add(new Gear("g4",1.586));
        vehiEnergy2.setGearList(gear_list2);
        car.setMass(13);
        car.setDragCoefficient(23);
        car.setFrontalArea(2);
        car.setWheelSize(0.5);
        car.setEnergy(vehiEnergy2);
        car.setRrc(12);
        
         ArrayList<Node> expResult = new ArrayList<>();
        expResult.add(n1);
        expResult.add(n2);
        expResult.add(n3);
       //double expResult=22;
        ArrayList<Node> res1 = GraphAlgorithm.theoreticalMostEnergyEfficientPath3(roadmap, orig, dest, car);
        System.out.println("EFFICIENT PATH for the car: " + res1);
        assertEquals(expResult, res1);
//        
//        double expResult2 = 23;
//        
//        double res2 = GraphAlgorithm.theoreticalMostEnergyEfficientPath2(roadmap, orig, dest, tractor);
//        System.out.println("EFFICIENT  PATH for the tractor: " + res2);
//        assertEquals(expResult2, res2,0.01);
    }

    @Test
    public void testCalculateVelocVehicle_inMperSecoWithWind(){
        Section c=new Section();
        Segment s=new Segment();
         Energy vehiEnergy = new Energy();
         
        vehiEnergy.setMaxRpm(5500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.25));
        gear_list.add(new Gear("g4",0.9));
        vehiEnergy.setGearList(gear_list);
        Vehicle v = new Vehicle();
        v.setWheelSize(0.5);
        v.setEnergy(vehiEnergy);
        
        c.setWindDirection(40);
        c.setWindSpeed(20);
        s.setMaxVelocity(40);
        
        double windD=c.getWindDirection();
        double windS=c.getWindSpeed();
        double maxV=s.getMaxVelocity();
        double maxSpeed=GraphAlgorithm.calcMaxLinearVelocOfVehicle_inMperSec(v);
        System.out.println("Velocidade do carro:"+maxSpeed);

//        double carVe=50;
        double res=calculateVelocVehicle_inMperSecoWithWind(v,maxSpeed,windS,windD);
        System.out.println("Velociodade com vento:"+res);
        double expectedResult=76.85;
        assertEquals(expectedResult, res,0.01);
       
        
    }
    
    @Test
    public void testCalculateRelativeVelocity(){
        double Vvehicle=90;
        double Vwind=20;
        double windDirection=40;
        double expectedResult=105;
        double res=calculateRelativeVelo(Vvehicle,Vwind,windDirection);
        System.out.println("Relative Velocity "+res);
        assertEquals(expectedResult, res,0.9);
    }

    @Test
    public void testEnergy(){
        double force=15;
        double deslocation=20;
        double expectedResult=300;
        double res=calculateEnergy(force,deslocation);
        System.out.println("Energy "+res);
        assertEquals(expectedResult,res,0.01);
    }
    
   @Test
   public void testCalculateForce(){
       Segment s=new Segment();
       Vehicle v=new Vehicle();
       double airDensity=1.225;
       double gravity=9.8;
       
       s.setRollResistCoef(12);
       s.setLength(14);
       s.setSlope(4);
       v.setRrc(2);
       v.setMass(25);
       v.setDragCoefficient(12);
       v.setFrontalArea(2);
       double velocity=50;
       double rrc=s.getRollResistCoef();
       double mass=v.getMass();
       double angle=s.getSlope();
       double frontalArea=v.getFrontalArea();
       double dragCoefficient=v.getDragCoefficient();
       double expected=39699.9286438312;
       double graviForce=calculateGravitationalForce(rrc, dragCoefficient, angle, mass, frontalArea, velocity);
       System.out.println("Gravitational Force "+graviForce);
       assertEquals(expected, graviForce,0.9);
   }
   
   @Test
   public void calculateRpm(){
        Section c=new Section();
        Segment s=new Segment();
         Energy vehiEnergy = new Energy();
         
        vehiEnergy.setMaxRpm(5500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.25));
        gear_list.add(new Gear("g4",0.9));
        vehiEnergy.setGearList(gear_list);
        Vehicle v = new Vehicle();
        v.setWheelSize(0.5);
        v.setEnergy(vehiEnergy);
        
        c.setWindDirection(40);
        c.setWindSpeed(20);
        s.setMaxVelocity(40);
        
        double windD=c.getWindDirection();
        double windS=c.getWindSpeed();
        double maxV=s.getMaxVelocity();
        double gRatio=gear_list.get(0).getRatio();
        double radious=v.getWheelSize();
        
        double maxSpeed=GraphAlgorithm.calcMaxLinearVelocOfVehicle_inMperSec(v);
        double velo=GraphAlgorithm.calculateVelocVehicle_inMperSecoWithWind(v, maxSpeed, windS, windD);
        double res=GraphAlgorithm.calculateWrpm(v, maxSpeed, windS, windD, velo, radious, gRatio);
        double expectedR=5137;
        System.out.println("Calculate RPM "+res);
        assertEquals(expectedR, res,0.9);
   }
   
   @Test
   public void calRealEnergy(){
      
        ArrayList<Double> gears = new ArrayList<>();
        gears.add(3.2);
        gears.add(2.5);
        gears.add(1.2);
        gears.add(0.8);
        Vehicle car = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Gasoline(),
                100, 30, 1.3, 15, 0.01, 0.5, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
        double force=10;
        
        Vehicle car2 = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Diesel(),
                102, 32, 1.3, 14, 0.02, 0.8, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
   
        Vehicle car3 = new Vehicle("carro", "toyota xpto", Vehicle.VehicleType.CAR, new Electric(),
                105, 31, 1.5, 12, 0.07, 0.4, new ArrayList<>(), new Energy(1000, 5500, 2.6, new ArrayList<Gear>(), new ArrayList<Throttle>(), 0.0));
       double sfc=12;
       double desl=15;
//       double reBrake=12;
       double diesel=48;
       double gasoline=44.4;
       
       double res=GraphAlgorithm.calculateRealEnergy(car, force, sfc, desl);
       double expected=79920;
       System.out.println("Energy for real most efficient path "+res);
       assertEquals(expected, res,0.9);
   }
   
   @Test
    public void testRealMostEnergyEfficientPath(){
        System.out.println("EfficientPath");
        
        roadmap.insertVertex(n1);
        roadmap.insertVertex(n2);
        roadmap.insertVertex(n3);
        roadmap.insertVertex(n4);
        roadmap.insertVertex(n5);
        
        seg1.setLength(10);
        seg1.setMaxVelocity(40);
        seg1.setRollResistCoef(23);
        seg1.setSlope(2);
        
        
        seg2.setLength(1);
        seg2.setMaxVelocity(40);
        seg2.setRollResistCoef(25);
        seg2.setSlope(3);

        seg3.setLength(20);
        seg3.setMaxVelocity(40); 
        seg3.setRollResistCoef(27);
        seg3.setSlope(4);
        
        seg4.setLength(1);
        seg4.setMaxVelocity(40);
        seg4.setRollResistCoef(29);
        seg4.setSlope(6);
        
        seg5.setLength(10);
        seg5.setMaxVelocity(40);
        seg5.setRollResistCoef(12);
        seg5.setSlope(9);
        
        seg6.setLength(1.1);
        seg6.setMaxVelocity(120);
        seg6.setRollResistCoef(27);
        seg6.setSlope(2);

        sec1.setWindDirection(30);
        sec1.setWindSpeed(40);
        sec1.addSegment(seg1);
        
        sec2.setWindDirection(32);
        sec2.setWindSpeed(20);
        sec2.addSegment(seg2);
        
        sec3.setWindDirection(50);
        sec3.setWindSpeed(80);
        sec3.addSegment(seg3);
        
        sec1.setWindDirection(10);
        sec1.setWindSpeed(60);
        sec4.addSegment(seg4);
        
        sec5.setWindDirection(20);
        sec5.setWindSpeed(90);
        sec5.addSegment(seg5);
        
        sec6.setWindDirection(30);
        sec6.setWindSpeed(40);
        sec6.addSegment(seg6);
        
        roadmap.insertEdge(n1, n2, sec1, 0);
        roadmap.insertEdge(n2, n3, sec2, 0);
        roadmap.insertEdge(n1, n4, sec3, 0);
        roadmap.insertEdge(n4, n3, sec4, 0);
        roadmap.insertEdge(n1, n5, sec5, 0);
        roadmap.insertEdge(n5, n3, sec6, 0);                
        
        Node orig = n1;
        Node dest = n3;  
        
        Vehicle tractor = new Vehicle();
        Energy vehiEnergy = new Energy();
        vehiEnergy.setMaxRpm(3500);
        vehiEnergy.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.75));
        gear_list.add(new Gear("g4",1.586));
        vehiEnergy.setGearList(gear_list);
        tractor.setWheelSize(0.25);
        tractor.setEnergy(vehiEnergy);
        tractor.setMass(43);
        tractor.setDragCoefficient(10);
        tractor.setFrontalArea(3);
        tractor.setWheelSize(1);
        tractor.setEnergy(vehiEnergy);
        tractor.setRrc(10);
        
        
        ///Another vehicle for testing
        Vehicle car = new Vehicle();
        Energy vehiEnergy2 = new Energy();
        vehiEnergy2.setMaxRpm(5500);
        vehiEnergy2.setFinalDriveRatio(2.6);
        ArrayList<Gear> gear_list2 = new ArrayList();
        gear_list2.add(new Gear("g1",3.5));
        gear_list2.add(new Gear("g1",2.5));
        gear_list2.add(new Gear("g3",1.75));
        gear_list2.add(new Gear("g4",1.586));
        vehiEnergy2.setGearList(gear_list2);
        car.setMass(13);
        car.setDragCoefficient(23);
        car.setFrontalArea(2);
        car.setWheelSize(0.5);
        car.setEnergy(vehiEnergy2);
        car.setRrc(12);
        
         ArrayList<Node> expResult = new ArrayList<>();
        expResult.add(n1);
        expResult.add(n4);
        expResult.add(n3);
       //double expResult=22;
        ArrayList<Node> res1 = GraphAlgorithm.theoreticalMostEnergyEfficientPathN05(roadmap, orig, dest, car);
        System.out.println("REAL EFFICIENT PATH for the car: " + res1);
        assertEquals(expResult, res1);
//        
//        double expResult2 = 23;
//        
//        double res2 = GraphAlgorithm.theoreticalMostEnergyEfficientPath2(roadmap, orig, dest, tractor);
//        System.out.println("EFFICIENT  PATH for the tractor: " + res2);
//        assertEquals(expResult2, res2,0.01);
    }
    
    @Test
    public void testCalcualteMotorization(){
        Vehicle v1=new Vehicle();
        Energy n1=new Energy();
        Regime r1=new Regime();
        
        n1.setFinalDriveRatio(15);
        r1.setTorque(12);
        v1.setEnergy(n1);
        v1.setWheelSize(12);
        
        ArrayList<Gear> gear_list = new ArrayList();
        gear_list.add(new Gear("g1",3.5));
        gear_list.add(new Gear("g2",2.5));
        gear_list.add(new Gear("g3",1.25));
        gear_list.add(new Gear("g4",0.9));
        n1.setGearList(gear_list);
        
        double torque=r1.getTorque();
        double gearRation=gear_list.get(0).getRatio();
        double wSize=v1.getWheelSize()/2;
        double fDriveRatio=n1.getFinalDriveRatio();
        double res=GraphAlgorithm.calculateMotorização(torque, wSize, fDriveRatio, gearRation);
        double expected=105;
        System.out.println("Motorization "+res);
        assertEquals(expected, res,0.9);
    }
    
//    @Test
//    public void testCalculateTorque(){
//        Throttle t1=new Throttle();
//        ArrayList<Regime> regime = new ArrayList();
//        regime.add(new Regime(1200,2500,14,4));
//        regime.add(new Regime(2600,3000,15,2));
//        regime.add(new Regime(3100,4400,16,1));
//        t1.setRegimeList(regime);
//        
//        double rpm=2300;
//        double res=GraphAlgorithm.calculateTorque(t1, rpm);
//        double expected=0;
//        System.out.println("Torque "+res);
//        assertEquals(expected, res,0.9);
//    }

}


