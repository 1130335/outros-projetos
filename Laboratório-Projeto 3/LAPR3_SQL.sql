/* Elimina��o das tabelas */
drop table typology cascade constraints;
drop table direction cascade constraints;
drop table roadNetwork cascade constraints;
drop table node cascade constraints;
drop table section cascade constraints;
drop table segment cascade constraints;
drop table roadsIn cascade constraints;
drop table roadsOut cascade constraints;
drop table project cascade constraints;
drop table vehicle cascade constraints;
drop table electricMotorization cascade constraints;
drop table hybridMotorization cascade constraints;
drop table combustionMotorization cascade constraints;
drop table gear cascade constraints;
drop table energy cascade constraints;
drop table regime cascade constraints;
drop table throttle cascade constraints;
drop table velocityLimit cascade constraints;
drop table vehicleType cascade constraints;
drop table electricVehicle cascade constraints;
drop table hybridVehicle cascade constraints;
drop table combustionVehicle cascade constraints;
drop table projectVehicle cascade constraints;
drop table vehicleSegment cascade constraints;
drop table simulation cascade constraints;

/* Cria��o das tabelas */
create table typology(
  typology_id integer not null,
  description varchar(50) not null,
  constraint typology_pk primary key (typology_id)
);

create table direction(
  direction_id integer not null,
  description varchar(50) not null,
  constraint direction_pk primary key (direction_id)
);

create table vehicleType(
  type_id integer not null,
  description varchar(50) not null,
  constraint vehicleType_pk primary key (type_id)
);

create table combustionMotorization(
  combustion_id integer not null,
  description varchar(50) not null,
  constraint combustion_pk primary key (combustion_id)
);

create table electricMotorization(
  electric_id integer not null,
  description varchar(50) not null,
  constraint electric_pk primary key (electric_id)
);

create table hybridMotorization(
  hybrid_id integer not null,
  electric_id integer not null,
  constraint fk_hybrid_eletric foreign key(electric_id) references electricMotorization(electric_id),
  combustion_id integer not null,
  constraint fk_hybrid_combustion foreign key(combustion_id) references combustionMotorization(combustion_id),
  constraint hybrid_pk primary key (hybrid_id)
);

create table energy(
  energy_id integer not null,
  constraint energy_pk primary key (energy_id),
  minRpm decimal not null,
  maxRpm decimal not null,
  finalDriveRatio decimal not null,
  energyRegenerationRatio decimal not null
);

/* Corresponde ao grafo */
create table roadNetwork(
  roadNetwork_id integer not null,
  constraint roadNetworkPK primary key(roadNetwork_id),
  roadNetworkName varchar(100) not null,
  roadNetworkDescription varchar(250) not null
);

create table project(
  project_id integer not null,
  constraint projectPK primary key(project_id),
  roadNetwork integer not null,
  constraint roadNetworkProjectFK foreign key(roadNetwork) references roadNetwork(roadNetwork_id),
  projectName varchar(100) not null,
  projectDescription varchar(250) not null
);

create table simulation(
  simulation_id integer not null,
  constraint simulationPK primary key(simulation_id),
  simName varchar(100) not null,
  simDescription varchar(250) not null,
  project integer not null,
  constraint SimProjFK foreign key(project) references project(project_id)
);

create table node(
  node_id integer not null,
  node_name varchar(10) not null,
  constraint node_pk primary key (node_id),
  roadNetwork integer not null,
  constraint roadNetworkNodeFK foreign key(roadNetwork) references roadNetwork(roadNetwork_id)
);

create table section(
  section_id integer not null,
  constraint section_pk primary key (section_id),
  beginningNode integer not null,
  constraint fk_beginningNode foreign key(beginningNode) references node(node_id),
  endingNode integer not null,
  constraint fk_endingNode foreign key(endingNode) references node(node_id),
  typology integer not null,
  constraint fk_typology foreign key(typology) references typology(typology_id),
  direction integer not null,
  constraint fk_direction foreign key(direction) references direction(direction_id),
  toll integer not null,
  windDirection decimal not null,
  windSpeed decimal not null,
  roadNetwork integer not null,
  constraint roadNetworkSectionFK foreign key(roadNetwork) references roadNetwork(roadNetwork_id),
  road varchar(5) not null
);

create table segment(
  segment_id integer not null,
  segment_index integer not null,
  inicialHeight decimal not null,
  slope decimal not null,
  segment_length decimal not null,
  rollResistCoef decimal not null,
  maxNumOfVehicles integer not null,
  minVelocity decimal not null,
  maxVelocity decimal not null,
  constraint segment_pk primary key (segment_id),
  belongingSection integer not null,
  constraint segmentSection_fk foreign key(belongingSection) references section(section_id)
);

create table vehicle(
  vehicle_id integer not null,
  constraint vehicle_pk primary key (vehicle_id),
  vehicleType integer not null,
  constraint fk_vehicleType foreign key (vehicleType) references vehicleType(type_id),
  mass decimal not null,
  load decimal not null,
  dragCoefficient decimal not null,
  name varchar(50) not null,
  description varchar(150) not null,
  rrc decimal not null,
  wheelSize decimal not null,
  energy integer not null,
  constraint fk_energy_vehicle foreign key (energy) references energy(energy_id),
  frontalArea decimal not null
);

create table vehicleSegment(
  vehicle integer not null,
  constraint vehicleSegmentFK1 foreign key (vehicle) references vehicle(vehicle_id),
  segment integer not null,
  constraint vehicleSegmentFK2 foreign key(segment) references segment(segment_id),
  constraint vehicleSegmentPK primary key(vehicle,segment)
);

create table gear(
  gear_id integer not null,
  constraint gear_pk primary key (gear_id),
  ratio decimal not null,
  energy integer not null,
  constraint fk_energy_gear foreign key(energy) references energy(energy_id),
  vehicle integer not null,
  constraint fk_vehicle_gear foreign key(vehicle) references vehicle(vehicle_id)
);

create table projectVehicle(
  vehicle integer not null,
  constraint projectVehicleFK1 foreign key (vehicle) references vehicle(vehicle_id),
  project integer not null,
  constraint projectVehicleFK2 foreign key (project) references project(project_id),
  constraint projectVehiclePK primary key (vehicle,project)
);



create table throttle(
  throttle_id integer not null,
  constraint throttle_pk primary key (throttle_id),
  energy integer not null,
  constraint fk_energy_throttle foreign key(energy) references energy(energy_id)
);

create table regime(
  regime_id integer not null,
  constraint regime_pk primary key (regime_id),
  torque decimal not null,
  rpmLow decimal not null,
  rpmHigh decimal not null,
  sfc decimal not null,
  throttle integer not null,
  constraint fk_throttle_regime foreign key(throttle) references throttle(throttle_id)
);

create table velocityLimit(
  velocityLimit_id integer not null,
  constraint velocityLimit_pk primary key (velocityLimit_id),
  section_type integer not null,
  constraint fk_section_type foreign key(section_type) references typology(typology_id),
  speedLimit decimal not null,
  vehicle integer not null,
  constraint fk_vehicle_velocity foreign key (vehicle) references vehicle(vehicle_id)
);

create table roadsIn(
  node integer not null,
  constraint nodeRoadsInFK foreign key (node) references node(node_id),
  section integer not null,
  constraint sectionRoadsInFK foreign key (section) references section(section_id),
  constraint roadsInPK primary key(node, section)
);

create table roadsOut(
  node integer not null,
  constraint nodeRoadsOutFK foreign key (node) references node(node_id),
  section integer not null,
  constraint sectionRoadsOutFK foreign key (section) references section(section_id),
  constraint roadsOutPK primary key(node, section)
);

create table electricVehicle(
  vehicle integer not null,
  constraint electricVehicleFK1 foreign key (vehicle) references vehicle(vehicle_id),
  electric integer not null,
  constraint electricVehicleFK2 foreign key (electric) references electricMotorization(electric_id),
  constraint electricVehiclePK primary key(vehicle, electric)
);

create table combustionVehicle(
  vehicle integer not null,
  constraint combustionVehicleFK1 foreign key (vehicle) references vehicle(vehicle_id),
  combustion integer not null,
  constraint combustionVehicleFK2 foreign key (combustion) references combustionMotorization(combustion_id),
  constraint combustionVehiclePK primary key(vehicle, combustion)
);

create table hybridVehicle(
  vehicle integer not null,
  constraint hybridVehicleFK1 foreign key (vehicle) references vehicle(vehicle_id),
  hybrid integer not null,
  constraint hybridVehicleFK2 foreign key (hybrid) references hybridMotorization(hybrid_id),
  constraint hybridVehiclePK primary key(vehicle, hybrid)
);

/* Preenchimento autom�tico das tabelas do tipo enumerado */
/* Typology_Section */
insert into typology(typology_id, description) values (1, 'REGULAR_ROAD');
insert into typology(typology_id, description) values (2, 'URBAN_ROAD');
insert into typology(typology_id, description) values (3, 'EXPRESS_ROAD');
insert into typology(typology_id, description) values (4, 'CONTROLLED_ACCESS_HIGHWAY');

/* Direction_Section */
insert into direction(direction_id, description) values (1, 'DIRECT');
insert into direction(direction_id, description) values (2, 'REVERSE');
insert into direction(direction_id, description) values (3, 'BIDIRECTIONAL');

/* Tipos de Ve�culos */
insert into vehicleType(type_id, description) values (1, 'TRUCK');
insert into vehicleType(type_id, description) values (2, 'TRACTOR');
insert into vehicleType(type_id, description) values (3, 'SEMITRAILER');
insert into vehicleType(type_id, description) values (4, 'CAR');
insert into vehicleType(type_id, description) values (5, 'MOTORCYCLE');

/* Tipos de Combust�o */
insert into combustionMotorization(combustion_id, description) values (1, 'Diesel');
insert into combustionMotorization(combustion_id, description) values (2, 'Gasoline');
insert into combustionMotorization(combustion_id, description) values (3, 'Hydrogen');

/* Tipo Eletrico */
insert into electricMotorization(electric_id, description) values (1, 'Electric');

/* Procedures para altera��es da base de dados */

create or replace procedure createNewRoadNetwork
(RNName in varchar, description in varchar) as
  RNId integer;
begin
  select count(*) 
    into RNId 
    from roadNetwork;
  insert into roadNetwork(roadNetwork_id, roadNetworkName, roadNetworkDescription) values (RNId + 1, RNName, description);
end createNewRoadNetwork;
/
create or replace procedure createNewProject
(roadNetworkId in integer, pName in varchar, pDescription in varchar) as
  Pid integer;
  
begin
  select count(*)
    into Pid
    from project;
    
  insert into project(project_id, roadNetwork, projectName, projectDescription) values (Pid + 1, roadNetworkId, pName, pDescription);
end createNewProject;
/

create or replace procedure createNewNode
(nName in varchar, rn in integer) as
  nId integer;
  
begin
  select count(*)
    into nId
    from node;
       
  insert into node(node_id, node_name, roadNetwork) values (nId + 1, nName, rn);
end createNewNode;
/

create or replace procedure createNewSection
(bNode in integer, eNode in integer, sType in integer, sDirection in integer, toll in integer, windD in decimal, windS in decimal, rnId in integer, road in varchar) as
  sectionId integer;

begin
  select count(*)
    into sectionId
    from section;
  
  insert into section(section_id, beginningNode, endingNode, typology, direction, toll, windDirection, windSpeed, roadNetwork, road) values (sectionId + 1, bNode, eNode, sType, sDirection, toll, windD, windS, rnId, road);
  insert into roadsIn(node,section) values (bNode, sectionId + 1);
  insert into roadsOut(node,section) values (eNode, sectionId + 1);
end createNewSection;
/

create or replace procedure createNewSegment
(sIndex in integer, iHeight in decimal, slope in decimal, sLength in decimal, rrc in decimal, maxVehicles in integer, minVelocity in decimal, maxVelocity in decimal, sectionId in integer) as
  segmentId integer;
  
begin
  select count(*)
    into segmentId
    from segment;
  insert into segment(segment_id,segment_index,inicialHeight,slope,segment_length,rollResistCoef,maxNumOfVehicles,minVelocity,maxVelocity,belongingSection) values (segmentId + 1,sIndex,iHeight,slope,sLength,rrc,maxVehicles,minVelocity,maxVelocity,sectionId);
end createNewSegment;
/

create or replace procedure createNewEnergy
(minRPM in decimal, maxRPM in decimal, fDR in decimal, enrr in decimal) as
  eId integer;
  
begin
  select count(*)
    into eId
    from energy;
  insert into energy(energy_id, minRpm, maxRpm, finalDriveRatio, energyRegenerationRatio) values (eId + 1, minRPM, maxRPM, fDR, enrr);
end createNewEnergy;
/

create or replace procedure createNewCombustionVehicle
(vType in integer, vMass in decimal, vLoad in decimal, dc in decimal, vName in varchar, vDescription in varchar, vRRC in decimal, vWS in decimal, eId in integer, area in decimal, cId in integer, pId in integer) as
  vId integer;
  
begin
  select count(*)
    into vId
    from vehicle;
  insert into vehicle(vehicle_id,vehicleType,mass,load,dragCoefficient,name,description,rrc,wheelSize,energy,frontalArea) values (vId + 1,vType,vMass,vLoad,dc,vName,vDescription,vRRC,vWS,eId,area);
  insert into combustionVehicle(vehicle, combustion) values (vId + 1, cId);
  insert into projectVehicle(vehicle,project) values (vId + 1, pId);
end createNewCombustionVehicle;
/

create or replace procedure createNewElectricVehicle
(vType in integer, vMass in decimal, vLoad in decimal, dc in decimal, vName in varchar, vDescription in varchar, vRRC in decimal, vWS in decimal, eId in integer, area in decimal, pId in integer) as
  vId integer;
  
begin
  select count(*)
    into vId
    from vehicle;
  insert into vehicle(vehicle_id,vehicleType,mass,load,dragCoefficient,name,description,rrc,wheelSize,energy,frontalArea) values (vId + 1,vType,vMass,vLoad,dc,vName,vDescription,vRRC,vWS,eId,area);
  insert into electricVehicle(vehicle, electric) values (vId + 1,1);
  insert into projectVehicle(vehicle,project) values (vId + 1, pId);
end createNewElectricVehicle;
/

create or replace procedure createNewHybridVehicle
(vType in integer, vMass in decimal, vLoad in decimal, dc in decimal, vName in varchar, vDescription in varchar, vRRC in decimal, vWS in decimal, eId in integer, area in decimal, hId in integer, pId in integer) as
  vId integer;
  
begin
  select count(*)
    into vId
    from vehicle;
  insert into vehicle(vehicle_id,vehicleType,mass,load,dragCoefficient,name,description,rrc,wheelSize,energy,frontalArea) values (vId + 1,vType,vMass,vLoad,dc,vName,vDescription,vRRC,vWS,eId,area);
  insert into hybridVehicle(vehicle, hybrid) values (vId + 1, hId);
  insert into projectVehicle(vehicle,project) values (vId + 1, pId);
end createNewHybridVehicle;
/

create or replace procedure createNewGear
(gRatio in decimal, eId in integer, vID in integer) as
  gId integer;
  
begin
  select count(*)
    into gId
    from gear;
  insert into gear(gear_id, ratio, energy, vehicle) values (gId + 1, gRatio, eId, vID);
end createNewGear;
/

create or replace procedure createNewThrottle
(eId in integer) as
  tId integer;
  
begin
  select count(*)
    into tId
    from throttle;
  insert into throttle(throttle_id, energy) values (tId + 1, eId);
end createNewThrottle;
/

create or replace procedure createNewVelocityLimit
(vLSectionType in integer, vLSpeedLimit in decimal, vLVehicle in integer) as
  vLId integer;
  
begin
  select count(*)
    into vLId
    from velocityLimit;
  insert into velocityLimit(velocityLimit_id, section_type, speedLimit, vehicle) values (vLId + 1, vLSectionType, vLSpeedLimit, vLVehicle);
end createNewVelocityLimit;
/

create or replace procedure createNewRegime
(rTorque in decimal, rRPMLow in decimal, rRPMHigh in decimal, rSFC in decimal, rThrottle in integer) as
  rId integer;
  
begin
  select count(*)
    into rId
    from regime;
  insert into regime(regime_id, torque, rpmLow, rpmHigh, sfc, throttle) values (rId + 1, rTorque, rRPMLow, rRPMHigh, rSFC, rThrottle);
end createNewRegime;
/

create or replace function getNodeByID
(nName in varchar, rn in integer)
return integer is
  nId integer := 0;

begin
  select n1.node_id 
    into nId
    from node n1
    where n1.node_name = nName and n1.roadNetwork = rn;
  
  return nId;
end;
/

create or replace function getTypologyByID
(tName in varchar)
return integer is
  tId integer := 0;

begin
  select t1.TYPOLOGY_ID 
    into tId
    from typology t1
    where t1.DESCRIPTION = tName;
  
  return tId;
end;
/

create or replace function getDirectionByID
(dName in varchar)
return integer is
  dId integer := 0;

begin
  select d1.DIRECTION_ID 
    into dId
    from direction d1
    where d1.DESCRIPTION = dName;
  
  return dId;
end;
/

create or replace function getSectionByID
(rn in integer)
return integer is
  secId integer := 0;

begin
  select count(*)
    into secId
    from section s1
    where s1.roadNetwork = rn;
    
  return secId;
end;
/

create or replace function getRoadNetworkByID
(rnName in varchar)
return integer is
  rnId integer := 0;

begin
  select rn1.ROADNETWORK_ID
    into rnId
    from roadNetwork rn1
    where rn1.ROADNETWORKNAME = rnName;
  
  return rnId;
end;
/

create or replace function allProjects
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from project;
  return my_cursor;
end;
/
create or replace function getRoadNetwork
(rnID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from roadNetwork r1 where r1.roadNetwork_id = rnID;
  return my_cursor;
end;
/
create or replace function getSections
(rnID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from section s1 where s1.roadNetwork = rnID;
  return my_cursor;
end;
/
create or replace function getNodes
(rnID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from node n1 where n1.roadNetwork = rnID;
  return my_cursor;
end;
/
create or replace function getTypology
(tID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from typology t1 where t1.typology_id = tID;
  return my_cursor;
end;
/
create or replace function getDirection
(dID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from direction d1 where d1.direction_id = dID;
  return my_cursor;
end;
/

create or replace function getNodeName
(nID in integer)
return varchar is
  nodeName varchar(10);

begin
  select n1.node_name
    into nodeName
    from Node n1
    where n1.NODE_ID = nID;
  
  return nodeName;
end;
/

create or replace function getSegments
(secID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from segment s1 where s1.belongingSection = secID;
  return my_cursor;
end;
/

create or replace function getProjectByName
(Pname in varchar)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from project p1 where p1.PROJECTNAME = Pname;
  return my_cursor;
end;
/

create or replace function getProjectID
(pName in varchar)
return integer is

pID integer := 0;

begin
  select p1.PROJECT_ID
  into pID
  from project p1
  where p1.PROJECTNAME = pName;
  
  return pID;
end;
/

create or replace function getEnergyID
return integer is
eID integer := 0;

begin
  select count(*)
  into eID
  from energy;
  
  return eID;
end;
/

create or replace function getCombustionID
(cName in varchar) 
return integer is
cID integer := 0;

begin
  select cm.COMBUSTION_ID
  into cID
  from COMBUSTIONMOTORIZATION cm
  where cm.DESCRIPTION = cName;
  
  return cID;
end;
/

create or replace function getVehicleID
(vName in varchar)
return integer is
vID integer := 0;

begin
  select v.VEHICLE_ID
  into vID
  from vehicle v
  where v.NAME = vName;
  
  return vID;
end;
/

create or replace function getVehicleTypeID
(vtName in varchar)
return integer is
vtID integer := 0;

begin
  select vt1.TYPE_ID
  into vtID
  from VEHICLETYPE vt1
  where vt1.DESCRIPTION = vtName;
  
  return vtID;
end;
/
create or replace function getThrottleID
return integer is
tID integer := 0;

begin
  select count(*)
  into tID
  from THROTTLE;
  
  return tID;
end;
/

create or replace function getTypologyByName
(tName in varchar)
return integer is
tID integer := 0;

begin
  select t1.TYPOLOGY_ID
  into tID
  from typology t1
  where t1.DESCRIPTION = tName;
  
  return tID;
end;
/

create or replace function getVehicleIDs
(pID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from PROJECTVEHICLE PV1 where PV1.PROJECT = pID;
  return my_cursor;
end;
/

create or replace function getVehicles
(vID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from VEHICLE V1 where V1.VEHICLE_ID = vID;
  return my_cursor;
end;
/

create or replace function getElectricMotorizationID
(vID in integer)
return integer is
eID integer := 0;

begin
  select elm.ELECTRIC
  into eID
  from ELECTRICVEHICLE elm
  where elm.VEHICLE = vID;
  
  return eID;
end;
/

create or replace function getCombustionMotorizationID
(vID in integer)
return integer is
cID integer := 0;

begin
  select cm.COMBUSTION
  into cID
  from COMBUSTIONVEHICLE cm
  where cm.VEHICLE = vID;
  
  return cID;
end;
/

create or replace function getCombustionMotorization
(cID in integer)
return varchar is
cName varchar(50);

begin
  select cm.DESCRIPTION
  into cName
  from COMBUSTIONMOTORIZATION cm
  where cm.COMBUSTION_ID = cID;
  
  return cName;
end;
/

create or replace function getEnergy
(eID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from ENERGY E1 where E1.ENERGY_ID = eID;
  return my_cursor;
end;
/

create or replace function getThrottles
(eID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from THROTTLE T1 where T1.ENERGY = eID;
  return my_cursor;
end;
/

create or replace function getGears
(eID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from GEAR G1 where G1.ENERGY = eID;
  return my_cursor;
end;
/

create or replace function getRegimes
(tID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from REGIME R1 where R1.THROTTLE = tID;
  return my_cursor;
end;
/

create or replace function getLimits
(vID in integer)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from VELOCITYLIMIT VT1 where VT1.VEHICLE = vID;
  return my_cursor;
end;
/

create or replace function getTypologyName
(tID in integer)
return varchar is
  typename varchar(50);

begin
  select t1.DESCRIPTION
    into typename
    from TYPOLOGY t1
    where t1.TYPOLOGY_ID = tID;
  
  return typename;
end;
/

create or replace function getVehicleType
(vtID in integer)
return varchar is
  typename varchar(50);

begin
  select vt1.DESCRIPTION
    into typename
    from VEHICLETYPE vt1
    where vt1.TYPE_ID = vtID;
  
  return typename;
end;
/

create or replace procedure updateProject
(pName in varchar, pDescription in varchar, pID in integer) as
begin

  update PROJECT P1
    set 
    P1.PROJECTNAME = pName,
    P1.PROJECTDESCRIPTION = pDescription
    where P1.PROJECT_ID = pID;
  
  commit;
end updateProject;
/

create or replace procedure createSimulation
(sName in varchar, sDescription in varchar, pID in integer) as
  simID integer := 0;
begin
  select count(*)
  into simID
  from simulation;
  
  insert into simulation(simulation_id,simName,simDescription,project) values(simID + 1, sName, sDescription, pID);

end createSimulation;
/

create or replace function getSimulationByName
(sName in varchar)
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from simulation s1 where s1.SIMNAME = sName;
  return my_cursor;
end;
/

create or replace function allSimulations
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from simulation;
  return my_cursor;
end;
/

create or replace function allVehicles
return SYS_REFCURSOR as
  my_cursor SYS_REFCURSOR;

begin
  open my_cursor for select * from vehicle;
  return my_cursor;
end;
/

create or replace procedure associateVehicleProject
(vId in integer, pId in integer) as
begin
  insert into projectVehicle(vehicle,project) values (vId, pId);
end associateVehicleProject;
/