package Model.Vehicles;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author 1140256
 */
public class Energy {

    private double minRpm;
    private double maxRpm;
    private double finalDriveRatio;
    private ArrayList<Gear> gearList;
    private ArrayList<Throttle> throttleList;
    private double energyRegenerationRatio; //electric vehicles only

    public Energy(double minRpm, double maxRpm, double finalDriveRatio, ArrayList<Gear> gearList, ArrayList<Throttle> throttleList, double energyRegenerationRatio) {
        this.minRpm = minRpm;
        this.maxRpm = maxRpm;
        this.finalDriveRatio = finalDriveRatio;
        this.gearList = gearList;
        this.throttleList = throttleList;
        this.energyRegenerationRatio = energyRegenerationRatio;
    }

    public Energy() {
        this.minRpm = 0;
        this.maxRpm = 0;
        this.finalDriveRatio = 0;
        this.gearList = new ArrayList<>();
        this.throttleList = new ArrayList<>();
        this.energyRegenerationRatio = 0;
    }

    /**
     * @return the minRpm
     */
    public double getMinRpm() {
        return minRpm;
    }

    /**
     * @return the maxRpm
     */
    public double getMaxRpm() {
        return maxRpm;
    }

    /**
     * @return the finalDriveRatio
     */
    public double getFinalDriveRatio() {
        return finalDriveRatio;
    }

    /**
     * @param minRpm the minRpm to set
     */
    public void setMinRpm(double minRpm) {
        this.minRpm = minRpm;
    }

    /**
     * @param maxRpm the maxRpm to set
     */
    public void setMaxRpm(double maxRpm) {
        this.maxRpm = maxRpm;
    }

    /**
     * @param finalDriveRatio the finalDriveRatio to set
     */
    public void setFinalDriveRatio(double finalDriveRatio) {
        this.finalDriveRatio = finalDriveRatio;
    }

    /**
     * @return the gearList
     */
    public ArrayList<Gear> getGearList() {
        return gearList;
    }

    /**
     * @return the throttleList
     */
    public ArrayList<Throttle> getThrottleList() {
        return throttleList;
    }

    /**
     * @param gearList the gearList to set
     */
    public void setGearList(ArrayList<Gear> gearList) {
        this.gearList = gearList;
    }

    /**
     * @param throttleList the throttleList to set
     */
    public void setThrottleList(ArrayList<Throttle> throttleList) {
        this.throttleList = throttleList;
    }

    public void addGear(Gear g) {
        this.gearList.add(g);
    }

    public void addThrottle(Throttle t) {
        this.throttleList.add(t);
    }
    
    public Throttle getThrottle(String id) {
        for(Throttle t : this.throttleList){
        
            if(t.getId().equals(id)){
            
                return t;
            }
        }
        return null;
    }

    /**
     * @return the energyRegenerationRatio
     */
    public double getEnergyRegenerationRatio() {
        return energyRegenerationRatio;
    }

    /**
     * @param energyRegenerationRatio the energyRegenerationRatio to set
     */
    public void setEnergyRegenerationRatio(double energyRegenerationRatio) {
        this.energyRegenerationRatio = energyRegenerationRatio;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Energy other = (Energy) obj;
        if (Double.doubleToLongBits(this.minRpm) != Double.doubleToLongBits(other.minRpm)) {
            return false;
        }
        if (Double.doubleToLongBits(this.maxRpm) != Double.doubleToLongBits(other.maxRpm)) {
            return false;
        }
        if (Double.doubleToLongBits(this.finalDriveRatio) != Double.doubleToLongBits(other.finalDriveRatio)) {
            return false;
        }
        
        if (this.gearList.size() != other.gearList.size()) {
            return false;
        }
        
        for (Gear gearList1 : gearList) {
            if (!gearList1.equals(other.gearList.get(this.gearList.indexOf(gearList1)))) {
                return false;
            }
        }
        
        if (this.throttleList.size() != other.throttleList.size()) {
            return false;
        }
        
        for (Throttle throttleList1 : throttleList) {
            if (!throttleList1.equals(other.throttleList.get(this.throttleList.indexOf(throttleList1)))) {
                return false;
            }
        }
        
        if (Double.doubleToLongBits(this.energyRegenerationRatio) != Double.doubleToLongBits(other.energyRegenerationRatio)) {
            return false;
        }
        return true;
    }
    
    

}
