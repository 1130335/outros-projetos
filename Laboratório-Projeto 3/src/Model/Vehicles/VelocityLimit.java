/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import Model.Section;

/**
 *
 * @author Francisco
 */
public class VelocityLimit {
    
    private Section.Typology sectionType;
    private double speedLimit;

    public VelocityLimit(Section.Typology sectionType, double speedLimit) {
        this.sectionType = sectionType;
        this.speedLimit = speedLimit;
    }

    public VelocityLimit() {
        
    }

    /**
     * @return the sectionType
     */
    public Section.Typology getSectionType() {
        return sectionType;
    }

    /**
     * @return the speedLimit
     */
    public double getSpeedLimit() {
        return speedLimit;
    }

    /**
     * @param sectionType the sectionType to set
     */
    public void setSectionType(Section.Typology sectionType) {
        this.sectionType = sectionType;
    }

    /**
     * @param speedLimit the speedLimit to set
     */
    public void setSpeedLimit(double speedLimit) {
        this.speedLimit = speedLimit;
    }

    @Override
    public String toString() {
        return "VelocityLimit{" + "sectionType=" + sectionType + ", speedLimit=" + speedLimit + '}';
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VelocityLimit other = (VelocityLimit) obj;
        if (this.sectionType != other.sectionType) {
            return false;
        }
        if (Double.doubleToLongBits(this.speedLimit) != Double.doubleToLongBits(other.speedLimit)) {
            return false;
        }
        return true;
    }

    
}
