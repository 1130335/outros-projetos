package Model.Vehicles;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author 1140256
 */
public class Vehicle {

    
    
    /**
     * Enum with the types of vehicles
     */
    public enum VehicleType {
        TRUCK{public String toString(){return "truck";}},
        TRACTOR{public String toString(){return "tractor";}},
        SEMITRAILER{public String toString(){return "semi-trailer";}},
        CAR{public String toString(){return "car";}},
        MOTORCYCLE{public String toString(){return "motorcycle";}}
    }

    /**
     * Type of the vehicle ex: car,truck etc
     */
    private VehicleType type;
    /**
     * Motorization of the vehicle
     */
    private Motorization motorization;
    /**
     * Mass of the vehicle in kg
     */
    private double mass;
    /**
     * Load that the vehicle can take in kg
     */
    private double load;
    /**
     * Drag coefficient
     */
    private double dragCoefficient;
    /**
     * Name of the vehicle
     */
    private String name;
    /**
     * Description of the vehicle
     */
    private String description;
    /**
     * Rolling resistance coefficient
     */
    private double rrc;
    /**
     * Wheel size in meters.                        // O diâmetro de um pneu (wheel size) é normalmente expressso em polegadas. Cada polegada corresponde a 2.54cm.
     *                                                                              // Assim um carro que tenha diâmetro de pneu de 20'' equivale a 20 * 2.54 = 50.8cm = 0.508m de diãmetro.
     *                                                                              // Radius of tire ou raio do pneu, variável utilizada no cálculo da velocidade linear de um veículo é portanto metade do valor anterior (0.254m neste caso).
     */
    private double wheelSize;
    /**
     * Energy of the vehicle.
     */
    private Energy energy;
    
    /**
     * frontal area of vehicle
     */
    private double frontalArea;
    
    /**
     * List with the velocity limits of the vehicle
     */
    private List<VelocityLimit> limitList;

    /**
     * Empty constructor of vehicle
     */
    public Vehicle() {
        setType(VehicleType.CAR);
        setMotorization(new Gasoline());
        setMass(1);
        setLoad(0);
        setDragCoefficient(0);
        setName("vehicle");
        setDescription("");
        setRrc(0);
        setWheelSize(0);
        setEnergy(new Energy());
        setFrontalArea(0);
        setLimitList(new ArrayList<>());
    }

    
    public Vehicle(String name, String description, VehicleType type, Motorization motorization, double mass,
            double load, double dragCoefficient, double frontalArea, double rrc,
            double wheelSize,  ArrayList<VelocityLimit> limitList, Energy energy) {
        setType(type);
        setMotorization(motorization);
        setMass(mass);
        setLoad(load);
        setDragCoefficient(dragCoefficient);
        setName(name);
        setDescription(description);
        setRrc(rrc);
        setWheelSize(wheelSize);
        setEnergy(energy);
        setFrontalArea(frontalArea);
        setLimitList(limitList);
        
    }
    
    /**
     * Cloner of vehicle.
     * @param v Vehicle that will be copied.
     */
    public Vehicle(Vehicle v){
        setType(v.getType());
        setMotorization(v.getMotorization());
        setMass(v.getMass());
        setLoad(v.getLoad());
        setDragCoefficient(v.getDragCoefficient());
        setName(v.getName());
        setDescription(v.getDescription());
        setRrc(v.getRrc());
        setWheelSize(v.getWheelSize());
        setEnergy(v.getEnergy());
        setFrontalArea(v.getFrontalArea());
        setLimitList(v.getLimitList());
    }

    /**
     * Returns the type
     * @return the type
     */
    public VehicleType getType() {
        return type;
    }

    /**
     * Returns the motorization
     * @return the motorization
     */
    public Motorization getMotorization() {
        return motorization;
    }

    /**
     * Returns the mass
     * @return the mass
     */
    public double getMass() {
        return mass;
    }

    /**
     * Returns the load
     * @return the load
     */
    public double getLoad() {
        return load;
    }

    /**
     * Returns the drag coefficient
     * @return the dragCoefficient
     */
    public double getDragCoefficient() {
        return dragCoefficient;
    }
    
    /**
     * Returns the name of the vehicle.
     * @return the name of the vehicle
     */
    public String getName() {
        return name;
    }
    
    /**
     * Returns the description of the vehicle.
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the rolling resistance coefficient
     * @return the rrc
     */
    public double getRrc() {
        return rrc;
    }

    /**
     * Return the wheel size
     * @return the wheelSize
     */
    public double getWheelSize() {
        return wheelSize;
    }

    /**
     * Returns the energy
     * @return the energy
     */
    public Energy getEnergy() {
        return energy;
    }

    /**
     * Changes the type of the vehicle
     * @param type the type to set
     */
    public void setType(VehicleType type) {
        this.type = type;
    }

    /**
     * Changes the motorization
     * @param motorization the motorization to set
     */
    public void setMotorization(Motorization motorization) {
        this.motorization = motorization;
    }

    /**
     * Changes the mass
     * @param mass the mass to set
     */
    public void setMass(double mass) {
        if (mass <= 0) {
            throw new IllegalArgumentException("Mass can't be equal or below 0.");
        }
        this.mass = mass;
    }

    /**
     * Changes the load
     * @param load the load to set
     */
    public void setLoad(double load) {
        if (load < 0) {
            throw new IllegalArgumentException("Load can't be below 0.");
        }
        this.load = load;
    }

    /**
     * Changes the drag coefficient
     * @param dragCoefficient the dragCoefficient to set
     */
    public void setDragCoefficient(double dragCoefficient) {
        if (dragCoefficient < 0) {
            throw new IllegalArgumentException("Drag coefficient can't be below 0.");
        }
        this.dragCoefficient = dragCoefficient;
    }

   

    /**
     * Modifies the name of the vehicle
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    

    /**
     * Changesthe description
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Changes the rrc
     * @param rrc the rrc to set
     */
    public void setRrc(double rrc) {
        this.rrc = rrc;
    }

    /**
     * Changes the wheel size
     * @param wheelSize the wheelSize to set
     */
    public void setWheelSize(double wheelSize) {
        this.wheelSize = wheelSize;
    }

    /**
     * Changes the energy
     * @param energy the energy to set
     */
    public void setEnergy(Energy energy) {
        this.energy = energy;
    }
    
    /**
     * @return the frontalArea
     */
    public double getFrontalArea() {
        return frontalArea;
    }

    /**
     * @param frontalArea the frontalArea to set
     */
    public void setFrontalArea(double frontalArea) {
        this.frontalArea = frontalArea;
    }

    /**
     * @return the limitList
     */
    public List<VelocityLimit> getLimitList() {
        return limitList;
    }

    /**
     * @param limitList the limitList to set
     */
    public void setLimitList(List<VelocityLimit> limitList) {
        this.limitList = limitList;
    }
    
    public void addVelocityLimit(VelocityLimit limit){
        
        this.limitList.add(limit);
    }

    /**
     * Textual description of the vehicle
     * @return textual description of the vehicle
     */
    @Override
    public String toString() {
        return "" + motorization +" "+ type +", name:" + name + ", description:" + description;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehicle other = (Vehicle) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.motorization.getMotorization(), other.motorization.getMotorization())) {
            return false;
        }
        if (Double.doubleToLongBits(this.mass) != Double.doubleToLongBits(other.mass)) {
            return false;
        }
        if (Double.doubleToLongBits(this.load) != Double.doubleToLongBits(other.load)) {
            return false;
        }
        if (Double.doubleToLongBits(this.dragCoefficient) != Double.doubleToLongBits(other.dragCoefficient)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (Double.doubleToLongBits(this.rrc) != Double.doubleToLongBits(other.rrc)) {
            return false;
        }
        if (Double.doubleToLongBits(this.wheelSize) != Double.doubleToLongBits(other.wheelSize)) {
            return false;
        }
        if (!this.energy.equals(other.energy)) {
            return false;
        }
        if (Double.doubleToLongBits(this.frontalArea) != Double.doubleToLongBits(other.frontalArea)) {
            return false;
        }
        
        if (this.limitList.size() != other.limitList.size()) {
            return false;
        }
        
        for (VelocityLimit limitList1 : limitList) {
            if (!limitList1.equals(other.limitList.get(this.limitList.indexOf(limitList1)))) {
                return false;
            }
        }
        
        return true;
    }

    
}
