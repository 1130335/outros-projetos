

package Model.Vehicles;

/**
 *
 * @author 1140256
 */
public class Hybrid extends Motorization{
    
    private Combustion comb;
    private Electric elec;

    public Hybrid() {
        super("Hybrid");
    }

    public Hybrid(Combustion comb, Electric elec) {
        super("Hybrid");
        this.comb = comb;
        this.elec = elec;
    }

    /**
     * @return the comb
     */
    public Combustion getComb() {
        return comb;
    }

    /**
     * @return the elec
     */
    public Electric getElec() {
        return elec;
    }

    /**
     * @param comb the comb to set
     */
    public void setComb(Combustion comb) {
        this.comb = comb;
    }

    /**
     * @param elec the elec to set
     */
    public void setElec(Electric elec) {
        this.elec = elec;
    }
    
    
    
    
    
    

}
