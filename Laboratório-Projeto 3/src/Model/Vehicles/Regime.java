/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

/**
 *
 * @author Francisco
 */
public class Regime {
    
    
    private double torque;
    private double rpmLow;
    private double rpmHigh;
    private double sfc; //combustion only

    public Regime(){
        this.rpmHigh =0;
        this.rpmLow =0;
        this.torque = 0;
        this.sfc = 0;
    
    }
    
    public Regime(double torque, double rpmLow, double rpmHigh, double sfc) {
        this.torque = torque;
        this.rpmLow = rpmLow;
        this.rpmHigh = rpmHigh;
        this.sfc = sfc;
    }
    
    /**
     * @return the torque
     */
    public double getTorque() {
        return torque;
    }

    /**
     * @param torque the torque to set
     */
    public void setTorque(double torque) {
        this.torque = torque;
    }

    /**
     * @return the rpmLow
     */
    public double getRpmLow() {
        return rpmLow;
    }

    /**
     * @param rpmLow the rpmLow to set
     */
    public void setRpmLow(double rpmLow) {
        this.rpmLow = rpmLow;
    }

    /**
     * @return the rpmHigh
     */
    public double getRpmHigh() {
        return rpmHigh;
    }

    /**
     * @param rpmHigh the rpmHigh to set
     */
    public void setRpmHigh(double rpmHigh) {
        this.rpmHigh = rpmHigh;
    }

    /**
     * @return the sfc
     */
    public double getSfc() {
        return sfc;
    }

    /**
     * @param sfc the sfc to set
     */
    public void setSfc(double sfc) {
        this.sfc = sfc;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Regime other = (Regime) obj;
        if (Double.doubleToLongBits(this.torque) != Double.doubleToLongBits(other.torque)) {
            return false;
        }
        if (Double.doubleToLongBits(this.rpmLow) != Double.doubleToLongBits(other.rpmLow)) {
            return false;
        }
        if (Double.doubleToLongBits(this.rpmHigh) != Double.doubleToLongBits(other.rpmHigh)) {
            return false;
        }
        if (Double.doubleToLongBits(this.sfc) != Double.doubleToLongBits(other.sfc)) {
            return false;
        }
        return true;
    }
    
    
    
}
