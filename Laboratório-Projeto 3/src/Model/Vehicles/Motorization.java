

package Model.Vehicles;

/**
 *
 * @author 1140256
 */
public abstract class Motorization {
    
    private String motorization;
    
    public Motorization(String m){
       setMotorization(m);
    }

    public String getMotorization() {
        return motorization;
    }

    public void setMotorization(String motorization) {
        this.motorization = motorization;
    }

    @Override
    public String toString() {
        return motorization;
    }
    
    

}
