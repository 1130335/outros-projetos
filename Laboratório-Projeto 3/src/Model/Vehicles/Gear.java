/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import java.util.Objects;

/**
 *
 * @author Francisco
 */
public class Gear {
    
    private String gearID;
    private double ratio;

    public Gear(String gearID, double ratio) {
        this.gearID = gearID;
        this.ratio = ratio;
    }

    public Gear() {
        this.gearID = "";
        this.ratio = 0;
    }
    

    /**
     * @return the gearID
     */
    public String getGearID() {
        return gearID;
    }

    /**
     * @return the ratio
     */
    public double getRatio() {
        return ratio;
    }

    /**
     * @param gearID the gearID to set
     */
    public void setGearID(String gearID) {
        this.gearID = gearID;
    }

    /**
     * @param ratio the ratio to set
     */
    public void setRatio(double ratio) {
        this.ratio = ratio;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gear other = (Gear) obj;
        if (!Objects.equals(this.gearID, other.gearID)) {
            return false;
        }
        if (Double.doubleToLongBits(this.ratio) != Double.doubleToLongBits(other.ratio)) {
            return false;
        }
        return true;
    }
    
    
    
}
