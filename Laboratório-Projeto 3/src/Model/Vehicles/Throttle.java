/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Vehicles;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Francisco
 */
public class Throttle {

    private String id;
    private ArrayList<Regime> regimeList;

    public Throttle() {
        this.id = "";
        this.regimeList = new ArrayList<>();
    }

    public Throttle(String id, ArrayList<Regime> regimeList) {
        this.id = id;
        this.regimeList = regimeList;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the regimeList
     */
    public ArrayList<Regime> getRegimeList() {
        return regimeList;
    }

    /**
     * @param regimeList the regimeList to set
     */
    public void setRegimeList(ArrayList<Regime> regimeList) {
        this.regimeList = regimeList;
    }

    public void addRegime(Regime r) {
        this.regimeList.add(r);
    }

    public Regime getRegime(double rpm) {

        for (Regime r : this.regimeList) {

            if (r.getRpmLow()<= rpm && r.getRpmHigh()>= rpm) {

                return r;
            }
        }
        return null;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Throttle other = (Throttle) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        
        if (this.regimeList.size() != other.regimeList.size()) {
            return false;
        }
        
        for (Regime regime1 : regimeList) {
            if (!regime1.equals(other.regimeList.get(this.regimeList.indexOf(regime1)))) {
                return false;
            }
        }
        return true;
    }
    
    

}
