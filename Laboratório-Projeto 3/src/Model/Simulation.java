/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Francisco
 */
public class Simulation {

    
    private String name;
    private String description;
    private ArrayList<TrafficPattern> trafficList;
    private Project activeProject;

    public Simulation() {
        this.name ="name";
        this.description = "description";
        this.trafficList = new ArrayList();
        this.activeProject = new Project();
    }

    public Simulation(String name, String description, ArrayList<TrafficPattern> trafficList, Project activeProject) {
        this.name = name;
        this.description = description;
        this.trafficList = trafficList;
        this.activeProject = activeProject;
    }

    public Simulation(Simulation s) {
        this.activeProject = s.activeProject;
        this.name = s.name;
        this.description = s.description;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the trafficList
     */
    public ArrayList<TrafficPattern> getTrafficList() {
        return trafficList;
    }

    /**
     * @return the activeProject
     */
    public Project getActiveProject() {
        return activeProject;
    }

    

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param trafficList the trafficList to set
     */
    public void setTrafficList(ArrayList<TrafficPattern> trafficList) {
        this.trafficList = trafficList;
    }

    /**
     * @param activeProject the activeProject to set
     */
    public void setActiveProject(Project activeProject) {
        this.activeProject = activeProject;
    }

    public void addTrafficPattern(TrafficPattern tp) {

        this.trafficList.add(tp);
    }

    @Override
    public String toString() {
        return "Simulation{" + "name=" + name + ", description=" + description + '}';
    }

}
