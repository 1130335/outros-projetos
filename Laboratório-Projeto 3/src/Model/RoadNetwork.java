/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.GraphAlgorithm;
import Graph.Vertex;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 *
 * @author Paulo silva
 */
public class RoadNetwork {

    private Graph<Node, Section> roadNetwork;
    private List<Node> nodesList = new ArrayList<Node>();
    private List<Section> sections = new ArrayList<Section>();
    /**
     * variables declaration
     */
    private String name;
    private String descrition;

    public RoadNetwork() {
        this.roadNetwork = new Graph<>(true); //true indica que grafo é orientado
        this.name = "";
        this.descrition = "";
        this.nodesList = new ArrayList<>();
        this.sections = new ArrayList<>();
    }

    public RoadNetwork(String name, String description) {
        this.name = name;
        this.descrition = description;
    }

    public void addNode(Node n) {
        roadNetwork.insertVertex(n);
        nodesList.add(n);
    }

    public void addSection(Section s) {

        sections.add(s);
    }

    public String getDescription() {
        return this.descrition;
    }

    public List<Node> getNodesList() {
        return this.nodesList;
    }

    public List<Section> getSections() {
        return this.sections;
    }

    public void setNodesList(List<Node> nodesList) {
        this.nodesList = nodesList;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

//        
//    /**
//     * empty constructor
//     */
//    public RoadNetwork() {
//        this.roadNetwork = new Graph<>(false);
//        this.roadList = new ArrayList<Road>();
//        this.nodeList = new ArrayList<Node>();
//        this.name = null;
//    }
//
//    /**
//     * constructor by copy
//     *
//     * @param roadNetwork:
//     */
//    public RoadNetwork(RoadNetwork roadNetwork) {
//        this.name = roadNetwork.getName();
//        this.descrition = roadNetwork.getDescription();
//    }
//
//    /**
//     *
//     * @param name
//     * @param description
//     */
//    public RoadNetwork(String name, String description) {
//        this.roadNetwork = new Graph<>(false);
//        this.roadList = new ArrayList<Road>();
//        this.nodeList = new ArrayList<Node>();
//        this.name = name;
//        this.descrition = description;
//    }
//    /**
//     * constructor with list parameter
//     *
//     * @param list
//     */
//    public RoadNetwork(List<Road> list) {
//        this.roadList = list;
//    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String descrition) {
        this.descrition = descrition;
    }

    public void addNodeToRoadNetwork(Node n) {
        roadNetwork.insertVertex(n);
    }

    public void addSectionBetweentwoNodes(Section sct) {

        roadNetwork.insertEdge(sct.getBeginningNode(), sct.getEndingNode(), sct, sct.getSectionLength());          // o nome atual não é muito assertivo
    }

    public boolean existsConnection(Node n1, Node n2) {
        if (this.roadNetwork.getEdge(roadNetwork.getVertex(n1), roadNetwork.getVertex(n2)) != null) {
            return true;
        }
        return false;
    }

    public Graph<Node, Section> getRoadNetworkGraph() {

        return this.roadNetwork;
    }

}
