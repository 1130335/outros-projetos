package Model;

import Model.Vehicles.Vehicle;

/**
 *
 * @author Francisco
 */
public class TrafficPattern {

    private String beginNodeId;
    private String endNodeId;
    private String vehicleId;
    private int arrivalRate;
    private String timeUnit; // pode ser 10 veículos por minuto ou 10 por segundo pe.

    public TrafficPattern() {
    }

    public TrafficPattern(String beginNode, String endNode, String vehicle, int arrivalRate, String timeUnit) {
        this.beginNodeId = beginNode;
        this.endNodeId = endNode;
        this.vehicleId = vehicle;
        this.arrivalRate = arrivalRate;
        this.timeUnit = timeUnit;
    }

    /**
     * @return the beginNodeId
     */
    public String getBeginNodeId() {
        return beginNodeId;
    }

    /**
     * @return the endNodeId
     */
    public String getEndNodeId() {
        return endNodeId;
    }

    /**
     * @return the vehicle
     */
    public String getVehicle() {
        return vehicleId;
    }

    /**
     * @return the arrivalRate
     */
    public int getArrivalRate() {
        return arrivalRate;
    }

    /**
     * @return the timeUnit
     */
    public String getTimeUnit() {
        return timeUnit;
    }

    /**
     * @param beginNodeId the beginNodeId to set
     */
    public void setBeginNodeId(String beginNodeId) {
        this.beginNodeId = beginNodeId;
    }

    /**
     * @param endNodeId the endNodeId to set
     */
    public void setEndNodeId(String endNodeId) {
        this.endNodeId = endNodeId;
    }

    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(String vehicle) {
        this.vehicleId = vehicle;
    }

    /**
     * @param arrivalRate the arrivalRate to set
     */
    public void setArrivalRate(int arrivalRate) {
        this.arrivalRate = arrivalRate;
    }

    /**
     * @param timeUnit the timeUnit to set
     */
    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public String toString() {
        return "TrafficPattern{" + "beginNodeId=" + beginNodeId + ", endNodeId=" + endNodeId + ", vehicleId=" + vehicleId + ", arrivalRate=" + arrivalRate + ", timeUnit=" + timeUnit + '}';
    }
    
    


}
