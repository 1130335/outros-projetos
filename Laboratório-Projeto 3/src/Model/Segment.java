package Model;

import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.List;

/**
 * A section is	composed of one or more segments, i.e. road segments with
 * uniform characteristics along the whole segment. A segment is characterized
 * by: Segment index (i = 1..n); Initial height; Slope; Length; Rolling
 * resistance coefficient (“Rolling resistance”,	2015); Maximum velocity
 * (optional); Minimum	velocity (optional); Maximum number of vehicles
 *
 * @author Helder
 */
public class Segment {

    /**
     * The index of the segment (corresponding to the position of the segment in
     * the section)
     */
    private int segm_index;

    /**
     * The initial heigh (elevação inicial) of the segment
     */
    private double initialHeigh;

    /**
     * The slope (declive/inclinação) of the segment
     */
    private double slope;

    /**
     * The length in Km (extensão ou comprimento) of the segment 
     */
    private double length;

    /**
     * The rolling resistance coefficient (“Rolling resistance”,	2015) of the
     * segment
     */
    private double rollResistCoef;

    /**
     * The maximum number of vehicles allowed in the segment
     */
    private int maxNumOfVehicles;

    /**
     * (optional) The minimum velocity of the segment
     */
    private double minVelocity;

    /**
     * (optional) The maximum velocity of the segment (in Km/h)
     */
    private double maxVelocity;

    /**
     * The list of vehicles on a segment
     */
    private List<Vehicle> listOfVehicles;

    /**
     * Complete Constructor of Segment (also with optional attributes)
     *
     * @param segm_index
     * @param initialHeigh
     * @param slope
     * @param length
     * @param rollResistCoef
     * @param maxNumOfVehicles
     * @param minVelocity
     * @param maxVelocity
     */
    public Segment(int segm_index, double initialHeigh, double slope, double length, double rollResistCoef, int maxNumOfVehicles, double minVelocity, double maxVelocity) {
        setSegm_index(segm_index);
        setInitialHeigh(initialHeigh);
        setSlope(slope);
        setLength(length);
        setRollResistCoef(rollResistCoef);
        setMaxNumOfVehicles(maxNumOfVehicles);
        setMinVelocity(minVelocity);
        setMaxVelocity(maxVelocity);
        this.listOfVehicles = new ArrayList<>();
    }

    /**
     * Constructor with the mandatory attributes of segment
     *
     * @param segm_index
     * @param initialHeigh
     * @param slope
     * @param length
     * @param rollResistCoef
     * @param maxNumOfVehicles
     */
    public Segment(int segm_index, double initialHeigh, double slope, double length, double rollResistCoef, int maxNumOfVehicles) {
        setSegm_index(segm_index);
        setInitialHeigh(initialHeigh);
        setSlope(slope);
        setLength(length);
        setRollResistCoef(rollResistCoef);
        setMaxNumOfVehicles(maxNumOfVehicles);
        setMinVelocity(0);
        setMaxNumOfVehicles(0);
        this.listOfVehicles = new ArrayList<>();
    }

    /**
     * Copy Constructor of Segment
     *
     * @param segment
     */
    public Segment(Segment segment) {
        this.segm_index = segment.getSegm_index();
        this.initialHeigh = segment.getInitialHeigh();
        this.slope = segment.getSlope();
        this.length = segment.getLength();
        this.rollResistCoef = segment.getRollResistCoef();
        this.maxNumOfVehicles = segment.getMaxNumOfVehicles();
        this.minVelocity = segment.getMinVelocity();
        this.maxVelocity = segment.getMaxVelocity();
        this.listOfVehicles = new ArrayList<>();
    }

    /**
     * Empty Constructor of Segment
     */
    public Segment() {
        this(0, 0, 0, 0, 0, 0, 0, 0);
        this.listOfVehicles = new ArrayList<>();
    }

    /**
     * Returns the index of the segment
     *
     * @return the segm_index
     */
    public int getSegm_index() {
        return segm_index;
    }

    /**
     * Modifies the index of the segment
     *
     * @param segm_index the segm_index to set
     */
    public void setSegm_index(int segm_index) {
        this.segm_index = segm_index;
    }

    /**
     * Returns the inital height of the segment
     *
     * @return the initialHeigh
     */
    public double getInitialHeigh() {
        return initialHeigh;
    }

    /**
     * Modifies the initial height of the segment
     *
     * @param initialHeigh the initialHeigh to set
     */
    public void setInitialHeigh(double initialHeigh) {
        this.initialHeigh = initialHeigh;
    }

    /**
     * Returns the slope of the segment
     *
     * @return the slope
     */
    public double getSlope() {
        return slope;
    }

    /**
     * Modifies the slope of the segment
     *
     * @param slope the slope to set
     */
    public void setSlope(double slope) {
        this.slope = slope;
    }

    /**
     * Returns the length of the segment
     *
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * Modifies the length of the segment
     *
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Returns the rolling resistance coefficient of the segment
     *
     * @return the rollResistCoef
     */
    public double getRollResistCoef() {
        return rollResistCoef;
    }

    /**
     * Modifies the rolling resistance coefficient of the segment
     *
     * @param rollResistCoef the rollResistCoef to set
     */
    public void setRollResistCoef(double rollResistCoef) {
        this.rollResistCoef = rollResistCoef;
    }

    /**
     * Returns the maximun number of vehicles of the segment
     *
     * @return the maxNumOfVehicles
     */
    public int getMaxNumOfVehicles() {
        return maxNumOfVehicles;
    }

    /**
     * Modifies the maximun number of vehicles of the segment
     *
     * @param maxNumOfVehicles the maxNumOfVehicles to set
     */
    public void setMaxNumOfVehicles(int maxNumOfVehicles) {
        this.maxNumOfVehicles = maxNumOfVehicles;
    }

    /**
     * Returns the minimum velocity of the segment
     *
     * @return the minVelocity
     */
    public double getMinVelocity() {
        return minVelocity;
    }

    /**
     * Modifies the minimum velocity of the segment
     *
     * @param minVelocity the minVelocity to set
     */
    public void setMinVelocity(double minVelocity) {
        this.minVelocity = minVelocity;
    }

    /**
     * Returns the maximum velocity of the segment
     *
     * @return the maxVelocity
     */
    public double getMaxVelocity() {
        return maxVelocity;
    }

    /**
     * Modifies the maximun velocity of the segment
     *
     * @param maxVelocity the maxVelocity to set
     */
    public void setMaxVelocity(double maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    /**
     * Return the list of vehicles of the segment
     *
     * @return the listOfVehicles
     */
    public List<Vehicle> getListOfVehicles() {
        return listOfVehicles;
    }

    /**
     * Modifies the list of vehicles of the segment
     *
     * @param listOfVehicles the listOfVehicles to set
     */
    public void setListOfVehicles(List<Vehicle> listOfVehicles) {
        this.listOfVehicles = listOfVehicles;
    }

    /**
     * Returns the complete description of attributes from a segment
     *
     * @return the complete description of attributes from a segment
     */
    public String toStringComplete() {
        String minVelocityStr = "";
        String maxVelocityStr = "";
        if (this.minVelocity != 0) {
            minVelocityStr = ", minimum velocity of " + this.minVelocity;//string
        }
        if (this.maxVelocity != 0) {
            maxVelocityStr = ", maximum velocity of " + this.maxVelocity + ".";
        }
        return "Segment " + this.segm_index + " has initial heigh " + this.initialHeigh
                + ", slope " + slope + ", length " + length + ", rolling resistance coefficient "
                + rollResistCoef + ", maximum number of vehicles " + maxNumOfVehicles
                + minVelocityStr + maxVelocityStr;
    }
    
     /**
     * Add a vehicle to the list of vehicles of the segment
     *
     * @param v the vehicle to add
     * return true if the vehicle was added
     */
    public boolean addVehicleToListOfV(Vehicle v) {
        if (!this.listOfVehicles.contains(v)) {
            return this.listOfVehicles.add(v);
        }
        return false;
    }
    
     /**
     * Removes an vehicle from the list of vehicles of the segment
     *
     * @param v the vehicle to remove
     * @return true if the vehicle was removed
     */
    public boolean removeVehicleToListOfV(Vehicle v) {
        if (this.listOfVehicles.contains(v)) {
            return this.listOfVehicles.remove(v);
        }
        return false;
    }    
}
