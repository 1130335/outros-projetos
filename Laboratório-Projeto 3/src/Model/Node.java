/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo silva
 */
public class Node {

    /**
     * variables declaration
     */
    private List<Section> roadsIn = new ArrayList<Section>();
    private List<Section> roadsOut = new ArrayList<Section>();
    private String nodeId;

    /**
     * empty constructor
     */
    public Node() {
        this.roadsIn = new ArrayList<Section>();
        this.roadsOut = new ArrayList<Section>();
    }

    public Node(String nodeId) {
        this.roadsIn = new ArrayList<Section>();
        this.roadsOut = new ArrayList<Section>();
        this.nodeId = nodeId;
    }

    /**
     * constructor with lists parameters
     *
     * @param roadsIn
     * @param roadsOut
     */
    public Node(List<Section> roadsIn, List<Section> roadsOut) {
        this.roadsIn = roadsIn;
        this.roadsOut = roadsOut;
    }

    /**
     * @return the roadsIn
     */
    public List<Section> getRoadsIn() {
        return this.roadsIn;
    }

    /**
     * @return the roadsOut
     */
    public List<Section> getRoadsOut() {
        return this.roadsOut;
    }

    public String getNodeID() {
        return this.nodeId;
    }

    public void addSectionOut(Section o) {
        this.roadsOut.add(o);
    }

    public void addSectionIn(Section i) {
        this.roadsIn.add(i);
    }
    
    public String toString(){
        return this.getNodeID();
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }
    
}
