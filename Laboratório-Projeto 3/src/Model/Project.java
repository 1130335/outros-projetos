package Model;

import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.List;

public class Project {

    private RoadNetwork roadNetwork;
    private String name;
    private String description;
    private boolean active;
    /**
     * The list of vehicles of the project
     */
    private List<Vehicle> listOfVehicles;

    public Project() {
        this.roadNetwork = new RoadNetwork();
        this.name = "";
        this.description = "";
//        this.name = name;
//        this.description = description;
        this.active = false;
        this.listOfVehicles = new ArrayList<>();
    }
    
    /**
     * Complete constructor of project
     *
     * @param roadNetwork: the road network associated
     * @param name: name of the project
     * @param description: description of the project
     */
    public Project(RoadNetwork roadNetwork, String name, String description) {
        this.roadNetwork = roadNetwork;
        this.name = name;
        this.description = description;
        this.active = false;
        this.listOfVehicles = new ArrayList<>();
    }

    /**
     * Constructor by copy
     *
     * @param project: project to copy
     */
    public Project(Project project) {
        this.name = project.getName();
        this.description = project.getDescription();
        this.roadNetwork = project.getRoadNetwork();
        this.active = false;
        this.listOfVehicles = project.getListOfVehicles();
    }

    /**
     * Returns the road network of the project
     *
     * @return roadNetwork
     */
    public RoadNetwork getRoadNetwork() {
        return roadNetwork;
    }

    /**
     * Modifies the road network of the project
     *
     * @param roadNetwork: new road network
     */
    public void setRoadNetwork(RoadNetwork roadNetwork) {
        this.roadNetwork = roadNetwork;
    }

    /**
     * Returns the name of the project
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Modifies the name of the project
     *
     * @param name: new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the description of the project
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Modifies the description of the project
     *
     * @param description: new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the state of the project
     *
     * @return active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Turns the project active
     */
    public void setActive() {
        this.active = true;
    }

    /**
     * Return the list of vehicles of the project
     *
     * @return the listOfVehicles
     */
    public List<Vehicle> getListOfVehicles() {
        return listOfVehicles;
    }

    /**
     * Modifies the list of vehicles of the project
     *
     * @param listOfVehicles the listOfVehicles to set
     */
    public void setListOfVehicles(List<Vehicle> listOfVehicles) {
        this.listOfVehicles = listOfVehicles;
    }

    /**
     * Returns a string with the information about the project
     *
     * @return string of information
     */
    @Override
    public String toString() {
        return "Project{" + "roadNetwork=" + roadNetwork + ", name=" + name + ", description=" + description + '}';
    }

    /**
     * Add a vehicle to the list of vehicles of the project
     *
     * @param v the vehicle to add return true if the vehicle was added
     */
    public boolean addVehicleToListOfV(Vehicle v) {
        if (!this.listOfVehicles.contains(v)) {
            return this.listOfVehicles.add(v);
        }
        return false;
    }

    /**
     * Removes an vehicle from the list of vehicles of the project
     *
     * @param v the vehicle to remove
     * @return true if the vehicle was removed
     */
    public boolean removeVehicleToListOfV(Vehicle v) {
        if (this.listOfVehicles.contains(v)) {
            return this.listOfVehicles.remove(v);
        }
        return false;
    }

}
