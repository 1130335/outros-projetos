package Model;

import Graph.Edge;
import Graph.Vertex;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Section: part of the road connection nodes/junction.
 */
public class Section {

    /**
     * Enum Typology Declaration
     */
    public enum Typology {

        REGULAR_ROAD,
        URBAN_ROAD,
        EXPRESS_ROAD,
        CONTROLLED_ACCESS_HIGHWAY
    };

    /**
     * Enum Direction Declaration
     */
    public enum Direction {

        DIRECT,
        REVERSE,
        BIDIRECTIONAL
    };

    /* Variables Declaration */
    private Node beginningNode;
    private Node endingNode;
    private Typology typology;
    private Direction direction;
    private String road;
    private int toll;
    private double windDirection;
    private double windSpeed;
    private List<Segment> sequenceOfSegments;

    /**
     * Empty Constructor Default Values: Typology = Regular Road Direction =
     * Direct (from beginning to the end) Toll = 0 Wind Direction = 0 (angle ->
     * decimal) Wind Speed = 0 (m/s)
     */
    public Section() {
        this.beginningNode = new Node();
        this.endingNode = new Node();
        this.typology = Typology.REGULAR_ROAD;
        this.direction = Direction.DIRECT;
        this.toll = 0;
        this.windDirection = 0;
        this.windSpeed = 0;
        this.sequenceOfSegments = new ArrayList<Segment>();
        this.road = "";
    }

    /**
     * Complete Constructor (All Parameters)
     *
     * @param beginningNode: node where the section begins
     * @param endingNode: node where the section ends
     * @param typology: type of the section
     * @param direction: direction of the section
     * @param toll: number of existing tolls
     * @param windDirection: direction (angle) of the wind
     * @param windSpeed: speed of the wind (m/s)
     * @param road: the road name of the section
     */
    public Section(Node beginningNode, Node endingNode, Typology typology, Direction direction, int toll, double windDirection, double windSpeed, String road) {
        this.beginningNode = beginningNode;
        this.endingNode = endingNode;
        this.typology = typology;
        this.direction = direction;
        this.toll = toll;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
        this.sequenceOfSegments = new ArrayList<Segment>();
        this.road = road;
    }

    /**
     * Constructor by Copy
     *
     * @param section: section to copy
     */
    public Section(Section section) {
        this.beginningNode = section.beginningNode;
        this.endingNode = section.endingNode;
        this.typology = section.typology;
        this.direction = section.direction;
        this.toll = section.toll;
        this.windDirection = section.windDirection;
        this.windSpeed = section.windSpeed;
        this.sequenceOfSegments = section.sequenceOfSegments;
        this.road = section.getRoad();
    }

    /**
     * Returns the beginning node of the section
     *
     * @return beginningNode
     */
    public Node getBeginningNode() {
        return this.beginningNode;
    }

    /**
     * Returns the ending node of the section
     *
     * @return endingNode
     */
    public Node getEndingNode() {
        return this.endingNode;
    }

    /**
     * Returns the type of the section
     *
     * @return typology
     */
    public Typology getTipology() {
        return this.typology;
    }

    /**
     * Returns the direction of the section
     *
     * @return direction
     */
    public Direction getDirection() {
        return this.direction;
    }

    /**
     * Returns the number of existing tolls in the section
     *
     * @return toll
     */
    public int getToll() {
        return this.toll;
    }

    /**
     * Returns the wind direction in that section
     *
     * @return windDirection
     */
    public double getWindDirection() {
        return this.windDirection;
    }

    /**
     * Returns the wind speed in that section
     *
     * @return windSpeed
     */
    public double getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * Returns the sequence of ordered segments of that section
     *
     * @return sequenceOfSegments
     */
    public List<Segment> getSequenceOfSegments() {
        return this.sequenceOfSegments;
    }

    /**
     * Modifies the beginning node of the section
     *
     * @param beginningNode: new beginning node
     */
    public void setBeginningNode(Node beginningNode) {
        this.beginningNode = beginningNode;
    }

    /**
     * Modifies the ending node of the section
     *
     * @param endingNode: new ending node
     */
    public void setEndingNode(Node endingNode) {
        this.endingNode = endingNode;
    }

    /**
     * Modifies the typology of the section
     *
     * @param typology: new typology
     */
    public void setTypology(Typology typology) {
        this.typology = typology;
    }

    /**
     * Modifies the direction of the section
     *
     * @param direction: new direction
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Modifies the number of tolls in the section
     *
     * @param toll: new number of tolls
     */
    public void setToll(int toll) {
        this.toll = toll;
    }

    /**
     * Modifies the angle of the wind direction in the section
     *
     * @param windDirection: new angle of the wind direction
     */
    public void setWindDirection(double windDirection) {
        this.windDirection = windDirection;
    }

    /**
     * Modifies the wind speed in the section
     *
     * @param windSpeed: new wind speed
     */
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * Modifies the sequence of ordered segments in the section
     *
     * @param sequenceOfSegments: new sequence of segments
     */
    public void setSequenceOfSegments(List<Segment> sequenceOfSegments) {
        this.sequenceOfSegments = sequenceOfSegments;
    }

    /**
     * Returns a string with the details of the sequence of segments
     *
     * @return sequenceString
     */
    public String listSequenceOfSegments() {
        String sequenceString = " ";
        for (Segment segment : this.sequenceOfSegments) {
            sequenceString = sequenceString + "\n" + segment.toString();
        }
        return sequenceString;
    }
    
    /**
     * Returns a string with the details of the section
     *
     * @return sectionString
     */
    @Override
    public String toString() {
        String sequenceString = listSequenceOfSegments();
        String sectionString = "Beginning Node: " + this.beginningNode.toString()
                + "\nEnding Node: " + this.endingNode.toString()
                + "\nRoad: " + this.road
                + "\nTypology: " + this.typology
                + "\nDirection: " + this.direction
                + "\nNumber of Tolls: " + this.toll
                + "\nWind Direction: " + this.windDirection + "º"
                + "\nWind Speed: " + this.windSpeed + " m/s"
                + "\nSequence of Segments: " + sequenceString + "\n";
        return sectionString;
    }

    /**
     * Add a new segment to the section
     *
     * @param segment: new segment
     */
    public void addSegment(Segment segment) {
        segment.setSegm_index(this.sequenceOfSegments.size()+1);
        this.sequenceOfSegments.add(segment);
    }

    /**
     * Remove a segment from the section
     *
     * @param segmentIndex: index of the segment to remove
     * @return boolean variable that indicates the success of the removal
     */
    public boolean removeSegment(int segmentIndex) {
        boolean success = false;
        if (this.sequenceOfSegments.size() > 1) {
            this.sequenceOfSegments.remove(segmentIndex);
            success = true;
        }
        return success;
    }

    /**
     * Returns a segment given it's index
     *
     * @param segmentIndex: index of the segment
     * @return segment
     */
    public Segment getSegmentByIndex(int segmentIndex) {
        return this.sequenceOfSegments.get(segmentIndex);
    }

    /**
     * Returns the section's length
     * @return length
     */
    public double getSectionLength() {

        double length = 0;
        
        for (Segment s:sequenceOfSegments) {

            length += s.getLength();
        }
        return length;
    }

    /**
     *
     * @return
     */
    public Node[] getEndpoints() {
        Node[] endverts = new Node[2];
        endverts[0] = this.beginningNode;
        endverts[1] = this.endingNode;
        return endverts;
    }

    /**
     *
     * @param n1
     * @param n2
     * @return
     */
    public Section getSectionEdge2(Node n1, Node n2) {
        Node[] endverts = getEndpoints();
        if (endverts[0].equals(n1) && endverts[1].equals(n2)) {
            Section sec = new Section();
            sec.windSpeed = 4;
            return sec;
            //return this.toString();
        }
        return null;
    }

    /**
     *
     * @param vorig
     * @param vdest
     * @return
     */
    public Section getSectionEdge(Vertex<Node, Section> vorig, Vertex<Node, Section> vdest) {
        Node[] endverts = getEndpoints();
        if (endverts[0].equals(vorig.getElement()) && endverts[1].equals(vdest)) {
            return this;
        }
        return null;
    }

    /**
     * 
     * @param vorig
     * @param vdest
     * @return
     */
    public Edge< Node, Section> getEdge(Vertex<Node, Section> vorig, Vertex<Node, Section> vdest) {
        Node[] endverts = getEndpoints();
        if (endverts[0].equals(vorig) && endverts[1].equals(vdest)) {
            return vorig.getOutgoing().get(vdest);
        }
        return null;
    }
    
    /**
     * Returns the road where the section is situated
     * @return road
     */
    public String getRoad() {
        return this.road;
    }

    /**
     * Modifies the road where the section is situated
     * @param road: new road
     */
    public void setRoad(String road) {
        this.road = road;
    }
}
