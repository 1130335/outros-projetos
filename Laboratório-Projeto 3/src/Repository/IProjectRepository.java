package Repository;

import Model.Project;
import java.util.ArrayList;
import java.util.List;

/**
 * Interface relative to the ProjectRepository
 */
public interface IProjectRepository {
    
    /**
     * Declaration of the method getAllProjects
     * @return list with all projects in memory
     */
    public List<Project> getAllProjects();
    
    /**
     * Declaration of the method that adds a new project to the list
     * @param project: new project
     */
    public void addProject(Project project);
    
    /**
     * Declaration of the method that returns a project by its index in the list
     * @param index: project's index
     * @return project
     */
    public Project getProjectByIndex(int index);
    
    /**
     * Declaration of the method that returns a project by its name
     * @param name: project's name
     * @return project
     */
    public Project getProjectByName(String name);
        
    /**
     * Declaration of the method that returns a list with the names of all existing project
     * @return list with names
     */
    public List<String> getCurrentProjects();
    
    /**
     * Declaration of the method that verifies if a project name already exists
     * @param name: name of the project to test
     * @return verification
     */
    public boolean verifyName(String name);
}
