package Repository;

import Repository.IRepositoryFactory;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;

/**
 * Factory that creates all repositories
 */
public class RepositoryFactory implements IRepositoryFactory{
      
    /**
     * Creates and returns the project repository in memory
     * @return project repository in memory
     */
    @Override
    public ProjectRepositoryInMemory createProjectRepositoryInMemory() {
        return new ProjectRepositoryInMemory();
    }

    @Override
    public ProjectRepositoryDB createProjectRepositoryDB() {
        return new ProjectRepositoryDB();
    }

}
