package Repository;

/**
 * Class that creates the RepositoryFactory
 */
public final class Persistence {
    
    /**
     * Returns a new instance of the RepositoryFactory
     * @return new RepositoryFactory
     */
    public static RepositoryFactory getRepositoryFactory() {
        return new RepositoryFactory();
    }
    
}
