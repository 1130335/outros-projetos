package Repository;

import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;

/**
 * Interface of the RepositoryFactory
 */
public interface IRepositoryFactory {
    
    /**
     * Declaration of the method that creates a new in memory ProjectRepository
     * @return ProjectRepositoryInMemory
     */
    ProjectRepositoryInMemory createProjectRepositoryInMemory();
    ProjectRepositoryDB createProjectRepositoryDB();
}
