package Repository.InMemory;

import Model.Project;
import Model.RoadNetwork;
import Repository.IProjectRepository;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements all declarated methods in IProjectRepository
 */
public class ProjectRepositoryInMemory implements IProjectRepository {

    private static final List<Project> projects = new ArrayList<>();
    
    @Override
    public List<Project> getAllProjects() {
        return this.projects;
    }

    @Override
    public void addProject(Project project) {
        this.projects.add(project);
    }

    @Override
    public Project getProjectByIndex(int index) {
        return this.projects.get(index);
    }

    @Override
    public Project getProjectByName(String name) {
        for(Project p : this.projects) {
            if(p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    @Override
    public List<String> getCurrentProjects() {
        List<String> projectNames = new ArrayList<>();
        for(Project rn : getAllProjects()) {
            projectNames.add(rn.getName());
        }
        return projectNames;
    }
    
    @Override
    public boolean verifyName(String name) {
        boolean verified = true;
        List<String> tmp = getCurrentProjects();
        for(int i = 0; i< tmp.size(); i++) {
            if(name.equals(tmp.get(i))) {
                verified = false;
            }
        }
        return verified;
    }

}
