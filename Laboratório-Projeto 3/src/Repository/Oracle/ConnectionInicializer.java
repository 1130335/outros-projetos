package Repository.Oracle;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe used for inicialization of database's connection
 */
public class ConnectionInicializer {

    private static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";

    // Substituir URL com o URL da base de dados (Exemplo: jdbc:oracle:thin:@myhost:1521:orcl)
//    private static final String DB_URL = "jdbc:oracle:thin:i130335@//localhost:1521/xe";
//
//    private static final String USER = "i130335";
//    private static final String PASS = "qkLwbiip";
    
    private static final String DB_URL = "jdbc:oracle:thin:LAPR3_22@//gandalf.dei.isep.ipp.pt:1521/pdborcl";

    private static final String USER = "LAPR3_22";
    private static final String PASS = "qwerty22";
    
    

    private Connection connection;

    /**
     * Empty constructor
     */
    public ConnectionInicializer() {

    }

    /**
     * Inicializes the connection with the database
     *
     * @return connection
     */
    public Connection inicializeDBConnection() {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            this.connection = DriverManager.getConnection(DB_URL, USER, PASS);
            this.connection.setAutoCommit(false);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return this.connection;
    }

    /**
     * Closes the connection with the database
     */
    public void closeDBConnection() {
        try {
            this.connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public CallableStatement createCallableStatement(String callableString) {
        try {
            CallableStatement cs = this.connection.prepareCall(callableString);
            return cs;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public void closeCallableStatement(CallableStatement cs)  {
        try {
            cs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
    }

}
