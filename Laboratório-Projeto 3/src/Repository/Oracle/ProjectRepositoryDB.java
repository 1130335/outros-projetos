package Repository.Oracle;

import Model.Node;
import Model.Project;
import Model.RoadNetwork;
import Model.Section;
import Model.Section.Direction;
import Model.Section.Typology;
import Model.Segment;
import Model.Simulation;
import Model.Vehicles.Diesel;
import Model.Vehicles.Electric;
import Model.Vehicles.Energy;
import Model.Vehicles.Gasoline;
import Model.Vehicles.Gear;
import Model.Vehicles.Hydrogen;
import Model.Vehicles.Regime;
import Model.Vehicles.Throttle;
import Model.Vehicles.Vehicle;
import Model.Vehicles.VelocityLimit;
import Repository.IProjectRepository;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;

/**
 * Class that implements all the methods from the interface IProjectRepository
 * with connection to database
 */
public class ProjectRepositoryDB implements IProjectRepository {

    private final ConnectionInicializer init;
    private static List<Project> projects = new ArrayList<>();
    private Map<String, String> nodes = new HashMap<String, String>();

    /**
     * Constructor that instanciates the ConnectionInicializer object
     */
    public ProjectRepositoryDB() {
        this.init = new ConnectionInicializer();
        this.projects = getAllProjects();
    }

    @Override
    public List<Project> getAllProjects() {

        return null;
    }

    public int getProjectID(Project p) {
        try {
            int pID = 0;
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs = null;

            cs = this.init.createCallableStatement("{? = call getProjectID(?)}");
            cs.registerOutParameter(1, Types.INTEGER);
            cs.setString(2, p.getName());
            cs.executeUpdate();
            pID = cs.getInt(1);

            this.init.closeCallableStatement(cs);
            this.init.closeDBConnection();
            return pID;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public void addProject(Project project) {
        Connection connection = this.init.inicializeDBConnection();
        CallableStatement cs = null;
        int rn = 0;
        int projID = 0;

        try {
            cs = this.init.createCallableStatement("{? = call getRoadNetworkByID(?)}");
            cs.registerOutParameter(1, Types.INTEGER);
            cs.setString(2, project.getRoadNetwork().getName());
            cs.executeUpdate();
            rn = cs.getInt(1);
            this.init.closeCallableStatement(cs);
            
            cs = this.init.createCallableStatement("{call createNewProject(?,?, ?)}");
            cs.setInt(1, rn);
            cs.setString(2, project.getName());
            cs.setString(3, project.getDescription());
            cs.executeUpdate();
            this.init.closeCallableStatement(cs);
            
        } catch (SQLException ex) {
            try {
                cs = this.init.createCallableStatement("{call createNewRoadNetwork(?,?)}");
                cs.setString(1, project.getRoadNetwork().getName());
                cs.setString(2, project.getRoadNetwork().getDescription());
                cs.executeUpdate();
                this.init.closeCallableStatement(cs);

                cs = this.init.createCallableStatement("{? = call getRoadNetworkByID(?)}");
                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, project.getRoadNetwork().getName());
                cs.executeUpdate();
                rn = cs.getInt(1);
                this.init.closeCallableStatement(cs);

                cs = this.init.createCallableStatement("{call createNewProject(?,?, ?)}");
                cs.setInt(1, rn);
                cs.setString(2, project.getName());
                cs.setString(3, project.getDescription());
                cs.executeUpdate();
                this.init.closeCallableStatement(cs);

                cs = this.init.createCallableStatement("{? = call getProjectID(?)}");
                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, project.getName());
                cs.executeUpdate();
                int pID = cs.getInt(1);
                this.init.closeCallableStatement(cs);

                Simulation s1 = new Simulation();
                s1.setName("Test" + pID);
                s1.setDescription("Test" + pID);

                cs = this.init.createCallableStatement("{call createSimulation(?,?,?)}");
                cs.setString(1, s1.getName());
                cs.setString(2, s1.getDescription());
                cs.setInt(3, pID);
                cs.executeUpdate();
                this.init.closeCallableStatement(cs);

                List<Node> tmpNodes = project.getRoadNetwork().getNodesList();
                cs = this.init.createCallableStatement("{call createNewNode(?,?)}");
                for (Node n : tmpNodes) {
                    cs.setString(1, n.getNodeID());
                    cs.setInt(2, rn);
                    cs.executeUpdate();
                }
                this.init.closeCallableStatement(cs);

                List<Section> tmpSections = project.getRoadNetwork().getSections();
                for (Section s : tmpSections) {
                    cs = this.init.createCallableStatement("{? = call getNodeByID(?,?)}");
                    cs.registerOutParameter(1, Types.INTEGER);
                    cs.setString(2, s.getBeginningNode().getNodeID());
                    cs.setInt(3, rn);
                    cs.executeUpdate();
                    int bNode = cs.getInt(1);
                    this.init.closeCallableStatement(cs);

                    cs = this.init.createCallableStatement("{? = call getNodeByID(?,?)}");
                    cs.registerOutParameter(1, Types.INTEGER);
                    cs.setString(2, s.getEndingNode().getNodeID());
                    cs.setInt(3, rn);
                    cs.executeUpdate();
                    int eNode = cs.getInt(1);
                    this.init.closeCallableStatement(cs);

                    cs = this.init.createCallableStatement("{? = call getTypologyByID(?)}");
                    cs.registerOutParameter(1, Types.INTEGER);
                    String typeAux = s.getTipology().toString();
                    cs.setString(2, typeAux);
                    cs.executeUpdate();
                    int type = cs.getInt(1);
                    this.init.closeCallableStatement(cs);

                    cs = this.init.createCallableStatement("{? = call getDirectionByID(?)}");
                    cs.registerOutParameter(1, Types.INTEGER);
                    String directionAux = s.getDirection().toString();
                    cs.setString(2, directionAux);
                    cs.executeUpdate();
                    int direction = cs.getInt(1);
                    this.init.closeCallableStatement(cs);

                    cs = this.init.createCallableStatement("{call createNewSection(?,?,?,?,?,?,?,?,?)}");
                    cs.setInt(1, bNode);
                    cs.setInt(2, eNode);
                    cs.setInt(3, type);
                    cs.setInt(4, direction);
                    cs.setInt(5, s.getToll());
                    cs.setDouble(6, s.getWindDirection());
                    cs.setDouble(7, s.getWindSpeed());
                    cs.setInt(8, rn);
                    cs.setString(9, s.getRoad());
                    cs.executeUpdate();
                    this.init.closeCallableStatement(cs);

                    List<Segment> tmpSegments = s.getSequenceOfSegments();
                    cs = this.init.createCallableStatement("{? = call getSectionByID(?)}");
                    cs.registerOutParameter(1, Types.INTEGER);
                    cs.setInt(2, rn);
                    cs.executeUpdate();
                    int section = cs.getInt(1);
                    this.init.closeCallableStatement(cs);

                    for (Segment seg : tmpSegments) {
                        cs = this.init.createCallableStatement("{call createNewSegment(?,?,?,?,?,?,?,?,?)}");
                        cs.setInt(1, seg.getSegm_index());
                        cs.setDouble(2, seg.getInitialHeigh());
                        cs.setDouble(3, seg.getSlope());
                        cs.setDouble(4, seg.getLength());
                        cs.setDouble(5, seg.getRollResistCoef());
                        cs.setInt(6, seg.getMaxNumOfVehicles());
                        cs.setDouble(7, seg.getMinVelocity());
                        cs.setDouble(8, seg.getMaxVelocity());
                        cs.setInt(9, section);
                        cs.executeUpdate();
                        this.init.closeCallableStatement(cs);
                    }
                }

            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }

        }
        
        if (!project.getListOfVehicles().isEmpty()) {
            try {
                cs = this.init.createCallableStatement("{? = call getProjectID(?)}");
                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, project.getName());
                cs.executeUpdate();
                projID = cs.getInt(1);
                this.init.closeCallableStatement(cs);

                for (Vehicle v : project.getListOfVehicles()) {
                    addVehicle(v, projID);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        this.init.closeDBConnection();
        System.out.println("Project loaded in the DB!");
    }

    @Override
    public Project getProjectByIndex(int index
    ) {
        return null;
    }

    @Override
    public Project getProjectByName(String name) {
        Project project = new Project();
        project.setActive();
        RoadNetwork road_network = new RoadNetwork();
        int rnID = 0;
        int pID = 0;

        try {

            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getProjectByName(?)}");
            ResultSet rs1 = null;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setString(2, name);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                project.setName(name);
                project.setDescription(rs1.getString("PROJECTDESCRIPTION"));
                rnID = rs1.getInt("ROADNETWORK");
                pID = rs1.getInt("PROJECT_ID");
            }
            this.init.closeCallableStatement(cs1);

            project.setRoadNetwork(getRoadNetwork(rnID));
            project.setListOfVehicles(getListVehicles(pID));

            this.init.closeDBConnection();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return project;
    }

    @Override
    public List<String> getCurrentProjects() {
        try {
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs = null;
            List<String> projectNames = new ArrayList<>();
            ResultSet rs = null;
            cs = this.init.createCallableStatement("{? = call allProjects}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {
                projectNames.add(rs.getString("PROJECTNAME"));
            }
            this.init.closeCallableStatement(cs);
            this.init.closeDBConnection();
            return projectNames;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean verifyName(String name
    ) {
        boolean verified = true;
        List<String> tmp = getCurrentProjects();
        for (int i = 0; i < tmp.size(); i++) {
            if (name.equals(tmp.get(i))) {
                verified = false;
            }
        }
        return verified;
    }

    public RoadNetwork getRoadNetwork(int id) {
        try {
            RoadNetwork rn = new RoadNetwork();
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getRoadNetwork(?)}");
            ResultSet rs1 = null;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, id);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                rn.setName(rs1.getString("ROADNETWORKNAME"));
                rn.setDescription(rs1.getString("ROADNETWORKDESCRIPTION"));
            }
            this.init.closeCallableStatement(cs1);
            List<Section> sections = getSections(id);
            List<Node> nodes = getNodes(id);
            List<Node> finalNodes = new ArrayList<>();
            int i = 0;
            for (String bn : this.nodes.keySet()) {
                for (int j = 0; j < nodes.size(); j++) {
                    if (nodes.get(j).getNodeID().equalsIgnoreCase(bn)) {
                        sections.get(i).setBeginningNode(nodes.get(j));
                        nodes.get(j).addSectionIn(sections.get(i));
                    }
                }
                i++;
            }

            i = 0;
            for (String en : this.nodes.values()) {
                for (int j = 0; j < nodes.size(); j++) {
                    if (nodes.get(j).getNodeID().equalsIgnoreCase(en)) {
                        sections.get(i).setEndingNode(nodes.get(j));
                        nodes.get(j).addSectionOut(sections.get(i));
                    }
                }
                i++;
            }

            rn.setNodesList(nodes);
            rn.setSections(sections);

            return rn;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<Section> getSections(int id) {
        try {
            List<Section> sections = new ArrayList<>();
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getSections(?)}");
            ResultSet rs1 = null;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, id);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                Section sec = new Section();
                sec.setToll(rs1.getInt("TOLL"));
                sec.setRoad(rs1.getString("ROAD"));
                sec.setWindDirection(rs1.getDouble("WINDDIRECTION"));
                sec.setWindSpeed(rs1.getDouble("WINDSPEED"));
                int dirID = rs1.getInt("DIRECTION");
                int typeID = rs1.getInt("TYPOLOGY");

                CallableStatement cs2 = this.init.createCallableStatement("{? = call getTypology(?)}");
                ResultSet rs2 = null;
                cs2.registerOutParameter(1, OracleTypes.CURSOR);
                cs2.setInt(2, typeID);
                cs2.executeUpdate();
                rs2 = (ResultSet) cs2.getObject(1);
                while (rs2.next()) {
                    String type = rs2.getString("DESCRIPTION");
                    if (type.equalsIgnoreCase("CONTROLLED_ACCESS_HIGHWAY")) {
                        sec.setTypology(Typology.CONTROLLED_ACCESS_HIGHWAY);
                    } else if (type.equalsIgnoreCase("EXPRESS_ROAD")) {
                        sec.setTypology(Typology.EXPRESS_ROAD);
                    } else if (type.equalsIgnoreCase("REGULAR_ROAD")) {
                        sec.setTypology(Typology.REGULAR_ROAD);
                    } else {
                        sec.setTypology(Typology.URBAN_ROAD);
                    }
                }
                this.init.closeCallableStatement(cs2);

                cs2 = this.init.createCallableStatement("{? = call getDirection(?)}");
                cs2.registerOutParameter(1, OracleTypes.CURSOR);
                cs2.setInt(2, dirID);
                cs2.executeUpdate();
                rs2 = (ResultSet) cs2.getObject(1);
                while (rs2.next()) {
                    String direction = rs2.getString("DESCRIPTION");
                    if (direction.equalsIgnoreCase("BIDIRECTIONAL")) {
                        sec.setDirection(Direction.BIDIRECTIONAL);
                    } else if (direction.equalsIgnoreCase("DIRECT")) {
                        sec.setDirection(Direction.DIRECT);
                    } else {
                        sec.setDirection(Direction.REVERSE);
                    }
                }
                this.init.closeCallableStatement(cs2);

                int idBN = rs1.getInt("BEGINNINGNODE");
                int idEN = rs1.getInt("ENDINGNODE");
                cs2 = this.init.createCallableStatement("{? = call getNodeName(?)}");
                cs2.registerOutParameter(1, Types.VARCHAR);
                cs2.setInt(2, idBN);
                cs2.executeUpdate();

                CallableStatement cs3 = this.init.createCallableStatement("{? = call getNodeName(?)}");
                cs3.registerOutParameter(1, Types.VARCHAR);
                cs3.setInt(2, idEN);
                cs3.executeUpdate();

                this.nodes.put(cs2.getString(1), cs3.getString(1));

                this.init.closeCallableStatement(cs2);
                this.init.closeCallableStatement(cs3);

                List<Segment> segments = getSegments(rs1.getInt("SECTION_ID"));
                sec.setSequenceOfSegments(segments);
                sections.add(sec);
            }
            return sections;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public List<Segment> getSegments(int id) {
        try {
            List<Segment> segments = new ArrayList<>();

            ResultSet rs1 = null;
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getSegments(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, id);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                Segment seg = new Segment();
                seg.setInitialHeigh(rs1.getDouble("INICIALHEIGHT"));
                seg.setLength(rs1.getDouble("SEGMENT_INDEX"));
                seg.setMaxNumOfVehicles(rs1.getInt("MAXNUMOFVEHICLES"));
                seg.setMaxVelocity(rs1.getDouble("MAXVELOCITY"));
                seg.setMinVelocity(rs1.getDouble("MINVELOCITY"));
                seg.setRollResistCoef(rs1.getDouble("ROLLRESISTCOEF"));
                seg.setSlope(rs1.getDouble("SLOPE"));
                segments.add(seg);
            }
            this.init.closeCallableStatement(cs1);

            return segments;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<Node> getNodes(int rnID) {
        try {
            List<Node> nodes = new ArrayList<>();
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getNodes(?)}");
            ResultSet rs1 = null;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, rnID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                Node n = new Node();
                n.setNodeId(rs1.getString("NODE_NAME"));
                nodes.add(n);
            }
            this.init.closeCallableStatement(cs1);
            return nodes;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void addVehicle(Vehicle v, int projID) {
        try {
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs1 = null;
            int vID = 0;
            cs1 = this.init.createCallableStatement("{? = call getVehicleID(?)}");
            cs1.registerOutParameter(1, Types.INTEGER);
            cs1.setString(2, v.getName());
            cs1.executeUpdate();
            vID = cs1.getInt(1);
            this.init.closeCallableStatement(cs1);
            
            cs1 = this.init.createCallableStatement("{call associateVehicleProject(?,?)}");
            cs1.setInt(1, vID);
            cs1.setInt(2, projID);
            cs1.executeUpdate();
            this.init.closeCallableStatement(cs1);

        } catch (SQLException ex) {
            try {
                CallableStatement cs1 = null;
                cs1 = this.init.createCallableStatement("{call createNewEnergy(?,?,?,?)}");
                cs1.setDouble(1, v.getEnergy().getMinRpm());
                cs1.setDouble(2, v.getEnergy().getMaxRpm());
                cs1.setDouble(3, v.getEnergy().getFinalDriveRatio());
                cs1.setDouble(4, v.getEnergy().getEnergyRegenerationRatio());
                cs1.executeUpdate();
                this.init.closeCallableStatement(cs1);

                int energyID = 0;
                cs1 = this.init.createCallableStatement("{? = call getEnergyID}");
                cs1.registerOutParameter(1, Types.INTEGER);
                cs1.executeUpdate();
                energyID = cs1.getInt(1);
                this.init.closeCallableStatement(cs1);

                int vehicleTypeID = 0;
                cs1 = this.init.createCallableStatement("{? = call getVehicleTypeID(?)}");
                cs1.registerOutParameter(1, Types.INTEGER);
                cs1.setString(2, v.getType().toString().toUpperCase());
                cs1.executeUpdate();
                vehicleTypeID = cs1.getInt(1);
                this.init.closeCallableStatement(cs1);

                int vehicleID = 0;
                if (v.getMotorization().toString().equalsIgnoreCase("Electric")) {
                    cs1 = this.init.createCallableStatement("{call createNewElectricVehicle(?,?,?,?,?,?,?,?,?,?,?)}");
                    cs1.setInt(1, vehicleTypeID);
                    cs1.setDouble(2, v.getMass());
                    cs1.setDouble(3, v.getLoad());
                    cs1.setDouble(4, v.getDragCoefficient());
                    cs1.setString(5, v.getName());
                    cs1.setString(6, v.getDescription());
                    cs1.setDouble(7, v.getRrc());
                    cs1.setDouble(8, v.getWheelSize());
                    cs1.setInt(9, energyID);
                    cs1.setDouble(10, v.getFrontalArea());
                    cs1.setInt(11, projID);
                    cs1.executeUpdate();
                    this.init.closeCallableStatement(cs1);

                    cs1 = this.init.createCallableStatement("{? = call getVehicleID(?)}");
                    cs1.registerOutParameter(1, Types.INTEGER);
                    cs1.setString(2, v.getName());
                    cs1.executeUpdate();
                    vehicleID = cs1.getInt(1);
                    this.init.closeCallableStatement(cs1);

                } else if (v.getMotorization().toString().equalsIgnoreCase("Diesel") || v.getMotorization().toString().equalsIgnoreCase("Gasoline") || v.getMotorization().toString().equalsIgnoreCase("Hydrogen")) {
                    int combustionID = 0;
                    cs1 = this.init.createCallableStatement("{? = call getCombustionID(?)}");
                    cs1.registerOutParameter(1, Types.INTEGER);
                    cs1.setString(2, v.getMotorization().toString());
                    cs1.executeUpdate();
                    combustionID = cs1.getInt(1);
                    this.init.closeCallableStatement(cs1);

                    cs1 = this.init.createCallableStatement("{call createNewCombustionVehicle(?,?,?,?,?,?,?,?,?,?,?,?)}");
                    cs1.setInt(1, vehicleTypeID);
                    cs1.setDouble(2, v.getMass());
                    cs1.setDouble(3, v.getLoad());
                    cs1.setDouble(4, v.getDragCoefficient());
                    cs1.setString(5, v.getName());
                    cs1.setString(6, v.getDescription());
                    cs1.setDouble(7, v.getRrc());
                    cs1.setDouble(8, v.getWheelSize());
                    cs1.setInt(9, energyID);
                    cs1.setDouble(10, v.getFrontalArea());
                    cs1.setInt(11, combustionID);
                    cs1.setInt(12, projID);
                    cs1.executeUpdate();
                    this.init.closeCallableStatement(cs1);

                    cs1 = this.init.createCallableStatement("{? = call getVehicleID(?)}");
                    cs1.registerOutParameter(1, Types.INTEGER);
                    cs1.setString(2, v.getName());
                    cs1.executeUpdate();
                    vehicleID = cs1.getInt(1);
                    this.init.closeCallableStatement(cs1);
                }

                for (Gear g : v.getEnergy().getGearList()) {
                    cs1 = this.init.createCallableStatement("{call createNewGear(?,?,?)}");
                    cs1.setDouble(1, g.getRatio());
                    cs1.setInt(2, energyID);
                    System.out.println(energyID);
                    cs1.setInt(3, vehicleID);
                    System.out.println(vehicleID);
                    cs1.executeUpdate();
                    this.init.closeCallableStatement(cs1);
                }

                for (Throttle t : v.getEnergy().getThrottleList()) {
                    cs1 = this.init.createCallableStatement("{call createNewThrottle(?)}");
                    cs1.setInt(1, energyID);
                    cs1.executeUpdate();
                    this.init.closeCallableStatement(cs1);

                    int tID = 0;
                    cs1 = this.init.createCallableStatement("{? = call getThrottleID}");
                    cs1.registerOutParameter(1, Types.INTEGER);
                    cs1.executeUpdate();
                    tID = cs1.getInt(1);
                    this.init.closeCallableStatement(cs1);

                    for (Regime r : t.getRegimeList()) {
                        cs1 = this.init.createCallableStatement("{call createNewRegime(?,?,?,?,?)}");
                        cs1.setDouble(1, r.getTorque());
                        cs1.setDouble(2, r.getRpmLow());
                        cs1.setDouble(3, r.getRpmHigh());
                        cs1.setDouble(4, r.getSfc());
                        cs1.setInt(5, tID);
                        cs1.executeUpdate();
                        this.init.closeCallableStatement(cs1);
                    }
                }
                if (!v.getLimitList().isEmpty()) {
                    for (VelocityLimit vl : v.getLimitList()) {
                        int typeID = 0;
                        cs1 = this.init.createCallableStatement("{? = call getTypologyByName(?)}");
                        cs1.registerOutParameter(1, Types.INTEGER);
                        cs1.setString(2, vl.getSectionType().toString());
                        cs1.executeUpdate();
                        typeID = cs1.getInt(1);
                        this.init.closeCallableStatement(cs1);

                        cs1 = this.init.createCallableStatement("{call createNewVelocityLimit(?,?,?)}");
                        cs1.setInt(1, typeID);
                        cs1.setDouble(2, vl.getSpeedLimit());
                        cs1.setInt(3, vehicleID);
                        cs1.executeUpdate();
                        this.init.closeCallableStatement(cs1);
                    }
                }

                System.out.println("Vehicle loaded into DB!");
                this.init.closeDBConnection();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }
        }
    }

    public List<Vehicle> getListVehicles(int pID) {
        try {
            List<Vehicle> vehicles = new ArrayList<>();
            ResultSet rs1 = null;
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getVehicleIDs(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, pID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                CallableStatement cs2 = this.init.createCallableStatement("{? = call getVehicles(?)}");
                ResultSet rs2 = null;
                cs2.registerOutParameter(1, OracleTypes.CURSOR);
                cs2.setInt(2, rs1.getInt("VEHICLE"));
                cs2.executeUpdate();
                rs2 = (ResultSet) cs2.getObject(1);
                while (rs2.next()) {
                    Vehicle v = new Vehicle();
                    v.setDescription(rs2.getString("DESCRIPTION"));
                    v.setDragCoefficient(rs2.getDouble("DRAGCOEFFICIENT"));
                    v.setEnergy(getEnergy(rs2.getInt("ENERGY")));
                    v.setFrontalArea(rs2.getDouble("FRONTALAREA"));
                    v.setLimitList(getVelocityLimits(rs2.getInt("VEHICLE_ID")));
                    v.setLoad(rs2.getDouble("LOAD"));
                    v.setMass(rs2.getDouble("MASS"));
                    v.setName(rs2.getString("NAME"));
                    v.setRrc(rs2.getDouble("RRC"));
                    v.setWheelSize(rs2.getDouble("WHEELSIZE"));
                    int vtype = rs2.getInt("VEHICLETYPE");

                    CallableStatement cs3 = null;
                    cs3 = this.init.createCallableStatement("{? = call getVehicleType(?)}");
                    cs3.registerOutParameter(1, Types.VARCHAR);
                    cs3.setInt(2, vtype);
                    cs3.executeUpdate();
                    String type = cs3.getString(1);
                    this.init.closeCallableStatement(cs3);

                    if (type.equalsIgnoreCase("CAR")) {
                        v.setType(Vehicle.VehicleType.CAR);
                    } else if (type.equalsIgnoreCase("TRUCK")) {
                        v.setType(Vehicle.VehicleType.TRUCK);
                    } else if (type.equalsIgnoreCase("TRACTOR")) {
                        v.setType(Vehicle.VehicleType.TRACTOR);
                    } else if (type.equalsIgnoreCase("MOTORCYCLE")) {
                        v.setType(Vehicle.VehicleType.MOTORCYCLE);
                    } else {
                        v.setType(Vehicle.VehicleType.SEMITRAILER);
                    }

                    int vID = rs2.getInt("VEHICLE_ID");
                    int mID = 0;
                    try {
                        cs3 = this.init.createCallableStatement("{? = call getElectricMotorizationID(?)}");
                        cs3.registerOutParameter(1, Types.INTEGER);
                        cs3.setInt(2, vID);
                        cs3.executeUpdate();
                        mID = cs3.getInt(1);
                        this.init.closeCallableStatement(cs3);

                        if (mID != 0) {
                            v.setMotorization(new Electric());
                        }

                    } catch (SQLException ex) {
                        String motorization = null;
                        /* Assumindo que não existem carros híbridos */
                        cs3 = this.init.createCallableStatement("{? = call getCombustionMotorizationID(?)}");
                        cs3.registerOutParameter(1, Types.INTEGER);
                        cs3.setInt(2, vID);
                        cs3.executeUpdate();
                        mID = cs3.getInt(1);
                        this.init.closeCallableStatement(cs3);

                        cs3 = this.init.createCallableStatement("{? = call getCombustionMotorization(?)}");
                        cs3.registerOutParameter(1, Types.VARCHAR);
                        cs3.setInt(2, mID);
                        cs3.executeUpdate();
                        motorization = cs3.getString(1);
                        this.init.closeCallableStatement(cs3);

                        if (motorization.equalsIgnoreCase("Diesel")) {
                            v.setMotorization(new Diesel());
                        } else if (motorization.equalsIgnoreCase("Gasoline")) {
                            v.setMotorization(new Gasoline());
                        } else {
                            v.setMotorization(new Hydrogen());
                        }
                    }
                    vehicles.add(v);
                }
                this.init.closeCallableStatement(cs2);

            }
            this.init.closeCallableStatement(cs1);

            return vehicles;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Energy getEnergy(int eID) {
        try {
            Energy e = new Energy();
            CallableStatement cs1 = null;
            ResultSet rs1 = null;
            cs1 = this.init.createCallableStatement("{? = call getEnergy(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, eID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                e.setEnergyRegenerationRatio(rs1.getDouble("ENERGYREGENERATIONRATIO"));
                e.setMaxRpm(rs1.getDouble("MAXRPM"));
                e.setMinRpm(rs1.getDouble("MINRPM"));
                e.setFinalDriveRatio(rs1.getDouble("FINALDRIVERATIO"));
                e.setGearList(getGears(eID));
                e.setThrottleList(getThrottles(eID));
            }

            this.init.closeCallableStatement(cs1);
            return e;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<Gear> getGears(int eID) {
        try {
            ArrayList<Gear> gears = new ArrayList<>();
            CallableStatement cs1 = null;
            ResultSet rs1 = null;

            cs1 = this.init.createCallableStatement("{? = call getGears(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, eID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                Gear g = new Gear();
                g.setRatio(rs1.getDouble("RATIO"));
                gears.add(g);
            }

            this.init.closeCallableStatement(cs1);

            return gears;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<Throttle> getThrottles(int eID) {
        try {
            ArrayList<Throttle> throttles = new ArrayList<>();
            CallableStatement cs1 = null;
            ResultSet rs1 = null;

            cs1 = this.init.createCallableStatement("{? = call getThrottles(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, eID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                Throttle t = new Throttle();
                t.setRegimeList(getRegimes(rs1.getInt("THROTTLE_ID")));
                throttles.add(t);
            }

            this.init.closeCallableStatement(cs1);

            return throttles;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<Regime> getRegimes(int tID) {
        try {
            ArrayList<Regime> regimes = new ArrayList<>();
            CallableStatement cs1 = null;
            ResultSet rs1 = null;

            cs1 = this.init.createCallableStatement("{? = call getRegimes(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, tID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                Regime r = new Regime();
                r.setRpmHigh(rs1.getDouble("RPMHIGH"));
                r.setRpmLow(rs1.getDouble("RPMLOW"));
                r.setSfc(rs1.getDouble("SFC"));
                r.setTorque(rs1.getDouble("TORQUE"));
                regimes.add(r);
            }

            this.init.closeCallableStatement(cs1);

            return regimes;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<VelocityLimit> getVelocityLimits(int vID) {
        try {
            List<VelocityLimit> limits = new ArrayList<>();
            CallableStatement cs1 = null;
            ResultSet rs1 = null;

            cs1 = this.init.createCallableStatement("{? = call getLimits(?)}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setInt(2, vID);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);

            while (rs1.next()) {
                VelocityLimit vl = new VelocityLimit();
                vl.setSpeedLimit(rs1.getDouble("SPEEDLIMIT"));

                CallableStatement cs2 = this.init.createCallableStatement("{? = call getTypologyName(?)}");
                cs2.registerOutParameter(1, Types.VARCHAR);
                cs2.setInt(2, rs1.getInt("SECTION_TYPE"));
                cs2.executeUpdate();
                String type = cs2.getString(1);
                this.init.closeCallableStatement(cs2);

                if (type.equalsIgnoreCase("CONTROLLED_ACCESS_HIGHWAY")) {
                    vl.setSectionType(Typology.CONTROLLED_ACCESS_HIGHWAY);
                } else if (type.equalsIgnoreCase("EXPRESS_ROAD")) {
                    vl.setSectionType(Typology.EXPRESS_ROAD);
                } else if (type.equalsIgnoreCase("REGULAR_ROAD")) {
                    vl.setSectionType(Typology.REGULAR_ROAD);
                } else {
                    vl.setSectionType(Typology.URBAN_ROAD);
                }
                limits.add(vl);
            }

            this.init.closeCallableStatement(cs1);
            return limits;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void updateProject(String name, String description, String oldName) {
        try {
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs1 = null;
            cs1 = this.init.createCallableStatement("{? = call getProjectByName(?)}");
            ResultSet rs1 = null;
            int pID = 0;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setString(2, oldName);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                pID = rs1.getInt("PROJECT_ID");
            }
            this.init.closeCallableStatement(cs1);

            cs1 = this.init.createCallableStatement("{call updateProject(?,?,?)}");
            cs1.setString(1, name);
            cs1.setString(2, description);
            cs1.setInt(3, pID);
            cs1.executeUpdate();
            this.init.closeCallableStatement(cs1);

            this.init.closeDBConnection();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<String> getCurrentSimulations() {
        try {
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs = null;
            List<String> simNames = new ArrayList<>();
            ResultSet rs = null;
            cs = this.init.createCallableStatement("{? = call allSimulations}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {
                simNames.add(rs.getString("SIMNAME"));
            }
            this.init.closeCallableStatement(cs);
            this.init.closeDBConnection();
            return simNames;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Simulation getSimulationByName(String name) {
        try {
            Simulation s = new Simulation();
            int pID = 0;
            String pName = "";
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs1 = this.init.createCallableStatement("{? = call getSimulationByName(?)}");
            ResultSet rs1 = null;
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.setString(2, name);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                s.setName(name);
                s.setDescription(rs1.getString("SIMDESCRIPTION"));
                pID = rs1.getInt("PROJECT");
            }
            this.init.closeCallableStatement(cs1);

            cs1 = this.init.createCallableStatement("{? = call allProjects}");
            cs1.registerOutParameter(1, OracleTypes.CURSOR);
            cs1.executeUpdate();
            rs1 = (ResultSet) cs1.getObject(1);
            while (rs1.next()) {
                if (pID == rs1.getInt("PROJECT_ID")) {
                    pName = rs1.getString("PROJECTNAME");
                }
            }
            this.init.closeCallableStatement(cs1);
            this.init.closeDBConnection();
            Project p = new Project();
            p = getProjectByName(pName);
            s.setActiveProject(p);
            return s;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int getProjectSimulationID(String oldName) {
        try {
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs = null;
            ResultSet rs = null;
            cs = this.init.createCallableStatement("{? = call allSimulations}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {
                if (oldName.equalsIgnoreCase(rs.getString("SIMNAME"))) {
                    return rs.getInt("PROJECT");
                }
            }
            this.init.closeCallableStatement(cs);
            this.init.closeDBConnection();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public void addSimulation(Simulation s, String oldName) {
        try {
            int pID = getProjectSimulationID(oldName);
            Connection connection = this.init.inicializeDBConnection();
            CallableStatement cs = null;
            cs = this.init.createCallableStatement("{call createSimulation(?,?,?)}");
            cs.setString(1, s.getName());
            cs.setString(2, s.getDescription());
            cs.setInt(3, pID);
            cs.executeUpdate();
            this.init.closeCallableStatement(cs);
            this.init.closeDBConnection();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
