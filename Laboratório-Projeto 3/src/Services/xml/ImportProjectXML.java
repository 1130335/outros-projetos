/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

import Controller.P02_CreateProjectController;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Class responsible for reading a xml file in this application context.
 * Provides the information found to the CreateProjectController helping in Project and RoadNetwork creation.
 */
public class ImportProjectXML {

    /**
     * Creates a SAX parser which reads from a XML file "xmlFile" and passes the
     * information found to the CreateProjectController controller through an HashMap.
     *
     * @param cpc
     * @param xmlFile
     */
    public static void readXML(P02_CreateProjectController cpc, File xmlFile) {
        
        Map parametros = new HashMap();

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean network = false;
                boolean node_list = false;
                boolean node = false;
                boolean section_list = false;
                boolean road_section = false;
                boolean road = false;
                boolean typology = false;
                boolean direction = false;
                boolean toll = false;
                boolean wind_direction = false;
                boolean wind_speed = false;
                boolean segment_list = false;
                boolean segment = false;
                boolean height = false;
                boolean slope = false;
                boolean s_length = false;
                boolean rrc = false;
                boolean max_velocity = false;
                boolean min_velocity = false;
                boolean number_vehicles = false;

                /**
                 * Each time the parser encounters the beginning of a new
                 * element, it calls this method, which resets the string buffer
                 * and puts the attributes found in specific tags into the
                 * HashMap.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @param attributes
                 * @throws SAXException
                 */
                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("NETWORK")) {
                        network = true;

                        parametros.put("id", attributes.getValue("id"));
                        parametros.put("description", attributes.getValue("description"));

                        cpc.setNetwork(parametros);
                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("NODE_LIST")) {
                        node_list = true;
                    } else if (qName.equalsIgnoreCase("NODE")) {
                        node = true;

                        cpc.addNode(attributes.getValue("id"));
                    } else if (qName.equalsIgnoreCase("SECTION_LIST")) {
                        section_list = true;
                    } else if (qName.equalsIgnoreCase("ROAD_SECTION")) {
                        road_section = true;

                        parametros.put("end", attributes.getValue("end"));
                        parametros.put("begin", attributes.getValue("begin"));
                    } else if (qName.equalsIgnoreCase("ROAD")) {
                        road = true;
                    } else if (qName.equalsIgnoreCase("TYPOLOGY")) {
                        typology = true;
                    } else if (qName.equalsIgnoreCase("DIRECTION")) {
                        direction = true;
                    } else if (qName.equalsIgnoreCase("TOLL")) {
                        toll = true;
                    } else if (qName.equalsIgnoreCase("WIND_DIRECTION")) {
                        wind_direction = true;
                    } else if (qName.equalsIgnoreCase("WIND_SPEED")) {
                        wind_speed = true;
                    } else if (qName.equalsIgnoreCase("SEGMENT_LIST")) {
                        segment_list = true;

                        cpc.addRoadSection(parametros);
                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("SEGMENT")) {
                        segment = true;

                        parametros.put("id", attributes.getValue("id"));
                    } else if (qName.equalsIgnoreCase("HEIGHT")) {
                        height = true;
                    } else if (qName.equalsIgnoreCase("SLOPE")) {
                        slope = true;
                    } else if (qName.equalsIgnoreCase("LENGTH")) {
                        s_length = true;
                    } else if (qName.equalsIgnoreCase("RRC")) {
                        rrc = true;
                    } else if (qName.equalsIgnoreCase("MAX_VELOCITY")) {
                        max_velocity = true;
                    } else if (qName.equalsIgnoreCase("MIN_VELOCITY")) {
                        min_velocity = true;
                    } else if (qName.equalsIgnoreCase("NUMBER_VEHICLES")) {
                        number_vehicles = true;
                    }
                }

                /**
                 * Each time the parser encounters the end of a new element, it
                 * calls this method, which is used to clear the HashMap after
                 * collecting certain information and finding specific end tags.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @throws SAXException
                 */
                public void endElement(String uri, String localName,
                        String qName)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("NETWORK")) {

                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("SEGMENT")) {

                        cpc.addSegment(parametros);

                        parametros.clear();
                    }

                }

                /**
                 * whenever the parser encounters plain text (not XML elements),
                 * it calls this method, which accumulates them in a string
                 * buffer. This string is then added into the HashMap.
                 *
                 * @param ch
                 * @param start
                 * @param length
                 * @throws SAXException
                 */
                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (section_list) {
                        section_list = false;
                    } else if (road_section) {
                        road_section = false;
                    } else if (road) {
                        
                        parametros.put("road", new String(ch, start, length));
                        road = false;
                    } else if (typology) {
                        parametros.put("typology", new String(ch, start, length));
                        typology = false;
                    } else if (direction) {
                        parametros.put("direction", new String(ch, start, length));
                        direction = false;
                    } else if (toll) {
                        parametros.put("toll", new String(ch, start, length));
                        toll = false;
                    } else if (wind_direction) {
                        parametros.put("wind_direction", new String(ch, start, length));
                        wind_direction = false;
                    } else if (wind_speed) {
                        parametros.put("wind_speed", new String(ch, start, length));
                        wind_speed = false;
                    } else if (segment) {
                        segment = false;
                    } else if (height) {
                        parametros.put("height", new String(ch, start, length));
                        height = false;
                    } else if (slope) {
                        parametros.put("slope", new String(ch, start, length));
                        slope = false;
                    } else if (s_length) {
                        parametros.put("length", new String(ch, start, length));
                        s_length = false;
                    } else if (rrc) {
                        parametros.put("rrc", new String(ch, start, length));
                        rrc = false;
                    } else if (max_velocity) {
                        parametros.put("max_velocity", new String(ch, start, length));
                        max_velocity = false;
                    } else if (min_velocity) {
                        parametros.put("min_velocity", new String(ch, start, length));
                        min_velocity = false;
                    } else if (number_vehicles) {
                        parametros.put("number_vehicles", new String(ch, start, length));
                        number_vehicles = false;
                    }

                }

            };

            InputStream inputStream = new FileInputStream(xmlFile);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
