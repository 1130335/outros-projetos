/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

import Controller.CreateVehicleListController;
import Controller.MainController;
import Controller.P02_CreateProjectController;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Class responsible for reading a xml file in this application context.
 * Provides the information found to the CreateProjectController helping in
 * Project and RoadNetwork creation.
 */
public class ImportVehicleXML {

    /**
     * Creates a SAX parser which reads from a XML file "xmlFile" and passes the
     * information found to the CreateProjectController controller through an
     * HashMap.
     *
     * @param mc
     * @param cvlc
     * @param xmlFile
     */
    public static void readXML(CreateVehicleListController cvlc, File xmlFile) {

        Map parametros = new HashMap();

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean vehicle_list = false;
                boolean vehicle = false;
                boolean type = false;
                boolean motorization = false;
                boolean fuel = false;
                boolean mass = false;
                boolean load = false;
                boolean drag = false;
                boolean frontal_area = false;
                boolean rrc = false;
                boolean wheel_size = false;
                boolean velocity_limit_list = false;
                boolean velocity_limit = false;
                boolean segment_type = false;
                boolean limit = false;
                boolean energy = false;
                boolean min_rpm = false;
                boolean max_rpm = false;
                boolean final_drive_ratio = false;
                boolean energy_regeneration_ratio = false;
                boolean gear_list = false;
                boolean gear = false;
                boolean ratio = false;
                boolean throttle_list = false;
                boolean throttle = false;
                boolean regime = false;
                boolean torque = false;
                boolean rpm_low = false;
                boolean rpm_high = false;
                boolean sfc = false;

                /**
                 * Each time the parser encounters the beginning of a new
                 * element, it calls this method, which resets the string buffer
                 * and puts the attributes found in specific tags into the
                 * HashMap.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @param attributes
                 * @throws SAXException
                 */
                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("VEHICLE_LIST")) {
                        vehicle_list = true;

                    } else if (qName.equalsIgnoreCase("VEHICLE")) {
                        vehicle = true;

                        parametros.put("name", attributes.getValue("name"));
                        parametros.put("description", attributes.getValue("description"));

                        cvlc.addVehicle(parametros);
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("TYPE")) {
                        type = true;

                    } else if (qName.equalsIgnoreCase("MOTORIZATION")) {
                        motorization = true;

                    } else if (qName.equalsIgnoreCase("FUEL")) {
                        fuel = true;

                    } else if (qName.equalsIgnoreCase("MASS")) {
                        mass = true;

                    } else if (qName.equalsIgnoreCase("LOAD")) {
                        load = true;

                    } else if (qName.equalsIgnoreCase("DRAG")) {
                        drag = true;

                    } else if (qName.equalsIgnoreCase("FRONTAL_AREA")) {
                        frontal_area = true;

                    } else if (qName.equalsIgnoreCase("RRC")) {
                        rrc = true;

                    } else if (qName.equalsIgnoreCase("WHEEL_SIZE")) {
                        wheel_size = true;

                    } else if (qName.equalsIgnoreCase("VELOCITY_LIMIT_LIST")) {
                        velocity_limit_list = true;
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("VELOCITY_LIMIT")) {
                        velocity_limit = true;

                    } else if (qName.equalsIgnoreCase("SEGMENT_TYPE")) {
                        segment_type = true;

                    } else if (qName.equalsIgnoreCase("LIMIT")) {
                        limit = true;

                    } else if (qName.equalsIgnoreCase("ENERGY")) {
                        energy = true;

                    } else if (qName.equalsIgnoreCase("MIN_RPM")) {
                        min_rpm = true;

                    } else if (qName.equalsIgnoreCase("MAX_RPM")) {
                        max_rpm = true;

                    } else if (qName.equalsIgnoreCase("FINAL_DRIVE_RATIO")) {
                        final_drive_ratio = true;

                    }else if (qName.equalsIgnoreCase("ENERGY_REGENERATION_RATIO")) {
                        energy_regeneration_ratio = true;

                    } else if (qName.equalsIgnoreCase("GEAR_LIST")) {
                        gear_list = true;

                        cvlc.addVehicleEnergyAttributes(parametros);
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("GEAR")) {
                        gear = true;

                        parametros.put("id", attributes.getValue("id"));

                        cvlc.addGear(parametros);
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("RATIO")) {
                        ratio = true;

                    } else if (qName.equalsIgnoreCase("THROTTLE_LIST")) {
                        throttle_list = true;

                    } else if (qName.equalsIgnoreCase("THROTTLE")) {
                        throttle = true;

                        parametros.put("id", attributes.getValue("id"));

                        cvlc.addThrottle(parametros);
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("REGIME")) {
                        regime = true;

                    } else if (qName.equalsIgnoreCase("TORQUE")) {
                        torque = true;

                    } else if (qName.equalsIgnoreCase("RPM_LOW")) {
                        rpm_low = true;

                    } else if (qName.equalsIgnoreCase("RPM_HIGH")) {
                        rpm_high = true;

                    } else if (qName.equalsIgnoreCase("SFC")) {
                        sfc = true;

                    }
                }

                /**
                 * Each time the parser encounters the end of a new element, it
                 * calls this method, which is used to clear the HashMap after
                 * collecting certain information and finding specific end tags.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @throws SAXException
                 */
                public void endElement(String uri, String localName,
                        String qName)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("VEHICLE_LIST")) {

                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("VELOCITY_LIMIT")) {

                        cvlc.addVelocityLimit(parametros);
                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("GEAR")) {

                        cvlc.addGearRatio(parametros);
                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("REGIME")) {

                        cvlc.addThrottleRegime(parametros);
                        parametros.clear();
                    }else if (qName.equalsIgnoreCase("VEHICLE")) {
 
                        parametros.clear();
                    }else if (qName.equalsIgnoreCase("WHEEL_SIZE")) {
 
                        cvlc.addVehicleAttributes(parametros);
                        parametros.clear();
                    }
 
                }

                /**
                 * whenever the parser encounters plain text (not XML elements),
                 * it calls this method, which accumulates them in a string
                 * buffer. This string is then added into the HashMap.
                 *
                 * @param ch
                 * @param start
                 * @param length
                 * @throws SAXException
                 */
                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (vehicle_list) {
                        vehicle_list = false;

                    } else if (vehicle) {
                        vehicle = false;
                    } else if (type) {
                        parametros.put("type", new String(ch, start, length));
                        type = false;

                    } else if (motorization) {
                        parametros.put("motorization", new String(ch, start, length));
                        motorization = false;

                    } else if (fuel) {
                        parametros.put("fuel", new String(ch, start, length));
                        fuel = false;

                    } else if (mass) {
                        parametros.put("mass", new String(ch, start, length));
                        mass = false;

                    } else if (load) {
                        parametros.put("load", new String(ch, start, length));
                        load = false;

                    } else if (drag) {
                        parametros.put("drag", new String(ch, start, length));
                        drag = false;

                    } else if (frontal_area) {
                        parametros.put("frontal_area", new String(ch, start, length));
                        frontal_area = false;

                    } else if (rrc) {
                        parametros.put("rrc", new String(ch, start, length));
                        rrc = false;

                    } else if (wheel_size) {
                        parametros.put("wheel_size", new String(ch, start, length));
                        wheel_size = false;

                    } else if (velocity_limit_list) {
                        velocity_limit_list = false;

                    } else if (velocity_limit) {
                        velocity_limit = false;

                    } else if (segment_type) {
                        parametros.put("segment_type", new String(ch, start, length));
                        segment_type = false;

                    } else if (limit) {
                        parametros.put("limit", new String(ch, start, length));
                        limit = false;

                    } else if (energy) {
                        energy = false;

                    } else if (min_rpm) {
                        parametros.put("min_rpm", new String(ch, start, length));
                        min_rpm = false;

                    } else if (max_rpm) {
                        parametros.put("max_rpm", new String(ch, start, length));
                        max_rpm = false;

                    } else if (final_drive_ratio) {
                        parametros.put("final_drive_ratio", new String(ch, start, length));
                        final_drive_ratio = false;
                    }else if (energy_regeneration_ratio) {
                        parametros.put("energy_regeneration_ratio", new String(ch, start, length));
                        energy_regeneration_ratio = false;
                    } else if (gear_list) {
                        gear_list = false;

                    } else if (gear) {
                        gear = false;

                    } else if (ratio) {
                        parametros.put("ratio", new String(ch, start, length));
                        ratio = false;

                    } else if (throttle_list) {
                        throttle_list = false;

                    } else if (throttle) {
                        throttle = false;
                    } else if (regime) {
                        regime = false;

                    } else if (torque) {
                        parametros.put("torque", new String(ch, start, length));
                        torque = false;

                    } else if (rpm_low) {
                        parametros.put("rpm_low", new String(ch, start, length));
                        rpm_low = false;

                    } else if (rpm_high) {
                        parametros.put("rpm_high", new String(ch, start, length));
                        rpm_high = false;

                    } else if (sfc) {
                        parametros.put("sfc", new String(ch, start, length));
                        sfc = false;

                    }

                }

            };

            InputStream inputStream = new FileInputStream(xmlFile);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
