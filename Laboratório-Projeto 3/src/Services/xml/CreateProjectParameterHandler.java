/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

import Model.Section;

/**
 * Class responsible for converting data that is not in the necessary format to
 * create RoadNetworks.
 */
public class CreateProjectParameterHandler {

    public static String getStringInsideQuotationMark(String textXML) {

        String[] text = textXML.split("\"");

        return text[1];
    }

    /**
     * Gets the correct typology that matches the received string.
     *
     * @param tip
     * @return Typology
     */
    public static Section.Typology getTypology(String tip) {

        if (tip.equalsIgnoreCase("REGULAR ROAD")) {

            return Section.Typology.REGULAR_ROAD;

        } else if (tip.equalsIgnoreCase("URBAN ROAD")) {

            return Section.Typology.URBAN_ROAD;

        } else if (tip.equalsIgnoreCase("EXPRESS ROAD")) {

            return Section.Typology.EXPRESS_ROAD;

        } else if (tip.equalsIgnoreCase("CONTROLLED ACCESS HIGHWAY")) {

            return Section.Typology.CONTROLLED_ACCESS_HIGHWAY;
        
        } else if (tip.equalsIgnoreCase("HIGHWAY")) {

            return Section.Typology.CONTROLLED_ACCESS_HIGHWAY;
        }
        return Section.Typology.REGULAR_ROAD;
    }

    /**
     * Converts the value in the string into a usable id value of type int.
     *
     * @param idXML
     * @return int
     */
    public static int getID(String idXML) {

        return Integer.parseInt(idXML);
    }

    /**
     * Converts the value in the string into a usable toll value of type int and
     * returns it.
     *
     * @param tollXML
     * @return int
     */
    public static int getToll(String tollXML) {

        return Integer.parseInt(tollXML);
    }

    /**
     * Converts the value in the string into a usable wind direction value of
     * type int and returns it.
     *
     * @param windDirectionXML
     * @return
     */
    public static int getWindDirection(String windDirectionXML) {

        return Integer.parseInt(windDirectionXML);
    }

    /**
     * Converts the value in the string into a usable wind speed value of type
     * double and returns it.
     *
     * @param windSpeedXML
     * @return
     */
    public static double getWindSpeed(String windSpeedXML) {

        String[] windSpeed = windSpeedXML.split("\\s+");

        return Double.parseDouble(windSpeed[0]);
    }

    /**
     * Converts the value in the string into a usable height value of type
     * double and returns it.
     *
     * @param heightXML
     * @return
     */
    public static double getHeight(String heightXML) {

        return Double.parseDouble(heightXML);
    }

    /**
     * Converts the value in the string into a usable slope value of type double
     * and returns it.
     *
     * @param slopeXML
     * @return
     */
    public static double getSlope(String slopeXML) {

        String[] slope = slopeXML.split("%");

        return Double.parseDouble(slope[0]);
    }

    /**
     * Converts the value in the string into a usable length value of type
     * double and returns it.
     *
     * @param lengthXML
     * @return
     */
    public static double getLength(String lengthXML) {

        String[] length = lengthXML.split("\\s+");

        return Double.parseDouble(length[0]);
    }

//    public static double getRRC(String rrcXML) {
//
//        return Double.parseDouble(rrcXML);
//    }
    /**
     * Converts the value in the string into a usable maximum velocity value of
     * type double and returns it.
     *
     * @param MaxVXML
     * @return
     */
    public static double getMaxVelocity(String MaxVXML) {

        String[] maxV = MaxVXML.split("\\s+");

        return Double.parseDouble(maxV[0]);
    }

    /**
     * Converts the value in the string into a usable minimum velocity value of
     * type double and returns it.
     *
     * @param MinVXML
     * @return
     */
    public static double getMinVelocity(String MinVXML) {

        String[] minV = MinVXML.split("\\s+");

        return Double.parseDouble(minV[0]);
    }

    /**
     * Converts the value in the string into a usable number of vehicles value
     * of type int and returns it.
     *
     * @param numberVehiclesVXML
     * @return
     */
    public static int getNumberVehicles(String numberVehiclesVXML) {

        return Integer.parseInt(numberVehiclesVXML);
    }

    public static String getRoad(String stringXML) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
