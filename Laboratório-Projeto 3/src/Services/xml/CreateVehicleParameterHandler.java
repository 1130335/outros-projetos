/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

import Model.Section;
import Model.Vehicles.Diesel;
import Model.Vehicles.Electric;
import Model.Vehicles.Gasoline;
import Model.Vehicles.Motorization;
import Model.Vehicles.Vehicle.VehicleType;

/**
 *
 * @author André
 */
public class CreateVehicleParameterHandler {

    public static String getStringInsideQuotationMark(String textXML) {
//
//        String[] text = textXML.split("\"");
//
//        return text[1];

        return textXML;
    }

    public static VehicleType getVehicleType(String vehicleTypeXML) {

        if (vehicleTypeXML.equalsIgnoreCase("CAR")) {

            return VehicleType.CAR;

        } else if (vehicleTypeXML.equalsIgnoreCase("MOTORCYCLE")) {

            return VehicleType.MOTORCYCLE;

        } else if (vehicleTypeXML.equalsIgnoreCase("SEMITRAILER")) {

            return VehicleType.SEMITRAILER;

        } else if (vehicleTypeXML.equalsIgnoreCase("TRACTOR")) {

            return VehicleType.TRACTOR;
        } else if (vehicleTypeXML.equalsIgnoreCase("TRUCK")) {

            return VehicleType.TRUCK;
        }
        return null;
    }

    public static Motorization getMotorization(String fuelXML) {

        if (fuelXML.equalsIgnoreCase("gasoline")) {
            return new Gasoline();
        } else if (fuelXML.equalsIgnoreCase("diesel")) {
            return new Diesel();
        } else if (fuelXML.equalsIgnoreCase("electric")) {
            return new Electric();
        }

        return null;
    }

    public static double getMass(String massXML) {

        String[] mass = massXML.split("\\s+");

        return Double.parseDouble(mass[0]);
    }

    public static double getLoad(String loadXML) {

        String[] load = loadXML.split("\\s+");

        return Double.parseDouble(load[0]);
    }

    public static double getDrag(String dragXML) {

        return Double.parseDouble(dragXML);
    }

    public static double getFrontalArea(String frontalAreaXML) {

        return Double.parseDouble(frontalAreaXML);
    }

    public static double getRrc(String rrcXML) {

        return Double.parseDouble(rrcXML);
    }

    public static double getWheelSize(String wheelSizeXML) {

        return Double.parseDouble(wheelSizeXML);
    }

    public static Section.Typology getSectionType(String tip) {

        if (tip.equalsIgnoreCase("REGULAR ROAD")) {

            return Section.Typology.REGULAR_ROAD;

        } else if (tip.equalsIgnoreCase("URBAN ROAD")) {

            return Section.Typology.URBAN_ROAD;

        } else if (tip.equalsIgnoreCase("EXPRESS ROAD")) {

            return Section.Typology.EXPRESS_ROAD;

        } else if (tip.equalsIgnoreCase("CONTROLLED ACCESS HIGHWAY")) {

            return Section.Typology.CONTROLLED_ACCESS_HIGHWAY;
        } else if (tip.equalsIgnoreCase("HIGHWAY")) {

            return Section.Typology.CONTROLLED_ACCESS_HIGHWAY;
        }
        return Section.Typology.REGULAR_ROAD;
    }

    public static double getMinRpm(String minRpmXML) {

        return Double.parseDouble(minRpmXML);
    }

    public static double getMaxRpm(String maxRpmXML) {

        return Double.parseDouble(maxRpmXML);
    }

    public static double getFinalDriveRatio(String finalDriveRatioXML) {

        return Double.parseDouble(finalDriveRatioXML);
    }

    public static double getEnergyRegenerationRatio(String energyRegenerationRatioXML) {

        if(energyRegenerationRatioXML != null){
        
            return Double.parseDouble(energyRegenerationRatioXML);
        }
        
        return 0;
    }

    public static double getGearRatio(String gearRatioXML) {

        return Double.parseDouble(gearRatioXML);
    }

    public static double getTorque(String torqueXML) {

        return Double.parseDouble(torqueXML);
    }

    public static double getRpmLow(String rpmLowXML) {

        return Double.parseDouble(rpmLowXML);
    }

    public static double getRpmHigh(String rpmHighXML) {

        return Double.parseDouble(rpmHighXML);
    }

    public static double getSfc(String sfcXML) {

        if (sfcXML != null) {
            return Double.parseDouble(sfcXML);

        } else {
            return 0;
        }

    }

}
