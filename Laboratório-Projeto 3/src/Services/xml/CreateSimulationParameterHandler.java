/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

/**
 *
 * @author André
 */
public class CreateSimulationParameterHandler {

    public static String getStringInsideQuotationMark(String textXML) {

        String[] text = textXML.split("\"");

        return text[1];
    }

    public static int getArrivalRate(String arrivalRateXML) {

        String[] arrivalRate = arrivalRateXML.split("\\s+");

        return Integer.parseInt(arrivalRate[0]);
    }

}
