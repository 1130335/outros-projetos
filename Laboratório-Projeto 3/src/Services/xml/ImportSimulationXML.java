/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.xml;

import Controller.CreateSimulationController;
import Controller.P02_CreateProjectController;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class ImportSimulationXML {
    
    
    /**
     * 
     * @param xmlFile
     */
    public static void readXML(CreateSimulationController csc, File xmlFile) {
        
        Map parametros = new HashMap();

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean Simulation  = false;
                boolean traffic_list = false;
                boolean traffic_pattern  = false;
                boolean vehicle = false;
                boolean arrival_rate = false;

                /**
                 * Each time the parser encounters the beginning of a new
                 * element, it calls this method, which resets the string buffer
                 * and puts the attributes found in specific tags into the
                 * HashMap.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @param attributes
                 * @throws SAXException
                 */
                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("SIMULATION")) {
                        Simulation  = true;

                        parametros.put("id", attributes.getValue("id"));
                        parametros.put("description", attributes.getValue("description"));

                        csc.setSimulation(parametros);
                        parametros.clear();
                        
                    } else if (qName.equalsIgnoreCase("TRAFFIC_LIST")) {
                        traffic_list = true;
                    } else if (qName.equalsIgnoreCase("TRAFFIC_PATTERN")) {
                        traffic_pattern  = true;
                        
                        parametros.put("begin", attributes.getValue("begin"));
                        parametros.put("end", attributes.getValue("end"));

                        csc.addTrafficPattern(parametros);
                        parametros.clear();

                    } else if (qName.equalsIgnoreCase("VEHICLE")) {
                        vehicle = true;
                    } else if (qName.equalsIgnoreCase("ARRIVAL_RATE")) {
                        arrival_rate = true;
                    } 
                }

                /**
                 * Each time the parser encounters the end of a new element, it
                 * calls this method, which is used to clear the HashMap after
                 * collecting certain information and finding specific end tags.
                 *
                 * @param uri
                 * @param localName
                 * @param qName
                 * @throws SAXException
                 */
                public void endElement(String uri, String localName,
                        String qName)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("NETWORK")) {

                        parametros.clear();
                    } else if (qName.equalsIgnoreCase("TRAFFIC_PATTERN")) {

                        csc.setTrafficPattern(parametros);

                        parametros.clear();
                    }

                }

                /**
                 * whenever the parser encounters plain text (not XML elements),
                 * it calls this method, which accumulates them in a string
                 * buffer. This string is then added into the HashMap.
                 *
                 * @param ch
                 * @param start
                 * @param length
                 * @throws SAXException
                 */
                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (Simulation) {
                        Simulation = false;
                    } else if (traffic_list) {
                        traffic_list = false;
                    } else if (traffic_pattern) {
                        traffic_pattern = false;
                    } else if (vehicle) {
                        parametros.put("vehicle", new String(ch, start, length));
                        vehicle = false;
                    } else if (arrival_rate) {
                        parametros.put("arrival_rate", new String(ch, start, length));
                        arrival_rate = false;
                    }

                }

            };

            InputStream inputStream = new FileInputStream(xmlFile);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
