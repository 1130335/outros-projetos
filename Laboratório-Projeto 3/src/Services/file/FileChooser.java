/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services.file;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author André
 */
public class FileChooser {

    public static File FileChooser(String title) {

        File file;
        String extension;

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle(title);
        int returnVal = chooser.showOpenDialog(null); // null used to be parent
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            file = chooser.getSelectedFile();

            extension = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length());

            if (extension.equalsIgnoreCase("xml")) {

                if (file.length() > 0) {
                    return file;
                } else {

                    JOptionPane.showMessageDialog(null, "This file is empty");
                }
            }
        }

        return null;
    }
}
