/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Paulo silva
 */
public class Graph<V,E> {
    
    private int numVert;
    private int numEdge;
    private boolean isDirected;
    private ArrayList<Vertex< V, E>> listVert;  //Vertice list

    // Constructs an empty graph (either undirected or directed)
    public Graph(boolean directed) {
        numVert = 0;
        numEdge = 0;
        isDirected = directed;
        listVert = new ArrayList<>();
    }

    public int numVertices() {
        return numVert;
    }

    public Iterable<Vertex<V, E>> vertices() {
        return getListVert();
    }

    public int numEdges() {
        return numEdge;
    }

    public Iterable<Edge< V, E>> edges() {
        ArrayList<Edge< V, E>> edges = new ArrayList<>();
        for (Vertex< V, E> v : getListVert()) {
            for (Edge< V, E> e : v.getOutgoing().values()) {
                if (e != null) {
                    edges.add(e);
                }
            }
        }
        return edges;
    }

    public Edge< V, E> getEdge(Vertex< V, E> vorig, Vertex< V, E> vdest) {

        if (getListVert().contains(vorig) && getListVert().contains(vdest)) {
            return vorig.getOutgoing().get(vdest);
        }

        return null;
    }

    public Vertex< V, E>[] endVertices(Edge< V, E> e) {
        if (e.getVDest() != null && e.getVOrig() != null) {
            return e.getEndpoints();
        }
        return null;
    }


    public Vertex< V, E> opposite(Vertex< V, E> vert, Edge< V, E> e) {
        Vertex< V, E> vOppos = new Vertex< V, E>();
        //vOppos = null;

        if (!listVert.contains(vert)) {
            return null;
        }
        boolean bool = false;
        for (Edge< V, E> edge : vert.getOutgoing().values()) {
            if (edge == e) {
                bool = true;
            }
        }

        if (bool) {
            Vertex< V, E>[] vertices = e.getEndpoints();
            vOppos = vertices[1];
        } else {
            vOppos = null;
        }

        return vOppos;
    }


    public int outDegree(Vertex< V, E> v) {

        if (getListVert().contains(v)) {
            return v.getOutgoing().size();
        } else {
            return -1;
        }
    }

    public int inDegree(Vertex< V, E> v) {
        if (!isDirected) {
            return this.outDegree(v);
        }

        int cont = 0;
        if (getListVert().contains(v)) {
            for (Vertex< V, E> ver : getListVert()) {
                for (Edge< V, E> e : ver.getOutgoing().values()) {
                    Vertex vDest = this.opposite(ver, e);
                    if (vDest == v) {
                        cont++;
                    }
                }
            }
            return cont;
        } else {
            return -1;
        }
    }

    public Iterable<Edge< V, E>> outgoingEdges(Vertex< V, E> v) {

        if (getListVert().contains(v)) {
            return v.getOutgoing().values();
        }

        return null;
    }

    public Iterable<Edge< V, E>> incomingEdges(Vertex< V, E> v) {
        ArrayList<Edge< V, E>> edges = new ArrayList<>();
        for (Vertex< V, E> vert : getListVert()) {
            for (Edge< V, E> e : vert.getOutgoing().values()) {
                if (e != null) {
                    Vertex vDest = this.opposite(vert, e);
                    if (vDest == v) {
                        edges.add(e);
                    }
                }
            }
        }
        return edges;
    }

    public Vertex< V, E> insertVertex( V vInf) {

        Vertex< V, E> vert = getVertex(vInf);
        if (vert == null) {
            Vertex< V, E> newvert = new Vertex<>(numVert, vInf);
            getListVert().add(newvert);
            numVert++;
            return newvert;
        }
        return vert;
    }

    public Edge< V, E> insertEdge( V vOrig,  V vDest, E eInf, double eWeight) {

        Vertex<V, E> vorig = getVertex(vOrig);
        if (vorig == null) {
            vorig = insertVertex(vOrig);
        }

        Vertex<V, E> vdest = getVertex(vDest);
        if (vdest == null) {
            vdest = insertVertex(vDest);
        }

        if (getEdge(vorig, vdest) == null) {
            Edge<V, E> newedge = new Edge<>(eInf, eWeight, vorig, vdest);
            vorig.getOutgoing().put(vdest, newedge);
            numEdge++;

            //if graph is not direct insert other edge in the opposite direction 
            if (!isDirected) {
                Edge<V, E> otheredge = new Edge<>(eInf, eWeight, vdest, vorig);
                vdest.getOutgoing().put(vorig, otheredge);
                numEdge++;
            }
            return newedge;
        }
        return null;
    }

    public void removeVertex( V vInf) {
        Vertex<V, E> v = getVertex(vInf);
        if (getListVert().contains(v)) {
            getListVert().remove(v);
            numVert--;
        }
    }

    public void removeEdge(Edge<V, E> edge) {

        Vertex<V, E>[] endpoints = endVertices(edge);

        Vertex<V, E> vorig = endpoints[0];
        Vertex<V, E> vdest = endpoints[1];

        if (vorig != null && vdest != null) {
            if (edge.equals(getEdge(vorig, vdest))) {
                vorig.getOutgoing().remove(vdest);
                numEdge--;
            }
        }
    }

    public Vertex<V, E> getVertex( V vInf) {

        for (Vertex<V, E> vert : this.getListVert()) {
            if (vInf.equals(vert.getElement())) {
                return vert;
            }
        }

        return null;
    }

    public Vertex<V, E> getVertex(int vKey) {

        if (vKey < getListVert().size()) {
            return getListVert().get(vKey);
        }

        return null;
    }

    //Returns a clone of the graph 
    public Graph<V, E> clone() {

        Graph<V, E> newObject = new Graph<>(this.isDirected);

        newObject.isDirected = this.isDirected;
        newObject.numVert = this.numVert;
        newObject.numEdge = this.numEdge;

        newObject.listVert = new ArrayList<Vertex<V, E>>();

        for (Vertex<V, E> v : this.getListVert()) {
            newObject.getListVert().add(v.clone());
        }

        return newObject;
    }

    /* equals implementation
     * @param the other graph to test for equality
     * @return true if both objects represent the same graph
     */
    public boolean equals(Object oth) {

        if (oth == null) {
            return false;
        }

        if (this == oth) {
            return true;
        }

        if (!(oth instanceof Graph<?, ?>)) {
            return false;
        }

        Graph<?, ?> other = (Graph<?, ?>) oth;

        if (numVert != other.numVert || numEdge != other.numEdge) {
            return false;
        }

        if (!listVert.equals(other.listVert)) {
            return false;
        }

        return true;
    }

    //string representation
    @Override
    public String toString() {
        String s = "";
        if (numVert == 0) {
            s = "\nGraph not defined!!";
        } else {
            s = "Graph: " + numVert + " vertices, " + numEdge + " edges\n";
            for (Vertex<V, E> vert : getListVert()) {
                s += vert + "\n";
                if (!vert.getOutgoing().isEmpty()) {
                    for (Map.Entry<Vertex<V, E>, Edge<V, E>> entry : vert.getOutgoing().entrySet()) {
                        s += entry.getValue() + "\n";
                    }
                } else {
                    s += "\n";
                }
            }
        }
        return s;
    }

    /**
     * @return the listVert
     */
    public ArrayList<Vertex< V, E>> getListVert() {
        return listVert;
    }
    
}