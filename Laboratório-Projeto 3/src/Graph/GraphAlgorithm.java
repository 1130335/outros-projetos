/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import Model.Node;
import Model.Section;
import Model.Segment;
import Model.Vehicles.Gear;
import Model.Vehicles.Throttle;
import Model.Vehicles.Vehicle;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Paulo silva
 */
public class GraphAlgorithm {

    public static final double airDensity = 1.225;
    public static final double gravity = 9.8;
    public static final double dieselEnergy = 48;
    public static final double gasolineEnergy = 44.4;

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> Deque<V> BreadthFirstSearch(Graph<V, E> g, V vInf) {

        Vertex<V, E> vOrig = g.getVertex(vInf);

        if (vOrig == null) {
            return null;
        }

        Deque<V> qbfs = new LinkedList<>();
        Deque<Vertex<V, E>> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vOrig.getElement());
        qaux.add(vOrig);
        int vKey = vOrig.getKey();
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vOrig = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
                Vertex<V, E> vAdj = g.opposite(vOrig, edge);
                vKey = vAdj.getKey();
                if (!visited[vKey]) {
                    qbfs.add(vAdj.getElement());
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    private static <V, E> void DepthFirstSearch(Graph<V, E> g, Vertex<V, E> vOrig, boolean[] visited, Deque<V> qdfs) {
        qdfs.add(vOrig.getElement());
        visited[vOrig.getKey()] = true;
        for (Edge e : g.outgoingEdges(vOrig)) {
            if (visited[e.getVDest().getKey()] == false) {
                DepthFirstSearch(g, e.getVDest(), visited, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> Deque<V> DepthFirstSearch(Graph<V, E> g, V vInf) {
        Deque qdfs = new LinkedList<V>();
        Vertex v = g.getVertex(vInf);
        if (v != null) {
            boolean visited[] = new boolean[g.numVertices()];
            for (int i = 0; i < visited.length; i++) {
                visited[i] = false;
            }
            DepthFirstSearch(g, v, visited, qdfs);
            return qdfs;
        }
        return null;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    public static <V, E> void allPaths(Graph<V, E> g, Vertex<V, E> vOrig, Vertex<V, E> vDest,
            boolean[] visited, Deque<V> path, ArrayList<Deque<V>> paths) {

        path.push(vOrig.getElement());
        int vKey = vOrig.getKey();
        visited[vKey] = true;

        for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
            Vertex<V, E> vAdj = g.opposite(vOrig, edge);
            if (vAdj.getElement() == vDest.getElement()) {
                path.push(vAdj.getElement());
                Deque<V> revpath = revPath(path);
                paths.add(new LinkedList(revpath));  //save clone of reverse path
                path.pop();
            } else {
                if (!visited[vAdj.getKey()]) {
                    allPaths(g, vAdj, vDest, visited, path, paths);
                }
            }
        }
        visited[vKey] = false;
        path.pop();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<Deque<V>> allPaths(Graph<V, E> g, V voInf, V vdInf) {

        Deque<V> path = new LinkedList<>();
        ArrayList<Deque<V>> paths = new ArrayList<>();
        boolean[] visited = new boolean[g.numVertices()];

        Vertex<V, E> vOrig = g.getVertex(voInf);
        Vertex<V, E> vDest = g.getVertex(vdInf);

        if (vOrig != null && vDest != null) {
            allPaths(g, vOrig, vDest, visited, path, paths);
        }

        return paths;
    }

    /**
     * @param <Node>
     * @param <Section>
     * @param g Graph instance
     * @param n1
     * @param n2
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <Node, Section> ArrayList<Deque<Node>> allPathsNew(Graph<Node, Section> g, Node n1, Node n2) {

        Deque<Node> path = new LinkedList<>();
        ArrayList<Deque<Node>> paths = new ArrayList<>();
        boolean[] visited = new boolean[g.numVertices()];

        Vertex<Node, Section> vOrig = g.getVertex(n1);
        Vertex<Node, Section> vDest = g.getVertex(n2);

        if (vOrig != null && vDest != null) {
            allPaths(g, vOrig, vDest, visited, path, paths);
        }

        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> g, Vertex<V, E> vOrig,
            boolean[] visited, int[] pathKeys, double[] dist) {
        int vkeyOrig = vOrig.getKey();
        dist[vkeyOrig] = 0;
        while (vkeyOrig != -1) {
            visited[vkeyOrig] = true;
            vOrig = g.getVertex(vkeyOrig);
            for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
                Vertex<V, E> vAdj = g.opposite(vOrig, edge);
                int vkeyAdj = vAdj.getKey();
                if (!visited[vkeyAdj] && dist[vkeyAdj] > dist[vkeyOrig] + edge.getWeight()) {
                    dist[vkeyAdj] = dist[vkeyOrig] + edge.getWeight();
                    pathKeys[vkeyAdj] = vkeyOrig;
                }
            }
            double minDist = Double.MAX_VALUE;
            vkeyOrig = -1;
            for (int i = 0; i < g.numVertices(); i++) {
                if (!visited[i] && dist[i] < minDist) {
                    minDist = dist[i];
                    vkeyOrig = i;
                }
            }
            if (minDist == Double.MAX_VALUE) {
                vkeyOrig = -1;
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V voInf, V vdInf, int[] pathKeys, Deque<V> path) {
        if (voInf != vdInf) {
            path.push(vdInf);
            Vertex<V, E> vert = g.getVertex(vdInf);
            int vKey = vert.getKey();
            int prevVKey = pathKeys[vKey];
            vert = g.getVertex(prevVKey);
            vdInf = vert.getElement();
            getPath(g, voInf, vdInf, pathKeys, path);
        } else {
            path.push(voInf);
        }
    }

    //shortest-path between voInf and vdInf
    public static <V, E> double shortestPath(Graph<V, E> g, V voInf, V vdInf, Deque<V> shortPath) {
        Vertex<V, E> vOrig = g.getVertex(voInf);
        Vertex<V, E> vDest = g.getVertex(vdInf);
        if (vOrig == null || vDest == null) {
            return 0;
        }
        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }
        shortestPathLength(g, vOrig, visited, pathKeys, dist);
        double lengthPath = dist[vDest.getKey()];
        if (lengthPath != Double.MAX_VALUE) {
            getPath(g, voInf, vdInf, pathKeys, shortPath);
            return lengthPath;
        }
        return 0;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> Deque<V> revPath(Deque<V> path) {

        Deque<V> pathcopy = new LinkedList<>(path);
        Deque<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }

    public static double calculateFastestPath(Graph<Node, Section> roadmap, Node orig, Node dest) {
        double time;
        double min = Double.MAX_VALUE;
        double total = 0;
        double velocMax;
        double distTotalSeg;
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {
                total = 0;
                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    for (Segment s : sec_x.getSequenceOfSegments()) {
                        velocMax = s.getMaxVelocity();
                        distTotalSeg = s.getLength();
                        time = calculateTime(velocMax, distTotalSeg);
                        total = total + time;
                    }
                }
                if (total < min) {
                    min = total;
                }
            }
        }
        return min;
    }

    public static double calculateTime(double v, double d) {
        double time;
        time = ((d * 1000) / (v / 3.6));
        return time;
    }

    public static double calcMaxLinearVelocOfVehicle_inMperSec(Vehicle v) {
        double maxVelocOfVehicle_inMperHour = 0;
        double radiusOfTire = v.getWheelSize() / 2; // radiusOfTire (raio do pneu em m).
        double engineSpeed_InRotationsPerSec = v.getEnergy().getMaxRpm() / 60;
        //double engineSpeed_InRotationsPerSec = convertRPM_to_RadPerSec(v.getEnergy().getMaxRpm()); //1 RPM = 2π(1)rad.min−1 = 2π/60 rad.s−1 = 0,104719755120 rad.s−1 (arredondado ao excesso aos 10−12); // é isto?
        double finalDriveRatio = v.getEnergy().getFinalDriveRatio(); //(no unit)
        double kthGearRatio = v.getEnergy().getGearList().get(v.getEnergy().getGearList().size() - 1).getRatio(); //gear da última mudança
        maxVelocOfVehicle_inMperHour = (2 * Math.PI * radiusOfTire * engineSpeed_InRotationsPerSec) / (finalDriveRatio * kthGearRatio);

        return maxVelocOfVehicle_inMperHour;
    }

    public static double convertRPM_to_RadPerSec(double value_InRPM) {
        return /*double valueInRadPerSec = */ value_InRPM * ((2 * Math.PI) / 60);   //1 RPM = 2π(1)rad.min−1 = 2π/60 rad.s−1
    }

    public static double convertMPerSec_to_KmPerH(double value_InMPerSec) {
        return /*double valueInKmPerHour = */ value_InMPerSec * 3.6;  //== 3.6 == (3600 / 1000). 1 meter per second = 3.6 kilometers per hour
    }

    public static ArrayList<Node> fastestPath(Graph<Node, Section> roadmap, Node orig, Node dest) {
        //Deque fastestPath = new LinkedList<>();
        ArrayList<Node> fastestPath = new ArrayList<>();
        double time;
        double min = Double.MAX_VALUE;
        double total = 0;
        double velocMax;
        double distTotalSeg;
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {
                total = 0;
                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    for (Segment s : sec_x.getSequenceOfSegments()) {
                        velocMax = s.getMaxVelocity();
                        distTotalSeg = s.getLength();
                        time = calculateTime(velocMax, distTotalSeg);
                        total = total + time;
                    }
                }
                if (total < min) {
                    min = total;
                    if (!(fastestPath.isEmpty())) {
                        fastestPath.clear();
                    }
                    for (Node n : deque) {
                        fastestPath.add(n);
                    }
                }
            }
        }
        return fastestPath;
    }

    //Calcula o caminnho mais rápido atendendo ao diferencial que possa existir entre a velocidade máxima do segmento e a velocidade máxima que o veículo pode atingir
    public static ArrayList<Node> fastestPathForVehicle(Graph<Node, Section> roadmap, Node orig, Node dest, Vehicle vehicle) {
        //Deque fastestPath = new LinkedList<>();
        ArrayList<Node> fastestPath = new ArrayList<>();
        double time;
        double min = Double.MAX_VALUE;
        double total = 0;
        double segm_maxVeloc;
        double distTotalSeg;
        double vehic_maxVeloc = convertMPerSec_to_KmPerH(calcMaxLinearVelocOfVehicle_inMperSec(vehicle));
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {
                total = 0;
                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    for (Segment s : sec_x.getSequenceOfSegments()) {
                        segm_maxVeloc = s.getMaxVelocity();
                        distTotalSeg = s.getLength();
                        if (vehic_maxVeloc < segm_maxVeloc) {
                            time = calculateTime(vehic_maxVeloc, distTotalSeg);
                        } else {
                            time = calculateTime(segm_maxVeloc, distTotalSeg);
                        }
                        total = total + time;
                    }
                }
                if (total < min) {
                    min = total;
                    if (!(fastestPath.isEmpty())) {
                        fastestPath.clear();
                    }
                    for (Node n : deque) {
                        fastestPath.add(n);
                    }
                }
            }
        }
        return fastestPath;
    }

    public static double calculateVelocVehicle_inMperSecoWithWind(Vehicle v, double maxSpeed, double windSpeed, double windDirection) {

        maxSpeed = calcMaxLinearVelocOfVehicle_inMperSec(v);
        double relativeVelo;

        relativeVelo = calculateRelativeVelo(maxSpeed, windSpeed, windDirection);

        return relativeVelo;

    }

    public static double calculateGravitationalForce(double rrc, double dragCoefficient, double angle, double mass, double frontalArea, double velocity) {
        double graviForce = rrc * mass * gravity * Math.cos(Math.toRadians(angle)) + 0.5 * dragCoefficient * frontalArea * airDensity * Math.pow(velocity, 2) + mass * gravity * Math.sin(Math.toRadians(angle));
        return graviForce;
    }

    public static double calculateForce(double rrc, double dragCoefficient, double angle, double mass, double frontalArea, double velocity) {
        double graviForce = rrc * mass * gravity * Math.cos(Math.toRadians(angle)) - 0.5 * dragCoefficient * frontalArea * airDensity * Math.pow(velocity, 2) - mass * gravity * Math.sin(Math.toRadians(angle));
        return graviForce;
    }

    public static double calculateRelativeVelo(double Vvelocidade, double Vwind, double windDirection) {
        double Vrelative = Vvelocidade + (Vwind * (Math.cos(Math.toRadians(windDirection))));
        return Vrelative;
    }

    public static double calculateEnergy(double force, double deslocation) {
        double energy = force * deslocation;
        return energy;
    }

//    public static ArrayList<Node> theoreticalMostEnergyEfficientPath(Graph<Node,Section> roadmap,Node orig, Node dest,Vehicle v){
//        ArrayList<Node> efficientPath=new ArrayList();
//        double time;
//        double min = Double.MAX_VALUE;
//        double total = 0;
//        double totalEnergy = 0;
//        double minEnergy = 0;
//        double graviForce=0;
//        double energy=0;
//        double maxSpeedVehicle=calcMaxLinearVelocOfVehicle_inMperSec(v);
//        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
//        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
//            for (Deque<Node> deque : allPathBetween2Nodes) {
//                total = 0;
//                List<Node> listaDNodesDPath = new ArrayList<>();
//                for (Node n : deque) {
//                    listaDNodesDPath.add(n);
//                }
//                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
//                    Node node1 = listaDNodesDPath.get(i);
//                    Node node2 = listaDNodesDPath.get(i + 1);
//                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
//                    Section sec_x = (Section) edge.getElement();
//
//                    for (Segment s : sec_x.getSequenceOfSegments()) {
//                        double windD=sec_x.getWindDirection();
//                        double windS=sec_x.getWindSpeed();
//                        double maxVSeg=s.getMaxVelocity();
//                        double segmentLenght=s.getLength();
//                        double rollResist=s.getRollResistCoef();
//                        double angle=s.getSlope();
//                        double dragCoef=v.getDragCoefficient();
//                        double frontArea=v.getFrontalArea();
//                        double mass=v.getMass();
//                        
//                        double vehic_maxVelocWind = calculateVelocVehicle_inMperSecoWithWind(v,maxSpeedVehicle,windS,windD);
//                        double vehic_maxVelocWindSeg=calculateVelocVehicle_inMperSecoWithWind(v, maxVSeg, windS, windD);
//                        if(vehic_maxVelocWind>maxVSeg){
//                            graviForce=calculateGravitationalForce(min, dragCoef, angle, mass, frontArea, vehic_maxVelocWindSeg);
//                            energy=calculateEnergy(graviForce, segmentLenght);
//                        }else{
//                            graviForce=calculateGravitationalForce(min, dragCoef, angle, mass, frontArea, vehic_maxVelocWind);
//                            energy=calculateEnergy(graviForce, segmentLenght);
//                        }
//                        
//                        totalEnergy=energy+totalEnergy;
//                    }
//                    minEnergy=totalEnergy;
//                }
//                if (totalEnergy < minEnergy) {
//                    minEnergy = totalEnergy;
//                    if (!(efficientPath.isEmpty())) {
//                        efficientPath.clear();
//                    }
//                    for (Node n : deque) {
//                        efficientPath.add(n);
//                    }
//                }
//            }
//        }
//        return efficientPath;
//    }
    public static double theoreticalMostEnergyEfficientPath2(Graph<Node, Section> roadmap, Node orig, Node dest, Vehicle v) {
        ArrayList<Node> efficientPath = new ArrayList();

        double totalEnergy = 0;
        double minEnergy = 0;
        double graviForce;
        double energy;
        double dragCoef = v.getDragCoefficient();
        double frontArea = v.getFrontalArea();
        double mass = v.getMass();
        double rrc = v.getRrc();

        double maxSpeedVehicle = calcMaxLinearVelocOfVehicle_inMperSec(v);
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {

                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    double windD = sec_x.getWindDirection();
                    double windS = sec_x.getWindSpeed();
                    for (Segment s : sec_x.getSequenceOfSegments()) {
//                        double windD=sec_x.getWindDirection();
//                        double windS=sec_x.getWindSpeed();
                        double maxVSeg = s.getMaxVelocity();
                        double segmentLenght = s.getLength();
                        double rollResist = s.getRollResistCoef();
                        double angle = s.getSlope();

                        double vehic_maxVelocWind = calculateVelocVehicle_inMperSecoWithWind(v, maxSpeedVehicle, windS, windD);
                        double vehic_maxVelocWindSeg = calculateVelocVehicle_inMperSecoWithWind(v, maxVSeg, windS, windD);
                        if (vehic_maxVelocWind > maxVSeg) {
                            graviForce = calculateGravitationalForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWindSeg);
                            energy = calculateEnergy(graviForce, segmentLenght);
                        } else {
                            graviForce = calculateGravitationalForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWind);
                            energy = calculateEnergy(graviForce, segmentLenght);
//                        totalEnergy=energy+totalEnergy;
                        }
                        totalEnergy = totalEnergy + graviForce;

                    }
                    minEnergy = totalEnergy;
                }
                if (totalEnergy < minEnergy) {
                    minEnergy = totalEnergy;

                }
            }
//        }

        }
        return minEnergy;

    }

    public static ArrayList<Node> theoreticalMostEnergyEfficientPath3(Graph<Node, Section> roadmap, Node orig, Node dest, Vehicle v) {
        ArrayList<Node> efficientPath = new ArrayList();

        double totalEnergy = 0;
        double minEnergy =Double.MAX_VALUE;
        double graviForce;
        double energy;
        double dragCoef = v.getDragCoefficient();
        double frontArea = v.getFrontalArea();
        double mass = v.getMass();
        double rrc = v.getRrc();

        double maxSpeedVehicle = calcMaxLinearVelocOfVehicle_inMperSec(v);
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {
                
                System.out.println(i);
                
                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    double windD = sec_x.getWindDirection();
                    double windS = sec_x.getWindSpeed();
                    for (Segment s : sec_x.getSequenceOfSegments()) {
//                        double windD=sec_x.getWindDirection();
//                        double windS=sec_x.getWindSpeed();
                        double maxVSeg = s.getMaxVelocity();
                        double segmentLenght = s.getLength();
                        double rollResist = s.getRollResistCoef();
                        double angle = s.getSlope();

                        double vehic_maxVelocWind = calculateVelocVehicle_inMperSecoWithWind(v, maxSpeedVehicle, windS, windD);
                        double vehic_maxVelocWindSeg = calculateVelocVehicle_inMperSecoWithWind(v, maxVSeg, windS, windD);
                        if (vehic_maxVelocWind > maxVSeg) {
                            graviForce = calculateGravitationalForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWindSeg);
                            energy = calculateEnergy(graviForce, segmentLenght);
                        } else {
                            graviForce = calculateGravitationalForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWind);
                            energy = calculateEnergy(graviForce, segmentLenght);
//                        totalEnergy=energy+totalEnergy;
                        }
                        totalEnergy = totalEnergy + graviForce;

                    }
 //                   minEnergy=totalEnergy;
                }
                if (totalEnergy < minEnergy) {
                    minEnergy = totalEnergy;
                    if (!(efficientPath.isEmpty())) {
                        efficientPath.clear();
                    }
                    for (Node n : deque) {
                        efficientPath.add(n);
                    }
                }
            }
//        }

        }
        return efficientPath;

    }

    public static double calculateWrpm(Vehicle v, double maxSpeed, double windSpeed, double windDirection, double velo, double radious, double gRatio) {
        velo = calculateVelocVehicle_inMperSecoWithWind(v, maxSpeed, windSpeed, windDirection);
        double Wrpm = (60 * gRatio * velo) / (2 * Math.PI * radious);
        return Wrpm;
    }

    public static double calculateRealEnergy(Vehicle v, double force, double sfc, double deslo) {
        double energy;
        if(v.getMotorization().toString().equalsIgnoreCase("Diesel")){
            energy=force*deslo*sfc*dieselEnergy;
        }else if(v.getMotorization().toString().equalsIgnoreCase("Gasoline")){
            energy=force*deslo*sfc*gasolineEnergy;
        }else{
            energy=force*deslo*v.getEnergy().getEnergyRegenerationRatio();
        }
        return energy;
    }

    public static ArrayList<String> theoreticalMostEnergyEfficientPathN05(Graph<Node, Section> roadmap, Node orig, Node dest, Vehicle v) {
        ArrayList<Node> efficientPath = new ArrayList();

        ArrayList<Gear> gearList = v.getEnergy().getGearList();
        ArrayList<Throttle> throttleList = v.getEnergy().getThrottleList();

        double totalEnergy = 0;
        double minEnergy = Double.MAX_VALUE;
        double graviForce;
        double energy = 0;
        double dragCoef = v.getDragCoefficient();
        double frontArea = v.getFrontalArea();
        double mass = v.getMass();
        double rrc = v.getRrc();
        double finalDriveRatio = v.getEnergy().getFinalDriveRatio(); // G no unit
        double gearRatio; //Depende da velocidade escolhida no unit
        double tireRadius = v.getWheelSize() / 2;
        double rpm;
        double torque;
        double sfc;
        double energiaR = 0;
        double rpmR = 0;
        double torqueR = 0;
        double sfcR = 0;
        int contador = 0;

        double maxSpeedVehicle = calcMaxLinearVelocOfVehicle_inMperSec(v);
        ArrayList<Deque<Node>> allPathBetween2Nodes = GraphAlgorithm.allPathsNew(roadmap, orig, dest);
        
        System.out.println(allPathBetween2Nodes);
        
        for (int i = 0; i < allPathBetween2Nodes.size(); i++) {
            for (Deque<Node> deque : allPathBetween2Nodes) {

                List<Node> listaDNodesDPath = new ArrayList<>();
                for (Node n : deque) {
                    listaDNodesDPath.add(n);
                }
                for (i = 0; i < (listaDNodesDPath.size() - 1); i++) {
                    Node node1 = listaDNodesDPath.get(i);
                    Node node2 = listaDNodesDPath.get(i + 1);
                    Edge edge = roadmap.getEdge(roadmap.getVertex(node1), roadmap.getVertex(node2));
                    Section sec_x = (Section) edge.getElement();

                    double windD = sec_x.getWindDirection();
                    double windS = sec_x.getWindSpeed();

                    for (Segment s : sec_x.getSequenceOfSegments()) {
//                        double windD=sec_x.getWindDirection();
//                        double windS=sec_x.getWindSpeed();
                        double maxVSeg = s.getMaxVelocity();
                        double segmentLenght = s.getLength();
                        double rollResist = s.getRollResistCoef();
                        double angle = s.getSlope();

                        double vehic_maxVelocWind = calculateVelocVehicle_inMperSecoWithWind(v, maxSpeedVehicle, windS, windD);
                        double vehic_maxVelocWindSeg = calculateVelocVehicle_inMperSecoWithWind(v, maxVSeg, windS, windD);
       
                        System.out.println(s);
                        
                        graviForce = 0;

                        for (i = gearList.size() - 1; graviForce < 0 && i >= 0; i--) {

                            System.out.println(i);
                            
                            for (int j = 0; graviForce < 0 && j < throttleList.size(); j++) {

                                if (vehic_maxVelocWind > maxVSeg) {
                                    rpm = calculateWrpm(v, vehic_maxVelocWindSeg, windS, windD, angle, tireRadius, gearList.get(i).getRatio());
                                    sfc = calculateSfc(throttleList.get(j), rpm);
                                    torque = calculateTorque(throttleList.get(j), rpm);
                                    graviForce = calculateMotorização(torque, tireRadius, finalDriveRatio, gearList.get(i).getRatio())
                                            - calculateForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWindSeg);
                                    energy += calculateRealEnergy(v, graviForce, sfc, segmentLenght);

                                } else {
                                    rpm = calculateWrpm(v, vehic_maxVelocWind, windS, windD, angle, tireRadius, gearList.get(i).getRatio());
                                    sfc = calculateSfc(throttleList.get(j), rpm);
                                    torque = calculateTorque(throttleList.get(j), rpm);
                                    graviForce = calculateMotorização(torque, tireRadius, finalDriveRatio, gearList.get(i).getRatio())
                                            - calculateForce(rrc, dragCoef, angle, mass, frontArea, vehic_maxVelocWind);
                                    energy += calculateRealEnergy(v, graviForce, sfc, segmentLenght);

                                }

                                contador++;
                                energiaR += energy;
                                rpmR += rpm;
                                torqueR += torque;
                                sfcR += sfc; 
                                totalEnergy = totalEnergy + graviForce;

                            }
                        }
                        ///
                    }

                }
                if (totalEnergy < minEnergy) {
                    minEnergy = totalEnergy;
                    if (!(efficientPath.isEmpty())) {
                        efficientPath.clear();
                    }
                    for (Node n : deque) {
                        efficientPath.add(n);
                    }
                }
            }
        }

        ArrayList<String> results = new ArrayList();
        
        String nodes = "";
        
        for(Node n : efficientPath){
        
        nodes += n.getNodeID() +"-";
        }
        
        System.out.println(efficientPath);
        
        results.add(nodes);
        results.add(null);
        results.add(Double.toString(energiaR/contador));
        results.add(Double.toString(rpmR/contador));
        results.add(Double.toString(torqueR/contador));
        results.add(Double.toString(sfcR/contador)); 

        return results;
    }

    public static double calculateMotorização(double torque, double tireRadius, double finalDriveRatio, double gearRatio) {

        return ((torque * finalDriveRatio * gearRatio) / tireRadius);
    }

    public static double calculateTorque(Throttle throttle, double rpm) {

        return throttle.getRegime(rpm).getTorque();
    }

    public static double calculateSfc(Throttle throttle, double rpm) {

        return throttle.getRegime(rpm).getSfc();
    }

}

//    public static double calculatePot(Vehicle v){
//        double power=0;
//        double engineSpeed_InRotationsPerSec = v.getEnergy().getMaxRpm() / 60;
//        double torque=0;
//        power=2*Math.PI*torque*(engineSpeed_InRotationsPerSec/60);
//        return power;
//    }

