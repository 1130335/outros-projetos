package Controller;

import Repository.Oracle.ProjectRepositoryDB;
import Repository.Persistence;

/**
 *
 * @author 1140256
 */
public class P04_ChangeProjectController {
    
    ProjectRepositoryDB repo;
    
    public P04_ChangeProjectController(){
        repo = Persistence.getRepositoryFactory().createProjectRepositoryDB();
    }
    
    public void changeProjectNameAndDescription(String newName, String newDescription, MainController mc){
        repo.updateProject(newName, newDescription, mc.getActiveProject().getName());
        mc.getActiveProject().setName(newName);
        mc.getActiveProject().setDescription(newDescription);
        
        
    }
}
