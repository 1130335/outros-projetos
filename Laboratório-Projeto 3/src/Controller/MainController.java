/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Graph.GraphAlgorithm;
import Model.Node;
import Model.Project;
import Model.Simulation;
import Model.Vehicles.Gasoline;
import Model.Vehicles.Vehicle;
import Repository.Oracle.ProjectRepositoryDB;
import Services.file.FileChooser;
import UI.MainJFrame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Main Controller. Designed to be the middle man between the Application UI and
 * the rest of the functionalities.
 */
public class MainController {

    private Project activeProject;
    private ArrayList<Vehicle> activeVehicleList;
    private Simulation activateSimulation;
    private Vehicle activeVehicle;
    ProjectRepositoryDB repository;

    /**
     * Constructor. Initializes the UI.
     */
    public MainController() {

        MainJFrame m1 = new MainJFrame(this);
        m1.setVisible(true);

        this.activeProject = new Project();
        this.activeVehicleList = new ArrayList<>();
        this.activateSimulation = new Simulation();
        this.repository = new ProjectRepositoryDB();
    }

    /**
     * Starts the process of creating a new Project. Begins by receiving the new
     * Projects name and description. It also asks the user to provide a file
     * from where to read the data to create the respective RoadNetwork.
     *
     * @param ProjectName
     * @param ProjectDescription
     */
    public void P02_createNewProject(String ProjectName, String ProjectDescription) {

        File file = FileChooser.FileChooser("Select a project configuration file");

        if (file != null) {

            P02_CreateProjectController cpc = new P02_CreateProjectController();

            cpc.createProject(this, ProjectName, ProjectDescription, file);

            file = FileChooser.FileChooser("Select a vehicle configuration file");

            if (file != null) {

                CreateVehicleListController cvlc = new CreateVehicleListController();

                cvlc.CreateVehicleListController(this, file);

                System.out.println("Número de veículos carregados no projecto \"" + activeProject.getName() + "\": " + activeProject.getListOfVehicles().size() + "\nCriação de novo projeto concluída!");

                ProjectRepositoryDB repository = new ProjectRepositoryDB();
                repository.addProject(this.activeProject); //add all the atributes (including list of vehicles of the active project
            }
        }
    }

//    public List<Vehicle> createtempVehicleList() {
//        CreateVehicleListController cvlc = new CreateVehicleListController();
//        cvlc.CreateVehicleListController(this, file);
//        //activeVehicleList = new ArrayList<>();
//        this.activeProject.setListOfVehicles(cvlc.getListofVehicles());
//        System.out.println("Número de veículos carregados no projecto \"" + activeProject.getName() + "\": " + activeProject.getListOfVehicles().size() + "\nCriação de novo projeto concluída!");
//
//        ProjectRepositoryDB repository = new ProjectRepositoryDB();
//        repository.addProject(this.activeProject); //add all the atributes (including list of vehicles of the active project
//        return null;
//    }
//
//
    public void createVehicleList() {

        File file = FileChooser.FileChooser("Select a vehicle configuration file");

        if (file != null) {

            CreateVehicleListController cvlc = new CreateVehicleListController();

            cvlc.CreateVehicleListController(this, file);
        }

    }

    public void createSimulation() {

        File file = FileChooser.FileChooser("Select a simulation configuration file");

        if (file != null) {

            CreateSimulationController csc = new CreateSimulationController();

            csc.CreateSimulationController(this, file);
        }

    }

    /**
     *
     * @return
     */
    public P03_CopyProjectController copyProject() {
        return new P03_CopyProjectController();
    }

    public void addVerifiedNewVehiclesToProject(P05_AddVehicleToProjectController control_P05) {
        //P05_AddVehicleToProjectController control_P05 = new P05_AddVehicleToProjectController();
        //this.setActiveProject(control_P05.getActiveProject());
        this.setActiveProject(control_P05.getActiveProject());
        System.out.println("Active project em main: " + this.getActiveProject().getName());
        System.out.println("Nomes de Veículos no active project do maincontroller: " + this.getActiveProject().getListOfVehicles().size());
        for (Vehicle v : this.getActiveProject().getListOfVehicles()) {
            System.out.println("" + v.getName());
        }

        System.out.println("Active project em controller" + control_P05.getActiveProject().getName());
        System.out.println("Nomes de Veículos no active project do controller: " + control_P05.getActiveProject().getListOfVehicles().size());
        for (Vehicle v : control_P05.getActiveProject().getListOfVehicles()) {
            System.out.println("" + v.getName());
        }

        if (!control_P05.getTempNewListofVehicles().isEmpty()) {
            for (Vehicle vehicle : control_P05.getTempNewListofVehicles()) {
                System.out.println("Nome de veículo em análise de validação: " + vehicle.getName());
                Vehicle vehicleToAdd = control_P05.validatesVehicleToImportAndName(vehicle);
                if (vehicleToAdd != null) {
                    //if (control_P05.validateAndImportNewVehicleToProject(vehicle)) { //adiciona a lista de veículos de projeto ativo de contro
                    this.getActiveProject().getListOfVehicles().add(vehicleToAdd);
                    //  System.out.println("maincontroller() Adicionado veiculo " + v.getName() + " à lista de veículos do projeto " + this.getActiveProject());
                    System.out.println("maincontroller() Veículo adicionado a projeto \"" + this.getActiveProject().getName() + "\"  " + control_P05.getActiveProject().getName() + " com o nome: " + vehicleToAdd.getName() + ".");
                    System.out.println("Número atual de veículos de projeto: " + this.getActiveProject().getListOfVehicles().size());
                    System.out.println("Nomes do veículos:");
                    for (Vehicle v : this.getActiveProject().getListOfVehicles()) {
                        System.out.println("" + v.getName());
                    }
                    //           JOptionPane.showMessageDialog(addVehicleJPanel, "New vehicle(s) added to project \"" + selecProjAddVehPanelComboBox.getSelectedItem().toString() + "\" !" /*+ this.controP05.getActiveProject().getName()*/, "Confirmation", JOptionPane.INFORMATION_MESSAGE);
//ADICIONAR NOVO VEÍCULO A BASE DE DADOS
                    int projID = repository.getProjectID(this.getActiveProject());
                    repository.addVehicle(vehicleToAdd, projID);
                    System.out.println("Veiculo " + vehicleToAdd.getName() + " adicionado à lista de veículos do projeto " + this.getActiveProject().getName() + " na BD!");
                } else {
                    System.out.println("maincontroller() Veículo \"" + vehicle.getName() + "\" não foi adicionado ao projeto \"" + this.getActiveProject().getName() + "\"  " + control_P05.getActiveProject().getName() + " pois já existe um veículo igual.");
                }
            }
        } else {
            System.out.println("Nenhum dos veículos do ficheiro reuniu condições para ser adicionado ao projeto.");
        }
    }

    public ArrayList<String> N05_mostEfficientPathInRealConditions(Vehicle vehicle, Node start, Node end) {

        ArrayList<String> results = GraphAlgorithm.theoreticalMostEnergyEfficientPathN05(this.activeProject.getRoadNetwork().getRoadNetworkGraph(), start, end, vehicle);

        return results;
    }

    public boolean checkIfProjectNameAvailable(String projectName) {

        List<String> aux = this.repository.getCurrentProjects();

        if (!(aux == null)) {

            for (String p : aux) {

                if (p.equals(projectName)) {

                    return false;
                }
            }

        }
        return true;
    }

    public void activateProject(Project p) {

        this.setActiveProject(p);
    }

    public Project getActiveProject() {

        return this.activeProject;
    }

    public void activateVehicleList(ArrayList<Vehicle> v) {

        this.setActiveVehicleList(v);
        
        this.activeProject.setListOfVehicles(v);
    }

    public void activateSimulation(Simulation s) {

        this.setActivateSimulation(s);
    }

    /**
     * @param activeProject the activeProject to set
     */
    public void setActiveProject(Project activeProject) {
        this.activeProject = activeProject;
    }

    /**
     * @return the activeVehicleList
     */
    public ArrayList<Vehicle> getActiveVehicleList() {
        return activeVehicleList;
    }

    /**
     * @param activeVehicleList the activeVehicleList to set
     */
    public void setActiveVehicleList(ArrayList<Vehicle> activeVehicleList) {
        this.activeVehicleList = activeVehicleList;
    }

    /**
     * @return the activateSimulation
     */
    public Simulation getActivateSimulation() {
        return activateSimulation;
    }

    /**
     * @param activateSimulation the activateSimulation to set
     */
    public void setActivateSimulation(Simulation activateSimulation) {
        this.activateSimulation = activateSimulation;
    }

    public Vehicle getActiveVehicle() {
        return activeVehicle;
    }

    public void setActivateVehicle(Vehicle activateVehicle) {
        this.activeVehicle = activateVehicle;
    }
}
