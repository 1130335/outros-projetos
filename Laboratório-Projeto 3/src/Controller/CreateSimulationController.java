/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Simulation;
import Model.TrafficPattern;
import Services.xml.CreateSimulationParameterHandler;
import Services.xml.ImportSimulationXML;
import java.io.File;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author André
 */
public class CreateSimulationController {

    private Simulation simulationAUX;

    private TrafficPattern trafficPatternAUX;

    public CreateSimulationController() {}

    public void CreateSimulationController(MainController mc, File xmlFile) {

        ImportSimulationXML.readXML(this, xmlFile);

        if (!(simulationAUX == null)) {

            mc.activateSimulation(simulationAUX);

        } else {
            JOptionPane.showMessageDialog(null, "Corrupt file");
        }
    }

    public void setSimulation(Map parameters) {

        simulationAUX = new Simulation();

        simulationAUX.setName(CreateSimulationParameterHandler.getStringInsideQuotationMark((String) parameters.get("name")));
        simulationAUX.setDescription(CreateSimulationParameterHandler.getStringInsideQuotationMark((String) parameters.get("description")));

    }

    public void addTrafficPattern(Map parameters) {

        trafficPatternAUX = new TrafficPattern();

        trafficPatternAUX.setBeginNodeId(CreateSimulationParameterHandler.getStringInsideQuotationMark((String) parameters.get("begin")));
        trafficPatternAUX.setEndNodeId(CreateSimulationParameterHandler.getStringInsideQuotationMark((String) parameters.get("end")));

        simulationAUX.addTrafficPattern(trafficPatternAUX);
    }

    public void setTrafficPattern(Map parameters) {

        trafficPatternAUX.setVehicle((String) parameters.get("vehicle"));
        trafficPatternAUX.setArrivalRate(CreateSimulationParameterHandler.getArrivalRate((String) parameters.get("arrival_rate")));

        simulationAUX.addTrafficPattern(trafficPatternAUX);
    }

}
