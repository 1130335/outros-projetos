package Controller;

import Model.Vehicles.Energy;
import Model.Vehicles.Motorization;
import Model.Project;
import Model.Vehicles.Vehicle;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;
import Repository.Persistence;
import Services.xml.ImportVehicleXML;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Helder
 */
public class P05_AddVehicleToProjectController {

    private ProjectRepositoryDB repository;
    private Project activeProject;
    private List<Vehicle> tempNewListofVehicles;
    private MainController mc;

    public P05_AddVehicleToProjectController() {
        this.repository = Persistence.getRepositoryFactory().createProjectRepositoryDB();
        this.tempNewListofVehicles = new ArrayList<>();
        this.activeProject = new Project();
        this.repository = new ProjectRepositoryDB();
    }

//    public P05_AddVehicleToProjectController(MainController mc) {
//        this.repository = Persistence.getRepositoryFactory().createProjectRepositoryDB();
//        this.activeProject = null;
//        this.tempNewListofVehicles = new ArrayList<>();
//        this.mc = mc;
//    }
//    public P05_AddVehicleToProjectController(String activProject, MainController mc) {
//        this.repository = Persistence.getRepositoryFactory().createProjectRepositoryDB();
//        this.mc = mc;
//        setSelectedProjectIntoActivProj(activProject, mc);
//        //       mc.activateProject(repository.getProjectByName(activProject));
//    }
    //Tirar MainController de parametro do proximo metodo  
    //Método seguinte pode ser void pois coloca lista em contro005.setTemo() p Main aceder
    public void /*boolean*/ /*List<Vehicle>*/ importedNewVehiclesList(/*MainController mc, */File xmlFile) {
                //MainContro;
                CreateVehicleListController cvlc = new CreateVehicleListController();
                //cvlc.CreateVehicleListController(mc, xmlFile);
                //cvlc.CreateVehicleListController(mc, xmlFile);
                ImportVehicleXML.readXML(cvlc, xmlFile);
                System.out.println("xmlFile confirmado: " + xmlFile.getAbsolutePath());
                //this.setTempNewListofVehicles(cvlc.getListofVehicles());
                System.out.println("Elementos de Lista de Veiculos de cvlc (ou veículos importados do ficheiro): " + cvlc.getListofVehicles().size());

                if (!cvlc.getListofVehicles().isEmpty()) {
                    System.out.println("Nomes de veículos:");
                    for (Vehicle v : cvlc.getListofVehicles()) {
                        System.out.println("" + v.getName());
                    }

                    this.setTempNewListofVehicles(cvlc.getListofVehicles());
                    System.out.println("Veículos na lista temporária: ");
                    System.out.println("Nomes de veículos:");
                    for (Vehicle v : this.getTempNewListofVehicles()) {
                        System.out.println("" + v.getName());
                    }
                    //if (!tempNewListofVehicles.isEmpty()) {
                    System.out.println("Os veículos do ficheiro foram adicionados à lista temporária"
                            + " e aguardam verificação de requisitos para determinar se devem ser "
                            + "adicionados à lista de veículos de projeto.");

                    //vai VERIFICAR: se PARA CADA VEÍCULO importado do ficheiro (adicionado(s) à lista de cvlc)
                    //Não EXISTir um veiculo IGUAL na lista de veiculos do projeto ativo, adiciona com
                    //ou sem alteração do nome do veiculo, através do método validateAndImportNewVehicleToProject(v):
////            for (Vehicle vehicle : cvlc.getListofVehicles()) {
////                validateAndImportNewVehicleToProject(vehicle);
////                if (validateAndImportNewVehicleToProject(vehicle)) { // se não existir carro igual adiciona (com o nome original nome alterado, se já existir outro veículo com o mesmo nome mas com caraterística(s) diferentes)
////                    this.tempNewListofVehicles.add(vehicle); //esta lista guarda agora apenas os veiculos que reunem condições de serem adicionados à lista de veiculos de projeto
                    //mc.getActiveProject().addVehicleToListOfV(vehicle);
////                }
////            } 
                    //mc.getActiveProject().setListOfVehicles(vehicleList);
                    //return cvlc.getListofVehicles();
                    ////   return this.getTempNewListofVehicles();
      //          return true;
                } else {
                    //JOptionPane.showMessageDialog(null, "Corrupt vehicle file");
                    System.out.println("O ficheiro não permitiu que fosse carregado qualquer veículo.");
                    System.out.println("Após a leitura do ficheiro a lista temporária de novos veículos está vazia? " + this.getTempNewListofVehicles().isEmpty());
       //            return false;
                }
                //return this.tempNewListofVehicles;
            }

            public Vehicle validatesVehicleToImportAndName(Vehicle newVehicle) {
                boolean verifDuplicatedName = false;
                boolean verifyIsNotTheSame = false;
                Vehicle newVehicleToAdd = null;

                for (Vehicle v : this.getActiveProject().getListOfVehicles()) {
                    if (v.getName().equalsIgnoreCase(newVehicle.getName())) {
                        verifDuplicatedName = true;
                        //se atributos NAO forem todos iguais, não é o mesmo objeto e vai adicionar com nome diferente
                        //           if (!newVehicle.equals(v)) {
                        if (!((newVehicle.getType() == v.getType()) //verifica se os restantes atributos do veículo (para além do nome) são todos iguais
                                && (newVehicle.getMotorization().toString().equalsIgnoreCase(v.getMotorization().toString()))
                                && (newVehicle.getMass() == v.getMass())
                                && (newVehicle.getLoad() == v.getLoad())
                                && (newVehicle.getDragCoefficient() == v.getDragCoefficient())
                                && (newVehicle.getDescription().equalsIgnoreCase(v.getDescription()))
                                && (newVehicle.getRrc() == v.getRrc())
                                && (newVehicle.getWheelSize() == v.getWheelSize())
                                && (newVehicle.getEnergy().toString().equals(v.getEnergy().toString())))) {
                            verifyIsNotTheSame = true;  //nome igual mas não é veiculo igual
                        } //else nome igual true mas é veiculo igual verifyIsNotTheSame = false -> return null;
                    }
                    // se encontrou veículo com o mesmo nome e mesmos atributos sai do ciclo:
                    if ((verifDuplicatedName == true) && (verifyIsNotTheSame == false)) {
                        break;
                    }
                }
                // se percorreu todos os veiculos da lista e não encontrou nenhum com o mesmo nome, devolve veiculo com nome original
                if (verifDuplicatedName == false) {
                    //this.getTempNewListofVehicles().add(newVehicle);
                    newVehicleToAdd = new Vehicle(newVehicle);
                } else {
                    //se existe nome igual
                    if (verifDuplicatedName == true) {
                        if (verifyIsNotTheSame == true) { //e atributos do veículo não são iguais, vai adicionar com novo nome
                            int vehicle_NameAddedNumber = getNumberOfLastReplica(newVehicle.getName());
                            if (vehicle_NameAddedNumber == 0) {  //no 1.º caso em que nome ainda não tem adicionado qualquer número de repetição de nome, o segundo carro com o mesmo nome vai ser "nomeigual_2"
                                vehicle_NameAddedNumber = 1;
                            }
                            String nameWithoutNumOfReplica = splitByUnderScore(newVehicle.getName());
                            newVehicle.setName(nameWithoutNumOfReplica + "_" + (vehicle_NameAddedNumber + 1));
                            newVehicleToAdd = validatesVehicleToImportAndName(newVehicle);
                        } else { //se atributos também são iguais não adiciona veículo ao projeto
                            newVehicleToAdd = null;
                        }
                    }
                }
                return newVehicleToAdd;
            }
            
                //
            //    /**
            //     * User Story P05: This method allows to add new vehicles to the project by
            //     * importing a vehicle configuration file. When name duplications are
            //     * identified, if the vehicles are not the same, the next sequential number
            //     * os replica is appended to the vehicle name
            //     *
            //     * @param newVehicle the new vehicle to add
            //     * @return true if the newVehicle was added, or false if the newVehicle
            //     * don't fulfill the requirements to be added
            //     */
            //    public boolean validateAndImportNewVehicleToProject(Vehicle newVehicle) {
            //        boolean verifDuplicatedName = false;
            //        boolean verifyIsNotTheSame = false;
            //        boolean newVehicleToAdd = false;
            //        for (Vehicle v : this.getActiveProject().getListOfVehicles()) {
            //            if (v.getName().equalsIgnoreCase(newVehicle.getName())) {
            //                verifDuplicatedName = true;
            //                //se atributos NAO forem todos iguais, não é o mesmo objeto e vai adicionar com nome diferente
            //                if (!newVehicle.equals(v)) {
            //                    verifyIsNotTheSame = true;
            //                }
            //            } // se encontrou veículo com o mesmo nome e mesmos atributos sai do ciclo:
            //            if ((verifDuplicatedName == true) && (verifyIsNotTheSame == false)) {
            //                break;
            //            }
            //        }
            //        // se percorreu todos os veiculos da lista e não encontrou nenhum com o mesmo nome, adiciona ao projeto
            //        if (verifDuplicatedName == false) {
            //            this.tempNewListofVehicles.add(newVehicle);
            //            //this.getActiveProject().addVehicleToListOfV(newVehicle);
            //            newVehicleToAdd = true;
            //        } else {
            //            //se existe nome igual
            //            if (verifDuplicatedName == true) {
            //                if (verifyIsNotTheSame == true) { //e atributos do veículo não são iguais, vai adicionar com novo nome
            //                    int vehicle_NameAddedNumber = getNumberOfLastReplica(newVehicle.getName());
            //                    if (vehicle_NameAddedNumber == 0) {  //no 1.º caso em que nome ainda não tem adicionado qualquer número de repetição de nome, o segundo carro com o mesmo nome vai ser "nomeigual_2"
            //                        vehicle_NameAddedNumber = 1;
            //                    }
            //                    String nameWithoutNumOfReplica = splitByUnderScore(newVehicle.getName());
            //                    newVehicle.setName(nameWithoutNumOfReplica + "_" + (vehicle_NameAddedNumber + 1));
            //                    newVehicleToAdd = validateAndImportNewVehicleToProject(newVehicle);
            //                } else { //se atributos também são iguais não adiciona veículo ao projeto
            //                    newVehicleToAdd = false;
            //                }
            //            }
            //        }
            //        return newVehicleToAdd;
            //    }

            /**
             * This method returns the original name of an vehicle without the
             * number of replica eventually added. Example "car_2" returns
             * "car", the original name of the vehicle
             *
             * @param str the original name of the vehicle without the number of
             * replica
             * @return
             */
            public String splitByUnderScore(String str) {
                String[] split = str.split("_"); // It matches positions between not-a-number and a number (in any order).
                return split[0];
            }

            /**
             * Returns the number of the last replica of vehicle with the same
             * original name in the list of vehicles of the project. For
             * example, if we have an vehicle named "car" in the list of
             * vehicles of the project and other vehicle named "car" with
             * different attributes is added to the project this last vehicle is
             * added under the new name of "car_2". This method returns the
             * number of the last replica of the original name of a vehicle in
             * the list of vehicles of project. If in that list we only have at
             * the moment "car", the method returns the replica number 0. If in
             * the list we already have an "car_2", the method will return 2.
             *
             * @param str the name of the vehicle supposed to be added to the
             * project
             * @return number of last replica already in the list of vehicles of
             * the project
             */
            public int getNumberOfLastReplica(String str) {
                int numberOfReplica;
                if (!str.contains("_")) {            //se nome for por exemplo "carro"
                    numberOfReplica = 0;
                } else {
                    String[] split = str.split("_"); // It matches positions between not-a-number and a number (in any order).
                    numberOfReplica = Integer.parseInt(split[1]);      // na posição 1 fica o underscore
                }
                return numberOfReplica;
            }

            /**
             * Returns the actual actived project
             *
             * @return the activeProject
             */
            public Project getActiveProject() {
//        return mc.getActiveProject();
                return this.activeProject;
            }

            /**
             * Sets the project passed by parameter as the active project at
             * that moment
             *
             * @param activeProject the activeProject to set
             */
            public void setActiveProject(Project activeProject) {
                this.activeProject = activeProject;
//        mc.activateProject(activeProject);
            }

            /**
             * @return the tempNewListofVehicles
             */
            public List<Vehicle> getTempNewListofVehicles() {
                return tempNewListofVehicles;
            }

            /**
             * @param tempNewListofVehicles the tempNewListofVehicles to set
             */
            public void setTempNewListofVehicles(List<Vehicle> tempNewListofVehicles) {
                this.tempNewListofVehicles = tempNewListofVehicles;
            }

//    /**
//     * Coloca o projeto selecionado na comboBox como active project atual
//     *
//     * @param name
//     * @param mc
//     * @return
//     */
//    public Project setSelectedProjectIntoActivProj(String name, MainController mc) {
//        for (String proj : this.repository.getCurrentProjects()) {
//            if (proj.equals(name)) {
//                Project proj1 = this.repository.getProjectByName(name);
//                this.setActiveProject(proj1);
//                //mc.activateProject(proj1);
//                return proj1;
//            }
//        }
//        return null;
//    }
            public Project setSelectedProjectIntoActivProj(String selectedProj) {
                for (String proj : this.repository.getCurrentProjects()) {
                    if (proj.equals(selectedProj)) {
                        Project proj1 = new Project(this.repository.getProjectByName(selectedProj));
                        this.setActiveProject(proj1);
                        //mc.activateProject(proj1);                
                        System.out.println("setSelectedProjectIntoActivProj(): " + this.activeProject.getName());
                        return proj1;
                    }
                }
                return null;
            }

            /**
             * Returns a list with the names of the existing projects
             *
             * @return list with names
             */
            public List<String> getCurrentProjects() {
                return this.repository.getCurrentProjects();
            }

            /**
             * Method only used for testing purposes
             *
             * @return repository
             */
            public ProjectRepositoryDB getRepository() {
                return this.repository;
            }

//    public String getStringInsideQuotationMark(String textXML) {
//
//        String[] text = textXML.split("\"");
//
//        return text[1];
//    }
            /**
             * Returns the actual number of projects of the repository
             *
             * @return number of projects of the repository
             */
            public int getNumberOfProjInRepository() {
                return this.repository.getCurrentProjects().size();
            }

}
