package Controller;

import Model.Project;
import Model.RoadNetwork;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;
import Repository.Persistence;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller of the user story P03: Copy Project
 */
public class P03_CopyProjectController {

    private final ProjectRepositoryDB repository;
    
    /**
     * Constructor that inicializes the repository variable
     */
    public P03_CopyProjectController() {
        this.repository = Persistence.getRepositoryFactory().createProjectRepositoryDB();
        
        // Testing Code
//        RoadNetwork rn = new RoadNetwork();
//        this.repository.addProject(new Project(rn, "P01", "P01 Description"));
//        this.repository.addProject(new Project(rn, "P02", "P02 Description"));
//        this.repository.addProject(new Project(rn, "P03", "P03 Description"));
    }
    
    /**
     * Returns a list with the names of the existing projects
     * @return list with names
     */
    public List<String> getCurrentProjects() {
        List<String> projectNames = this.repository.getCurrentProjects();
        return projectNames;
    }
    
    private Project getSelectedProject(String name) {
        return this.repository.getProjectByName(name);
    }
    
    /**
     * Verifies the existance of the given name
     * @param name: name to verify
     * @return verification
     */
    public boolean verifyName(String name) {
        boolean verified = this.repository.verifyName(name);
        return verified;
    }
    
    /**
     * Creates a new project by copy
     * @param newName: name  of the new project
     * @param oldName: name of the copied project
     * @param description: description of the new project
     * @param mc
     */
    public void copyProject(String newName, String oldName, String description, MainController mc) {
        Project copiedProject = new Project(getSelectedProject(oldName));
        copiedProject.setName(newName);
        copiedProject.setDescription(description);
        this.repository.addProject(copiedProject);
        mc.activateProject(copiedProject);
    }
    
    /**
     * Method only used for testing purposes
     * @return repository
     */
    public ProjectRepositoryDB getRepository() {
        return this.repository;
    }
}
