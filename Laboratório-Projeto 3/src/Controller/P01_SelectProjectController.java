/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import Repository.InMemory.ProjectRepositoryInMemory;
import Repository.Oracle.ProjectRepositoryDB;
import Repository.Persistence;
import java.util.List;

/**
 *
 * @author Paulo silva
 */
public class P01_SelectProjectController {
   
    private ProjectRepositoryDB repo;
    private Project proj;
    
    public P01_SelectProjectController(){
        repo=Persistence.getRepositoryFactory().createProjectRepositoryDB();
    }
    
    
    public List<String> getAllProjects() {
        return this.repo.getCurrentProjects();
    }
    
    public void activeProject(MainController mc, String pn){
        Project proj = repo.getProjectByName(pn);
        mc.activateProject(proj);
    }
   
    public void verifyProject(){
        List<Project> list=repo.getAllProjects();
        boolean b;
        for(Project p:list){
            if(proj.isActive()==true){
                b=false;
        }
    }
}
}
