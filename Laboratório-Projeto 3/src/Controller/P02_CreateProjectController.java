/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Node;
import Model.Project;
import Model.RoadNetwork;
import Model.Section;
import Model.Section.Direction;
import Model.Segment;
import Model.Vehicles.Vehicle;
import Repository.Oracle.ProjectRepositoryDB;
import Services.xml.ImportProjectXML;
import Services.xml.CreateProjectParameterHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 * Controller of the user story P02: Create Project
 */
public class P02_CreateProjectController {

    private Project pt;
    private RoadNetwork rn;
    private ArrayList<Node> nodeList = new ArrayList<>();
    private Section sAUX;

    /**
     * Constructor
     */
    public P02_CreateProjectController() {
    }

    /**
     * Inicializes both Project and RoadNetwork variables. Sets the Project's
     * name and description. Starts the whole process of assembly of the
     * RoadNetwork by reading from the previously selected file. After that Sets
     * the Project's RoadNetwork and makes it the active one for use in the
     * application.
     *
     * @param mc
     * @param name
     * @param description
     * @param xmlFile
     */
    public void createProject(MainController mc, String name, String description, File xmlFile) {

        pt = new Project();
        rn = new RoadNetwork();

        pt.setName(name);
        pt.setDescription(description);

        ImportProjectXML.readXML(this, xmlFile);

        if (!(nodeList == null)) {

            pt.setRoadNetwork(rn);
            mc.activateProject(pt);

            dadosGuardadosDeNetworkAposCriacaoProjeto();

        } else {
            JOptionPane.showMessageDialog(null, "Corrupt file");
        }
    }

    public Project getProject() {
        return pt;
    }

    /**
     * Sets the RoadNetwork's id/name and description.
     *
     * @param parameters
     */
    public void setNetwork(Map parameters) {

        rn.setName((String) parameters.get("id"));
        rn.setDescription((String) parameters.get("description"));

        //pt.setRoadNetwork(rn);
        // É preciso fazer split por ".
    }

    /**
     * Creates a new node with the received name and adds it to the auxiliary
     * node list and to the RoadNetwork.
     *
     * @param nodeName
     */
    public void addNode(String nodeName) {

        Node n = new Node(nodeName);

        nodeList.add(n);

        rn.addNode(n);
    }

    /**
     * Creates a new Section with the received parameters and adds it to its
     * "begin" and "end" nodes.
     *
     * @param parameters
     */
    public void addRoadSection(Map parameters) {

        Node begin = this.getNode((String) parameters.get("begin"));
        Node end = this.getNode((String) parameters.get("end"));

        String tip = (String) parameters.get("typology");
        String dir = (String) parameters.get("direction");

        Direction direction = Direction.valueOf(dir.toUpperCase());

        Section s = new Section(begin,
                end,
                CreateProjectParameterHandler.getTypology(tip),
                direction,
                CreateProjectParameterHandler.getToll((String) parameters.get("toll")),
                CreateProjectParameterHandler.getWindDirection((String) parameters.get("wind_direction")),
                CreateProjectParameterHandler.getWindSpeed((String) parameters.get("wind_speed")),
                (String) parameters.get("road")
        );

        sAUX = s;
        begin.addSectionOut(s);
        end.addSectionIn(s);

        rn.addSection(s);
    }

    /**
     * Creates a new Section Segment with the received parameters and adds it
     * currently selected Road Segment.
     *
     * @param parameters
     */
    public void addSegment(Map parameters) {

        Segment sgm = new Segment(CreateProjectParameterHandler.getID((String) parameters.get("id")),
                CreateProjectParameterHandler.getHeight((String) parameters.get("height")),
                CreateProjectParameterHandler.getSlope((String) parameters.get("slope")),
                CreateProjectParameterHandler.getLength((String) parameters.get("length")),
                //                                CreateProjectParameterHandler.getRRC((String) parameters.get("rrc")),
                0,
                CreateProjectParameterHandler.getNumberVehicles((String) parameters.get("number_vehicles")),
                CreateProjectParameterHandler.getMinVelocity((String) parameters.get("min_velocity")),
                CreateProjectParameterHandler.getMaxVelocity((String) parameters.get("max_velocity")));

        sAUX.addSegment(sgm);
    }

    /**
     * Gets the node with the provided id.
     *
     * @param id
     * @return Node
     */
    public Node getNode(String id) {

        for (Node n : nodeList) {

            if (id.equals(n.getNodeID())) {

                return n;
            }
        }
        return null;
    }
    
        //para efeitos de teste
    public void dadosGuardadosDeNetworkAposCriacaoProjeto() {
        System.out.println("dadosGuardadosAposCriacaoProjeto()\n");
        System.out.println("Project name: " + pt.getName());
        System.out.println("Project description: " + pt.getDescription());
        System.out.println("RoadNetwork name: " + pt.getRoadNetwork().getName());
        System.out.println("RoadNetwork description: " + pt.getRoadNetwork().getDescription());
        System.out.println("Nós: ");
        for (Node x : pt.getRoadNetwork().getNodesList()) {
            System.out.print(x.toString() + " ");
        }
        System.out.println("");
        System.out.println("Secções: " + pt.getRoadNetwork().getSections().size());
        System.out.println("====SectionList: ====");
        for (Section sec : pt.getRoadNetwork().getSections()) {
            System.out.println("Road/Section: " + sec.getRoad());
            System.out.println("Beginning Node: " + sec.getBeginningNode().toString());
            System.out.println("Ending Node: " + sec.getEndingNode().toString());
            System.out.println("Typology: " + sec.getTipology());
            System.out.println("Direction: " + sec.getDirection());
            System.out.println("Toll: " + sec.getToll());
            System.out.println("Wind Direction: " + sec.getWindDirection());
            System.out.println("Wind Speed: " + sec.getWindSpeed());

            System.out.println("->N.º de Segmentos: " + sec.getSequenceOfSegments().size());
            System.out.println("==Segment List: ==");
            for (Segment seg : sec.getSequenceOfSegments()) {
                System.out.println("Segment Index: " + seg.getSegm_index());
                System.out.println("Height: " + seg.getInitialHeigh());
                System.out.println("Slope: " + seg.getSlope());
                System.out.println("Length: " + seg.getLength());
                System.out.println("Max_velocity: " + seg.getMaxVelocity());
                System.out.println("Min velocity: " + seg.getMinVelocity());
                System.out.println("Number of vehicles: " + seg.getMaxNumOfVehicles() + "\n");
            }
            System.out.println("-------------");
        }
    }

}
