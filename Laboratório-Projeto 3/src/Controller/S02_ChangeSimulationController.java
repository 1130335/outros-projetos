/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Simulation;
import Repository.Oracle.ProjectRepositoryDB;
import Repository.Persistence;
import java.util.List;

/**
 *
 * @author Francisco
 */
public class S02_ChangeSimulationController {
    
    private final ProjectRepositoryDB repository;
    
    public S02_ChangeSimulationController() {
        this.repository = Persistence.getRepositoryFactory().createProjectRepositoryDB();
    }
    
    public List<String> getCurrentSimulations() {
        List<String> simNames = this.repository.getCurrentSimulations();
        return simNames;
    }
    
    private Simulation getSelectedSimulation(String name) {
        return this.repository.getSimulationByName(name);
    }
    
    public void changeSimulation(String newName, String newDescription, MainController mc) {
        Simulation sim = mc.getActivateSimulation();
        sim.setName(newName);
        sim.setDescription(newDescription);
        //update DB
    }
    
    /**
     * Method only used for testing purposes
     * @return repository
     */
    public ProjectRepositoryDB getRepository() {
        return this.repository;
    }
}
