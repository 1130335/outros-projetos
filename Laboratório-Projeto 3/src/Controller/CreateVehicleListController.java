/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Vehicles.Energy;
import Model.Vehicles.Gear;
import Model.Vehicles.Regime;
import Model.Vehicles.Throttle;
import Model.Vehicles.Vehicle;
import Model.Vehicles.VelocityLimit;
import Services.xml.CreateVehicleParameterHandler;
import Services.xml.ImportVehicleXML;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author André
 */
public class CreateVehicleListController {

    private List<Vehicle> vehicleList;

    private Vehicle vehicleAux;

    private Energy energyAux;

    private Gear gearAux;

    private Throttle throttleAux;

    private Regime regimeAux;

    public CreateVehicleListController() {
        vehicleList = new ArrayList<>();
    }

    /**
     *
     *
     * @param mc
     * @param xmlFile
     */
    public void CreateVehicleListController(MainController mc, File xmlFile) {

        vehicleList = new ArrayList<>();

        ImportVehicleXML.readXML(this, xmlFile);

        if (!vehicleList.isEmpty()) {
            this.setListOfVehicles(vehicleList);
            mc.setActivateVehicle(vehicleAux);
            mc.getActiveProject().setListOfVehicles(vehicleList);
            dadosGuardadosDeVeiculosAposCriacaoProjeto(mc);

        } else {
            JOptionPane.showMessageDialog(null, "Corrupt vehicle file!");
        }
    }

    /**
     * Returns the list of vehicles
     *
     * @return list of vehicles
     */
    public List<Vehicle> getListofVehicles() {
        return this.vehicleList;
    }

    /**
     * Modifies the list of vehicles of the project
     *
     * @param listOfVehicles the listOfVehicles to set
     */
    public void setListOfVehicles(List<Vehicle> listOfVehicles) {
        this.vehicleList = listOfVehicles;
        
    }

    public void addVehicle(Map parameters) {

        vehicleAux = new Vehicle();

//        vehicleAux.setName(CreateVehicleParameterHandler.getStringInsideQuotationMark((String) parameters.get("name")));
//        vehicleAux.setDescription(CreateVehicleParameterHandler.getStringInsideQuotationMark((String) parameters.get("description")));
        vehicleAux.setName((String) parameters.get("name"));
        vehicleAux.setDescription((String) parameters.get("description"));
//        vehicleAux.setName("name");
//        vehicleAux.setDescription("des");

        vehicleList.add(vehicleAux);

    }

    public void addVehicleAttributes(Map parameters) {

        vehicleAux.setType(CreateVehicleParameterHandler.getVehicleType((String) parameters.get("type")));
        vehicleAux.setMotorization(CreateVehicleParameterHandler.getMotorization((String) parameters.get("fuel")));
        vehicleAux.setMass(CreateVehicleParameterHandler.getMass((String) parameters.get("mass")));
        vehicleAux.setLoad(CreateVehicleParameterHandler.getLoad((String) parameters.get("load")));
        vehicleAux.setDragCoefficient(CreateVehicleParameterHandler.getDrag((String) parameters.get("drag")));
        vehicleAux.setFrontalArea(CreateVehicleParameterHandler.getFrontalArea((String) parameters.get("frontal_area")));
        vehicleAux.setRrc(CreateVehicleParameterHandler.getRrc((String) parameters.get("rrc")));
        vehicleAux.setWheelSize(CreateVehicleParameterHandler.getWheelSize((String) parameters.get("wheel_size")));

    }

    public void addVelocityLimit(Map parameters) {

        VelocityLimit vl1 = new VelocityLimit();
        vl1.setSectionType(CreateVehicleParameterHandler.getSectionType((String) parameters.get("segment_type")));
        vl1.setSpeedLimit(CreateVehicleParameterHandler.getMass((String) parameters.get("limit")));

        vehicleAux.addVelocityLimit(vl1);

    }

    public void addVehicleEnergyAttributes(Map parameters) {

        energyAux = new Energy();

        energyAux.setMinRpm(CreateVehicleParameterHandler.getMinRpm((String) parameters.get("min_rpm")));
        energyAux.setMaxRpm(CreateVehicleParameterHandler.getMaxRpm((String) parameters.get("max_rpm")));
        energyAux.setFinalDriveRatio(CreateVehicleParameterHandler.getFinalDriveRatio((String) parameters.get("final_drive_ratio")));
        energyAux.setEnergyRegenerationRatio(CreateVehicleParameterHandler.getEnergyRegenerationRatio((String) parameters.get("energy_regeneration_ratio")));

        vehicleAux.setEnergy(energyAux);
    }

    public void addGear(Map parameters) {

        gearAux = new Gear();

        gearAux.setGearID(CreateVehicleParameterHandler.getStringInsideQuotationMark((String) parameters.get("id")));

        energyAux.addGear(gearAux);

    }

    public void addGearRatio(Map parameters) {

        gearAux.setRatio(CreateVehicleParameterHandler.getGearRatio((String) parameters.get("ratio")));
    }

    public void addThrottle(Map parameters) {

        throttleAux = new Throttle();

        throttleAux.setId(CreateVehicleParameterHandler.getStringInsideQuotationMark((String) parameters.get("id")));

        energyAux.addThrottle(throttleAux);

    }

    public void addThrottleRegime(Map parameters) {

        regimeAux = new Regime();

        regimeAux.setTorque(CreateVehicleParameterHandler.getTorque((String) parameters.get("torque")));
        regimeAux.setRpmLow(CreateVehicleParameterHandler.getRpmLow((String) parameters.get("rpm_low")));
        regimeAux.setRpmHigh(CreateVehicleParameterHandler.getRpmHigh((String) parameters.get("rpm_high")));

        regimeAux.setSfc(CreateVehicleParameterHandler.getSfc((String) parameters.get("sfc")));

        throttleAux.addRegime(regimeAux);
    }

    //para efeitos de teste
    public void dadosGuardadosDeVeiculosAposCriacaoProjeto(MainController mc) {
        System.out.println("==TESTE DE LEITURA DE FILE VEÍCULOS==");
        System.out.println("Tamanho da lista de veículos: " + this.vehicleList.size());
        System.out.println("N.º de veículos de projeto ativo: " + mc.getActiveProject().getListOfVehicles().size());
        for (Vehicle vehicle : mc.getActiveProject().getListOfVehicles()) {
            System.out.println("\nNome do veículo: " + vehicle.getName());
            System.out.println("Descrição de veículo: " + vehicle.getDescription());
            System.out.println("Type: " + vehicle.getType());
//            System.out.println("Motorization: " + vehicle.getMotorization());
            System.out.println("Fuel: " + vehicle.getMotorization().toString());
            System.out.println("Mass: " + vehicle.getMass());
            System.out.println("Load: " + vehicle.getLoad());
            System.out.println("Drag: " + vehicle.getDragCoefficient());
            System.out.println("frontal_area: " + vehicle.getFrontalArea());
            System.out.println("rrc: " + vehicle.getRrc());
            System.out.println("wheel_size: " + vehicle.getWheelSize());

            //FALTA CONFIRMAR VELOCITY LIMIT LIST
            System.out.println("==Energy==");
            System.out.println("min_rpm: " + vehicle.getEnergy().getMinRpm());
            System.out.println("max_rpm: " + vehicle.getEnergy().getMaxRpm());
            System.out.println("final_drive_ratio: " + vehicle.getEnergy().getFinalDriveRatio());
            System.out.println("energy_regeneration_ratio: " + vehicle.getEnergy().getEnergyRegenerationRatio());

            System.out.println("Number of Gears: " + vehicle.getEnergy().getGearList().size());
            for (Gear gear : vehicle.getEnergy().getGearList()) {
                System.out.println("Gear ID: " + gear.getGearID());
                System.out.println("Gear ratio: " + gear.getRatio());
            }

            System.out.println("Number of throttles: " + vehicle.getEnergy().getThrottleList().size());
            for (Throttle throttle : vehicle.getEnergy().getThrottleList()) {
                System.out.println("throttle id: " + throttle.getId());
                System.out.println("Number of Regimes: " + throttle.getRegimeList().size());
                for (Regime regime : throttle.getRegimeList()) {
                    System.out.println("torque: " + regime.getTorque());
                    System.out.println("rpm_low: " + regime.getRpmLow());
                    System.out.println("rpm_high: " + regime.getRpmHigh());
                    System.out.println("SFC: " + regime.getSfc());
                }
            }
        }
    }

}
