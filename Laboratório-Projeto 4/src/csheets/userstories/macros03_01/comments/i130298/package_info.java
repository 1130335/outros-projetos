package csheets.userstories.macros03_01.comments.i130298;

/**
 * Technical documentation regarding the user story:Base Operatores 
 * <br/>
 * <br/>
 * 
 * <b>Requirement</b><br/>
 * Implement the add and subtraction operatores having the cells range like operands. The result of operations will be matrixs,
 * That result matrixs can be used like operands of funcTions that can apply to the range cells, like AVERAGE. For example: "=AVERAGE(A1:B2+C1:D2)".	
 * <br/>
 * <br/>
 *  
 * <b>S130a: Analysis</b><br/>
 * The conclusion that i get is the user can select a cells range and he can add all cells or subtract, first i need to see what´s the active  
 * spreadsheet, that spreadsheet have active cells and are that cells that i need allow to add or subtract. Then i need to create a result matrix
 * and she will save the values of operations. The matrix can be the operands of functions.
 * <br/>
 * <br/>
 * 
 * <b>S130d: Design</b><br/>
 * <br/><br/>
 * <img src="">
 * <br/>
 * 
 * <img src="">
 * <br/>
 * <br/>
 * 
 * <b>S130c: Coding</b><br/>
 * 
 * <br/>
 * <br/>
 * 
 * <b>S130u: Unit Tests</b><br/>
 * 
 * <br/>
 * <br/>
 * 
 * <b>S130f: Functional Tests</b><br/>
 * <br/>
 * <br/>
 * <br/>
 * 
 */
class _Dummy_{
    

}
