/**
 * Technical documentation regarding the user story core01_01: Activate and
 * Deactivate Extensions
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * 1- A window for the user to select which versions of the extensions he wants
 * to load 2- A window for the user to select which extensions he wants
 * activated and deactivated, by the information provided.
 *
 * <br/>
 * <br/>
 * <b>Use Case "Extensions auto-description": Before the application starts, the
 * user may select which versions of the extensions he want to load. After this,
 * a window pops up and the user is able to choose if he wants to activate or
 * deactivate any extension. He is able to read all the important informations
 * about the extension. The application will start with all the preferences he
 * opted.</b>
 *
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis </h2>
 *
 *
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case:
 *
 *
 * <img src="doc-files/core01_02_class_diagram.png">
 *
 *
 * <br/>
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how versions will be selected:
 *
 * <img src="doc-files/core01_02_design.png">
 *
 *
 * <b>Sequence Diagram</b> illustrating how extensions will be load or not:
 *
 * <img src="doc-files/core01_02_design.png">
 *
 *
 * <h2>5. Coding</h2>
 *
 *
 *
 *
 * <h2>6. Tests </h2>
 *
 * Since this use case is mostly based on showing the user, which versions of
 * the extensions he prefers to load, we must make sure all versions appear in
 * the first window for the user to choose before the application starts.
 *
 * Also, we need to verify that all characteristics of the extensions appear in
 * a window and are able to activate or deactivate.
 *
 *
 *
 * @author i130522
 */
package csheets.userstories.core01_02.auto_description_extensions.i130522;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
