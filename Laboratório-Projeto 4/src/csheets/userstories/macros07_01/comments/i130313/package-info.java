/**
 * Technical documentation regarding the user story macros07_01: Create Form.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * There must be a window that allows to 'describe'/'draw' a form. The
 * formularies are windows that allow to data entry. The forms should be simple.
 * They must be made of lines. In each line should be able to set two visual
 * components. The visual components supported are : button, edit box and static
 * text box. In form editing window should be a 'play' button to test the form.
 * For now only need to support one form. The form is associated with a
 * spreadsheet. For it is no longer necessary to persist the form. It should be
 * possible to invoke the 'display' of a form in the formulas and macros in
 * through a ' proper ' function that is passed the name of the form. The form
 * should have an option to ' close' . When you close the form the function that
 * invoked it ends.
 * <br/>
 * <br/>
 * <b>Use Case "Create Form":</b>
 * The user starts create form. The system displays a editable form creator so
 * the user can choose the new form's components. The user selects components he
 * wants in the form. The user selects the create option.The system generate the
 * form and add it to the active spreadsheet.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * work.<br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="./doc-files/uc_realization1.png"/>
 * <br/>
 * <br/>
 *
 * <h3>Analysis of Technical Problem</h3>
 * We can see a class diagram of the domain model of the create form
 * functionality
 * <img src="doc-files/domain_model.png"/><br/>
 *
 * <br/>
 * <h2>3. Design</h2>
 * <h3>Create Form</h3>
 * How the action create form works <br/>
 * <img src = "doc-files/uc_design_createForm.png"/>
 *
 * <h2>4. Coding</h2>
 * see:<br/>
 * Add new Class (function) called Display function
 *
 * * public class Display implements Function {
 *
 *
 *
 * The function's parameter: form's name
 *
 * public static final FunctionParameter[] parameters = new FunctionParameter[]{
 * new FunctionParameter(Value.Type.UNDEFINED, "Term", false, "A sequence of
 * instructions") };
 *
 * public String getIdentifier() { return "DISPLAY"; } ... }
 * <br/>
 *
 * <h4>Added new if clause to Excel ExpressionCompiler</h4>
 * <br/>
 * else if (function.getIdentifier().equalsIgnoreCase("display")) {
 *
 * JFrame form = cell.getSpreadsheet().getForm(); if (form != null) { if
 * (form.getTitle().equals(node.getChild(0).getText())) {
 * form.setLocationRelativeTo(null); form.setVisible(true); } } return new
 * FunctionCall(function, new Expression[]{convert(cell, node. getChild(0))});
 * <br/>
 *
 *
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 *
 *
 * <br/>
 * <br/>
 * <h2>6. Tests</h2>
 * A new test was cread and it shows how to use the interface Line Item used to
 * make a line of the form. It is importante so future changes can be made with
 * just a few changes.
 *
 */
package csheets.userstories.macros07_01.comments.i130313;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author Paulo Andrade
 */
class _Dummy_ {
}
