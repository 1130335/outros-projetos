/**
 * Technical documentation regarding the user story crm05_01: edition of
 * calendar
 * <br/>
 * <br/>
 *
 * <h2> 1. Requirement</h2>
 *
 * Window that allow creation, editing and removal calendars . Calendars has a
 * name(eg. work, Birthdays , houses , etc.) and a short description and has
 * an associated. Must be possible to select a color associated to
 * each calendars ( from a " pallet " ) .Should update contact
 * to allow events to be associated with some calendar. Events have a duration too.
 * <br/>
 * <br/>
 * <b>Use Case "edition of calendar":</b>
 *
 * Window that will allow the user to create, edit and remove calendars. The
 * calendars will have a name and a small description with association to a
 * specific contact. All calendars have a color. The events will have a duration
 * time.
 *
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * The user select create, edit or remove after choose the type of person that
 * want edit the calendar. Selects what contact want create a calendar. Chooses
 * the calendar attributes and create calendar with association. To edit the
 * user select edit, also chooses the contact but this time select one of the
 * calendars associate to a chose contact and edit the the contact parameters.
 * When contact is chose his events are showed to the user in order to allow
 * him/her to add events of this contact to the calendar. To remove is very
 * simple. The user just have to choose the calendar after choose the contact.
 * If this calendar have associated events here the relationship between events
 * and calendars will be cut.
 *
 * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>
 * <h2>User selects create Calendar</h2>
 * The follow sequence diagram is about create a calendar, the user starts the
 * application, choose extensions, choose type of contact, create calendar.
 * <img src="doc-files/CreateCalendarSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects edit Calendar</h2>
 * The second sequence diagram is about edit a calendar, the user choose a
 * contact and he can edit his information.
 * <img src="doc-files/EditCalendarSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects remove Calendar</h2>
 * The follow sequence diagram about remove, the user choose a calendar from the
 * list and remove it.
 * <img src="doc-files/RemoveCalendarSequenceDiagram.png">
 * <br/>
 * <h3>Analysis of CRM Technical Problem</h3>
 * The problem of this issue was only keep simple all the methods with all the
 * relationships between classes that were persisted.
 *
 * <h2>3.Tests</h2>
 * <br/>
 * <b>4. Unit Tests</b>
 * <br/>
 * The tests that were made are:<br/>
 * testAddCalendar * <br/>
 * testUpdateCalendar * <br/>
 * testGetCalendarByIndex //method created only to verify if the list of
 * calendars is truthful * <br/>
 * testRemoveCalendar() * <br/>
 * testAllCalendars() * <br/>
 * testAllCalendarsByContact() * <br/>
 * <br/>
 * <b>4. Functional Tests</b>
 * <br/>
 * <br/>
 * Create Calendar Menu
 * <br/>
 * All the functional tests were done with success. The main problem in those tests were 
 * don't allow the user choose options that create errors.
 * <br/>
 * <br/>
 * <br/>
 * <h2>4. Coding</h2>
 * <br/>
 * <h2>4. Final Remarks</h2>
 * This Use Case does not have any extra's implemented.
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.crm05_01.comments.i120311;

/**
 *
 * @author Daniel Oliveira
 */
class _Dummy_ {
}
