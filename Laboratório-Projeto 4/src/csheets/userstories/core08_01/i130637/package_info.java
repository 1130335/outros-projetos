package csheets.userstories.core08_01.i130637;

/**
 * 
 * <h2>Requirements</h2>
 * 
 * It should be possible to export the information from a workbook, page(s) or part of a page to a XML file.
 * To make the process lighter the solution should not use external libraries. There should be a window that alows
 * to configure the tags to use for each element. In this use case it can be usefull to explore the use
 * of design patterns that "help" the execution of an "elegant" solution. Right now it should only export
 * "text" from the cells. The export option should appear in the "File" menu.
 *  
 * <br/>
 * <br/>
 * 
 * <h2>Analysis</h2>
 * 
 * When the user selects the export to a xml file option, a window will appear asking what he wants to export
 * if its the workbook, page(s) or part of a page, there he will configure the tags used in the xml file
 * with the tags "workbook", "page", "cell", "column", "row" and "text". After the user chooses what to
 * export he will choose where to save the file and its name.
 * 
 * <br/>
 * 
 * <img src="doc-files/core08_01_AnalysisDiagram.png">
 * 
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h2>Design</h2>
 * 
 * In the Design for this use case, I made an Sequence Diagram and a Class Diagram that explain
 * how it works from the inside after the user chooses what to export.
 * From the active Workbook i will get the desired Spreadsheets and the desired Cells and with that
 * I can get it's Address and content to be exported.
 * <br/>
 *
 * <h3>SD</h3>
 * <br/>
 * <img src="doc-files/core08_01_DesignDiagram.png">
 * 
 * <br/>
 * <br/>
 * 
 * <h3>CD</h3>
 * <br/>
 * <img src="doc-files/core08_01_ClassDiagram.png">
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Testes</h2>
 * 
 * <h3>Here appears images that explain how my funcitonal tests work and they have a breif text with them.</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/Primeira_Spreadsheet.png">
 * 
 * <br/>
 * <br/>
 * <h3>Here I have the first page where I put two cells with information and the rest without.</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/Segunda_Spreadsheet.png">
 * 
 * <br/>
 * <br/>
 * <h3>Here I have the second page where I put two cells with information and the rest without.</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/Terceira_Spreadsheet.png">
 * 
 * <br/>
 * <br/>
 * <h3>Here I have the third page where I didn't put any cell with information.</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/Selecionar__Exportar.png">
 * 
 * <br/>
 * <br/>
 * <h3>Here I have the option to export a Workbook, Spreadsheet(s) or a selection of the actual Spreadsheet.</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/Formato_Base_do_XML.png">
 * 
 * <br/>
 * 
 * <h3>Here I have the result of a selection of the second Spreadsheet.
 * Here isn't the other results because of size issues, but the base format is the
 * same for all of them.</h3>
 * 
 * 
 * 
 * 
 * 
 */
public class package_info {
    
}
