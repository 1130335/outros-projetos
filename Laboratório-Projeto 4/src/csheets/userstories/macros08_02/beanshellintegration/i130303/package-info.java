/**
 * Documentation regarding to the use case Macros08_02 : BeanShell Integration
 * <br/>
 * <br/>
 *
 * <b>1. Requirement</b>
 * <br/>
 * <br/>
 * There should be a feature to invoke beanshell scripts through macros and formulas. 
 * The function to run a script should allow to do it synchronously or asynchronously. 
 * Synchronously, the function is waiting for the script to finish. 
 * Asynchronously, the function returns immediately; beanshell the script and formula / macro execute in parallel.
 * The variables that exist should be able to be accessed in beanshell script itself by its name. 
 * Changes must also be automatically incorporated into variables. In the case of synchronous invocation, the function returns the value 
 * should be the ultimate expression of beanshell script.
 * <br/>
 * <br/>
 * <b>2. Analysis</b>
 * <br/>
 * <br/>
 * The user must have the option to use a beanshell script through macros and formulas.<br/>
 * Two options are required: to run a script synchronously or asynchronously. <br/>
 * In the first, the fuction returns when the script finish.<br/>
 * In the second, the fuction returns immediately and the script and the rest of the formula/macro run in parallel (background).<br/>
 * <br/>
 * To simplify the UC, my interpretation is to only use script files excluding scripts used like macros.<br/>
 * <br/>
 * To allow the user to use this functionality two new operators need to be created. (First ideia) Example: "=BSS_ASY(name_file)" and "=BSS_SYN(name_file)".<br/>
 * <br/>
 * If the user selects the synchronous option the last value from the script is returned.<br/>
 * <br/>
 * <br/>
 * <b>3. Design</b>
 * <br/>
 * <br/>
 * 
 *  DESIGN STUFF HERE
 * 
 * <br/>
 * <br/>
 * Class Diagram
 * <br/>
 * <br/>
 *  CLASS DIAGRAM HERE
 * <br/>
 * <br/>
 * <b>3. Tests</b>
 * <br/>
 * <br/>
 * 
 *  TESTS STUFF HERE
 * 
 * <br/>
 * <br/>
 */
package csheets.userstories.macros08_02.beanshellintegration.i130303;

/*
 * @author Rui Freitas 1130303
 */
class _Dummy_ {

}
