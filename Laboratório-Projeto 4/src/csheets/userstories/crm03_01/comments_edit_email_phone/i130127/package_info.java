/**
 * Technical documentation regarding the user story crm01_01: edit contact
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Window that allows for creating, editing and removing emails and a contact
 * telephone . It should only be possible to register e-mail and correct
 * syntactically phones. Emails must be a primary and several secondary. For
 * phones should be : fixed work , fixed home, mobile working , mobile workforce
 * . The application must validate the target phones according to the country
 * code.
 * <br/>
 * <br/>
 * <b>Use Case "Edit Emails and Phone Numbers":</b>The user selects the option
 * he want to use, Create, edit or remove. The system displays the other
 * instances of cleansheet available in a new window. The window that user
 * selected is shown and the user starts the process. When the process is
 * completed a message will pop-up and it will finish the apllication.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * My analysis divides this user case in four parts: create email/phone number,
 * edit email/phone number, remove email / phone number and implement the
 * validations. In general, my user case allows the user to create, edite or
 * remove a email/ phone numbers. If we wants to create he needs to introduce
 * first name, last name and image of the contact first, and then advance to the
 * insertion of email and phone numbers. To edit a contact he need to choose a
 * contact from the list and edit the parameteres. Remove is almost the same
 * that edit.
 * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3 * + * <h2>User selects
 * Create Email/PhoneNumber</h2>
 *
 * The follow sequence diagram is about create an Email/PhoneNumber, the use
 * starts the aplication, choose
 * extensions/EmailPhoneNumber/EmailPhoneNumberMainMenu/Create, selects the
 * contact for the Email/PhoneNumber and fills the parameteres asked.
 *
 * <img src="doc-files/CreateEmailPhoneNumberSequenceDiagram.png">
 * <br/>
 *
 *  * <h2>User selects EditEmail/PhoneNumber </h2>
 *
 * + * The second sequence diagram is about edit an Email/PhoneNumber, as i
 * said, the user choose a conctact and he can edit the information about his
 * two parameters.
 *
 * + * <img src="doc-files/EditEmailPhoneNumberSequenceDiagram.png">
 * <br/>
 * + * <h2>User selects Remove Email/PhoneNumber</h2>
 * The follow sequence diagram about remove, the user choose a contact from the
 * list, then selects witch elements he wants to remove and then click remove.
 *  * <img src="doc-files/RemoveEmailPhoneNumberfromContactSequenceDiagram.png">
 *
 * <br/>
 *
 *
 * <h3>Analysis of the Use Case Problems</h3>
 *
 * In this Use Case i have some troubles. In the structure between Email and
 * phone Number and the time was the enemy, in a week that the class have spent
 * a lot of time with conflit in Merges, etc...
 *
 * <br/>
 *
 * <h2>3. Tests</h2>
 *
 * see: <code>csheets.ext.contacts.contactsTest</code><br/>
 *
 *
 * <h2>6. Final Remarks</h2>
 * I dont have any consideration or extra to say.
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.crm03_01.comments_edit_email_phone.i130127;

/**
 *
 * @author Miguel Teixeira
 */
class _Dummy_ {

}
