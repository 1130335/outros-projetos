/**
 * Technical documentation regarding the user story core03_02:
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Sort an area of ​​cells. The ordering should be done by selecting a column of the area. It should be possible to indicate
 * the order: ascending or descending . The interaction with the user should be based on a "popup menu "
 * <br/>
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * In this use case i need to use the same methodology that was used in the previous use case. The application should allow
 * the user to sort a group of cells. To achieve that goal i need to get the text from the cells and order it.
 * <br/>
 * <br/>
 * <img src="doc-files/core03_02_analysis.png">
 * <br/>
 * <br/>
 *
 * <h2>Functional Tests</h2>
 *
 * <img src="doc-files/teste1.PNG">
 *
 * <img src="doc-files/teste3.PNG">
 *
 *
 * <br/>
 * <br/>
 *
 * <h3>Sort</h3>
 *
 * <img src="doc-files/teste2.PNG">
 *
 *
 *
 *
 * <h2>3. Design</h2>
 *
 *
 * <img src="doc-files/core03_02_Design.png">
 *
 *
 *
 * <h2>Class Diagram</h2>
 *
 * <img src="doc-files/core03_02_class_diagram.png">
 *
 * <br/>
 * <br/>
 *
 * @author Pedro Magalhaes
 */
package csheets.userstories.core03_02.SortGroupOfCells.i130487;

/**
 *
 * @author Utilizador
 */
class _Dummy_ {

}
