/**
 * Technical documentation regarding the user story core 2.3: Comment Navigator
 * <br/>
 * <br/>
 *
 * <b>Requirement</b><br/>
 * Needs to exist at least a comment available for search.
 * <br/>
 * <br/>
 *
 * <b>Analysis</b><br/>
 * 2ª Análise<br/>
 * Fazer um explorador de comentários para as células:<br/>
 * - Adicionar ao menu uma opção (extensão) apresentando uma nova janela com a
 * pesquisa a efectuar;<br/>
 * - Uma Janela nova com o explorador dos comentários bem como uma função de
 * pesquisa para procurar comentários;<br/>
 *
 * Análise Final<br/>
 * - O explorador mostrará todas as células comentadas apenas para a sheet
 * activa e não para todas as sheets existentes.<br/>
 * - Explorador será implementado através de uma JTree sendo o "root" a sheet
 * seleccionada e os "ficheiros filho" as células comentadas.<br/>
 * - Haverá uma excepção lançada para caso não hajam nenhuns comentários
 * existentes.<br/>
 * <br/>
 * <br/>
 *
 *
 * <b>Design</b><br/>
 * <img src="../../../csheets/userstories/core02_03/Navegador_Comentarios/i121248/doc-files/core_02_03_design1.png">
 * <br/>
 * <br/>
 *
 * <b>Coding</b><br/>
 * see:<br/>
 * <a href="../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><br/>
 * <a href="../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a><br/>
 * <br/>
 * <br/>
 *
 * <b>Unit Tests</b><br/>
 * see:<br/>
 * Test Packages.csheets.ext.comments.ui<br/>
 * <br/>
 * <br/>
 *
 * <b>Functional Tests</b><br/>
 * To test this user story, the user should follow these steps:<br/>
 * 1- run cleansheets;<br/>
 * 2- click on the Extensions menu and select Comments. There must appear a list
 * of menu options containning one option for the Comment Navigator
 * sidebar;<br/>
 * 3- navigate through the JTree to check the comments within the cells;<br/>
 * 4- also earch for matching words in the JTextField for comments. <br/>
 * 5- it should appear the matching comments with also their respective cells
 * thye're currently in.<br/>
 * 6- close the window.<br/>
 * <br/>
 * <br/>
 *
 * @author 1121248DiogoVieira
 */
/*
 *
 @startuml doc-files/core02_03_design1.png
 participant "uic : UIController" as UIC
 participant ExtensionManager as ExtM
 participant "extension : CommentsExtension" as EComments
 participant "uiExtension : UIExtensionComments" as UIExt
 participant "commentMenu : CommentMenu" as CMenu
 participant "cmtAction : CommentAction" as CAct
 participant "CommentNavigatorJDialog : JDialog" as cnjd
 UIC -> ExtM : extensions=getExtensions();
 loop for Extension ext : extensions
 UIC -> EComments : uiExtension=getUIExtension(this);
 activate EComments
 create UIExt
 EComments -> UIExt : new(extension, uic)
 deactivate EComments
 UIExt -> UIExt : getMenu();
 activate UIExt
 create CMenu
 UIExt -> CMenu: new(commentMenu)
 deactivate CMenu
 create CAct
 CMenu -> CAct : new(cmtAction)
 CAct -> CAct : actionPerformed()
 create cnjd
 CAct -> cnjd : new(CommentNavigatorJDialog)
 deactivate cnjd
 deactivate CAct
 deactivate UIExt
 UIC -> UIC : uiExtensions.add(uiExtension);
 end
 @enduml
 *
 */
package csheets.userstories.core02_03.Navegador_Comentarios.i121248;
