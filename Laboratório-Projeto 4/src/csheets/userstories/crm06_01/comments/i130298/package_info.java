
package csheets.userstories.crm06_01.comments.i130298;

/**
 * Technical documentation regarding the user story: Alerts edit
 * <br/>
 * <br/>
 * 
 * <b>Requirement</b><br/>
 * Window that allows the creation, edition and removel of alerts.
 * An alert have a name, description and a timestamp. The aplication must allow only valid timestamps.
 * Must be possible list the existing alerts. When the arrives the alert moment, the aplication must show an alert(window like "popup"). 
 * In that window must appear the name and description and a button to cloese and other to remember in 5 minutes. When creates events must be possible create an alert to the event.
 * <br/>
 * <br/>
 *  
 * <b>S130a: Analysis</b><br/>
 * The user can create, edit and remove an create an alert. For that i will implement on the menu a window with a simple interface. In that interface will have a camp to introduce the name, description. date and hour.
 * The date will be in the format:dd/mm/yyyy and the hour in: hh:mm. 
 * After he creates the alert he can edit or remove him. When we choose edit, he can edit all the elemnts: name, description, etc. In the remove he only select the alert and remove it.
 * <br/>
 * 
 * 
 * <b>S130d: Design</b><br/>
 * My design is separated in 3 analyzes, create alert, edit alert and remove alert.
 * <br/>
 * <b>Create alert</b><br/>
 * <img src="doc-files/sequencediagram1.png"/>
 * <br/>
 * <b>Edit alert</b><br/>
 * <img src="doc-files/sequencediagram2.png"/>
 * <br/>
 * <b>Remove alert</b><br/>
 * <img src="doc-files/sequencediagram3.png"/>
 * <br/>
 * 
 * <b>S130c: Coding</b><br/>
 * 
 * <br/>
 * <br/>
 * 
 * <b>S130u: Unit Tests</b><br/>
 * <br/>
 * In the test i test the creation of timestamp introducing the hour and minutes, i tested to the validation of the date introducing the date in the form:dd/mm/yyyy
 * see: <code>csheets.ext.crm.AlertTest</code><br/>
 * <br/>
 * 
 * <b>S130f: Functional Tests</b><br/>
 * <br/>
 * <br/>
 * <br/>
 * 
 */
public class package_info {
    
}
