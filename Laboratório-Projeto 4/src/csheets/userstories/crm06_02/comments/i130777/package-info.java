/**
 * Technical documentation regarding the user story ipc06_02: Task Edition.
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Window that allows for creating, editing and removing tasks. 
 * A task has a name ,a brief description and is associated with
 * a contact. A task also has a level priority (1 to 5) and a 
 * percentage of execution. There must be a sidebar window to list
 * the tasks relating to a contact (must show name, priority and
 * rate of implementation). It should be possible to edit / remove 
 * a selected task  from this window. Also it should be possible to
 * sort tasks by  priority or  date and filter by rate of  percentage
 * of execution (always for a particular contact).
 * <br/>
 * <br/>
 * <b>
 * <br/>
 * <br/>
 * 
 * <h2>2. Analysis</h2>
*The Analysis is about create ,edit and delete a task. 
* there is a menu for tasks where you have 3 options :
* create,edit and delete. if you select the create option
* ,there is a new window than you can insert the datas for
* create a new task. if you select the edit option you have
* a new window where you have a list of created task and you
* choose a task ,the system will show you the datas of the 
* selected task and you can edit them. And for the delete 
* option you have a new window where you have a list of created
* task you choose one ,system show you the datas from the selected
* task and you confirm.Also there is a sidebar where you can have 
* the list of tasks sorted by date or priority ,and there is possibility
* to filter this list with contact or percentage you can edit or
* delete a task from that sidebar too.
* <h3>3.sequence diagrams </h3>

 * <h2>User selects create option</h2>
*The follow sequence diagram is about create a task,
* the user select the create option .system will create
* a new  newTask window and controller calss ,newtask
* get the list of contact from controller,and ask the
* user for insert the datas ,user insert the datas and
* newTask will send them to controller ,this class will
* create a new task with datas and add the task in repository
* calss *
* <img src="doc-files/CreateTaskDiagram.png"> 
 * <br/>
 * <h2>User selects edit option</h2>
*The follow sequence diagram is about edit a task,
* the user select the edit option .system will create
* a new editTask window and controller class ,editTask
* get the list of tasks from controller,and ask the
* user for choice one for edit,user select one ,editTask 
* will get the task datas form task class and show them to
* user, user will edit the datas , and editTask will send
* them to controller ,this class will edit task with dataset
* and update the task in repository class
* <img src="doc-files/EditTaskDiagram.png"> 
 * <br/>
 * <h2>User selects delet option</h2>
*The follow sequence diagram is about delet a task,the
* user select the delet option .system will create a new
* deleteTask window and controller class ,deleteTask get
* the list of tasks from controller,and ask the user for
* choice one for delete,user select one ,deleteTask will
* get the task's datas form task class and show them to 
* user, user will confirm  , and deleteTask will send thet
* task to controller ,this class will delet the task in 
* repository class.
* <img src="doc-files/DeleteTaskDiagram.png"> 
 * <br/>
 * 
 
 * 

 * <h3>Class Diagram</h3>
 
 * 
* <img src="doc-files/taskClassDiagram.png"> 
 * <br/>
 * <h2>3. Tests</h2>
 * <br> 
 * <br/>
 
 * 
 */
package csheets.userstories.crm06_02.comments.i130777;

/**
 *
 * @author Aria
 */
 class _Dummy_ {
    
}
