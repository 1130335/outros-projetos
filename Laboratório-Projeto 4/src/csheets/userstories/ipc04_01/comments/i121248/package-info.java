/**
 * Documentacao tecnica para user story IPC04.01: importar ficheiro texto.
 *
 * * @author 1121248DiogoVieira
 * <br/>
 * <br/>
 *
 * <b>Requirement</b><br/>
 * Deve ser possível importar/exportar dados de um ficheiro de texto, cujas
 * colunas são divididas por um caractere que se deve especificar na janela
 * wizard. Deve-se ainda especificar se existe uma primeira linha de cabeçalho
 * ou não e se esta deve ser incluída na operação. No caso da importação, os
 * dados devem aparecer a partir de uma célula que é identificada como o canto
 * superior esquerdo. No caso da exportação, os dados devem ser exportados a
 * partir de uma célula que é identificada como canto superior esquerdo.
 * <br/>
 * <br/>
 *
 * <b>IPC04.1: Analise</b><br/>
 * Analisar como e que se insere um botao correspondente a importacao de
 * ficheiros na . <br/>
 * Estudar como e seleciona a celula que corresponde ao canto superir esquerdo.
 * <br/>
 * Aplicar um explorer do windows para saber a localizacao do ficheiro.<br/>
 * Ler o conteudo do ficheiro, interpretar e submeter os dados na aplicacao.
 * <br/>
 * <br/>
 *
 * <b>IPC04.1: Design</b><br/>
 * Para realizar esta user story preciso de criar uma subclasse de "Extension".
 * Também vai ser preciso uma subclasse de "UIExtension". Para implementar este
 * caso de uso preciso de um JFileChooser.<br/>
 * O diagrama seguinte mostra como a nova extensão vai ser integrada no
 * "cleansheets".<br/><br/>
 * <img src="../../../csheets/userstories/csheets/userstories/ipc04_01/comments/i121248/doc-files/ipc041_01_design1.png">
 * <br/>
 * <img src="../../../csheets/userstories/csheets/userstories/ipc04_01/comments/i121248/doc-files/ipc041_01_design2.png">
 * <br/>
 * <br/>
 * <b>Diagrama de Sequencia</b>
 * <br/>
 * <img src="../../../csheets/userstories/csheets.userstories/ipc04_01/comments/i121248/doc-files/ipc04_01_ImporFileSequenceDiagram.png">
 * <br/>
 * <br/>
 * <b>IPC04.1: Coding</b><br/>
 * see:<br/>
 * <a href="../../../csheets/ext/importFile/package-summary.html">csheets.ext.importFile</a><br/>
 * <a href="../../../csheets/ext/importFile/ui/package-summary.html">csheets.ext.importFile.ui</a><br/>
 * <br/>
 * <br/>
 *
 * <b>IPC04.1: Unit Tests</b><br/>
 * Para este caso de uso, os testes unitários não se aplicam, uma vez que os
 * resultados da execução do código não podem ser verificados.
 * <br/>
 * <br/>
 *
 * <b>IPC04.1: Functional Tests</b><br/>
 * Para testar esta user story siga os seguintes passos <br/>
 * 1- abrir o cleansheets;<br/>
 * 2- clique no menu "Extensions", e de sequida clique em "Import";<br/>
 * 3- Selecione o ficheiro que pretende importar;<br/>
 * 4- As celulas do cleansheets devem ficar com a informação do ficheiro. <br/>
 * <br/>
 * <br/>
 *
 *
 * @startuml doc-files/ipc04_01_design1.png participant "uic : UIController" as
 * UIC participant ExtensionManager as ExtM participant "extension :
 * ExtensionImportFile" as EExample participant "uiExtension :
 * UIExtensionImportFile" as UIExt participant "UIExtensionImportFile" as cp UIC
 * -> ExtM : extensions=getExtensions(); loop for Extension ext : extensions UIC
 * -> EExample : uiExtension=getUIExtension(this); activate EExample create
 * UIExt EExample -> UIExt : new(extension, uic) create cp UIExt -> cp : new
 * (uic) deactivate UIExt UIC -> UIC : uiExtensions.add(uiExtension); end
 * @enduml
 *
 * <b>S073d: Design</b><br/>
 *
 * O seguinte diagrama mostra como será o processo de integracao da extensao no
 * cleansheets.<br/><br/>
 * <img src="../../../csheets/userstories/us073/doc-files/export_extension_image1.png">
 * <br/>
 * <br/>
 * @startuml doc-files/export_extension_image1.png class ExportFileAction { }
 * class ExportFileMenu class ExportFileExtension { -String NAME; } class
 * UIExportFileExtension class JMenuItem ExportFileExtension ->
 * UIExportFileExtension : getUIExtension(UIController) UIExportFileExtension ->
 * ExportFileMenu : getMenu() ExportFileMenu -> JMenuItem : 'items' JMenuItem
 * o-> ExportFileAction : action
 * @enduml
 *
 * @startuml doc-files/export_extension_image2.png participant ExtensionManager
 * as ExtM participant Class participant "aClass:Class" as aClass participant
 * "extension : ExportFileExtension" as EExample ExtM -> Class : aClass =
 * forName("csheets.ext.simple.ExtensionExample"); activate Class create aClass
 * Class -> aClass : new deactivate Class ExtM -> aClass : extension =
 * (Extension)newInstance(); activate aClass create EExample aClass -> EExample
 * : new deactivate aClass ExtM -> EExample : name = getName(); activate
 * EExample deactivate EExample ExtM -> ExtM : extensionMap.put(name, extension)
 * @enduml
 *
 * @startuml doc-files/export_extension_image3.png participant UIController as
 * UIC participant ExtensionManager as ExtM participant "extension :
 * ExportFileExtension" as EExample participant "uiExtension :
 * UIExportFileExtension" as UIExt UIC -> ExtM : extensions=getExtensions();
 * loop for Extension ext : extensions UIC -> EExample :
 * uiExtension=getUIExtension(this); activate EExample create UIExt EExample ->
 * UIExt : new deactivate EExample UIC -> UIC : uiExtensions.add(uiExtension);
 * end
 * @enduml
 *
 * @startuml doc-files/export_extension_image4.png participant MenuBar as MB
 * participant "extensionsMenu : JMenu" as extensionsMenu participant
 * UIController as UIC participant "extension : UIExportFileExtension" as UIE
 * participant "extensionMenu : ExportFileMenu" as EM MB -> MB : extensionsMenu
 * = addMenu("Extensions", KeyEvent.VK_E); activate MB create extensionsMenu MB
 * -> extensionsMenu : new deactivate MB MB -> UIC : extensions=getExtensions();
 * loop for UIExtension extension : extensions MB -> UIE :
 * extensionMenu=extension.getMenu(); activate UIE create EM UIE -> EM : new
 * deactivate UIE MB -> EM : icon = getIcon(); MB -> extensionsMenu :
 * add(extensionMenu); end
 * @enduml
 *
 */
package csheets.userstories.ipc04_01.comments.i121248;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
