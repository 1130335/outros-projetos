/**
 * Technical documentation regarding the user story core08_02 - Import XML
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 *
 * <b>Use Case "Import XML": </b>
 * It should be possible to import the information to a workbook, page(s) or
 * part of a page from xml file. To make the process lighter the solution should
 * not use external libraries. There should be a window that alows to configure
 * the tags to use for each element. In this use case it can be usefull to
 * explore the use of design patterns that "help" the execution of an "elegant"
 * solution. Right now it should only import "text" from xml files. The import
 * option should appear in the "File" menu.
 * <br/>
 *
 *
 * <h2>Analysis</h2>
 *
 * When the user selects import from xml file option , a window will appear
 * asking what he wants to import if its the workbook, page(s) or part of a
 * page, there the user will configure the tags used in the xml file with the
 * tags "workbook", "page", "cell", "column", "row" and "text". After the user
 * chooses the tags used in xml file to import, the user will choose what xml
 * file will be imported.
 *
 * <br/>
 *
 * To understand how this option woorks is important to know how Export to XML
 * woorks:
 *
 * Check package-info of core08_01.i130637
 *
 * <br/>
 * <br/>
 *
 * <b>Analysis Diagram Import XML file</b>
 *
 * <br/>
 * <img src="doc-files/core08_02_DiagramAnalysis.png">
 *
 *
 * <h2>4. Design</h2>
 *
 * <b> Sequence Diagram Import XML file</b> Illustratin how the file is imported
 * <br/>
 * <img src="doc-files/core08_02_DesignDiagram.png">
 *
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/core08_02_ClassDiagram.png"/>
 * <br/>
 * <br/>
 * <br/>
 * Note:
 * <br/>
 * If the file was previously used for export , then the import will be based on
 * the tags used in export, and user need not to introduce them
 * <br/>
 * <br/>
 * <h2>5. Tests </h2>
 * <br/>
 *
 * <b>Functional Tests</b><br/>
 *
 * To import a workbook or test this functionality, pages, or part of
 * pages(selected cells) should follow next steps be done:
 *
 * <br/>Select File
 * <br/>
 * <br/>
 * <br/>
 * <img src="doc-files/#1.png"/>
 * <br/>
 * <br/>
 *
 * <br/>Select Import XML Option
 * <br/>
 * <br/>
 * <img src="doc-files/#2.png"/>
 * <br/>
 * <br/>
 * <br/>1 - If the user want import a workbook, the user must follow next steps:
 * <br/>
 * <br/>a. Select what import: Workbook
 * <br/>
 * <br/>b. Select file to import
 * <br/>
 * <br/>c. Select Import
 * <br/>
 * <br/>Example:
 * <br/>
 * <img src="doc-files/#3.png"/>
 * <br/>
 * <br/>After that, the user introduce the tags:
 * <br/>
 * <br/>
 * <br/>d. User introduce tag wich represent workbook in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagWorkbook.png"/>
 * <br/>
 * <br/>
 * <br/>e. User introduce tag wich represent spreadsheet in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagSpreadsheet.png"/>
 * <br/>
 * <br/>
 * <br/>f. User introduce tag wich represent cell in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagCell.png"/>
 * <br/>
 * <br/>
 * <br/>g. User introduce tag wich represent cell content in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagContent.png"/>
 * <br/>
 * <br/>
 * <br/>h. User introduce tag wich represent cell columns in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagColumn.png"/>
 * <br/>
 * <br/>
 * <br/>i. User introduce tag wich represent cell rows in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagRow.png"/>
 * <br/>
 * <br/>
 * <br/>
 * <br/>At this point, the workbook should be updated,like this:
 * <br/>
 * <br/>Spreadsheet 1
 * <br/>
 * <br/>
 * <img src="doc-files/sp1Worbook.png"/>
 * <br/>
 * <br/>
 * <br/>Spreadsheet 2
 * <br/>
 * <br/>
 * <img src="doc-files/sp2Worbook.png"/>
 * <br/>
 * <br/>
 * <br/>Spreadsheet 3
 * <br/>
 * <br/>
 * <img src="doc-files/sp3Worbook.png"/>
 * <br/>
 * <br/>
 * <br/>To do this test we used the following xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/worbook.png"/>
 * <br/>
 * <br/>
 * <br/>
 * <br/>2- If the user choose import spreadsheets, the user must follow next
 * steps:
 * <br/>
 * <br/>a. Select what import: Pages
 * <br/>
 * <br/>b. Select file to import
 * <br/>
 * <br/>c. Select Import
 * <br/>
 * <br/>Example:
 * <br/>
 * <img src="doc-files/pages.png"/>
 * <br/>
 * <br/>After that, the user introduce the tags:
 * <br/>
 * <br/>
 * <br/>d. User introduce tag wich represent workbook in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagWorkbook.png"/>
 * <br/>
 * <br/>
 * <br/>e. User introduce tag wich represent spreadsheet in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagSpreadsheet.png"/>
 * <br/>
 * <br/>
 * <br/>f. User introduce tag wich represent cell in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagCell.png"/>
 * <br/>
 * <br/>
 * <br/>g. User introduce tag wich represent cell content in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagContent.png"/>
 * <br/>
 * <br/>
 * <br/>h. User introduce tag wich represent cell columns in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagColumn.png"/>
 * <br/>
 * <br/>
 * <br/>i. User introduce tag wich represent cell rows in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagRow.png"/>
 * <br/>
 * <br/>
 * <br/>j. User introduce number of pages to impor, for this test we introduced
 * 2:
 * <br/>
 * <br/>
 * <img src="doc-files/numberofPages.png"/>
 * <br/>
 * <br/>
 * <br/>k. User introduce number of pages to import, for this test we introduced
 * 1 and 2:
 * <br/>
 * <br/>
 * <img src="doc-files/numberPage.png"/>
 * <br/>
 * <br/>
 * <br/>
 * <br/> At this point, Sheet 1 and Sheet 2 should be updated,like this:
 * <br/>
 * <br/>Spreadsheet 1
 * <br/>
 * <br/>
 * <img src="doc-files/sp1Worbook.png"/>
 * <br/>
 * <br/>
 * <br/>Spreadsheet 2
 * <br/>
 * <br/>
 * <img src="doc-files/sp2Worbook.png"/>
 * <br/>
 * <br/>
 * <br/>To do this test we used the following xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/worbook.png"/>
 * <br/>
 * <br/>
 * <br/>
 * 3- If the user choose import part of page(selected cells), the user must
 * follow next steps:
 * <br/>
 * <br/>In this case, user must select wich cells want import, per example:
 * <br/>
 * <br/>
 * <img src="doc-files/sellected.png"/>
 * <br/>
 * <br/>
 * <br/>a. Select what import: Sellected Cells
 * <br/>
 * <br/>b. Select file to import
 * <br/>
 * <br/>c. Select Import
 * <br/>
 * <br/>Example:
 * <br/>
 * <img src="doc-files/selectedCells.png"/>
 * <br/>
 * <br/>After that, the user introduce the tags:
 * <br/>
 * <br/>
 * <br/>d. User introduce tag wich represent workbook in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagWorkbook.png"/>
 * <br/>
 * <br/>
 * <br/>e. User introduce tag wich represent spreadsheet in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagSpreadsheet.png"/>
 * <br/>
 * <br/>
 * <br/>f. User introduce tag wich represent cell in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagCell.png"/>
 * <br/>
 * <br/>
 * <br/>g. User introduce tag wich represent cell content in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagContent.png"/>
 * <br/>
 * <br/>
 * <br/>h. User introduce tag wich represent cell columns in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagColumn.png"/>
 * <br/>
 * <br/>
 * <br/>i. User introduce tag wich represent cell rows in xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/tagRow.png"/>
 * <br/>
 * <br/>
 * <br/>k. User introduce number of page to import, for this test we introduced
 * 1:
 * <br/>
 * <br/>
 * <img src="doc-files/numberPage.png"/>
 * <br/>
 * <br/>
 * <br/>
 * <br/> At this point, Sheet 1 should be updated,like this:
 * <br/>
 * <br/>Spreadsheet 1
 * <br/>
 * <br/>
 * <img src="doc-files/sellectedSetted.png"/>
 * <br/>
 * <br/>
 * <br/>To do this test we used the following xml file:
 * <br/>
 * <br/>
 * <img src="doc-files/sellectedxml.png"/>
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.core08_02.ImportarXML.i130711;
/*
 * @author Sergio Leites 1130711
 */

class _Dummy_ {
}
