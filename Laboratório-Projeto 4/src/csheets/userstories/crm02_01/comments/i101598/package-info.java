/**
 * Technical documentation regarding the user story crm02_01: edit address.
 * <br/>
 * <br/>
 *
 *
 * <b>Requirement</b><br/>
 * Allows the user to create, edit and remove Addresses, associated to a certain
 * Contact.
 * <br/>
 * <br/>
 *
 * <b> Analysis: Use Case 'Edit Address'</b><br/>
 *
 * To implement this UseCase i've created a new Domain Class "Address" wich
 * creates Addresses as an object. I've also modified the methods in the class
 * Contact( added the fields mainAddress and SecAddress to constructor).
 *
 * I decided to implement this use case as an Extension instead of a
 * Sidebar(after talking with the Professor) to give it a look more
 * "User-Friendly" and for better understanding and view of every window.
 *
 * When the user wants to create an address there are some "restrictions" before
 * he can do it:<br/> - first of all to create and address the user needs to
 * insert a contact first.<br/> - The Main Address fields cannot be empty.<br/>
 * - If all fields from Main Address are filled and any SecondaryAddress field
 * is empty, the system assumes only one Address Inserted. - The user has the
 * possibility to let the system auto-fill the Area,City and Country Fields
 * based on inserted zipcode (button autofill from zipcode) <br/>
 *
 *
 * <br/>
 * <br/>
 *
 * <h1>Design</h1><br/>
 *
 * The user selects the AddressMenu in the Extension/Address Menu. The system
 * displays the options available in a new window and then the user selects the
 * option he wants: Create, Edit or Remove. For these three options the system
 * will display a different menu and ask the user to fill/edit data or simply
 * select the contact's address and remove it.
 * <br/>
 * <h2>User selects Create Address</h2>
 * The follow sequence diagram is about create an address, the use starts the
 * aplication, choose extensions/Address/AddressMainMenu/Create, selects the
 * contact for the address and fills the parameteres asked.
 * <img src="doc-files/CreateAddressSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects Edit Address</h2>
 * The second sequence diagram is about edit an Address, as i said, the user
 * choose a conctact and he can edit the information about his two adresses.
 * <img src="doc-files/EditAddressSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects Remove Address</h2>
 * The follow sequence diagram about remove, the user choose a contact from the
 * list, then selects witch address he wants to remove and then click remove.
 * <img src="doc-files/RemoveAddressSequenceDiagram.png">
 * <br/>
 * <br/>
 * <h2>Tests</h2>
 *
 * Created two classes to test the methods from Domain/Address class and
 * Persistence/JPAAddressRepository.
 *
 * Obs: The tests 'testDeleteAddress' and 'testListAll' from
 * 'JPAAddressRepositoryTest' are supposed to fail.
 * <br/>
 *
 * see: <code>csheets.ext.crm.domain.AddressTest</code><br/>
 * <code>csheets.ext.crm.persistence.JPAAddressRepositoryTest</code><br/>
 *
 *
 * <h2>Final Remarks</h2>
 * Added a button to auto-fill the fields Area,City and Country based on the
 * ZipCode inserted. That button also validates if the zipcode is valid or not
 * (zipcodes text file)
 * <br/>
 * <br/>
 *
 * @author Andre Garrido - 1101598
 */
/*

 */
package csheets.userstories.crm02_01.comments.i101598;

class _Dummy_ {
}
