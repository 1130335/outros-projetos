/**
 * Technical documentation regarding the user story core07_01: Search Text.
 * <br/>
 * <br/>
 *
 *
 * <h1>Requirement</h1><br/>
 *
 *
 * Allows the user to search words on cells in the active spreadsheet.
 * <br/>
 * <br/>
 *
 * <h1> Analysis: Use Case 'Search Text'</h1><br/>
 * <br/>
 * To implement this UseCase i'm going to create a new package
 * csheets.ext.core.SearchText which contains all the UI classes (the main ui is
 * a Sidebar) and the class to perform the search.
 *
 *
 * <br/>
 * <br/>
 *
 * <h1>Design</h1><br/>
 *
 * The user selects the SearchText option. The system will display a sidebar.
 * Then, the user inserts the word to search and click serch.
 *
 * <br/>
 * <br/>
 * <img src="doc-files/SearchTextSequenceDiagram.png">
 * <br/>
 * <br/>
 * <h2>User click SEARCH</h2>
 * When the user click0s the button, the system will search the cells that
 * contains the word inserted by the user. In the sidebar, a list is filled with
 * the cells that contains the word.
 *
 * <br/>
 *
 * <h2>User Selects a Cell from list</h2>
 * After the list is filled, the user can select a cell from that list. When he
 * selects one, the cell selected is now the ActiveCell.
 * <br/>
 * <br/>
 * <h2>Tests</h2>
 *
 * There are 3 tests implemented for this UseCase:<br/><br/>
 *
 * testGetSearchResult1 -- searches a word in the cell's content.
 * <br/>
 * testGetSearchResult2 -- searches a regular expression in the cell's content
 * <br/>
 * testGetSearchResult3 -- searches a word that don't exist in the cells (it's
 * supposed to fail)
 * <br/>
 * <br/>
 * see: <code>csheets.ext.core.SearchText.SearchTextTest</code><br/>
 * <br/>
 * <br/>
 * <h2>Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 * @author Andre Garrido - 1101598
 */
/*

 */
package csheets.userstories.core07_01.SearchText.i101598;

class _Dummy_ {
}
