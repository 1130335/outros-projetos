/**
 * Technical documentation regarding the user story crm03_02: Import/Export
 * Email Phone.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * It must be possible to import and export the phone numbers and / or emails to
 * / from a spreadsheet . It should be given the opportunity to override or add.
 * These operations can be kept active . If kept active should be update
 * whenever the source changes .
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 *  * <b>Use Case "Import/Export Email Phone":</b>
 * <b> Import<b/>
 * The user select the option import. The system shows all contacts available
 * and the mails or phone numbers that the user want to import. The user selects
 * one contact and the options. The system import the mails and the phone
 * numbers of the contact.<br/>
 * <b>Export<b/>
 * The user select the export option. The user selects one or all contacts he
 * wants to export. The system export the selected contact(s).
 * <br/>
 * <br/>
 * <b>So there will be 2 analysis diagram, one for the import other to the
 * exports<b/>
 * <br/>
 * <br/>
 *
 * <b>Import Analysis<b/>
 * <br/>
 * <img src="doc-files/import_export_import_realization.png">
 * <br/>
 * <br/>
 * <b>Export Analysis</b>
 * <br/>
 * <img src="doc-files/import_export_export_realization.png">
 * <br/>
 *
 * <br/>
 *
 * <h2>4. Design</h2>
 * From the previous diagrams we realize that to solve this user story we need
 * to divide the use case in two.Important.
 *
 * <br/>
 * <b>One for the import. In the import the user only has the option to choose
 * the line, the column is always the first one(the information fields start in
 * the first column). One to the export. Note:The user dont have the option to
 * choose not to export the first email and the personal number, since this
 * fields are not optional.
 * <br/>
 * <br/>
 * <b>Import Email Phone diagram<b/>
 * <br/>
 * <img src="doc-files/design_import.png">
 * <br/>
 *
 * <b>Export Email Phone diagram<b/>
 * <br/>
 * <img src="doc-files/design_export.png">
 * <br/>
 *
 * <h2>5. Coding</h2>
 * see:<br/>
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.crm.EmailPhone</a><br/>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.crm.ImportExport</a><br/>
 * <br/>
 * <h2>6. Tests</h2>
 * <br/>
 * To test this use case, a functional test was made.
 * <br/>
 * <b>First import email and phones to a contact <b/>
 * <br/>
 * <img src="doc-files/teste1.jpg">
 * <br/>
 * <img src="doc-files/teste2.jpg">
 * <br/>
 * <b>Second<b/>
 * <br>
 * <b>The export, if the information exported match with the information
 * imported, the test result is success.</b>
 * <br/>
 * <img src="doc-files/teste3.jpg">
 * <br/>
 * <img src="doc-files/teste4.jpg">
 * <br/>
 *
 * @author Paulo Andrade
 */
package csheets.userstories.crm03_02.import_export.i130313;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author Paulo Andrade
 */
class _Dummy_ {
}
