/**
 * Technical documentation regarding the user story ipc03_03: Search Content in
 * Workbooks
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * It should be possible to send a search request of a workbook content for all
 * the instances of cleansheets in the local network. The search should
 * contemplate only workbooks that are present in another machine hard disk.
 * There should be a sidebar window with a list that should contain the search
 * results. The list should be updated as the content is being found. In the
 * list there should be the instance of where the workbook was found, with its
 * name and the summary of the workbook's content.
 * <br/>
 * <br/>
 * <b>Use Case "Content Search":</b> The user starts the use case. The system
 * searches for instances of cleansheets in the local network, and shows them to
 * the user as they are being found, along with the their names and summaries.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since the Content search is an extension, we need to study how extensions
 * work. The system will allow instances to communicate through TCP and UDP, and
 * share information between them, such as the name ,content and summary of the
 * workbooks. For searching in the local network for other instances, we need to
 * broadcast the name and content of the workbook we want to find, so that all
 * instances of cleansheets can receive the request. The instance which
 * broadcasted the name will be receiving woorkbooks from any instance who has
 * them.
 * <br/>
 * <br/>
 * <h2>3. Tests</h2>
 * The UDPClient connection and UDPServer connection tests need to be made to
 * test if there's any connection. The file search in the Servers who receive
 * the name of the workbook needs to be tested when the connection is made.
 * <br/>
 * <br/>
 *
 * <h2>4. Design</h2>
 * The design diagram shows that we need to have a UDPClient sending the name or
 * a part of the content of the workbook it wants to find in the local network.
 * The UDPServers listen for connection requests, and when they receive the
 * request they use the FileSearch class to look for it in their HDs. If they
 * find the workbook with that name or a part in their content, they attempt for
 * a TCP connection with the instance which sent the name and send the workbook
 * through a more secure connection. In the class SearchNetworkWorkbooksMenu we
 * need to implement the connections in the action listeners of the buttons
 * "search for workbooks" and "listen for requests".
 * <br/>
 * <br/>
 * <h2>5. Coding</h2>
 *
 * <br/>
 * <br/>
 * <h2>6. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.ipc03_03.Search_Content.i130127;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
