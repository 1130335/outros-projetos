package csheets.userstories.ipc05_02.findparticipants.i130576;

/**
 * Technical documentation regarding the user story ipc01_01: initiate sharing.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * 
 * It should be possible to find instances of cleansheets in the local network for contact. 
 * It should be possible to define an icon (for example, a picture) to each participant as well as a message and a state (online or offline). 
 * The sidebar window will provide this data to the participants of the chat (whether they have sent messages or not). 
 * Each cleansheets instance should allow or not the receiveng of messages from other instances (through the state of the user). 
 * The history of messages must be persistent.
 * 
 * <br/>
 * <br/>
 * <h2>2. Analysis</h2>
 * <br/>
 * illustrating how the user interacts
 * with the system in order to find participants
 * <br/>
 * <br/>
 * 
 * <img src="doc-files/ipc05_02_findParticipantAnalysis.png">
 * 
 * <br/>
 * <br/>
 * 
 * illustrating how the user interacts with the system in order
 * to send a message to an online participant
 * 
 * <img src="doc-files/ipc05_02_findParticipantAnalysis2.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * 
 * 
 * illustrating how the user interacts with the system in order
 * to access the history of messages
 * 
 * <img src="doc-files/ipc05_02_findParticipantAnalysis3.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * <h2>3. Design</h2>
 * 
 * sequence diagram for the "register participant" part of the use case requirements
 * 
 * 
 * 
 * 
 * 
 * <br/>
 * <br/>
 * <h2>4. Tests</h2>
 * 
 * Functional tests will be as follow:
 *  
 *  • ...
 *  • Make sure that only "online" participants receive the messages
 *  • Make sure that the status is one of two options: online / offline (can not be blank)
 *  • Make sure that all the required fields are filled by the user
 *  • Make sure that the photo is a valid image
 *  • ...
 * 
 * <br/>
 * <br/>
 *
 * @author Francisco Lopes
 */
class _Dummy_ {

}
