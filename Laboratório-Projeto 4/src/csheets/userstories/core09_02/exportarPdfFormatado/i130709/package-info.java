package csheets.userstories.core09_02.exportarPdfFormatado.i130709;

/**
 * 
 * Technical documentation regarding the user story Core09.2 - Exportação formatada para PDF
 * <br/>
 * <br/>
 * 
 * <h2>Requirement:</h2>
 * Export of previous contents should appear in the PDF file with a similar look to the screen. 
 * The appearance must include the formatting visible on the screen. It should also be possible
 * to configure whether there should be lines demarcating the cells, which line type and its 
 * color (not to be confused with the 'border' of the cell).
 * 
 * <br/>
 * <br/>
 *  
 * <h2>Analysis</h2>
 * It is needed to add on File's Menu the option to export to pdf, it can be the active
 * spreadsheet, selected cells or the entire workbook. However this time this action 
 * can be set on a table, like you see on cleansheets the cells contoured by lines. 
 * It is also asked that while doing the exportation, it has to shown up on .pdf
 * with all formatting, like (Font, Color, Size,...). To do all this it will be need
 * to use the suggested library iText.
 * 
 * <br/>
 * 
 * <h3>Analysis Diagram:</h3>
 * 
 * <img src="doc-files/core09_02_AnalysisDiagram.png"/>
 * 
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Design</h2>
 *
 * The formatted exportation to pdf is all based on getting the cell content
 * for role to export (workbook, spreadsheet, active cell), after we get 
 * all cell value, it is need to get all the formatation of the cells
 * like colors,alignments,fonts,... To do that it was needed to create a table
 * on the document and create a cell for each CleanSheets cell, that after create
 * those cells, is just about set all the information to the cell.
 * On this use case it was asked that the user could "configure" if he wanted
 * lines to demarcating the cells, however this "configuration" for me should not
 * happen, because if we need to export to pdf as we the cells on the cleansheets
 * we need a Table, and cells to that table, otherwise formatations can't be
 * exported to pdf, it is need to do things like, cell.setColor(), cell.setFont(),...
 * 
 * 
 * 
 * 
 * <br/>
 * 
 * <h3>Design Diagram:</h3>
 * 
 * <img src="doc-files/core09_02_DesignDiagram.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h3>Class Diagram:</h3>
 * 
 * <img src="doc-files/core09_02_ClassDiagram.png">
 * 
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h2>Tests</h2>
 * 
 * <h3>Menu Test:</h3>
 * Here is the menu where the user can choose what to export
 * and if he want table or not and what color will be added 
 * to table's border
 * (Note: this option (no table) is explained on design section)
 * 
 * <img src="doc-files/menuTest.jpg">
 * 
 * <h3>Formatted Exportation Test:</h3>
 * Exportation done, showed on picture all
 * possible fomatation on cleansheets:
 * 
 * -alignment; background ; foreground ; size ; font ; italic ; bold
 * 
 * <img src="doc-files/menuTestexportTest.jpg">
 * 
 * <h3>Table border Test:</h3>
 *  
 * After the user chooses the border color on menu, it is 
 * applied successfully
 * 
 * 
 * <img src="doc-files/borderTest.jpg">
 * 
 * <br/>
 * <br/>
 * 
 */

/*
 * @author 1130709
 */
class _Dummy_ {
}

