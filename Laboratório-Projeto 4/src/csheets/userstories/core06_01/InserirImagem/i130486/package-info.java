/**
 * Technical documentation regarding the user story core06_01: Insert Image on
 * Cells
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * An Extension in order to insert pictures on cells. The image inserted is
 * associated with the selected cell at the moment. There's also a sidebar
 * showing the cell's image.
 * <br/>
 * <br/>
 * <b>Use Case: Insert Picture</b> The user selects the cell he wants to insert
 * an image and chooses the insert picture option. The system displays a file
 * chooser window and waits for the user to choose the picture. The user selects
 * the picture. The system saves the picture on the cell and displays it on a
 * sidebar.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since pictures on cells will be supported in a new extension we need to study
 * how extensions are loaded by cleansheets and how they work.
 * <br/>
 *
 * <h3>Analysis Diagram</h3>
 *
 * <img src="doc-files/core06_01_Analysis.png">
 * <br/>
 *
 * <h2>3. Tests</h2>
 *
 * * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to add an attribute to cells to
 * be used to store an image. We need to be able to set and get its
 * content.<br/>
 * Following this approach we can start by coding a unit test that uses a
 * subclass of <code>CellExtension</code> with a new attribute for user comments
 * with the corresponding method accessors (set and get). A simple test can be
 * to set this attribute with an image and to verify if the get method returns
 * the same image.<br/>
 * As usual, in a test driven development approach tests normally fail in the
 * beginning. The idea is that the tests will pass in the end.<br>
 * <br/>
 * see: <code>csheets.ext.core.InsertImage</code><br/>
 *
 * <h2>4. Design</h2>
 * In order to make this user story we'll need to create an Extension's
 * subclass.<br/>
 *
 * <h3>Class Diagram</h3>
 *
 * Verificar o diagrama depois de fazer os metodos etc...
 * <img src="core06_01_ClassDiagram.png"/>
 *
 * <h3>Picture Extension</h3>
 * The following diagram shows how the PictureExtension is created.<br/><br/>
 * <img src="doc-files/core06_01_Design.png">
 * <br/>
 *
 * <h3>User Selects a Cell</h3>
 * The following diagram shows how the system interacts with the user when he
 * tries to insert an image in a selected cell. The system displays a sidebar
 * with the image.<br/>
 * <br/>
 * <img src="doc-files/core06_01_Design2.png">
 *
 * <h3>User Inserts a Picture in the Cell</h3>
 * The following diagram illustrates what happens when the user inserts a
 * picture on the selected cell.<br/>
 * <br/>
 * <img src="doc-files/core06_01_InsertDesign.png">
 *
 * <h2>5. Coding</h2>
 * <br/>
 *
 * @author 1130486 - Pedro Tavares
 */
package csheets.userstories.core06_01.InserirImagem.i130486;

class _Dummy_ {
}
