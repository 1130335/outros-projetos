package csheets.userstories.macros05_01.Wizard_Basico.i130709;

/**
 * Technical documentation regarding the user story macros05_01: Wizard basico inserir funcao.
 * <br/>
 * <br/>
 *
 * <h2>Requirement:</h2>
 * 
 * There must be a window that allows you to choose the function you want to perform.
 * The available functions must appear in a list. The construction of this list should
 * be made dynamically based on the properties file and self-description of the functions. 
 * When a function is selected in the list must be provided in an edit box, the syntax
 * to invoke this function. Just to appear in the edit box the name of the function
 * and the space for the parameter. For example, for the factorial function 
 * which has only one parameter should appear 'FACT = (Number)'. 
 * The window must also submit a 'help' that describes the function. 
 * When it confirms the text function, this is written in cleansheets the formula bar. 
 * The wizard option can be one of a menu extension option.
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Analysis:</h2>
 *
 * Analise the possibility of choosing a function from a list of function and get
 * another list with all the parameters of that function. Then you can set thoose
 * parameter's value(s) to get the result of the function execution on the active
 * cell.
 * Also you can get all the function's description by pressing the Button Help
 * on the Wizard Window.
 * 
 * <br/>
 * 
 * <h3>Analysis Diagram:</h3>
 * 
 * <img src="doc-files/macros05_01_SequenceDiagramAnalyis.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Design</h2>
 * 
 * <br/>
 * 
 * <h3>Design Diagram:</h3>
 * 
 * <img src="doc-files/macros05_01_SequenceDiagramDesign.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h3>Class Diagram</h3>
 * 
 * <img src="doc-files/macros05_01_ClassDiagram.png">
 * 
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Tests</h3>
 * 
 * Unit tests:
 * <img src="doc-files/macros05_01_Tests.png">
 * 
 * Beyond thoose tests, the other funcionalities can be "tested" by
 * runnning the apliccation. Like seeing if the comboxes are filled with all the functions
 * and the respectives parameters. After that you enter the parameters you want and
 * you see the result on the active cell.
 * 
 * <br/>
 * <br/>
 * 
 * 
 */

/*
 * @author 1130709
 */
class _Dummy_ {
}