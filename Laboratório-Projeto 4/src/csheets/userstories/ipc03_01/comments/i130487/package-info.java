/**
 *
 * <h2>1. Requirement</h2>
 * Be able to identify multiple instances of cleansheets . It should be possible to send a request for workbook to search for
 * another machine (search by name). Research should cover only the workbooks that are open in the other instance of
 * cleansheets . The origin should receive an answer if the file was found or not. If found should still receive a summary of
 * the contents of this workbook (name of the leaves and values ​​of the first cell with a value of each sheet ) .
 *
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * This use case will allow instances to comunicate. To achieve that goal, the program should allow the user to select an
 * user whose name is previously stored in the program using the UDP protocol. The user will select one and the user will
 * receive the resume of the content of the workbook(name of the sheets and values of the first cell that are not empty),
 * using the TCP protocol to establish the comunications.
 *
 * <br/>
 * <br/>
 *
 * <img src="doc-files/analise_Diagram_ipc03_01.png">
 *
 * <br/>
 * <br/>
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <h3>Send</h3>
 * <img src="doc-files/ipc03_01_design_send.png">
 *
 * <h3>Recieve</h3>
 *  * <img src="doc-files/ipc01_01_design_receive.png">
 *
 * <h3>Class Diagram</h3>
 *  * <img src="doc-files/ClassDiagram_ipc03.png">
 *
 *
 * <h3>Receive</h3>
 *
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 *
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.ipc03_01.comments.i130487;

class _Dummy_ {

}
