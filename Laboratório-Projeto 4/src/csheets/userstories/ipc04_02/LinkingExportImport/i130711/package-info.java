/**
 * Technical documentation regarding the user story ipc04_02 - Linking file to
 * import/export
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 *
 * <b>Use Case "linking file to import/export": </b>Add a window that allows the
 * user set a link to a file,and the information inside of him is loaded/
 * updated. Any change/update in file must reflect in the working
 * sheet.Similarly any change in currente working sheet must reflect in file.
 * <br/>
 *
 *
 * <h2>2. Analysis</h2>
 *
 * Any change to the file will be reflected on the selected sheet,this changes
 * are going to be detected using, a thread(with an infinite loop) responsible
 * to check if any changes occurred in file,when detect a change in sheet notify
 * de "Observers", thar are responsibabled to update the woorking sheet with
 * file data,the class where this thread is implemented is "called" the
 * Obserbable(extends Observable), another thread(with an infinite loop) is used
 * to update file with sheet changes, the class where this thread is implemented
 * is "called" the Obserbable(extends Observable),this thread when detect a
 * change in sheet notify de "Observers",classes that extends Observer. This
 * "observers" export data to the file selected in Linking file option by user,
 * when notified. The same file can be associated to diferent sheets,each sheet
 * is associated only to a file to import data and many files to export data.
 * When the user want to cancel/stop the option,Linking file to
 * import/export,the user select the option unlink in Export Linking File and
 * the same process in Import, with that the linkking export/import is finished
 * ,by stoping threads.
 *
 *
 * <b>Analysis of Sequence Diagram Import from file</b> Illustratin how the file
 * is linked to woorking sheet to import
 *
 * <br/>
 * <br/>
 * <img src="doc-files/ipc04_02_SequenceDiagramFileToWSheetAnalysis.png"/>
 *
 * <b>Analysis of Sequence Diagram Export to file</b> Illustratin how the file
 * is linked to woorking sheet to export
 *
 * <br/>
 * <br/>
 * <img src="doc-files/ipc04_02_SequenceDiagramWSheetToFileAnalysis.png"/>
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/ipc04_02_ClassDiagram.png"/>
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b> Sequence Diagram Woorking Export to file</b> Illustratin how the file is
 * linked to woorking sheet to export, and how export works
 *
 * <br/>
 * <br/>
 * <img src="doc-files/ipc04_02_SequenceDiagramWSheetToFile.png"/>
 *
 * <b>Sequence Diagram Import from file</b> Illustrating how the information of
 * file is loaded, and how import works
 *
 * <br/>
 * <br/>
 * <img src="doc-files/ipc04_02_SequenceDiagramFileToWSheet.png"/>
 *
 *
 *
 * <h2>5. Coding</h2>
 *
 *
 *
 *
 * <h2>6. Tests </h2>
 * <b>Functional Tests</b><br/>
 *
 * To test Import Linking File functionality,follow these steps: First of all a
 * file have to be choosed: 1 - Select the Extensions Menu<br/>
 * 2 - Select the Import options <br/>
 * 3 - Select Import Linking File <br/>
 * 4 - Select file and settings<br/>
 * 5 - Select import <br/>
 * <br/>
 * To check if the the file is correctly linked: 1 - Open the file linked to the
 * sheet <br/>
 * 2 - Make one or more changes<br/>
 * 3 - Save file <br/>
 * 4 - Changes must appear in the woorking sheet linked to the file<br/>
 * <br/>
 * To test if unlink funcionality is correct: 1 - Select the Extensions
 * Menu<br/>
 * 2 - Select the Import options <br/>
 * 3 - Select Import Linking File <br/>
 * 4 - Select Unlink <br/>
 * 5 - Open a file <br/>
 * 6 - Make one or more changes<br/>
 * 7 - Save file <br/>
 * 8 - Any changes in file shall not replects in woorking sheet linked to the
 * file<br/>
 *
 *
 * To test Export Linking File functionality,follow these steps: First of all a
 * file have to be choosed: 1 - Select the Extensions Menu<br/>
 * 2 - Select the Export options <br/>
 * 3 - Select Export Linking File <br/>
 * 4 - Select file and settings<br/>
 * 5 - Select export <br/>
 * <br/>
 * To check if the the file is correctly linked: 1 - Change cells value in
 * woorking sheet 2 - Open the file linked to the woorking sheet changed <br/>
 * 3 - Changes must appear in the file<br/>
 * <br/>
 * To test if unlink funcionality is correct: 1 - Select the Extensions
 * Menu<br/>
 * 2 - Select the Export options <br/>
 * 3 - Select Export Linking File <br/>
 * 4 - Select Unlink <br/>
 * 5 - Make one or more changes in woorking sheet<br/>
 * 6 - Open the file linked to the woorking sheet changed <br/>
 * 7 - Changes shall not appear in the file<br/>
 *
 * <br/>
 * <br/>
 */
package csheets.userstories.ipc04_02.LinkingExportImport.i130711;
/*
 * @author Sergio Leites 1130711
 */

class _Dummy_ {
}
