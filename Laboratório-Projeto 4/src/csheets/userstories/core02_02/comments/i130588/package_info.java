/**
 * Technical documentation regarding the user story core02_02: Tooltip and User
 * associated to the comments
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * If the mouse hovers over a cell and the cells has comments, then these
 * comments should show as a tooltip. It should be permited to associate the
 * identification of the user and several comments per cell.
 * <br/>
 * <br/>
 * <b>Use Case "Tooltip and User associated to the comments":</b> The user
 * hovers with the mouse over a cell. The system displays a tooltip showing the
 * comments and the user who created each comment.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since the tooltip is an extension, we need to study how extensions work. For
 * creating a tooltip we need to use the Tool Tip API in the JComponent class.
 * The extensions must be subclasses of the Extension abstract class and need to
 * be registered in special files. The Extension class has a method called
 * getUIExtension that should be implemented and return an instance of a class
 * that is a subclass of UIExtension. There are no users associated to the
 * comments in a commentable cell for now, but if there'll ever have, it can be
 * easily resolved since this use case was designed thinking of possible
 * extensions and variations.
 *
 * <h2>3. Tests</h2>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to get a comment from a
 * commentable cell.<br/>
 * Following this approach we can start by coding a unit test that uses a
 * subclass of <code>CellExtension</code> with a new attribute for user's
 * comment with the corresponding method accessors (set and get). A simple test
 * can be to set this attribute with a simple string and to verify if the get
 * method returns the same string. The setter and the getter are tested
 * simultaneously.
 * <br/>
 *
 * <h2>4. Design</h2>
 * To realize this user story we will need to create a class with the
 * responsibility to manage the tooltip that extends Extension. We will also
 * need to use the Tool Tip API in the JComponent class to create the tool tip.
 *
 *
 *
 * <h2>5. Coding</h2>
 *
 *
 * <h2>6. Final Remarks</h2>
 * The main problem of the making of this use case was that a cell could not be
 * converted to a JComponent, only the whole spradsheet could be used as a
 * JComponent. That implies that when the mouse was over the spreadsheet, there
 * was always a tooltip with the comment of the cell that was selected when
 * requesting fot the tooltip extension to be enabled.
 * <br/>
 * <br/>
 *
 * @author Daniela Maia
 */
package csheets.userstories.core02_02.comments.i130588;

/**
 *
 * @author Daniela Maia
 */
public class package_info {

}
