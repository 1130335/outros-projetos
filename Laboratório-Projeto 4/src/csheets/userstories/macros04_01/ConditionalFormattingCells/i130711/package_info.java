package csheets.userstories.macros04_01.ConditionalFormattingCells.i130711;

/**
 * Technical documentation regarding the user story macros04_01 - Cell
 * Conditional Formatting
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 *
 * <b>Use Case "Cell Conditional Formatting": </b>Add a window that allows the
 * user enter a formula.As result, the cell selected is painted with green if
 * avaliation result is true or red if avaliation result is false. Also add a
 * sidebar window "Conditional Formatting Single" that presents the changed
 * cell.
 * <br/>
 *
 *
 * <h2>2. Analysis</h2>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how cell is changed
 * based in formula avaliation
 *
 * <br/>
 * <br/>
 * <img src="doc-files/macros04_01_Analysis.png"/>
 *
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how cells changed will
 * be showed up in a sideBar
 *
 *
 * <img src="doc-files/macros04_01_SideBarAnalysis.png"/>
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/macros04_01_ClassDiagram.png"/>
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how cell is changed based in formula
 * avaliation
 *
 * <img src="doc-files/macros04_01_Design.png"/>
 *
 *
 *
 * <b>Sequence Diagram</b> illustrating how cells changed will be showed up in a
 * sideBar
 *
 * <img src="doc-files/macros04_01_SideBar.png"/>
 *
 * <h2>5. Coding</h2>
 *
 *
 *
 *
 * <h2>6. Tests </h2>
 *
 * This use case is mostly based on showing the user the cell changed, the test
 * consists in making sure that the cell is formatted based in formula
 * avaliation,this formatation diferes if avaliation results is true or
 * false.Otherwise unnit test were made for some methods, to test they qualitty
 * and functionality
 *
 *
 * Also, the cell changed must appears in sidebar "Condiitional Formatting"
 *
 * <br/>
 * <br/>
 *
 * @author Sergio Leites 1130711
 */
class _Dummy_ {
}
