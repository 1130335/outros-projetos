
package csheets.userstories.ipc06_02.network_explorer.i130298;

/**
 * Technical documentation regarding the user story: Network explorer
 * <br/>
 * <br/>
 * 
 * <b>Requirement</b><br/>
 * Must exist a sidebar window with a tree that represents multi intances of cleansheets on network.
 * To each intance must apear a descrition of their resources(open workbooks and their leaves, open extensions).
 * This tree must be updated in real time, when something is changed it must be viewed in explorer.
 * <br/>
 * <br/>
 *  
 * <b>Analysis</b><br/>
 * My uc needs to find cleansheets instances and some information about them like active spreadsheets and active extensions. 
 * For that i need a method that finds all the instances, that method is a thread and it will be running at the same time with the aplication.
 * Every times that he finds new instances, the sidebar is refreshed with new information.
 * <br/>
 * 
 * 
 * <b>Design</b><br/>
 *
 * <br/>
 * <b>Sidebar and Extensions</b><br/>
 * <img src="doc-files/networkexplorer_design1.png"/>
 * <br/>
 * 
 * <b>Cleansheets instances and their information</b><br/>
 * <img src="doc-files/networkexplorer_design2.png2/>
 * <b>S130c: Coding</b><br/>
 * 
 * <br/>
 * <br/>
 * 
 * <b>S130u: Unit Tests</b><br/>
 * <br/>
 * 
 * see: <code></code><br/>
 * <br/>
 * 
 * <b>S130f: Functional Tests</b><br/>
 * <br/>
 * <br/>
 * <br/>
 * 
 */
public class package_info {
    
}
