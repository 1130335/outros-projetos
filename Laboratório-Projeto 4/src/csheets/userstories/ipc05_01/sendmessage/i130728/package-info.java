package csheets.userstories.ipc05_01.sendmessage.i130728;

/**
 * Technical documentation regarding the user story ipc01_01: initiate sharing.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Make an extension that allows you to send text messages to another
 * cleansheets instance (for which we know the address) and this message appears
 * in a "popup" window in another instance. It should also exist a sidebar in
 * which appear incoming messages. From this window sidebar it should be
 * possible to reply to a message that we have received. The messages from that
 * window must be grouped by the "origin" of the message.
 * <br/>
 * <br/>
 * <h2>2. Analysis</h2>
 * This use case allows the communication between different instances. The user
 * has a option in the application menu to start communication between
 * instances. The selected instance will receive the sent message in a "popup"
 * window and a sidebar, with the received messages, grouped by the "origin" of
 * the message, will be created. The instance that received the message will be
 * able to communicate through the sidebar. UDP protocol will be used to check
 * if the desired instance (which IP is known) is connectable, if so TCP
 * protocol will be used to communicate.
 * <br/>
 * <br/>
 * <img src="doc-files/analise_Diagram_IPC05_01.png"/>
 * <br/>
 * <h2>3. Design</h2>
 * Make an extension that allows you to send text messages to another
 * cleansheets instance (for which we know the address) and this message appears
 * in a "popup" window in another instance. It should also exist a sidebar in
 * which appear incoming messages. From this window sidebar it should be
 * possible to reply to a message that we have received. The messages from that
 * window must be grouped by the "origin" of the message.
 * <br/>
 * <br/>
 * <b>Create Side Bar Diagram</b>
 * <br/>
 * <br/>
 * <img src="doc-files/ipc05_01_CreateSideBarDiagram.png"/>
 * <br/>
 * <br/>
 * <b>Send Diagram</b>
 * <br/>
 * <br/>
 * <img src="doc-files/ipc05_01_DesignDiagramSend.png"/>
 * <br/>
 * <br/>
 * <b>Receive Diagram</b>
 * <br/>
 * <br/>
 * <img src="doc-files/ipc05_01_DesignDiagramReceive.png"/>
 * <br/>
 * <br/>
 * <b>Class Diagram</b>
 * <br/>
 * <br/>
 * <img src="doc-files/ipc05_01_ClassDiagram.png"/>
 * <br/>
 * <br/>
 * <h2>4. Tests</h2>
 * I couldn't find a way to make unitary or funcional tests so I decided to make
 * the analysis, the design and the implementation first and then if I'm able to
 * finish it I will make the unitary and functional tests. For the moment I made
 * the analysis and the design but I'm still working on the implementation so
 * there aren't any tests to show.
 * <br/>
 * <br/>
 *
 * @author João Cabral
 */
class _Dummy_ {

}
