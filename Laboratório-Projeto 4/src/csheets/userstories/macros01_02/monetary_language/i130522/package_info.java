package csheets.userstories.macros01_02.monetary_language.i130522;

/**
 * Technical documentation regarding the user story core01_01 - Activate and
 * Deactivate Extensions
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 * This user story add a new language. The character that begins formula should
 * be the '#' and the block must be delimited by braces - "{" and "}".The
 * formula should only accept the addition, subtraction, multiplication and
 * division. The operands are monetary values ​​on which indicated the currency
 * (eg 10.21€, 1.32£ or 0.20$ ).
 * <br/>
 * <h4>Block Instruction Example:</h4>
 * The formula: <code>#euro{10.32$ + 12.89£ }</code> or
 * <code>#dollar {10.32$  + 12.89£ }</code> or <code> #pound{ 10.32$ + 12.89£ }
 * </code>
 * <br/>
 * This new language should be possible to use the cells to assign values (just
 * us to begin the formula with the character '#' instead of '=' character).
 * There should also be a setting for exchange rates to use. The implementation
 * should avoid the use of numbers in floating point representation (eg: float,
 * double) in order to avoid precision problems.
 *
 *
 * <br/>
 *
 *
 * <h2>2. Analysis</h2>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how actor will interact
 * with the new language
 *
 * <img src="doc-files/macros01_02_analysis_ssd.png">
 *
 * <br/>
 * <br/>
 *
 *
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how the system will
 * respond to this use case:
 *
 * <img src="doc-files/macros01_02_analysis_ssd.png">
 *
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case:
 * <br/>
 *
 * <img src="doc-files/macro01_02_class_diagram.png">
 *
 * <h2>4. Design</h2>
 *
 *
 *
 * <b>Sequence Diagrams</b>
 *
 * Sequence diagram of the use case:
 *
 * <br/>
 * <img src=doc-files/macros01_02_design_sd.png">
 *
 *
 * <br/>
 * Sequence diagram of the window to change the rates:
 * <br/>
 *
 * <img src="doc-files/macros01_02_design_changerate.png">
 * <h2>5. Coding</h2>
 *
 * The solution to avoid numbers in floating point representation was to use
 * BigDecimal representation.
 *
 *
 * <h2>6. Tests
 * </h2>
 *
 * Since this use case is mostly based on allowing the user to make monetary
 * operations, we need to make sure all operations results are correct and the
 * taxes are correctly applied.
 *
 * Also, we need to verify that the user can change the taxes to be applied
 * anytime.
 *
 * To test the grammar, it was possible to use ANTLRworks library to validate
 * the new expression.
 * <br/>
 * <br/>
 *
 * @author Ricardo Leite 1130522
 */
/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
