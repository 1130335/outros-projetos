/**
 * Technical documentation regarding the user story Ipc08.02:Automatic Download
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * The user has the chance to download a file that is in a share folder
 * available to evryone connected. If he chooses a certain file and decides to
 * download it a progress bar pops up showing the download progress. The user
 * also has the chance to update a file in case he has already downloaded it.
 * There is also the option to create a new version of the file or to overwrite
 * it (when updating).
 * <br/>
 *
 * <b>Use Case: Automatic download</b> The user runs the Use case Ipc08.01 and
 * creates a shared folder. When acessing the shared folder content he first
 * needs to choose the inuput folder (folder that will store the downloaded
 * files) and then select one file. The system asks if he wants to download or
 * update the file. The user makes his choice. The system downloads the file or
 * if the user selected "update" the system asks if he wants to overwrite the
 * file or if he wants to create a new one. <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * In order to understand completly this use case you need to see Ipc08.01 fisrt
 * since its the first part of this funcionality.
 *
 * <h3>Analysis Diagram</h3>
 *
 * Create Share -> see :
 * <img src="doc-files/ipc08_01_SequenceDiagramCreateShareAnalyis.png"/>
 *
 * Access Share see:
 * <img src="doc-files/ipc08_01_SequenceDiagramAccessShareAnalyis.png"/>
 *
 * <h2>3. Tests</h2>
 *
 * * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to select one file from the
 * shared folder and download it or update it. <br/>
 * For that we can check if the funcionality is working if we can successfuly
 * download a certain file to our own computer. The test I have made is simply
 * to try and download one existent file and verify if it was successful or not.
 *
 * * see: <code>csheets.ext.core.sharingFiles</code><br/>
 * <br/>
 *
 * <h2>4. Design</h2>
 *
 * * <h3>Download File</h3>
 * The following diagram shows how the user downloads the file.<br/>
 * <img src="SequenceDiagramAnalysisDownload"/>
 * <br/>
 * <h3>Update File</h3>
 * The following diagram shows how the system works to update the file.<br/>
 * <img src="SequenceDiagramAnalysisUpdate"/>
 * <br/>
 *
 * <h3>Class Diagram</h3>
 *
 * <img src="doc-files/ipc08_02_ClassDiagram.png"/>
 *
 *
 * <h2>5. Comments </h2>
 * This use case was hard to implement since the UC - Ipc08.01 wasn't completly
 * done. One funcionality that is really important is the fact that the system
 * has to be capable of updating the content of the shared folder. That wasn't
 * implemented so it brought some problems when updating the files. I still
 * implemented the Watcher (watcher of the shared folder) but I couldn't find a
 * way to update the server's array of files since the codification of the last
 * issue wasn't optimized for that.
 *
 * Also the fact that it is impossible to access the shared folder from another
 * computer makes this UC alot more complicated. I had to change the user
 * interface of Ipc08.01 since I had to select one file from the given. For that
 * I simply added a JComboBox with all the info I needed and the right methods
 * to run the Ipc08.02 funcionality.
 *
 * Its possible to Download a file to a desired folder and to update the same
 * file. The only thing that is not possible is to overwrite the file.
 *
 *
 * <br/>
 *
 * @author 1130486 - Pedro Tavares
 */
package csheets.userstories.ipc08_02.AutomaticDownload.i130486;

class _Dummy_ {
}
