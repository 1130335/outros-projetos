/**
 * Technical documentation regarding the user story core10_1: Graphics Wizard.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * There should be an option to launch a "wizard" to generate a graphic. This
 * wizard should have two steps. In the first step, the user must introduce the
 * data of the graphic that at this moment are it's name and the "range" of
 * cells that contain the data for the graphic. In the second step, the window
 * of visualization of the graphic, which in this moment it's not necessary to
 * do any visualization, as it should appear a blank space. The user can go back
 * to change the data ou to confirm the creation of the graphic. When the
 * graphic is created, this is associated to the cell in the upper left corner
 * of the "range". In this cell should appear a tiny icon indication the it has
 * an associated graphic.
 * <br/>
 * <br/>
 * <b>Use Case "Core 10.1 - Graphics Wizard":</b> The user starts the wizard and
 * a window appears for the user to write the name of the graphic and to
 * indicate the range of cells to be used on the graphic. Then a window appears
 * for the visualization, which should be blank in the use case. The user should
 * have the possibility to go back and edit the data of the graphic or confirm
 * it. After being the graphic being created, the user should be able to see an
 * icon in the upper left corner of the selected range of cells indicating that
 * this is a graphic associated to it.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * This use case is based in the creation of windows, though there is an access
 * to the content of the cells selected, what should be done manually by
 * entering the address of the upper left cell and the bottom right cell of the
 * range and the created graphic should be associated to the upper left cell of
 * the range, which is indicated to the user by a tiny icon. For this purpose,
 * there will be a need to create a class called Graphic which will contain its
 * name and range of cells, and also the icon to be associated to the cell so
 * every cell which has graphic associated will have the same image. There will
 * be created a controller to handle the interaction between the domain classes
 * and the user interface. Using the class SpreadsheetImpl, it's possible to get
 * the range of cells as if they were selected, but using the data given by the
 * user.
 * <br/>
 * <br/>
 * <b>Update 1:</b> It wasn't possible to implement the little icon to indicate
 * the existence of a graphic in the upper left cell of the range.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * This following diagram will represent the analysis for the creation of a
 * graphic with the wizard and its association with the Cell.
 * <br/>
 * <br/>
 * <img src = "doc-files/analysis_diagram.png"/>
 * <br/>
 * <br/>
 * From the previous diagram, we conclude that the class Cell will suffer some
 * changes in order to be possible to associate a graphic to the upper left cell
 * of the desired range.
 *
 * <h2>3. Tests</h2>
 * The unit tests developed for this use case were related to the new class,
 * especially created for this use case.  <br/>
 * see:<br/>
 * <code>csheets.core.GraphicTest</code><br/>
 *
 * <h2>4. Design</h2>
 * The following sequence diagram represents how the code is strutured fo this
 * use case.<br/>
 * <img src = "doc-files/DS_GraphicWizard.png"/>
 * <h2>5. Coding</h2>
 * see the package:<br/>
 * <code>csheets.core.graphic</code>
 * <br/>
 * <code>csheets.ext.wizard.ui</code>
 * <br/>
 * <br/>
 *
 * @author alexandrebraganca
 */
package csheets.userstories.core10_1.graphicswizard.i130335;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
