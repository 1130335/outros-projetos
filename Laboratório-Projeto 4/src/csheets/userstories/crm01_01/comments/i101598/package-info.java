/**
 * Technical documentation regarding the user story crm01_01: edit contact
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Allows the user to create, edit and remove Contacts. Also allows the user to
 * create events associated to a certain Contact. Events are simply a
 * "timestamp" and a descrition.
 *
 * <br/>
 * <br/>
 * <b>Use Case "Edit Contact":</b>The user selects the ContactsMenu in the
 * Extension/Contatcs Menu. The system displays the options available in a new
 * window and then the user selects the option he wants: Create, Edit or Remove.
 * For these three options the system will display a different menu and ask the
 * user to fill data/edit data or simply select the contact and remove it. After
 * inserting a contact the user can create an event associated to a certain
 * contact. Events will be saved in the DataBase.
 *
 *
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * My analysis divides this user case in four parts: create contact, edit
 * contact, remove contact and create event. In general, my user case allows the
 * user to create, edit or remove a contact. If he wants to create a contact he
 * needs to introduce first name, last name and path to an image. To edit a
 * contact he needs to choose a contact from the list and edit the fields, to
 * remove the user only needs to select the contact from the list and click
 * "remove".
 * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>
 *
 * <h2>User selects create contact</h2>
 * The follow sequence diagram is about create a contact, the use starts the
 * aplication, choose extensions, create contact and fills the parameteres.
 * <img src="doc_files/CreateContactSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects edit contact</h2>
 * The second sequence diagram is about edit a contact, as i said, the user
 * choose a conctact and he can edit firs and last name and his image.
 * <img src="doc-files/EditContactsSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects remove contact</h2>
 * The follow sequence diagram about remove, the user choose a contact from the
 * list and remove it.
 * <img src="doc-files/RemoveContactsSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects create event The user can create an event introducing data
 * and description, the event is saved in EventRepository.
 * <img src="CreateEventSequenceDiagram.png">
 *
 * <h3>Analysis of CRM Technical Problem</h3>
 * The only real problem i had was to create a new JPARepository from scratch. I
 * used the example we had in EAPLI MyMoney project.
 *
 * <h2>3. Tests</h2>
 *
 * the following tests were made: - GetContactFirtName - GetContactLastName -
 * GetImage (not working) - SetContactFirtName - SetContactLastName -
 * TestAddContact (not fully working) - TestEditContact (not fully working) -
 * TestRemoveContact - TestAllContacts
 *
 * <h2>4. Coding</h2>
 * see:<br/>
 * <a href="../../../../csheets/ext/crm/contacts/package-summary.html">csheets.ext.contacts</a><br/>
 * <a href="../../../../csheets/ext/crm/contacts/ui/package-summary.html">csheets.ext.crm.contacts.ui</a><br/>
 * <a href="../../../../csheets/ext/crm/event/package-summary.html">csheets.ext.crm.event</a><br/>
 * <a href="../../../../csheets/ext/crm/event/ui/package-summary.html">csheets.ext.crm.event.ui</a><br/>
 * <br/>
 * <h2>6. Final Remarks</h2>
 * This Use Case does not have any extra's implemented.
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.crm01_01.comments.i101598;

/**
 *
 * @author André Garrido
 */
class _Dummy_ {
}
