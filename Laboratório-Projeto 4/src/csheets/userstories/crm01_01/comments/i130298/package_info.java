/**
 * Technical documentation regarding the user story crm01_01: edit contact
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Allows to establish a connection to another instanceof the cleansheets and
 * send a range of cells to another instance. The received content must be
 * showed in the same location. To make it happen , every instance of the
 * cleansheets must have a port for the connections. Should be able to find
 * another instances of the cleansheets in the same local network. The instances
 * <br/>
 * <br/>
 * <b>Use Case "Start Sharing":</b>The user selects the cell/cell group he wants
 * to share. The user starts the share. The system displays the other instances
 * of cleansheet available in a new window. The user select the cleansheet he
 * wants to share to. The system make the connection and send the selected cells
 * to selected cleansheet. The system send a sucess message to user.<br/>
 * <br/>
 * 
 * <h2>2. Analysis</h2>
 * My analysis divides this user case in four parts: create contact, edit contact, remove contact and create event.
 * In general, my user case allows the user to create, edite or remove a contact. If we wants to create a contact he needs to introduce first name, last name and image. To edit a contact he need to choose a contact from the list and edit the parameteres, remove is almost the same.
 * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>

 * <h2>User selects create contact</h2>
 * The follow sequence diagram is about create a contact, the use starts the aplication, choose extensions, create contact and fills the parameteres.
 * <img src="doc_files/NewContactSequenceDiagram.png"> 
 * <br/>
 * 
 * <h2>User selects edit contact</h2>
 * The second sequence diagram is about edit a contact, as i said, the user choose a conctac and he can edit firs and last name and his image.
 * <img src="doc_files/EditContactsSequenceDiagram.png">
 * <br/>
 * 
 * <h2>User selects remove contact</h2>
 * The follow sequence diagram about remove, the user choose a contact from the list and remove it.
 * <img src="doc_files/RemoveContactsSequenceDiagram.png">
 * <br/>
 * 
 * <h2>User selects create event
 * The user can create an event introducing data and description, the event is saved in EventRepository.
 * <img src="doc_files/CreateEventSequenceDiagram.png">
 * 
 * <h3>Analysis of Core Technical Problem</h3>
 * The follow diagram is about model diagram, we only have two class, Events and Contact and the relation is many to many.
 * <img src="doc-files/comments_extension_uc_realization1.png"> 
 * <br/>
 * <h2>3. Tests</h2>
 *  Basically, from requirements and also analysis, we see that the core functionality of this use case is to be able to add an attribute to cells to be used to store a comment/text. We need to be able to set and get its value.<br/>
 * Following this approach we can start by coding a unit test that uses a subclass of <code>CellExtension</code> with a new attribute for user comments with the corresponding method accessors (set and get). A simple test can be to set this attribute with a simple string and to verify if the get method returns the same string.<br/>
 * As usual, in a test driven development approach tests normally fail in the beginning. The idea is that the tests will pass in the end.<br> 
 * <br/>
 * see: <code>csheets.ext.contacts.contactsTest</code><br/>
 *<h2>4. Coding</h2>
 *
 * <h2>6. Final Remarks</h2>
 * We dont have any consideration or extra to say.
 * <br/>
 * <br/>
 * 
 */
package csheets.userstories.crm01_01.comments.i130298;

/**
 *
 * @author Paulo silva
 */
class _Dummy_{
    
}