/**
 * Technical documentation regarding the user story ipc02:02 - Advanced
 * woorkbook search
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 * IPC02.2) Advanced search of workbooks. A sidebar window that contains the
 * result of the search must have a area that allows a preview with some content
 * of the spreadsheet without it being open. The content to preview must have
 * the number of spreedsheets and for each spreadsheet the content of the first
 * cells with value.
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * <br/>
 * <h3>Uc realization sequence diagram</h3>
 * <br/>
 * The following diagram suggests how the uc is expected to go along. It
 * represents a general sequence of how the interaction between the user and the
 * system.
 * <br/>
 * <img src=doc-files/ipc02_02_uc_realization.png/>
 * <br/>
 * <h2>3. Design</h2>
 * <br/>
 * <h3>Start the extension and sidebar</h3>
 * <img src = "doc-files/ipc02_02_design01.png"/>
 * <br/>
 * <h3>Thread that search files</h3>
 * <img src = "doc-files/ipc02_02_design2.png"/>
 * <br/>
 * <br/>
 * <h3>Preview a workbook</h3>
 * This diagram represents in what this use case consists. It makes possible for
 * the user to see a preview of the content of every spreadsheet of a workbook.
 * The user is presented with a list of .csl files that each one represents a
 * woorkbook. After one is select and the button "preview is pressed, a chain of
 * events makes possible for the user to see the preview. First we get the item
 * selected with one click on the workbook and that click converts the .csl file
 * into a manageable file (workbook). The secret for the success of this uc is
 * the method getFirstCellsWithContent() that returns for each spreadsheet of a
 * workbook a 2x2 matrix of the first cells with content. For last a preview is
 * presented in a JTextArea of the content of the cells. A brief explanation of
 * the method getFirstCellsWithContent() is presented below.
 *
 * {@code
 * 	@Override
 * public Cell[][] getFirstCellsWithContent() {
 * Cell[][] range = new Cell[2][2];
 * int row;
 * int collumn;
 * for (Map.Entry<Address, Cell> entrySet : cells.entrySet()) {
 * Address key = entrySet.getKey();
 * Cell value = entrySet.getValue();
 *
 * if (!(value.getContent().equals(""))) {
 * row = key.getRow();
 * collumn = key.getColumn();
 * range[0][0] = value;
 * range[0][1] = this.getCell(row, collumn + 1);
 * range[1][0] = this.getCell(row + 1, collumn);
 * range[1][1] = this.getCell(row + 1, collumn + 1);
 *
 * return range;
 * }
 * }
 *
 * return range;
 * }
 * }
 *
 * <img src = "doc-files/ipc02_02_design4.png"/>
 *
 * <br/>
 *
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 */
package csheets.userstories.ipc02_02.comments.i130404;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
