package csheets.userstories.ipc07_01.i130637.i120674;

/**
 * <h2>Requirement:</h2>
 * 
 * Create an extension that allows two instances of the cleansheets to establish a connection to play games.
 * It should apper 2 options to play: Battleship (http://en.wikipedia.org/wiki/Battleship_(game)) and a game
 * of Draughts/Checkers (http://en.wikipedia.org/wiki/Draughts) . They have to agree which game to play.
 * Each user should choose a name and an image that should be transmited to the other instance.
 * The user can play multiple games at once. There should be a sidebar that lists the active games.
 * It should be possible to quit games.
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Analysis:</h2>
 *
 * The user will need to select which game he wants to play between the two available and he will wait until another
 * user chooses the same game, only then the system creates a connection between the two users using Java Sockets.
 * 
 * <br/>
 * 
 * <h3>Analysis Diagram:</h3>
 * 
 * <img src="doc-files/ipc07_01_AnalysisDiagram.png">
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Design</h2>
 * 
 * For this use case I thought of the common options of Creating a GameRoom where
 * you select a game and wait for opponents to appear and Searching for Players
 * where you search for a GameRoom and join, then both players play.
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Create and join Game</h3>
 * 
 * <img src="doc-files/ipc07_01_DesignDiagram.png">
 * 
 * <br/>
 * <br/>
 * 
 * 
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Class Diagram</h3>
 * 
 * <img src="doc-files/ipc07_01_ClassDiagram.png">
 * 
 * 
 * <br/>
 * <br/>
 * 
 * 
 */
public class package_info {
    
}
