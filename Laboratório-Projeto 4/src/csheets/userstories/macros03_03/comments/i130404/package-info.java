/**
 * Technical documentation regarding the user story macros03_3: Array Formula's
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Make possible to write array formula's. An array formula is a formula which
 * result is an array and because of that it must be atribuied to a range of
 * cells. The formula must be writen in the uper left corner . The result of the
 * formula apply to a range of cells iniciated in that uper left corner. The
 * array formula's have a similar sintaxe to a commun formula but they are
 * surrounded by braces. The initial character is '{'. It only makes sense to
 * use operators or functions which results are arrays. Example:
 * "{=MINVERSE(B6:E9)}".
 * <br/>
 * <br/>
 * <b>Use Case "Array Formula":</b>The user fills two range of cells. Then he
 * selects the beginning cells that will store the result of the operation. The
 * formula must start with'{' in order to the formula compiler understands that
 * the result is a matrix or an array and not a simple number. From that point
 * the formula is writen by the user like a normal formula. Its only supposed to
 * let the result appear if the result is a matrix of cells otherwise an error
 * message will appear. The result will appear in the initial selected cell
 * filling in a right buttomn diagonal. <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since array formula's starts differentelly than the others formulas we need
 * to understand who will be responsible to interpret the beggining character as
 * '{'. After that point we need to understant which method will be responsible
 * to make the calculations regarding the formula selected, and which class is
 * going to present it to the user by inserting the values in the spreadsheet.
 * What the user is going to see is a matrix starting in the first cell selected
 * and filled in a lower right diagonal.
 * .<br/>
 * The use case realization can be seen here:
 * <img src="doc-files/macros03_3_ucrealization.png">
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="doc-files/macros03_3_sequence_diagram.png">
 * <br/>
 * <br/>
 * From the previous diagram we see that this use case requires special atention
 * and that it is very complex. The method "writeMatrixFromCell()" is a new
 * method to be created that will allow us to convert the formula given by the
 * user into a matrix of cells. For that purpose i'm counting on having access
 * to the ative cell and start filling the matrix from that point like the
 * problem proposes. To know where the cell starts we nedd to get the address
 * with the methods getCollumn() and getRow(). From that point with a loop i'm
 * expecting to be able to write all the matrix.
 *
 * <h2>4. Design</h2>
 * To realize this user story we won't need to create any new classes. We will
 * only need to readjust the way that some methods work or implement new
 * funcionalities to the method. After starting the implementation we faced with
 * a problem that was that the first cell of the result matriz wasn't storing
 * the value , only the content. In order to solve this proble we needed to
 * create a verification that will allow to first store the content of all the
 * cells except the first one which will only be stored in the end. In order to
 * make it happen it was created a new atribute in Formula called is Array and
 * the respective get and set methods which will allow us to control if a
 * determined formula is or not a formula array expression. As a result a new
 * and more complete diagram was created as well as a class diagram representing
 * the classes envolved in this user story.
 *
 * Design Sequence Diagram
 * <br/>
 * <img src="doc-files/macros03_3_sequence_diagram_design.png">
 * <br/>
 *
 * Design class diagram
 * <br/>
 * <img src="doc-files/macros03_3_classes_diagram.png">
 * <br/>
 *
 *
 * <h2>5. Coding</h2>
 * Here is the method responsible to create a matriz from the given formula.
 * <pre>
 * {@code
 * 	@Override
 * public void writeMatrixFromCell(Cell cell, String content) {
 *
 * String expression = content;
 * expression = expression.substring(0, expression.length() - 1);
 * expression = expression.substring(1);
 * System.out.println(expression);
 * String[] values = expression.split(Pattern.quote(";"));
 * cell.clear();
 * int collumn = cell.getAddress().getColumn();
 * int row = cell.getAddress().getRow();
 * int collumnInitialValue = collumn;
 * int endRow = values.length / 2;
 *
 * int count = 0;
 *
 *	for (int i = 0; i < values.length; i++) {
 *
 *		try {
 *			cell.getSpreadsheet().getCell(collumn, row).
 *			setContent(values[i]);
 *		} catch (FormulaCompilationException ex) {
 *		Logger.getLogger(SpreadsheetImpl.class.getName()).
 *			log(Level.SEVERE, null, ex);
 *		}
 *		collumn++;
 *		count++;
 *		if (count == endRow) {
 *			count = 0;
 *		row++;
 *		collumn = collumnInitialValue;
 *		}
 *	}
 * }
 * </pre>
 *
 *
 * <h2>6. Final Remarks</h2>
 *
 * It was created two test classes , one for FormulaTest and another for
 * SpreasheetImplTest. In each one i tested the methods created. The Formula
 * test is at 100% and Spreadsheet is at 0%.
 *
 * <br/>
 * <br/>
 *
 * @author ivo teixeira
 */
package csheets.userstories.macros03_03.comments.i130404;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author ivo teixeira
 */
class _Dummy_ {
}
