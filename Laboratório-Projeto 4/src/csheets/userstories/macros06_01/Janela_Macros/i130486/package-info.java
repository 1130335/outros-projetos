/**
 * Technical documentation regarding the user story macros06.1: Macro's Window
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Open a window to write and execute a macro. The user can write a sequence of
 * macros in order to execute something but the output is the last macro the
 * user wrote and it will show up in the same window. Now its possible to write
 * comments only if the line starts with a ";".
 *
 * <br/>
 * <br/>
 * <b>Use Case "Macro's Window":</b> The user presses a button and the system
 * shows a window. The user can write macros in the window's text area and press
 * a button to execute them. The system shows the result of the last macro
 * written.<br/>
 * <br/>
 *
 * <h2>2. Analysis </h2>
 * <br/>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how the user interacts
 * with the system in order to show the macro's output.
 * <br/>
 * <br/>
 * <img src="doc-files/macros06_01_SequenceDiagram.png">
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/macros06_01_ClassDiagram.png">
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how the user interacts with the system
 * in order to show the macro's output.
 *
 * <h2>5. Coding</h2>
 *
 *
 * <h2>6. Tests </h2>
 * Since this use case is mostly based in showing the correct output of the
 * macros or a system error message, the tests to be done consists in writting
 * wrong grammar to see if the system detects the unsuitable macros. *
 *
 * @author i130486
 */
package csheets.userstories.macros06_01.Janela_Macros.i130486;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
