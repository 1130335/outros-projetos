/**
 * Technical documentation regarding the user story crm02_02: Import/Export
 * Contacts and/or Address.
 * <br/>
 * <br/>
 *
 *
 * <h1>Requirement<h1/>><br/>
 * Allows the user to import and export the Contact and/or Contact's Address to
 * <br/>
 * <br/>
 *
 * <h1> Analysis: Use Case 'Import/Export Contacts and/or Address'<h1/><br/>
 * <br/>
 * To implement this UseCase i've created a new package
 * "Csheets.ext.crm.ImportExport" which contains two Interfaces (Import and
 * Export). There are two classes that implements the interfaces above,
 * Import_Address which imports contacts and addresses from Spreadsheet and
 * Export_Address wich Exports Contacts and Addresses to Spreadsheet. There were
 * some changes to AddressControler, added methods to import and export. This
 * allows to implement a Strattegy pattern and make it easy to implement further
 * use cases that involves import/export.<br/>
 *
 * I decided to implement these options in the AddressManager MainMenu in order
 * to make it easier for the user.
 *
 * <br/>
 * The restrictions used in the creation of the Address will be used in this
 * UseCase too, for example:
 * <br/>
 * - If any field from secondary address is empty (when the user wants to import
 * from Spreadsheet) the system will assume only the main address.<br/>
 * -To export a Contact or and Address the user needs to insert a contact and
 * address before he can do it
 * <br/>
 *
 *
 *
 *
 * <br/>
 * <br/>
 *
 * <h1>Design</h1><br/>
 *
 * The user selects the AddressMenu in the Extension/Address Menu. The system
 * displays the options available in a new window and then the user selects the
 * option he wants: Import or Export.
 * <br/>
 * <br/>
 * <h2>User selects Import</h2>
 * The follow sequence diagram is about import a Contact's address, the user
 * starts the aplication choose extensions/Address/AddressMainMenu/Import, the
 * system will ask the user for the row that as the contact's information. After
 * that, the contact's information is imported.
 * <img src="doc-files/ImportAddressSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects Export</h2>
 * The second sequence diagram is about export a Contact's address. The system
 * will display a window for the user to select the contact to export and which
 * fields he wants to export. the system will display the information exported
 * starting on current Active Cell. There are two options:
 * <h3> Export ALL CONTACTS<h3/>
 * <br/>
 * The user selects this option and the fields he wants to export, the system
 * will export the fields selected for all contacts in DB.
 * <img src="doc-files/ExportAddress_AllContacts_SequenceDiagram.png">
 * <br/>
 * <h3> Export ONE CONTACT<h3/>
 * <br/>
 * The user selects this option and the system will display a list of contacts
 * to chooose. He will also have to select the fields he wants to export, the
 * system will export the fields of teh selected contact.
 * <img src="doc-files/ExportAddress_OneContact_SequenceDiagram.png">
 * <br/>
 * <br/>
 * <h2>Tests</h2>
 * <br/>
 * I will explain the tests using images to show the application exporting and
 * importing.
 * <br/>
 * <h3> EXPORT TEST<h3/>
 *
 * <br/><br/>
 * Firts of all i've inserted two contacts:
 * <br/>
 * --first and last name-- Manuel Oliveira
 *
 * --Main Address-- Street: street1 Area: Valadares ZipCode: 4405-534 City:
 * Porto Country: Portugal
 * <br/>
 * <img src="doc-files/contact1.png">
 * <br/>
 * <br/>
 * * --first and last name-- Andre Garrido
 * <br/>
 * --Main Address-- Street: street_c1 Area: Porto ZipCode: 4405-000 City: Porto
 * Country: Portugal
 * <br/>
 * --Secondary Address-- *Street: street_c2 Area: RioMaior ZipCode: 1111-111
 * City: Santarem Country: Portugal
 * <br/>
 * <img src="doc-files/contact2.png">
 * <br/>
 * Then, the user should go to AddressMain Menu and Click Export, in the UI
 * select One Contact, select the contact and fields that he wants to export.
 * <br/>
 * <img src="doc-files/export_one1.png">
 * <br/>
 * On click Export, the system will export the fields to active cell.
 * <img src="doc-files/export_one2.png">
 * <br/>
 * <br/>
 *
 * The other option is for the user to Select All Contacts and the fields that
 * he wants to export.
 * <br/>
 * <img src="doc-files/export_all1.png">
 * <br/>
 * On click Export, the system will export the fields to active cell.
 * <img src="doc-files/export_all2.png">
 * <br/>
 * <br/>
 *
 *
 * <h3> IMPORT TEST<h3/>
 *
 * <br/><br/>
 * I will show two kinds of imports: The Contacts only and the Contacts plus
 * Addresses.
 * <br/>
 * <img src="doc-files/import_contacts1.png">
 * <br/>
 *
 * The first three don't have addresses and the last two have one and two
 * addresses.
 *
 * <br/>
 *
 * To import the first three the user selects Import-> Person and select the
 * lines to import.
 * <br/>
 * <img src="doc-files/import_person.png">
 * <br/>
 * On click Import, the system will import the Contacts and save them.
 * <br/>
 * <br/>
 * To import the last two the user selects Import-> Person + Address and select
 * the lines to import.
 * <br/>
 * <img src="doc-files/import_personAddr.png">
 * <br/>
 * On click Import, the system will import the Contacts and Addresses and save
 * them.
 * <br/>
 * <br/>
 *
 *
 * Then we can see the contacts in the system, some with addresses and some
 * without.
 * <br/>
 * <img src="doc-files/imp_contact1.png">
 * <img src="doc-files/imp_contact2.png">
 * <img src="doc-files/imp_contact3.png">
 * <br/>
 * <br/>
 * <h2>Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 * @author Andre Garrido - 1101598
 */
/*

 */
package csheets.userstories.crm02_02.comments.i101598;

class _Dummy_ {
}
