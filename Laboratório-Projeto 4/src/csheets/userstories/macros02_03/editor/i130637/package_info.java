/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.userstories.macros02_03.editor.i130637;

/**
 *
 *
 * <h2>Requirements</h2>
 *
 * There should be a sidebar to consult and edit the values of the variables.
 * When a variable is selected it should appear automatically which cells use
 * that variable (by containing the reference to it in the formula).
 * As soon as the formula is edited that window should be updated.
 *
 * 
 * <br/>
 * 
 * <h2>Analysis</h2>
 * 
 * When a cell with a variable is selected that variable will be compared with
 * all the variables that are in the spreadsheet at the moment. When it finds
 * the same variable its address will be saved to be shown in the sidebar.
 * When a user wants to edit the value or the name of the variable, it will
 * change all of the variables saved with the original name. For this
 * to be possible every time the user selects a cell it will be tested
 * to check if the <code>Variable</code> attribute in the <code>Cell</code>
 * is null or not, if it isn't then all the active cells will be tested
 * to see if they have the same variable or not. Having the same, the system
 * will save the cell's address so that it is possible to show the user
 * in the sidebar. When changing the name or value of the variable
 * the system will apply the same changes to the saved variables.
 * Also if a formula uses and changes the value of a variable it
 * also searches for other cells with that variable and changes its
 * value with the new one.
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Diagram explaining the analysis</h3>
 * 
 * <br/>
 * <img src="doc-files/macros02_03_AnalysisDiagram.png">
 * 
 * <br/>
 * <br/>
 * <br/>
 * 
 * <h2>Design</h2>
 * 
 * In here I show two sequence diagrams that explain the two types of editing
 * variables and their corresponding class diagram so it will help with the
 * development of the use case.
 * 
 * <br/>
 * <br/>
 * 
 * <h3>SD: Edit Manually </h3>
 * 
 * <br/>
 * 
 * <img src="doc-files/macros02_03_SequenceDiagramEditManually.png">
 * 
 * <br/>
 * <br/>
 * 
 * <h3>SD: Edit by Formula</h3>
 * 
 * <br/>
 * 
 * <img src="doc-files/macros02_03_SequenceDiagramEditByFormula.png">
 * 
 * <br/>
 * <br/>
 * 
 * In the sequence diagram above if the formula changes the value of a variable
 * then when the <code>Cell</code> calls the method updateDependencies(), it
 * will change all the value of the variable thus changing the value of the
 * variable in other formulas
 * 
 * <h3>CD:</h3>
 * 
 * <br/>
 * 
 * <img src="doc-files/macros02_03_ClassDiagram.png">
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Tests</h3>
 * 
 * <br/>
 * <br/>
 * 
 * <h3>Comments</h3>
 * 
 * Before beginning to work on the use case I studied it so it would be easier
 * to work with, but when i was doing the research I saw that the implementation
 * of the previous use cases where not correct because Formula.g was not
 * prepared to recieve a variablewhich would not allow my use case to work
 * because of the reference. So I started fixing it and at the same time fixed
 * attribuition operand that also was not working at the time.
 * 
 *
 */
public class package_info {

}
