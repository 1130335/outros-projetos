package csheets.userstories.core09_01.comments.i120674;

/**
 *
 * <h2>Requirements</h2>
 *
 * It should be possible to export the information from a workbook, page(s) or
 * part of a page to a PDF file. In this particular use case it may be useful
 * explore the adoption patterns that help the creation of an elegant file. At
 * this time, it's only required the inclusion of cells' text while exporting.
 * The export option must appear in the "File" menu. Visual aspect doesn't
 * matter at this point, this being, it can be different from the sheet
 *
 * <br/>
 * <br/>
 *
 * <h2>Analysis</h2>
 *
 * When the user selects the export to a pdf file option, a window will appear
 * asking what he wants to export if its the workbook, page(s) or part of a
 * page. After the user chooses what to export he will choose where to save the
 * file and its name.
 *
 * <br/>
 *
 * <img src="doc-files/core09_01_AnalysisDiagram.png"/>
 *
 *
 * <br/>
 * <br/>
 *
 *
 * <h2>Design</h2>
 * <br/>
 * <img src="doc-files/core09_01_DesignDiagram.png"/>
 * <br/>
 * <br/>
 *
 * <h2>Class Diagram</h2>
 * <br/>
 * <img src="doc-files/core09_01_ClassDiagram.png"/>
 * <br/>
 * <br/>
 *
 * <h2>Functional Test for Cell selection</h2>
 * Only did Functional tests because it works with IO files!
 * <br/>
 * <img src="doc-files/FuncTest1.png"/>
 * <br/>
 * <img src="doc-files/FuncTest2.png"/>
 * <br/>
 * <br/>
 *
 */
public class package_info {

}
