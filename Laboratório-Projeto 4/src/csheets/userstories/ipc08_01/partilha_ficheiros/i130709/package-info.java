package csheets.userstories.ipc08_01.partilha_ficheiros.i130709;

/**
 * 
 * Technical documentation regarding the user story IPC08.1 - Partilha de ficheiros
 * <br/>
 * <br/>
 * 
 * <h2>Requirement:</h2>
 * Make an extension that allows sharing files in a folder (outbox) specified in cleansheets.
 * The cleansheets must show the names of files shared on another instance of cleansheets that call.
 * In addition to the name it should still show file's size. It should also be possible to set a folder
 * for receipt of files (inbox) from another cleansheets. The shared folders must be configured in an
 * appropriate file. To no longer need to download, however it is necessary to maintain the updated 
 * file list automatically.
 * 
 * <br/>
 * <br/>
 *  
 * <h2>Analysis</h2>
 * It is needed to create interface so the user can choose to share files.
 * To do that it is needed a client/server architecture so the server will host the
 * share's create. On that, it will be asked the path of the files's folder and the port
 * to use on the connection. On the other side, the client will get noticed of the server IP
 * so he can establize a connection, after that he should be able to see all the files's names
 * and their size.
 * 
 * <br/>
 * 
 * <h3>Analysis Create Share Diagram:</h3>
 * 
 * <img src="doc-files/ipc08_01_SequenceDiagramCreateShareAnalyis.png"/>
 * 
 * <h3>Analysis Access Share Diagram:</h3>
 * <img src="doc-files/ipc08_01_SequenceDiagramAccessShareAnalyis.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * <h2>Design</h2>
 * For this user story it as asked to create an extension that allow the user to set up
 * a server in his instance of cleansheets so he could share files from a selected folder.
 * To do that, it was implemented the SharingFilesController, TcpServer. The controller provides
 * a mehtod startSharing that is responsable for creating a instance of TcpServer that will "send"
 * the files in one directory, writting them on a ObjectOutputStream object, by a created socket.
 * On the other hand, another instance of cleansheets will act as the client, he will access that created
 * shared. To do that he must enter the IP adress and the port that the server had noticed after he created
 * the share. Adding SharingFilesController to TcpClient the user will get the file's name shared in one JList
 * as in another JList he gets the file's size. To make this possible SharingFilesController provides accessSharing method
 * that will instance TcpClient and will store the ArrayList of File sent by the server, this is possible thanks to
 * getFilesShare() method on TcpClient that will write this ArrayList on a ObjectInputStream object.
 * So as we can see, the sharing is done. But it was also asked to prepare a future download, so
 * it was implemented a Button ( set inputbox ) that is responsible for show a JFileChooser
 * with another implemented button ( new folder ), so the user can select a existing folder for the inputbox
 * or he also can create a new folder to do it.
 * 
 * <br/>
 * 
 * <h3>Design Diagram:</h3>
 * 
 * <img src="doc-files/ipc08_01_SequenceDiagramDesign.png"/>
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h3>Class Diagram:</h3>
 * 
 * <img src="doc-files/ipc08_01_ClassDiagram.png">
 * 
 * 
 * <br/>
 * <br/>
 * 
 * 
 * <h2>Tests</h2>
 * 
 * In this user story all tests are functional tests
 * as you need to check if the files are actually
 * "sent" and "received". And it is needed to see
 * if the share is created and accessed by seeing
 * if JList are properly filled will the file's shared
 * and their size on the other JList
 * 
 * <h3>Create share:</h3>
 * 
 * - As a user that will create a share you firstly
 * need to select a port that will be waiting for other
 * instance to connect. After that you will set your 
 * folder's path, the one you want to share files from.
 * After that you Press OK Button, to get the share created.
 * 
 * <img src="doc-files/createShare1.jpg">
 * 
 * <img src="doc-files/createShare1.jpg">
 * 
 * <img src="doc-files/createShare1.jpg">
 * 
 * 
 * <h3>Access share:</h3>
 * 
 * - As a user that whats to access one share, 
 * you just need the IP and port from the created
 * share. After enter thoose fields you just need
 * to Press OK Button and wait for a confirmation
 * Then you will see the JLists filled in with
 * the file's name and their sizes.
 * 
 * <img src="doc-files/accessShare1.jpg">
 * 
 * <img src="doc-files/accessShare1.jpg">
 * 
 * <h3>Get ready for future download:</h3>
 * 
 * - It was said that it was needed to prepare
 * a inbox for a future download. So it is
 * implemented a JFileChooser just to directories
 * and it also has a extra button (New folder), 
 * so if the user wants to create a new folder
 * to the inputbox he is able to do it.
 * 
 * <img src="doc-files/input1.jpg">
 * 
 * <img src="doc-files/input2.jpg">
 * 
 * <img src="doc-files/input3.jpg">
 * 
 * fixed--
 * <br/>
 * <br/>
 * 
 */

/*
 * @author 1130709
 */
class _Dummy_ {
}

