/**
 * Technical documentation regarding the user story macros09_01: Edit Menus.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Edit Menus. Allow the user to extend the cleansheet's menu options by adding
 * the execution of macros ("regular" or beanshell). The types of extensions
 * are: before=executes the macro before the option; after=executes the macro
 * after the option; around=executes the macro instead of the option. It should
 * also be possible to activate or deactivate menu options with new
 * macros/beanshell functions.
 * <br/>
 * <br/>
 * <b>Use Case "Edit Menus through sidebar":</b> The user opens the sidebar. The
 * system displays the list of available menu options to extend, the available
 * macros and the option of execution. The user selects one element of each list
 * and when to execute the macro. The system registers the information and
 * informs the operation went correctly.
 * <br/>
 * <br/>
 * <b>"Edit Menus through functions":</b>The user executes a macro function that
 * contains the action he wants to perform. The system interprets the action and
 * executes it. The user is informed of the alterations made to the menu option.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * The new part of this use case is the association of a macro to a menu option.
 * As the previous line says, a menu option, therefore the place where the
 * association needs to be done is in the menu, so that when an option is
 * selected the macro executes when its supposed to.
 * <br/>
 * Although it seems pretty straight forward I have a problem. There are no
 * macros available in the program except beanshell macros, therefore these are
 * the only ones available for the association.
 * <br/>
 * <br/>
 * <img src="doc-files/AnalysisDiagram.png"/>
 * <br/>
 * The previous diagram represents how the recognition of an action being
 * selected should be detected by Menu. After it being detected we need to see
 * which macro to execute and when.
 * <br/>
 * <br/>
 *
 * <h2>3. Tests</h2>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is the association, so what we need to test is
 * the association and execution of the pair.
 * <br/>
 * Since the other required parts have already been implemented and tested by my
 * piers, I only need to test the association, and to do this (since it happens
 * in the Menu) I will perform functional test and present them here.
 * <br/>
 *
 * <h2>4. Design</h2>
 * To edit the menu options we need to figure a list of things out:
 * <br/>
 * <br/>
 * <b>-How to get the complete list of menu actions/options?</b>
 * <br/>
 * <br/>
 * <b>-How to get the list of available macros?</b>
 * <br/>
 * <br/>
 * <b>-How to associate the option with the macro and where?</b>
 * <br/>
 * <br/>
 * <b>-How to make that the association can be altered by functions
 * (macros)?</b>
 * <br/>
 * <br/>
 * <b>-How to make it that when the action is selected the macro executes?</b>
 * <br/>
 * <br/>
 *
 * So lets start answering these questions
 * <br/>
 * <br/>
 *
 * <h3>4.1 How to get the complete list of menu actions/options?</h3>
 * <br/>
 * The options are "Actions" that are added to the MenuBar's menus, so to get
 * the list we just need to create a method in the "MenuBar"class that creates
 * and returns a list of all the existing action.
 * <br/>
 *
 * <h3>4.2 How to get the list of available macros?</h3>
 * <br/>
 * Since the only macros available are Beanshell all we need to do to get the
 * list is call the method "getScriptList()" from the class
 * "BeanShellInterpreter"
 * <br/>
 *
 * <h3>4.3 How to associate the option with the macro and where?</h3>
 * <br/>
 * This association needs to be done in the "MenuBar". We need to implement in
 * this class the "ActionListener" interface so that when an option is selected
 * we can see which one is it and perform it accordingly.
 * <br/>
 * To keep track of this association a map will be implemented where the keys
 * are the actions and the values are a String array with the keyword for when
 * to execute the macro, the name of the file that contains the BeanShell macro
 * and whether or not the extension is active.
 * <br/>
 * To do this we need to create a method in the MenuBar that creates a new entry
 * in the map for the combination, or alters an existing one for that action.
 *
 * <h3>4.4 How to make that the association can be altered by functions
 * (macros)?</h3>
 * <br/>
 *
 * <h3>4.5 How to make it that when the action is selected the macro
 * executes?</h3>
 * For this we need to create a class that implements the ActionListener
 * interface. This class is added to every item in the MenuBar and when the
 * option is clicked, the event is caught, so that it can see if the action has
 * an extension or not and execute it accordingly.
 * <br/>
 * <br/>
 *
 * In conclusion we need the methods and attributes represented in the following
 * class diagram;
 * <br/>
 * <img src="doc-files/DesignClassDiagram.png"/>
 * <br/>
 * <br/>
 *
 *
 * <h2>5. Coding</h2>
 * see:<br/>
 * <a href="../../../../csheets/ui/MenuBar.html">csheets.ext.comments</a><br/>
 * <a href="../../../../csheets/csheets.ext.beanshell.html">csheets.ext.comments.ui</a><br/>
 * <br/>
 *
 * <h2>6. Final Remarks</h2>
 * Due to lack of implementation of macros this use case only uses BeanShell
 * Macros.
 * <br/>
 * Due to lack of time the integration of BeanShell functions for
 * activating/deactivating extensions is not available.
 * <br/>
 * <br/>
 * <br/>
 *
 * @author coelho
 */
package csheets.userstories.macros09_02.i130353;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author coelho
 */
class _Dummy_ {
}
