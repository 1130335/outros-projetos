/**
 * Technical documentation regarding the user story ipc01_02: Sharing Automatic
 * Update.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * After the connection is established between two instances of cleansheets,
 * changes made in either 'side' must lead to immediate updates. It should be
 * possible to share all the properties of the cells (e.g., one should include
 * the formatting). It is not necessary to share the formulas.
 * <br/>
 * <br/>
 * <b>Use Case "Sharing Automatic Update":</b> The user selects from the
 * application's extensions the option for connecting with another instance. As
 * soon the connection is established, any alteration made by the user until
 * that moment will be automatic send to the other instance. <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Using the same thought of the use case of the previous week related to the
 * sharing of cells between cleansheets instances, sharing cells in this case
 * may be total, in other words, not only the cell content will appear in the
 * instance that receives, but also the properties and formatation that this
 * cell may contain. As the sharing should be automatic, the connection must
 * always be active like a chat, only that the "messages" are transmitted cells.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagram represents only a first approach of the use case for
 * the instance that sends the cell that was altered to the instance connected
 * to it. This should not be taken as the final design diagram and the methods
 * used in it will probably not appear in the final solution. <br/>
 * <br/>
 * <b>Sender diagram</b><br/>
 * <img src="doc-files/ipc01_02_analysis_send.png"/>
 * <br/>
 * <br/>
 * The following diagram represents only a first approach of the use case for
 * the instance that receives the cell that was altered to the instance
 * connected to it. This should not be taken as the final design diagram and the
 * methods used in it will probably not appear in the final solution. <br/>
 * <br/>
 * <b>Receiver diagram</b><br/>
 * <img src="doc-files/ipc01_02_analysis_send.png"/>
 * <br/>
 * <br/>
 * From the previous diagrams, we conclude that we will be using the same
 * strategy used in the previous use case. What changes in this one is the fact
 * that the user only sends and receives a cell at a time and the connection
 * must be active until one of the users at either sides of the cleansheets
 * decides to close it.
 * <h3>Analysis of Core Technical Problem</h3>
 * In order to find online cleansheets, the end sender will use UDP (User
 * Datagram Protocol) in which the sender will wait for the receiver ends online
 * to send a broadcast datagrams, the exact same thing the previous user case
 * does. The difference in this one is the TCP (Transmission Control Protocol)
 * usage. In the previous UC, it was only allowed to send a range of cells and
 * the connection was over. In this one, the TCP connection will be open until
 * the user says so, sending one active cell at a time. We will need to take our
 * finish TCP implementation and create a variation for the current problem,
 * still using the same UDP implementation. In this case, both instances of the
 * cleansheets will be "clients" working connected to a server.
 *
 * <br/>
 * <br/>
 * <h2>3. Tests</h2>
 * Only some methods of the controller were tested, as some weren't possible to
 * test do to the it's funcionality as for example the send and receive cells
 * methods.
 *
 * <h2>4. Design</h2>
 * After some deliberation about how this would use case would work, the first
 * analysis becomes obsolete because after connecting to each other, the
 * instances will get all the cells in the active spreadsheet and send them. One
 * of the problems encountered in this use case is the fact that after
 * connecting only the first instances to click in Update in the User Interface
 * will send correctly the cells. The other will be as like it was cancelled.
 * There will be a need to update again if that instance really wants to send
 * it's cells. The other problem is the sending of the cells properties, which
 * doesn't work right now.
 * <br/>
 * <br/>
 * <img src = "/doc-files/ipc01_02_design.png"/>
 *
 * <h2>5. Coding</h2>
 * All the coding can be found in the ipc package within the extensions package,
 * having a controller a UI.
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.ipc01_02.comments;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
