/**
 * Technical documentation regarding the user story ipc01_03: several shares.
 * 
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * It should be possible to have multiple shares (with different names).
 * should be possible to connect the various shares. The location that
 * receives the shares (in the spreadsheet) can be different with local
 * "source". also it should be possible to share formulas!
 * <br/>
 * <br/>
 * <b>
 * <br/>
 * <br/>
 * 
 * <h2>2. Analysis</h2>
 **The Analysis is about having several shares and for doing
 * that we should have possibility to choose the port for the
 * connection ,and then we can have 2 or more connections at
 * same time , but unfortunately there was some problems with
 * implementation of UC 1.1 and 1.2 so i had to correct them
 * first and because of that reason i didn't get time too do all
 * l the points of UC 1.3 and now we can have several shares with
 * different ports but we can choose the location of that in the 
 * spreadsheet .<br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>

 * <h2>Send the cells</h2>
  
 * <img src="doc-files/ipc1-3_design_Send.png"> 
 * 
 * 

 * <h2>receive the cells</h2>
  
 * <img src="doc-files/ipc1-3_design_receive.png"> 
 * 
 * <br/>
 
 * 


 * <h2>3. Tests</h2>
 * <br> 
 * the testes for this UC are like the first use cases 
 * testes because both are doing the same functionality.
 * <br/>
 
 * 
 */
package csheets.userstories.ipc01_03.comments.i130777;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
