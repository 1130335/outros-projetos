/**
 * Documentation regarding to the use case Macros08_01 : BeanShell Edit Window
 * <br/>
 * <br/>
 *
 * <b>1. Requirement</b>
 * <br/>
 * <br/>
 * Create an extension that allows the writing of macros using the language Java
 * Beanshell. This option must be integrated with the other macros. The user
 * must have the option to write in beanshell language or macros cleansheets.
 * Also the user must be able to write a beanshell script that opens a new
 * workbook, create a macro, execute the macro and show the result in a window.
 * <br/>
 * <br/>
 * <b>2. Analysis</b>
 * <br/>
 * <br/>
 * This extension must support the integration with the other macros. If the
 * user chooses the beanshell language the extension must be prepared to accept
 * not only macros but also scripts, used to open a new workbook. If the choice
 * is macros cleansheets the extension must allow the user to create a macro,
 * execute it and show the result in a window.
 * <br/>
 * <br/>
 * <img src="doc-files/analysis_Diagram_Macros08_01.png"/>
 * <br/>
 * <br/>
 * <b>3. Design</b>
 * <br/>
 * <br/>
 * Scripts Option Sequence Diagram
 * <br/>
 * <br/>
 * <img src="doc-files/design_DiagramScriptOption_Macros08_01.png"/>
 * <br/>
 * <br/>
 * Macros Option Sequence Diagram
 * <br/>
 * <br/>
 * <img src="doc-files/design_DiagramMacroOption_Macros08_01.png"/>
 * <br/>
 * <br/>
 * Class Diagram
 * <br/>
 * <br/>
 * <img src="doc-files/macros08_01_ClassDiagram.png"/>
 * <br/>
 * <br/>
 * <b>3. Tests</b>
 * <br/>
 * <br/>
 * To test this use case first we have to choose the BeanShell extension. Two
 * options will be shown and we have to choose one.
 * <br/>
 * <br/>
 * <img src="doc-files/ChooseOption.png"/>
 * <br/>
 * <br/>
 * First we will choose 'Macro Option'. The window bellow will appear.
 * <br/>
 * <br/>
 * <img src="doc-files/MacroOptionWindow.png"/>
 * <br/>
 * <br/>
 * Now we write the expression and press the execute button.
 * <br/>
 * <br/>
 * <img src="doc-files/ExecuteMacro.png"/>
 * <br/>
 * <br/>
 * Now we will close and open the 'Script Option'. A list with the existing
 * scripts will appear and we have to choose one and press the 'Load Option'.
 * <br/>
 * <br/>
 * <img src="doc-files/ChooseScriptAndLoad.png"/>
 * <br/>
 * <br/>
 * Now, with the script loaded we press the 'Execute Option'.
 * <br/>
 * <br/>
 * <img src="doc-files/ExecuteScript.png"/>
 * <br/>
 * <br/>
 * The script was executed successfully. Now we will choose the 'Edit Option'. A
 * new window will open and we may edit the script there.
 * <br/>
 * <br/>
 * <img src="doc-files/EditScript.png"/>
 * <br/>
 * <br/>
 * It is also possible to create a new script. For that funcionality we have to
 * choose the 'Create Option'.
 * <br/>
 * <br/>
 * <img src="doc-files/CreateScript.png"/>
 * <br/>
 * <br/>
 * The script was successfully created and now we will test the 'Delete Option'.
 * <br/>
 * <br/>
 * <img src="doc-files/ChooseDeleteOption.png"/>
 * <br/>
 * <br/>
 * We can see that the script was deleted.
 * <br/>
 * <br/>
 * <img src="doc-files/ScriptDeleted.png"/>
 * <br/>
 * <br/>
 */
package csheets.userstories.macros08_01.beanshellwindow.i130728;

/*
 * @author João Cabral
 */
class _Dummy_ {

}
