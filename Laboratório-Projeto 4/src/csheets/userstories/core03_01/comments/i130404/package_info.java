package csheets.userstories.core03_01.comments.i130404;

/**
 * Technical documentation regarding the user story core03_01: Sort cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Sort the content of the cells. For this use case is only necessary to sort a
 * collumn. You must indicate the order (ascendant or descendant). The
 * interaction with the user can be based only in the options of the
 * application.
 * <br/>
 * <br/>
 * <b>Use Case "Sort cells":</b> The user selects the range of the cells that he
 * wants to sort. After that the user select the sort option in the Extensions
 * menu. Than the user select the sorting option that he pretends.
 * <br/>
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since sorting cells will be supported in a new extension to cleansheets we
 * need to study how extensions are loaded by cleansheets and how they
 * work.<br/>
 * The first sequence diagram in the section
 * <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application
 * Startup</a> tells us that extensions must be subclasses of the Extension
 * abstract class and need to be registered in special files.<br/>
 * The Extension class has a method called getUIExtension that should be
 * implemented and return an instance of a class that is a subclass of
 * UIExtension.<br/>
 * In this subclass of UIExtension there is a method (getSideBar) that returns
 * the sidebar for the extension. A sidebar is a JPanel.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <br/>
 * <img src="doc-files/core03_03_uc_analysis.png">
 * <br/>
 * <br/>
 * From the previous diagram we see that the uc is quite easy to implement
 * without the need of adding much classes or atributes. At this point it was
 * only needed to create two new classes. <br/>
 * <br/>
 * <h3>Analysis of Core Technical Problem</h3>
 * We can see a class diagram of the domain model of the application
 * <a href="../../../../overview-summary.html#modelo_de_dominio">here</a><br/>
 * The domain model is the same that the "analisys" diagram because after the
 * draft diagram the use case was quite easy to implement and there was not the
 * need to change it . From the domain model we see that there are two classes
 * that support the whole use case. The SortAction that works almost like a
 * "controller" because it manages the whole use case to work. The other
 * important class is SortSelectedCells that is a UI interface responsible to
 * allow the user to select the sorting method between Ascending and Descending.
 * <br/>
 * If we open the {@link csheets.core.ext.sort.SortAction} code we see the
 * SortAction defined as:
 * <code>public class SortAction extends FocusOwnerAction</code>. If we further
 * investigate the hierarchy of {@link csheets.core.ext.sort.SortAction} we see
 * that it extends FocusOwnerAction and this one extends Base Action.
 * <br/>
 *
 * <br/>
 *
 * The following class diagram depicts the relations between classes for this UC
 * .<br/>
 * <img src="doc-files/core03_03_classes_diagram.png">
 * <br/>
 *
 * <pre>
 * {@code
 *
 *	}
 * }
 * </pre>
 * <h2>3. Design</h2>
 *
 *
 * <br/>
 * <br/>
 *
 * @author IvoTeixeira
 */
public class package_info {

}
