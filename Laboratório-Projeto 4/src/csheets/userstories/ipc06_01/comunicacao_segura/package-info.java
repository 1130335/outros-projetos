/**
 * Technical documentation regarding the user story ipc06_01: safe
 * communication.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Safe communication. Implement a mechanism that allows establishing safe
 * (cyphered) connections between instances of "cleansheets". To test the
 * infra-structure a simple sidebar window that presents the instances found on
 * the network should be implemented. Every instance has a name (which must be
 * unique, or concatenated with the IP address).
 * <br/>
 * <br/>
 * <b>Use Case "Safe Communication":</b> The user clicks on the sidebar to see
 * the instances that use safe connections available on is network. The system
 * displays the list. The Use Case ends.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * The base communication is already implemented. This Use Case will simply add
 * the encoding part to the code. To do that the application will have to have a
 * few alterations in the code.
 * <br/>
 * The communication is already divided in two part: Recognition of other
 * instances in the network (that are available to stablish a connection). After
 * the recognition, the actual connection/pairing between the two instances.
 * <br/>
 * To tackle the recognition problem the application shall use UDP (User
 * Datagram Protocol). Once the user chooses to see the list of available
 * instances the application sends a UDP datagram and expects to receive an
 * answer. If an answer is received (or multiple answers) that means another
 * instance received the datagram, is available and sent an answer. Every
 * instance that replies is added to the list.
 * <br/>
 * The application already does this, so there's only minor changes to do.
 * <br/>
 * When the datagram is received by an instance it must answer with its name and
 * the info needed to do the coding, so when the TCP (Transmission Control
 * Protocol) connection is established, the only thing different now is that the
 * info is encoded before sending and decoded after sending, using the info
 * received from the UDP info transfer.
 * <br/>
 * <br/>
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <br/>
 * <br/>
 * <img src="doc-files/firstAnalysisSequenceDiagram.png">
 * <br/>
 * <br/>
 * From the previous diagram we see that we need to add a new class for
 * Encoding/Decoding needs to be created.
 * <br/>
 * Therefore, at this point, we need to study what type of encoding should be
 * implemented and where should the encoding happen.
 * <br/>
 * I thought of using either RSA encrypting or AES and decided to use AES. Since
 * AES only requires-me to send the key used for encrypting and decrypting, and
 * the classes and methods that I require are already available in the standard
 * Java library "crypto".
 * <br/>
 * Now that we decided on what type of encryption to use, we must think of were
 * to keep the key used on the communication between instances. I came to the
 * conclusion that the best place to keep that key is in the
 * <a href="../../../ext/ipc/domain/NetworkInstance.html">"NetworkInstance"
 * class</a>.
 *
 *
 * <h3>Analysis of Core Technical Problem</h3>
 * After the previous analysis I concluded that most of the code was already
 * implement, so all I had to do is encoding part and select which encoding to
 * do. As we can see in the US of
 * <a href="../../ipc01_01/comments/i130335/package-summary.html">IPC 1.01</a> I
 * shall do minor alterations in that code for the encoding.
 * <br/>
 * The only class that I considered importante to put here on a "class diagram"
 * is a possibility for a Encryption class:
 * <br/>
 * <img src="doc-files/AnalysisEncryptionClassDiagram.png">
 * <br/>
 * Since theres no indication of the need for more then one tipe of encryption I
 * decided to use only one class for this purpose. Else way I would have created
 * an Interface for Encryption and created an implementation called
 * AESEncryption.
 *
 * <h2>3. Tests</h2>
 * After reviewing the documentation again I concluded that the only main
 * innovation in the code is the encryption, therefore I only need to test two
 * aspects of the application:
 * <br/>
 * <b>--></b> Test encryption/decryption of data (specially of "cells");
 * <br/>
 * <b>--></b> And communication between cells using encrypted data, to test
 * sending encrypted data and decrypting it after receiving it;
 * <br/>
 * Since the requirements are not clear on which communications need to be
 * secure or not, I decided to implement on the main source of communication
 * (IPC01.1) and leave the code ready to be implemented in every other type of
 * communication, and alter the tests accordingly.
 * <br/>
 * To see the actual tests see the classes:
 * <br/>
 * <b>--></b> csheets/ext/ipc/Ecncryption/EncryptionTest;<br/>
 * <b>--></b>
 * csheets/ext/ipc/Ecncryption/ReceiveCellsControlelrWithAESTest;<br/>
 * <b>--></b> csheets/ext/ipc/Ecncryption/SendCellsControlelrWithAESTest;<br/>
 * The test were created and thoughts before the desing and will be updated
 * while doing desing and implementation.
 * <br/>
 * <br/>
 * <h2>4. Design</h2>
 * Now in the final part of planing, the Design, every thing is clear and
 * explicit due to the information given in the previous sections of this
 * javadoc.
 * <br/>
 * The only thing needed now is to implement the AESEncryption class, with 3
 * methods:
 * <br/>
 * <b>--></b> a method that recieves the key size and return a AES SecretKey;
 * <b>--></b> a method that recieves a cell and the key and encrypts the cell;
 * <b>--></b> and a method that recieves an encrypted cell and the key in order
 * to decrypt the cell.
 * <br/>
 * After this class is created and fully implemented, the UDP thread needs to be
 * altered to send a generated key if its the sender, and it needs to receive
 * the key and store it if its the receiver.
 * <br/>
 * The TCP thread also needs to be altered in order to do encryption and decryption in either sides.
 * <br/
 *
 * <h2>5. Coding</h2>
 * see:<br/>
 * <a href="../../../../csheets/ext/ipc/Encryption/AESEncryption.html">csheets.ext.comments</a><br/>
 * <br/>
 * <h2>6. Final Remarks</h2>
 * Due to lack of time this Use case was not fully implemented because i needed to alter the code so everybody
 * used a new class to do communication, because the code as is does not allow me to save the encrypting key that i need
 * on the receiving side.
 * <br/>
 * If i continue next week this use case i can refine it a lot and implement it correctly.
 * <br/>
 * <br/>
 *
 * @author Pedro Coelho
 */
package csheets.userstories.ipc06_01.comunicacao_segura;

/**
 * @author Pedro Coelho
 */
class _Dummy_ {
}
