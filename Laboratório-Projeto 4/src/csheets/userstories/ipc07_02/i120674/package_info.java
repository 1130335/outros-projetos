package csheets.userstories.ipc07_02.i120674;

/**
 * <h2>Requirement:</h2>
 *
 * The board must be designed in the sheet. That sheet will be exclusively to the game . The plays' history will appear bellow the
 * board.
 *
 * <br/>
 * <br/>
 *
 * <h2>Analysis:</h2>
 *
 *
 *
 * <br/>
 *
 * <h3>Analysis Diagram:</h3>
 *
 * <img src="doc-files/ipc07_01_AnalysisDiagram.png">
 *
 * <br/>
 * <br/>
 *
 * <h2>Design</h2>
 *
 * For this use case I thought of the common options of Creating a GameRoom where you select a game and wait for opponents to
 * appear and Searching for Players where you search for a GameRoom and join, then both players play.
 *
 * <br/>
 * <br/>
 *
 * <h3>SD: CreateGameRoom</h3>
 *
 * <img src="doc-files/ipc07_01_DesignDiagram_CreateGameRoom.png">
 *
 * <br/>
 * <br/>
 *
 * Here we have the creation of the room, I use the UTP Protocoll to find opponents and in here the user will be like a server
 * where the other users will search for him.
 *
 * <br/>
 * <br/>
 *
 * <h3>SD: SearchPlayers</h3>
 *
 * <img src="doc-files/ipc07_01_DesignDiagram_SearchPlayers.png">
 *
 * <br/>
 * <br/>
 *
 * Here we have the search for players, I use the UTP Protocoll to find opponents and in here the user will be like a client where
 * he will search for server like users.
 *
 * <br/>
 * <br/>
 *
 * <h3>Class Diagram</h3>
 *
 * <img src="doc-files/ipc07_01_ClassDiagram.png">
 *
 *
 * <br/>
 * <br/>
 *
 *
 */
public class package_info {

}
