/**
 * Technical documentation regarding the user story ipc01_01: initiate sharing.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Find workbooks. Allow to search in local disk, files that are spreadsheet. It
 * should do a thorough research to all folders and display a list with all
 * files found. The list should be presented in a sidebar that will be updated
 * as files are being found. It must be possible to open a selected file in the
 * list.
 * <br/>
 * <br/>
 * <b>Use Case "Find Workbooks":</b>The user choose find workbooks' option. A
 * list in sidebar will be presented to the user. As the files are found , the
 * list will be updated. If the user want he can open one of the files
 * (spreadsheet) found.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * My analysis
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="doc-files/ipc02_01_uc_realization.png"/>
 * <br/>
 * <br/>
 * <h2>3. Design</h2>
 * <h3>Start the extension and sidebar</h3>
 * <img src = "doc-files/ipc02_01_design.png"/>
 *
 * <h3>Thread that search files</h3>
 * <img src = "doc-files/ipc02_01_design2.png"/>
 * <h3>Open a workbook</h3>
 * <img src = "doc-files/ipc02_01_design3.png"/>
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 */
package csheets.userstories.ipc02_01.find_workbooks.i130313;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
