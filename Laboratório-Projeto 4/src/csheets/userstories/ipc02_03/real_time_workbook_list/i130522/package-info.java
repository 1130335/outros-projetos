/**
 * Technical documentation regarding the user story ipc02_03: Real time workbook
 * list.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * The user choose "Real Time WorkBook List', option C listed in the sidebar
 * that will be presented to the user. As the user adds folders to the list,
 * files with the extensions ".cls" there will be added to the list and
 * presented in a window named "WorkBook list". The list will be updated if any
 * file is removed or any file will be added. The user may add more than one
 * folder to search.
 * <br/>
 * <br/>
 *
 *
 * <b>Use Case "Real Time List of Workbooks":</b>
 * It may be possible to define one or more folders that can be "read" regularly
 * to identify files of the cleensheets and adds theses to a window named "Real
 * time WorkBook List"<br/>
 * <br/>
 *
 *
 *
 * <h2>2. Analysis & Design </h2>
 *
 *
 * <b> Analysis of the UC Realization </b>
 *
 *
 * <img src="doc-files/ipc02_03_uc_realization.png">
 *
 * <br>
 *
 * <b>Class Diagram</b> illustrating how classes will interact in this use case:
 *
 *
 * <img src="doc-files/ipc_realtimesearchclassdiagram.png">
 *
 * <b>Sequence Diagram</b> illustrating how the new extension will be added:
 *
 * <img src="doc-files/ipc02_03_design.png">
 *
 *
 * <b>Sequence Diagram</b> illustrating how list will be filled and updated:
 *
 * <img src="doc-files/ipc02_01_design2.png">
 *
 * <br/>
 * <h2>Tests</h2>
 * Since this use case is based on showing an updated list, we need to make sure
 * that: -The list is completed with all the files ".cls" in the folder/folders
 * selected. -If any file from the folder is removed, the list must be updated.
 * (The observer pattern will make this happen) -If any file is added to the
 * folder, the list must be updated.(The observer pattern will make this happen)
 * -The user may be notified in any list update.
 *
 * <br/>
 * <br/>
 */
package csheets.userstories.ipc02_03.real_time_workbook_list.i130522;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
