/**
 * Technical documentation regarding the user story core05_01: Email Configuration.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 *
 * <br/>
 * Email configuration: There should be a window to configure the information
 * for the email account information and service to use. There needs to be a
 * button to test the email. The window should be a sidebar. The information
 * needs to be stored in an "appropriate" file to saving this "global"
 * information. To test the email it should be possible to select a cell to send
 * the content.
 * <br/>
 * <b>Use Case "Email configuration":</b>
 * <br/>
 * -Part 1 - Window configuration: The user starts the use case. The system
 * presents the window to insert the information. The user enters the data and
 * selects the service. The system informs of the success and saves the info.
 * <br/>
 * -Part 2 - Test Button: The user presses the button. The systems asks him to
 * identify the cell to send the content. The user selects the cell. The content
 * is sent by email and the user is informed of the success of the operation.
 *
 * <h2>2. Analysis</h2>
 * Since the email configuration will be supported in a new extension to
 * cleansheets we need to study how extensions are loaded by cleansheets and how
 * they work.<br/>
 * The first sequence diagram in the section
 * <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application
 * Startup</a> tells us that extensions must be subclasses of the Extension
 * abstract class and need to be registered in special files.<br/>
 * The Extension class has a method called getUIExtension that should be
 * implemented and return an instance of a class that is a subclass of
 * UIExtension.<br/>
 * In this subclass of UIExtension there is a method (getSideBar) that returns
 * the sidebar for the extension. A sidebar is a JPanel. After the configuration
 * is done the user can use the "test email" button to test the configurations that will be included in the sidebar.
 * <br/>
 * The JavaMailAPI gives the possibility to establish and connect a SMTP Server of a given User E-Mail and send e-mails using that sever connection.
 * <br/>
 * To send the email we need to do three steps:
 * <br/>
 * 1) Setup the SMTP properties (Host, Port and some TLS Protocol Settings);
 * <br/>
 * 2) Start the Connection Session to the SMTP Server with Authentication within the server;
 * <br/>
 * 3) Create the intended message and send it.
 * <br/>
 * Here is short code example on how to send an email, divided by steps:
 * <br/>
 * <pre>
 * {@code 
 * 	public static void sendMail() {
		//Step 1 - Seting up SMTP properties
 *		final String email_address = "sender@gmail.com";
 *		final String password = "password";
 *		
 *		Properties smtp_props = new Properties();
 *		smtp_props.put("mail.smtp.starttls.enable", "true"); 	// enables the STARTTLS function
 *		smtp_props.put("mail.smtp.auth", "true"); 		// enables the Authentication on the SMTP Server
*		smtp_props.put("mail.smtp.host", "smtp.gmail.com"); 	// sets the SMTP Server Host Address
 *		smtp_props.put("mail.smtp.port", "587"); 		// sets the SMTP Server Port
 *		
 *		//Step 2 - Creating the Connection Session and authenticating the user
 *		Session session = Session.getInstance(smtp_props, new javax.mail.Authenticator() {
 *			protected PasswordAuthentication getPasswordAuthentication() {
 *				return new PasswordAuthentication(email_address, password);
 *			}
 *		});
 *		
 *		// Creates a new E-Mail and Sends to the Recipients List
 *		try {
 *			Message message = new MimeMessage(session);
 *			message.setFrom(new InternetAddress(email_address));
 *			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("recipient@gmail.com"));
 *			message.setSubject("JAVA Test E-Mail");
 *			message.setText("This is a testing e-mail sended by a Java Application using the JavaMail API.");
 *			Transport.send(message);
 *		} catch (MessagingException e) {
 *			throw new RuntimeException(e);
 *		}
 *	} 
 * }
 * </pre>
 * <br/>
 * <h2>3. Tests</h3>
 * From the previous analysis whe can assume that the core functionality of this use case is is to be able to add an SMTP Configuration * to the application and save it on a proper file. Load the previous configuration, edit it, save it againg and send a test email to
 * verify the SMTP configuration.
 * <br/>
 * In order to develop and garante that the code is functional and correct i devised theese test:
 * - Test the creation, editing and storing of the <i>Properties Object</i>;<br/>
 * - Test the importation of the stored configuration;<br/>
 * - And test sending an email to the entered email acount;<br/>
 * <br/>
 * see: <code>csheets.ext.email.EmailConfigurationTest</code>
 * <br/>
 * <h2>4. Design</h2>
 * To realize this user story, we will need to create a subclass of Extension and also UIExtension. For the sidebar
 * we need to implement a JPanel. In the code of the extension <code>chseets.ext.style</code> it's possible to find 
 * examples that illustrate how to implement these techical requirements
 * <br/><br/>
 * The following diagrams illustrate core aspects of the design of the solution for this use case
 * <br/>
 * <img src="doc-files/DesignEmailConfigurationPart1.png">
 * <br/>
 * The previous diagram represents the first part of this use case, where while the UI is being created the controller imports the
 * information from the Email Properties, if it exists, so that the window displayes the current email configuration.
 * <br/>
 * After the user enters is email configuration the UI sends the information to be saved. This information is sent to the <i>Email</i>
 * class that will be responsible for treating email related matters. It recieves the information, creates a new Properties file and 
 * sends it to the <i>EmailConfigurationProperties</i> class that saves it. This class is also responsible for importing the info from
 * the file when the UI is created.
 * <br/>
 * <br/>
 * The next diagram represents the second part of the use case, the email test button, sending an test email.
 * <br/>
 * <img src="doc-files/DesignEmailConfigurationPart2.png">
 * As we can se above, the UI asks the SpreadsheetTable for the selected cells, so he can send them to the Email class to hand the cells 
 * and the email properties file to the <i>EmailSTMP</i> so he cand send the test email.
 * <br/>
 * To see the unitary test see the <i>EmailTest class</i>.
 * <br/>
 * Now all we need is implement the code.
 * <br/>
 * <br/>
 * <h2>5. Coding</h2>
 * see:
 * <br/>
 * <br/>
 * <h3>5.1 Functional Tests</h3>
 * <br/>
 * <img src="doc-files/testes_funcionais1.png">
 * <br/>
 * <br/>
 * <img src="doc-files/testes_funcionais1.png">
 * <br/>
 * Tests that comprove the use case is working.
 * <h2>6. Final Remarks</h2>
 * <br/>
 * When using gmail the "Allow less secure apps" option must  be "ON" and the "2-Step Verification" of the password must be off for this
 * to work.
 * <br/>
 * <br/>
 */
 
package csheets.userstories.core05_01.enviar_email;

/**
 *
 * @author coelho
 */
class _Dummy_ {
}
