/**
 * Documentation regarding to the use case Core05_02 : Send email
 * <br/>
 * <br/>
 * <b>Requirement</b>
 * <br/>
 * <br/>
 * Add a window that allows the user to send an email. It should be possible to
 * indicate e-mail recipients, the subject and the message. It should be
 * possible to include in the message body the contents of an area of the sheet.
 * In the sidebar window should now display the history of all sent messages. By
 * clicking on one of these historic messages should appear in your detail
 * window similar to the send window.
 * <br/>
 * <br/>
 * <b>1. Analysis</b>
 * <br/>
 * <br/>
 * To realize this use case a window will appear as an interface with the user
 * that will have to input the destination email, the email subject, the message
 * and, if the user desires, the content of an area of the sheet.
 * <br/>
 * <br/>
 * <img src="doc-files/analysisDiagram_Core05_02.png"/>
 * <br/>
 * <br/>
 * <b>2. Design</b>
 * <br/>
 * <br/>
 * Design Diagram
 * <br/>
 * <br/>
 * <img src="doc-files/designDiagram_Core05_02.png"/>
 * <br/>
 * <br/>
 * Class Diagram
 * <br/>
 * <br/>
 * <img src="doc-files/classDiagram.png"/>
 * <br/>
 * <br/>
 * <b>3. Tests</b>
 * <br/>
 * <br/>
 * To test this use case first the user has to choose the Send Email extension.
 * A new window will open and the user has to fill the parameters (To, Subject
 * and Message) necessary to send an email. When that is done he will press the
 * send button.
 * <br/>
 * <br/>
 * <br/>
 * <br/>
 *
 * @author João Cabral
 */
package csheets.userstories.core05_02.sendmail.i130728;

/**
 *
 * @author João Cabral
 */
class _Dummy_ {

}
