/**
 *
 * <b>Requirement</b><br/>
 * It should be possible to create various forms . The forms must stay persistent. When you run a form their fields (edit box
 * and static text box) must be automatically associated with variables that already exist or variables must be created (if
 * it do not exist ) . The values ​​of variables must appear on the form automatically when it It is executed. For such forms
 * of the field must have a name that will be used as the name of respective variable . So if a macro or formula if you have
 * assigned a value to a variable and then if you display a form with a field with the same variable name should appear the
 * value the variable in the form field .
 * <br/> <br/>
 *
 * <b>Analysis</b><br/>
 *
 * In this use case i have to allow the user to create several forms and those forms should persistent. The form would create
 * variables and store the value. When the form is presented the programm should replace the name of the variable with the
 * value of it.
 * <br/>
 * <img src="doc-files/macros07_02_analysis.png"/>
 * <br/>
 *
 * <b>Design</b><br/>
 *
 * <img src="doc-files/macros07_02_design.png"/>
 * <img src="doc-files/uc_design_createForm.png"/>
 *
 * <br/>
 * <br/>
 *
 * <b>ClassDiagram</b><br/>
 * <br/>
 * <br/>
 * <br/>
 *
 *
 *
 * <b>Functional Tests</b><br/>
 * <img src="doc-files/teste1.PNG"/>
 * <br/>
 * <br/>
 * <img src="doc-files/teste2.PNG"/>
 * <br/>
 * <br/>
 * <img src="doc-files/teste3.PNG"/>
 * <br/>
 * <br/>
 * <img src="doc-files/teste4.PNG"/>
 * <br/>
 * <br/>
 * <img src="doc-files/teste5.PNG"/>
 * <br/>
 * <br/>
 *
 */
/*
 *
 */
package csheets.userstories.macros07_02.FormulariosEVariaveis.i130487;

/**
 *
 * @author Utilizador
 */
class _Dummy_ {

}
