/**
 * Technical documentation regarding the user story macros09.1: Toolbar Buttons
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Add a window (sidebar) that will allow the user to add buttons (icons) to the toolbar. Each button
 * will be associated to a text that should show when the mouse hovers over it the icon (tooltip).
 * An action should also be associated with the icon clicking. That action can be a macro, a beanshell script or
 * a formula. The toolbar buttons may be active or not. If they are inactive the clicking will have no effect.
 * Functions in macros and beanshell should be added that will allow activating and deactivating toolbar's button.
 *
 * <br/>
 * <br/>
 * <b>Use Case "Toolbar Buttons":</b> 
 * The user presses a button and the system shows a sidebar. 
 * The user selects a button to add to the toolbar. The user selects the button action and . The system adds the button to the toolbar.
 * 
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis </h2>
 * <br/>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how the user interacts
 * with the system in order to add a button to the toolbar.
 * <br/>
 * <br/>
 * <img src="doc-files/macros09_01_SequenceDiagram">
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/macros09_01_ClassDiagram.png">
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how the user interacts with the system
 * in order to add a button to the toolbar.
 *
 * <h2>5. Coding</h2>
 *
 *
 * <h2>6. Tests </h2>
 * 
 * The tests will be exclusively functional. The test are as follows:
 * Does a new window appear when the "Add Buttons" button is clicked?
 * Are the added buttons using the "Add Buttons" button included in a new toolbar?
 * Whenever a button is disabled, does it really stays disabled?
 * ...
 * @author i130576
 */
package csheets.userstories.macros09_01.Botoes_na_Toolbar.i130576;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
