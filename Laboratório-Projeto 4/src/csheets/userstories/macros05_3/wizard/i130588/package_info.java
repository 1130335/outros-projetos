/**
 * Technical documentation regarding the user story macros05_3: Advanced wizard
 * insert formula
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * The wizard window should now contain an optional area that should show a
 * "tree" with the "structure" of the formula's expression (like Abstract Syntax
 * Tree). Since that's an optional area, there should be a button (or checkbox)
 * to activate/deactivate that functionality (in the wizard window itself). The
 * syntax tree must be built based on the result of the compilation of the
 * formula. When as tree's element is clicked the respective text on the edit
 * box should be highlighted.
 * <br/>
 * <br/>
 * <b>Use Case "Advanced wizard insert formula": The user starts the application
 * and chooses the wizard. The user chooses the function and inserts the
 * parameters. The result is shown along with the syntax tree with the structure
 * of the formula's expression.
 * </b>
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * The wizard is an extension, and it is already defined in
 * ExtensionWizard.java. The GUI is already implemented, so the AST area is
 * going to be implemented in the WizardWindow.java class. The button to
 * activate and deactive the AST functionality is also going to be implemented
 * there. The ActionListeners are also going to be implemented there. The most
 * important thing about this use case is to show the AST so we need to study
 * how to create and show and AST in NetBeans. A solution could be using the
 * DOTTreeGenerator ANTLR 3 API.
 *
 * <h2>3. Tests</h2>
 * The functionalities that need to be tested are if the tree is visible when
 * the checkbox is selected, and if the tree is not visible when the checkbox is
 * not selected, and if the tree beeing shown is the tree of the formula's
 * expression. We also need to test if the elements clicked on the tree are the
 * same that highlight in the edit box.
 * <br/>
 *
 * <h2>4. Design</h2>
 * To get the Formula we need, first we need to go to the FormulaCompiler and
 * use the method getInstance().compile(). The method compile() returns Formula,
 * from which we can get its Expression by calling the method getExpression().
 * With the Expression we get from it we can create te Tree we need to show.
 *
 *
 * <h2>5. Coding</h2>
 *
 *
 * <h2>6. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 * @author Daniela Maia
 */
package csheets.userstories.macros05_3.wizard.i130588;

/**
 *
 * @author Daniela Maia
 */
public class package_info {

}
