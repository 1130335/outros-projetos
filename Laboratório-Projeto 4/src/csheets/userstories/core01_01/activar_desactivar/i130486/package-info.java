/**
 * Technical documentation regarding the user story core01_01: Activate and
 * Deactivate Extensions
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Activate and deactivate extensions. Show a window that allows the user to
 * activate or deactivate the cleansheets extensions. It also has to exist a
 * sidebar called "Navigator" that shows the current extensions.
 *
 * <br/>
 * <br/>
 * <b>Use Case "Activate and Deactivate Extensions":</b> The user selects the
 * Extensions +Manager and a window pops up showing two columns. One for the
 * active extensions and another for the deactivated ones. The user can choose
 * what extension he wants to activate or deactivate. The systems
 * activates/deactivates the extension choosed. The system also displays a
 * sidebar in the right side with the activated/deactivated extensions.<br/>
 * <br/>
 *
 * <h2>2. Analysis </h2>
 * <br/>
 * <h2>2. Analysis</h2>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how extensions will be
 * enabled or disabled
 * <br/>
 * <br/>
 * <img src="doc-files/core01_01_SequenceExtensionManagerAnalysis.png">
 *
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how extensions will be
 * showed up in the "Navigator" tab
 *
 *
 * <img src="doc-files/core01_01_SequenceSideBar.png">
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/core01_01_ClassExtensionManager.png">
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how extensions will be enabled or
 * disabled
 *
 * <img src="doc-files/core01_01_SequenceExtensionManagerDesign.png">
 *
 *
 *
 * <b>Sequence Diagram</b> illustrating how extensions will be showed up in the
 * "Navigator" tab
 *
 * <img src="doc-files/core01_01_SequenceSideBarDesign.png">
 *
 * <h2>5. Coding</h2>
 *
 *
 *
 *
 * <h2>6. Tests </h2>
 *
 * Since this use case is mostly based on showing the user which Extensions are
 * enabled and disabled, the test we must do consist in making sure that all
 * extensions enabled or disabled appear in the right place(list).
 *
 * Also, we need to verify that all extensions disabled are not clickable (shows
 * up in grey) and the others must be still able to use.
 *
 *
 *
 * @author i130486
 */
package csheets.userstories.core01_01.activar_desactivar.i130486;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
