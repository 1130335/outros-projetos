/**
 * Technical documentation regarding the user story core01_01 - Activate and
 * Deactivate Extensions
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * <br/>
 *
 * <b>Use Case "Activate and deactivate extensions": </b>Adds a window that
 * allows the user to enable and/ or disable extensions of the cleansheets. Also
 * add a sidebar window "Navigator" that presents all the enabled and disabled
 * extensions.
 * <br/>
 *
 *
 * <h2>2. Analysis</h2>
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how extensions will be
 * enabled or disabled
 * <br/>
 * <br/>
 * <img src="doc-files/core01_01_analysis_sequence_diagram.png">
 *
 *
 * <b>Analysis of the Sequence Diagram</b> illustrating how extensions will be
 * showed up in the "Navigator" tab
 *
 *
 * <img src="doc-files/core01_01_analysis_sequence_diagram2.png">
 *
 * <br/>
 * <b>Class diagram</b> illustrating which classes will interact in this use
 * case
 * <br/>
 * <img src="doc-files/core01_01_class_diagram.png">
 *
 *
 * <h2>4. Design</h2>
 *
 *
 * <b>Sequence Diagram</b> illustrating how extensions will be enabled or
 * disabled
 *
 * <img src="doc-files/core01_01_sequence_diagram.png">
 *
 *
 *
 * <b>Sequence Diagram</b> illustrating how extensions will be showed up in the
 * "Navigator" tab
 *
 * <img src="doc-files/core01_01_sequence_diagram2.png">
 *
 * <h2>5. Coding</h2>
 *
 *
 *
 *
 * <h2>6. Tests </h2>
 *
 * Since this use case is mostly based on showing the user which Extensions are
 * enabled and disabled, the test we must do consist in making sure that all
 * extensions enabled or disabled appear in the right place(list).
 *
 * Also, we need to verify that all extensions disabled are not clickable (shows
 * up in grey) and the others must be still able to use.
 *
 *
 * <br/>
 * <br/>
 *
 * @author Francisco Lopes 1130576
 */
