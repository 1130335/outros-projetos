package csheets.userstories.macros01_01.comments.i130728;

/**
 * Documentação para o user story 30: bloco de instruções
 * <br/>
 * <br/>
 *
 * <b>Requirement</b><br/>
 * Possability to add a block (or a sequence) of instructions. A block should be
 * between '{' '}' and it's instructions between ';'. The instructions of a
 * block are executed in left to right and the result of that block is the
 * result of the last instruction of that block. For example the block "={1+2;
 * sum(A1:A10); B3+4}" should result in the execution of all the instructions
 * from left to right and the result will be the value of "B3+4". Add the
 * operator ":=" for attribution. This operator should put the result of the
 * expression of the right to the left of the operator. For now the left of the
 * operator should be able to be the name of a Cell. Implement the repetition
 * cicle FOR using the blocks. For example the block "=FOR{ a1:=1; a1<10;
 * a2:=a2+a1; a1:=a1+1 }" executes a repetition cicle FOR in which: the first
 * expression is the initialization; the second expression is the limit
 * condition; all the other expressions are executed for each iteration of the
 * cicle. <br/> <br/>
 *
 * <b>Analysis</b><br/>
 * Analise the possibility of writing a block(or sequence of characteres) that
 * contain instructions. This function will receive as a paramter several
 * Strings with a ";" spliting them. This information has to processed with the
 * purpose of executing each instruction separately and sequentially. The result
 * will be the return of the last instruction. As another requirement we will
 * have to implement an instruction(loop "FOR") in order to allow the user to
 * introduce cicles that we are able to process.
 * <br/>
 * <img src="doc-files/analise_Diagram_Macros01.png"/>
 * <br/>
 *
 * <br/>
 * <br/>
 * <b>Design</b><br/>
 *
 * <img src="doc-files/Design_Diagram_Macros01.png"/>
 * <br/>
 * <br/>
 *
 * <b>ClassDiagram</b><br/>
 * <img src="doc-files/ClassDiagram_macros01.png"/>
 * <br/>
 * <br/>
 * <br/>
 *
 *
 * <b>Coding</b><br/>
 * see:<br/>
 * <br/>
 * <br/>
 * <br/>
 *
 *
 * <b>Functional Tests</b><br/>
 * To test our user Story we follow the following steps:<br/>
 * 1- run the program;<br/>
 * 2- choose a cell and edit the value introducing an expression;<br/>
 * 4 - ex: {5+3} 8.
 * <br/>
 * <br/>
 * <img src="doc-files/testesFuncionias_Macros01_01.jpg"/>
 *
 */
/*
 *
 */
public class package_info {

}
