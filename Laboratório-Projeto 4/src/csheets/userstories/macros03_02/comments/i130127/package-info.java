/**
 * Technical documentation regarding the user story macros03_02
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * This use Case was based on two functions, the MMULT (with the functionality
 * to multiply matrix) and the function MINVERSE (with the functionality to
 * invert matrix), the user can choose what function he will use.
 * <br/>
 * <br/>
 * <b>Use Case "MACROS_03.2":</b>
 * <br/>
 * <br/>
 * The user select a cell to test the functions, and if the parameters are ok,
 * he will return the expected output.
 *
 * <h2>2. Analysis</h2>
 * To this Use case i created two classes, the MMULT and the MINVERSE, and done
 * the implementation to calculate the expected output. After that, i configure
 * both functions in the language.props , to recognize the created functions.
 * <br/>
 *
 *
 * <h2>3. Tests</h2>
 *
 *
 * <h2>4. Design</h2>
 *
 * <b>MINVERSE SEQUENCE DIAGRAM:</b><br/>
 * <img src="doc-files/FunctionMINVERSESequenceDiagram.png"/>
 * <br/>
 * <br/>
 *
 * * <b>MMULT SEQUENCE DIAGRAM:</b><br/>
 * <img src="doc-files/FunctionMMULTSequenceDiagram.png"/>
 * <br/>
 * <br/>
 *
 * <h2>5. Coding</h2>
 *
 * <br/>
 * One of the principal implemented methods:
 *
 * <b>MMULT CLASS: </b>
 * <br/>
 * <br/>
 *
 * public Value multiply(Value[][] Matrix1, Value[][] Matrix2) {
 * <br/>
 * <br/>
 *
 * if (Matrix1[0].length == Matrix2.length) {
 * <br/>
 * <br/>
 * Value[][] newMatrix = new
 * <br/>
 * <br/>
 * Value[Matrix1.length][Matrix2[0].length]; double sum = 0;
 * <br/>
 * <br/>
 *
 * for (int i = 0; i < Matrix1.length; i++) { <br/> <br/>
 * for (int j = 0; j < Matrix2[0].length; j++) { <br/> <br/>
 * for (int k = 0; k < Matrix2.length; k++) { <br/> <br/>
 * try {
 * <br/>
 * <br/>
 * sum += Matrix1[i][k].toDouble() * Matrix2[k][j]. toDouble(); }
 * <br/>
 * <br/>
 * catch (IllegalValueTypeException ex) {
 * <br/>
 * <br/>
 * Logger.getLogger(MMULT.class.getName()). log(Level.SEVERE, null, ex); } }
 * <br/>
 * <br/>
 * newMatrix[i][j] = new Value(sum); sum = 0; } }
 * <br/>
 * <br/>
 * return new Value(newMatrix); } else {
 * <br/>
 * <br/>
 * JOptionPane.showMessageDialog(null, "Can't multiply those matrixes"); }
 * <br/>
 * <br/>
 * return new Value(""); } <br/>
 *
 * <h2>6. Final Remarks</h2>
 * The use Case is 90% done. I had created two new classes: MMULT and MINVERSE,
 * which functionalitys i had already explicated.
 *
 * <br/>
 */
package csheets.userstories.macros03_02.comments.i130127;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author Miguel
 */
class _Dummy_ {
}
