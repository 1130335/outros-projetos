/**
 * Technical documentation regarding the user story macro02_01: edit create variable
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Add the concept variable. The variables must have a name
 * that starts whit "@" symbol. When the variable is referred to in a
 * formula it must be created if it does not exist.in case the variable
 * has been created, you should use "that" variable. The variables must
 * be persistent. The variables must exist in context of workbooks.
 * The variables have the type of the value assigned to them.
 * <br/>
 * <br/>
 * <b>
 * <br/>
 * <br/>
 * 
 * <h2>2. Analysis</h2>
 *The Analysis is about create a variable and the use case should have
 * possibility to create a variable and save the value ,that a value
 * can be a number or a formula ,so the use case also should have
 * possibility to calculate a value from a formula,and at last show
 * the name and value of the variable. * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>

 * <h2>User selects create variable</h2>
 *The follow sequence diagram is about create a variable, the use starts
 * the application, write a name and a formula (in case the user want to
 * insert the value),system receive the items and check if there is no
 * variable created with that name and will create the variable(in case 
 * variable has been created the system just will change the value).  
 * <img src="doc-files/CreateVariableSequenceDiagram.png"> 
 * <br/>
 * 
 
 * 

 * <h3>Class Diagram</h3>
 
 * <img src="doc-files/ClassDiagramCreateVariable.png"> 
 * <br/>
 * <h2>3. Tests</h2>
 * <br> 
 * <br/>
 
 * 
 */
package csheets.userstories.macros02_01.comments.i130777;

/**
 *
 * @author Aria
 */
 class _Dummy_ {
    
}
