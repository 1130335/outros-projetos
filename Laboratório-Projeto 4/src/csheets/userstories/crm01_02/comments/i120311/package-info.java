/**
 * Technical documentation regarding the user story crm01_02: company contact
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * This user story clarify that there are two types of contacts, company and person. This two types are going to be classes that
 * will be on JPA persistence, so is important create different repositories for different identities. 
 * The contact person must include attributes like profession and the company which is related.
 * The contact company doesn't have the first and last name but has one designation. 
 * Create a configuration file to predefine the list of professions.
 * In contacts that are companies must appear a list of all related people.
 * 
 *
 * <br/>
 * <br/>
 * <b>Use Case "company Contact:</b>
 * The user selects the ContactsMenu in the
 * Extension/Contacts Menu. The system displays the options available in a new
 * window and then the user selects if want work at companies contacts or people contacts.
 * Then there are the option he wants to this type of contact: Create, Edit or Remove.
 * For these three options the system will display a different menu and ask the
 * user to fill data/edit data or simply select the contact and remove it.
 *
 *
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 *This use case is responsible for interaction that two kinds of contacts need to have.
 *With two different contacts I must be aware of the type of dependence between them. 
 *If a contact person doesn't have a Company can be eliminated from the database because a company can have anyone related.
 *However if a contact person related with some company it limit the possibility of this company be eliminated,
* being necessary first change the relationship between the two types to eventually eliminate the company .
* This dependence forces also not be possible to make updates of a contact just replacing old with new . 
* The merge is necessary to be best implemented code , thus correcting previous issues of possible errors .
This relationship between them is the main function of this use case, making use of the configuration file just to 
* improve the program in the direction of future changes.
 * <br/>
 *
 * <h3>3.First "analysis" sequence diagram and design</h3>
 *
 * <h2>User selects create contact(Company)</h2>
 * The follow sequence diagram is about create a company, the use starts the
 * application, choose extensions, choose type of company, create contact and fills the information.
 * <img src="doc-files/CreateCompanySequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects edit contact(Company)</h2>
 * The second sequence diagram is about edit a contact, the user
 * choose a contact and he can edit designation and his image.
 * <img src="doc-files/EditCompanySequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects remove contact(Company)</h2>
 * The follow sequence diagram about remove, the user choose a contact from the
 * list and remove it.
 * <img src="doc-files/RemoveCompanySequenceDiagram.png">
 * <br/>
  * <h2>User selects create contact(Person)</h2>
 * The follow sequence diagram is about create a person, the use starts the
 * application, choose extensions, choose type of person, create contact and fills the information.
 * <img src="doc-files/CreatePersonSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects edit contact(Person)</h2>
 * The second sequence diagram is about edit a contact, the user
 * choose a contact and he can edit his information.
 * <img src="doc-files/EditPersonSequenceDiagram.png">
 * <br/>
 *
 * <h2>User selects remove contact(Person)</h2>
 * The follow sequence diagram about remove, the user choose a contact from the
 * list and remove it.
 * <img src="doc-files/RemovePersonSequenceDiagram.png">
 * <br/>
 * <h3>Analysis of CRM Technical Problem</h3>
 * The only problem could be the similarity between UI classes however they are very very different in terms of content.
 *
 * <h2>3. Tests</h2>
 * <h3>Functional Tests Company</h3><br/>
 * <br/>
 * Create Company Menu
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_createCompanyMenu.jpg"/>
 * <br/>
 * Fill the information
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_createCompany.jpg"/>
 * <br/>
 * Edit Company with previous added person
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_editCompany1.jpg"/>
 * <br/>
 * Remove Company
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_removeCompany.jpg"/>
 * <br/>
 * <br/>
 * 
 * <b>Functional Tests Person</b><br/>
 * <br/>
 * Create Person Menu
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_createPersonMenu.jpg"/>
 * <br/>
 * Create person
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_createPerson1.jpg"/>
 * <br/>
 * Create person menu without company
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_createPerson2.jpg"/>
 * <br/>
 * Edit Person
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_editPerson1.jpg"/>
 * <br/>
 * Edit person, fill the information
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_editPerson2.jpg"/>
 * <br/>
 * Remove Person
 * <br/>
 * <img src="doc-files/testesFuncionias_crm01_02_removePerson.jpg"/>
 * <br/>
 * <br/>
 * <br/>
 * <br/>
 * <h2>4. Coding</h2>
 * <br/>
 * <h2>6. Final Remarks</h2>
 * This Use Case does not have any extra's implemented.
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.crm01_02.comments.i120311;

/**
 *
 * @author Daniel Oliveira
 */
class _Dummy_ {
}
