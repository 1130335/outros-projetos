/**
 * Technical documentation regarding the user story ipc01_01: start sharing
 * cells.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Allows to establish a connection to another instanceof the cleansheets and
 * send a range of cells to another instance. The received content must be
 * showed in the same location. To make it happen , every instance of the
 * cleansheets must have a port for the connections. Should be able to find
 * another instances of the cleansheets in the same local network. The instances
 * <br/>
 * <br/>
 * <b>Use Case "Start Sharing":</b>The user selects the cell/cell group he wants
 * to share. The user starts the share. The system displays the other instances
 * of cleansheet available in a new window. The user select the cleansheet he
 * wants to share to. The system make the connection and send the selected cells
 * to selected cleansheet. The system send a sucess message to user.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since comments on cells will be supported in a new extension to cleansheets
 * we need to study how extensions are loaded by cleansheets and how they
 * work.<br/>
 * The first sequence diagram in the section
 * <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application
 * Startup</a> tells us that extensions must be subclasses of the Extension
 * abstract class and need to be registered in special files.<br/>
 * The Extension class has a method called getUIExtension that should be
 * implemented and return an instance of a class that is a subclass of
 * UIExtension.<br/>
 * In this subclass of UIExtension there is a method (getSideBar) that returns
 * the sidebar for the extension. A sidebar is a JPanel.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="./doc_files/uc_realization1.png">
 * <br/>
 * <br/>
 * From the previous diagram we see that we need to add a new "attribute" to a
 * cell: "comment".<br/>
 * Therefore, at this point, we need to study how to add this new attribute to
 * the class/interface "cell". This is the core technical problem regarding this
 * issue.<br/>
 * <h3>Analysis of Core Technical Problem</h3>
 * We can see a class diagram of the domain model of the application
 * <a href="../../../../overview-summary.html#modelo_de_dominio">here</a><br/>
 * From the domain model we see that there is a Cell interface. This defines the
 * interface of the cells. We also see that there is a class CellImpl that must
 * implement the Cell interface.<br/>
 * If we open the {@link csheets.core.Cell} code we see that the interface is
 * defined as:
 * <code>public interface Cell extends Comparable &lt;Cell&gt;, Extensible&lt;Cell&gt;, Serializable</code>.
 * Because of the <code>Extensible</code> it seams that a cell can be
 * extended.<br/>
 * If we further investigate the hierarchy of {@link csheets.core.Cell} we see
 * that it has a subclass {@link csheets.ext.CellExtension} which has a subclass
 * {@link csheets.ext.style.StylableCell}.
 * {@link csheets.ext.style.StylableCell} seems to be an example of how to
 * extend cells.<br/>
 * Therefore, we will assume that it is possible to extend cells and start to
 * implement tests for this use case. <br/>
 * <br/>
 * The <a href="http://en.wikipedia.org/wiki/Delegation_pattern">delegation
 * design pattern</a> is used in the cell extension mechanism of cleansheets.
 * The following class diagram depicts the relations between classes in the
 * "Cell" hierarchy.<br/>
 * <img src="doc-files/core02_01_analysis_cell_delegate.png">
 * <br/>
 *
 * One important aspect is how extensions are dynamically created and returned.
 * The <code>Extensible</code> interface has only one method,
 * <code>getExtension</code>. Any class, to be extensible, must return a
 * specific extension by its name. The default (and base) implementation for the
 * <code>Cell</code> interface, the class <code>CellImpl</code>, implements the
 * method in the following manner:<br/>
 * <pre>
 * {@code
 * 	public Cell getExtension(String name) {
 *		// Looks for an existing cell extension
 *		CellExtension extension = extensions.get(name);
 *		if (extension == null) {
 *			// Creates a new cell extension
 *			Extension x = ExtensionManager.getInstance().getExtension(name);
 *			if (x != null) {
 *				extension = x.extend(this);
 *				if (extension != null)
 *					extensions.put(name, extension);
 *			}
 *		}
 *		return extension;
 *	}
 * }
 * </pre> As we can see from the code, if we are requesting a extension that is
 * not already present in the cell, it is applied at the moment and then
 * returned. The extension class (that implements the <code>Extension</code>
 * interface) what will do is to create a new instance of its cell extension
 * class (this will be the <b>delegator</b> in the pattern). The constructor
 * receives the instance of the cell to extend (the <b>delegate</b> in the
 * pattern). For instance, <code>StylableCell</code> (the delegator) will
 * delegate to <code>CellImpl</code> all the method invocations regarding
 * methods of the <code>Cell</code> interface. Obviously, methods specific to
 * <code>StylableCell</code> must be implemented by it.<br/>
 * Therefore, to implement a cell that can have a associated comment we need to
 * implement a class similar to <code>StylableCell</code>.<br/>
 * <h2>3. Design</h2>
 * <h3>Send</h3>
 * <img src = "doc-files/ipc01_01_design_send.png"/>
 *
 * <h3>Receive</h3>
 * <img src = "doc-files/ipc01_01_design_receive.png"/>
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 */
package csheets.userstories.ipc01_01.start_sharing.i130313;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
