/**
 * Technical documentation regarding the user story ipc01_1: initiate sharing.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * This use case should allow the usert connect to another instance of
 * cleansheets and send a range of cells for the other instance. The received
 * content should be presented in the same "local". In each instance of the
 * cleansheets must be possible to define a port for connections. It should be
 * possible to find other cleansheets instances on the same LAN. Instances
 * should appear in a window and should be used to make the connection. At this
 * stage it is only necessary to transmit the contents of the cells.
 *
 * <br/>
 * <br/>
 * <b>Use Case "Initiate Sharing":</b> The user starts with the selection of the
 * cells that he wishes to send to the other cleansheets and then clicks on a
 * new item in the menu Extension called "IPC". Inside this new item theres
 * another one that allows the user to select within the buttons presented, but
 * right now only the sharing of cells is available. In the next menu presented,
 * the user chooses if he wants to receive or send cells.
 * <br/>
 * <br/>
 * This only allows to send a range of cells at a time.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * This use case allows the user to share cells with other instances of the
 * cleansheets as an extension in the application.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagrams an "analysis" use case
 * realization because their functions are like a draft that we can do during
 * analysis or early design in order to get a previous approach to the design.
 * For that reason we mark the elements of the diagrams with the stereotype
 * "analysis" that states that the element is not a design element and,
 * therefore, does not exists as such in the code of the application (at least
 * at the moment that this diagrams were created).<br/>
 * <br/>
 * <h4>Sender</h4>
 * <img src="doc-files/analysis_diagram_sender.png"/>
 * <br/>
 * <br/>
 * <h4>Receiver</h4>
 * <img src="doc-files/analysis_diagram_receiver.png"/>
 * <br/>
 * <br/>
 *
 * After this first analysis, we know how the classes will interact with each
 * other and what is necessary for the sharing. Now we will study how to
 * implement this in the application.
 * <h3>Analysis of Core Technical Problem</h3>
 * After a closer look at the use case, we quickly realized that this feature
 * was actually an extension and would not be "integrated" in the application
 * itself. Then we change our way of thinking in the solution and had to
 * rediscover the application in terms of extensions. We decided to use part of
 * our previous thinking and just make it so that we can identify our use case
 * as an extension, requiring more connections between application classes.
 *
 * <h2>3. Design</h2>
 * <h3>Send</h3>
 * <img src = "doc-files/ipc01_01_design_send.png"/>
 *
 * <h3>Receive</h3>
 * <img src = "doc-files/ipc01_01_design_receive.png"/>
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 * @author alexandrebraganca
 */
package csheets.userstories.ipc01_01.start_sharing.i130335;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
