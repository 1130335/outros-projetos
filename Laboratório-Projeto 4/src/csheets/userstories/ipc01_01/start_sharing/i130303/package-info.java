/**
 * Technical documentation regarding the user story ipc01_01: initiate sharing.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Allow to connect to another instance of cleansheets and send a range of cells
 * for the other instance. The received content should be presented in the same
 * "local". For this, in each instance of cleansheets must be possible to define
 * a port for connections. It should be possible to find other cleansheets
 * instances on the same LAN. Instances should appear in a window and used to
 * make the connection. At this stage it is only necessary to transmit the
 * contents of the cells.
 * <br/>
 * <br/>
 * <b>Use Case "Initiate sharing":</b>The user selects a range of cells and then
 * the option to initate sharing. The user has the option to choose if he wants
 * to receive or to send information. If the "receive" option is selected a
 * "waiting" message appears until it receives information. Once that happens
 * the information received is pasted to the current worksheet in the right
 * location. If the "send" option is selected a list of the available instances
 * of cleansheets in the local network is displayed and the user selects the
 * instance he wants to send to.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * My analysis divides this user case into two separated ones: receive cell data
 * and send cell data. If the user wants to receive information, two new threads
 * will be created: one to "run" a receive-reply type udp connection; and one to
 * run a receive tcp type connection. The first one is responsible to reply to
 * udp requests and the second one is responsable for receiving cell's data. If
 * the user wants to send information, one thread will be created: this one will
 * be "running" a send-receive type udp connection (in broadcast mode) to find
 * other instances in the same local network. Once at least one instance reply
 * to the udp request the user select one of the found instances and a new
 * connection is made: a send tcp type connection to send the data to the other
 * instance. To acomplish this two classes will be used: Connection.UDP and
 * Connection.TCP. Both will implement a java interface allowing future changes
 * or new protocols in the future.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="doc-files/ipc01_01_send_analysis.png">
 * <img src="doc-files/ipc01_01_receive_analysis.png">
 * <br/>
 * <br/>

 * <h3>Analysis of Core Technical Problem</h3>
 * The goal of this UC is to send a range of cell's (content and address) between two instances
 * of the application in the same local network. <br/>
 * As we can see in the sequence diagram (see in the bottom) we used the FocusOwnerAction {@link csheets.ui.ctrl.FocusOwnerAction} 
 * abstract class as super class for our InitiateSharingAction {@link csheets.ext.ipc.ui.InitiateSharingAction}. This away we have
 * access to the current range of selected cells. With this interpretation the user must select the cells before initiate the UC. <br/>
 * <br/>
 * The other part of this UC are the network connections required to find other intances in the same local network and to send information between 2 instances.<br/>
 * To accomplish this we created a java interface NetworkConnection {@link csheets.ext.ipc.protocols.NetworkConnection}. This allow to future implementations
 * of a type of network connection. In particulary in this UC we needed a UDP connection and TCP connection. The first is required to find other instances in 
 * local network and the second is required to send information between two instances. This two connections are represented by abstract classes:
 * UDP connection {@link csheets.ext.ipc.protocols.UDP_Connection} and TCP connection {@link csheets.ext.ipc.protocols.TCP_Connection}.<br/>
 * <br/>
 * As it is recommended any network connection must run in a different thread to the main thread of the program doesn't lock. To accomplish this any type of connection
 * implements Runnable. <br/>
 * Now when the use of any type of connection is needed a implementation needs to be done. In this UC we created 4 implementation classes: a server side and a client side for each
 * connection type (TCP and UDP). Each implementation class implements the send and read data methods and the run method from the Runnable interface as well. <br/>
 * So the type connection class is responsable to init and to end all the necessary components and the implementation class is responsable to send and receive data
 * and implement the run method to instruct the new thread how to execute the code.<br/>
 * <br/>
 * In the server side (the instance that will received the data) both UDP server and TCP server are started. This allow to this instance to be found and
 * to be ready to receive the data at the same time.<br/>
 * As for the client side (the instance that will send the data) first the UDP client is started. Then, the list of the found instances is presented and 
 * the user chooses the instance that he wants to send to. Then the TCP client is started.<br/>
 * 
 * We also created 2 domain classes: NetworkInstace {@link csheets.ext.ipc.domain.NetworkInstance} and SimplerCell {@link csheets.ext.ipc.domain.SimplerCell} <br/>
 * The first one represents a instance as a network instance. It has a ip address and a port. We use this class when an instance is found by the UDP client. 
 * A new object is created and added to a list. Later this list is showned to the user and the select object is used to start the TCP client<br/>
 * <br/>
 * The second one represents a cell with only content and a address. We use this class when the selected cells are sent and received using a TCP connection. 
 * We could have used the existing Cell or CellImpl but some attributes from the CellImpl object can't be send/received because they can't be the same 
 * in different instances. So SimplerCells fixes that problem by using only the required attributes. This is accomplish by a construtor that receives a
 * Cell object and extracts the address and the content to the SimplerCell object.<br/>
 * 
 * <br/>
 * <br/>
 * <h2>3. Design</h2>
 * <h3>Send</h3>
 * <img src = "doc-files/ipc01_01_design_send.png"/>
 *
 * <h3>Receive</h3>
 * <img src = "doc-files/ipc01_01_design_receive.png"/>
 *
 * <br/>
 * <br/>
 * <br/>
 *
 */
package csheets.userstories.ipc01_01.start_sharing.i130303;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
