/**
 * Technical documentation regarding the user story ipc01_01: start sharing
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * Allows to establish connection with other instance of cleansheets and send a
 * range of cells to other instance. The received content should be shown in the
 * same "spot". To do so, it must be possible to define a port for the
 * connections for each instance of cleansheets. It should be possible to find
 * other instances of cleansheets in the same local network. The instances
 * should appear in a window and be used to make a connection. In this fase it
 * is only necessary to transmit the content of the cells.
 * <br/>
 * <br/>
 * <b>Use Case "Start Sharing":</b> The user selects the cell/cells he/she wants
 * to share and selects the option connections > send in the menu bar. The
 * system displays the search results for other instances of cleansheets in the
 * local network which are waiting to receive cells. The user selecets the
 * instance/instances of cleansheets he/she wants to share the cell/cells with.
 * The system connects with the other instance of cleansheets and shows the
 * cell/cells to the other instance in the same spot. After the other instance
 * of cleansheets receives the content of the selected cell/cells, the user is
 * notified that the sending was successful.<br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * Since sharing other objects will be supported in a new extension to
 * cleansheets we need to study how extensions are loaded by cleansheets and how
 * they work.<br/>
 * The first sequence diagram in the section
 * <br/>
 * <img src="doc-files/comments_extension_uc_realization1.png">
 * <br/> tells us that the way to get the selected cells is using the method
 * getSelectedCells in the SpreadsheetTable class.<br/>
 * The SendCtrl class has a method called sendCells that connects to a socket.
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).<br/>
 * <br/>
 * <img src="doc-files/comments_extension_uc_realization1.png">
 * <br/>
 * <br/>
 * From the previous diagram we see that we need to make a connection in the
 * sendCells method of the class SendCtrl.<br/>
 * Therefore, at this point, we need to study how to make a connection between
 * two instances of cleansheets.<br/>
 * <h3>Analysis of Core Technical Problem</h3>
 * -----------------------------------------------------------------------------
 * -----------------------------------------------------------------------------
 * <h2>4. Design</h2>
 * To realize this user story we will need to create a class that extends
 * FocusOwnerAction so we have an instance of SpreadsheetTable on which we can
 * call the getSelectedCells function. We will also need to add a new JMenu to
 * the MenuBar for the connections, and add the send and receive options to the
 * connection JMenu.
 *
 * <h3>Send</h3>
 * <img src = "doc-files/ipc01_01_design_send.png"/>
 *
 * <h3>Receive</h3>
 * <img src = "doc-files/ipc01_01_design_receive.png"/>
 * <h2>4. Coding</h2>
 * see:<br/>
 *
 * <br/>
 * <h2>5. Final Remarks</h2>
 *
 * <br/>
 * <br/>
 *
 * @author Daniela Maia
 */
package csheets.userstories.ipc01_01.start_sharing.i130588;

/**
 *
 * @author Daniela Maia
 */
public class package_info {

}
