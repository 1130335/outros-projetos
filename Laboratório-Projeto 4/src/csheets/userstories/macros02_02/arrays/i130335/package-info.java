/**
 * Technical documentation regarding the user story macros02_02: arrays.
 * <br/>
 * <br/>
 *
 * <h2>1. Requirement</h2>
 * It should be added support for the array variables. Any variable can be an
 * array. When accessing the variable only by name are accessing the contents of
 * the first position of the array. To access a position of the array the user
 * must explicitly use it's index in straight brackets. For example, the formula
 * "= @ abc [2]: = 123" will place the value 123 in the secong position of the
 * 'abc' array. Each position of the array can have a different data type. For
 * example, we have in the same array numeric and alphanumeric values.
 * <br/>
 * <br/>
 * <b>Use Case "Macros 02.02 - Arrays":</b> The user still can write variables
 * into cells, for example "@a = 1". In this use case that expression will
 * actually stand for "@a[0] = 1", which is a default when the user doesn't
 * specify the position of the array of variables that he wishes to save the
 * value. If the user does specify the position, for example "@a[3] = 4", the
 * value should be save in the array at that position. This use case should also
 * allow the user to save in the array both numeric and alphanumeric values.
 * <br/>
 * <br/>
 *
 * <h2>2. Analysis</h2>
 * The first thing that's needed to do is modify the Variable class, so it will
 * be able to support an array of values, which will need modifications too
 * because they will be either numeric or alphanumeric. The array used should
 * have both types of values. My first thought was to use a template to cast the
 * type of the arraylist that would be created for this use case in particular
 * and the value inserted by the user, turning the Variable class generic. But I
 * think will be better for the class Variable should stay the way it is
 * implemented right now, changing upgrading the value to an abstract class,
 * having as it's subclasses "Numeric" and "Alphanumeric". For example, if the
 * user creates a variable this way, "@a[1] = 1", the type of the value will be
 * numeric. For the same expression, if the user creates the variable this way,
 * "@a[1] = '1'", the type of value, even if is a number, will be alphanumeric.
 * There will be a need to create a method that will search within the variable
 * name for the position inside the straight brackets.
 * <br/>
 * <b>Update 1: </b>After some tests, the conclusion reached was that it was
 * impossible to use an arraylist for this use case. It was changed to an
 * Hashmap that saves the value of the variable in the position given by the
 * user.
 * <br/>
 * <b>Update 2: </b> As the implementation was taking place, I noticed that
 * already existed a class called Value that supports the types of data I needed
 * to insert inside the variable. So, I deleted the classes created before as
 * they were no longer necessary.
 *
 * <br/>
 * <br/>
 * <h3>First "analysis" sequence diagram</h3>
 * This following diagram will represent the analysis for the creation of a
 * numeric type value in a random position of a variable. For the alphanumeric,
 * the process should be the same and the methods used in the diagram may not be
 * coded in the final implementation of the use case at all.
 * <br/>
 * <br/>
 * <img src = "doc-files/analysis_DS.png"/>
 * <br/>
 * <br/>
 * From the previous diagram, we conclude that the Variable shall have an
 * hashmap of values, instead of a single value and that this new hashmap shall
 * be able to contain two types of values, but the main "type" shall be the
 * superclass.
 *
 * <h2>3. Tests</h2>
 * The tests developed for this use case were for the classes AlphanumericValue,
 * NumericValue and Variable. <br/>
 * see:<br/>
 * <code>csheets.core.variable.VariableTest</code><br/>
 *
 * <h2>4. Design</h2>
 * The following sequence diagram represents how the code is strutured fo this
 * use case, which is very similar to the use case that preceeds this one.<br/>
 * <img src = "doc-files/design_sdiagram.png"/>
 * <h2>5. Coding</h2>
 * see the package:<br/>
 * <code>csheets.core.variable</code>
 * <br/>
 * <code>csheets.core.CellImpl</code>
 * <br/>
 * <br/>
 *
 * @author alexandrebraganca
 */
package csheets.userstories.macros02_02.arrays.i130335;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
