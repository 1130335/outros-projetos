/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author RICARDLEITE
 */
public class VersionsFileInput {

	public VersionsFileInput() {
	}

	/**
	 * Search for every file named extension name +.txt and for each line,
	 * creates a version to be load for that extension
	 *
	 * @param extensionName
	 * @return
	 */
	public ArrayList<String> inputFile(String extensionName) {

		String fileName = extensionName + ".txt";

		ArrayList<String> fileContent = new ArrayList();

		BufferedReader buffer;
		try {
			buffer = new BufferedReader(new FileReader(fileName));

			String l;

			while ((l = buffer.readLine()) != null) {

				fileContent.add(l);
			}
		} catch (FileNotFoundException ex) {

			return fileContent;
		} catch (IOException ex) {

		}

		return fileContent;
	}
}
