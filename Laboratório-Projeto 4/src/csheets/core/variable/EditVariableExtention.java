/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import csheets.core.variable.ui.UIExtensionEditVariables;
import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

public class EditVariableExtention extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Edit Variables";

    public EditVariableExtention() {
        super(NAME);
    }

    /**
     * Returns the user interface extension of this extension (an instance of
     * the class {@link  csheets.ext.ipc.ui.UIExtensionGames}). <br/>
     *
     * @param uiController
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        if (this.uiExtension == null) {
            this.uiExtension = new UIExtensionEditVariables(this, uiController);
        }
        return this.uiExtension;
    }

}
