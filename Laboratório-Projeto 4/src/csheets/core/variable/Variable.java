/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import csheets.core.Value;
import java.util.HashMap;

/**
 *
 * @author Aria
 */
public class Variable {

	private String name;
	private HashMap<Integer, Value> value;
	private int currentOccupiedPosition;

	public Variable(String name, String value, int position) {
		this.name = name;
		this.value = new HashMap<>();
		if (Character.toString(value.charAt(0)).matches("'")) {
			this.value.put(position, new Value(value.
						   substring(1, value.length() - 1)));
		} else {
			this.value.
				put(position, new Value(Integer.parseInt(value)));
		}
		currentOccupiedPosition = position;

	}

	public Variable(String name) {
		this.name = name;
		this.value = new HashMap<>();
		this.value.put(0, new Value(0));
		currentOccupiedPosition = 0;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public int getCurrentPosition() {
		return this.currentOccupiedPosition;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue(int position) {
		return this.value.get(position).toString();
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value, int position) {
		if (Character.toString(value.charAt(0)).matches("'")) {
			this.value.put(position, new Value(value.
						   substring(1, value.length() - 1)));
		} else {
			this.value.
				put(position, new Value(Integer.parseInt(value)));
		}
	}
}
