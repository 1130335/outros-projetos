/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aria
 */
public class VariableList {

	public static List<Variable> VariableList = new ArrayList();

	public static Variable newVariable(String name, String value, int position) {
		int valid = 0;
		Variable va = null;
		for (int i = 0; i < VariableList.size(); i++) {
			if (name == VariableList.get(i).getName()) {
				setValue(VariableList.get(i), value, position);
				valid++;
				va = VariableList.get(i);
			}
		}
		if (valid == 0) {

			Variable v = new Variable(name, value, position);
			VariableList.add(v);
			va = v;

		}
		return va;
	}

	public static Variable newVariable(String name) {
		int valid = 0;
		Variable variable = null;
		for (int i = 0; i < VariableList.size(); i++) {
			if (VariableList.get(i).getName() == name) {
				valid = valid + 1;
				variable = VariableList.get(i);

			}
		}
		if (valid == 0) {
			Variable v = new Variable(name);
			VariableList.add(v);

			variable = v;
		}
		return variable;
	}

	public static String getValue(String name, int position) {
		String v = "0";
		int valid = 0;
		for (int i = 0; i < VariableList.size(); i++) {
			if (VariableList.get(i).getName().equals(name)) {
				v = VariableList.get(i).getValue(position);
				valid++;

			}

		}
		if (valid == 0) {
			Variable var = new Variable(name);
			VariableList.add(var);
			v = var.getValue(position);
		}
		return v;

	}

	public static void setValue(Variable v, String value, int position) {
		v.setValue(value, position);
	}
        
        public static Variable getVariable(String name)
        {
            for (Variable VariableList1 : VariableList) {
                if(VariableList1.getName().equals(name))
                    return VariableList1;
            }
            return null;
        }
}
