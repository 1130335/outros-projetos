/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Formula;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author Aria
 */
public class VariableCompiler {

	public static String name;
	public static String formula;
	public static String value;
	public static Cell cell;

	public static Variable compile(Cell cell1, String S) throws FormulaCompilationException, IllegalValueTypeException {
		String[] parts = S.split("=");
		name = parts[0];
		Variable v;
		int position = 0;
		cell = cell1;
		if (parts.length > 1) {
			formula = new String("=" + parts[1]);
			value = compileFormula(formula, cell);
			for (int i = 0; i < name.length(); i++) {
				if (Character.toString(name.charAt(i)).matches("\\[")) {
					position = Integer.parseInt(Character.toString(name.
						charAt(i + 1)));
				}
			}
			v = VariableList.newVariable(name, value, position);

		} else {

			v = VariableList.newVariable(name);
		}

		return v;
	}

	public static String compileFormula(String f, Cell cell) throws FormulaCompilationException, IllegalValueTypeException {
		Formula formula = null;
		String valor;
		String F = compileVariable(f);
		if (f.contains("'")) {
			valor = String.valueOf(f.substring(1, f.length()));
		} else {
			formula = FormulaCompiler.getInstance().compile(cell, F);
			Value v = formula.evaluate();
			valor = String.valueOf(v);
		}
		return valor;

	}

	public static String compileVariable(String S) {
		int cont = 0;
		int position = 0;
		String[] aux = S.split("=");
		List<String> operatorList = new ArrayList<String>();
		List<String> operandList = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(aux[1], "+-*/", true);
		while (st.hasMoreTokens()) {
			String token = st.nextToken();

			if ("+-/*".contains(token)) {
				operatorList.add(token);
			} else {
				operandList.add(token);
			}
		}
		int operation = operandList.size();
		S = Character.toString(S.charAt(0));
		if (operandList.size() != 1) {
			if (operandList.get(0).charAt(0) != '@' || operandList.get(0).
				matches("[0-9]")) {
				List<String> auxList = new ArrayList<>();

				if (operandList.size() > 2) {
					auxList.add(operandList.get(0).substring(0));
					for (int k = 1; k < operandList.size() - 1; k++) {
						auxList.add(operandList.get(k));
					}
					auxList.add(operandList.
						get(operandList.size() - 1).
						substring(0, operandList.
								  get(operandList.size() - 1).length() - 1));
				} else {
					auxList.add(operandList.get(0).substring(1));
					auxList.add(operandList.
						get(operandList.size() - 1).
						substring(0, operandList.
								  get(operandList.size() - 1).length() - 1));
				}
				operandList.clear();
				operandList.addAll(auxList);
			}
			System.out.println(operandList);
			for (int i = 0; i < operandList.size(); i++) {
				position = 0;
				if (operandList.get(i).charAt(0) == '@') {
					for (int j = 0; j < operandList.get(i).length(); j++) {
						if (operandList.get(i).charAt(j) == '[') {
							position = Integer.parseInt(Character.
								toString(operandList.get(i).charAt(j + 1)));
						}
					}
					S = S + VariableList.
						getValue(operandList.get(i), position);
					System.out.println("String: " + S);
					if (!operatorList.isEmpty()) {
						if (operatorList.size() > cont) {
							S = S + operatorList.get(i);
							cont++;
						}
					}
				} else if (operandList.get(i).matches("[0-9]+")) {
					S = S + operandList.get(i);
					if (!operatorList.isEmpty()) {
						if (operatorList.size() > cont) {
							S = S + operatorList.get(i);
							cont++;
						}
					}
				}
			}
		} else {
			if (operandList.get(0).charAt(0) == '@') {
				for (int j = 0; j < operandList.get(0).length(); j++) {
					if (operandList.get(0).charAt(j) == '[') {
						position = Integer.parseInt(Character.
							toString(operandList.get(0).charAt(j + 1)));
					}
				}
				S = S + VariableList.getValue(operandList.get(0), position);
			} else {
				S = S + operandList.get(0);
			}
		}

		System.out.println(
			"My String: " + S);
		return S;
	}
}
