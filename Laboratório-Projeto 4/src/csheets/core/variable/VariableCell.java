/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import csheets.core.Cell;
import csheets.core.graphics.Graphic;
import csheets.core.variable.ui.EditVariableCellListener;
import csheets.ext.CellExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;


public class VariableCell extends CellExtension{

    
    /**
	 * The listeners registered to receive events from the comentable cell
	 */
	private transient List<EditVariableCellListener> listeners
		= new ArrayList<EditVariableCellListener>();
    
    public VariableCell(Cell cell) {
        super(cell, EditVariableExtention.NAME);
    }
    
    
    public void addVariableListener(EditVariableCellListener evcl)
    {
        this.listeners.add(evcl);
    }
    
    public void removeVariableListener(EditVariableCellListener evcl)
    {
        this.listeners.remove(evcl);
    }

    
    
    @Override
    public void createGraphic(String name, SortedSet<Cell> chosenCells) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Graphic getGraphic() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
