/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable.ui;

import csheets.core.variable.VariableCell;
import java.util.EventListener;


public interface EditVariableCellListener extends EventListener{
    
    /**
	 * Invoked when a variable is edited from a cell.
	 * @param vc the cell that was modified
	 */
    public void variableEdited(VariableCell vc);
}
