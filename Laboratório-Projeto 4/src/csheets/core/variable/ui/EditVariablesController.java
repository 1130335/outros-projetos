/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable.ui;

import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.variable.Variable;
import csheets.core.variable.VariableList;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;


public class EditVariablesController {
    
    private UIController uiController;
    private Cell activeCell;
    
    public EditVariablesController(UIController uiController)
    {
        this.uiController=uiController;
    }
    
    public String getName(Cell activeCell)
    {
        this.activeCell=activeCell;
        
        String name = this.activeCell.getContent();
        if(hasVariavel(name))
        {
            int i = name.indexOf("@");
            name = name.substring(i,i+1);
        }
        else
        {
            name="";
        }
        return name;
    }
    
    public String getValue(Cell activeCell)
    {
        this.activeCell=activeCell;
        String value = this.activeCell.getContent();
        if(hasVariavel(value))
        {
            int i = value.indexOf("=");
            value = value.substring(i);
        }
        else
        {
            value="";
        }
        return value;
    }
    
    public String[] getCellsWithVariable()
    {
        List<String> s = new ArrayList<>();
        
        int i = this.uiController.getActiveSpreadsheet().getColumnCount();
        int j = this.uiController.getActiveSpreadsheet().getRowCount();
        
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
                String str = this.uiController.getActiveSpreadsheet().getCell(i, j).getContent();
                if(hasVariavel(str))
                {
                    s.add("Cell ("+i+","+j+")");
                }
            }
        }
        String[] ret = new String[s.size()];
        
        for (int k = 0; k < s.size(); k++) {
            ret[k] = s.get(k);
        }
        
        return ret;
    }

    public void variableEdited(Cell c, String text) {
        this.activeCell=c;
        
        Variable v = VariableList.getVariable(getName(this.activeCell));
        
        VariableList.setValue(v, text, 0);
        
        ((CellImpl)c).setVariable(v);
    }
    
    public boolean hasVariavel(String str)
    {
        CellImpl cell = (CellImpl) this.activeCell;
        return cell.getVariable()!=null;
    }
}
