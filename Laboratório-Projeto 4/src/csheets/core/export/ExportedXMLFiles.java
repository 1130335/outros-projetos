/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.export;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sergio 1130711
 */
public class ExportedXMLFiles {

	/**
	 * List of ExportXML previously used in aplicattion
	 */
	public List<ExportXML> exportedFiles;

	/**
	 * Create a ExportedXMLFiles
	 */
	public ExportedXMLFiles() {
		exportedFiles = new ArrayList<>();
	}

	/**
	 * Add a ExportXMl object on exportedFiles list
	 *
	 * @param exp ExportXML object to add
	 */
	public void add(ExportXML exp) {
		this.exportedFiles.add(exp);
	}

	/**
	 * Check if file passed by parameter was previously used for export
	 * operation
	 *
	 * @param file to check
	 * @return boolean
	 */
	public boolean wasUsed(File file) {
		for (ExportXML exp : exportedFiles) {
			if (exp.getF().equals(file)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the tags used to export data to xml file passed by parameter
	 *
	 * @param file to check
	 * @return exp.getTags or null
	 */
	public String[] getTagsFromUssedFile(File file) {

		for (ExportXML exp : exportedFiles) {
			if (exp.getF().equals(file)) {
				return exp.getTags();
			}
		}
		return null;
	}

}
