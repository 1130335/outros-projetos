/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.export;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ExportXML {

	private File f;
	private String[] tags;
	private Workbook w;
	private UIController controller;

	public ExportXML(File f, String[] tags, Workbook w, UIController controller) {
		this.f = f;
		this.tags = tags;
		this.w = w;
		this.controller = controller;
	}

	public void exportWorkbook() throws IOException {

		FileWriter fw = new FileWriter(this.getF());
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		bw.newLine();

		bw.write("<" + this.getTags()[0] + ">");
		bw.newLine();

		int numSpreadsheets = this.w.getSpreadsheetCount();

		for (int i = 0; i < numSpreadsheets; i++) {

			bw.write("<" + this.getTags()[1] + ">");
			bw.newLine();

			Spreadsheet s = this.w.getSpreadsheet(i);

			bw.write("<spreadsheet_number>");
			bw.write("" + i);
			bw.write("</spreadsheet_number>");
			bw.newLine();

			SpreadsheetTable st = new SpreadsheetTable(s, this.controller);

			st.selectAll();

			Cell[][] list = st.getSelectedCells();

			for (int j = 0; j < list.length; j++) {
				for (int k = 0; k < list[0].length; k++) {
					if (!((list[j][k].getContent()).equalsIgnoreCase(""))) {
						bw.write("<" + this.getTags()[2] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[3] + ">");
						bw.write(list[j][k].getContent());
						bw.write("</" + this.getTags()[3] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[4] + ">");
						bw.write("" + list[j][k].getAddress().getColumn());
						bw.write("</" + this.getTags()[4] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[5] + ">");
						bw.write("" + list[j][k].getAddress().getRow());
						bw.write("</" + this.getTags()[5] + ">");
						bw.newLine();

						bw.write("</" + this.getTags()[2] + ">");
						bw.newLine();
					}

				}
			}

			bw.write("</" + this.getTags()[1] + ">");
			bw.newLine();
		}

		bw.write("</" + this.getTags()[0] + ">");
		bw.newLine();

		bw.close();
	}

	public void exportPages(int[] pages) throws IOException {
		FileWriter fw = new FileWriter(this.getF());
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		bw.newLine();

		bw.write("<" + this.getTags()[0] + ">");
		bw.newLine();

		for (int i = 1; i <= pages[0]; i++) {

			bw.write("<" + this.getTags()[1] + ">");
			bw.newLine();

			Spreadsheet s = this.w.getSpreadsheet(pages[i]);

			bw.write("<spreadsheet_number>");
			bw.write("" + pages[i]);
			bw.write("</spreadsheet_number>");
			bw.newLine();

			SpreadsheetTable st = new SpreadsheetTable(s, this.controller);

			st.selectAll();

			Cell[][] list = st.getSelectedCells();

			for (int j = 0; j < list.length; j++) {
				for (int k = 0; k < list[0].length; k++) {
					if (!((list[j][k].getContent()).equalsIgnoreCase(""))) {
						bw.write("<" + this.getTags()[2] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[3] + ">");
						bw.write(list[j][k].getContent());
						bw.write("</" + this.getTags()[3] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[4] + ">");
						bw.write("" + list[j][k].getAddress().getColumn());
						bw.write("</" + this.getTags()[4] + ">");
						bw.newLine();

						bw.write("<" + this.getTags()[5] + ">");
						bw.write("" + list[j][k].getAddress().getRow());
						bw.write("</" + this.getTags()[5] + ">");
						bw.newLine();

						bw.write("</" + this.getTags()[2] + ">");
						bw.newLine();
					}

				}
			}

			bw.write("</" + this.getTags()[1] + ">");
			bw.newLine();

		}
		bw.write("</" + this.getTags()[0] + ">");
		bw.newLine();
		bw.close();

	}

	public void exportPartOfPage(final SpreadsheetTable focusOwner,
								 int numberPage) throws IOException {
		FileWriter fw = new FileWriter(this.getF());
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		bw.newLine();

		bw.write("<" + this.getTags()[0] + ">");
		bw.newLine();

		bw.write("<" + this.getTags()[1] + ">");
		bw.newLine();

		bw.write("<spreadsheet_number>");
		bw.write("" + numberPage);
		bw.write("</spreadsheet_number>");
		bw.newLine();

		Cell[][] list = focusOwner.getSelectedCells();

		for (int j = 0; j < list.length; j++) {
			for (int k = 0; k < list[0].length; k++) {
				if (!((list[j][k].getContent()).equalsIgnoreCase(""))) {
					bw.write("<" + this.getTags()[2] + ">");
					bw.newLine();

					bw.write("<" + this.getTags()[3] + ">");
					bw.write(list[j][k].getContent());

					bw.write("</" + this.getTags()[3] + ">");
					bw.newLine();

					bw.write("<" + this.getTags()[4] + ">");
					if (list[j][k] != null) {
						bw.write("" + list[j][k].getAddress().getColumn());
					} else {
						bw.write(" ");
					}
					bw.write("</" + this.getTags()[4] + ">");
					bw.newLine();

					bw.write("<" + this.getTags()[5] + ">");
					if (list[j][k] != null) {
						bw.write("" + list[j][k].getAddress().getRow());
					} else {
						bw.write(" ");
					}
					bw.write("</" + this.getTags()[5] + ">");
					bw.newLine();

					bw.write("</" + this.getTags()[2] + ">");
					bw.newLine();
				}

			}
		}

		bw.write("</" + this.getTags()[1] + ">");
		bw.newLine();

		bw.write("</" + this.getTags()[0] + ">");
		bw.newLine();

		bw.close();
	}

	/**
	 * @return the f
	 */
	public File getF() {
		return f;
	}

	/**
	 * @return the tags
	 */
	public String[] getTags() {
		return tags;
	}

}
