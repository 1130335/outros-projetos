/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.export;

import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.File;
import java.io.IOException;

public class ExportXMLController {

	private UIController controller;
	private ExportXML e;

	public ExportXMLController(UIController controller) {
		this.controller = controller;
	}

	public void exportXMLFile(String[] tags, File f) {
		Workbook w = this.controller.getActiveWorkbook();
		this.e = new ExportXML(f, tags, w, this.controller);
		controller.getExportedXMLfiles().add(e);
	}

	public void exportWorkbook() throws IOException {
		this.e.exportWorkbook();
	}

	public void exportPages(int[] pages) throws IOException {
		this.e.exportPages(pages);
	}

	public void exportPartOfPage(final SpreadsheetTable focusOwner,
								 int numberPage) throws IOException {
		this.e.exportPartOfPage(focusOwner, numberPage);
	}

}
