/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.graphics;

import csheets.core.Cell;
import java.io.IOException;
import java.util.SortedSet;

/**
 *
 * @author Rita
 */
public class Graphic {

	private String myName;
	private SortedSet<Cell> chosenCells;

	public Graphic(String name, SortedSet<Cell> chosen) throws IOException {
		this.myName = name;
		this.chosenCells = chosen;
	}

	public SortedSet<Cell> getCells() {
		return this.chosenCells;
	}

	public String getName() {
		return this.myName;
	}

	public String getAddressFirstCell() {
		return this.chosenCells.first().getAddress().toString();
	}

	public String getAddressLastCell() {
		return this.chosenCells.last().getAddress().toString();
	}
}
