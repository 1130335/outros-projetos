/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.graphics;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Rita
 */
public class UIExtensionGraphics extends UIExtension {

	private CellDecorator cellDecorator;

	public UIExtensionGraphics(Extension extension, UIController uiController) {
		super(extension, uiController);
	}

	@Override
	public CellDecorator getCellDecorator() {
		if (cellDecorator == null) {
			cellDecorator = new IconCellDecorator();
		}
		return cellDecorator;
	}
}
