/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.graphics;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ui.ext.CellDecorator;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import javax.swing.JComponent;

/**
 *
 * @author Rita
 */
public class IconCellDecorator extends CellDecorator {

	private static final Font font = new Font("Dialog", Font.PLAIN, 10);

	public IconCellDecorator() {

	}

	@Override
	public void decorate(JComponent component, Graphics g, Cell cell,
						 boolean selected, boolean hasFocus) {
		if (enabled) {
			PicCell imageCell = (PicCell) cell.getExtension(ImageExt.Nome);

			if (imageCell.hasImage()) {
				// Stores current graphics context properties
				Graphics2D g2 = (Graphics2D) g;
				Color oldPaint = g2.getColor();
				Font oldFont = g2.getFont();

				// Prints 'A' using own font, then restores the old font
				g2.setColor(Color.red);
				g2.setFont(font);
				Image img1 = Toolkit.getDefaultToolkit().
					getImage(CleanSheets.class.
						getResource("res/img/border.gif"));
				g2.drawImage(img1, 4, 2, new ImageObserver() {

					@Override
					public boolean imageUpdate(Image img, int infoflags, int x,
											   int y, int width, int height) {
						return true;
					}
				});
				// Restores graphics context properties
				g2.setColor(oldPaint);
				g2.setFont(oldFont);
			}
		}
	}

}
