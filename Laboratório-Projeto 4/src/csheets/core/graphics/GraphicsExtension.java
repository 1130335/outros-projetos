/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.graphics;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Rita
 */
public class GraphicsExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Graphics";

	/**
	 * Creates a new Example extension.
	 */
	public GraphicsExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionGraphics(this, uiController);
	}
}
