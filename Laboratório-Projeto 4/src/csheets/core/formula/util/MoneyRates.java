/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.util;

import java.math.BigDecimal;

/**
 *
 * @author RICARDLEITE
 */
public class MoneyRates {

	private String money;
	private BigDecimal dollar;
	private BigDecimal euro;
	private BigDecimal pound;

	/**
	 * Set all default money rates. All taxes are default to EURO currency.
	 */
	public MoneyRates() {
		this.money = "EURO";
		this.dollar = new BigDecimal("1.1249");
		this.pound = new BigDecimal("0.7341");
		this.euro = new BigDecimal("1");
	}

	/**
	 * Sets in which money currency the user wants to get the result.
	 *
	 * @param money Money currency the user selected
	 */
	public void setMoney(String money) {
		this.money = money;
		refreshMoney();

	}

	/**
	 * Refresh all rates for which currency the user may selected.
	 */
	public void refreshMoney() {
		if (money == "EURO") {
			setEuro(new BigDecimal("1"));
			setDollar(new BigDecimal("1.1249"));
			setPound(new BigDecimal("0.7341"));
		} else if (money == "POUND") {
			setPound(new BigDecimal("1"));
			setDollar(new BigDecimal("1.5299"));
			setEuro(new BigDecimal("1.3610"));
		} else if (money == "DOLLAR") {
			setDollar(new BigDecimal("1"));
			setPound(new BigDecimal("0.6536"));
			setEuro(new BigDecimal("0.8908"));
		}
	}

	public void setDollar(BigDecimal d) {
		this.dollar = d;
	}

	public void setPound(BigDecimal d) {
		this.pound = d;
	}

	public void setEuro(BigDecimal d) {
		this.euro = d;
	}

	public BigDecimal getDollar() {
		return this.dollar;
	}

	public BigDecimal getEuro() {
		return this.euro;
	}

	public BigDecimal getPound() {
		return this.pound;
	}

}
