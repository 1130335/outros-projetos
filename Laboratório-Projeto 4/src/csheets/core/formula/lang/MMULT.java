/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Class that multiplies matrixes
 *
 * @author Miguel
 */
public class MMULT implements Function {

	/**
	 * The only parameter: a numeric term
	 */
	public static final FunctionParameter[] parameters = new FunctionParameter[]{
		new FunctionParameter(Value.Type.NUMERIC, "Term", false,
							  "A matrix to be multiplied")
	};

	/**
	 * Creates a new and empty instance of the MMULT function.
	 */
	public MMULT() {
	}

	/**
	 * Returns function identifier
	 *
	 * @return
	 */
	public String getIdentifier() {
		return "MMULT";
	}

	/**
	 * Functionality of the class
	 *
	 * @param arguments function arguments
	 * @return
	 * @throws IllegalValueTypeException
	 */
	public Value applyTo(Expression[] arguments) throws IllegalValueTypeException {
		Value[][] Matrix1 = null;
		Value[][] Matrix2 = null;
		Value MatrixOutput = null;

		boolean flag0 = false;
		boolean flag1 = false;
		int indice = 0;

		while (indice < arguments.length) {

			if (!flag0 && Value.Type.MATRIX == arguments[indice].evaluate().
				getType()) {
				flag0 = true;
				Matrix1 = arguments[indice].evaluate().toMatrix();

			} else if (!flag1 && Value.Type.MATRIX == arguments[indice].
				evaluate().getType()) {
				flag1 = true;
				Matrix2 = arguments[indice].evaluate().toMatrix();

			}

			indice++;
			if (flag0 && flag1) {
				MatrixOutput = multiply(Matrix1, Matrix2);
				Matrix1 = MatrixOutput.toMatrix();
				flag1 = false;
			}

		}
		if (Matrix2 != null) {
			return new Value(Matrix1);
		} else {
			JOptionPane.showMessageDialog(null, "Can't multiply those matrixes");
		}
		return new Value("");

	}

	/**
	 * Returns parameters
	 *
	 * @return
	 */
	public FunctionParameter[] getParameters() {
		return parameters;
	}

	public boolean isVarArg() {
		return true;
	}

	/**
	 * Method that multiplies matrixes
	 *
	 * @param Matrix1 Matrix1 matrix
	 * @param Matrix2 Matrix2 matrix
	 * @return
	 */
	public Value multiply(Value[][] Matrix1, Value[][] Matrix2) {

		if (Matrix1[0].length == Matrix2.length) {
			Value[][] newMatrix = new Value[Matrix1.length][Matrix2[0].length];
			double sum = 0;

			for (int i = 0; i < Matrix1.length; i++) {
				for (int j = 0; j < Matrix2[0].length; j++) {
					for (int k = 0; k < Matrix2.length; k++) {
						try {
							sum += Matrix1[i][k].toDouble() * Matrix2[k][j].
								toDouble();
						} catch (IllegalValueTypeException ex) {
							Logger.getLogger(MMULT.class.getName()).
								log(Level.SEVERE, null, ex);
						}
					}

					newMatrix[i][j] = new Value(sum);
					sum = 0;
				}
			}

			return new Value(newMatrix);
		} else {
			JOptionPane.showMessageDialog(null, "Can't multiply those matrixes");
		}

		return new Value("");
	}

	@Override
	public String getFunctionDescription() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
