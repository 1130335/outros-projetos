/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.util.ExpressionVisitor;
import csheets.core.variable.Variable;
import csheets.core.variable.VariableList;


public class VariableReference implements Expression{
    
    private String name;
    private Spreadsheet s;
    private Variable v;
    
    
    public VariableReference(Spreadsheet s, String name)
    {
        this.s = s;
        this.name = name;
        
        this.v = VariableList.getVariable(name);
        
        if(this.v==null)
        {
            this.v = VariableList.newVariable(name);
        }
        
    }

    @Override
    public Value evaluate() throws IllegalValueTypeException {
        //Validar o tipo do value
        return new Value(Integer.parseInt(VariableList.getValue(this.name, 0)));
    }

    @Override
    public Object accept(ExpressionVisitor visitor) {
        return visitor.visitVariableReference(this);
    }
    
    public Spreadsheet getSpreadsheet()
    {
        return this.s;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
}
