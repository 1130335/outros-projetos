/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Miguel
 */
public class MINVERSE implements Function {

	/**
	 * The only parameter: a numeric term
	 */
	public static final FunctionParameter[] parameters = new FunctionParameter[]{
		new FunctionParameter(Value.Type.NUMERIC, "Term", false,
							  "A matrix to be inverted")
	};

	/**
	 * Creates a new and empty instance of the MINVERSE function
	 */
	public MINVERSE() {
	}

	/**
	 * Returns function identifier
	 *
	 * @return
	 */
	public String getIdentifier() {
		return "MINVERSE";
	}

	/**
	 * Requirements of implementation
	 *
	 * @param arguments function arguments
	 * @throws csheets.core.IllegalValueTypeException
	 */
	@Override
	public Value applyTo(Expression[] arguments) throws IllegalValueTypeException {
		Value[][] matrix = null;

		if (Value.Type.MATRIX == arguments[0].evaluate().getType() || arguments.length > 1) {
			matrix = arguments[0].evaluate().toMatrix();
			if (matrix.length == matrix[0].length) {
				return invert(matrix);
			} else {
				JOptionPane.
					showMessageDialog(null, "The calculated matrix must have a equal number of rows and columns ");
			}
		} else {
			JOptionPane.
				showMessageDialog(null, "In order to invert, you have to select a matrix");
		}

		return new Value("");
	}

	/**
	 * Method that inverts a matrix
	 *
	 * @param matrix matrix to invert
	 * @return
	 */
	public Value invert(Value[][] matrix) {

		int n = matrix.length;
		double x[][] = new double[n][n];
		double b[][] = new double[n][n];
		int index[] = new int[n];
		for (int i = 0; i < n; ++i) {
			b[i][i] = 1;
		}

		// Transform the matrix into an upper triangle
		Value[][] matrixCopy = new Value[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrixCopy[i][j] = matrix[i][j];
			}
		}
		gaussian(matrixCopy, index);

		// Update the matrix b[i][j]
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					try {
						b[index[j]][k] -= matrixCopy[index[j]][i].toDouble() * b[index[i]][k];
					} catch (IllegalValueTypeException ex) {
						Logger.getLogger(MINVERSE.class.getName()).
							log(Level.SEVERE, null, ex);
					}
				}
			}
		}

		// Perform backward substitutions
		for (int i = 0; i < n; ++i) {
			try {
				x[n - 1][i] = b[index[n - 1]][i] / matrixCopy[index[n - 1]][n - 1].
					toDouble();
			} catch (IllegalValueTypeException ex) {
				Logger.getLogger(MINVERSE.class.getName()).
					log(Level.SEVERE, null, ex);
			}
			for (int j = n - 2; j >= 0; --j) {
				x[j][i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {
					try {
						x[j][i] -= matrixCopy[index[j]][k].toDouble() * x[k][i];
					} catch (IllegalValueTypeException ex) {
						Logger.getLogger(MINVERSE.class.getName()).
							log(Level.SEVERE, null, ex);
					}
				}
				try {
					x[j][i] /= matrixCopy[index[j]][j].toDouble();
				} catch (IllegalValueTypeException ex) {
					Logger.getLogger(MINVERSE.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}
		}
		Value[][] result = new Value[x.length][x[0].length];
		DecimalFormat df = new DecimalFormat("0.00");
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[0].length; j++) {
				result[i][j] = new Value(df.format(x[i][j]));
			}
		}
		return new Value(result);
	}

	/**
	 * Method to carry out the partial-pivoting Gaussian elimination. Here
	 * index[] stores pivoting order.
	 *
	 * @param a
	 * @param index
	 */
	public void gaussian(Value a[][], int index[]) {
		int n = index.length;
		double c[] = new double[n];

		// Initialize the index
		for (int i = 0; i < n; ++i) {
			index[i] = i;
		}

		// Find the rescaling factors, one from each row
		for (int i = 0; i < n; ++i) {
			double c1 = 0;
			for (int j = 0; j < n; ++j) {
				double c0 = 0;
				try {
					c0 = Math.abs(a[i][j].toDouble());
				} catch (IllegalValueTypeException ex) {
					Logger.getLogger(MINVERSE.class.getName()).
						log(Level.SEVERE, null, ex);
				}
				if (c0 > c1) {
					c1 = c0;
				}
			}
			c[i] = c1;
		}

		// Search the pivoting element from each column
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			double pi1 = 0;
			for (int i = j; i < n; ++i) {
				double pi0 = 0;
				try {
					pi0 = Math.abs(a[index[i]][j].toDouble());
				} catch (IllegalValueTypeException ex) {
					Logger.getLogger(MINVERSE.class.getName()).
						log(Level.SEVERE, null, ex);
				}
				pi0 /= c[index[i]];
				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}

			// Interchange rows according to the pivoting order
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;
			for (int i = j + 1; i < n; ++i) {
				double pj = 0;
				try {
					pj = a[index[i]][j].toDouble() / a[index[j]][j].toDouble();
				} catch (IllegalValueTypeException ex) {
					Logger.getLogger(MINVERSE.class.getName()).
						log(Level.SEVERE, null, ex);
				}

				// Record pivoting ratios below the diagonal
				a[index[i]][j] = new Value(pj);

				// Modify other elements accordingly
				for (int l = j + 1; l < n; ++l) {
					try {
						a[index[i]][l] = new Value(a[index[i]][l].toDouble() - pj * a[index[j]][l].
							toDouble());
					} catch (IllegalValueTypeException ex) {
						Logger.getLogger(MINVERSE.class.getName()).
							log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}

	/**
	 * Returns parameters
	 *
	 * @return
	 */
	public FunctionParameter[] getParameters() {
		return parameters;
	}

	public boolean isVarArg() {
		return true;
	}

	@Override
	public String getFunctionDescription() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
