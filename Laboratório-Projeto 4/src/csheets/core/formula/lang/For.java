/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;

/**
 *
 * @author rafael
 */
public class For implements Function {

	public static final FunctionParameter[] parameters = new FunctionParameter[]{new FunctionParameter(Value.Type.UNDEFINED, "Term", false, "A sequence of instructions")};

	@Override
	public String getIdentifier() {
		return "FOR";
	}

	@Override
	public Value applyTo(Expression[] args) throws IllegalValueTypeException {
//		for (Expression e : args) {
//			System.out.println(e.toString() + "\n");
//		}
		Expression startCond = args[0];
		Expression endCond = args[1];
                
		double start = startCond.evaluate().toDouble();
		double end = endCond.evaluate().toDouble();
		double valorSaida = 0;
		if (end > start) {
			for (int i = (int) start; i < end; i++) {
				valorSaida += args[args.length - 1].evaluate().toDouble();
			}
		}

		return new Value(valorSaida);
	}

	@Override
	public FunctionParameter[] getParameters() {
		return parameters;
	}

	@Override
	public boolean isVarArg() {
		return true;
	}

	@Override
	public String getFunctionDescription() {
		return "For cycle";
	}

}
