/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;

/**
 * Attribution Operator
 *
 * @author dnamorim
 */
public class Attribution implements BinaryOperator {

	/**
	 * Creates a new Attribution Operator
	 */
	public Attribution() {

	}

	/**
	 * Evaluetes the expression and returns the result
	 *
	 * @param leftOperand cell reference
	 * @param rightOperand expression to attribute
	 * @return the result of the expression
	 * @throws IllegalValueTypeException unknown value founded
	 */
	@Override
	public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {
		Value rightOp = rightOperand.evaluate();

		if (leftOperand instanceof CellReference) {
			switch (rightOp.getType()) {
				case NUMERIC:
					return new Value(rightOp.toDouble());
				case BOOLEAN:
					return new Value(rightOp.toBoolean());
				case DATE:
					return new Value(rightOp.toDate());
				case TEXT:
					return new Value(rightOp.toText());
				default:
					throw new IllegalValueTypeException(rightOp, Value.Type.NUMERIC);
			}
		}

		throw new IllegalValueTypeException(rightOp, Value.Type.NUMERIC);
	}

	@Override
	public String getIdentifier() {
		return ":=";
	}

	@Override
	public Value.Type getOperandValueType() {
		return Value.Type.NUMERIC;
	}

}
