/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;

/**
 * Represents a function which returns a sequence of instructions
 *
 * @author dnamorim
 */
public class Block implements Function {

	/**
	 * The only (but repeatable) parameter: a numeric term
	 */
	public static final FunctionParameter[] parameters = new FunctionParameter[]{new FunctionParameter(Value.Type.UNDEFINED, "Term", false, "A sequence of instructions")};

	/**
	 * Creates a new instance of InstructionBlock function
	 */
	public Block() {
	}

	@Override
	public String getIdentifier() {
		return "{";
	}

	@Override
	public Value applyTo(Expression[] args) throws IllegalValueTypeException {
		double expResult = 0f;
		Value value;
//		for (Expression e : args) {
//			System.out.println(e.toString() + "\n");
//		}
		Expression expr = args[args.length - 1];
//		if (expr instanceof BinaryOperation) {
//			value = (((BinaryOperation) expr).getRightOperand()).evaluate();
//			return value;
//		}

		value = expr.evaluate();
		if (value.getType() == Value.Type.NUMERIC) {
			expResult += value.toDouble();
		} else if (value.getType() == Value.Type.MATRIX) {
			for (Value[] vector : value.toMatrix()) {
				for (Value item : vector) {
					if (item.getType() == Value.Type.NUMERIC) {
						expResult += item.toDouble();
					} else {
						throw new IllegalValueTypeException(item, Value.Type.NUMERIC);
					}
				}
			}
		} else {
			throw new IllegalValueTypeException(value, Value.Type.NUMERIC);
		}
		return new Value(expResult);
	}

	@Override
	public FunctionParameter[] getParameters() {
		return parameters;
	}

	@Override
	public boolean isVarArg() {
		return true;
	}

	@Override
	public String getFunctionDescription() {
		return "Represents a function which returns a sequence of instructions";
	}

}
