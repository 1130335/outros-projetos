/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;

/**
 *
 * @author Paulo
 */
public class Display implements Function {

	/**
	 * The function's parameter: form's name
	 */
	public static final FunctionParameter[] parameters = new FunctionParameter[]{
		new FunctionParameter(Value.Type.UNDEFINED, "Term", false, "A sequence of instructions")
	};

	@Override
	public String getIdentifier() {
		return "DISPLAY";
	}

	@Override
	public Value applyTo(Expression[] args) throws IllegalValueTypeException {

		return new Value();
	}

	@Override
	public FunctionParameter[] getParameters() {
		return parameters;
	}

	@Override
	public boolean isVarArg() {
		return true;
	}

	@Override
	public String getFunctionDescription() {
		return "A function that given the form's name display the match form";
	}

}
