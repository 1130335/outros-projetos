/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.compiler;

import csheets.core.Cell;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.UnaryOperation;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import csheets.core.formula.lang.UnknownElementException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 *
 * @author RICARDOLEITE
 */
public class MonetaryExpressionCompiler implements ExpressionCompiler {

	/**
	 * The character that signals that a cell's content is a formula ('#')
	 */
	public static final char FORMULA_STARTER = '#';

	/**
	 * Creates the Monetary expression compiler.
	 */
	public MonetaryExpressionCompiler() {
	}

	public char getStarter() {
		return FORMULA_STARTER;
	}

	public Expression compile(Cell cell, String source) throws FormulaCompilationException {
		// Creates the lexer and parser
		ANTLRStringStream input = new ANTLRStringStream(source);

		// create the buffer of tokens between the lexer and parser
		FormulaLexer lexer = new FormulaLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);

		FormulaParser parser = new FormulaParser(tokens);

		CommonTree tree = null;

		try {
			// Attempts to match an expression
			tree = (CommonTree) parser.expression().getTree();
		} catch (RecognitionException e) {
			//String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
			String message = parser.getErrorMessage(e, parser.tokenNames);
			throw new FormulaCompilationException("At (" + e.line + ";" + e.charPositionInLine + "): " + message);
		} catch (Exception e) {
			String message = "Other exception : " + e.getMessage();
			throw new FormulaCompilationException(message);
		}
		// Converts the expression and returns it
		return convert(cell, tree);
	}

	/**
	 * Converts the given ANTLR AST to an expression.
	 *
	 * @param node the abstract syntax tree node to convert
	 * @return the result of the conversion
	 */
	protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException {
		int nrChilds;
		nrChilds = node.getChildCount();
		if (nrChilds == 0) {
			try {
				switch (node.getType()) {
					case FormulaLexer.NUMBER:
						return new Literal(Value.parseNumericValue(node.
							getText()));
					case FormulaLexer.STRING:
						return new Literal(Value.
							parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
					case FormulaLexer.CELL_REF:
						return new CellReference(cell.getSpreadsheet(), node.
												 getText());
//					case FormulaParserTokenTypes.NAME:
						/* return cell.getSpreadsheet().getWorkbook().
					 getRange(node.getText()) (Reference)*/
				}
			} catch (ParseException e) {
				throw new FormulaCompilationException(e);
			}
		}

		// Convert function call
		Function function = null;
		try {
			function = Language.getInstance().getFunction(node.getText());

		} catch (UnknownElementException e) {
		}

		if (function != null) {
			List<Expression> args = new ArrayList<Expression>();
			Tree child = node.getChild(0);
			if (child != null) {

				for (int nChild = 0; nChild < nrChilds; ++nChild) {
					child = node.getChild(nChild);
					args.add(convert(cell, child)); //recursive, will add to args the final nodes
				}
			}
			Expression[] argArray = args.toArray(new Expression[args.size()]); //final function
			return new FunctionCall(function, argArray);
		}

		if (node.getChildCount() == 1) // Convert unary operation
		{
			return new UnaryOperation(
				Language.getInstance().getUnaryOperator(node.getText()),
				convert(cell, node.getChild(0))
			);
		} else if (node.getChildCount() == 2) {

			// Convert binary operation
			BinaryOperator operator = Language.getInstance().
				getBinaryOperator(node.getText());
			if (operator instanceof RangeReference) {
				return new ReferenceOperation(
					(Reference) convert(cell, node.getChild(0)),
					(RangeReference) operator,
					(Reference) convert(cell, node.getChild(1))
				);
			} else {
				return new BinaryOperation(
					convert(cell, node.getChild(0)),
					operator,
					convert(cell, node.getChild(1))
				);
			}
		} else // Shouldn't happen
		{
			throw new FormulaCompilationException();
		}
	}
}
