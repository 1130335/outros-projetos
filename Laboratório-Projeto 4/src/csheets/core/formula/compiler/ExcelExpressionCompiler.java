/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * ATB (April, 2014): Updated to use antlr3 generated parser and lexer
 */
package csheets.core.formula.compiler;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.UnaryOperation;
import csheets.core.formula.lang.Attribution;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import csheets.core.formula.lang.RelationalOperator;
import csheets.core.formula.lang.UnknownElementException;
import csheets.core.formula.lang.VariableReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * A compiler that generates Excel-style formulas from strings.
 *
 * @author Einar Pehrson
 */
public class ExcelExpressionCompiler implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a formula ('=')
     */
    public static final char FORMULA_STARTER = '=';

    /**
     * Creates the Excel expression compiler.
     */
    public ExcelExpressionCompiler() {
    }

    @Override
    public char getStarter() {
        return FORMULA_STARTER;
    }

    @Override
    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        // Creates the lexer and parser
        ANTLRStringStream input = new ANTLRStringStream(source);

        // create the buffer of tokens between the lexer and parser
        FormulaLexer lexer = new FormulaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        FormulaParser parser = new FormulaParser(tokens);

        CommonTree tree = null;

        try {
            // Attempts to match an expression
            tree = (CommonTree) parser.expression().getTree();
        } /* catch (MismatchedTokenException e){
         //not production-quality code, just forming a useful message
         String expected = e.expecting == -1 ? "<EOF>" : parser.tokenNames[e.expecting];
         String found = e.getUnexpectedType() == -1 ? "<EOF>" : parser.tokenNames[e.getUnexpectedType()];

         String message="At ("+e.line+";"+e.charPositionInLine+"): "+"Fatal mismatched token exception: expected " + expected + " but was " + found;
         throw new FormulaCompilationException(message);
         } catch (NoViableAltException e) {
         //String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
         String message=parser.getErrorMessage(e, parser.tokenNames);
         String message2="At ("+e.line+";"+e.charPositionInLine+"): "+message;
         throw new FormulaCompilationException(message2);
         } */ catch (RecognitionException e) {
            //String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
            String message = parser.getErrorMessage(e, parser.tokenNames);
            throw new FormulaCompilationException("At (" + e.line + ";" + e.charPositionInLine + "): " + message);
        } catch (Exception e) {
            String message = "Other exception : " + e.getMessage();
            throw new FormulaCompilationException(message);
        }

        // Converts the expression and returns it
        return convert(cell, tree);
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     */
    protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException {
        // System.out.println("Converting node '" + node.getText() + "' of tree '" + node.toStringTree() + "' with " + node.getNumberOfChildren() + " children.");
        if (node.getChildCount() == 0) {
            try {
                switch (node.getType()) {
                    case FormulaLexer.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case FormulaLexer.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case FormulaLexer.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());
//					case FormulaParserTokenTypes.NAME:
						/* return cell.getSpreadsheet().getWorkbook().
                     getRange(node.getText()) (Reference)*/
                    case FormulaLexer.VAR_REF:
                        return new VariableReference(cell.getSpreadsheet(), node.getText());
                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }
//=FOR{A2:=0;A2:=5;5}
        // Convert function call
        Function function = null;
        try {
            if(node.getText() != null) {
            function = Language.getInstance().getFunction(node.getText());
            }
        } catch (UnknownElementException e) {
        }

        //if the expression starts with a Instruction Block
        if (function != null) {
            if (function.getIdentifier().equalsIgnoreCase("{")) {
                Expression[] argArray;
                argArray = compileInstructionBlock(cell, node);
                return new FunctionCall(function, argArray);
            } else if (function.getIdentifier().equalsIgnoreCase("for")) {
                //	System.out.println("meh");
                Expression[] argArray;
                argArray = compileFor(cell, node);
//				for (Expression argArray1 : argArray) {
//					System.out.println(argArray1);
//				}
                return new FunctionCall(function, argArray);
            } else if (function.getIdentifier().equalsIgnoreCase("display")) {

                JFrame form = cell.getSpreadsheet().getFormByName(node.
                        getChild(0).getText());
                if (form != null) {
                    if (form.getTitle().equals(node.getChild(0).getText())) {
                        form.setLocationRelativeTo(null);
                        form.setVisible(true);
                    }
                }
                return new FunctionCall(function, new Expression[]{convert(cell, node.
                    getChild(0))});
            } else {
                List<Expression> args = new ArrayList<>();
                for (int nChild = 0; nChild < node.getChildCount(); ++nChild) {
                    Tree child = node.getChild(nChild);
                    if (child != null) {
                        args.add(convert(cell, child));

                    }
                }
                Expression[] argArray = args.
                        toArray(new Expression[args.size()]);
                return new FunctionCall(function, argArray);
            }
        }

        // Convert unary operation
        if (node.getChildCount() == 1) {
            return new UnaryOperation(
                    Language.getInstance().getUnaryOperator(node.getText()),
                    convert(cell, node.getChild(0))
            );
        } else if (node.getChildCount() == 2) {
            // Convert binary operation
            BinaryOperator operator = Language.getInstance().
                    getBinaryOperator(node.getText());
            if (operator instanceof RangeReference) {
                return new ReferenceOperation(
                        (Reference) convert(cell, node.getChild(0)),
                        (RangeReference) operator,
                        (Reference) convert(cell, node.getChild(1))
                );

            } else if (operator instanceof Attribution) {
                String childContent = node.getChild(0).getText();
                try {
                    CellReference cellR = new CellReference(cell.
                            getSpreadsheet(), childContent);
                    Cell cellToAttribute = cellR.getCell();
                    Expression exp = convert(cellToAttribute, node.getChild(1));
                    cellToAttribute.setContent(exp.evaluate().toString());
                } catch (ParseException ex) {
                    throw new FormulaCompilationException(ex);
                } catch (IllegalValueTypeException ex) {
                    throw new FormulaCompilationException(ex);
                }

                return new BinaryOperation((Reference) convert(cell, node.
                        getChild(0)), operator, convert(cell, node.
                                getChild(1)));
            } else {
                return new BinaryOperation(
                        convert(cell, node.getChild(0)),
                        operator,
                        convert(cell, node.getChild(1))
                );
            }
        } else // Shouldn't happen
        {
            throw new FormulaCompilationException();
        }
    }

    /**
     * Compiles and regonzise a Instruction Block
     *
     * @param cell
     * @param node
     * @return
     */
    private Expression[] compileInstructionBlock(Cell cell, Tree node) throws FormulaCompilationException {
        List<Expression> args = new ArrayList<Expression>();
        int total = node.getChildCount();
        for (int nChild = 0; nChild < total; ++nChild) {
            Tree child = node.getChild(nChild);

            if (child != null) {
                if ((nChild < total - 1)) {
                    Tree nextchild = node.getChild(nChild + 1);
                    if (nextchild.getText().equalsIgnoreCase(":=")) {
                        // Attribution Code
                        BinaryOperator operator = Language.getInstance().
                                getBinaryOperator(":=");
                        BinaryOperation binop = new BinaryOperation(convert(cell, node.
                                getChild(nChild)), operator, convert(cell, node.
                                        getChild(nChild + 1)));
                        args.add(binop);
                        nChild = nChild + 2;
                    }
                } else {
                    args.add(convert(cell, child));
                }
            }
        }

        Expression[] argArray = args.toArray(new Expression[args.size()]);
        return argArray;
    }

    private Expression[] compileFor(Cell cell, Tree node) throws FormulaCompilationException {
        List<Expression> arguments = new ArrayList();
                Tree child = node.getChild(0);
 
                Tree child2 = child.getChild(0);
                Expression exp = convert(cell, child2);
                if (exp instanceof BinaryOperation) {
                        if (((BinaryOperation) exp).getOperator() instanceof Attribution) {
                                arguments.add(exp);
                                child2 = child.getChild(1);
                                exp = convert(cell, child2);
                                if (exp instanceof BinaryOperation) {
                                        if (((BinaryOperation) exp).getOperator() instanceof RelationalOperator) {
                                                arguments.add(exp);
                                                int total = child.getChildCount();
                                                if (total > 0) {
                                                        for (int nChild = 2; nChild < total; ++nChild) {
                                                                child2 = child.getChild(nChild);
                                                                exp = convert(cell, child2);
                                                                arguments.add(exp);
                                                        }
                                                } else {
                                                        throw new FormulaCompilationException();
                                                }
                                        } else {
                                                throw new FormulaCompilationException();
                                        }
                                } else {
                                        throw new FormulaCompilationException();
                                }
                        } else {
                                throw new FormulaCompilationException();
                        }
                } else {
                        throw new FormulaCompilationException();
                }
 
                Expression[] argArray = arguments.toArray(new Expression[arguments.
                        size()]);
                return argArray;
    }
}
