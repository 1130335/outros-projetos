/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui.ctrl;

import csheets.core.formula.util.MoneyRates;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 *
 * @author RICARDLEITE
 */
public class EditRatesAction extends FocusOwnerAction {

	private final JRadioButton dollar = new JRadioButton("DOLLAR", false);
	private final JRadioButton euro = new JRadioButton("EURO", false);
	private final JRadioButton pound = new JRadioButton("POUND", false);
	private final RadioButtonHandler handler = new RadioButtonHandler();
	private MoneyRates mr = new MoneyRates();
	private final JLabel dollarlabel = new JLabel("DOLLAR : " + mr.getDollar(), JLabel.LEFT);
	private final JLabel eurolabel = new JLabel("EURO : " + mr.getEuro(), JLabel.LEFT);
	private final JLabel poundlabel = new JLabel("POUND : " + mr.getPound(), JLabel.LEFT);

	public EditRatesAction() {
	}

	@Override
	protected String getName() {
		return "Edit Rates";
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		final JFrame frame = new JFrame("Money Rates");
		JPanel bottomPanel = createPanelButtons(frame);
		JPanel panel1 = createPrincipalPanel();
		JPanel panel2 = createRadioButtonsPanel();
		JPanel principal = new JPanel();
		Container c = frame.getContentPane();
		c.setBackground(Color.red);
		principal.setLayout(new BorderLayout());
		principal.add(bottomPanel, BorderLayout.SOUTH);
		principal.add(panel2, BorderLayout.NORTH);
		principal.add(panel1, BorderLayout.CENTER);

		frame.getContentPane().add(principal, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close(frame);
			}
		});

		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setSize(350, 350);
		frame.setLocation(450, 200);
		frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));
		frame.setResizable(false);
		frame.setVisible(true);

	}

	private void close(JFrame frame) {
		frame.dispose();
	}

	private JPanel createPanelButtons(final JFrame frame) {
		JPanel bottomPanel = new JPanel();

		bottomPanel.setLayout(new FlowLayout());
		JButton ok = new JButton("Confirm");
		bottomPanel.add(ok);
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.
					showMessageDialog(null, "Your changes have been saved");
				close(frame);
			}
		});
		JButton cancel = new JButton("Cancel");
		bottomPanel.add(cancel);
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new MoneyRates();
				close(frame);
			}
		});

		return bottomPanel;

	}

	private JPanel createPrincipalPanel() {
		JPanel p = new JPanel();
		GridLayout gl = new GridLayout(4, 2);
		gl.setHgap(50);
		p.setLayout(gl);
		JButton buttond = new JButton("Edit Dollar Rate");
		JButton buttone = new JButton("Edit Euro Rate");
		JButton buttonp = new JButton("Edit Pound Rate");
		p.add(new JLabel("", JLabel.LEFT));
		p.add(new JLabel(""));
		p.add(eurolabel);
		p.add(buttone);
		p.add(dollarlabel);
		p.add(buttond);

		buttond.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BigDecimal dollar = new BigDecimal(JOptionPane.
					showInputDialog("Rate for dollar:"));
				mr.setDollar(dollar);
				dollarlabel.setText("DOLLAR : " + mr.getDollar());
			}
		});

		buttone.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BigDecimal euro = new BigDecimal(JOptionPane.
					showInputDialog("Rate for Euro:"));
				mr.setEuro(euro);
				eurolabel.setText("EURO : " + mr.getEuro());
			}
		});
		p.add(poundlabel);
		p.add(buttonp);

		buttonp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BigDecimal pound = new BigDecimal(JOptionPane.
					showInputDialog("Rate for pound:"));
				mr.setPound(pound);
				poundlabel.setText("POUND : " + mr.getPound());
			}
		});

		return p;
	}

	private JPanel createRadioButtonsPanel() {
		JPanel radiopanel = new JPanel();

		radiopanel.setLayout(new FlowLayout());

		JLabel myLabel = new JLabel("Desired Currency:");

		ButtonGroup bg = new ButtonGroup();
		radiopanel.add(myLabel);
		radiopanel.add(euro);
		radiopanel.add(dollar);
		radiopanel.add(pound);
		bg.add(dollar);
		bg.add(euro);
		bg.add(pound);
		dollar.addItemListener(handler);
		euro.addItemListener(handler);
		pound.addItemListener(handler);

		return radiopanel;

	}

	class RadioButtonHandler implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent event) {
			if (euro.isSelected()) {
				mr.setMoney("EURO");
				eurolabel.setText("EURO : " + mr.getEuro());
				dollarlabel.setText("DOLLAR : " + mr.getDollar());
				poundlabel.setText("POUND : " + mr.getPound());
			}
			if (dollar.isSelected()) {
				mr.setMoney("DOLLAR");
			}
			eurolabel.setText("EURO : " + mr.getEuro());
			dollarlabel.setText("DOLLAR : " + mr.getDollar());
			poundlabel.setText("POUND : " + mr.getPound());
			if (pound.isSelected()) {
				mr.setMoney("POUND");
				eurolabel.setText("EURO : " + mr.getEuro());
				dollarlabel.setText("DOLLAR : " + mr.getDollar());
				poundlabel.setText("POUND : " + mr.getPound());
			}
		}
	}
}
