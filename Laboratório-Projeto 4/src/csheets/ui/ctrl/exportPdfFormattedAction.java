/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui.ctrl;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import com.itextpdf.text.Image;
import csheets.core.exportpdf.ui.ExportPdfFormattedUI;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * Core09.2 Harnessed code from prior use case developed by Rafael
 *
 * @author Paulo
 */
public class exportPdfFormattedAction extends FocusOwnerAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;
    SpreadsheetTable fOwner;

    public exportPdfFormattedAction(UIController controller) {
        this.uiController = controller;
    }

    @Override
    protected String getName() {
        return "Export Formatted PDF";
    }

    @Override
    protected void defineProperties() {
        putValue(MNEMONIC_KEY, KeyEvent.VK_X);
        putValue(ACCELERATOR_KEY, KeyStroke.
                getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        putValue(SMALL_ICON, new ImageIcon(CleanSheets.class.
                getResource("res/img/pdf.gif")));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
            fOwner=this.focusOwner;
            ExportPdfFormattedUI exportPdfFormattedUI = new ExportPdfFormattedUI(fOwner,uiController);
            exportPdfFormattedUI.run();
            
        }
}
    
            
//            String[] options = {"SpreadSheet", "Selected Cells", "Workbook", "Cancel"};
//            int response = JOptionPane.
//                    showOptionDialog(null, "What do you wish to export?", "Export pdf", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
//
//            int color;
//
//            if (response == 3) {
//                return;
//
//            } else {
//                String[] opt = {"BLACK", "BLUE", "CYAN", "DARK_GRAY",
//                    "GRAY", "GREEN", "LIGHT_GREY", "MAGENTA", "ORANGE",
//                    "PINK", "RED", "WHITE", "YELLOW", "Cancel"
//                };
//
//                color = JOptionPane.
//                        showOptionDialog(null, "Choose a color to border", "Border Color", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, opt, opt[0]);
//            }
//
//            if (color == 13) {
//                return;
//            }
//
//            JFileChooser fc = new JFileChooser();
//            File workingDirectory = new File(System.getProperty("user.dir"));
//            fc.setCurrentDirectory(workingDirectory);
//            int v = fc.showSaveDialog(this.focusOwner);
//            if (v != JFileChooser.APPROVE_OPTION) {
//                return;
//            }
//            Document document = new Document();
//            PdfWriter.getInstance(document, new FileOutputStream(fc.
//                    getSelectedFile() + ".pdf"));
//            document.open();
//
//            /**
//             * Front page
//             */
//            Paragraph cover = new Paragraph();
//
//            // Empty Line
//            cover.add(new Paragraph(" "));
//            // Header
//            Paragraph header = new Paragraph("Export PDF",
//                    new Font(Font.FontFamily.COURIER, 18, Font.BOLD));
//            header.setAlignment(0);
//            cover.add(header);
//
//            Image img = Image.getInstance(CleanSheets.class.getResource("res/img/isep.jpg"));
//            document.add(img);
//
//            //Empty Lines
//            for (int i = 0; i < 7; i++) {
//                cover.add(new Paragraph(" "));
//            }
//
//            cover.add(new Paragraph("Exported PDF from CleanSheets.",
//                    new Font(Font.FontFamily.COURIER, 10, Font.BOLD)));
//
//            for (int i = 0; i < 14; i++) {
//                cover.add(new Paragraph(" "));
//            }
//
//            cover.add(new Paragraph("CleanSheets - LAPR4 14/15",
//                    new Font(Font.FontFamily.COURIER, 10, Font.BOLD)));
//
//            document.add(cover);
//            // Start a new page
//            document.newPage();
//
//            /**
//             * Front page
//             */
//            int columnNumber;
//            int rowNumber;
//            Paragraph pdfParagraph = new Paragraph();
//            String paragraph = "";
//            int flag = 0;
//            int contcol = 0;
//            if (response == 2) {
//                Workbook wb;
//                wb = uiController.getActiveWorkbook();
//                for (int i = 0; i < wb.getSpreadsheetCount(); i++) {
//                    Spreadsheet ss = wb.getSpreadsheet(i);
//                    /**
//                    * Page title and his font set
//                    */
//                    pdfParagraph.add(ss.getTitle());
//                    pdfParagraph.add(new Paragraph(" "));
//                    pdfParagraph.setAlignment(Element.ALIGN_CENTER);
//                    document.add(pdfParagraph);
//                    pdfParagraph.clear();
//                    
//                    columnNumber = focusOwner.getColumnCount();
//                    rowNumber = focusOwner.getRowCount();
//
//                    Phrase cellContent = new Phrase();
//                    PdfPCell c1 = new PdfPCell();
//                    Font fg = new Font();
//
//                    /**
//                     * contcol number of columns used
//                     */
//                    
//                    for (int k = 0; k < rowNumber; k++) {
//                        for (int l = 0; l < columnNumber; l++) {
//                            if (!ss.getCell(k, l).getContent().equals("")) {
//                                contcol++;
//                                break;
//                            }
//                        }
//                    }
//                    System.out.println(""+contcol);
//                    for (int j = 0; j < rowNumber; j++) { //
//
//                        Color awtColor = null, awtColorFG = null;
//                        int awtRgb = 0, awtRgbFG = 0, awtFontStyle = 0, awtFontSize = 0,hAlignment=0;
//                        String awtFontFamily = null;
//
//                        PdfPTable table = new PdfPTable(contcol);
//
//                        for (int k = 0; k < columnNumber; k++) {
//                            if (!ss.getCell(k, j).getContent().equals("")) {
////                              
//                                /**
//                                 * Getting Cell background color
//                                 */
//                                awtColor = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getBackgroundColor();
//                                awtRgb = awtColor.getRGB();
//                                //--
//
//                                /**
//                                 * Getting Font info
//                                 */
//                                awtColorFG = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getForegroundColor();
//                                awtRgbFG = awtColorFG.getRGB();
//                                awtFontStyle = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getStyle();
//                                awtFontSize = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getSize();
//                                awtFontFamily = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getFamily();
//                                //--
//
//                                /**
//                                 * Setting Font
//                                 */
//                                fg = new Font();
//                                fg.setColor(new BaseColor(awtRgbFG));
//                                fg.setStyle(awtFontStyle);
//                                fg.setSize(awtFontSize);
//                                fg.setFamily(awtFontFamily);
//
//                                /**
//                                 * Adding cell content
//                                 */
//                                cellContent = new Phrase("" + ss.getCell(k, j).getValue(), fg);
//                                //--
//
//                                /**
//                                 * Set cell content and it background color
//                                 */
//                                c1 = new PdfPCell(cellContent);
//
//                                c1.setBackgroundColor(new BaseColor(awtRgb));
//
//                                /**
//                                 * Setting Horizontal Alignment iText PdfCell
//                                 * Alignment != from Cleansheets Aligment
//                                 */
//                                hAlignment = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getHorizontalAlignment();
//
//                                if (hAlignment == 2) {
//                                    c1.setHorizontalAlignment(0);
//                                } else if (hAlignment == 0) {
//                                    c1.setHorizontalAlignment(1);
//                                } else if (hAlignment == 4) {
//                                    c1.setHorizontalAlignment(2);
//                                }
//
//                                /**
//                                * Color for table border
//                                */
//                                if (color == 0) {
//                                    c1.setBorderColor(BaseColor.BLACK);
//                                } else if (color == 1) {
//                                    c1.setBorderColor(BaseColor.BLUE);
//                                } else if (color == 2) {
//                                    c1.setBorderColor(BaseColor.CYAN);
//                                } else if (color == 3) {
//                                    c1.setBorderColor(BaseColor.DARK_GRAY);
//                                } else if (color == 4) {
//                                    c1.setBorderColor(BaseColor.GRAY);
//                                } else if (color == 5) {
//                                    c1.setBorderColor(BaseColor.GREEN);
//                                } else if (color == 6) {
//                                    c1.setBorderColor(BaseColor.LIGHT_GRAY);
//                                } else if (color == 7) {
//                                    c1.setBorderColor(BaseColor.MAGENTA);
//                                } else if (color == 8) {
//                                    c1.setBorderColor(BaseColor.ORANGE);
//                                } else if (color == 9) {
//                                    c1.setBorderColor(BaseColor.PINK);
//                                } else if (color == 10) {
//                                    c1.setBorderColor(BaseColor.RED);
//                                } else if (color == 11) {
//                                    c1.setBorderColor(BaseColor.WHITE);
//                                } else {
//                                    c1.setBorderColor(BaseColor.YELLOW);
//                                }
//
//                                table.addCell(c1);
//
//                            }
//                        }
//                        document.add(table);
//                        pdfParagraph.clear();
//
//                    }
//                    document.newPage();
//                }
//
//            } else if (response == 0) {
//
//                Spreadsheet ss;
//                ss = uiController.getActiveSpreadsheet();
//                columnNumber = focusOwner.getColumnCount();
//                rowNumber = focusOwner.getRowCount();
//                
//                /**
//                 * Page title and his font set
//                 */
//                
//                Paragraph title = new Paragraph("Active Spreadsheet");
//
//                Paragraph empty = new Paragraph(" ");
//
//                title.setAlignment(Element.ALIGN_CENTER);
//                Font f = new Font();
//                f.setColor(BaseColor.BLACK);
//                f.setSize(16);
//                f.setStyle("Arial");
//                title.setFont(f);
//
//                document.add(title);
//                document.add(empty);
//
//                Phrase cellContent = new Phrase();
//                PdfPCell c1 = new PdfPCell();
//                Font fg = new Font();
//
//                /**
//                 * contcol number of columns used
//                 */
//                
//                for (int j = 0; j < rowNumber; j++) {
//                    for (int i = 0; i < columnNumber; i++) {
//                        if (!ss.getCell(j, i).getContent().equals("")) {
//                            contcol++;
//                            break;
//                        }
//                    }
//                }
//
//                for (int j = 0; j < rowNumber; j++) { //
//
//                    Color awtColor = null, awtColorFG = null;
//                    int awtRgb = 0, awtRgbFG = 0, awtFontStyle = 0, awtFontSize = 0;
//                    String awtFontFamily = null;
//                    int hAlignment = 0;
//
//                    PdfPTable table = new PdfPTable(contcol);
//
//                    for (int k = 0; k < columnNumber; k++) {
//
//                        if (!ss.getCell(k, j).getContent().equals("")) {
//
//                            /**
//                             * Getting Cell background color
//                             */
//                            awtColor = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getBackgroundColor();
//                            awtRgb = awtColor.getRGB();
//                            //--
//
//                            /**
//                             * Getting Font info
//                             */
//                            awtColorFG = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getForegroundColor();
//                            awtRgbFG = awtColorFG.getRGB();
//                            awtFontStyle = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getStyle();
//                            awtFontSize = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getSize();
//                            awtFontFamily = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getFont().getFamily();
//                            //--
//
//                            /**
//                             * Setting Font
//                             */
//                            fg = new Font();
//                            fg.setColor(new BaseColor(awtRgbFG));
//                            fg.setStyle(awtFontStyle);
//                            fg.setSize(awtFontSize);
//                            fg.setFamily(awtFontFamily);
//
//                            /**
//                             * Adding cell content
//                             */
//                            cellContent = new Phrase("" + ss.getCell(k, j).getValue(), fg);
//                            //--
//
//                            /**
//                             * Set cell content and it background color
//                             */
//                            c1 = new PdfPCell(cellContent);
//
//                            c1.setBackgroundColor(new BaseColor(awtRgb));
//
//                            /**
//                             * Setting Horizontal Alignment iText PdfCell
//                             * Alignment != from Cleansheets Aligment
//                             */
//                            hAlignment = ((StylableCell) ss.getCell(k, j).getExtension(StyleExtension.NAME)).getHorizontalAlignment();
//
//                            if (hAlignment == 2) {
//                                c1.setHorizontalAlignment(0);
//                            } else if (hAlignment == 0) {
//                                c1.setHorizontalAlignment(1);
//                            } else if (hAlignment == 4) {
//                                c1.setHorizontalAlignment(2);
//                            }
//
//                            /**
//                             * Color for table border
//                             */
//                            if (color == 0) {
//                                c1.setBorderColor(BaseColor.BLACK);
//                            } else if (color == 1) {
//                                c1.setBorderColor(BaseColor.BLUE);
//                            } else if (color == 2) {
//                                c1.setBorderColor(BaseColor.CYAN);
//                            } else if (color == 3) {
//                                c1.setBorderColor(BaseColor.DARK_GRAY);
//                            } else if (color == 4) {
//                                c1.setBorderColor(BaseColor.GRAY);
//                            } else if (color == 5) {
//                                c1.setBorderColor(BaseColor.GREEN);
//                            } else if (color == 6) {
//                                c1.setBorderColor(BaseColor.LIGHT_GRAY);
//                            } else if (color == 7) {
//                                c1.setBorderColor(BaseColor.MAGENTA);
//                            } else if (color == 8) {
//                                c1.setBorderColor(BaseColor.ORANGE);
//                            } else if (color == 9) {
//                                c1.setBorderColor(BaseColor.PINK);
//                            } else if (color == 10) {
//                                c1.setBorderColor(BaseColor.RED);
//                            } else if (color == 11) {
//                                c1.setBorderColor(BaseColor.WHITE);
//                            } else {
//                                c1.setBorderColor(BaseColor.YELLOW);
//                            }
//
//                            table.addCell(c1);
//
//                        }
//                    }
//
//                    document.add(table);
//                    pdfParagraph.clear();
//
//                }
//            } else if (response == 1) {
//                /**
//                 * Page title and his font set
//                 */
//                Paragraph title = new Paragraph("Selected Cells");
//
//                Paragraph empty = new Paragraph(" ");
//
//                title.setAlignment(Element.ALIGN_CENTER);
//                Font f = new Font();
//                f.setColor(BaseColor.BLACK);
//                f.setSize(16);
//                f.setStyle("Arial");
//                title.setFont(f);
//
//                document.add(title);
//                document.add(empty);
//
//                Phrase cellContent = new Phrase();
//                PdfPCell c1 = new PdfPCell();
//                Font fg = new Font();
//
//                for (Cell[] row : focusOwner.getSelectedCells()) {
//
//                    Color awtColor = null, awtColorFG = null;
//                    int awtRgb = 0, awtRgbFG = 0, awtFontStyle = 0, awtFontSize = 0, hAlignment = 0;
//                    String awtFontFamily = null;
//
//                    PdfPTable table = new PdfPTable(focusOwner.getSelectedColumns().length);
//
//                    for (Cell cell : row) {
//
//                        if (!cell.getContent().equals("")) {
//
//                            /**
//                             * Getting Cell background color
//                             */
//                            awtColor = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getBackgroundColor();
//                            awtRgb = awtColor.getRGB();
//                            //--
//
//                            /**
//                             * Getting Font info
//                             */
//                            awtColorFG = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getForegroundColor();
//                            awtRgbFG = awtColorFG.getRGB();
//                            awtFontStyle = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getFont().getStyle();
//                            awtFontSize = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getFont().getSize();
//                            awtFontFamily = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getFont().getFamily();
//                            //--
//
//                            /**
//                             * Setting Font
//                             */
//                            fg = new Font();
//                            fg.setColor(new BaseColor(awtRgbFG));
//                            fg.setStyle(awtFontStyle);
//                            fg.setSize(awtFontSize);
//                            fg.setFamily(awtFontFamily);
//
//                            /**
//                             * Adding cell content
//                             */
//                            cellContent = new Phrase("" + cell.getValue(), fg);
//                            //--
//
//                            /**
//                             * Set cell content and it background color
//                             */
//                            c1 = new PdfPCell(cellContent);
//
//                            c1.setBackgroundColor(new BaseColor(awtRgb));
//
//                            /**
//                             * Setting Horizontal Alignment iText PdfCell
//                             * Alignment != from Cleansheets Aligment
//                             */
//                            hAlignment = ((StylableCell) cell.getExtension(StyleExtension.NAME)).getHorizontalAlignment();
//
//                            if (hAlignment == 2) {
//                                c1.setHorizontalAlignment(0);
//                            } else if (hAlignment == 0) {
//                                c1.setHorizontalAlignment(1);
//                            } else if (hAlignment == 4) {
//                                c1.setHorizontalAlignment(2);
//                            }
//
//                            
//                            /**
//                             * Color for table border
//                             */
//                            if (color == 0) {
//
//                                c1.setBorderColor(BaseColor.BLACK);
//                            } else if (color == 1) {
//                                c1.setBorderColor(BaseColor.BLUE);
//                            } else if (color == 2) {
//                                c1.setBorderColor(BaseColor.CYAN);
//                            } else if (color == 3) {
//                                c1.setBorderColor(BaseColor.DARK_GRAY);
//                            } else if (color == 4) {
//                                c1.setBorderColor(BaseColor.GRAY);
//                            } else if (color == 5) {
//                                c1.setBorderColor(BaseColor.GREEN);
//                            } else if (color == 6) {
//                                c1.setBorderColor(BaseColor.LIGHT_GRAY);
//                            } else if (color == 7) {
//                                c1.setBorderColor(BaseColor.MAGENTA);
//                            } else if (color == 8) {
//                                c1.setBorderColor(BaseColor.ORANGE);
//                            } else if (color == 9) {
//                                c1.setBorderColor(BaseColor.PINK);
//                            } else if (color == 10) {
//                                c1.setBorderColor(BaseColor.RED);
//                            } else if (color == 11) {
//                                c1.setBorderColor(BaseColor.WHITE);
//                            } else {
//                                c1.setBorderColor(BaseColor.YELLOW);
//                            }
//
//                            table.addCell(c1);
//                        }
//                    }
//
//                    document.add(table);
//                }
//            }
//
//            document.close();
//        } catch (FileNotFoundException | DocumentException event) {
//            event.printStackTrace();
//        } catch (IOException ex) {
//            Logger.getLogger(exportpdfAction.class.getName()).log(Level.SEVERE, null, ex);
//        }
    

