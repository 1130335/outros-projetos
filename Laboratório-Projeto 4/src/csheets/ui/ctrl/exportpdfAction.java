/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui.ctrl;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author rafael
 */
public class exportpdfAction extends FocusOwnerAction {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	public exportpdfAction(UIController controller) {
		this.uiController = controller;
	}

	@Override
	protected String getName() {
		return "Export pdf";
	}

	@Override
	protected void defineProperties() {
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
		putValue(ACCELERATOR_KEY, KeyStroke.
				 getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		putValue(SMALL_ICON, new ImageIcon(CleanSheets.class.
				 getResource("res/img/pdf.gif")));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
//			File file = new File(FILE);
//			try {
//				file.createNewFile();
//			} catch (IOException ex) {
//				Logger.getLogger(exportpdfAction.class.getName()).
//					log(Level.SEVERE, null, ex);
//			}
			String[] options = {"SpreadSheet", "Selected Cells", "Workbook", "Cancel"};
			int response = JOptionPane.
				showOptionDialog(null, "What do you wish to export?", "Export pdf", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
			if (response == 3) {
				return;
			}
			JFileChooser fc = new JFileChooser();
			File workingDirectory = new File(System.getProperty("user.dir"));
			fc.setCurrentDirectory(workingDirectory);
			int v = fc.showSaveDialog(this.focusOwner);
			if (v != JFileChooser.APPROVE_OPTION) {
				return;
			}
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(fc.
								  getSelectedFile()));
			document.open();
			int columnNumber;
			int rowNumber;
			Paragraph pdfParagraph = new Paragraph();
			String paragraph = "";
			int flag = 0;
			if (response == 2) {
				Workbook wb;
				wb = uiController.getActiveWorkbook();
				for (int i = 0; i < wb.getSpreadsheetCount(); i++) {
					Spreadsheet ss = wb.getSpreadsheet(i);
					pdfParagraph.add(ss.getTitle());
					document.add(pdfParagraph);
					pdfParagraph.clear();
					columnNumber = focusOwner.getColumnCount();
					rowNumber = focusOwner.getRowCount();
					for (int j = 0; j < rowNumber; j++) {
						paragraph = "";
						for (int k = 0; k < columnNumber; k++) {
							if (!ss.getCell(k, j).getContent().equals("")) {
								paragraph = paragraph + ss.getCell(k, j).
									getAddress().
									toString() + ":=" + ss.getCell(k, j).
									getValue().
									toString() + " ";
							}
						}
						pdfParagraph.add(paragraph);
						document.add(pdfParagraph);
						pdfParagraph.clear();

					}
				}

			} else if (response == 0) {
				Spreadsheet ss;
				ss = uiController.getActiveSpreadsheet();
				columnNumber = focusOwner.getColumnCount();
				rowNumber = focusOwner.getRowCount();
//				columnNumber = ss.getRowCount();
//				rowNumber = ss.getColumnCount();
				for (int j = 0; j < rowNumber; j++) {
					paragraph = "";
					for (int k = 0; k < columnNumber; k++) {
						if (!ss.getCell(k, j).getContent().equals("")) {
							paragraph = paragraph + ss.getCell(k, j).
								getAddress().
								toString() + ":=" + ss.getCell(k, j).
								getValue().
								toString() + " ";
						}
					}
					pdfParagraph.add(paragraph);
					document.add(pdfParagraph);
					pdfParagraph.clear();

				}
			} else if (response == 1) {
//				Spreadsheet ss;
//				ss = uiController.getActiveSpreadsheet();
				//SpreadsheetTable tabela = new SpreadsheetTable(ss, uiController);
				//Cell[][] cells = focusOwner.getSelectedCells();
				//	System.out.println("");
				for (Cell[] row : focusOwner.getSelectedCells()) {
					paragraph = "";
					for (Cell cell : row) {
						if (!cell.getContent().equals("")) {
							paragraph = paragraph + cell.
								getAddress().
								toString() + ":=" + cell.
								getValue().
								toString() + " ";
						}
					}
					pdfParagraph.add(paragraph);
					document.add(pdfParagraph);
					pdfParagraph.clear();

				}
			}

			document.close();
		} catch (FileNotFoundException | DocumentException event) {
			event.printStackTrace();
		}
	}

}