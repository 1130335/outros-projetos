/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui.ctrl;

import csheets.core.export.ExportXMLController;
import csheets.ui.FileChooser;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.MNEMONIC_KEY;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ExportXMLAction extends FocusOwnerAction {

	private UIController controller;

	public ExportXMLAction(UIController controller) {
		this.controller = controller;
	}

	@Override
	protected String getName() {
		return "Export XML";
	}

	@Override
	protected void defineProperties() {
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
		putValue(ACCELERATOR_KEY, KeyStroke.
				 getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		ExportXMLController controllerExp = new ExportXMLController(this.controller);

		String[] list = {"Workbook", "Spreadsheet(s)", "Selected Cells", "Cancel"};

		int resp = JOptionPane.
			showOptionDialog(null, "What do you want to export?", "Export XML", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, list, list[0]);

		if (resp == 3) {
			return;
		}

		String[] tags = {"workbook", "spreadsheet", "cell", "content", "column", "row"};

		tags[0] = JOptionPane.showInputDialog("Tag for Workbook", tags[0]);
		tags[1] = JOptionPane.showInputDialog("Tag for Spreadsheet", tags[1]);
		tags[2] = JOptionPane.showInputDialog("Tag for Cell", tags[2]);
		tags[3] = JOptionPane.showInputDialog("Tag for Content", tags[3]);
		tags[4] = JOptionPane.showInputDialog("Tag for Column", tags[4]);
		tags[5] = JOptionPane.showInputDialog("Tag for Row", tags[5]);

		FileChooser fc = new FileChooser(null, null);

		File f = fc.getFileToSave();

		controllerExp.exportXMLFile(tags, f);

		if (resp == 0) {
			try {
				controllerExp.exportWorkbook();
			} catch (IOException ex) {
				Logger.getLogger(ExportXMLAction.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		} else if (resp == 1) {
			int numPages = Integer.parseInt(JOptionPane.
				showInputDialog("Number of pages"));
			int[] pages = new int[numPages + 1];
			pages[0] = numPages;
			for (int i = 1; i <= numPages; i++) {
				pages[i] = Integer.parseInt(JOptionPane.
					showInputDialog("Number of the page")) - 1;
			}
			try {
				controllerExp.exportPages(pages);
			} catch (IOException ex) {
				Logger.getLogger(ExportXMLAction.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		} else {
			int numberPage = Integer.parseInt(JOptionPane.
				showInputDialog("Number of the page")) - 1;
			try {
				controllerExp.exportPartOfPage(focusOwner, numberPage);
			} catch (IOException ex) {
				Logger.getLogger(ExportXMLAction.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		}

	}

}
