/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui;


import javax.swing.ImageIcon;
import javax.swing.JMenu;
import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import csheets.ui.ctrl.exportPdfFormattedAction;
import csheets.ui.ctrl.exportpdfAction;

/**
 *
 * @author Paulo
 */
@SuppressWarnings("serial")
public class ExportPdfMenu extends JMenu {

	/** The user interface controller */
	private UIController uiController;

        
	public ExportPdfMenu(UIController uiController) {
		super("Export PDF");

		
		this.uiController = uiController;
		

		// Configures menu
		setIcon(new ImageIcon(CleanSheets.class.getResource("res/img/pdf.gif")));


		// Adds removal items
		addSeparator();
		add(new exportpdfAction(uiController));
		add(new exportPdfFormattedAction(uiController));
		
        }
}
