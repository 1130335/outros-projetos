/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.Game;

import csheets.ext.Extension;
import csheets.ext.Game.ui.UIgameChallenge;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * Classe gameChallenge
 *
 * @author Rafael
 */
public class gameChallenge extends Extension {

	public static final String NAME = "Game";

	//construtor que cria a extensão SendMessage e inicia o TcpServer quando esta for adicionada ao menu de extensões.
	public gameChallenge() {
		super("Game");


	}

	public UIExtension getUIExtension(UIController uiController) {
		return new UIgameChallenge(this, uiController);
	}


}