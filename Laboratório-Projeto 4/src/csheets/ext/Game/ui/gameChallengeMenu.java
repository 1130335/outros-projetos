/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.Game.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Classe que cria a extensão Desafio no menu Extensions
 *
 * @author Vitor
 */
public class gameChallengeMenu extends JMenu {

	public gameChallengeMenu(UIController uiController) {
		super("Gaming");
		setMnemonic(KeyEvent.VK_J);

		add(new gameChallengeAction(uiController));
	}

}
