package csheets.ext.Game.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class gameChallengeAction extends BaseAction {

	protected UIController uiController;

	public gameChallengeAction(UIController uiController) {
		this.uiController = uiController;
	}

	@Override
	protected String getName() {
		return "Challenge Player";
	}

	@Override
	protected void defineProperties() {
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		final JFrame f = new JFrame("Game");
		f.setSize(new Dimension(220, 100));
		f.setAlwaysOnTop(true);
		f.setLocationRelativeTo(null);
		f.setResizable(false);

		//painel titulo
		JPanel pt = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pt.add(new JLabel("Games"));
		f.add(pt, BorderLayout.NORTH);

		//painel sequencias
		JPanel Emenu = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel titulo = new JLabel("Choose your game");
		Emenu.add(titulo);
		JPanel EBotoes = new JPanel(new FlowLayout(FlowLayout.CENTER));

		//JButton jogoDesafio = new JButton("Challenge");
		JButton jogoCheckers = new JButton("Checkers");
		JButton jogoBattleship = new JButton("Battleship");
		//EBotoes.add(jogoDesafio);
		EBotoes.add(jogoCheckers);
		EBotoes.add(jogoBattleship);

		f.add(Emenu);
		f.add(EBotoes);

		//US85 - Vitor Rodrigues
//		jogoDesafio.addActionListener(new java.awt.event.ActionListener() {
//
//			@Override
//			public void actionPerformed(java.awt.event.ActionEvent evt) {
//				gameChallengePanel gameChallengePanel = new gameChallengePanel();
//				f.dispose();
//			}
//		});
		jogoCheckers.addActionListener(new java.awt.event.ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				UIBattleship uiJogoCheckers = new UIBattleship();
				f.dispose();
			}
		});

		jogoBattleship.addActionListener(new java.awt.event.ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				UIBattleship uiJogoBattleship = new UIBattleship();
				f.dispose();
			}
		});

		f.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}

}
