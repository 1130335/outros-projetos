package csheets.ext.Game.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Rafael
 */
public class UIBattleship extends JFrame {

	private ServerSocket serverSocket = null;
	private Socket connection = null;
	private ObjectInputStream input = null;
	private ObjectOutputStream output = null;

	private int width;
	private int height;

	private JTextArea textArea;	//textarea para chat e notificações
	private JScrollPane sp;     //scroll pane da textarea acima

	private JTextField ip, port, nick, message; // Endereço IP, numero de porta, nickname, mensagem chat
	private JButton join, create, newGame; 	//botões para Entrar em Jogo, Criar Jogo e Jogar de Novo

	private String xo = ""; 		// X -> Servidor / O -> Cliente
	private String nick1, nick2, msg; 	// nick1 -> Servidor / nick2 -> Cliente, mensagem chat

	private boolean signal; 		// sinal para mudança de turno
	private boolean signalTransmission = true;	//se o valor for falso, então não são mandadas mais mensagens pela internet

	private int countGames = 0;		//Numero de jogos. 0 joga X primeiro; 1 joga O primeiro; 2 joga X primeiro, etc
	private int nMoves = 0;

	private Font fontText, fontButtons; //Fonts

	private int score1 = 0; //scores para ambos os jogadores
	private int score2 = 0;

	public UIBattleship() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
		} //Aspecto da janela ( clássico ;) )

		//Dimensoes da janela de jogo
		width = 450;
		height = 250;

		setSize(width, height);
		setResizable(false);
		setTitle("Battleship");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);

		setLayout(new BorderLayout());

		fontText = new Font("Arial", Font.PLAIN, 30);
		fontButtons = new Font("Arial", Font.PLAIN, 12);

		//PAINEL CENTRAL (Botoes)
		JPanel pCenter = new JPanel();
		pCenter.setLayout(new GridLayout(3, 3));
//
		add(pCenter, BorderLayout.CENTER);

		//PAINEL CHAT
		JPanel pEast = new JPanel();
		pEast.setLayout(new BorderLayout());
		pEast.setPreferredSize(new Dimension(270, height));
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		textArea.setFont(fontButtons);
		sp = new JScrollPane(textArea);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pEast.add(sp, BorderLayout.CENTER);
		add(pEast, BorderLayout.EAST);

		//PAINEL SUL (msg chat)
		JPanel pSouth = new JPanel();
		pSouth.setLayout(new BorderLayout());
		pSouth.setPreferredSize(new Dimension(width, 50));
		message = new JTextField(" ");
		message.setEditable(false);
		message.setFont(fontText);
		message.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					textArea.append(nick.getText() + ":" + message.getText() + "\n");
					scrollToBottom();
					sendMessage(message.getText());
					message.setText(" ");
				}
			}
		});
		pSouth.add(message, BorderLayout.CENTER);
		add(pSouth, BorderLayout.SOUTH);

		ip = new JTextField("127.0.0.1");
		ip.setToolTipText("Insert IP");
		ip.setPreferredSize(new Dimension(100, 25));
		port = new JTextField("8100");
		port.setToolTipText("Insert server port, default:8100");
		port.setPreferredSize(new Dimension(100, 25));
		nick = new JTextField();
		nick.setToolTipText("Insert username");
		nick.setPreferredSize(new Dimension(100, 25));

		//Botao Criar novo Jogo
		create = new JButton("Create");
		create.setToolTipText("Create new game");
		create.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if (nick.getText().isEmpty() || ip.getText().isEmpty() || port.getText().isEmpty()) {
					try {
						JOptionPane.showMessageDialog(null, "You didn't insert all the data needed!");
					} catch (ExceptionInInitializerError exc) {
					}
					return;
				}

				new CreateButtonThread("CreateButton"); // precisamos de thread enquanto esperamos pelo cliente
			}
		});

		//Botão entrar num jogo
		join = new JButton("Join");
		join.setToolTipText("Join an existing game");
		join.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					if (nick.getText().isEmpty() || ip.getText().isEmpty() || port.getText().isEmpty()) {
						try {
							JOptionPane.showMessageDialog(null, "Please insert all the needed data!");
						} catch (ExceptionInInitializerError exc) {
						}
						return;
					}

					connection = new Socket(ip.getText(), Integer.parseInt(port.getText()));

					output = new ObjectOutputStream(connection.getOutputStream());
					output.flush();
					input = new ObjectInputStream(connection.getInputStream());

					msg = (String) input.readObject();
					textArea.append(msg + "\n");
					scrollToBottom();

					xo = "O";
					signal = false;

					nick2 = nick.getText();

					msg = (String) input.readObject(); // get do nickname do servidor
					nick1 = "" + msg;

					sendMessage(nick2);

//					placeAll(true);
					message.setEditable(true);

					ip.setEnabled(false);
					port.setEnabled(false);
					nick.setEnabled(false);

					textArea.append(nick1 + " plays first!\n");
					scrollToBottom();

					join.setEnabled(false);
					create.setEnabled(false);
					ip.setEnabled(false);
					port.setEnabled(false);
					nick.setEnabled(false);

					new RetrieveMessages("mensagemServidor"); // thread para receber informaçao do servidor
				} catch (HeadlessException | NumberFormatException | IOException | ClassNotFoundException e) {
					closeAll();
					runAll();
					try {
						JOptionPane.showMessageDialog(null, "Error joining game, Server not found!\n" + e);
					} catch (ExceptionInInitializerError exc) {
					}
				}
			}
		});

		//Botão para novo Jogo
		

		//PAINEL NORTE
		JPanel pNorth = new JPanel();
		pNorth.add(ip);
		pNorth.add(port);
		pNorth.add(nick);
		pNorth.add(create);
		pNorth.add(join);

		add(pNorth, BorderLayout.NORTH);

		// --- WINDOW ADAPTER ---
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent event) {
				try {
					InetAddress thisIp = InetAddress.getLocalHost();
					ip.setText(thisIp.getHostAddress());
				} catch (UnknownHostException e) {
					ip.setText("127.0.0.1");
				}
			}

			@Override
			public void windowClosing(WindowEvent event) {
				if (connection != null) {
					sendMessage("Shutting down...");
				}
				closeAll();
			}
		});

		pack();
		setVisible(true);
	}

	//THREAD Botao Criar Jogo
	private class CreateButtonThread implements Runnable {

		public CreateButtonThread(String name) {
			new Thread(this, name).start();
		}

		@Override
		public void run() {
			try {
				join.setEnabled(false);
				create.setEnabled(false);
				port.setEnabled(false);
				nick.setEnabled(false);

				serverSocket = new ServerSocket(Integer.parseInt(port.getText()));

				textArea.append("Waiting for the other player...\n");
				scrollToBottom();
				connection = serverSocket.accept();

				output = new ObjectOutputStream(connection.getOutputStream());
				output.flush();
				input = new ObjectInputStream(connection.getInputStream());
				sendMessage(nick.getText() + " Successfully connected!");
				//textArea.append("Jogador conectado com sucesso!\n");
				scrollToBottom();

				xo = "X";
				signal = true;

				nick1 = nick.getText();

				sendMessage(nick1);

				msg = (String) input.readObject(); // nick do servidor
				nick2 = "" + msg;

				message.setEditable(true);
				ip.setEnabled(false);

				textArea.append(nick1 + " plays first!\n");
				scrollToBottom();
				new RetrieveMessages("MensagemCliente");
			} catch (IOException | ClassNotFoundException | NumberFormatException e) {
				closeAll();
				runAll();
				try {
					JOptionPane.showMessageDialog(null, "Error creating new game!\n" + e);
				} catch (ExceptionInInitializerError exc) {
				}
			}
		}
	}

	//Enviar data através da ligação
	private void sendMessage(String p) {
		try {
			if (signalTransmission) {
				output.writeObject(p);
				output.flush();
			}
		} catch (SocketException e) {
			if (signalTransmission) {
				signalTransmission = false;
				closeAll();
				runAll();
			}
		} catch (IOException e) {
			if (signalTransmission) {
				signalTransmission = false;
				closeAll();
				runAll();
				try {
					JOptionPane.showMessageDialog(null, "Send Info/Disconnect\n" + e);
				} catch (ExceptionInInitializerError exc) {
				}
			}
		}
	}

	//Thread para receber mensagens
	private class RetrieveMessages implements Runnable {

		private boolean threadSignal;
		private final String neitherName;

		public RetrieveMessages(String i) {
			threadSignal = true;
			neitherName = i;
			new Thread(this, neitherName).start();
		}

		@Override
		public void run() {
			while (threadSignal) {
				try {
					msg = "";
					msg = (String) input.readObject();	// receber mensagens

					switch (neitherName) {
						case "mensagemServidor":
							if (msg.equalsIgnoreCase("true")) {
								signal = true;
							} else if (msg.equalsIgnoreCase("false")) {
								signal = false;
							} else if (msg.equalsIgnoreCase("Empate!")) {
								//JOptionPane.showMessageDialog(null, "Empate!");
							} else if (msg.equalsIgnoreCase("Pedido de um novo jogo!")) {

								signal = true;
//
							} else {
								textArea.append(nick1 + ":" + msg + "\n");
								scrollToBottom();
							}
							break;
						case "MensagemCliente":
							if (msg.equalsIgnoreCase("true")) {
								signal = true;
//
							} else {
								textArea.append(nick2 + ":" + msg + "\n");
								scrollToBottom();
							}
							break;
					}
				} catch (HeadlessException | IOException | ClassNotFoundException e) {
					threadSignal = false;
					closeAll();
					runAll();
					try {
						JOptionPane.showMessageDialog(null, "Ligação terminada!");
					} catch (ExceptionInInitializerError exc) {
					}
				}
			}
		}
	}

	private void runAll() {
//		setTextFields();
//		placeAll(false);

		msg = "";
		xo = "";
		signalTransmission = true;
		nMoves = 0;
		countGames = 0;

		ip.setEnabled(true);
		port.setEnabled(true);
		nick.setEnabled(true);
		create.setEnabled(true);
		join.setEnabled(true);

		message.setEditable(false);
	}

	//DESLIGA todas as streams
	private void closeAll() {
		try {
			output.flush();
		} catch (Exception e) {
		}
		try {
			output.close();
		} catch (Exception e) {
		}
		try {
			input.close();
		} catch (Exception e) {
		}
		try {
			serverSocket.close();
		} catch (Exception e) {
		}
		try {
			connection.close();
		} catch (Exception e) {
		}
	}

	//Quando recebe mensagem, faz scroll para o fundo
	public void scrollToBottom() {
		textArea.setCaretPosition(textArea.getText().length());
	}

}
