/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.Game.Sidebar;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Pi Barros Pereira
 */
public class CurrentGameUI extends JPanel{
    private static DefaultListModel gamesModel=new DefaultListModel();
    public CurrentGameUI(){
        JPanel tempPanel = new JPanel();
	tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.Y_AXIS));
        
        JList gamesList=new JList(gamesModel);
        tempPanel.add(new JScrollPane(gamesList));
        
        add(tempPanel);
    }
    public static void addToList(String lel){
        gamesModel.addElement(lel);
    }
}
