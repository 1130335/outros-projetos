/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.Game.Sidebar;

import csheets.ext.Extension;
import csheets.ext.ipc.SendMessage.UI.SendMessageExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Pi Barros Pereira
 */
public class GameSidebarExtension extends Extension {

    public static final String NAME = "Active Games";

    public GameSidebarExtension() {
        super(NAME);
    }

    @Override
    public UIExtension getUIExtension(UIController uiController) {
        if (this.uiExtension == null) {
            this.uiExtension = new GameSidebarUI(this, uiController);
        }
        return this.uiExtension;
    }
}
