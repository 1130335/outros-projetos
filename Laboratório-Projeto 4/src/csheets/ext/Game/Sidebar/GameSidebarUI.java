/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.Game.Sidebar;

import csheets.ext.ipc.SendMessage.UI.SendMessageUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Pi Barros Pereira
 */
public class GameSidebarUI extends UIExtension {

    /**
     * A panel in which the chat window is displayed
     */
    private JPanel sideBar;

    public GameSidebarUI(GameSidebarExtension extension, UIController uiController) {
        super(extension, uiController);
    }
    
    /**
	 * Returns a SendMessageUI
	 *
	 * @return a panel where a chat can take place.
	 */
	@Override
	public JComponent getSideBar() {
		if (sideBar == null) {
			sideBar = new CurrentGameUI();
		}
		return sideBar;
	}

}
