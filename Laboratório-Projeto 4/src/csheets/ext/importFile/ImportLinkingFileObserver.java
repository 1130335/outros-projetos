/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile;

import csheets.ext.importFile.ui.ImportFileController;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Sergio
 */
public class ImportLinkingFileObserver implements Observer {

	private Observable itr;

	public ImportLinkingFileObserver(Observable obs) {
		this.itr = obs;
		itr.addObserver(this);
	}

	@Override
	public void update(Observable ob, Object arg1) {
		if (ob instanceof ImportThreadRun) {
			ImportThreadRun iptr = (ImportThreadRun) ob;
			ImportFileController importController = new ImportFileController(iptr.
				getM_controller());
			importController.setImport(iptr.getFl().getFile(), iptr.
									   getM_controller().
									   getActiveCell().getAddress(), 0, iptr.
									   getFl().
									   getSeparator(), iptr.getM_controller().
									   getActiveSpreadsheet());
		}
	}
}
