/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile;

import csheets.core.Cell;
import csheets.ui.ctrl.UIController;
import java.util.ConcurrentModificationException;
import java.util.Observable;

/**
 * Represents the run of Linking Import Thread
 *
 * @author Sergio 1130711
 */
public class ImportThreadRun extends Observable implements Runnable {

	/**
	 * Linking File to associate with thread
	 */
	private LinkingFile fl;
	/**
	 * User interface controller
	 */
	private UIController m_controller;

	private Cell cell;

	private long lastModified;

	/**
	 * Sets the LinkingFile to this Thread
	 *
	 * @param fl
	 */
	public ImportThreadRun(LinkingFile fl, UIController controller) {
		this.fl = fl;
		this.m_controller = controller;
		this.cell = m_controller.getActiveCell();
		ImportLinkingFileObserver ilfo = new ImportLinkingFileObserver(this);
	}

	/**
	 * Run to thread
	 */
	@Override
	public void run() {
		try {
			while (!fl.getThread().interrupted()) {
				if (getFl().getFile().lastModified() != this.lastModified) {
					setChanged();
					notifyObservers();

					lastModified = getFl().getFile().lastModified();
				}

			}
		} catch (ConcurrentModificationException exc) {
			run();
		}

	}

	/**
	 * @return the fl
	 */
	public LinkingFile getFl() {
		return fl;
	}

	/**
	 * @param fl the fl to set
	 */
	public void setFl(LinkingFile fl) {
		this.fl = fl;
	}

	/**
	 * @return the m_controller
	 */
	public UIController getM_controller() {
		return m_controller;
	}

	/**
	 * @param m_controller the m_controller to set
	 */
	public void setM_controller(UIController m_controller) {
		this.m_controller = m_controller;
	}
}
