/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile.ui;

import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.importFile.ImportXML;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author Sergio 1130711
 */
public class ImportXMLController {

	/**
	 * User interface controller
	 */
	private UIController controller;
	/**
	 * ImportXML object where file,tags and ative workbook are registered
	 */
	private ImportXML imp;

	/**
	 * Create a new Import XML controller
	 *
	 * @param controller user interface controller
	 */
	public ImportXMLController(UIController controller) {
		this.controller = controller;
	}

	/**
	 * Create a ImportXMLFile object with data passed by parameter
	 *
	 * @param tags in xml file
	 * @param f file to import
	 */
	public void importXMLFile(String[] tags, File f) {

		this.imp = new ImportXML(f, tags, this.controller);
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import a Workbook is requested
	 *
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws IllegalValueTypeException
	 * @throws FormulaCompilationException
	 */
	public void importWorkbook() throws IOException, XMLStreamException, IllegalValueTypeException, FormulaCompilationException {
		this.imp.importWorkbook();
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import Pages is requested
	 *
	 * @param pages to import
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws FormulaCompilationException
	 * @throws FileNotFoundException
	 * @throws IllegalValueTypeException
	 */
	public void importPages(int[] pages) throws IOException, XMLStreamException, FormulaCompilationException, FileNotFoundException, IllegalValueTypeException {
		this.imp.importPages(pages);
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import part of Pages/Selected Cells is requested
	 *
	 * @param focusOwner SpreadsheetTable to apply import
	 * @param numberPage number of page to import
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws FormulaCompilationException
	 * @throws FileNotFoundException
	 * @throws IllegalValueTypeException
	 */
	public void importPartOfPage(final SpreadsheetTable focusOwner,
								 int numberPage) throws IOException, XMLStreamException, FormulaCompilationException, FileNotFoundException, IllegalValueTypeException {
		this.imp.importPartOfPage(focusOwner, numberPage);
	}

	/**
	 * Check if file passed by parameter was previously used for export
	 * operation
	 *
	 * @param file to check
	 * @return boolean
	 */
	public boolean wasUsed(File file) {

		if (controller.getExportedXMLfiles().wasUsed(file)) {
			return true;
		}
		return false;
	}

	/**
	 * Return the tags used to export data to xml file passed by parameter
	 *
	 * @param file to check
	 * @return exp.getTags or null
	 */
	public String[] getTagsFromUssedFile(File file) {
		return controller.getExportedXMLfiles().getTagsFromUssedFile(file);

	}

}
