package csheets.ext.importFile.ui;

import csheets.core.Address;
import csheets.core.Spreadsheet;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * An action of the import file extension that exemplifies how to interact with
 * the spreadsheet.
 *
 * @author 1121248DiogoVieira
 */
public class ImportFilelAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public ImportFilelAction(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 * name of menu
	 *
	 * @return
	 */
	@Override
	protected String getName() {
		return "Import File";
	}

	@Override
	protected void defineProperties() {
	}

	/**
	 * A import file action that presents new import and apresents a message
	 * dialog to infor user.
	 *
	 * @param event the event that was fired
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		Address activeCell = this.uiController.getActiveCell().getAddress();
		Spreadsheet spreadsheet = this.uiController.getActiveSpreadsheet();
		File file = fileChosser();
		String[] options = {"Yes", "No"};
		int answerHeader;
		String separator = ",";
		answerHeader = JOptionPane.
			showOptionDialog(null, "Does the file contain a header?", "header", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
		int answer = JOptionPane.
			showOptionDialog(null, "Do you which to define a separator character? (default is set to \",\")", "header", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
		boolean pass = false;
		if (answer == 0) {
			while (!pass) {
				try {
					separator = JOptionPane.
						showInputDialog("Define the separator character");
					if (separator.length() == 1) {
						if (separator.equalsIgnoreCase(".")) {
							separator = "\\.";
						}
						pass = true;
					} else {
						JOptionPane.
							showMessageDialog(null, "Input error", "Input error", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.
						showMessageDialog(null, "Input error", "Input error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			separator = ",";
		}
		ImportFileController importController = new ImportFileController(uiController);
		if (importController.setImport(file, uiController.getActiveCell().
									   getAddress(), answerHeader, separator, spreadsheet)) {
			JOptionPane.showMessageDialog(null, "Import Successful");
		} else {
			JOptionPane.showMessageDialog(null, "Import Failed");
		}
	}

	/**
	 * presents an explorer for the user to choose the file
	 *
	 * @return file selected
	 */
	public File fileChosser() {
		File selectedFile = null;
		JFileChooser fileChooser = new JFileChooser();
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
		}
		return (selectedFile);
	}
}
