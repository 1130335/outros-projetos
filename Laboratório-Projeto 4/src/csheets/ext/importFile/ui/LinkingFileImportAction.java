/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Sergio 1130711
 */
public class LinkingFileImportAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public LinkingFileImportAction(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 * name of menu
	 *
	 * @return
	 */
	@Override
	protected String getName() {
		return "Import Linking File";
	}

	/**
	 * Properties
	 */
	@Override
	protected void defineProperties() {
	}

	/**
	 *
	 * @param event the event that was fired
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		UIImportLinkingFile ui = new UIImportLinkingFile(uiController);
	}
}
