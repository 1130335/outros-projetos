package csheets.ext.importFile.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Representes the UI extension menu of the import extension.
 *
 * @author 1121248DiogoVieira
 */
public class ImportFileMenu extends JMenu {

	/**
	 * Creates a new import menu. This constructor creates and adds the menu
	 * options. In this method only one menu option is created. A menu option is
	 * an action (in this case
	 * {@link csheets.ext.importFile.ui.ImportFileAction})
	 *
	 * @param uiController the user interface controller
	 */
	public ImportFileMenu(UIController uiController) {
		super("Import");
		setMnemonic(KeyEvent.VK_I);
		// Adds font actions
		add(new ImportFilelAction(uiController));
		add(new LinkingFileImportAction(uiController));
	}
}
