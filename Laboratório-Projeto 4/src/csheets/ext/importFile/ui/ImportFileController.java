package csheets.ext.importFile.ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import csheets.core.Address;
import csheets.core.Spreadsheet;
import csheets.ext.importFile.ImportFile;
import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 * Represents the controller to import file
 *
 * @author 1121248DiogoVieira
 */
public class ImportFileController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new ImportFile controller.
	 *
	 * @param uiController the user interface controller
	 */
	public ImportFileController(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 * Attempting to create a new ImportFile If successful, calls the
	 * "FileChooser" for the user to select your file
	 *
	 * @param newFile obeto representa um ficheiro
	 * @param activeCell objecto que presenta a celula ativa
	 * @param answerHeader inteiro para resposta de JOptionPane
	 * @param separator Separador de strings
	 * @param spreadsheet objecto que representa uma spreadsheet
	 * @return
	 */
	public boolean setImport(File newFile, Address activeCell, int answerHeader,
							 String separator, Spreadsheet spreadsheet) {
		ImportFile newImport = new ImportFile(uiController);
		if (newImport != null) {
			if (newImport.
				FileRead(newFile, activeCell, answerHeader, separator, spreadsheet)) {
				return true;
			}
		}
		return false;
	}

}
