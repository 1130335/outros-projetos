/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile.ui;

import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;

/**
 *
 * @author Sergio 1130711
 */
public class ImportXMLAction extends FocusOwnerAction {

	private UIController controller;

	public ImportXMLAction(UIController controller) {
		this.controller = controller;
	}

	@Override
	protected String getName() {
		return "Import XML";
	}

	@Override
	protected void defineProperties() {
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
		putValue(ACCELERATOR_KEY, KeyStroke.
				 getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ImportXMLWindow window = new ImportXMLWindow(focusOwner, controller);
		window.run();
	}

}
