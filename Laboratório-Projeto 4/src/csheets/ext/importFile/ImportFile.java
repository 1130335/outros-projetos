package csheets.ext.importFile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Represent an import from a file.
 *
 * @author 1121248DiogoVieira
 */
public class ImportFile {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	/**
	 * Create new import file
	 *
	 * @param uiController the user interface controller
	 */
	public ImportFile(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 * reads the file and inserts the sheet
	 *
	 * @param selectedFile File for read
	 * @param activeCell The address of the cell that was selected to be the
	 * starting point of the import
	 * @param answerHeader inteiro para opção da resposta de JOptionPane
	 * @param separator Separador das strings
	 * @param spreadsheet objeto que representa uma spreadsheet
	 * @return
	 */
	public boolean FileRead(File selectedFile, Address activeCell,
							int answerHeader, String separator,
							Spreadsheet spreadsheet) {
		try {
			int indexY = activeCell.getColumn();
			int indexX = activeCell.getRow();
			Scanner fileScanner = new Scanner(new File(selectedFile.
				getAbsolutePath()));
			String line;
			String[] values;
			if (answerHeader == 0) {
				fileScanner.nextLine();
			}
			while (fileScanner.hasNext()) {
				Cell cell;
				line = fileScanner.nextLine();
				values = line.split(separator);
				int length = values.length;
				int cont = 0;
				for (int i = indexX; i < indexX + length; i++) {
					cell = spreadsheet.getCell(i, indexY);
					cell.setContent(values[cont]);
					cont++;
				}
				indexY++;
			}
		} catch (FileNotFoundException ex) {
			return false;
		} catch (FormulaCompilationException ex) {
			return false;
		}
		return true;
	}
}
