package csheets.ext.importFile;

import csheets.ext.Extension;
import csheets.ext.importFile.ui.UIExtensionImportFile;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * A simple extension just to show how the extension mechanism works. An
 * extension must extend the Extension abstract class. The class that implements
 * the Extension is the "bootstrap" of the extension.
 *
 * @see Extension
 * @author 1121248DiogoVieira
 */
public class ExtensionImportFile extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Import";

	/**
	 * Creates a new Example extension.
	 */
	public ExtensionImportFile() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension (an instance of
	 * the class {@link  csheets.ext.importFile.ui.UIExtensionImportFile}). <br/>
	 * In this extension example we are only extending the user interface.
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionImportFile(this, uiController);
	}
}
