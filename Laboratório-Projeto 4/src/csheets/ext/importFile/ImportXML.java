/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Sergio 1130711
 */
public class ImportXML {

	/**
	 * File to import
	 */
	private File f;
	/**
	 * Tags in xml file
	 */
	private String[] tags;
	/**
	 * The active workbook
	 */
	private Workbook w;
	/**
	 * User interface controller
	 */
	private UIController m_controller;

	/**
	 * Create a new ImportXML
	 */
	public ImportXML() {
	}

	/**
	 * Create a new ImportXML
	 *
	 * @param f to import
	 * @param tags in xml file
	 * @param controller user interface controller
	 */
	public ImportXML(File f, String[] tags, UIController controller) {
		this.f = f;
		this.tags = tags;
		this.m_controller = controller;
		this.w = controller.getActiveWorkbook();
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import a Workbook is requested
	 *
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws IllegalValueTypeException
	 * @throws FormulaCompilationException
	 */
	public void importWorkbook() throws IOException, XMLStreamException, IllegalValueTypeException, FormulaCompilationException {
		Map<String, String[][]> spreadsheets = new HashMap<String, String[][]>();
		XMLInputFactory factory = XMLInputFactory.newInstance();

		XMLStreamReader reader = factory.
			createXMLStreamReader(new FileInputStream(
					new File(f.getAbsolutePath())));

		String[][] data = new String[1000][1000];
		String text = null;
		String number = null;
		int row = 0;
		int column = 0;
		String value = "";

		while (reader.hasNext()) {
			int Event = reader.next();

			switch (Event) {

				case XMLStreamConstants.START_ELEMENT: {
					if (tags[0].equals(reader.getLocalName())) {
						// workbook
					}
					break;

				}

				case XMLStreamConstants.CHARACTERS: {

					text = reader.getText().trim();

					break;
				}
				case XMLStreamConstants.END_ELEMENT: {
					if (tags[1].equalsIgnoreCase(reader.getLocalName())) {
						//new spreadsheet
						spreadsheets.put(number, data);
						data = new String[20][20];
					} else if ("spreadsheet_number".equalsIgnoreCase(reader.
						getLocalName())) {
						//spreadsheet number
						if (!text.equalsIgnoreCase("")) {
							number = text;
						}
					} else if (tags[3].equalsIgnoreCase(reader.getLocalName())) {
						//cell content
						if (!text.equalsIgnoreCase("")) {
							value = text;
						}
					} else if (tags[4].equalsIgnoreCase(reader.getLocalName())) {
						//column cell
						if (!text.equalsIgnoreCase("")) {
							column = Integer.
								parseInt(text);
						}
					} else if (tags[5].equalsIgnoreCase(reader.getLocalName())) {
						if (!text.equalsIgnoreCase("")) {
							//row cell
							row = Integer.parseInt(text);
						}
						//add cell content to data array
						data[row][column] = value;
					} else if (tags[2].equalsIgnoreCase(reader.getLocalName())) {
						//cell
					}
					break;
				}

			}
		}
		//Update workbook:
		Iterator it;
		int turn = 0;

		it = (Iterator) spreadsheets.keySet().iterator();
		while (it.hasNext()) {

			if (turn != 0) {
				String no = (String) it.next();
				String[][] matriz = spreadsheets.get(no);

				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[0].length; j++) {
						if (matriz[i][j] != null) {
							w.getSpreadsheet(Integer.parseInt(no)).
								getCell(j, i).
								setContent(matriz[i][j]);
						}
					}
				}

			}
			turn++;
		}

		spreadsheets.clear();
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import Pages is requested
	 *
	 * @param pages to import
	 * @throws XMLStreamException
	 * @throws FileNotFoundException
	 * @throws FormulaCompilationException
	 * @throws IllegalValueTypeException
	 */
	public void importPages(int[] pages) throws XMLStreamException, FileNotFoundException, FormulaCompilationException, IllegalValueTypeException {
		Map<String, String[][]> spreadsheets = new HashMap<String, String[][]>();
		XMLInputFactory factory = XMLInputFactory.newInstance();

		XMLStreamReader reader = factory.
			createXMLStreamReader(new FileInputStream(
					new File(f.getAbsolutePath())));

		String[][] data = new String[1000][1000];
		String text = null;
		String number = null;
		int row = 0;
		int column = 0;
		String value = "";

		while (reader.hasNext()) {
			int Event = reader.next();

			switch (Event) {

				case XMLStreamConstants.START_ELEMENT: {
					if (tags[0].equals(reader.getLocalName())) {
						// workbook
					}
					break;

				}

				case XMLStreamConstants.CHARACTERS: {

					text = reader.getText().trim();

					break;
				}
				case XMLStreamConstants.END_ELEMENT: {
					if (tags[1].equalsIgnoreCase(reader.getLocalName())) {
						//new spreadsheet
						spreadsheets.put(number, data);
						data = new String[20][20];
					} else if ("spreadsheet_number".equalsIgnoreCase(reader.
						getLocalName())) {
						//spreadsheet number
						if (!text.equalsIgnoreCase("")) {
							number = text;
						}
					} else if (tags[3].equalsIgnoreCase(reader.getLocalName())) {
						//cell content
						if (!text.equalsIgnoreCase("")) {
							value = text;
						}
					} else if (tags[4].equalsIgnoreCase(reader.getLocalName())) {
						//column cell
						if (!text.equalsIgnoreCase("")) {
							column = Integer.
								parseInt(text);
						}
					} else if (tags[5].equalsIgnoreCase(reader.getLocalName())) {
						//row cell
						if (!text.equalsIgnoreCase("")) {
							row = Integer.parseInt(text);
						}
						//add cell content to data array
						data[row][column] = value;
					} else if (tags[2].equalsIgnoreCase(reader.getLocalName())) {
						//cell
					}
					break;
				}

			}
		}
		//Update spreadsheet´s choosed:
		Iterator it;
		int turn = 0;
		for (int t = 0; t < pages.length; t++) {

			it = (Iterator) spreadsheets.keySet().iterator();
			while (it.hasNext()) {

				if (turn != 0) {
					String no = (String) it.next();
					String[][] matriz = spreadsheets.get(no);

					if (Integer.parseInt(no) == t) {
						for (int i = 0; i < matriz.length; i++) {
							for (int j = 0; j < matriz[0].length; j++) {
								if (matriz[i][j] != null) {
									w.getSpreadsheet(Integer.parseInt(no)).
										getCell(j, i).
										setContent(matriz[i][j]);
								}
							}
						}
					}

				}
				turn++;
			}
		}

		spreadsheets.clear();
	}

	/**
	 * This method is responsible for importing information from xml file when
	 * the operation Import part of Pages/Selected Cells is requested
	 *
	 * @param focusOwner SpreadsheetTable to apply import
	 * @param numberPage number of page to import
	 * @throws FormulaCompilationException
	 * @throws FileNotFoundException
	 * @throws XMLStreamException
	 * @throws IllegalValueTypeException
	 */
	public void importPartOfPage(final SpreadsheetTable focusOwner,
								 int numberPage) throws FormulaCompilationException, FileNotFoundException, XMLStreamException, IllegalValueTypeException {

		XMLInputFactory factory = XMLInputFactory.newInstance();

		XMLStreamReader reader = factory.
			createXMLStreamReader(new FileInputStream(
					new File(f.getAbsolutePath())));

		String[][] data = new String[20][20];
		String text = null;
		String number = null;
		int row = 0;
		int column = 0;
		String value = "";

		while (reader.hasNext()) {
			int Event = reader.next();

			switch (Event) {

				case XMLStreamConstants.START_ELEMENT: {
					if (tags[0].equals(reader.getLocalName())) {
						//workbook
					}
					break;

				}

				case XMLStreamConstants.CHARACTERS: {

					text = reader.getText().trim();

					break;
				}
				case XMLStreamConstants.END_ELEMENT: {
					if (tags[1].equalsIgnoreCase(reader.getLocalName())) {
						//spreadsheet
					} else if (("spreadsheet_number").equalsIgnoreCase(reader.
						getLocalName())) {
						//spreadsheet number
						if (!text.equalsIgnoreCase("")) {
							number = text;

						}
					} else if (tags[3].equalsIgnoreCase(reader.getLocalName())) {
						//cell content
						if (!text.equalsIgnoreCase("")) {
							value = text;
						}
					} else if (tags[4].equalsIgnoreCase(reader.getLocalName())) {
						if (!text.equalsIgnoreCase("")) {
							//column cell
							column = Integer.
								parseInt(text);
						}
					} else if (tags[5].equalsIgnoreCase(reader.getLocalName())) {
						//row cell
						if (!text.equalsIgnoreCase("")) {
							row = Integer.parseInt(text);
						}
						data[row][column] = value;
					} else if (tags[2].equalsIgnoreCase(reader.getLocalName())) {
						//cell
					}
					break;
				}

			}
		}

		//Update selected cells in the spreadsheet choosed with imported data:
		if (numberPage != Integer.parseInt(number)) {
			JOptionPane.
				showMessageDialog(null, "Page number not found in file", "ERROR", ERROR_MESSAGE);
		} else {
			Cell[][] list = focusOwner.getSelectedCells();
			for (int i = 0; i < list.length; i++) {
				for (int j = 0; j < list[0].length; j++) {
					if (data[i][j] != null) {
						list[i][j].setContent(data[i][j]);

					}

				}
			}
		}
	}
}
