/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile;

import csheets.core.Spreadsheet;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 *
 * @author Sergio 1130711
 */
public class LinkingFileImport {

	/**
	 * LinkingFile list of the sheets
	 */
	private List<LinkingFile> linkedFiles = new ArrayList<LinkingFile>();
	/**
	 * User Interface Controller
	 */
	private UIController m_controller;

	/**
	 * Create a new LinkingFileImport
	 *
	 * @param controller user interface controller
	 */
	public LinkingFileImport(UIController controller) {
		this.m_controller = controller;

	}

	/**
	 * Add LinkingFile lf to linkedFiles
	 *
	 * @param lf LinkingFile to add
	 */
	public boolean addNewLinkingFile(LinkingFile lf) {
		if (getLinkedFiles().add(lf)) {
			return true;
		}
		return false;

	}

	/**
	 * Removes the LinkingFile and interrupts the respective thread to
	 * spreasheet given by parameter
	 *
	 * @param spreadsheet to apply modifications
	 */
	public void removeLinkedFile(Spreadsheet ss) {
		int index;
		try {
			for (LinkingFile fl : getLinkedFiles()) {
				if (fl.getSheet().equals(ss)) {
					index = getLinkedFiles().indexOf(fl);
					getLinkedFiles().get(index).getThread().interrupt();
					getLinkedFiles().remove(index);
				}
			}
		} catch (ConcurrentModificationException ce) {
			removeLinkedFile(ss);
		}

	}

	/**
	 *
	 * Check if the spreadsheet given by parameter is linked
	 *
	 * @param spreadsheet the spreadsheet to chek
	 * @return true if spreadsheet is already linked
	 */
	public boolean checkLinkedFile(Spreadsheet ss) {
		for (LinkingFile fl : getLinkedFiles()) {
			if (fl.getSheet() == ss) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the LinkinFile list
	 *
	 * @return the linkedFiles
	 */
	public List<LinkingFile> getLinkedFiles() {
		return linkedFiles;
	}

	/**
	 * Set LinkinFile list
	 *
	 * @param linkedFiles new LinkinFile list
	 */
	public void setLinkedFiles(
		List<LinkingFile> linkedFiles) {
		this.linkedFiles = linkedFiles;
	}
}
