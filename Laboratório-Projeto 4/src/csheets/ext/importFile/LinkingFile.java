/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importFile;

import csheets.core.Spreadsheet;
import csheets.ext.exportTextFile.ExportThreadRun;
import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 *
 * @author Sergio 1130711
 */
public class LinkingFile {

	/**
	 * Spreadsheet to apply
	 */
	private Spreadsheet sheet;
	/**
	 * File to link
	 */
	private File fileToLink;
	/**
	 * Header
	 */
	private boolean m_header = true;
	/**
	 * The separator cells in file
	 */
	private String separator = ";";
	/**
	 * Thread to do execute
	 */
	private Thread thread;
	/**
	 * User interface Controller
	 */
	private UIController m_controller;

	/**
	 * Create a new LinkingFile
	 */
	public LinkingFile() {

	}

	/**
	 * Create a new LinkingFile
	 *
	 * @param sheet spreadsheet to apply
	 * @param fileLink file to link
	 * @param header header
	 * @param separator separator cells in file
	 * @param controller user intereface controller
	 */
	public LinkingFile(Spreadsheet sheet, File fileLink, boolean header,
					   String separator, UIController controller) {
		this.sheet = sheet;
		this.fileToLink = fileLink;
		this.m_header = header;
		this.separator = separator;
		m_controller = controller;

	}

	/**
	 * Starts linking import
	 */
	public void startImport() {
		ImportThreadRun it = new ImportThreadRun(this, m_controller);
		Thread t = new Thread(it);
		setThread(t);
		t.start();
	}

	/**
	 * Starts linking export
	 */
	public void startExport() {
		ExportThreadRun et = new ExportThreadRun(this, m_controller);
		Thread t = new Thread(et);
		setThread(t);
		t.start();
	}

	/**
	 * Return spreadsheet
	 *
	 * @return spreadsheet
	 */
	public Spreadsheet getSheet() {
		return sheet;
	}

	/**
	 * Return file
	 *
	 * @return file
	 */
	public File getFile() {
		return fileToLink;
	}

	/**
	 * Return header
	 *
	 * @return
	 */
	public boolean getHeader() {
		return m_header;
	}

	/**
	 * Return separator
	 *
	 * @return
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * Return thread
	 *
	 * @return thread
	 */
	public Thread getThread() {
		return thread;
	}

	/**
	 * Set thread
	 *
	 * @param new thread
	 */
	public void setThread(Thread thread) {
		this.thread = thread;
	}

}
