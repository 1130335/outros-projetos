/**
 * Provides an example of how to build a import file extension. An extension is
 * a class that extends the class Extension. CleanSheets dynamically loads all
 * the extensions that it finds declared in the following property files:
 * res/extensions.props and extensions.props. For the import file extension to
 * be loaded at startup a line with the fully qualified name of the simple
 * extension class must be present in one of the property files. The fully
 * qualified name of the import file extension is:
 * csheets.ext.importFile.ExtensionImportFile
 *
 * <p>
 * <b>Class Diagram</b>
 * <p>
 * <img src="doc-files/ImportFile_extension_image1.png">
 * <p>
 * <b>Sequence Diagrams</b> illustrating the setup of the extension
 * <p>
 * The following sequence diagram illustrates the creation of the import file
 * extension. All the extensions are loaded dynamically by the ExtensionManager
 * at application startup. <br/><br/>
 * <img src="doc-files/ImportFile_extension_image2.png">
 *
 * <p>
 * The following sequence diagram illustrates the creation of the user interface
 * extension. All the UI extensions are loaded by the UIController at
 * application startup. <br/><br/>
 * <img src="doc-files/ImportFile_extension_image3.png">
 *
 * <p>
 * The following sequence diagram illustrates the creation of the menu
 * extension. All the menu extensions are loaded by the MenuBar at application
 * startup. <br/><br/>
 * <img src="doc-files/ImportFile_extension_image4.png">
 *
 * <p>
 * <b>Sequence Diagrams</b> illustrating use cases of the extension
 * <p>
 * The following sequence diagram illustrates the use case <b>"IPC04.1:
 * Import/Export File"</b>. <br/>
 * To be noticed that the operation actionPerformed does not originate directly
 * from the User. There are several other classes involved that are not depicted
 * for clarity purposes.
 * <br/><br/>
 * <img src="doc-files/ImportFile_extension_image5.png">
 *
 * @author 1121248DiogoVieira
 *
 */
/*
 @startuml doc-files/ImportFile_extension_image1.png
 class ImportFileAction {
 }
 class ImportFileMenu
 class ExtensionImportFile {
 -String NAME;
 }
 class UIExtensionImportFile
 class JMenuItem
 ExtensionImportFile -> UIExtensionImportFile : getUIExtension(UIController)
 UIExtensionImportFile -> ImportFileMenu : getMenu()
 ImportFileMenu -> JMenuItem : 'items'
 JMenuItem o-> ImportFileAction : action
 class ImportFileController
 ImportFileAction -> ImportFileController : import
 @enduml

 @startuml doc-files/ImportFile_extension_image2.png
 participant ExtensionManager as ExtM
 participant Class
 participant "aClass:Class" as aClass
 participant "extension : ExtensionImportFile" as EImportFile
 ExtM -> Class : aClass = forName("csheets.ext.importFile.ExtensionImportFile");
 activate Class
 create aClass
 Class -> aClass : new
 deactivate Class
 ExtM -> aClass : extension = (Extension)newInstance();
 activate aClass
 create EImportFile
 aClass -> EImportFile : new
 deactivate aClass
 ExtM -> EImportFile : name = getName();
 activate EImportFile
 deactivate EImportFile
 ExtM -> ExtM : extensionMap.put(name, extension)
 @enduml

 @startuml doc-files/ImportFile_extension_image3.png
 participant UIController as UIC
 participant ExtensionManager as ExtM
 participant "extension : ExtensionImportFile" as EImportFile
 participant "uiExtension : UIExtensionImportFile" as UIExt
 UIC -> ExtM : extensions=getExtensions();
 loop for Extension ext : extensions
 UIC -> EImportFile : uiExtension=getUIExtension(this);
 activate EImportFile
 create UIExt
 EImportFile -> UIExt : new
 deactivate EImportFile
 UIC -> UIC : uiExtensions.add(uiExtension);
 end
 @enduml

 @startuml doc-files/ImportFile_extension_image4.png
 participant MenuBar as MB
 participant "extensionsMenu : JMenu" as extensionsMenu
 participant UIController as UIC
 participant "extension : UIExtensionImportFile" as UIE
 participant "extensionMenu : ImportFileMenu" as EM
 MB -> MB : extensionsMenu = addMenu("Extensions", KeyEvent.VK_X);
 activate MB
 create extensionsMenu
 MB -> extensionsMenu : new
 deactivate MB
 MB -> UIC : extensions=getExtensions();
 loop for UIExtension extension : extensions
 MB -> UIE : extensionMenu=extension.getMenu();
 activate UIE
 create EM
 UIE -> EM : new
 deactivate UIE
 MB -> EM : icon = getIcon();
 MB -> extensionsMenu : add(extensionMenu);
 end
 @enduml

 @startuml doc-files/ImportFile_extension_image5.png
 actor User
 participant ImportFileAction as IFA
 participant ImportFileController as IFC
 participant ImportFile as IF
 participant FileChooser as chooser
 participant JOptionPane as JOption
 participant "this.uiController : UIController" as UIC
 participant "sheet : Spreadsheet" as ss
 participant "cell : Cell" as cell
 IFA -> IFC : importController=new ImportFileController(uiController);

 IFA -> IFA : file=newImport.FileChosser();
 IFA -> chooser : showOpenDialog(null);
 IFA -> IFC : setImport(file);
 IFC -> IF : newImport=new ImportFile(uiController);

 IFC -> IF : newImport.FileRead(newFile);
 IF -> UIC : activeCell=getActiveCell();
 IF -> UIC : activeCell.getAddress();
 IF -> UIC : sheet=getActiveSpreadsheet();
 IF -> sheet : getCell(indexX, indexY);
 IF -> cell : setContent(values[i]);
 IF -> IFC : return(true);
 IFC -> IFA : return(true);


 IFA -> JOption : result=showMessageDialog(...)
 @enduml

 */
package csheets.ext.importFile;
