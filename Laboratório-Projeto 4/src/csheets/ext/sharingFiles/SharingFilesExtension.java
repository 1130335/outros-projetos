package csheets.ext.sharingFiles;

import csheets.ext.Extension;
import csheets.ext.sharingFiles.ui.SharingFilesUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * SharingFilesExtension
 *
 * @author 1130709
 *
 */
public class SharingFilesExtension extends Extension {

	/**
	 * Extension's Name
	 */
	public static final String NAME = "Sharing Files";

	/**
	 * Creates a new extension.
	 */
	public SharingFilesExtension() {
		super(NAME);
	}

	/**
	 * Returns user interface extension.
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		return new SharingFilesUI(this, uiController);
	}

}
