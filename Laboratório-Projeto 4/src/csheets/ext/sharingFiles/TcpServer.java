package csheets.ext.sharingFiles;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TcpServer - create server
 *
 * @author 1130709
 */
public class TcpServer {

	ServerSocket serverSocket;
	Thread thread;
	ArrayList<File> files;

	/**
	 * Method thats starts the server
	 *
	 * @param port defined port that stay wainting for other instance to connect
	 * @param f ArrayList of File that matches the files to share
	 * @throws java.io.IOException
	 */
	public void startSF(int port, ArrayList<File> f) throws IOException {
		files = f;
		serverSocket = new ServerSocket(port);
		thread.start();
	}

	/**
	 * Method to instance TcpServer, basically using a Socket we can share the
	 * files writting it on and ObjectOutputStream instance.
	 *
	 * @param f ArrayList of files
	 */
	public TcpServer(ArrayList<File> f) {
		files = f;
		thread = new Thread(new Runnable() {
			@Override
			public void run() {

				while (true) {

					Socket socket;
					try {
						socket = serverSocket.accept();
						ObjectOutputStream outputStream = new ObjectOutputStream(socket.
							getOutputStream());
						outputStream.writeObject(files);
						outputStream.flush();
						outputStream.close();
					} catch (IOException ex) {
						Logger.getLogger(TcpServer.class.getName()).
							log(Level.SEVERE, null, ex);
					}

				}
			}
		});
	}

	/**
	 * Method that sets the new Array of Files
	 *
	 * @param files New Array of Files
	 */
	void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	/**
	 * Method that Returns the files
	 *
	 * @return files Array of Files
	 */
	public ArrayList<File> getFiles() {
		return this.files;
	}
}
