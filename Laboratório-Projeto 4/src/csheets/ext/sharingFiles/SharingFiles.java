/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sharingFiles;

import java.io.File;

/**
 *
 * @author i130486 - Pedro Tavares
 */
public class SharingFiles extends File {

	/**
	 * Variable that stores the file
	 */
	private final File file;

	/**
	 * Constructor to create the SharingFiles instance
	 *
	 * @param file The file itself
	 * @param string The name of the file
	 */
	public SharingFiles(File file, String string) {
		super(file, string);
		this.file = file;
	}

	/**
	 * Method that return the file
	 *
	 * @return file The file
	 */
	public File getFile() {
		return this.file;
	}

	/**
	 * Method that returns the file's size
	 *
	 * @return file.lenght The size of the file
	 */
	public long getFileLenght() {
		return this.file.length();
	}

	/**
	 * Method that returns the info of the Sharing File
	 *
	 * @return String with FIle information
	 */
	@Override
	public String toString() {
		return file.getName() + " - " + file.length() + " Kbytes";
	}

}
