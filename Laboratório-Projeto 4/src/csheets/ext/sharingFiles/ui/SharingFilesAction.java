package csheets.ext.sharingFiles.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;

/**
 * Creates Sharing Files Frame
 *
 * @author 1130709
 */
public class SharingFilesAction extends BaseAction {

    protected UIController uiController;
    JFrame jFsharingFiles;

    public SharingFilesAction(UIController uiController) {
        this.uiController = uiController;
    }
    
    @Override
    protected String getName() {
        return "Sharing Files";
    }

    @Override
    protected void defineProperties() {
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        
        /**
         * Create JFrame
         */
        jFsharingFiles = new SharingFilesFrame(uiController);
        jFsharingFiles.setVisible(true);
        jFsharingFiles.setLocationRelativeTo(null);
        

    }

}
