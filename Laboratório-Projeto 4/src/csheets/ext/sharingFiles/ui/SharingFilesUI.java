/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sharingFiles.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 * SharingFilesUI creates extension on Menu
 *
 * @author 1130709
 */
public class SharingFilesUI extends UIExtension {

	private SharingFilesMenu menu;

	public SharingFilesUI(Extension extension, UIController uiController) {
		super(extension, uiController);
	}

	@Override
	public JMenu getMenu() {
		if (menu == null) {
			menu = new SharingFilesMenu(uiController);
		}
		return menu;

	}

}
