package csheets.ext.sharingFiles.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Creates extension on Menu
 * 
 * @author 1130709
 */
public class SharingFilesMenu extends JMenu {

	
	public SharingFilesMenu(UIController uiController) {
		super("Sharing Files");
		setMnemonic(KeyEvent.VK_M);

		//Add extension
		add(new SharingFilesAction(uiController));
	}	  
        
        
    
}
