/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sharingFiles;

import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import static java.math.BigInteger.valueOf;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 *
 * @author 1130486 - Pedro Tavares
 */
public class DownloadController {

	/**
	 * UIController Variable
	 */
	private final UIController uiCont;
	/**
	 * The file to be downloaded
	 */
	private File file;
	private SharingFilesController sharingFilesController;

	/**
	 * Construtor for the DownloadController
	 *
	 * @param ui
	 */
	public DownloadController(UIController ui) {
		this.uiCont = ui;
		this.file = null;
	}

	/**
	 * Method encharged to Download the file
	 *
	 * @param path The path were the file will be saved
	 * @param file The file to be downloaded
	 * @throws java.io.FileNotFoundException
	 */
	public void Download(String path, File file) throws FileNotFoundException, IOException {
		this.file = file;
		//Frame, Panel and Progress Bar creation
		final JFrame frame = new JFrame("Download State");
		JPanel pane = new JPanel(new BorderLayout(1, 1));
		JProgressBar progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
		progressBar.setPreferredSize(new Dimension(250, 40));
		JPanel panel = new JPanel();
		panel.add(progressBar);
		panel.setPreferredSize(new Dimension(350, 55));
		pane.add(panel, BorderLayout.CENTER);
		frame.getContentPane().add(pane);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		//-----//---------------

		//File Creation with inbox path
		File newFile = new File(path);

		if (!newFile.exists()) {
			ContinueDownload(newFile, progressBar);
		} else {
			JOptionPane.
				showMessageDialog(null, "You already Downloaded that file.\n You check your recent downloads at " + path);
			frame.setVisible(false);
		}
		//Frame windows Listener - dispose window
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
			}
		});
		pane.setPreferredSize(new Dimension(250, 100));
	}

	/**
	 * Method encharged to Update the file
	 *
	 * @param savePath
	 * @param path The path were the file will be saved
	 * @param file The file to be downloaded
	 * @throws java.io.IOException
	 */
	public void Update(String savePath, String path, File file) throws IOException {
		this.file = file;

		Object[] options = {"Overwrite", "Create New"};
		int n = JOptionPane.
			showOptionDialog(null, "What do you want to do?", "Choose an option", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		//OverWritesFile!!
		if (n == 0) {
			JOptionPane.showMessageDialog(null, "Not implemented.");

			//This is supposed to work with the Folder Watcher, if Someone modified the shared data everyone should download the new content but it's not possible due to week3 implementation.
			//CREATES NEW FILE!
		} else if (n == 1) {

			String dotExtension;
			String allPathExceptExtension;
			if (path == null
				|| path.length() < 4) {
				dotExtension = path;
			} else { //in case the
				//file already exist a new file is created with diferent name
				dotExtension = path.substring(path.length() - 4);
				allPathExceptExtension = path.substring(0, path.length() - 4);

				String newPath = allPathExceptExtension.concat("(version2)").
					concat(dotExtension);
				File file2 = new File(newPath);
				ContinueDownloadWithoutBar(file2);
			}

		}
	}

	/**
	 * Method that DOwnloads the file without a Progress Bar
	 *
	 * @param file File to be downloaded
	 * @throws IOException
	 */
	public void ContinueDownloadWithoutBar(File file) throws IOException {
		file.getParentFile().mkdirs();
		file.createNewFile();
		FileOutputStream f = new FileOutputStream(file);
		FileInputStream fileInputStream = new FileInputStream(this.file);
		int maxBuffer = 512;
		int remainingBytes = fileInputStream.available(); //remaining bytes to be read
		int bufferSize = Math.min(remainingBytes, maxBuffer);
		byte[] FileBytes = new byte[bufferSize]; //size in bytes

		//Read File
		int bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);

		//until there's no more bytes to read (fills the progress bar with percentage)
		while (bytesRead > 0) {
			f.write(FileBytes, 0, bytesRead);
			remainingBytes = fileInputStream.available();
			bufferSize = Math.min(remainingBytes, maxBuffer);
			FileBytes = new byte[bufferSize];
			bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);
		}
		//Close stream
		fileInputStream.close();
		f.flush();
		f.close();
	}

	/**
	 * Method that Continues the download with a progress Bar
	 *
	 * @param newFile File to be downloaded
	 * @param progressBar The Download Bar
	 * @throws IOException
	 */
	public void ContinueDownload(File newFile, JProgressBar progressBar) throws IOException {
		newFile.getParentFile().mkdirs();
		newFile.createNewFile();
		FileOutputStream f = new FileOutputStream(newFile);
		FileInputStream fileInputStream = new FileInputStream(file);
		int maxBuffer = 512;
		int remainingBytes = fileInputStream.available(); //remaining bytes to be read
		int bufferSize = Math.min(remainingBytes, maxBuffer);
		byte[] FileBytes = new byte[bufferSize]; //size in bytes

		//Read File
		int bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);
		BigInteger progressPercentage = BigInteger.valueOf(0);
		BigInteger mult100 = BigInteger.valueOf(100);
		BigInteger length = BigInteger.valueOf(file.length());
		BigInteger last;

		//until there's no more bytes to read (fills the progress bar with percentage)
		while (bytesRead > 0) {
			progressPercentage = progressPercentage.add(valueOf(bytesRead));
			f.write(FileBytes, 0, bytesRead);
			remainingBytes = fileInputStream.available();
			last = ((progressPercentage.multiply(mult100)).divide(length));
			int valueBar = last.intValue();
			progressBar.setValue(valueBar);
			bufferSize = Math.min(remainingBytes, maxBuffer);
			FileBytes = new byte[bufferSize];
			bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);
		}

		//Close stream
		fileInputStream.close();
		progressBar.setValue(100);
		f.flush();
		f.close();
	}
}
