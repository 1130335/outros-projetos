package csheets.ext.sharingFiles;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * SharingFilesController, the controller of user story It has the main methods
 * to create a share and acess a share by usin other important Classes TcpServer
 * and TcpClient
 *
 * @author 1130709
 *
 */
public class SharingFilesController extends Extension {

	public static final String NAME = "Sharing Files";
	protected UIController uiController;
	int port;
	String path;
	ArrayList<File> files;
	ArrayList<SharingFiles> sharFiles;
	TcpServer server;

	public SharingFilesController(UIController uiCont) {
		super(NAME);
		files = new ArrayList<File>();
		sharFiles = new ArrayList<>();
		port = 44900;
	}

	/**
	 * Responsible for initiating the share it will create an TcpServer object
	 * passing by parameter the ArrayList returned by Search()
	 *
	 * @param port
	 * @throws IOException
	 */
	public void startSharing(int port) throws IOException {
		server = new TcpServer(Search());
		server.startSF(port, Search());
	}

	/**
	 * This method will return an ArrayList of files that it gets from a defined
	 * path
	 *
	 * @return ArrayList list of files to share
	 */
	public ArrayList<File> Search() {
		ArrayList<File> results = new ArrayList<File>();
		File[] files = new File(path).listFiles();

		for (File file : files) {
			results.add(file);
		}

		return results;
	}

	/**
	 * Responsible for accessing a created share It creates an object of
	 * TcpClient passing by parameter the ip to access the share Then it will
	 * save on arraylist 'files' the arraylist returned from getFilesShare()
	 * from TcpClient
	 *
	 * @param ip
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void accessSharing(String ip) throws UnknownHostException, IOException, ClassNotFoundException {
		TcpClient cliente = new TcpClient(port, ip);
		files = cliente.getFilesShare();
	}

	/**
	 * Get port
	 *
	 * @return port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Set port
	 *
	 * @param port
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Set path of folder's dir.
	 *
	 * @param path folder's directory
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Method that sets a new Array of files
	 *
	 * @param files new files
	 */
	public void setFiles(ArrayList<File> files) {
		//server.setFiles(files);
		this.files = files;
	}

	/**
	 * Get folder's path
	 *
	 * @return path
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * Get the ArrayList of files
	 *
	 * @return ArrayList of File
	 */
	public ArrayList<File> getFiles() {
		return files;
	}

	/**
	 * Get the ArrayList of Sharedfiles
	 *
	 * @return ArrayList of Sharedfiles
	 */
	public ArrayList<SharingFiles> getSharedFiles() {
		return sharFiles;
	}

	/**
	 * Method that updates the content of the files array
	 *
	 * @return newFiles
	 */
	public ArrayList<SharingFiles> UpdateList() {
		ArrayList<SharingFiles> newFiles = new ArrayList<>();
		for (File fich : files) {
			SharingFiles sharedFile = new SharingFiles(fich, fich.getName());
			newFiles.add(sharedFile);
		}
		return newFiles;
	}
}
