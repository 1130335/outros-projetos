package csheets.ext.sharingFiles;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * TcpClient - create client
 *
 * @author 1130709
 */
public class TcpClient {

	private int port;
	private String ip;

	/**
	 * Method to instance TcPClient
	 *
	 * @param port port to access share
	 * @param ip IP adress to access share
	 */
	public TcpClient(int port, String ip) {
		this.port = port;
		this.ip = ip;
	}

	/**
	 * Method that by the socket receives the shared elements
	 *
	 * @return ArrayList of Files
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<File> getFilesShare() throws UnknownHostException, IOException, ClassNotFoundException {
		Socket socket = new Socket(ip, port);
		ObjectInputStream objectInput = new ObjectInputStream(socket.
			getInputStream());
		ArrayList<File> filesShare = (ArrayList<File>) objectInput.readObject();
		objectInput.close();
		return filesShare;
	}
}
