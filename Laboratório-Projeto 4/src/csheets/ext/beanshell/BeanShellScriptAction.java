/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import csheets.ext.beanshell.ui.BeanShellScriptUI;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author João Cabral
 */
public class BeanShellScriptAction extends BaseAction {

	/*
	 * Instance of UIController.
	 */
	public static UIController m_uiController;

	/*
	 * Constructor of BeanShellScriptAction.
	 */
	public BeanShellScriptAction(UIController uiController) {
		m_uiController = uiController;
	}

	@Override
	protected String getName() {
		return "Scripts Option";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BeanShellScriptUI beanShellScriptUI = new BeanShellScriptUI();
		beanShellScriptUI.setVisible(true);
		beanShellScriptUI.setLocationRelativeTo(null);
	}
}
