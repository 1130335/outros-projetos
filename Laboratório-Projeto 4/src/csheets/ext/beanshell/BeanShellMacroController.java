/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

/**
 *
 * @author João Cabral
 */
public class BeanShellMacroController {

	/*
	 * Object of the interpreter of BeanShell scripts.
	 */
	private BeanShellInterpreter m_BSInterpreter;

	/**
	 * Constructor of the controller.
	 */
	public BeanShellMacroController() {
		m_BSInterpreter = new BeanShellInterpreter();
	}

	public BeanShellInterpreter getInterpreter() {
		return m_BSInterpreter;
	}
}
