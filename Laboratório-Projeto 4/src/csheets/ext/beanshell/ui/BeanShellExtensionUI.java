/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell.ui;

import csheets.ext.Extension;
import csheets.ext.beanshell.BeanShellMenu;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 *
 * @author João Cabral
 */
public class BeanShellExtensionUI extends UIExtension {

	/**
	 * Instance of BeanShellMenu
	 */
	private BeanShellMenu window;

	public BeanShellExtensionUI(Extension extension, UIController uiController) {
		super(extension, uiController);
	}

	/**
	 * PT : Retorna uma instância da classe que implementa o JMenu.
	 */
	@Override
	public JMenu getMenu() {
		if (window == null) {
			window = new BeanShellMenu(uiController);
		}

		return window;
	}
}
