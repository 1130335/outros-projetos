/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import csheets.ext.beanshell.ui.BeanShellMacroUI;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author João Cabral
 */
public class BeanShellMacroAction extends BaseAction {

	/*
	 * Instance of UIController.
	 */
	public static UIController m_uiController;

	/*
	 * Constructor of BeanShellMacroAction.
	 */
	public BeanShellMacroAction(UIController uiController) {
		m_uiController = uiController;
	}

	@Override
	protected String getName() {
		return "Macros Option";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BeanShellMacroUI beanShellMacroUI = new BeanShellMacroUI();
	}

}
