/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author João Cabral
 */
public class BeanShellMenu extends JMenu {

	public BeanShellMenu(UIController uiController) {
		super("BeanShell");
		setMnemonic(KeyEvent.VK_B);
		add(new BeanShellScriptAction(uiController));
		add(new BeanShellMacroAction(uiController));
	}
}
