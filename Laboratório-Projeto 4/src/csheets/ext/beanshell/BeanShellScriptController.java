/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import bsh.EvalError;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class BeanShellScriptController {

	/*
	 * Object of the interpreter of BeanShell scripts.
	 */
	private BeanShellInterpreter m_BSInterpreter;

	/**
	 * Constructor of the controller.
	 */
	public BeanShellScriptController() {
		m_BSInterpreter = new BeanShellInterpreter();
	}

	/**
	 * Method that loads the script to the interpreter.
	 *
	 * @param file name of the file that contains the script
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws bsh.EvalError
	 */
	public void loadScript(String file) throws IOException, FileNotFoundException, EvalError {
		m_BSInterpreter.getInterpreter(file);
	}

	/**
	 * Method that runs the script indicated.
	 *
	 * @param name name of the script
	 */
	public void executeScript(String name) {
		m_BSInterpreter.executeScript(name);
	}

	/**
	 * Method that returns the body of a script
	 *
	 * @param name name of the script
	 * @return body of the script
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String getScriptContent(String name) throws FileNotFoundException, IOException {
		return m_BSInterpreter.getScriptContent(name);
	}

	/**
	 * Method that replaces the original file of an edited script for a new one.
	 *
	 * @param body body do script
	 * @param name name of the script
	 */
	public void replaceScript(String body, String name) {
		m_BSInterpreter.replaceScript(body, name);
	}

	/**
	 * Method that saves a script in a file.
	 *
	 * @param body body of the script
	 */
	public void saveNewScript(String body) {
		m_BSInterpreter.saveNewScript(body);
	}

	/**
	 * Method that deletes the file that contains a script.
	 *
	 * @param name name of the script to delete
	 * @return true if file deleted; false if file is not deleted.
	 */
	public boolean deleteScript(String name) {
		return m_BSInterpreter.deleteScript(name);
	}

	/**
	 * Method that gets the name of a script from its body.
	 *
	 * @param body body of the script
	 * @return name of the script
	 */
	public String getNameFromBody(String body) {
		return m_BSInterpreter.getNameFromBody(body);
	}

	/**
	 * Method that returns the list with the stored scripts.
	 *
	 * @return name of stored scripts
	 */
	public List<String> getScriptsList() {
		return m_BSInterpreter.getScriptsList();
	}
}
