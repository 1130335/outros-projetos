/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.util.JConsole;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author João Cabral
 */
public class BeanShellInterpreter extends Interpreter {

//    private final String path_to_scripts = "/*/BeanShellScripts"; 
    private final String path_to_scripts = "./BeanShellScripts/";
    
    /**
     * Interpreter console.
     */
    private static final JConsole console = new JConsole();

    /*
     * Instance of Interpreter.
     */
    private static final Interpreter m_interpreter = new Interpreter(console);

    /*
     * Constructor of BeanShellInterpreter.
     */
    public BeanShellInterpreter() {
    }

    public Object getInterpreter(String file) throws IOException, FileNotFoundException, EvalError {
        return m_interpreter.source(path_to_scripts + file);
    }

    /**
     * Method that runs the script indicated.
     *
     * @param name name of the script
     */
    public void executeScript(String name) {
        try {
            this.eval(name + "();");
        } catch (EvalError ee) {
            try {
                BeanShellInterpreter.m_interpreter.
                        source(path_to_scripts + name + ".bsh");
                BeanShellInterpreter.m_interpreter.eval(name + "();");
            } catch (IOException | EvalError ex1) {
                Logger.getLogger(BeanShellScriptController.class.getName()).
                        log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * Method that returns the body of a script
     *
     * @param name name of the script
     * @return body of the script
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String getScriptContent(String name) throws FileNotFoundException, IOException {
        File file = new File(path_to_scripts + name);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        String body = "";

        while ((line = reader.readLine()) != null) {
            body += line + "\n";
        }

        return body;
    }

    /**
     * Method that saves a script in a file.
     *
     * @param body body of the script
     */
    public void saveNewScript(String body) {
        try {
            File theFile = new File(path_to_scripts);
            theFile.mkdirs();
            String name = this.getNameFromBody(body);
            File file = new File(path_to_scripts + name + ".bsh");
            FileWriter fw = new FileWriter(file);
            fw.write(body);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(BeanShellScriptController.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @param body body do script
     * @param name name of the script
     */
    public void replaceScript(String body, String name) {
        try {
            File originalFile = new File(path_to_scripts + name);
            originalFile.delete();
            File file = new File(path_to_scripts + name);
            FileWriter fw = new FileWriter(file);
            fw.write(body);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(BeanShellScriptController.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method that deletes the file that contains a script.
     *
     * @param name name of the script to delete
     * @return true if file deleted; false if file is not deleted.
     */
    public boolean deleteScript(String name) {
        File file = new File(path_to_scripts + name);
        return file.delete();
    }

    /**
     * Method that gets the name of a script from its body.
     *
     * @param body body of the script
     * @return name of the script
     */
    public String getNameFromBody(String body) {
        String aux[] = body.split("\\(");
        return aux[0];
    }

    /**
     * Method that returns the list with the stored scripts.
     *
     * @return name of stored scripts
     */
    public List<String> getScriptsList() {
        File theFile = new File(path_to_scripts);
        theFile.mkdirs();

        List<String> list = new ArrayList();
        File f = new File(path_to_scripts);

        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".bsh");
            }
        };
        File[] files = f.listFiles(filter);
        for (File file : files) {
            list.add(file.getName());
        }

        return list;
    }

    /*
     * Method that executes the macro expression.
     *
     */
    public String executeMacro(String macro) throws EvalError {
        try {
            m_interpreter.eval("MACRO=" + macro);
        } catch (EvalError evlx) {
            return macro;
        }

        return m_interpreter.get("MACRO").toString();
    }
}
