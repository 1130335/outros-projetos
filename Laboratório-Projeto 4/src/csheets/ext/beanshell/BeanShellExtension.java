/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import csheets.ext.Extension;
import csheets.ext.beanshell.ui.BeanShellExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author João Cabral
 */
public class BeanShellExtension extends Extension {

	/*
	 * Name of the extension.
	 */
	public static final String NAME = "BeanShell";

	/**
	 * Constructor of BeanShellExtension.
	 */
	public BeanShellExtension() {
		super(NAME);
	}

	/**
	 * Returns a new instance of BeanShellExtensionUI.
	 *
	 * @return instance of BeanShellExtensionUI
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		return new BeanShellExtensionUI(this, uiController);
	}
}
