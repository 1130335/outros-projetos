/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.SearchText.Application;

import csheets.core.Cell;
import csheets.ext.core.SearchText.SearchText;
import csheets.ui.ctrl.UIController;
import java.util.List;

/**
 *
 * @author André Garrido - 1101598
 */
public class SearchTextController {

	private final UIController uiController;

	private SearchText sT;

	public SearchTextController(UIController uiController) {
		this.uiController = uiController;
	}

	public void search(String text) {
		sT = new SearchText();
		sT.Search(uiController.getActiveSpreadsheet(), text);
	}

	public List<Cell> getResultList() {
		return sT.getSearchResult();
	}
}
