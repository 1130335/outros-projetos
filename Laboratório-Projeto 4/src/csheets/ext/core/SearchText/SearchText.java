/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.SearchText;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author André Garrido - 1101598
 */
public class SearchText {

	Spreadsheet spreadsheet;
	ArrayList<Cell> results = new ArrayList<>();

	public SearchText() {
	}

	/**
	 *
	 * Code to Search for cells that contains the text inserted by the user This
	 * method fills a list of Cells.
	 *
	 *
	 * @param ss Active Spreadsheet
	 * @param text word to search
	 */
	public void Search(Spreadsheet ss, String text) {

		/**
		 * A regular expression, specified as a string, must first be compiled
		 * into an instance of this class The resulting pattern can then be used
		 * to create a Matcher object that can match arbitrary character
		 * sequences against the regular expression.
		 */
		Pattern pattern = Pattern.compile(text);
		this.spreadsheet = ss;
		int col_count = ss.getColumnCount();
		int row_count = ss.getRowCount();
		/**
		 * loop all the cells from spreadsheet from 0,0 to col_count,row_count.
		 */
		for (int i = 0; i < row_count; i++) {
			for (int j = 0; j < col_count; j++) {
				Cell c = ss.getCell(i, j);
				String cell_content = c.getContent();
				Matcher matcher = pattern.matcher(cell_content);
				if (matcher.matches()) {
					results.add(c);
				}
			}
		}
	}

	/**
	 *
	 * Method to return a List of cells using the search from the method above.
	 *
	 * @return a list of cells.
	 */
	public ArrayList<Cell> getSearchResult() {
		return results;
	}

}
