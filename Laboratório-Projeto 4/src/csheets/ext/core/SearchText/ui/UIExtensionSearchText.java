/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.SearchText.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author André Garrido - 1101598
 */
public class UIExtensionSearchText extends UIExtension {

	private JComponent sideBar;

	public UIExtensionSearchText(Extension extension, UIController uiController) {
		super(extension, uiController);
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	/**
	 * Returns an instance of a class that implements JMenu. In this simple case
	 * this class only supplies one menu option.
	 *
	 * @see ExampleMenu
	 * @return a JMenu component
	 */
	@Override
	public JMenu getMenu() {
		return null;
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	@Override
	public JToolBar getToolBar() {
		return null;
	}

	/**
	 * Returns a side bar that provides Search Text.
	 *
	 * @return a side bar
	 */
	@Override
	public JComponent getSideBar() {
		if (sideBar == null) {
			sideBar = new SearchTextPanel(uiController);
		}
		return sideBar;
	}
}
