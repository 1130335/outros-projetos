/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.SearchText;

import csheets.ext.Extension;
import csheets.ext.core.SearchText.ui.UIExtensionSearchText;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author André Garrido - 1101598
 */
public class SearchTextExtension extends Extension {

	public static final String NAME = "SearchText";

	public SearchTextExtension() {
		super(NAME);
	}

	@Override
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionSearchText(this, uiController);
	}
}
