/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.controller;

import csheets.core.Cell;
import csheets.ext.core.Email.domain.Email;
import csheets.ext.core.Email.domain.EmailConfigurationProperties;
import csheets.ext.core.Email.domain.EmailSMTP;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.Properties;
import javax.mail.MessagingException;

/**
 *
 * @author João Cabral
 */
public class SendEmailController {

	/* Instance of SpreadsheetTable. */
	private SpreadsheetTable m_ssheetTable;
	private Properties m_props;

	private EmailSMTP m_SMTP;
	private Email m_email;

	/* Default constructor. */
	public SendEmailController(SpreadsheetTable focusOwner) {
		m_ssheetTable = focusOwner;
		m_props = EmailConfigurationProperties.importEmailPropertiesFile();
		m_email = new Email();
	}


	/* Method that sends the email.
	 * @throws MessagingException if an error occurs while sending the email. */
	public void sendEmail(String subject) throws MessagingException {
		m_email.importEmailPropertiesFile();
		String[] str_email = m_email.fromPropertiesToString(this.m_props);
		m_SMTP.sendEmail(str_email, subject, m_props);
	}

	/* Method that prepares an email to be sent.
	 * @param receivers.
	 * @param email subject.
	 * @param email body.
	 * @param cells optional cell content
	 */
	public void setEmail(String to, String subject, String message,
						 Cell[][] cells) {
		m_SMTP = new EmailSMTP(new Email(to, subject, message, cells), m_ssheetTable);
	}

}
