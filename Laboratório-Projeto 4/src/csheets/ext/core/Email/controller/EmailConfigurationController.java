/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.controller;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.ext.core.Email.domain.Email;
import csheets.ui.ctrl.UIController;
import javax.mail.AuthenticationFailedException;

/**
 *
 * @author coelho
 */
public class EmailConfigurationController{
    
    private Email email;
    
    public EmailConfigurationController(){
        this.email = new Email();
    }

    public String[] importEmailPropertiesFile() {
        return this.email.importEmailPropertiesFile();
    }

    public void exportEmailPropertiesFile(String[] email_props) {
        this.email.exportEmailPropertiesFile(email_props);
    }

    public void sendTestEmail(Cell[][] active_cells) throws AuthenticationFailedException {
        this.email.sendTestEmail(active_cells);
    }
    
}
