/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.ext.Extension;
import csheets.ext.core.Email.ui.SendEmailExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author João Cabral
 */
public class SendEmailExtension extends Extension {

	/*
	 * Name of the extension.
	 */
	public static final String NAME = "Email";

	public SendEmailExtension(String name) {
		super(name);
	}

	/**
	 * Constructor of SendEmailExtension.
	 */
	public SendEmailExtension() {
		super(NAME);
	}

	/**
	 * Returns a new instance of SendEmailExtensionUI.
	 *
	 * @return instance of BeanShellExtensionUI
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		return new SendEmailExtensionUI(this, uiController);
	}

}
