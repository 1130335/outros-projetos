/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.core.Cell;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author coelho
 */
public class EmailSMTP {

	/**
	 * Instance of Email.
	 */
	private Email m_email;

	private SpreadsheetTable m_focusOwner;

	public EmailSMTP(Email email, SpreadsheetTable focusOwner) {
		m_email = email;
		m_focusOwner = focusOwner;
	}

	static void sendTestEmail(String[] string_email, Cell[][] active_cells) throws AuthenticationFailedException {
		final String email = string_email[0];
		final String password = string_email[1];
		final String host = string_email[2];
		final String port = string_email[3];

		String message_text = createMessage(active_cells);

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);

		Session session = Session.getInstance(props,
											  new javax.mail.Authenticator() {
												  @Override
												  protected PasswordAuthentication getPasswordAuthentication() {
													  return new PasswordAuthentication(email, password);
												  }
											  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.
								  parse(email));
			message.setSubject("Test Email");
			message.setText(message_text);

			Transport.send(message);

		} catch (javax.mail.AuthenticationFailedException e) {
			throw new javax.mail.AuthenticationFailedException("Email failed");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/* Send the email.
	 * @throws MessagingException
	 */
	public void sendEmail(String[] string_email, String subject,
						  Properties properties) throws MessagingException {
		final String email = string_email[0];
		final String password = string_email[1];
		final String host = string_email[2];
		final String port = string_email[3];

		Properties props = properties;
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);

		Session session = Session.
			getInstance(props, new javax.mail.Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(email, password);
				}
			});

		String message_text = createMessage(m_focusOwner.getSelectedCells());

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(email));
		message.setSubject(subject);

		String emailTo = m_email.getEmails();
		message.
			addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));

		BodyPart messageBodyPart = new MimeBodyPart();

		messageBodyPart.
			setText(m_email.getMessage() + "\n\nSelected cells content:\n\n" + message_text);

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		message.setContent(multipart);
		Transport.send(message);

	}

	private static String createMessage(Cell[][] active_cells) {

		String cells_content = "";

		for (int row = 0; row < active_cells.length; row++) {
			for (int collum = 0; collum < active_cells[0].length; collum++) {
				cells_content += active_cells[row][collum].getContent();
				if (collum != active_cells[0].length - 1) {
					cells_content += ";;";
				}
			}
			if (row != active_cells.length - 1) {
				cells_content += "\n";
			};
		}

		return cells_content;
	}

}
