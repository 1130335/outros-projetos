/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author João Cabral
 */
public class SendEmailMenu extends JMenu {

	public SendEmailMenu(UIController uiController) {
		super("Email");
		setMnemonic(KeyEvent.VK_B);
		add(new SendEmailAction(uiController));
	}
}
