/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.core.Cell;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;

/**
 *
 * @author coelho
 */
public class Email {

	private Properties eProps;

	/* Array of strings that stores emails. */
	private String m_email;

	/* Email subject. */
	private String m_subject;

	/* Email message. */
	private String m_message;

	/* Email selected cells. */
	private Cell[][] m_cells;

	public Email() {
	}

	public Email(String emails, String subject, String message) {
		this.eProps = EmailConfigurationProperties.importEmailPropertiesFile();
		m_email = emails;
		m_subject = subject;
		m_message = message;
	}

	public Email(String emails, String subject, String message, Cell[][] cells) {
		this.eProps = EmailConfigurationProperties.importEmailPropertiesFile();
		m_email = emails;
		m_subject = subject;
		m_message = message;
		m_cells = cells;
	}

	public String[] importEmailPropertiesFile() {

		this.eProps = EmailConfigurationProperties.importEmailPropertiesFile();

		return fromPropertiesToString(this.eProps);
	}

	public void exportEmailPropertiesFile(String[] email_props) {
		this.eProps = fromStringToProperties(email_props);

		EmailConfigurationProperties.exportEmailPropertiesFile(eProps);
	}

	public String[] fromPropertiesToString(Properties props) {

		String[] info = new String[4];

		info[0] = this.eProps.getProperty("email");
		info[1] = this.eProps.getProperty("password");
		info[2] = this.eProps.getProperty("host");
		info[3] = this.eProps.getProperty("port");

		return info;

	}

	private Properties fromStringToProperties(String[] email_props) {

		this.eProps.setProperty("email", email_props[0]);
		this.eProps.setProperty("password", email_props[1]);
		this.eProps.setProperty("host", email_props[2]);
		this.eProps.setProperty("port", email_props[3]);

		return this.eProps;
	}

	public void sendTestEmail(Cell[][] active_cells) throws AuthenticationFailedException {
		String[] str_email = fromPropertiesToString(this.eProps);
		EmailSMTP.sendTestEmail(str_email, active_cells);
	}

	public String getSubject() {
		return m_subject;
	}

	public Properties getEProps() {
		return eProps;
	}

	public String getEmails() {
		return m_email;
	}

	public String getMessage() {
		return m_message;
	}

	public Cell[][] getCells() {
		return m_cells;
	}

	public void setEmails(String m_emails) {
		this.m_email = m_emails;
	}

	public void setSubject(String m_subject) {
		this.m_subject = m_subject;
	}

	public void setMessage(String m_message) {
		this.m_message = m_message;
	}

	public void setFile(Cell[][] cells) {
		this.m_cells = cells;
	}

	/* Represent the object as text.
	 * @return the string representation.
	 */
	@Override
	public String toString() {
		return "Email{"
			+ "email=" + (m_email).toString()
			+ ", message='" + m_message + '\''
			+ ", subject='" + m_subject + '\''
			+ ", cells='" + m_cells + '\''
			+ '}';
	}

}
