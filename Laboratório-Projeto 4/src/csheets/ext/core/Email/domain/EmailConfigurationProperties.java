/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author coelho
 */
public class EmailConfigurationProperties {

	private final static String PROPS_FILEPATH = "../src-resources/csheets/ext/email/emailConfig.properties";

	public static Properties importEmailPropertiesFile() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(PROPS_FILEPATH);

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
				}
			}
		}

		return prop;
	}

	static void exportEmailPropertiesFile(Properties eProps) {
		File file = null;
		OutputStream output = null;

		try {
			file = new File(PROPS_FILEPATH);
			if (file.exists()) {
				output = new FileOutputStream(file);
			} else {
				file.createNewFile();
				output = new FileOutputStream(file);
			}

			// save properties to project root folder
			eProps.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
