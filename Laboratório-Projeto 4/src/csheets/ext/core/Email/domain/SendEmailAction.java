/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.ext.core.Email.ui.SendEmailUI;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author João Cabral
 */
public class SendEmailAction extends FocusOwnerAction {

	/*
	 * Instance of UIController.
	 */
	public static UIController m_uiController;

	/*
	 * Constructor of SendEmailAction.
	 */
	public SendEmailAction(UIController uiController) {
		m_uiController = uiController;
	}

	/*
	 * Method that returns the name of the option.
	 */
	@Override
	protected String getName() {
		return "Send Email Option";
	}

	/*
	 * Method that launches the SendEmailUI.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SendEmailUI sendEmailUI = new SendEmailUI(focusOwner);
	}
}
