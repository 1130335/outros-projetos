/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.ui;

import csheets.ext.Extension;
import csheets.ext.core.Email.domain.SendEmailMenu;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 *
 * @author João Cabral
 */
public class SendEmailExtensionUI extends UIExtension {

	/**
	 * Instance of SendEmailMenu.
	 */
	private SendEmailMenu window;

//	/**
//	 * A side bar that shows the sent emails.
//	 */
//	private JComponent sideBar;
	public SendEmailExtensionUI(Extension extension, UIController uiController) {
		super(extension, uiController);
	}

	/**
	 * PT : Retorna uma instância da classe que implementa o JMenu.
	 */
	@Override
	public JMenu getMenu() {
		if (window == null) {
			window = new SendEmailMenu(uiController);
		}

		return window;
	}

//	/* Returns a side bar that shows the sent emails. */
//	@Override
//	public JComponent getSideBar() {
//		if (sideBar == null) {
//			this.sideBar = new SentEmailsSidebarUI();
//		}
//		return sideBar;
//	}
}
