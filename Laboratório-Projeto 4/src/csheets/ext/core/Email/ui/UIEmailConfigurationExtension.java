/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.ui;

import csheets.ext.Extension;
import csheets.ext.core.Email.controller.EmailConfigurationController;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JToolBar;

/**
 *
 * @author coelho
 */
public class UIEmailConfigurationExtension extends UIExtension {

    /**
     * A side bar that provides editing of comments
     */
    private JComponent sideBar;

    private EmailConfigurationController m_controller;

    /**
     * The menu of the extension
     *
     * @param extension
     * @param uiController
     */
//	private ExampleMenu menu;
    public UIEmailConfigurationExtension(Extension extension, UIController uiController) {
        super(extension, uiController);
        this.m_controller = new EmailConfigurationController();
        // TODO Auto-generated constructor stub
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    @Override
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see ExampleMenu
     * @return a JMenu component
     */
    @Override
    public JMenu getMenu() {
        return null;
    }

    /**
     * Returns a toolbar that gives access to extension-specific functionality.
     *
     * @return a JToolBar component, or null if the extension does not provide
     * one
     */
    @Override
    public JToolBar getToolBar() {
        return null;
    }

    /**
     * Returns a side bar that provides editing of comments.
     *
     * @return a side bar
     */
    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            this.m_controller = new EmailConfigurationController();
            this.sideBar = new UIEmailConfigurationSidebar(this.m_controller, this.uiController);
            //this.sideBar = new JPanel();
        }
        return sideBar;
    }

    public EmailConfigurationController getController() {
        return this.m_controller;
    }

    public void setSideBarNull() {
        this.sideBar = null;
    }

}
