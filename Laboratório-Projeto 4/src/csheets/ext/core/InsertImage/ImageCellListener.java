/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage;

import java.util.EventListener;
/*
 * Interface
 */

public interface ImageCellListener extends EventListener {

	/**
	 * Invoked when an image is added to or removed from a cell.
	 *
	 * @param cell the cell that was modified
	 */
	public void imageChanged(PicCell cell);
}
