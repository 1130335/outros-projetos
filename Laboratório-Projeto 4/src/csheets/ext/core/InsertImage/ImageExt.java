package csheets.ext.core.InsertImage;

import csheets.core.Cell;
import csheets.ext.Extension;
import csheets.ext.core.InsertImage.ui.UIInsertImage;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to support comments on cells. An extension must extend the
 * Extension abstract class. The class that implements the Extension is the
 * "bootstrap" of the extension.
 *
 * @see Extension
 * @author 1130486 - Pedro Tavares
 */
public class ImageExt extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String Nome = "Insert Image";

	/**
	 * Creates a new Example extension.
	 */
	public ImageExt() {
		super(Nome);
	}

	/**
	 * Makes the given cell commentable.
	 *
	 * @param cell the cell to comment
	 * @return a commentable cell
	 */
	@Override
	public PicCell extend(Cell cell) {
		return new PicCell(cell);
	}

	/**
	 * Returns the user interface extension of this extension (an instance of
	 * the class {@link  csheets.ext.simple.ui.UIExtensionExample}). <br/>
	 * In this extension example we are only extending the user interface.
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		if (this.uiExtension == null) {
			this.uiExtension = new UIInsertImage(this, uiController);
		}
		return this.uiExtension;
	}
}
