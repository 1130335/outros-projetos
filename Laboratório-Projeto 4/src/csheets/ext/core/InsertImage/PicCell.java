/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage;

import csheets.core.Cell;
import csheets.core.graphics.Graphic;
import csheets.ext.CellExtension;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

/**
 * An extension of a cell in a spreadsheet, with support for comments.
 *
 * @author 1130486 - Pedro Tavares
 */
public class PicCell extends CellExtension {

	/**
	 * The Serial Version variable
	 *
	 */
	private static final long serialVersionUID = 7L; //verificar o valor

	/**
	 * The picture that is going to be associated with the cell
	 */
	private File image;

	/**
	 * List of all the ImageCell Listeners
	 */
	private transient List<ImageCellListener> listeners = new ArrayList<ImageCellListener>();

	/**
	 * Constructor for PicCell
	 *
	 * @param cell The chosen cell
	 */
	public PicCell(Cell cell) {
		super(cell, ImageExt.Nome);
	}

	/**
	 * Returns whether the cell has a image.
	 *
	 * @return true if the cell has an image
	 */
	public boolean hasImage() {
		return image != null;
	}

	/**
	 * Sets the user-specified image for the cell.
	 *
	 * @param image the user-specified image
	 */
	public void setUserImage(File image) {
		this.image = image;
		fireCommentsChanged();
	}

	/**
	 * Get the cell's user image.
	 *
	 * @return image The user supplied image for the cell
	 */
	public File getImage() {
		return image;
	}

	/**
	 * Notifies all registered listeners that the cell's image changed.
	 */
	protected void fireCommentsChanged() {
		for (ImageCellListener listener : listeners) {
			listener.imageChanged(this);
		}
	}

	/**
	 * Registers the given listener on the cell.
	 *
	 * @param listener the listener to be added
	 */
	public void addImageCellListener(ImageCellListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the given listener from the cell.
	 *
	 * @param listener the listener to be removed
	 */
	public void removeImageCellListener(ImageCellListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Customizes serialization, by recreating the listener list.
	 *
	 * @param stream the object input stream from which the object is to be read
	 * @throws IOException If any of the usual Input/Output related exceptions
	 * occur
	 * @throws ClassNotFoundException If the class of a serialized object cannot
	 * be found.
	 */
	private void readObject(java.io.ObjectInputStream stream)
		throws java.io.IOException, ClassNotFoundException {
		stream.defaultReadObject();
		listeners = new ArrayList<ImageCellListener>();
	}

	@Override
	public void createGraphic(String name, SortedSet<Cell> chosenCells) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Graphic getGraphic() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
