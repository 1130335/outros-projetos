package csheets.ext.core.InsertImage.ui;

import csheets.ext.core.InsertImage.PicCell;
import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 *
 * @author 1130486 - Pedro Tavares
 */
public class InsertImageController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * User interface panel *
	 */
	private ImageCell imgPanel;

	/**
	 * Creates a new controller
	 *
	 * @param uiController the interface controller
	 * @param imgPanel the interface panel
	 */
	public InsertImageController(UIController uiController,
								 ImageCell imgPanel) {
		this.uiController = uiController;
		this.imgPanel = imgPanel;
	}

	/**
	 * Attempts to create a new comment from the given string. If successful,
	 * adds the comment to the given cell. If the input string is empty or null,
	 * the comment is set to null.
	 *
	 * @param cell the cell for which the image should be set
	 * @param image the image choosed by the user
	 */
	public void setChoosedImage(PicCell cell, File image) {
		//In case the user didn't select any image
		if (image == null) {
			cell.setUserImage(null);
			uiController.
				setWorkbookModified(cell.getSpreadsheet().getWorkbook());
		} else {
			cell.setUserImage(image);
			uiController.
				setWorkbookModified(cell.getSpreadsheet().getWorkbook());
		}
	}

	/**
	 * A cell is selected.
	 *
	 * @param cell the cell whose image changed
	 */
	public void cellSelected(PicCell cell) {
		if (cell.hasImage()) {
			imgPanel.setImage(cell.getImage());
		} else {
			imgPanel.setImage(cell.getImage());
		}
	}

}
