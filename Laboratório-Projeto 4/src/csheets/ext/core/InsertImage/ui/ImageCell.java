package csheets.ext.core.InsertImage.ui;

import csheets.core.Cell;
import csheets.ext.core.InsertImage.ImageCellListener;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ext.macros.createForm.Components.Button;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author 1130486 - Pedro Tavares
 */
@SuppressWarnings("serial")
public class ImageCell extends JPanel implements SelectionListener,
        ImageCellListener {

    private InsertImageController controller;
    private PicCell cell;
    private JLabel image;
    private JPanel picPanel;
    private JPanel northPanel;

    /**
     * Creates a new picture panel.
     *
     * @param uiController the user interface controller
     */
    public ImageCell(UIController uiController) {
        super(new BorderLayout());
        setName("Images");
        controller = new InsertImageController(uiController, this);
        uiController.addSelectionListener(this);

        //Creates a new Panel and sets the layout
        picPanel = new JPanel();
        picPanel.setLayout(new BoxLayout(picPanel, BoxLayout.PAGE_AXIS));
        picPanel.setPreferredSize(new Dimension(140, 350));

        //Creates a new Label that's going to have the choosed image
        image = new JLabel();
        picPanel.add(image);
        JButton but = new JButton("Export to file");
        but.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               ExportImage export= new ExportImage(cell.getImage());
              
            }
        });
        // Adds panels
        northPanel = new JPanel(new BorderLayout());
        northPanel.add(picPanel, BorderLayout.NORTH);
        add(northPanel, BorderLayout.NORTH);
        JPanel centerPanel = new JPanel();
        centerPanel.add(but);
        add(centerPanel, BorderLayout.CENTER);
    }

    /**
     * Updates the image cell when the user of the active cell is changed.
     *
     * @param cell the cell whose image changed
     */
    //@Override
    @Override
    public void imageChanged(PicCell cell) {
        this.cell = cell;
        controller.cellSelected(cell);
    }

    /**
     * Updates the panel
     *
     * @param event the selection event
     */
    @Override
    public void selectionChanged(SelectionEvent event) {
        Cell cell = event.getCell();
        if (cell != null) {
            PicCell choosedCell = (PicCell) cell.
                    getExtension(ImageExt.Nome);
            choosedCell.addImageCellListener(this);
            imageChanged(choosedCell);
        }

        // Stops listening to previous active cell
        if (event.getPreviousCell() != null) {
            ((PicCell) event.getPreviousCell().
                    getExtension(ImageExt.Nome))
                    .removeImageCellListener(this);
        }
    }

    /**
     * Updates the image panel when the user of the active cell is changed.
     *
     * @param newImage the image of the active cell
     */
    public void setImage(File newImage) {
        if (newImage != null) {
            clearPanel();
            ImageIcon img = new ImageIcon(newImage.getAbsolutePath());
            Image im = img.getImage();
            ImageIcon imgResizable = new ImageIcon(resizeImage(im));
            image = new JLabel(imgResizable);
            picPanel.add(image);
            northPanel.add(picPanel);

        } else {
            //Clean the picture panel
            clearPanel();
            image = new JLabel();
            picPanel.add(image);
            northPanel.add(picPanel);
        }
    }

    /**
     * Resizes the image to display it correctly.
     *
     * @param image the picture of the active cell
     */
    private Image resizeImage(Image image) {
        Image newimg;
        if (image.getWidth(this) < 140 && image.getHeight(this) < 350) {
            newimg = image.getScaledInstance(image.getWidth(this), image.
                    getHeight(this), java.awt.Image.SCALE_SMOOTH);
            return newimg;
        } else if (image.getWidth(this) < 140) {
            newimg = image.
                    getScaledInstance(image.getWidth(this), 350, java.awt.Image.SCALE_SMOOTH);
            return newimg;
        } else if (image.getHeight(this) < 350) {
            newimg = image.
                    getScaledInstance(130, image.getHeight(this), java.awt.Image.SCALE_SMOOTH);
            return newimg;
        }
        newimg = image.
                getScaledInstance(140, 350, java.awt.Image.SCALE_SMOOTH);
        return newimg;
    }

    /**
     * Clears the image panel
     */
    public void clearPanel() {
        northPanel.remove(picPanel);
        picPanel = new JPanel();
        picPanel.setLayout(new BoxLayout(picPanel, BoxLayout.PAGE_AXIS));
        picPanel.setPreferredSize(new Dimension(140, 350));
        picPanel.
                setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
    }
}
