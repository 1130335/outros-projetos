/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import csheets.ext.Extension;
import csheets.ext.simple.ui.ExampleMenu;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 * This class implements the UI interface extension for the comments extension.
 * A UI interface extension must extend the UIExtension abstract class.
 *
 * @see UIExtension
 * @author 1130486 - Pedro Tavares
 */
public class UIInsertImage extends UIExtension {

	/**
	 * The menu of the extension
	 */
	private InsertImageMenu menu;

	/**
	 * A side bar that provides editing of comments
	 */
	private JComponent sideBar;

        private CellDecorator cellDecorator;
        
	public UIInsertImage(Extension extension, UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	@Override
	public JToolBar getToolBar() {
		return null;
	}
        
        /**
	 * Returns a cell decorator that visualizes comments on cells.
	 *
	 * @return decorator for cells with comments
	 */
	@Override
	public CellDecorator getCellDecorator() {
		if (cellDecorator == null) {
			cellDecorator = new PicCellDecorator();
		}
		return cellDecorator;
	}

	/**
	 * Returns an instance of a class that implements JMenu. In this simple case
	 * this class only supplies one menu option.
	 *
	 * @see ExampleMenu
	 * @return a JMenu component
	 */
	@Override
	public JMenu getMenu() {
		if (menu == null) {
			menu = new InsertImageMenu(uiController);
		}
		return menu;
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon with style
	 */
	@Override
	public Icon getIcon() {
		return null;
	}

	/**
	 * Returns a side bar that provides editing of comments.
	 *
	 * @return a side bar
	 */
	@Override
	public JComponent getSideBar() {
		if (sideBar == null) {
			sideBar = new ImageCell(uiController);
		}
		return sideBar;
	}
}
