/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import csheets.core.Cell;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Class that is responsible for the insertion of the image
 *
 * @author 1130486 - Pedro Tavares
 */
public class InsertImageAction extends FocusOwnerAction {

	/**
	 * The InsertImage Controller
	 */
	private InsertImageController controller;

	/**
	 * Constructor that is responsible for the creation of the controller
	 *
	 * @param uiController
	 */
	public InsertImageAction(UIController uiController) {
		this.controller = new InsertImageController(uiController, null);
	}

	/**
	 * Returns the name of the item
	 *
	 * @return name Name of the item
	 */
	@Override
	protected String getName() {
		return "Insert Image";
	}

	/**
	 * Action to Insert Image
	 *
	 * @param ae Action Event
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		String imageExt1 = "jpg";
		String imageExt2 = "jpeg";
		String imageExt3 = "png";
		String imageExt4 = "gif";
		boolean perm = true;

		Cell[][] selectedCells = this.focusOwner.getSelectedCells();

		if (selectedCells.length == 0) {
			JOptionPane.
				showMessageDialog(null, "You have to select one cell in order to insert an image!");
			perm = false;
		} else if (selectedCells[0].length > 1 || selectedCells.length > 1) {
			perm = false;
			JOptionPane.
				showMessageDialog(null, "You can only select one cell.");
		}

		Cell c = selectedCells[0][0];

		if (perm == true) {
			JFileChooser fileChooser = new JFileChooser();
			int valid = fileChooser.showOpenDialog(null);

			if (valid == JFileChooser.APPROVE_OPTION) {

				File image = fileChooser.getSelectedFile();
				String ImgName = image.getName();
				String extension = ImgName.substring(ImgName.
					lastIndexOf(".") + 1, ImgName.length());

				if (!imageExt1.equals(extension) && !imageExt2.
					equals(extension)
					&& !imageExt3.equals(extension) && !imageExt4.
					equals(extension)) {
					JOptionPane.
						showMessageDialog(null, "You can only select images with one of the extensions:\n file.jpeg\nfile.png\nfile.jpg\nfile.gif");
					perm = false;
				}

				if (perm == true) {
					PicCell choosedCell = (PicCell) c.
						getExtension(ImageExt.Nome);
					controller.setChoosedImage(choosedCell, image);
				}
                                MouseHoverTable mouse= new MouseHoverTable(focusOwner,this.focusOwner.getSpreadsheet());
                                
			}
		}
	}
}
