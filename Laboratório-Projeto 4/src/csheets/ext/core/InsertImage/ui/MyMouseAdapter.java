/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ext.crm.calendar.ui.CalendarEditUI;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

/**
 *
 * @author daniel
 */
public class MyMouseAdapter extends MouseMotionAdapter {

    Spreadsheet spreadsheet = null;

    public MyMouseAdapter(Spreadsheet spreadsheet) {
        this.spreadsheet = spreadsheet;
    }

    public void mouseMoved(MouseEvent e) {
        final JTable aTable = (JTable) e.getSource();
        int itsRow = aTable.rowAtPoint(e.getPoint());
        int itsColumn = aTable.columnAtPoint(e.getPoint());

        Cell c = this.spreadsheet.getCell(itsColumn, itsRow);

        PicCell choosedCell = (PicCell) c.getExtension(ImageExt.Nome);
        if (choosedCell.hasImage()) {
            aTable.removeMouseMotionListener(this);
            System.out.println(choosedCell.toString());

            final JFrame frame = new JFrame();
            ImageIcon img = new ImageIcon(choosedCell.getImage().getAbsolutePath());
            Image im = img.getImage();
            Image newimg = im.
			getScaledInstance(140, 350, java.awt.Image.SCALE_SMOOTH);
            ImageIcon imgResizable = new ImageIcon(newimg);
            JLabel image = new JLabel(imgResizable);
            JPanel picPanel = new JPanel();
            picPanel.add(image);
            frame.add(picPanel);
            frame.setSize(140, 350);

            // here's the part where i center the jframe on screen
            frame.setVisible(true);

            frame.addMouseListener(new MouseListener() {
                public void mouseExited(MouseEvent e) {
                    frame.dispose();
                    MyMouseAdapter aMouseAda = new MyMouseAdapter(spreadsheet);
                    aTable.addMouseMotionListener((MouseMotionListener) aMouseAda);
                }

                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }
            });

        }

    }
}
