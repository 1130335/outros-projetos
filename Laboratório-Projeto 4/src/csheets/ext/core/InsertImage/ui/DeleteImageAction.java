/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import csheets.core.Cell;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 * @author 1130486 - Pedro Tavares
 */
public class DeleteImageAction extends FocusOwnerAction {

	/**
	 * The InsertImageController
	 */
	private InsertImageController controller;

	/**
	 * Constructor that is responsible for the creation of the controller
	 *
	 * @param uiController
	 */
	public DeleteImageAction(UIController uiController) {
		this.controller = new InsertImageController(uiController, null);
	}

	/**
	 * Returns the name of the item
	 *
	 * @return name Name of the item
	 */
	@Override
	protected String getName() {
		return "Remove Image";
	}

	/**
	 * Action to Remove Image
	 *
	 * @param ae Action Event
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		boolean perm = true;
		Cell[][] selectedCells = this.focusOwner.getSelectedCells();

		//In case the user didn´t select any cell
		if (selectedCells.length == 0) {
			JOptionPane.
				showMessageDialog(null, "You have to select one cell in order to insert an image!");
			perm = false;
			//in case the user selected more than one cell
		} else if (selectedCells.length > 1 || selectedCells[0].length > 1) {
			JOptionPane.
				showMessageDialog(null, "You can only select one cell.");
			perm = false;
		}

		Cell c = selectedCells[0][0];

		PicCell choosedCell = (PicCell) c.
			getExtension(ImageExt.Nome);

		//In case the user tries to delete an image from a cell that doesn't have one
		if (!choosedCell.hasImage()) {
			perm = false;
			JOptionPane.
				showMessageDialog(null, "The selected cell doesn't have a picture associated!");
		}

		//Deletes the image
		if (perm == true) {
			controller.setChoosedImage(choosedCell, null);
		}
	}
}
