/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author daniel
 */
public class ExportImage {

    public ExportImage(File img) {

        if (img != null) {

           
            try {
                BufferedImage image= ImageIO.read(img);
   
                File outputfile = new File("saved.png");
                ImageIO.write(image, "png", outputfile);
            } catch (IOException e) {
               
            }
        }
    }
}
