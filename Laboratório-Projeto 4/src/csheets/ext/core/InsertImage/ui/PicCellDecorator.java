/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage.ui;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.ext.core.InsertImage.ImageExt;
import csheets.ext.core.InsertImage.PicCell;
import csheets.ui.ext.CellDecorator;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import javax.swing.JComponent;

/**
 *
 * @author daniel
 */
class PicCellDecorator extends CellDecorator {

    private static final Font font = new Font("Dialog", Font.PLAIN, 10);

    /**
     * Creates a new cell decorator.
     */
    public PicCellDecorator() {
    }

    /**
     * Decorates the given graphics context if the cell being rendered has a
     * comment.
     *
     * @param component the cell renderer component
     * @param g the graphics context on which drawing should be done
     * @param cell the cell being rendered
     * @param selected whether the cell is selected
     * @param hasFocus whether the cell has focus
     */
    public void decorate(JComponent component, Graphics g, Cell cell,
            boolean selected, boolean hasFocus) {
        if (enabled) {
            PicCell picCell = (PicCell) cell.getExtension(ImageExt.Nome);
            if (picCell.hasImage()) {
                // Stores current graphics context properties
                Graphics2D g2 = (Graphics2D) g;
                Color oldPaint = g2.getColor();
                Font oldFont = g2.getFont();

                 g2.setColor(Color.red);
                g2.setFont(font);
                Image img1 = Toolkit.getDefaultToolkit().getImage(CleanSheets.class.getResource("res/img/image_icon.gif"));
               
                g2.drawImage(img1, 4, 2, new ImageObserver() {

                    @Override
                    public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                       return true;
                    }
                });
                // Restores graphics context properties
                g2.setColor(oldPaint);
                g2.setFont(oldFont);
            }

        }
    }

}
