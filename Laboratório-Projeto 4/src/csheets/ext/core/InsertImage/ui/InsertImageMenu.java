package csheets.ext.core.InsertImage.ui;

import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 * The Image Menu
 *
 * @author 1130486 - Pedro Tavares
 */
public class InsertImageMenu extends JMenu {

	/**
	 * @param uiController
	 */
	public InsertImageMenu(UIController uiController) {
		super("Insert Image");

		// Fazer para adicionar a fotografia e apagá-la!
		add(new InsertImageAction(uiController));
		add(new DeleteImageAction(uiController));
	}

}
