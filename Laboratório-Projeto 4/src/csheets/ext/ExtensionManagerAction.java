/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext;

import csheets.ext.navigator.NavigatorExtension;
import csheets.ext.navigator.ui.UINavigatorExtension;
import csheets.ui.Frame;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author RICARDLEITE
 */
public class ExtensionManagerAction extends BaseAction {

	private UIController uiController;
	private JScrollPane listPane;
	/**
	 * Stores the existing extensions for use during the activation/deactivation
	 * proccess.
	 */
	private JCheckBox[] checkList;

	ExtensionManager extensionManager = ExtensionManager.getInstance();
	/**
	 * SideBarMenu from MenuBar class
	 */
	private JMenu m_sideBarMenu;

	public ExtensionManagerAction(JMenu sideBarMenu, UIController controller) {
		this.m_sideBarMenu = sideBarMenu;
		uiController = controller;
		fillCheckBoxesWithExtensionsByStatus();
	}

	/**
	 * Method to return the method of the button frame
	 *
	 * @return Name of the frame
	 */
	@Override
	protected String getName() {
		return "Extension Manager";
	}

	/**
	 * Sets the extensions status after user's selection
	 *
	 * @param e Action Event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog((Frame) null, listPane, "Extension Manager", JOptionPane.PLAIN_MESSAGE, (Icon) null);
		extensionState();
	}

	/**
	 * Sets the extension status of the extensions after user's selection
	 */
	private void extensionState() {
                        for(JCheckBox jcheck : checkList){
                            if(jcheck.isSelected()){
                                for(UIExtension uiExt : uiController.getExtensions()){
                                    if(uiExt.getExtension().getName().equalsIgnoreCase(jcheck.getText())){
                                        uiExt.getExtension().enable();
                                        if(uiExt.getSideBar() != null){
                                            for(int i = 0; i < uiExt.getSideBar().getComponentCount(); i++){
                                                uiExt.getSideBar().getComponent(i).setVisible(true);
                                            }
                                        }
                                        if(uiExt.getToolBar() != null){
                                            for(int i = 0; i < uiExt.getToolBar().getComponentCount(); i++){
                                                uiExt.getToolBar().getComponentAtIndex(i).setEnabled(true);
                                            }
                                        }
                                        if(uiExt.getMenu() != null){
                                            uiExt.getMenu().setEnabled(true);
                                        }
                                    }
                                }
                            }
                            else{
                                for(UIExtension uiExt : uiController.getExtensions()){
                                    if(uiExt.getExtension().getName().equalsIgnoreCase(jcheck.getText())){
                                        uiExt.getExtension().disable();
                                        if(uiExt.getSideBar() != null){
                                            for(int i = 0; i < uiExt.getSideBar().getComponentCount(); i++){
                                                uiExt.getSideBar().getComponent(i).setVisible(false);
                                            }
                                        }
                                        if(uiExt.getToolBar() != null){
                                            for(int i = 0; i < uiExt.getToolBar().getComponentCount(); i++){
                                                uiExt.getToolBar().getComponentAtIndex(i).setEnabled(false);
                                            }
                                        }
                                        if(uiExt.getMenu() != null){
                                            uiExt.getMenu().setEnabled(false);
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        for(UIExtension uiExt : uiController.getExtensions()){
                            Extension temp = uiExt.getExtension();
                            if(temp instanceof NavigatorExtension){
                               for(Extension ext : extensionManager.getExtensions()){
                                   if(ext.isEnabled()){
                                       ((UINavigatorExtension)temp.uiExtension).getController().updateText();
                                   }
                               }
                            }
                        }
                  }
        
        
	/**
	 * Method that fill the check boxes with the extension state. If the
	 * extension is already enabled, shows a "check" in the check box.
	 */
	private void fillCheckBoxesWithExtensionsByStatus() {
		Extension[] extensions = extensionManager.getExtensions();
		checkList = new JCheckBox[extensions.length];

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		for (int i = 0; i < extensions.length; i++) {
			JCheckBox cb = new JCheckBox();
			cb.setText(extensions[i].getName());
			if (extensions[i].isEnabled()) {
				cb.setSelected(true);
			} else {
				cb.setSelected(false);
			}
			panel.add(cb);
			checkList[i] = cb;
		}
		listPane = new JScrollPane(panel);

	}
}
