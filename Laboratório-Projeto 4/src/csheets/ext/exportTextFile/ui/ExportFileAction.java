package csheets.ext.exportTextFile.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * An action of the simple extension that exemplifies how to interact with the
 * spreadsheet.
 *
 * @author 1121248DiogoVieira
 *
 */
public class ExportFileAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;
	private String cabecalho = "";
	private String separador = "";

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public ExportFileAction(UIController uiController) {
		this.uiController = uiController;
	}

	protected String getName() {
		return "Text File";
	}

	protected void defineProperties() {
	}

	/**
	 * A simple action that presents a confirmation dialog. If the user confirms
	 * then the contents of the cell A1 of the current sheet are set to the
	 * string "Changed".
	 *
	 * @param event the event that was fired
	 */
	public void actionPerformed(ActionEvent event) {

		// Vamos exemplificar como se acede ao modelo de dominio (o workbook)
		saveProjectDialog(null, "csv");
	}

	private String buildStringToExport(String separador, String cabecalho) {
		Address selectedCellCoordinates = this.uiController.getActiveCell().
			getAddress();

		int maxX = this.uiController.getActiveSpreadsheet().getColumnCount();
		int maxY = this.uiController.getActiveSpreadsheet().getRowCount();

		Address lastCellCoordinates = new Address(maxX, maxY);

		String contentToExport = "";
		Set<Cell> cellList = this.uiController.getActiveSpreadsheet().
			getCells(selectedCellCoordinates, lastCellCoordinates);

		int colCounterForParagraph = 0;
		int currentColumn = 0;

		if (cabecalho.equalsIgnoreCase("")) {
			//nao faz nada
		} else {
			contentToExport += cabecalho;

			contentToExport += "\n";
		}

		for (Cell c : cellList) {
			currentColumn = c.getAddress().getColumn();
			if (colCounterForParagraph != currentColumn) {
				contentToExport += "\n";
			}
			if (separador.equalsIgnoreCase("")) {
				contentToExport += c.getContent() + ";";
			} else {
				contentToExport += c.getContent() + separador;
			}

			colCounterForParagraph = currentColumn;
		}
		return contentToExport;
	}

	public synchronized void writeFile(File fic, String separador,
									   String cabecalho) throws IOException, Exception {
		String content = buildStringToExport(separador, cabecalho);
		FileWriter fo = new FileWriter(fic);
		if (fic.exists()) {
			fic.delete();
		}
		fo.write(content);
		fo.close();
	}

	private static String s = "";

	public void defenirFiltro(JFileChooser fc, String filtro) {
		s = filtro;
		fc.setFileFilter(new javax.swing.filechooser.FileFilter() {
			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}
				String extensao = extensao(f);
				if (extensao != null) {
					return extensao.equals(s);
				}
				return false;
			}

			@Override
			public String getDescription() {
				return "*." + s;
			}

			public String extensao(File f) {
				String ext = null;
				String s = f.getName();
				int i = s.lastIndexOf('.');
				if (i != -1) {
					ext = s.substring(i + 1).toLowerCase();
				}
				return ext;
			}

		});
	}

	public void saveProjectDialog(JPanel p, String filtro) {
		JFileChooser escolher = new JFileChooser(UIExportTextFileExtension.class.
			getProtectionDomain().getCodeSource().getLocation().getPath());
		defenirFiltro(escolher, filtro);
		int resp = escolher.showSaveDialog(p);
		if (resp == JFileChooser.APPROVE_OPTION) {
			try {
				String path = escolher.getSelectedFile().getAbsolutePath();
				String ext;
				int i = path.lastIndexOf('.');
				if (i != -1) {
					ext = path.substring(i);
					if (!ext.equalsIgnoreCase(".csv")) {
						path = path.replaceAll(ext, ".csv");
					}
				} else {
					path = path + ".csv";
				}
				File f = new File(path);

				NewJDialog escolhas = new NewJDialog(null, enabled);
				escolhas.setVisible(true);
				escolhas.setEnabled(true);

				writeFile(f, this.separador, this.cabecalho);
				JOptionPane.
					showMessageDialog(null, "File Saved!", "Save File", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception ex) {
				JOptionPane.
					showMessageDialog(null, ex, "Error ocurred while saving", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}

	private void setSeparador(String separador) {
		this.separador = separador;
	}

	/*
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */
	/**
	 *
	 * @author Pedro
	 */
	public class NewJDialog extends javax.swing.JDialog {

		/**
		 * Creates new form NewJDialog
		 */
		public NewJDialog(java.awt.Frame parent, boolean modal) {
			super(parent, modal);
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jTextField1 = new javax.swing.JTextField();
			jCheckBox1 = new javax.swing.JCheckBox();
			jCheckBox2 = new javax.swing.JCheckBox();
			jTextField2 = new javax.swing.JTextField();
			jButton1 = new javax.swing.JButton();

			setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
			setTitle("Deseja inserir cabeçalho ou caracter separador?");

			jTextField1.setEditable(false);
			jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jTextField1MouseClicked(evt);
				}
			});
			jTextField1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jTextField1ActionPerformed(evt);
				}
			});

			jCheckBox1.setText("Cabeçalho");
			jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jCheckBox1ActionPerformed(evt);
				}
			});

			jCheckBox2.setText("Separador");
			jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jCheckBox2ActionPerformed(evt);
				}
			});

			jTextField2.setEditable(false);
			jTextField2.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jTextField2MouseClicked(evt);
				}
			});
			jTextField2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jTextField2ActionPerformed(evt);
				}
			});

			jButton1.setText("Terminar");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(
				layout.
				createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.
						createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).
						addGroup(layout.createSequentialGroup()
							.addComponent(jCheckBox1)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).
							addComponent(jCheckBox2)
							.addGap(60, 60, 60))
						.addGroup(layout.createSequentialGroup()
							.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE).
							addGroup(layout.
								createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).
								addGroup(layout.createSequentialGroup()
									.addGap(18, 18, 18)
									.addComponent(jTextField2)
									.addContainerGap())
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.
										  createSequentialGroup()
										  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE).
										  addComponent(jButton1)
										  .addGap(55, 55, 55))))))
			);
			layout.setVerticalGroup(
				layout.
				createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.
						  createSequentialGroup()
						  .addGap(25, 25, 25)
						  .addGroup(layout.
							  createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).
							  addComponent(jCheckBox1)
							  .addComponent(jCheckBox2))
						  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).
						  addGroup(layout.
							  createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).
							  addGroup(layout.createSequentialGroup()
								  .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).
								  addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).
								  addComponent(jButton1))
							  .addGroup(layout.createSequentialGroup()
								  .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE).
								  addGap(0, 15, Short.MAX_VALUE)))
						  .addContainerGap())
			);

			pack();
			setLocationRelativeTo(null);
		}// </editor-fold>

		private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {
		}

		private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {
		}

		private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {
			if (jCheckBox1.isSelected()) {
				jTextField1.setEnabled(true);
				jTextField1.setEditable(true);
				jTextField1.setText("Insira aqui o cabeçalho");
			} else {
				jTextField1.setEnabled(false);
				jTextField1.setEditable(false);
				jTextField1.setText("");
			}
		}

		private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			if (jCheckBox2.isSelected()) {
				jTextField2.setEnabled(true);
				jTextField2.setEditable(true);
				jTextField2.setText("Ensira aqui o separador");
			} else {
				jTextField2.setEnabled(false);
				jTextField2.setEditable(false);
				jTextField2.setText("");
			}
		}

		private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			jTextField1.setText("");
		}

		private void jTextField2MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			jTextField2.setText("");
		}

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			if (!jTextField1.getText().equalsIgnoreCase("")) {
				setCabecalho(jTextField1.getText());
			}
			if (!jTextField2.getText().equalsIgnoreCase("")) {
				setSeparador(jTextField2.getText());
			}
			dispose();
		}

		/**
		 * @param args the command line arguments
		 */
		public void main(String args[]) {
			/* Set the Nimbus look and feel */
			//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
			 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
			 */
			try {
				for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.
					getInstalledLookAndFeels()) {
					if ("Nimbus".equals(info.getName())) {
						javax.swing.UIManager.
							setLookAndFeel(info.getClassName());
						break;
					}
				}
			} catch (ClassNotFoundException ex) {
				java.util.logging.Logger.getLogger(NewJDialog.class.getName()).
					log(java.util.logging.Level.SEVERE, null, ex);
			} catch (InstantiationException ex) {
				java.util.logging.Logger.getLogger(NewJDialog.class.getName()).
					log(java.util.logging.Level.SEVERE, null, ex);
			} catch (IllegalAccessException ex) {
				java.util.logging.Logger.getLogger(NewJDialog.class.getName()).
					log(java.util.logging.Level.SEVERE, null, ex);
			} catch (javax.swing.UnsupportedLookAndFeelException ex) {
				java.util.logging.Logger.getLogger(NewJDialog.class.getName()).
					log(java.util.logging.Level.SEVERE, null, ex);
			}
			//</editor-fold>

			/* Create and display the dialog */
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					NewJDialog dialog = new NewJDialog(new javax.swing.JFrame(), true);
					dialog.
						addWindowListener(new java.awt.event.WindowAdapter() {
							@Override
							public void windowClosing(
								java.awt.event.WindowEvent e) {
									System.exit(0);
								}
						});
					dialog.setVisible(true);
				}
			});
		}

		// Variables declaration - do not modify
		private javax.swing.JButton jButton1;
		private javax.swing.JCheckBox jCheckBox1;
		private javax.swing.JCheckBox jCheckBox2;
		private javax.swing.JTextField jTextField1;
		private javax.swing.JTextField jTextField2;
		// End of variables declaration
	}

}
