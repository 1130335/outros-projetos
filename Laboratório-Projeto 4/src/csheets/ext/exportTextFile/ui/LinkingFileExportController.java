/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.exportTextFile.ui;

import csheets.core.Spreadsheet;
import csheets.ext.importFile.LinkingFile;
import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 *
 * @author Sergio
 */
public class LinkingFileExportController {

	/**
	 * User Interface Controller
	 */
	private UIController m_controller;
	/**
	 * Separator
	 */
	private String separator;
	/**
	 * Header
	 */
	private boolean m_header;
	/**
	 * File to export
	 */
	private File file = null;
	/**
	 * The active spreadsheet
	 */
	private Spreadsheet m_sheet;

	/**
	 * Create a new LinkingFileExportController
	 *
	 * @param controller Linking File Export controller
	 */
	public LinkingFileExportController(UIController controller) {
		m_controller = controller;
		m_sheet = m_controller.getActiveSpreadsheet();

	}

	/**
	 * Return separator
	 *
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * Set separator
	 *
	 * @param separator new separator
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * Return header
	 *
	 * @return true or false
	 */
	public boolean isHeader() {
		return m_header;
	}

	/**
	 * Set header
	 *
	 * @param header new header
	 */
	public void setHeader(boolean header) {
		this.m_header = header;
	}

	/**
	 * Return file
	 *
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Set file
	 *
	 * @param file new file
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Export data spreadsheet to file/Starts linked exportation
	 */
	public void exportToFile() {
		LinkingFile lf = new LinkingFile(m_sheet, file, m_header, separator, m_controller);
		lf.startExport();
		m_controller.addNewExportLinkingFile(lf);

	}

	/**
	 * Stop linked exportation
	 */
	public void stopExportToFile() {
		m_controller.removeLinkedExportFile(m_sheet);
	}

	/**
	 * Check if active spreadsheet is already linked
	 *
	 * @return true if is already linked or false
	 */
	public boolean isLinked() {
		return m_controller.isLinked();
	}
}
