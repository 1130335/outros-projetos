package csheets.ext.exportTextFile.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Representes the UI extension menu of the simple extension.
 *
 * @author 1121248DiogoVieira
 *
 */
@SuppressWarnings("serial")
public class ExportFileMenu extends JMenu {

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created. A menu
	 * option is an action (in this case
	 * {@link csheets.ext.simple.ui.ExampleAction})
	 *
	 * @param uiController the user interface controller
	 */
	public ExportFileMenu(UIController uiController) {
		super("Export");
		setMnemonic(KeyEvent.VK_X);

		// Adds Text export options
		add(new ExportFileAction(uiController));
		add(new LinkingFileExportAction(uiController));
	}
}
