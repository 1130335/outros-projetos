package csheets.ext.exportTextFile;

import csheets.ext.Extension;
import csheets.ext.exportTextFile.ui.UIExportTextFileExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to support comments on cells. An extension must extend the
 * Extension abstract class. The class that implements the Extension is the
 * "bootstrap" of the extension.
 *
 * @see Extension
 * @author 1121248DiogoVieira
 */
public class ExportTextFileExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Export Test File";

	/**
	 * Creates a new Example extension.
	 */
	public ExportTextFileExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExportTextFileExtension(this, uiController);
	}

	/**
	 * Saves the content selected to a file
	 */
	public void exportFile() {

	}
}
