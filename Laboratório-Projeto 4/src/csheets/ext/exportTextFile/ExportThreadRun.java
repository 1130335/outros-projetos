/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.exportTextFile;

import csheets.ext.importFile.LinkingFile;
import csheets.ui.ctrl.UIController;
import static java.lang.Thread.sleep;
import java.util.ConcurrentModificationException;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents the run of Linking Export Thread
 *
 * @author Sergio
 */
public class ExportThreadRun implements Runnable {

	/**
	 * Linking File to associate with thread
	 */
	private LinkingFile m_fl;
	/**
	 * User interface controller
	 */
	private UIController m_controller;

	/**
	 * Create the export thread run
	 *
	 * @param fl linkingfile
	 * @param controller user interface controller
	 */
	public ExportThreadRun(LinkingFile fl, UIController controller) {
		this.m_fl = fl;
		this.m_controller = controller;

	}

	/**
	 * Run to thread
	 */
	@Override
	public void run() {
		for (;;) {
			while (!m_fl.getThread().interrupted()) {
				writeInFile();
				try {
					sleep(10);
				} catch (InterruptedException ex) {
					Logger.getLogger(ExportThreadRun.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	/**
	 * Write spreadsheet data in txt file
	 */
	public void writeInFile() {
		try {
			try {

				Formatter fout = new Formatter(m_fl.getFile());
				int row = 0;

				if (m_fl.getHeader()) {
					row = 1;
				}

				for (; row < m_fl.getSheet().getRowCount(); row++) {
					for (int column = 0; column < m_fl.getSheet().
						getColumnCount() + 1; column++) {
						synchronized (m_fl.getSheet().
							getCell(column, row)) {
							fout.
								format(m_fl.getSheet().
									getCell(column, row).
									getContent()
									+ m_fl.getSeparator());
						}
					}
					fout.format("%n");
				}

				fout.close();
			} catch (Exception ex) {
			}

		} catch (ConcurrentModificationException exc) {
			run();
		}
	}
}
