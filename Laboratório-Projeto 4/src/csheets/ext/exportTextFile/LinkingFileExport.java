/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.exportTextFile;

import csheets.core.Spreadsheet;
import csheets.ext.importFile.LinkingFile;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class LinkingFileExport {

	/**
	 * LinkingFile list of the sheets
	 */
	private List<LinkingFile> linkedFiles = new ArrayList<LinkingFile>();
	/**
	 * User Interface Controller
	 */
	private UIController m_controller;

	/**
	 * Create a new LinkingFileExport
	 *
	 * @param controller user interface controller
	 */
	public LinkingFileExport(UIController controller) {
		this.m_controller = controller;

	}

	/**
	 * Add LinkingFile lf to linkedFiles
	 *
	 * @param lf LinkingFile to add
	 */
	public void addNewLinkingFile(LinkingFile lf) {

		getLinkedFiles().add(lf);

	}

	/**
	 * Removes the LinkingFile and interrupts the respective thread to
	 * spreasheet given by parameter
	 *
	 * @param spreadsheet to apply modifications
	 */
	public void removeLinkedFile(Spreadsheet spreadsheet) {
		int index;
		try {
			for (LinkingFile fl : getLinkedFiles()) {
				if (fl.getSheet().equals(spreadsheet)) {
					index = getLinkedFiles().indexOf(fl);
					getLinkedFiles().get(index).getThread().interrupt();
					getLinkedFiles().remove(index);
				}
			}
		} catch (ConcurrentModificationException ce) {
			removeLinkedFile(spreadsheet);
		}

	}

	/**
	 *
	 * Check if the spreadsheet given by parameter is linked
	 *
	 * @param spreadsheet the spreadsheet to chek
	 * @return true if spreadsheet is already linked
	 */
	public boolean checkLinkedFile(Spreadsheet spreadsheet) {
		for (LinkingFile fl : getLinkedFiles()) {
			if (fl.getSheet() == spreadsheet) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the LinkinFile list
	 *
	 * @return the linkedFiles
	 */
	public List<LinkingFile> getLinkedFiles() {
		return linkedFiles;
	}

	/**
	 * Set LinkinFile list
	 *
	 * @param linkedFiles new LinkinFile list
	 */
	public void setLinkedFiles(
		List<LinkingFile> linkedFiles) {
		this.linkedFiles = linkedFiles;
	}
}
