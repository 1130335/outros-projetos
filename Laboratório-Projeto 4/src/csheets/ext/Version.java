/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext;

/**
 *
 * @author RICARDLEITE
 */
public class Version {

	/**
	 * description of the version
	 */
	private String description;

	/**
	 * Method to create a new version
	 *
	 * @param description
	 */
	public Version(String description) {
		this.description = description;
	}

	/**
	 * To string of the description of the version
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return "\nVersion Description: " + description;
	}
}
