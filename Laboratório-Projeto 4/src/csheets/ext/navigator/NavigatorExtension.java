/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.navigator;

import csheets.ext.Extension;
import csheets.ext.navigator.ui.UINavigatorExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author User
 */
public class NavigatorExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Navigator";

	/**
	 * Creates a new Navigator extension
	 */
	public NavigatorExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		if (this.uiExtension == null) {
			this.uiExtension = new UINavigatorExtension(this, uiController);
		}
		return this.uiExtension;
	}

	public void renewStatusInfo() {
		UINavigatorExtension uiExt = (UINavigatorExtension) this.uiExtension;
		uiExt.setSideBarNull();
		uiExt.getSideBar();
	}
}
