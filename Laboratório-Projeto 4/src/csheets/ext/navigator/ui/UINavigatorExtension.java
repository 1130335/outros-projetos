/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.navigator.ui;

import csheets.ext.Extension;
import csheets.ext.navigator.NavigatorExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

/**
 *
 * @author User
 */
public class UINavigatorExtension extends UIExtension {

	/**
	 * A side bar that provides editing of comments
	 */
	private JComponent sideBar;

	private NavigatorExtensionController controller;

	/**
	 * The menu of the extension
	 */
//	private ExampleMenu menu;
	public UINavigatorExtension(Extension extension, UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return null;
	}

	/**
	 * Returns an instance of a class that implements JMenu. In this simple case
	 * this class only supplies one menu option.
	 *
	 * @see ExampleMenu
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		return null;
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	public JToolBar getToolBar() {
		return null;
	}

	/**
	 * Returns a side bar that provides editing of comments.
	 *
	 * @return a side bar
	 */
	public JComponent getSideBar() {
		if (sideBar == null) {

			sideBar = new JPanel(new GridLayout(2, 1));
			sideBar.setName(NavigatorExtension.NAME);

			// Creates components
			JTextArea active = new JTextArea();
			JTextArea inactive = new JTextArea();

			controller = new NavigatorExtensionController();

			controller.setText(active, inactive);

			JScrollPane enabledPane = new JScrollPane(active);
			JScrollPane disabledPane = new JScrollPane(inactive);

			// Adds borders
			TitledBorder border = BorderFactory.createTitledBorder("Active");
			border.setTitleJustification(TitledBorder.CENTER);
			enabledPane.setBorder(border);
			border = BorderFactory.createTitledBorder("Inactive");
			border.setTitleJustification(TitledBorder.CENTER);
			disabledPane.setBorder(border);

			// Creates side bar
			sideBar.add(enabledPane);
			sideBar.add(disabledPane);
		}
		return sideBar;
	}

	public NavigatorExtensionController getController() {
		return controller;
	}

	public void setSideBarNull() {
		this.sideBar = null;
	}
}
