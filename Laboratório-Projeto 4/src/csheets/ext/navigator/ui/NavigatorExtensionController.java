/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.navigator.ui;

import csheets.ext.Extension;
import csheets.ext.ExtensionManager;
import javax.swing.JTextArea;

/**
 *
 * @author User
 */
public class NavigatorExtensionController {
                   
                    //Temporary solution to update navigator lists
                    JTextArea m_active; JTextArea m_inactive;
	/**
	 * Creates a new comment controller.
	 *
	 */
	public NavigatorExtensionController() {

	}

	public void setText(JTextArea active, JTextArea inactive) {
                                    m_active = active; m_inactive = inactive;
		Extension[] extensions = ExtensionManager.getInstance().getExtensions();
		String strActive = "";
		String strInactive = "";

		for (int i = 0; i < extensions.length; i++) {
			if (extensions[i].isEnabled()) {
				strActive += extensions[i].getName();
				strActive += "\n";
			} else {
				strInactive += extensions[i].getName();
				strInactive += "\n";
			}
		}

		m_active.setText(strActive);
		m_inactive.setText(strInactive);
	}
        
                  public void updateText(){
                      Extension[] extensions = ExtensionManager.getInstance().getExtensions();
		String strActive = "";
		String strInactive = "";

		for (int i = 0; i < extensions.length; i++) {
			if (extensions[i].isEnabled()) {
				strActive += extensions[i].getName();
				strActive += "\n";
			} else {
				strInactive += extensions[i].getName();
				strInactive += "\n";
			}
		}

		m_active.setText(strActive);
		m_inactive.setText(strInactive);
                  }  
}
