/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.calendar.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author André Garrido - 1101598
 */
public class CalendarAction extends BaseAction {

	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public CalendarAction(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 *
	 * @return Action Name
	 */
	protected String getName() {
		return "Calendar Menu";
	}

	/**
	 *
	 */
	protected void defineProperties() {
	}

	
	public void actionPerformed(ActionEvent event) {

		  new CalendarMainMenuUI().setVisible(true);
	}
}
