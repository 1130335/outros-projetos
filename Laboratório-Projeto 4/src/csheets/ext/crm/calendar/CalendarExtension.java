/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.calendar;

import csheets.ext.Extension;
import csheets.ext.crm.calendar.ui.UIExtensionCalendar;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Daniel Oliveira 
 */
public class CalendarExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Calendar";

	/**
	 * Creates a new Example extension.
	 */
	public CalendarExtension() {
		super(NAME);
	}

	/*
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionCalendar(this, uiController);
	}

}