/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.alerts.Alerts;
import java.util.List;

/**
 *
 * @author Paulo silva
 */
public class JPAAlertsRepository extends JPARepository<Alerts, Long>{

    
    public void saveAlert(Alerts a){
      super.save(a);
    }
    
    public void updateAlert(Alerts a){
        super.update(a);
        super.save(a);
    }
    
    public void deleteAlert(Alerts a){
        super.delete(a);
    }
    
    public List<Alerts> listAll(){
       return (super.all());
    }
            
    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }
    
}
