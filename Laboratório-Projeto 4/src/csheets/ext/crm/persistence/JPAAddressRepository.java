/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Address;
import java.util.List;

/**
 *
 * @author André Garrido - 1101598
 */
public class JPAAddressRepository extends JPARepository<Address, Long> {

	/**
	 * Saves a Contact's Address
	 *
	 * @param a type Address
	 */
	public void saveAddress(Address a) {
		super.save(a);
	}

	/**
	 * Edit a Contact's Address
	 *
	 * @param a type Address
	 */
	public void updateAddress(Address a) {
		super.update(a);
		super.save(a);
	}

	/**
	 * Remove a Contact's Address
	 *
	 * @param a type Address
	 */
	public void deleteAddress(Address a) {

		super.delete(a);
	}

	public List<Address> listAll() {
		return (super.all());
	}

	/**
	 *
	 * @return DB Name
	 */
	@Override
	protected String persistenceUnitName() {
		return "cleansheetsPU";
	}
}
