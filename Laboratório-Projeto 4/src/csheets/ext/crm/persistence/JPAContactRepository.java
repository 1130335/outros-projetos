/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Contact;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author daniel
 */
public class JPAContactRepository extends JPARepository<Contact, Long> {

    public JPAContactRepository() {

    }

    public List<Contact> AllContacts() {
        List<Contact> contactlist = Persistence.getJPARepositoryFactory().
                getJPAContactRepository().all();

        return contactlist;
    }

    public List<Class> getChildClasses() {
        List<Class> classes = new ArrayList<>();
        List<Contact> contacts = AllContacts();
        for (Contact cont : contacts) {
            classes.add(cont.getClass());
        }
        Set<Class> hs = new HashSet<>();
        hs.addAll(classes);
        classes.clear();
        classes.addAll(hs);
        return classes;
    }

    public List<Contact> getAllContactsByType(Class c) {
        List<Contact> contacts = AllContacts();
        List<Contact> result = new ArrayList<>();
        for (Contact cont : contacts) {
            if (cont.getClass().equals(c)) {
                result.add(cont);
            }
        }
        return result;
    }

    public Class getClassByName(String type) {
        List<Class> list_class = getChildClasses();
        Class result = null;
        for (Class list_clas : list_class) {
            if ((list_clas.getSimpleName()).equals(type)) {
                result=list_clas;
            }
        }
        return result;
    }

    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }

}
