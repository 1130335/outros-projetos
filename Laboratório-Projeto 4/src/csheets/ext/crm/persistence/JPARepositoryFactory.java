/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

/**
 *
 * @author André Garrido - 1101598
 */
public class JPARepositoryFactory {

	public JPAPersonRepository getJPAPersonRepository() {
		return new csheets.ext.crm.persistence.JPAPersonRepository();
	}

	public JPACompanyRepository getJPACompanyRepository() {
		return new csheets.ext.crm.persistence.JPACompanyRepository();
	}

	public JPAEventsRepository getJPAEventsRepository() {
		return new csheets.ext.crm.persistence.JPAEventsRepository();
	}

	public JPAAddressRepository getJPAAddressRepository() {
		return new csheets.ext.crm.persistence.JPAAddressRepository();
	}
        
        public JPACalendarRepository getJPACalendarRepository() {
		return new csheets.ext.crm.persistence.JPACalendarRepository();
	}
        
	public JPAEmailandPhoneNumberRepository getJPAEmailandPhoneNumberRepository() {
		return new csheets.ext.crm.persistence.JPAEmailandPhoneNumberRepository();
	}

    public JPAContactRepository getJPAContactRepository() {
		return new csheets.ext.crm.persistence.JPAContactRepository();
	}
    public JPAAlertsRepository getJPAAlertsRepository() {
		return new csheets.ext.crm.persistence.JPAAlertsRepository();
	}
        

}
