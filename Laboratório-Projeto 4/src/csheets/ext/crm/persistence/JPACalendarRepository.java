/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Contact;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author André Garrido<1101598>
 */
public class JPACalendarRepository extends JPARepository<Calenda, Long> {

    public JPACalendarRepository() {

    }

    public void addCalendar(Calenda c) {
        System.out.println("*****NEW CALENDAR******");
        System.out.println("Name: " + c.getName());
        System.out.println("Description: " + c.getDescription());
        super.add(c);
    }

    public void updateCalendar(Calenda calendar) {

        Calenda detached = super.findById(calendar.getId());
        detached = super.update(calendar);
        Calenda over = super.findById(detached.getId());
        super.save(over);
        

    }

    public Calenda getCalendarByIndex(int index) {
        List<Calenda> calendarlist = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository().all();

        Calenda calendar = calendarlist.get(index);
        return calendar;
    }

    public void removeCalendar(Calenda c) {
        
        super.delete(c);
    }

    public List<Calenda> AllCalendars() {
        List<Calenda> calendarlist = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository().all();
        return calendarlist;
    }
    
    
    

    public List<Calenda> AllCalendarsByContact(Contact c) {
        List<Calenda> calendarList = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository().all();

        List<Calenda> resultList = new ArrayList<>();
        for (Calenda cal : calendarList) {
            if (cal.getContact().equals(c)) {
                resultList.add(cal);

            }

        }

        return resultList;
    }
    
    
    

    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }

    public void removeCalendar(Calendar calendar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    


}
