/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author André Garrido - 1101598
 */
public final class Persistence {

	public static JPARepositoryFactory getJPARepositoryFactory() {

		return new JPARepositoryFactory();

	}

	private Persistence() {
	}

	@Deprecated
	public static EntityManager EntityManagerInit() {
		EntityManagerFactory factory = javax.persistence.Persistence.
			createEntityManagerFactory("cleansheetsPU");
		EntityManager manager = factory.createEntityManager();
		return manager;
	}
}
