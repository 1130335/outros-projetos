/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.domain.Person;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author André Garrido<1101598>
 */
public class JPAPersonRepository extends JPARepository<Person, Long> {

    public JPAPersonRepository() {

    }

    public void addContact(Person c) {
        System.out.println("*****NEW CONTACT******");
        System.out.println("Name: " + c.getFirstName() + " " + c.getLastName());
        System.out.println("Image link: " + c.getImage());
        super.save(c);
    }

    public void editContact(Person contact, String firstName, String lastName,
            byte[] image, String profission) {

        Person detached = super.findById(contact.getId());
        detached.setFirstName(firstName);
        detached.setLastName(lastName);
        detached.setProfession(profission);
        detached.setImage(image);

        detached = super.update(detached);

        Person over = super.findById(contact.getId());
        super.save(over);

    }

    public void editContact(Person contact) {

        Person detached = super.findById(contact.getId());

        detached = super.update(detached);
        super.save(detached);

//		Person over = super.findById(contact.getId());
//		super.save(over);
    }

    public void editContact(Person contact, String firstName, String lastName,
            byte[] image, String profission, Company company) {

        Person detached = super.findById(contact.getId());
        detached.setFirstName(firstName);
        detached.setLastName(lastName);
        detached.setProfession(profission);
        detached.setImage(image);
        detached.setCompanyRelated(company);

        detached = super.update(detached);

        Person over = super.findById(contact.getId());
        super.save(over);

    }

    public void editContact(Person contact_new, int index_old) {

        List<Person> contactlist = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository().all();

        Person contact_old = contactlist.get(index_old);
        contact_new.setId(contact_old.getId());
        super.update(contact_new);
        super.save(contact_new);
        super.delete(contact_old);

    }

    public Person getPersonByIndex(int index) {
        List<Person> contactlist = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository().all();
       

        Person contact = contactlist.get(index);
        return contact;
    }

    public void removeContact(int index) {
        List<Person> contactlist = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository().all();

        Person contact = contactlist.get(index);
        super.delete(contact);
    }

    public List<Person> AllContacts() {
        List<Person> contactlist = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository().all();
        return contactlist;
    }

    public List<Person> getAllContactsWithCalendar() {
        List<Calenda> calendarlist = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository().AllCalendars();
        List<Person> contactlist = AllContacts();
        List<Person> result = new ArrayList<>();
        for (Person p : contactlist) {
            for (Calenda cal : calendarlist) {
                if (cal.getContact().equals(p)) {
                    result.add(p);
                }

            }

        }
        return result;
    }

    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }

}
