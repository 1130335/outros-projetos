/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.domain.Person;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author André Garrido - 1101598
 */
public class JPAEventsRepository extends JPARepository<Event, Serializable> {

    public void addEvent(Event e) {
        System.out.println("*****NEW EVENT******");
        System.out.println("Date: " + e.getDate().toString());
        System.out.println("Description: " + e.getDescription());
        System.out.println("Client: " + e.getContact());
        super.save(e);
    }

    /**
     * @param e
     * @Method Update in the DB
     */
    public void updateEvents(Event e) {
        super.update(e);
        super.save(e);
    }
    
    public void updateEvent(Event event, Calenda cal) {
       Event aux= super.findById(event.getId());
       aux.setCalendar(cal);
       super.update(aux);
       super.save(aux);
    }

    /**
     * @param e
     * @Method Delete from the DB
     */
    public void deleteEvents(Event e) {
        super.delete(e);

    }
    /*
     *List all Events
     */

    public List<Event> listAllEvents() {
        return Persistence.getJPARepositoryFactory().getJPAEventsRepository().
                all();
    }

    public List<Event> AllEventsByContact(Contact c) {
        List<Event> eventlist = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository().all();
        List<Event> events_result = new ArrayList<>();

        for (Event el : eventlist) {
            if (el.getContact().equals(c)) {
                events_result.add(el);
            }
        }
        return events_result;

    }
    
   

    public List<Event> getEventsNotifications() {
        List<Event> all_events = listAllEvents();
        List<Event> events_to_notification = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.setTime(cal.getTime());

        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        for (int i = 0; i < all_events.size(); i++) {

            Calendar c = Calendar.getInstance();
            c.setTime(all_events.get(i).getDate());

            if ((c.get(Calendar.MONTH)) == month && c.get(Calendar.YEAR) == year && c.
                    get(Calendar.DATE)
                    == day && (all_events.get(i).getEventhour()== hour) && (all_events.
                    get(i).getEventminutes()== minutes)) {

                events_to_notification.add(all_events.get(i));
            }

        }

        return events_to_notification;

    }
    
    public List<Event> AllEventsOfCalendar(Calenda selected_calendar) {
       JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
       List<Event> events;
       events= listAllEvents();
       List<Event> events_of_calendar= new ArrayList<>();
        for (Event event : events) {
            if(event.getCalendar()!=null){
            if(event.getCalendar().equals(selected_calendar)){
                events_of_calendar.add(event);
            }
            }
        }
       return events_of_calendar;
    }

    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }

    

    

    

}
