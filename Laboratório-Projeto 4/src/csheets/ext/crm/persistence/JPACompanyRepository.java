/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Person;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author André Garrido<1101598>
 */
public class JPACompanyRepository extends JPARepository<Company, Long> {

    public JPACompanyRepository() {

    }

    public void addContact(Company c) {
        System.out.println("*****NEW COMPANY******");
        System.out.println("Des: " + c.getDesignation());
        System.out.println("Image link: " + c.getImage());
        super.save(c);
    }

    public void editContact(Company contact, String designation, byte[] image) {

        Company detached = super.findById(contact.getId());

        detached.setDesignation(designation);
        detached.setImage(image);

        detached = super.update(detached);

        Company over = super.findById(contact.getId());
        super.save(over);

    }

    public Company getCompanyByIndex(int index) {
        List<Company> contactlist = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository().all();

        Company contact = contactlist.get(index);
        return contact;
    }

    public boolean removeContact(int index) {
        List<Company> contactlist = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository().all();

        Company contact = contactlist.get(index);
        List<Person> aux = getPeopleRelated(contact);
        if (aux.isEmpty()) {
          
            super.delete(contact);
            return true;
        }
        return false;
    }

    public List<Company> AllContacts() {
        List<Company> contactlist = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository().all();

        return contactlist;
    }

    public List<Person> getPeopleRelated(Company company) {
        List<Person> contactlist = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository().all();
        List<Person> newList = new ArrayList<>();
        for (Person p : contactlist) {
            if (p.getCompanyRelated() != null) {
                if (p.getCompanyRelated().equals(company)) {

                    newList.add(p);
                }
            }
        }

        return newList;
    }
    
    
     public List<Company> getAllCompanyWithCalendar() {
         List<Calenda> calendarlist = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository().AllCalendars();
        List<Company> contactlist = AllContacts();
        List<Company> result = new ArrayList<>();
        for (Company c : contactlist) {
            for (Calenda cal : calendarlist) {
                if (cal.getContact().equals(c)) {
                    result.add(c);
                }

            }

        }
        return result;
    }

    @Override
    protected String persistenceUnitName() {
        return "cleansheetsPU";
    }

   

}
