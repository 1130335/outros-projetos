/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.tasks.task;
import java.util.List;


/**
 *
 * @author Aria
 */
public class JPARepositoryTasks extends JPARepository<task, Long> {
    /*
    * Save Task
    */
    public void saveTask(task t){
        super.save(t);
 
    }
    
    /*
    * Update Task
    */
    public void updateTask(task t){
        super.update(t);
        super.save(t);
    }
    
    /*
    * Delete Task
    */
    public void deleteTask(task t){
        super.delete(t);
    }
    
    /*
    *List all Tasks
    */
    
    public List<task> listAll(){
        return (super.all());
    }
    /*
    *
    */
    
    @Override
    protected String persistenceUnitName() {
        throw new UnsupportedOperationException("Task repository."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
