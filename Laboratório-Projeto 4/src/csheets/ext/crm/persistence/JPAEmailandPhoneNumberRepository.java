/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Email;
import csheets.ext.crm.domain.PhoneNumber;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class JPAEmailandPhoneNumberRepository extends JPARepository<PhoneNumber, Email> {

//	// Saves a Contact's Email
//	public void saveEmail(Email e) {
//		super.save(e);
//	}
//
//	// Edit a Contact's Email
//	public void updateEmail(Email e) {
//		super.update(e);
//		super.save(e);
//
//	}
//
//	// Remove a Contact's Email
//	public void removeEmail(Email e) {
//		super.delete(e);
//		super.save(e);
//
//	}
	// Saves a Contact's PhoneNumber
	public void savePhoneNumber(PhoneNumber pn) {
		super.save(pn);
	}

	// Edit a Contact's PhoneNumber

	public void updatePhoneNumber(PhoneNumber pn) {
		super.update(pn);
		super.save(pn);

	}

	// Remove a Contact's PhoneNumber
	public void removePhoneNumber(PhoneNumber pn) {
		super.delete(pn);
		super.save(pn);

	}

//	public List<Email> listAllEmails() {
//		return (super.all());
//	}
	public List<PhoneNumber> listAllPhoneNumbers() {
		return (super.all());
	}

	/**
	 *
	 * @return DB Name
	 */
	@Override
	protected String persistenceUnitName() {
		return "cleansheetsPU";
	}

}
