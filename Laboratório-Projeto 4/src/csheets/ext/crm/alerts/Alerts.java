/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.alerts;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Paulo silva
 */
@Entity
public class Alerts implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date time;

    public Alerts() {

    }

    public Alerts(String name, String description, Date time) {
        this.name = name;
        this.description = description;
        this.time = time;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return time.toString();
    }

    /**
     * get the id
     *
     * @param list
     * @param name
     * @return
     */
    public long getId(List<Alerts> list, String name) {
        long Id = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().compareToIgnoreCase(name) == 0) {
                Id = list.get(i).getId();
            }
        }
        return Id;
    }

    public Timestamp createTimestamp(Date dateDay, int hour, int minute) {

        Date date = dateDay;
        date.setHours(hour);
        date.setMinutes(minute);
        Timestamp time = new Timestamp(date.getTime());
        return time;
    }

    /**
     * validate the hours
     *
     * @param time
     * @return
     */
    public int validateAndReturnHour(String[] time) {
        int hour;
        try {
            hour = Integer.parseInt(time[0]);
            if (hour > 0 || hour < 23) {
                return hour;
            } else {
                return -1;
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return -1;
        }
    }

    /**
     * validate the minutes
     *
     * @param time
     * @return
     */
    public int validateAndReturnMinute(String[] time) {
        int minute;
        try {
            minute = Integer.parseInt(time[1]);
            if (minute > 0 || minute < 60) {
                return minute;
            } else {
                return -1;
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return -1;
        }

    }

    /**
     * Validate the date, must be in format: dd/mm/yyyy
     *
     * @param dates
     * @return
     */
    public static Date validateDate(String dates) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);
        try {
            date = sdf.parse(dates);
        } catch (ParseException ex) {
            System.err.printf("Error date");
        }
        return date;
    }

    /**
     * Will split the string time in 2 parts: hours and minutes
     *
     * @param time
     * @return
     */
    public String[] splitString(String time) {
        String[] string = new String[2];
        String s;
        s = time;
        try {
            string = s.split(Pattern.quote(":"));
            return string;
        } catch (NumberFormatException ex) {
            return null;

        }
    }
}
