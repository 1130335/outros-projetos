/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.alerts.controller;

import csheets.ext.crm.alerts.Alerts;
import csheets.ext.crm.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulo silva
 */
public class AlertController {

    JPAAlertsRepository repo;
    Alerts alert = new Alerts();

    public AlertController() {
        repo = Persistence.getJPARepositoryFactory().getJPAAlertsRepository();
    }

    /**
     * save an alert
     *
     * @param a
     */
    public void addAlert(Alerts a) {
        repo.save(a);
    }

    public JPAAlertsRepository getRepo() {
        return repo;
    }

    /**
     * update an alert
     *
     * @param a
     */
    public void updateAlert(Alerts a) {
        repo.update(a);
    }

    /**
     * delete an alert
     *
     * @param a
     */
    public void deleteAlert(Alerts a) {
        repo.deleteAlert(a);
    }

    /**
     * list all alerts
     *
     * @return
     */
    public List<Alerts> listAll() {
//                   EntityManagerFactory emf =
// javax.persistence.Persistence.createEntityManagerFactory("cleansheetsPU");
// EntityManager em = emf.createEntityManager();
// Alerts p1 = new Alerts("ASD", "ASD", new Date(2020, 10, 5));
// em.getTransaction().begin();
// em.persist(p1);
// em.getTransaction().commit();
// System.out.println("ID gerado: " + p1.getId());
		// entityClass.getAnnotation(Table.class).name();
//Query q = em.createQuery("SELECT it FROM  Alerts it");
//List<Alerts> all = q.getResultList();
// em.close();
// emf.close();
        
        return (repo.all());

    }

    /**
     * get the ID calling the method of class alerts
     *
     * @param list
     * @param name
     * @return
     */
    public long getID(List<Alerts> list, String name) {
        return alert.getId(list, name);
    }

    public Alerts getById(long id) {
        return (Alerts) repo.findById(id);
    }

    /**
     * calls the method from alert class
     *
     * @param dateDay
     * @param hr
     * @param min
     * @return
     */
    public Timestamp creationProcess(Date dateDay, int hr, int min) {
        return alert.createTimestamp(dateDay, hr, min);
    }

    /**
     * get the hour calling the method of class alerts
     *
     * @param time
     * @return
     */
    public int validateAndReturnHour(String[] time) {
        return alert.validateAndReturnHour(time);
    }

    /**
     * get the minutes calling the method of class alerts
     *
     * @param time
     * @return
     */
    public int validateAndReturnMinute(String[] time) {
        return alert.validateAndReturnMinute(time);
    }

    /**
     * validate date calling the method of class alerts
     *
     * @param dates
     * @return
     */
    public Date validateDate(String dates) {
        return alert.validateDate(dates);
    }

    /**
     * split strings calling the method of class alerts
     *
     * @param time
     * @return
     */
    public String[] splitString(String time) {
        return alert.splitString(time);
    }

    public Alerts createNewAlert(String name, String description, String dateDay, String timehour) {
        Date date = validateDate(dateDay);

        if (date == null) {
            JOptionPane.showMessageDialog(null, "Invalid timestamp, Date error");
            return null;
        } else {
            String[] split = new String[2];
            split = alert.splitString(timehour);
            if (split == null) {
                return null;
            } else {
                int hr = alert.validateAndReturnHour(split);
                int min = alert.validateAndReturnMinute(split);
                if (min == -1 || hr == -1) {
                    JOptionPane.showMessageDialog(null, "Invalid timestamp, Time error");
                    return null;
                } else {
                    Timestamp time = alert.createTimestamp(date, hr, min);
                    Alerts a = new Alerts(name, description, time);
                    repo.add(a);
                    return a;

                }
            }
        }
    }

    /**
     * Method that updates an existing alert
     *
     * @param list
     * @param nome1
     * @param name
     * @param desc
     * @param dateDay
     * @param timeHour
     * @return the alert
     */
    public Alerts updateExistingAlert(List<Alerts> list, String nome1, String name, String desc, String dateDay, String timeHour) {
        long idA;
        idA = alert.getId(list, nome1);

        String[] splitted = new String[2];
        splitted = alert.splitString(timeHour);
        int hr = alert.validateAndReturnHour(splitted);
        int min = alert.validateAndReturnMinute(splitted);
        if (min == -1 || hr == -1) {
            JOptionPane.showMessageDialog(null, "Invalid timestamp, Time error");
            return null;
        } else {
            Date date = new Date();
            date = alert.validateDate(dateDay);

            if (date == null) {
                JOptionPane.showMessageDialog(null, "Invalid timestamp, Date error");
                return null;
            } else {
                Timestamp time = alert.createTimestamp(date, hr, min);
                Alerts a = this.getById(idA);
                a.setName(name);
                a.setDescription(desc);
//                a.setTime(time);
                return a;
            }
        }
    }

    public void updateExistingAlerte(Alerts a, String nome1, String name, String desc, String dateDay, String timeHour) {
        a.setName(name);
        a.setDescription(desc);
//        a.setTime(new Date());
    }

}
