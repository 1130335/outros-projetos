/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author André Garrido - 1101598
 */
@Entity
public class Event implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private Contact contact;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    protected int eventhour;
    protected int eventminutes;
    protected int eventhour_end;
    protected int eventminutes_end;
    private String description;
    private Calenda calendar;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date end;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date duration;

    protected Event() {
    }

    public Event(Contact contact, Date date, int hour, int minutes,
            String description,Calenda calenda, Date end,int eventhour_end, int eventminutes_end) {
        this.contact = contact;
        this.date = date;
        this.eventhour = hour;
        this.eventminutes = minutes;
        this.description = description;
        this.calendar= calenda;
        this.end=end;
        this.eventhour_end=eventhour_end;
        this.eventminutes_end=eventminutes_end;

    }

    public void setEventhour(int eventhour) {
        this.eventhour = eventhour;
    }

    public int getEventhour() {
        return eventhour;
    }

   

    public String getDescription() {
        return description;
    }

    public void setCalendar(Calenda calendar) {
        this.calendar = calendar;
    }

    public Calenda getCalendar() {
        return calendar;
    }

    public String toString() {

        return this.description;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }
    
    
    public void setEnd(Date end) {
        this.end = end;
    }

    public void setEventhour_end(int eventhour_end) {
        this.eventhour_end = eventhour_end;
    }

    public int getEventminutes_end() {
        return eventminutes_end;
    }

    public Date getDate() {
        return date;
    }

    public Date getEnd() {
        return end;
    }

    public int getEventhour_end() {
        return eventhour_end;
    }

    public int getEventminutes() {
        return eventminutes;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
    
    

    /**
     * @return the contact
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

   
     @Override
    public boolean equals(Object obj) {
        Event obj_new=(Event)obj;
        return (this.toString()+this.id).equals(obj_new.toString()+obj_new.id); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
