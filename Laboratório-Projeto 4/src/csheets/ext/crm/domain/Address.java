/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author André Garrido - 1101598
 */
@Entity
public class Address implements Serializable {

	/**
	 *
	 */
	@Id
	@GeneratedValue
	private int id;
	private String street1;
	private String area1;
	private String zipCode1;
	private String city1;
	private String country1;

	/**
	 * Construtor vazio
	 *
	 */
	protected Address() {

	}

	/**
	 * Construtor com todos os atributos
	 *
	 */
	public Address(String street, String area, String zipCode, String city,
				   String country) {

		this.street1 = street;
		this.area1 = area;
		this.zipCode1 = zipCode;
		this.city1 = city;
		this.country1 = country;
	}

	/**
	 * Metodo para que seja retornado o atributo id
	 */
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	/**
	 * Metodo para que seja retornado o atributo street
	 */
	public String getStreet() {
		return street1;
	}

	/**
	 * Metodo para que seja retornado o atributo area
	 */
	public String getArea() {
		return area1;
	}

	/**
	 * Metodo para que seja retornado o atributo zipCode
	 *
	 */
	public String getZipCode() {
		return zipCode1;
	}

	/**
	 * Metodo para que seja retornado o atributo city
	 *
	 */
	public String getCity() {
		return city1;
	}

	/**
	 * Metodo para que seja retornado o atributo country
	 *
	 */
	public String getCountry() {
		return country1;
	}

	public String toString() {
		return "Street: " + getStreet() + " Area: " + getArea() + " ZipCode: " + getZipCode() + " City: " + getCity() + " Country: " + getCountry();
	}
}
