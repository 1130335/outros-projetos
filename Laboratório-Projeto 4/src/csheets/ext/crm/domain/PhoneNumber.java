/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Miguel
 */
@Entity
public class PhoneNumber implements Serializable {

	/**
	 *
	 */
	@Id
	@GeneratedValue
	private long id;
	private int PersonalNumber;
	private int JobNumber;
	private int JobPersonalNumber;
	private int HomeNumber;

	public PhoneNumber() {
	}

	public PhoneNumber(int PersonalNumber, int JobNumber,
					   int JobPersonalNumber, int HomeNumber) {

		this.PersonalNumber = PersonalNumber;
		this.JobNumber = JobNumber;
		this.JobPersonalNumber = JobPersonalNumber;
		this.HomeNumber = HomeNumber;
	}

	public long getId() {
		return id;
	}

	public int getPersonalNumber() {
		return PersonalNumber;
	}

	public int getJobNumber() {
		return JobNumber;
	}

	public int getJobPersonalNumber() {
		return JobPersonalNumber;
	}

	public int getHomeNumber() {
		return HomeNumber;
	}

}
