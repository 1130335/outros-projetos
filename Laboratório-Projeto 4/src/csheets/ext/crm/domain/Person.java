/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author André Garrido<1101598>
 */
@Entity
public class Person extends Contact implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	private byte[] image;
	private Company companyRelated;
	private String profession;
	private Address mainAddress;
	private Address secAddress;
	private Email email;
	private PhoneNumber pn;

	/**
	 *
	 */
	public Person() {
	}

	/**
	 *
	 * @param fn
	 * @param ln
	 *
	 */
	public Person(String fn, String ln) {
		firstName = fn;
		lastName = ln;
		image = null;
		mainAddress = null;
		secAddress = null;
		email = null;
		pn = null;
		setContact_id(id);

	}

	/**
	 *
	 * @param fn
	 * @param ln
	 * @param img
	 * @param pro
	 * @param compRel
	 */
	public Person(String fn, String ln, byte[] img, String pro, Company compRel,
				  Address mainAddr, Address secAddr) {
		firstName = fn;
		lastName = ln;
		image = img;
		mainAddress = mainAddr;
		secAddress = secAddr;
		profession = pro;
		companyRelated = compRel;
		setContact_id(id);
		email = null;
		pn = null;
	}

	/**
	 *
	 * @param fn
	 * @param ln
	 * @param img
	 * @param pro
	 * @param compRel
	 */
	public Person(String fn, String ln, byte[] img, String pro) {
		firstName = fn;
		lastName = ln;
		image = img;
		mainAddress = null;
		secAddress = null;
		profession = pro;
		companyRelated = null;
		setContact_id(id);
		email = null;
		pn = null;
	}

	/**
	 *
	 * @param fn
	 * @param ln
	 * @param img
	 * @param addr1
	 * @param pro
	 * @param compRel
	 */
	public Person(String fn, String ln, byte[] img, Address addr1, String pro,
				  Company compRel) {
		firstName = fn;
		lastName = ln;
		image = img;
		mainAddress = addr1;
		secAddress = null;
		profession = pro;
		companyRelated = compRel;
		setContact_id(id);
		email = null;
		pn = null;
	}

	/**
	 *
	 * @param fn
	 * @param ln
	 * @param img
	 * @param addr1
	 * @param addr2
	 * @param pro
	 * @param compRel
	 */
	public Person(String fn, String ln, byte[] img, Address addr1, Address addr2,
				  String pro, Company compRel, Email em1, PhoneNumber phone) {
		firstName = fn;
		lastName = ln;
		image = img;
		mainAddress = addr1;
		secAddress = addr2;
		profession = pro;
		companyRelated = compRel;
		email = em1;
		pn = phone;
		setContact_id(id);

	}

	public Person(String fn, String ln, Address addr1, Address addr2) {
		firstName = fn;
		lastName = ln;
		mainAddress = addr1;
		secAddress = addr2;
		image = null;
		email = null;
		pn = null;
		setContact_id(id);
	}

	public Person(String fn, String ln, Address addr1) {
		firstName = fn;
		lastName = ln;
		mainAddress = addr1;
		secAddress = null;
		image = null;
		email = null;
		pn = null;
		setContact_id(id);
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @return
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Address getMainAddress() {
		return this.mainAddress;
	}

	public Address getSecAddress() {
		return this.secAddress;
	}

	public void insertEmails(Email allEmails) {
		this.email = allEmails;
	}

	public void insertPhoneNumbers(PhoneNumber allPhones) {
		this.pn = allPhones;
	}

	public void insertMainAddress(Address addr) {
		this.mainAddress = addr;
	}

	public void insertSecAddress(Address addr) {
		this.secAddress = addr;
	}

	public Email getEmail() {
		return this.email;
	}

	public PhoneNumber getPhoneNumber() {
		return this.pn;
	}

	public Company getCompanyRelated() {
		return this.companyRelated;
	}

	public void setCompanyRelated(Company companyRelated) {
		this.companyRelated = companyRelated;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	/**
	 * @return a string with the first and last name of a contact
	 */
	@Override
	public String toString() {
		return super.toString() + getFirstName() + " " + getLastName();
	}

	@Override
	public boolean equals(Object obj) {
		return (this.toString() + this.getId()).equals(obj.toString() + this.
			getId()); //To change body of generated methods, choose Tools | Templates.
	}

}
