package csheets.ext.crm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author daniel
 */
@Entity
public class Contact implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long contact_id;

    /**
     *
     */
    public Contact() {
    }

    public void setContact_id(Long contact_id) {
        this.contact_id = contact_id;
    }

    public Long getContact_id() {
        return contact_id;
    }

    public Long getId() {
        return id;
    }
    
    /**
     *
     * @param fn
     * @param ln
     *
     */
    /**
     * @return a string with the first and last name of a contact
     */
    @Override
    public String toString() {
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }

}
