/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.awt.Color;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Calenda implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Contact contact;
    private Color color;

    public Calenda() {

    }

    public Calenda(String name, String description, Contact contact, Color color) {
        this.name = name;
        this.description = description;
        this.contact = contact;
        this.color = color;

    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    public Color getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public Contact getContact() {
        return contact;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return this.name; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        Calenda obj_new=(Calenda)obj;
        return (this.toString()+this.id).equals(obj_new.toString()+obj_new.id); //To change body of generated methods, choose Tools | Templates.
    }

}
