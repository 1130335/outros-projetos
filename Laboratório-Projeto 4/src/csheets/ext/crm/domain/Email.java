/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Miguel
 */
@Entity
public class Email implements Serializable {

	/**
	 *
	 */
	@Id
	@GeneratedValue
	private long id;
	private String email1;
	private String email2;
	private String email3;

	public Email() {
	}

	public Email(String email1, String email2, String email3) {
		this.email1 = email1;
		this.email2 = email2;
		this.email3 = email3;
	}

	public Email(String email1, String email2) {
		this.email1 = email1;
		this.email2 = email2;
		this.email3 = "";
	}

	public Email(String email1) {
		this.email1 = email1;
		this.email2 = "";
		this.email3 = "";
	}

	public long getId() {
		return id;
	}

	public String getEmail1() {
		return email1;
	}

	public String getEmail2() {
		return email2;
	}

	public String getEmail3() {
		return email3;
	}

}
