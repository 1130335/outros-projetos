/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Company extends Contact implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String designation;
    private byte[] image;
    private Address mainAddress;
    private Address secAddress;
    
    public Company() {
        setContact_id(id);
    }
    

    /**
     *
     * @param des
     * @param ln
     * @param img
     * @param people
     */
    public Company(String des, String ln, byte[] img) {
        designation = des;
        image = img;
        mainAddress = null;
        secAddress = null;
        setContact_id(id);
      
    }

    /**
     *
     * @param des
     * @param img
     * @param addr1
     */
    public Company(String des, byte[] img, Address addr1) {
        designation = des;
        image = img;
        mainAddress = addr1;
        secAddress = null;
        setContact_id(id);

    }
    
    /**
     *
     * @param des
     * @param img
     */
    public Company(String des, byte[] img) {
        designation = des;
        image = img;
        secAddress = null;
        setContact_id(id);

    }


    /**
     *
     * @param des
     * @param img
     * @param addr1
     * @param addr2
     */
    public Company(String des, byte[] img, Address addr1, Address addr2) {
        designation = des;
        image = img;
        mainAddress = addr1;
        secAddress = addr2;
        setContact_id(id);
   
    }

    /**
     *
     * @param designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     *
     * @return 
     */
    public String getDesignation() {
        return designation;
    }

    /**
     *
     * @return    
     */
    public byte[] getImage() {
        return image;
    }

    /**
     *
     * @return     
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id     
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return     
     */
    public Address getMainAddress() {
        return this.mainAddress;
    }

    /**
     *
     * @return     
     */
    public Address getSecAddress() {
        return this.secAddress;
    }

    /**
     *
     * @param addr  
     */
    public void insertMainAddress(Address addr) {
        this.mainAddress = addr;
    }

    /**
     *
     * @param addr     
     */
    public void insertSecAddress(Address addr) {
        this.secAddress = addr;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public void setSecAddress(Address secAddress) {
        this.secAddress = secAddress;
    }
    
    
    

    /**
     * @return a string with the first and last name of a contact
     */
    @Override
    public String toString() {
        return super.toString()+getDesignation();
    }
    
    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString()); //To change body of generated methods, choose Tools | Templates.
    }

}
