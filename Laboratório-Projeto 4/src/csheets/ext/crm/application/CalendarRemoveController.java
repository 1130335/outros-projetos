/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.persistence.JPACalendarRepository;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.JPAEventsRepository;
import csheets.ext.crm.persistence.Persistence;

import java.util.List;

public class CalendarRemoveController {

    public CalendarRemoveController() {

    }

    public List<Contact> getAllContactsByType(Class c) {
        JPAContactRepository repo = Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getAllContactsByType(c);
    }

    public Class getClassByName(String type) {
        JPAContactRepository repo = Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getClassByName(type);
    }

    public List<Calenda> getAllCalendarsByContact(Contact c) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        return repo.AllCalendarsByContact(c);
    }

    public Calenda getCalendarByIndex(List<Calenda> calenda, int index) {
        return calenda.get(index);
    }

    public List<Event> getAllEventsOfCalendar(Calenda selected_calendar) {
        JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository();
        return repo.AllEventsOfCalendar(selected_calendar);
    }

    public void removeCalendar(Calenda c) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        repo.removeCalendar(c);

    }

    public void updateEvent(Event event, Calenda cal) {
        JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository();
        repo.updateEvent(event, cal);
    }

}
