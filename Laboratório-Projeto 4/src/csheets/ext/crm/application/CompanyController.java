/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPACompanyRepository;
import csheets.ext.crm.persistence.Persistence;
import java.util.List;

public class CompanyController {

    public CompanyController() {

    }

    public Company NewContact(String des, byte[] img) {
        Company newContact = new Company(des, img);
        return newContact;
    }

    public void saveContactDB(Company contact) {
        JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();
        repo.addContact(contact);
    }

    public List<Company> getallContacts() {

        JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();
        return repo.AllContacts();
    }

    public void editContact(Company contact, String design, byte[] image) {

        JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();
        repo.editContact(contact,design, image);
    }

    public boolean removeContact(int index) {
        JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();

        return repo.removeContact(index);
    }

    public Company getContactByid(long idContact) {
        JPACompanyRepository repo = new JPACompanyRepository();
        return (Company) repo.findById(idContact);
    }

    public List<Person> getPeopleRelated(Company company) {
        JPACompanyRepository repo = new JPACompanyRepository();
        return repo.getPeopleRelated(company);
    }

    public Company getContactByIndex(int index) {
        JPACompanyRepository repo = new JPACompanyRepository();
        return repo.getCompanyByIndex(index);

    }
}
