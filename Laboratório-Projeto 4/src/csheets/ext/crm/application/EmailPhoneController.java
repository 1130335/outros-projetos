/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Person;
import csheets.ext.crm.domain.PhoneNumber;
import csheets.ext.crm.persistence.JPAEmailandPhoneNumberRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class EmailPhoneController {

	JPAEmailandPhoneNumberRepository rep = Persistence.getJPARepositoryFactory().
		getJPAEmailandPhoneNumberRepository();

	public void EmailPhoneManagerController() {

	}

//	// Saves a Contact's Email
//	public void addEmail(Email e) {
//		rep.saveEmail(e);
//	}
//
//	// Edit a Contact's Email
//	public void updateEmail(Email e) {
//		rep.updateEmail(e);
//	}
//
//	// Remove a Contact's Email
//	public void removeEmail(Email e) {
//		rep.removeEmail(e);
//	}
//	// Saves a Contact's PhoneNumber
//	public void addPhoneNumber(PhoneNumber pn) {
//		rep.savePhoneNumber(pn);
//	}
	// Edit a Contact's PhoneNumber
	public void updatePhoneNumber(PhoneNumber pn) {
		rep.updatePhoneNumber(pn);
	}

	// Remove a Contact's PhoneNumber
	public void removePhoneNumber(PhoneNumber pn) {
		rep.removePhoneNumber(pn);
	}

//	 //procura o email do contato no repositorio
//	public Email getEmail(long id) {
//		return rep.findById(id);
//	}
	public List<Person> getallContacts() {

		return Persistence.getJPARepositoryFactory().getJPAPersonRepository().
			AllContacts();
	}

	public void editContact(Person contact, int index) {
		JPAPersonRepository rep_cont = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		rep_cont.editContact(contact, index);
	}
}
