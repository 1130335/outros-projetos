/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.persistence.JPACompanyRepository;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.JPAEventsRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import java.util.List;

/**
 *
 * @author André Garrido<1101598>
 */
public class EventsController {

    public EventsController() {

    }

    JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
            getJPAEventsRepository();

    public void addEvent(Event e) {
        repo.addEvent(e);

    }

    public Event getfirstEvent() {
        return (Event) repo.first();
    }

    public List<Event> getAllEvents() {
        return repo.listAllEvents();
    }

    public void updateEvent(Event e) {
        repo.update(e);
    }

    public void deleteEvent(Event e) {
        repo.delete(e);
    }

    public Event getEventsbyID(long id) {
        return (Event) repo.findById(id);
    }

    public List<Person> getContactsPerson() {
        return Persistence.getJPARepositoryFactory().getJPAPersonRepository().
                AllContacts();
    }

    public List<Company> getContactsCompany() {
        return Persistence.getJPARepositoryFactory().getJPACompanyRepository().
                AllContacts();
    }

    public Person getPersonByIndex(int index) {
        JPAPersonRepository repo = new JPAPersonRepository();
        return repo.getPersonByIndex(index);

    }
    
    public Company getCompanyByIndex(int index) {
        JPACompanyRepository repo = new JPACompanyRepository();
        return repo.getCompanyByIndex(index);

    }

    public List<Event> getEventsNotifications() {
        return repo.getEventsNotifications();
    }

    public Class getClassByName(String type) {
        JPAContactRepository repo = Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getClassByName(type);
    }

    public List<Contact> getAllContactsByType(Class c) {
        JPAContactRepository repo= Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getAllContactsByType(c);
    }
}
