/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.crm.ImportExport.Export_Address;
import csheets.ext.crm.ImportExport.Import_Address;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPAAddressRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author André Garrido<1101598>
 */
public class AddressController {

	JPAAddressRepository rep = Persistence.getJPARepositoryFactory().
		getJPAAddressRepository();
	protected UIController uiController;

	public void AddressManagerController() {

	}

	/**
	 * Saves a Contact's Address
	 *
	 * @param addr type Address
	 */
	public void saveAddress(Address addr) {
		rep.saveAddress(addr);
	}

	/**
	 * Edit a Contact's Address
	 *
	 * @param a type Address
	 */
	public void updateAddress(Address a) {
		rep.updateAddress(a);
	}

	/**
	 * Remove a Contact's Address
	 *
	 * @param a type Address
	 */
	public void deleteAddress(Address a) {
		rep.deleteAddress(a);
	}

	/**
	 * procura a morada do contato no repositorio
	 *
	 * @param id atributo de morada responsavel por identificar o contato
	 * @return morada do contato identificado pelo seu id
	 */
	public Address getAddress(long id) {
		return (Address) rep.findById(id);
	}

	public List<Person> getallContacts() {

		return Persistence.getJPARepositoryFactory().getJPAPersonRepository().
			AllContacts();
	}

	public void editContact(Person contact, int index) {
		JPAPersonRepository rep_cont = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		rep_cont.editContact(contact, index);
	}

	public void exportAddress_all(UIController uicontroller,
								  List<Person> allcontacts,
								  ArrayList<Boolean> fields) throws FormulaCompilationException {
		Export_Address EA = new Export_Address(uicontroller);
		EA.exportAddress_all(uicontroller, allcontacts, fields);
	}

	public void exportAddress_one(UIController uicontroller, Person contact,
								  ArrayList<Boolean> fields) throws FormulaCompilationException {
		Export_Address EA = new Export_Address(uicontroller);
		EA.exportAddress_one(uicontroller, contact, fields);
	}

	public void importContact(UIController uicontroller, Person contact) {
		Import_Address IA = new Import_Address(uicontroller);
		IA.ImportOneContact(contact);
	}

	public void importContacts(UIController uicontroller,
							   List<Person> allcontacts) {
		Import_Address IA = new Import_Address(uicontroller);
		IA.ImportContacts(allcontacts);
	}

	public void importContactAddress(UIController uicontroller,
									 List<Person> allcontacts) {
		Import_Address IA = new Import_Address(uicontroller);
		IA.ImportContactAddress(allcontacts);
	}

}
