/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.contacts.util.AccessConfigFile;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPACompanyRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import java.util.List;

public class PersonController {

	public PersonController() {

	}

	public Person NewContact(String fn, String ln, byte[] img, String pro,
							 Company compRel, Address mainAddr, Address secAddr) {
		Person newContact = new Person(fn, ln, img, pro, compRel, mainAddr, secAddr);
		return newContact;
	}

	public Person NewContact(String fn, String ln, byte[] img, String pro) {
		Person newContact = new Person(fn, ln, img, pro);
		return newContact;
	}

	public void saveContactDB(Person contact) {
		JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		repo.addContact(contact);
	}

	public List<Person> getallContacts() {

		JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		return repo.AllContacts();
	}

	public void editContact(Person contact, String firstName, String lastName,
							byte[] image, String profission) {
		JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		repo.editContact(contact, firstName, lastName, image, profission);
	}

	public void editContact(Person contact, String firstName, String lastName,
							byte[] image, String profission, Company company) {
		JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		repo.
			editContact(contact, firstName, lastName, image, profission, company);
	}

	public void removeContact(int index) {
		JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
			getJPAPersonRepository();
		repo.removeContact(index);
	}

	public Person getContactByid(long idContact) {
		JPAPersonRepository repo = new JPAPersonRepository();
		return (Person) repo.findById(idContact);
	}

	public String[] getProfissionList() {

		AccessConfigFile acf = new AccessConfigFile();

		return acf.getProfissionsList();
	}

	public List<Company> getCompanyList() {
		JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
			getJPACompanyRepository();

		return repo.AllContacts();
	}

	public Person getContactByIndex(int index) {
		JPAPersonRepository repo = new JPAPersonRepository();
		return repo.getPersonByIndex(index);

	}
}
