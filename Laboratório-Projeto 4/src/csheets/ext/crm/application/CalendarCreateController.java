/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.persistence.JPACalendarRepository;
import csheets.ext.crm.persistence.JPAContactRepository;

import csheets.ext.crm.persistence.Persistence;
import java.awt.Color;

import java.util.List;

public class CalendarCreateController {

    public CalendarCreateController() {

    }


    public Calenda NewCalendar(String name, String description, Contact contact, Color color) {
        Calenda new_calendar = new Calenda(name, description, contact, color);
        return new_calendar;
    }

    public void saveCalendarDB(Calenda calendar) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        repo.addCalendar(calendar);
    }

    
    public List<Contact> getAllContactsByType(Class c) {
        JPAContactRepository repo = Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getAllContactsByType(c);
    }

    public Class getClassByName(String type) {
        JPAContactRepository repo = Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getClassByName(type);
    }

    

    

    

}
