/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPACalendarRepository;
import csheets.ext.crm.persistence.JPACompanyRepository;
import csheets.ext.crm.persistence.JPAEventsRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import java.awt.Color;

import java.util.List;

public class CalendarController {

    public CalendarController() {

    }

    public Calenda NewCalendar(String name, String description, Contact contact, Color color) {
        Calenda new_calendar = new Calenda(name, description, contact, color);
        return new_calendar;
    }

    public void saveCalendarDB(Calenda calendar) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        repo.addCalendar(calendar);
    }

    public List<Calenda> getAllCalendars() {

        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        return repo.AllCalendars();
    }

    public List<Calenda> getAllCalendarsByContact(Contact c) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        return repo.AllCalendarsByContact(c);
    }

    public List<Event> getAllEventsByContact(Contact p) {
        JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository();
        return repo.AllEventsByContact(p);
    }

    public void updateCalendar(Calenda calendar) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        repo.updateCalendar(calendar);
    }

    public Calenda getCalendarByIndex(int index) {
        JPACalendarRepository repo = new JPACalendarRepository();
        return repo.getCalendarByIndex(index);

    }

    public List<Person> getAllContactsPerson() {

        JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository();
        return repo.AllContacts();
    }

    public List<Company> getAllContactsCompany() {

        JPACompanyRepository repo = Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();
        return repo.AllContacts();
    }

    public Person getContactPersonByIndex(int index) {
        JPAPersonRepository repo = new JPAPersonRepository();
        return repo.getPersonByIndex(index);

    }

    public Company getContactCompanyByIndex(int index) {
        JPACompanyRepository repo = new JPACompanyRepository();
        return repo.getCompanyByIndex(index);

    }

    public Calenda getCalendarByIndex(List<Calenda> calenda, int index) {
        return calenda.get(index);
    }

    public List<Event> getAllEventsOfCalendar(Calenda selected_calendar) {
        JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository();
        return repo.AllEventsOfCalendar(selected_calendar);
    }

    public void updateEvent(Event event, Calenda cal) {
        JPAEventsRepository repo = Persistence.getJPARepositoryFactory().
                getJPAEventsRepository();
        repo.updateEvent(event, cal);
    }

    public List<Person> getAllContactsPersonWithCalendar() {
        JPAPersonRepository repo = Persistence.getJPARepositoryFactory().
                getJPAPersonRepository();
        return repo.getAllContactsWithCalendar();
    }

    public List<Company> getAllContactsCompanyWithCalendar() {
        JPACompanyRepository repo =  Persistence.getJPARepositoryFactory().
                getJPACompanyRepository();
        return repo.getAllCompanyWithCalendar();
    }

    public void removeCalendar(Calenda calendar) {
        JPACalendarRepository repo = Persistence.getJPARepositoryFactory().
                getJPACalendarRepository();
        repo.removeCalendar(calendar);

    }

}
