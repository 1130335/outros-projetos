/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;

import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.Persistence;
import java.util.List;

/**
 *
 * @author daniel
 */
public class ContactController {
    
    public ContactController(){
    
    }
    public List<Contact> getallContacts() {

        JPAContactRepository repo = Persistence.getJPARepositoryFactory().
                getJPAContactRepository();
        return repo.AllContacts();
    }
}
