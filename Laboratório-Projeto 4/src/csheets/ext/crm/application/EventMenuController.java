/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.application;


import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPACompanyRepository;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;


import java.util.List;

public class EventMenuController {

    public EventMenuController() {

    }

    
    public List<Person> getAllContactsPersonWithCalendar() {
        JPAPersonRepository repo = new JPAPersonRepository();
        return repo.getAllContactsWithCalendar();
    }
    
    public List<Company> getAllContactsCompanyWithCalendar() {
        JPACompanyRepository repo = new JPACompanyRepository();
        return repo.getAllCompanyWithCalendar();
    }

    public List<Class> getChildClasses() {
        JPAContactRepository repo= Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getChildClasses();
    }

    
    public List<Contact> getAllContactsByType(Class c) {
        JPAContactRepository repo= Persistence.getJPARepositoryFactory().getJPAContactRepository();
        return repo.getAllContactsByType(c);
    }
   

}
