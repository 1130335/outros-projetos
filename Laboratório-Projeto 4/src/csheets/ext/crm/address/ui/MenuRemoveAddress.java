/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.address.ui;

import csheets.ext.crm.application.AddressController;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Person;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author André Garrido - 1101598
 */
public class MenuRemoveAddress extends javax.swing.JFrame {

	AddressController addrctr = new AddressController();
	List<Person> allcontacts = addrctr.getallContacts();
	Person contact = new Person();
	long id = -1;

	/**
	 * Creates new form MenuRemoveAddress
	 */
	public MenuRemoveAddress() {
		if (allcontacts.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please create a contact first");
			dispose();
		} else {
			initComponents();
			Object[] test = new Object[allcontacts.size()];
			for (int i = 0; i < allcontacts.size(); i++) {
				String cont = allcontacts.get(i).toString();
				test[i] = (Object) cont;
			}
			list_contacts.setListData(test);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        label_addresslist = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        list_address = new javax.swing.JList();
        remove_button = new javax.swing.JButton();
        removeAll_button = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        list_contacts = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Lucida Fax", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Contact Address Manager");

        jLabel2.setText("Please choose the contact:");

        label_addresslist.setText("Please choose the address to remove");

        list_address.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_addressMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(list_address);

        remove_button.setText("Remove");
        remove_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                remove_buttonMouseClicked(evt);
            }
        });

        removeAll_button.setText("Remove All");
        removeAll_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeAll_buttonMouseClicked(evt);
            }
        });

        list_contacts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_contactsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(list_contacts);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(remove_button, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(removeAll_button, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label_addresslist, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(37, 37, 37))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label_addresslist)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(remove_button, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(removeAll_button, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void remove_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remove_buttonMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		contact = allcontacts.get(index);

//		long addr_index = (long) list_address.locationToIndex(evt.getPoint());
//
//		addrctr.deleteAddress(addrctr.getAddress(addr_index));
		String selected_addr = list_address.getSelectedValue().toString();
		if (selected_addr.equals("MainAddress")) {
			Address addr = contact.getMainAddress();
			contact.insertMainAddress(null);
			addrctr.editContact(contact, index);
			addrctr.deleteAddress(addr);
			JOptionPane.
				showMessageDialog(null, "MainAddress Removed");
			dispose();

		}
		if (selected_addr.equals("SecondaryAddress")) {
			Address addr = contact.getSecAddress();
			contact.insertSecAddress(null);
			addrctr.editContact(contact, index);
			addrctr.deleteAddress(addr);
			JOptionPane.
				showMessageDialog(null, "SecondaryAddress Removed");
			dispose();

		}
    }//GEN-LAST:event_remove_buttonMouseClicked

    private void removeAll_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeAll_buttonMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		contact = allcontacts.get(index);

		Address addr = contact.getMainAddress();
		contact.insertMainAddress(null);
		addrctr.editContact(contact, index);
		addrctr.deleteAddress(addr);

		Address secaddr = contact.getSecAddress();
		contact.insertSecAddress(null);
		addrctr.editContact(contact, index);
		addrctr.deleteAddress(secaddr);
		JOptionPane.
			showMessageDialog(null, "Both Addresses Removed");
		dispose();

    }//GEN-LAST:event_removeAll_buttonMouseClicked

    private void list_contactsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_contactsMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		contact = allcontacts.get(index);

		Address mainAddress = contact.getMainAddress();
		Address secAddress = contact.getSecAddress();

		Object[] test = new Object[2];
		String main = "MainAddress";
		String sec = "SecondaryAddress";
		if (mainAddress != null) {
			test[0] = (Object) main;
		}
		if (secAddress != null) {
			test[1] = (Object) sec;
		}

		list_address.setListData(test);

    }//GEN-LAST:event_list_contactsMouseClicked

    private void list_addressMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_addressMouseClicked

    }//GEN-LAST:event_list_addressMouseClicked

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.
				getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.
				getLogger(MenuRemoveAddress.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.
				getLogger(MenuRemoveAddress.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.
				getLogger(MenuRemoveAddress.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.
				getLogger(MenuRemoveAddress.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MenuRemoveAddress().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label_addresslist;
    private javax.swing.JList list_address;
    private javax.swing.JList list_contacts;
    private javax.swing.JButton removeAll_button;
    private javax.swing.JButton remove_button;
    // End of variables declaration//GEN-END:variables
}
