/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.address.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.crm.application.AddressController;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Person;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author André Garrido - 1101598
 */
public class ImportUI extends javax.swing.JFrame {

	private final UIController uicontroller;
	AddressController addrctr = new AddressController();
	List<Person> allcontacts = new ArrayList();
	private Spreadsheet ss;
	Cell cl;
	int col, row;

	String fn, ln, street1, area1, zipcode1, city1, country1;
	String street2, area2, zipcode2, city2, country2;

	/**
	 * Creates new form ImportUI
	 *
	 * @param uicontroller
	 */
	/*
	 */
	public ImportUI(UIController uicontroller) {
		this.uicontroller = uicontroller;
		initComponents();
		setLocationRelativeTo(null);

		bt_group.add(bt_person);
		bt_group.add(bt_personAddr);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bt_group = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        bt_person = new javax.swing.JRadioButton();
        bt_personAddr = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        text_begin = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        text_end = new javax.swing.JTextField();
        bt_import = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Lucida Fax", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Import Contact/Address Manager");

        bt_person.setText("Person");
        bt_person.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_personActionPerformed(evt);
            }
        });

        bt_personAddr.setSelected(true);
        bt_personAddr.setText("Person + Address");

        jLabel2.setText("Please select the range of import:");

        text_begin.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel3.setText("to");

        text_end.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        bt_import.setText("IMPORT");
        bt_import.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bt_importMouseClicked(evt);
            }
        });
        bt_import.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_importActionPerformed(evt);
            }
        });

        jLabel4.setText("Please Select an option:");

        jLabel5.setText("From Line: ");

        jLabel6.setForeground(new java.awt.Color(255, 0, 0));
        jLabel6.setText("OBS: To import a Person you need only First + Last Name");

        jLabel7.setForeground(new java.awt.Color(255, 0, 0));
        jLabel7.setText("If any fields from Secondary Address are empty,");

        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText(" the system will save only the Main Address");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(bt_import, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(21, 21, 21))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bt_personAddr)
                                    .addComponent(bt_person)
                                    .addComponent(jLabel4))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(text_begin, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel3)
                                        .addGap(12, 12, 12)
                                        .addComponent(text_end, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(57, 57, 57))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel7))))
                                .addGap(4, 4, 4))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(14, 14, 14)
                        .addComponent(bt_person))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(text_begin, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(text_end, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bt_personAddr)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8))
                    .addComponent(bt_import, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_personActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_personActionPerformed
		// TODO add your handling code here:
    }//GEN-LAST:event_bt_personActionPerformed

    private void bt_importMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bt_importMouseClicked

		ss = uicontroller.getActiveSpreadsheet();
		int begin_line = Integer.parseInt(text_begin.getText()) - 1;
		int end_line = Integer.parseInt(text_end.
			getText()) - 1;

		/**
		 * When Person button is pressed.
		 */
		if (bt_person.isSelected()) {

			if (begin_line == end_line) {
				cl = ss.getCell(0, begin_line);
				fn = cl.getContent();
				cl = ss.getCell(1, end_line);
				ln = cl.getContent();
				Person new_contact = new Person(fn, ln);
				addrctr.importContact(uicontroller, new_contact);

			} else {

				for (int i = begin_line; i <= end_line; i++) {
					cl = ss.getCell(0, i);
					fn = cl.getContent();
					cl = ss.getCell(1, i);
					ln = cl.getContent();

					Person new_contact = new Person(fn, ln);
					allcontacts.add(new_contact);
				}
				addrctr.importContacts(uicontroller, allcontacts);
			}

			/**
			 * Test if range is valid- if begin line is greater then end line.
			 */
			if (begin_line > end_line) {
				JOptionPane.
					showMessageDialog(null, "Error on Range- End Line is smaller then Begin Line");
			}
			JOptionPane.
				showMessageDialog(null, "Contacts Imported Successfully!");
			dispose();
		}

		/**
		 * When Person+Address button is pressed.
		 */
		if (bt_personAddr.isSelected()) {

			/**
			 * When there's only one line to import.
			 */
			if (begin_line == end_line) {
				cl = ss.getCell(0, begin_line);
				fn = cl.getContent();
				cl = ss.getCell(1, end_line);
				ln = cl.getContent();
				for (int i = 2; i < 12; i++) {
					switch (i) {
						case 2:
							cl = ss.getCell(i, begin_line);
							street1 = cl.getContent();
							break;
						case 3:
							cl = ss.getCell(i, begin_line);
							area1 = cl.getContent();
							break;
						case 4:
							cl = ss.getCell(i, begin_line);
							zipcode1 = cl.getContent();
							break;
						case 5:
							cl = ss.getCell(i, begin_line);
							city1 = cl.getContent();
							break;
						case 6:
							cl = ss.getCell(i, begin_line);
							country1 = cl.getContent();
							break;
						case 7:
							cl = ss.getCell(i, begin_line);
							street2 = cl.getContent();
							break;
						case 8:
							cl = ss.getCell(i, begin_line);
							area2 = cl.getContent();
							break;
						case 9:
							cl = ss.getCell(i, begin_line);
							zipcode2 = cl.getContent();
							break;
						case 10:
							cl = ss.getCell(i, begin_line);
							city2 = cl.getContent();
							break;
						case 11:
							cl = ss.getCell(i, begin_line);
							country2 = cl.getContent();
							break;

					}
				}
				/**
				 * Test if any fields from Main Address are empty.
				 */
				if ("".equals(street1) || "".equals(area1) || "".
					equals(zipcode1) || "".equals(city1) || "".equals(country1)) {
					JOptionPane.
						showMessageDialog(null, "Error MainAddress. Not enough fields");
				}
				/**
				 * Test if any fields from Main Address are empty.
				 */
				if ("".equals(street2) || "".equals(area2) || "".
					equals(zipcode2) || "".equals(city2) || "".equals(country2)) {
					Address mainAddr = new Address(street1, area1, zipcode1, city1, country1);
					Person contact = new Person(fn, ln, mainAddr);
					allcontacts.add(contact);
				} else {

					Address mainAddr = new Address(street1, area1, zipcode1, city1, country1);
					Address secAddr = new Address(street2, area2, zipcode2, city2, country2);
					Person contact = new Person(fn, ln, mainAddr, secAddr);
					allcontacts.add(contact);
				}
				addrctr.importContactAddress(uicontroller, allcontacts);

			} /**
			 * When there's more then one line to import.
			 */
			else {

				for (int j = begin_line; j <= end_line; j++) {
					cl = ss.getCell(0, j);
					fn = cl.getContent();
					cl = ss.getCell(1, j);
					ln = cl.getContent();
					for (int i = 2; i < 12; i++) {
						switch (i) {
							case 2:
								cl = ss.getCell(i, j);
								street1 = cl.getContent();
								break;
							case 3:
								cl = ss.getCell(i, j);
								area1 = cl.getContent();
								break;
							case 4:
								cl = ss.getCell(i, j);
								zipcode1 = cl.getContent();
								break;
							case 5:
								cl = ss.getCell(i, j);
								city1 = cl.getContent();
								break;
							case 6:
								cl = ss.getCell(i, j);
								country1 = cl.getContent();
								break;
							case 7:
								cl = ss.getCell(i, j);
								street2 = cl.getContent();
								break;
							case 8:
								cl = ss.getCell(i, j);
								area2 = cl.getContent();
								break;
							case 9:
								cl = ss.getCell(i, j);
								zipcode2 = cl.getContent();
								break;
							case 10:
								cl = ss.getCell(i, j);
								city2 = cl.getContent();
								break;
							case 11:
								cl = ss.getCell(i, j);
								country2 = cl.getContent();
								break;

						}
					}
					if ("".equals(street1) || "".equals(area1) || "".
						equals(zipcode1) || "".equals(city1) || "".
						equals(country1)) {
						JOptionPane.
							showMessageDialog(null, "Error MainAddress. Not enough fields");
					}
					if ("".equals(street2) || "".equals(area2) || "".
						equals(zipcode2) || "".equals(city2) || "".
						equals(country2)) {

						Address mainAddr = new Address(street1, area1, zipcode1, city1, country1);
						Person contact = new Person(fn, ln, mainAddr);
						allcontacts.add(contact);
					} else {

						Address mainAddr = new Address(street1, area1, zipcode1, city1, country1);
						Address secAddr = new Address(street2, area2, zipcode2, city2, country2);
						Person contact = new Person(fn, ln, mainAddr, secAddr);
						allcontacts.add(contact);
					}

				}
				addrctr.importContactAddress(uicontroller, allcontacts);
			}
			if (begin_line > end_line) {
				JOptionPane.
					showMessageDialog(null, "Error on Range- End Line is smaller then Begin Line");
			}

			JOptionPane.
				showMessageDialog(null, "Contacts and Addresses Imported Successfully!");
			dispose();
		}
    }//GEN-LAST:event_bt_importMouseClicked

    private void bt_importActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_importActionPerformed

    }//GEN-LAST:event_bt_importActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.
				getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ImportUI.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ImportUI.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ImportUI.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ImportUI.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new ImportUI(uicontroller).setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bt_group;
    private javax.swing.JButton bt_import;
    private javax.swing.JRadioButton bt_person;
    private javax.swing.JRadioButton bt_personAddr;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField text_begin;
    private javax.swing.JTextField text_end;
    // End of variables declaration//GEN-END:variables
}
