/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.address.ui;

import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author André Garrido - 1101598
 */
public class AddressMainMenu extends JFrame {

	private static final int widthFrame = 300;
	private static final int heightFrame = 200;
	private static int option;
	private UIController uicontroller;
	private GridLayout grid;
	private JPanel mainPanel, upPanel, downPanel;
	private JLabel label;
	private JButton createButton, editButton, removeButton, importButton, exportButton;
	private ButtonEvent create, edit, remove, import_addr, export_addr;

	public AddressMainMenu(UIController uicontroller) {
		super("Address");
		this.uicontroller = uicontroller;
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screensize = toolkit.getScreenSize();
		int widthEcra = ((int) screensize.getWidth() / 2);
		int heightEcra = ((int) screensize.getHeight() / 2);
		widthEcra -= (widthFrame / 2);
		heightEcra -= (heightFrame / 2);
		setLocation(widthEcra, heightEcra);

		grid = new GridLayout(2, 1);

		mainPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		label = new JLabel("Choose an option");

		create = new ButtonEvent();
		edit = new ButtonEvent();
		remove = new ButtonEvent();
		import_addr = new ButtonEvent();
		export_addr = new ButtonEvent();

		createButton = new JButton("Create");
		editButton = new JButton("Edit");
		removeButton = new JButton("Remove");
		importButton = new JButton("Import");
		exportButton = new JButton("Export");

		createButton.addActionListener(create);
		editButton.addActionListener(edit);
		removeButton.addActionListener(remove);
		importButton.addActionListener(import_addr);
		exportButton.addActionListener(export_addr);

		mainPanel.setLayout(grid);

		upPanel.add(label, BorderLayout.CENTER);

		downPanel.add(createButton);
		downPanel.add(editButton);
		downPanel.add(removeButton);
		downPanel.add(importButton);
		downPanel.add(exportButton);

		mainPanel.add(upPanel);
		mainPanel.add(downPanel);

		add(mainPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setVisible(true);

	}

	private class ButtonEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("Create".equals(e.getActionCommand())) {

				MenuCreateAddress mca = new MenuCreateAddress();
				mca.setVisible(true);

			} else if ("Edit".equals(e.getActionCommand())) {

				MenuEditAddress mea = new MenuEditAddress();
				mea.setVisible(true);

			} else if ("Remove".equals(e.getActionCommand())) {

				MenuRemoveAddress mra = new MenuRemoveAddress();
				mra.setVisible(true);

			} else if ("Import".equals(e.getActionCommand())) {

				ImportUI imm = new ImportUI(AddressMainMenu.this.uicontroller);
				imm.setVisible(true);

			} else if ("Export".equals(e.getActionCommand())) {

				ExportUI EUI = new ExportUI(AddressMainMenu.this.uicontroller);
				EUI.setVisible(true);

			}
		}

	}

}
