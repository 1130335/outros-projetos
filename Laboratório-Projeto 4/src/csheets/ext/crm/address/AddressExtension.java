/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.address;

import csheets.ext.Extension;
import csheets.ext.crm.address.ui.UIExtensionAddress;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author André Garrido - 1101598
 */
public class AddressExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Address";

	/**
	 * Creates a new Example extension.
	 */
	public AddressExtension() {
		super(NAME);
	}

	/*
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionAddress(this, uiController);
	}

}
