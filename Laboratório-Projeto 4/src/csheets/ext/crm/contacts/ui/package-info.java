/**
 * Provides the UI for the Contacts Extension
 *
 * @author AndreGarrido 1101598
 */
package csheets.ext.crm.contacts.ui;
