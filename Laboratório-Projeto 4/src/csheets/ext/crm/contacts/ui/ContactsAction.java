package csheets.ext.crm.contacts.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 * An action of the simple extension that exemplifies how to interact with the
 * spreadsheet.
 *
 * @author Alexandre Braganca
 */
public class ContactsAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public ContactsAction(UIController uiController) {
		this.uiController = uiController;
	}

	protected String getName() {
		return "ContactsMenu";
	}

	protected void defineProperties() {
	}

	/**
	 * A simple action that presents a confirmation dialog. If the user confirms
	 * then the contents of the cell A1 of the current sheet are set to the
	 * string "Changed".
	 *
	 * @param event the event that was fired
	 */
	public void actionPerformed(ActionEvent event) {

		//Present the main menu for contacts
		ContactsMainMenu cmm = new ContactsMainMenu();
	}
}
