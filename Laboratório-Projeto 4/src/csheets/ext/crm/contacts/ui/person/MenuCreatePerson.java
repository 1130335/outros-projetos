package csheets.ext.crm.contacts.ui.person;

import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.contacts.util.FilesManager;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class MenuCreatePerson extends JFrame {

	private final FilesManager files_manager = new FilesManager();
	private final PersonController person_controller = new PersonController();
	private static final int widthFrame = 500;
	private static final int heightFrame = 300;
	GridLayout grid;
	private final JPanel mainPanel, sentencePanel, firstNamePanel, lastNamePanel, imagePanel, buttonsPanel, profissionPanel, companyPanel;
	private final JLabel sentenceLabel, firstNameLabel, lastNameLabel, imageLabel, profissionsLabel, companyLabel;
	private final JTextField Text_firstName, Text_lastName, Text_image;
	private final JButton createButton, button_search;
	private final ButtonEvent createEvent, searchPhoto;
	final JFileChooser fc = new JFileChooser();
	JScrollPane scroll;
	private final JComboBox ProfissionsList, CompanyList;

	public MenuCreatePerson() {

		super("Create Person");

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screensize = toolkit.getScreenSize();
		int widthEcra = ((int) screensize.getWidth() / 2);
		int heightEcra = ((int) screensize.getHeight() / 2);
		widthEcra -= (widthFrame / 2);
		heightEcra -= (heightFrame / 2);
		setLocation(widthEcra, heightEcra);

		grid = new GridLayout(8, 1);

		mainPanel = new JPanel(grid);
		sentencePanel = new JPanel();
		firstNamePanel = new JPanel();
		lastNamePanel = new JPanel();
		imagePanel = new JPanel();
		profissionPanel = new JPanel();
		companyPanel = new JPanel();
		buttonsPanel = new JPanel();

		sentenceLabel = new JLabel("Please insert the data for the new Contact");
		firstNameLabel = new JLabel("First Name");
		lastNameLabel = new JLabel("Last Name");
		imageLabel = new JLabel("Image Location");
		profissionsLabel = new JLabel("Profission");
		companyLabel = new JLabel("Company");
		Text_firstName = new JTextField();
		Text_lastName = new JTextField();
		Text_image = new JTextField();

		Text_firstName.setColumns(30);
		Text_lastName.setColumns(30);
		Text_image.setColumns(15);

		ProfissionsList = new JComboBox(person_controller.getProfissionList());

		CompanyList = new JComboBox();
		DefaultComboBoxModel comb = new DefaultComboBoxModel();

		comb.addElement("");
		for (Company comp : person_controller.getCompanyList()) {
			comb.addElement(comp);
		}

		CompanyList.setModel(comb);
		companyPanel.add(companyLabel);
		companyPanel.add(CompanyList);

		createEvent = new ButtonEvent();
		searchPhoto = new ButtonEvent();

		createButton = new JButton("Create");
		button_search = new JButton("Search");
		createButton.addActionListener(createEvent);
		button_search.addActionListener(searchPhoto);
		sentencePanel.add(sentenceLabel);
		firstNamePanel.add(firstNameLabel);
		firstNamePanel.add(Text_firstName);
		lastNamePanel.add(lastNameLabel);
		lastNamePanel.add(Text_lastName);
		imagePanel.add(imageLabel);
		imagePanel.add(Text_image);
		imagePanel.add(button_search);
		profissionPanel.add(profissionsLabel);
		profissionPanel.add(ProfissionsList);

		buttonsPanel.add(createButton);

		mainPanel.add(sentencePanel);
		mainPanel.add(firstNamePanel);
		mainPanel.add(lastNamePanel);
		mainPanel.add(imagePanel);
		mainPanel.add(profissionPanel);
		mainPanel.add(companyPanel);
		mainPanel.add(buttonsPanel);

		add(mainPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setVisible(true);

	}

	private class ButtonEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("Create".equals(e.getActionCommand())) {
				if (!Text_firstName.getText().isEmpty() && !Text_lastName.
					getText().isEmpty() && !Text_image.getText().isEmpty()) {
					int result = JOptionPane.
						showConfirmDialog(null, "You have selected the Add Contact option. Do you want to insert the contact?");

					if (result == JOptionPane.YES_OPTION) {
						Person aux_contact;
						PersonController c_controller = new PersonController();
						byte[] fileContent = null;
						File f = new File(Text_image.getText());
						if (f.exists() && !f.isDirectory()) {
							try {

								fileContent = files_manager.
									fileToByteArray(new File(Text_image.
											getText()));

							} catch (IOException ex) {
								Logger.getLogger(MenuCreatePerson.class.
									getName()).log(Level.SEVERE, null, ex);
							}

							if (CompanyList.getSelectedItem() != "") {
								aux_contact = c_controller.
									NewContact(Text_firstName.
										getText(), Text_lastName.
											   getText(), fileContent, ProfissionsList.
											   getSelectedItem().toString(), person_controller.
											   getCompanyList().get(CompanyList.
												   getSelectedIndex() - 1), null, null);
								c_controller.saveContactDB(aux_contact);
							} else {
								aux_contact = c_controller.
									NewContact(Text_firstName.
										getText(), Text_lastName.
											   getText(), fileContent, ProfissionsList.
											   getSelectedItem().toString());
								c_controller.saveContactDB(aux_contact);
							}
							dispose();

						} else {
							JOptionPane.
								showMessageDialog(null, "Invalid path to file");

						}
					}
				} else {
					JOptionPane.
						showMessageDialog(null, "Please fill all the fields");
				}

			} else if ("Search".equals(e.getActionCommand())) {
				System.out.println("Ola");
				int returnVal = fc.showOpenDialog(MenuCreatePerson.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					Text_image.setText(file.getPath());

				} else {

				}
			}
		}

	}

}
