package csheets.ext.crm.contacts.ui.person;

import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.domain.Person;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author André Garrido<1101598>
 */
public class MenuRemovePerson extends JFrame {

    //private ContactsController controller;
    private static final int widthFrame = 400;
    private static final int heightFrame = 150;
    GridLayout grid;
    private JLabel titleLabel;
    private JPanel mainPanel, titlePanel, listPanel, buttonsPanel;
    JList listCont;
    JScrollPane scroll;
    DefaultListModel model;
    private int index_oldContact;
    private JButton createButton;
    private ButtonEvent createEvent;

    public MenuRemovePerson() {

        super("Remove Person");

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        int widthEcra = ((int) screensize.getWidth() / 2);
        int heightEcra = ((int) screensize.getHeight() / 2);
        widthEcra -= (widthFrame / 2);
        heightEcra -= (heightFrame / 2);
        setLocation(widthEcra, heightEcra);
         model = new DefaultListModel();
        PersonController controller = new PersonController();
        List<Person> list_cont = controller.getallContacts();
       
        for (Person c : list_cont) {
            model.addElement(c);
            
        }

        if (!list_cont.isEmpty()) {
            grid = new GridLayout(3, 1);

            mainPanel = new JPanel(grid);
            titlePanel = new JPanel();
            listPanel = new JPanel();
            buttonsPanel = new JPanel();

            titleLabel = new JLabel("Contacts");
            titlePanel.add(titleLabel);
           

            listCont = new JList(model);
            listCont.setPreferredSize(new Dimension(300, 150));

            createEvent = new ButtonEvent();

            createButton = new JButton("Remove");
            createButton.addActionListener(createEvent);

            listPanel.add(listCont);

            buttonsPanel.add(createButton);
            mainPanel.add(titlePanel);
            mainPanel.add(listPanel);
            mainPanel.add(buttonsPanel);

            add(mainPanel);
            setResizable(false);
            setSize(widthFrame, heightFrame);
            setVisible(true);

            listCont.addListSelectionListener(new ListSelectionListener() {

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    String value_selected = listCont.getSelectedValue().toString();
                    int index_path = value_selected.indexOf(value_selected);
                    index_oldContact = index_path;

                }
            });
        } else {

            JOptionPane.showMessageDialog(null, "Create any contact first");
            dispose();
        }
    }

    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ("Remove".equals(e.getActionCommand())) {

                int result = JOptionPane.
                        showConfirmDialog(null, "You will delete permanently this contact. Do you want to continue?");

                if (result == JOptionPane.YES_OPTION) {

                    PersonController c_controller = new PersonController();

                    c_controller.removeContact(index_oldContact);
                    dispose();

                }

            }

        }
    }
}
