/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.contacts.ui.person;

import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.contacts.util.FilesManager;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MenuEditPerson extends JFrame {

    private final FilesManager files_manager = new FilesManager();
    private PersonController person_controller = new PersonController();
    private List<Person> list_cont;
    private static final int widthFrame = 600;
    private static final int heightFrame = 600;
    GridLayout grid;
    private JPanel mainPanel, sentencePanel, firstNamePanel, lastNamePanel, imagePanel, listPanel, buttonsPanel, profissionPanel, companyPanel;
    private JLabel sentenceLabel, firstNameLabel, lastNameLabel, imageLabel, profissionsLabel, companyLabel, picLabel;
    private JTextField Text_firstName, Text_lastName, Text_image;
    JList listCont;
    JScrollPane scroll;
    DefaultListModel model;
    private int index_oldContact;
    private JButton createButton, button_search;
    private ButtonEvent createEvent, searchPhoto;
    final JFileChooser fc = new JFileChooser();
    private JComboBox ProfissionsList, CompanyList;

    public MenuEditPerson() {

        super("Edit Person");

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        int widthEcra = ((int) screensize.getWidth() / 2);
        int heightEcra = ((int) screensize.getHeight() / 2);
        widthEcra -= (widthFrame / 2);
        heightEcra -= (heightFrame / 2);
        setLocation(widthEcra, heightEcra);
        model = new DefaultListModel();
        PersonController controller = new PersonController();
        list_cont = controller.getallContacts();

        for (Person c : list_cont) {
            model.addElement(c);

        }

        if (!list_cont.isEmpty()) {
            grid = new GridLayout(8, 1);

            mainPanel = new JPanel(grid);
            sentencePanel = new JPanel();
            firstNamePanel = new JPanel();
            lastNamePanel = new JPanel();
            imagePanel = new JPanel();

            listPanel = new JPanel();
            profissionPanel = new JPanel();
            companyPanel = new JPanel();
            buttonsPanel = new JPanel();

            sentenceLabel = new JLabel("Please insert the data for the new Contact");
            firstNameLabel = new JLabel("First Name");
            lastNameLabel = new JLabel("Last Name");
            imageLabel = new JLabel("Image Location");
            profissionsLabel = new JLabel("Profission");
            companyLabel = new JLabel("Company");
            picLabel = new JLabel();
            picLabel.setPreferredSize(new Dimension(60, 80));

            Text_firstName = new JTextField();
            Text_lastName = new JTextField();
            Text_image = new JTextField();

            Text_firstName.setColumns(30);
            Text_lastName.setColumns(30);
            Text_image.setColumns(5);

            listCont = new JList(model);
            listCont.setPreferredSize(new Dimension(200, 150));

            ProfissionsList = new JComboBox(person_controller.getProfissionList());
            CompanyList = new JComboBox();
            DefaultComboBoxModel comb = new DefaultComboBoxModel();

            comb.addElement("");
            for (Company comp : person_controller.getCompanyList()) {
                comb.addElement(comp);
            }

            CompanyList.setModel(comb);
            companyPanel.add(companyLabel);
            companyPanel.add(CompanyList);

            createEvent = new ButtonEvent();
            searchPhoto = new ButtonEvent();
            createButton = new JButton("Edit");
            button_search = new JButton("Search");
            createButton.addActionListener(createEvent);
            button_search.addActionListener(searchPhoto);
            sentencePanel.add(sentenceLabel);
            firstNamePanel.add(firstNameLabel);
            firstNamePanel.add(Text_firstName);
            lastNamePanel.add(lastNameLabel);
            lastNamePanel.add(Text_lastName);
            imagePanel.add(imageLabel);
            imagePanel.add(Text_image);
            profissionPanel.add(profissionsLabel);
            profissionPanel.add(ProfissionsList);

            imagePanel.add(button_search);
            imagePanel.add(picLabel);
            listPanel.add(listCont);

            buttonsPanel.add(createButton);

            mainPanel.add(sentencePanel);
            mainPanel.add(firstNamePanel);
            mainPanel.add(lastNamePanel);
            mainPanel.add(imagePanel);
            mainPanel.add(profissionPanel);
            mainPanel.add(listPanel);
            mainPanel.add(companyPanel);
            mainPanel.add(buttonsPanel);

            add(mainPanel);
            setResizable(false);
            setSize(widthFrame, heightFrame);
            setVisible(true);

            listCont.addListSelectionListener(new ListSelectionListener() {

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    String value_selected = listCont.getSelectedValue().toString();

                    String[] splited = value_selected.split(" ");
                    int index_path = value_selected.indexOf(value_selected);
                    index_oldContact = index_path;
                    String firstname = splited[0];
                    String lastname = splited[1];

                    Text_firstName.setText(firstname);
                    Text_lastName.setText(lastname);
                    Text_image.setText("");
                    ProfissionsList.setSelectedItem(list_cont.get(index_path).getProfession());
                    byte[] imageData = list_cont.get(index_path).getImage();
                    setImage(imageData);
                    if (!person_controller.getCompanyList().isEmpty()) {
                        CompanyList.setSelectedItem(person_controller.getCompanyList().get(index_path));
                    }
                }
            });
        } else {

            JOptionPane.showMessageDialog(null, "Create any contact first");
            dispose();
        }
    }

    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ("Edit".equals(e.getActionCommand())) {

                int result = JOptionPane.
                        showConfirmDialog(null, "You will change permanently this contact. Do you want to continue?");

                if (result == JOptionPane.YES_OPTION) {
                    Person aux_contact;

                    PersonController c_controller = new PersonController();
                    byte[] fileContent = null;
                    File f = new File(Text_image.getText());
                    if (f.exists() && !f.isDirectory() || Text_image.getText().isEmpty()) {
                        if (!Text_image.getText().isEmpty()) {
                            f = new File(Text_image.getText());
                            try {
                                fileContent = files_manager.fileToByteArray(new File(Text_image.getText()));
                            } catch (IOException ex) {
                                Logger.getLogger(MenuCreatePerson.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {

                            fileContent = person_controller.getallContacts().get(index_oldContact).getImage();

                        }
                        if (CompanyList.getSelectedItem() != "") {
                            String firstName = Text_firstName.getText();
                            String lastName = Text_lastName.getText();
                            byte[] image = fileContent;
                            String profission = ProfissionsList.getSelectedItem().toString();
                            Company company = person_controller.getCompanyList().get(CompanyList.getSelectedIndex() - 1);
                            c_controller.editContact(c_controller.getContactByIndex(index_oldContact), firstName, lastName, image, profission, company);

                        } else {
                            String firstName = Text_firstName.getText();
                            String lastName = Text_lastName.getText();
                            byte[] image = fileContent;
                            String profission = ProfissionsList.getSelectedItem().toString();
                            c_controller.editContact(c_controller.getContactByIndex(index_oldContact), firstName, lastName, image, profission);
                        }

                        dispose();

                    } else {
                        JOptionPane.showMessageDialog(null, "Invalid path to file");

                    }
                }
            } else if ("Search".equals(e.getActionCommand())) {

                int returnVal = fc.showOpenDialog(MenuEditPerson.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    Text_image.setText(file.getPath());

                } else {

                }
            }

        }

    }

    public void setImage(byte[] imgdata) {
        ImageIcon pic = null;
        if (imgdata != null) {
            pic = new ImageIcon(imgdata);
        } else {
            pic = new ImageIcon(getClass().getResource(""));
        }
        Image scaled = pic.getImage().getScaledInstance(picLabel.getWidth(), picLabel.getHeight(), Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaled);
        picLabel.setIcon(icon);
    }
}
