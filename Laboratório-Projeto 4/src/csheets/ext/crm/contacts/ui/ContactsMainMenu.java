/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.contacts.ui;

import csheets.ext.crm.contacts.ui.company.MenuCreateCompany;
import csheets.ext.crm.contacts.ui.company.MenuEditCompany;
import csheets.ext.crm.contacts.ui.company.MenuRemoveCompany;
import csheets.ext.crm.contacts.ui.person.MenuRemovePerson;
import csheets.ext.crm.contacts.ui.person.MenuEditPerson;
import csheets.ext.crm.contacts.ui.person.MenuCreatePerson;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author André Garrido - 1101598.
 */
public class ContactsMainMenu extends JFrame {

    private static final int widthFrame = 300;
    private static final int heightFrame = 150;
    private static int option;

    private GridLayout grid;
    private JPanel mainPanel, upPanel, downPanel, wrapperPanel;
    private JLabel label, typeLabel;
    private JButton createButton, editButton, removeButton;
    private ButtonEvent create, edit, remove;
    private JComboBox TypeList;

    public ContactsMainMenu() {
        super("Contacts");

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        int widthEcra = ((int) screensize.getWidth() / 2);
        int heightEcra = ((int) screensize.getHeight() / 2);
        widthEcra -= (widthFrame / 2);
        heightEcra -= (heightFrame / 2);
        setLocation(widthEcra, heightEcra);

        grid = new GridLayout(3, 1);

        mainPanel = new JPanel();
        wrapperPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        label = new JLabel("Choose an option");
        typeLabel = new JLabel("Type of contact:");
        create = new ButtonEvent();
        edit = new ButtonEvent();
        remove = new ButtonEvent();

        createButton = new JButton("Create");
        editButton = new JButton("Edit");
        removeButton = new JButton("Remove");

        String[] types_strings = {"Person", "Company"};

        TypeList = new JComboBox(types_strings);

        createButton.addActionListener(create);
        editButton.addActionListener(edit);
        removeButton.addActionListener(remove);

        mainPanel.setLayout(grid);
        wrapperPanel.add(typeLabel, BorderLayout.WEST);
        wrapperPanel.add(TypeList, BorderLayout.CENTER);
        upPanel.add(label, BorderLayout.CENTER);

        downPanel.add(createButton);
        downPanel.add(editButton);
        downPanel.add(removeButton);
        mainPanel.add(wrapperPanel);
        mainPanel.add(upPanel);
        mainPanel.add(downPanel);

        add(mainPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setVisible(true);

    }

    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if ("Create".equals(e.getActionCommand())) {
                if (TypeList.getSelectedItem().toString() == "Person") {
                    MenuCreatePerson cw = new MenuCreatePerson();
                } else if (TypeList.getSelectedItem().toString() == "Company") {
                    MenuCreateCompany cw = new MenuCreateCompany();
                }
            } else if ("Edit".equals(e.getActionCommand())) {
                if (TypeList.getSelectedItem().toString() == "Person") {
                    MenuEditPerson cw = new MenuEditPerson();
                } else if (TypeList.getSelectedItem().toString() == "Company") {
                    MenuEditCompany cw = new MenuEditCompany();
                }

            } else if ("Remove".equals(e.getActionCommand())) {

                if (TypeList.getSelectedItem().toString() == "Person") {
                    MenuRemovePerson cw = new MenuRemovePerson();
                } else if (TypeList.getSelectedItem().toString() == "Company") {
                    MenuRemoveCompany cw = new MenuRemoveCompany();
                }

            }
        }

    }

}
