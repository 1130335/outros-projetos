/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.contacts.ui.company;

import csheets.ext.crm.application.CompanyController;
import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.contacts.ui.person.MenuCreatePerson;
import csheets.ext.crm.contacts.util.FilesManager;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author André Garrido<1101598>
 */
public class MenuEditCompany extends JFrame {

    private final FilesManager files_manager = new FilesManager();
    private CompanyController controller = new CompanyController();
    private static final int widthFrame = 500;
    private static final int heightFrame = 350;
    GridLayout grid;
    private JPanel mainPanel, designationPanel, imagePanel, companyPanel, peoplePanel, buttonsPanel;
    private JLabel designationLabel, imageLabel, companyLabel, peopleLabel, picLabel;
    private JTextField Text_designationName, Text_image;
    private JComboBox comboBox;
    private JList peopleRelated, companyList;
    private String[] peopleList = {""};
    private DefaultListModel model;
    private JButton createButton, button_search;
    private ButtonEvent createEvent, searchPhoto;
    private int index_oldContact;
    final JFileChooser fc = new JFileChooser();
    JScrollPane scroll;

    public MenuEditCompany() {

        super("Edit Company");

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        int widthEcra = ((int) screensize.getWidth() / 2);
        int heightEcra = ((int) screensize.getHeight() / 2);
        widthEcra -= (widthFrame / 2);
        heightEcra -= (heightFrame / 2);
        setLocation(widthEcra, heightEcra);

        peopleRelated = new JList(peopleList); //data has type Object[]
        peopleRelated.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        peopleRelated.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        peopleRelated.setVisibleRowCount(-1);
        JScrollPane listScroller = new JScrollPane(peopleRelated);
        listScroller.setPreferredSize(new Dimension(250, 80));

        model = new DefaultListModel();

        final List<Company> list_cont = controller.getallContacts();

        for (Company c : list_cont) {
            model.addElement(c);

        }

        if (!list_cont.isEmpty()) {
            grid = new GridLayout(6, 1);

            mainPanel = new JPanel(grid);
            designationPanel = new JPanel();
            imagePanel = new JPanel();
            peoplePanel = new JPanel();
            companyPanel = new JPanel();
            buttonsPanel = new JPanel();

            designationLabel = new JLabel("Designation");
            imageLabel = new JLabel("Image Location");
            companyLabel = new JLabel("Company");
            peopleLabel = new JLabel("People Related");
            picLabel = new JLabel();
            picLabel.setPreferredSize(new Dimension(60, 80));
            Text_image = new JTextField();
            Text_designationName = new JTextField();
            Text_image.setColumns(5);
            Text_designationName.setColumns(30);

            companyList = new JList();
            companyList.setModel(model);

            createEvent = new ButtonEvent();
            searchPhoto = new ButtonEvent();
            createButton = new JButton("Edit");
            button_search = new JButton("Search");

            createButton.addActionListener(createEvent);
            button_search.addActionListener(searchPhoto);

            designationPanel.add(designationLabel);
            designationPanel.add(Text_designationName);
            imagePanel.add(imageLabel);
            imagePanel.add(Text_image);
            imagePanel.add(button_search);
            imagePanel.add(picLabel);
            companyPanel.add(companyLabel);
            companyPanel.add(companyList);
            peoplePanel.add(peopleLabel);
            peoplePanel.add(peopleRelated);
            buttonsPanel.add(createButton);

            mainPanel.add(companyPanel);
            mainPanel.add(designationPanel);
            mainPanel.add(imagePanel);
            mainPanel.add(peoplePanel);
            mainPanel.add(buttonsPanel);

            add(mainPanel);
            setResizable(false);
            setSize(widthFrame, heightFrame);
            setVisible(true);

            companyList.addListSelectionListener(new ListSelectionListener() {

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    String value_selected = companyList.getSelectedValue().toString();

                    int index_path = companyList.getSelectedIndex();
                    index_oldContact = index_path;

                    Text_designationName.setText(value_selected);

                    List<Person> pe = controller.getPeopleRelated(controller.getallContacts().get(index_path));
                    List<String> aux = new ArrayList<>();

                    for (Person pe1 : pe) {
                        aux.add(pe1.toString());
                    }

                    peopleList = aux.toArray(new String[aux.size()]);

                    peopleRelated.setListData(peopleList);
                    peopleRelated.setEnabled(false);
                    byte[] imageData = list_cont.get(index_path).getImage();
                    setImage(imageData);

                }
            });
        } else {

            JOptionPane.showMessageDialog(null, "Create any contact first");
            dispose();
        }
    }

    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ("Edit".equals(e.getActionCommand())) {
                if (!Text_designationName.getText().isEmpty()) {
                    int result = JOptionPane.
                            showConfirmDialog(null, "You will change permanently this contact. Do you want to continue?");
                    if (result == JOptionPane.YES_OPTION) {
                        Company aux_contact;
                        Company old_contact;
                        CompanyController c_controller = new CompanyController();
                        byte[] fileContent = null;
                        File f = new File(Text_image.getText());
                        if (f.exists() && !f.isDirectory() || Text_image.getText().isEmpty()) {
                            if (!Text_image.getText().isEmpty()) {
                                f = new File(Text_image.getText());
                                try {
                                    fileContent = files_manager.fileToByteArray(new File(Text_image.getText()));
                                } catch (IOException ex) {
                                    Logger.getLogger(MenuCreatePerson.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else {

                                fileContent = c_controller.getallContacts().get(index_oldContact).getImage();

                            }
                            c_controller.editContact(c_controller.getContactByIndex(index_oldContact),Text_designationName.getText(), fileContent);

                            dispose();

                        } else {
                            JOptionPane.showMessageDialog(null, "Invalid path to file");

                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please fill all the fields");
                }

            } else if ("Search".equals(e.getActionCommand())) {

                int returnVal = fc.showOpenDialog(MenuEditCompany.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    Text_image.setText(file.getPath());

                } else {

                }
            }
        }

    }

    public void setImage(byte[] imgdata) {
        ImageIcon pic = null;
        if (imgdata != null) {
            pic = new ImageIcon(imgdata);
        } else {
            pic = new ImageIcon(getClass().getResource(""));
        }
        Image scaled = pic.getImage().getScaledInstance(picLabel.getWidth(), picLabel.getHeight(), Image.SCALE_DEFAULT);
        ImageIcon icon = new ImageIcon(scaled);
        picLabel.setIcon(icon);
    }
}
