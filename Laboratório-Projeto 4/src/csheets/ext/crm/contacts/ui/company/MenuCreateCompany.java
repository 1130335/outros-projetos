package csheets.ext.crm.contacts.ui.company;

import csheets.ext.crm.application.CompanyController;
import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.contacts.ui.person.MenuCreatePerson;
import csheets.ext.crm.contacts.util.FilesManager;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MenuCreateCompany extends JFrame {

    private final FilesManager files_manager = new FilesManager();
    private static final int widthFrame = 500;
    private static final int heightFrame = 200;
    GridLayout grid;
    private final JPanel mainPanel, designationPanel, imagePanel, peoplePanel, buttonsPanel;
    private final JLabel designationLabel, imageLabel, peopleLabel;
    private final JTextField Text_designationName, Text_image;
    private final JButton createButton, button_search;
    private final ButtonEvent createEvent, searchPhoto;
    final JFileChooser fc = new JFileChooser();

    public MenuCreateCompany() {

        super("Create Company");

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        int widthEcra = ((int) screensize.getWidth() / 2);
        int heightEcra = ((int) screensize.getHeight() / 2);
        widthEcra -= (widthFrame / 2);
        heightEcra -= (heightFrame / 2);
        setLocation(widthEcra, heightEcra);

        grid = new GridLayout(3, 1);

        mainPanel = new JPanel(grid);
        designationPanel = new JPanel();
        imagePanel = new JPanel();
        peoplePanel = new JPanel();
        buttonsPanel = new JPanel();

        designationLabel = new JLabel("Designation");
        imageLabel = new JLabel("Image Location");
        peopleLabel = new JLabel("People Related");

        Text_designationName = new JTextField();
        Text_image = new JTextField();

        Text_designationName.setColumns(30);
        Text_image.setColumns(15);

        createEvent = new ButtonEvent();
        searchPhoto = new ButtonEvent();

        createButton = new JButton("Create");
        button_search = new JButton("Search");

        createButton.addActionListener(createEvent);
        button_search.addActionListener(searchPhoto);

        designationPanel.add(designationLabel);
        designationPanel.add(Text_designationName);
        imagePanel.add(imageLabel);
        imagePanel.add(Text_image);
        imagePanel.add(button_search);

        buttonsPanel.add(createButton);

        mainPanel.add(designationPanel);
        mainPanel.add(imagePanel);

        mainPanel.add(buttonsPanel);

        add(mainPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setVisible(true);

    }

    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ("Create".equals(e.getActionCommand())) {
                if (!Text_designationName.getText().isEmpty() && !Text_image.getText().isEmpty()) {
                    int result = JOptionPane.
                            showConfirmDialog(null, "You have selected the Add Contact option. Do you want to insert the contact?");

                    if (result == JOptionPane.YES_OPTION) {
                        Company aux_contact;
                        CompanyController c_controller = new CompanyController();
                        byte[] fileContent = null;
                        File f = new File(Text_image.getText());
                        if (f.exists() && !f.isDirectory()) {
                            try {

                                fileContent = files_manager.fileToByteArray(new File(Text_image.getText()));

                            } catch (IOException ex) {
                                Logger.getLogger(MenuCreatePerson.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            aux_contact = c_controller.NewContact(Text_designationName.getText(), fileContent);
                            c_controller.saveContactDB(aux_contact);

                            dispose();

                        } else {
                            JOptionPane.showMessageDialog(null, "Invalid path to file");

                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please fill all the fields");
                }

            } else if ("Search".equals(e.getActionCommand())) {

                int returnVal = fc.showOpenDialog(MenuCreateCompany.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    Text_image.setText(file.getPath());

                } else {

                }
            }
        }

    }
}
