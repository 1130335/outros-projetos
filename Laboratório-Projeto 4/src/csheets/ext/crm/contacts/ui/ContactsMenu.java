package csheets.ext.crm.contacts.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Representes the UI extension menu of the simple extension.
 *
 * @author Alexandre Braganca
 */
public class ContactsMenu extends JMenu {

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created. A menu
	 * option is an action (in this case
	 * {@link csheets.ext.simple.ui.ExampleAction})
	 *
	 * @param uiController the user interface controller
	 */
	public ContactsMenu(UIController uiController) {
		super("Contacts");
		setMnemonic(KeyEvent.VK_E);

		// Adds font actions
		add(new ContactsAction(uiController));
	}
}
