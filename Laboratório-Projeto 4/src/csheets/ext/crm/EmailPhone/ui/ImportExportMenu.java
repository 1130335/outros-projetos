/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.EmailPhone.ui;

import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Paulo
 */
class ImportExportMenu extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 100;

	private final GridLayout grid;
	private final JPanel centralPanel, upPanel, downPanel;
	private final JLabel info;
	private final JButton importButton, exportButton;
	private final UIController uiController;

	public ImportExportMenu(final UIController uiController) {
		super("Import/Export Email Phone");
		this.uiController = uiController;

		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		importButton = new JButton("Import");
		exportButton = new JButton("Export");

		importButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				new EmailPhoneImportWindow(ImportExportMenu.this.uiController);
				dispose();
			}
		}
		);

		exportButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				new EmailPhoneExportWindow(ImportExportMenu.this.uiController);
				dispose();
			}
		}
		);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(importButton);
		downPanel.add(exportButton);

		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		pack();
		setVisible(true);

	}
}
