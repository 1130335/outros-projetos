/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.EmailPhone.ui;

import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import csheets.ui.ctrl.UIController;
import java.util.List;

/**
 * Controller to export or import the email/phone (Person)
 *
 * @author Paulo
 */
public class ImportExportEmailPhoneController {

	private final UIController uiController;

	/**
	 * Repository instance
	 */
	JPAPersonRepository rep = Persistence.getJPARepositoryFactory().
		getJPAPersonRepository();

	public ImportExportEmailPhoneController(UIController uiController) {
		this.uiController = uiController;
	}

	public List<Person> getallContacts() {

		return rep.
			AllContacts();
	}

	public void editContact(Person contact, int index) {
		rep.editContact(contact, index);
	}

}
