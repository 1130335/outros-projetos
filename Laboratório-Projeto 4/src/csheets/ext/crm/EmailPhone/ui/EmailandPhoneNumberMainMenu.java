package csheets.ext.crm.EmailPhone.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Miguel
 */
public class EmailandPhoneNumberMainMenu extends JFrame {

	private static final int widthFrame = 300;
	private static final int heightFrame = 150;
	private static int option;
	private GridLayout grid;
	private JPanel mainPanel, upPanel, downPanel;
	private JLabel label;
	private JButton createButton, editButton, removeButton;
	private ButtonEvent create, edit, remove;

	public EmailandPhoneNumberMainMenu() {
		super("Email/PhoneNumber");

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screensize = toolkit.getScreenSize();
		int widthEcra = ((int) screensize.getWidth() / 2);
		int heightEcra = ((int) screensize.getHeight() / 2);
		widthEcra -= (widthFrame / 2);
		heightEcra -= (heightFrame / 2);
		setLocation(widthEcra, heightEcra);

		grid = new GridLayout(2, 1);

		mainPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		label = new JLabel("Choose an option");

		create = new ButtonEvent();
		edit = new ButtonEvent();
		remove = new ButtonEvent();

		createButton = new JButton("Create");
		editButton = new JButton("Edit");
		removeButton = new JButton("Remove");

		createButton.addActionListener(create);
		editButton.addActionListener(edit);
		removeButton.addActionListener(remove);

		mainPanel.setLayout(grid);

		upPanel.add(label, BorderLayout.CENTER);

		downPanel.add(createButton);
		downPanel.add(editButton);
		downPanel.add(removeButton);

		mainPanel.add(upPanel);
		mainPanel.add(downPanel);

		add(mainPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setVisible(true);

	}

	private static class ButtonEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("Create".equals(e.getActionCommand())) {

				CreateMenuEmailandPhoneNumber mca = new CreateMenuEmailandPhoneNumber();
				mca.setVisible(true);

			} else if ("Edit".equals(e.getActionCommand())) {

				EditMenuEmailandPhoneNumber mea = new EditMenuEmailandPhoneNumber();
				mea.setVisible(true);

			} else if ("Remove".equals(e.getActionCommand())) {

				RemoveMenuEmailandPhoneNumber mra = new RemoveMenuEmailandPhoneNumber();
				mra.setVisible(true);

			}
		}
	}
}
