/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.EmailPhone.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Miguel
 */
public class EmailPhoneAction extends BaseAction {

	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public EmailPhoneAction(UIController uiController) {
		this.uiController = uiController;
	}

	/**
	 *
	 * @return Action Name
	 */
	protected String getName() {
		return "Email/PhoneNumberMainMenu";
	}

	/**
	 *
	 */
	protected void defineProperties() {
	}

	/**
	 * torna a janela para gestao de moradas visivel
	 *
	 * @param event the event that was fired
	 */
	public void actionPerformed(ActionEvent event) {
		EmailandPhoneNumberMainMenu cmm = new EmailandPhoneNumberMainMenu();
	}
}
