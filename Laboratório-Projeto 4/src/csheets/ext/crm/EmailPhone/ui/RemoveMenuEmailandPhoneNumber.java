/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.EmailPhone.ui;

import csheets.ext.crm.application.EmailPhoneController;
import csheets.ext.crm.domain.Email;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.domain.PhoneNumber;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel
 */
public class RemoveMenuEmailandPhoneNumber extends javax.swing.JFrame {

	EmailPhoneController EmailPhonectr = new EmailPhoneController();
	List<Person> allcontacts = EmailPhonectr.getallContacts();
	Person Person = new Person();
	long id = -1;

	/**
	 * Creates new form MenuRemoveAddress
	 */
	public RemoveMenuEmailandPhoneNumber() {
		if (allcontacts.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please create a contact first");
			dispose();
		} else {
			initComponents();
			Object[] test = new Object[allcontacts.size()];
			for (int i = 0; i < allcontacts.size(); i++) {
				String cont = allcontacts.get(i).toString();
				test[i] = (Object) cont;
			}
			list_contacts.setListData(test);
		}
	}

	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        removeAll_button = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        list_contacts = new javax.swing.JList();
        remove_button = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        list_emailphone = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        label_emailphonelist = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        removeAll_button.setText("Remove All");
        removeAll_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeAll_buttonMouseClicked(evt);
            }
        });
        removeAll_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAll_buttonActionPerformed(evt);
            }
        });

        list_contacts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_contactsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(list_contacts);

        remove_button.setText("Remove");
        remove_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                remove_buttonMouseClicked(evt);
            }
        });

        list_emailphone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_emailphoneMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(list_emailphone);

        jLabel2.setText("Please choose the contact:");

        label_emailphonelist.setText("Please choose the info you want to remove:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Contact Email/PhoneNumber Manager");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(removeAll_button, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(256, 256, 256))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(remove_button, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(56, 56, 56)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(label_emailphonelist, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(285, 285, 285))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(301, 301, 301)))))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(47, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(39, 39, 39)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(label_emailphonelist)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remove_button, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(removeAll_button, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void removeAll_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeAll_buttonMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		Person = allcontacts.get(index);

		Email allEmails = Person.getEmail();
		PhoneNumber allPhones = Person.getPhoneNumber();
		Person.insertEmails(null);

		Person.insertPhoneNumbers(null);
		EmailPhonectr.editContact(Person, index);

		JOptionPane.
			showMessageDialog(null, "All Data Removed");
		dispose();
    }//GEN-LAST:event_removeAll_buttonMouseClicked

    private void list_contactsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_contactsMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		Person = allcontacts.get(index);

		Email allEmails = Person.getEmail();
		String email1 = allEmails.getEmail1();
		String email2 = allEmails.getEmail2();
		String email3 = allEmails.getEmail3();

		PhoneNumber allPhones = Person.getPhoneNumber();
		int PersonalNumber = allPhones.getPersonalNumber();
		int JobNumber = allPhones.getJobNumber();
		int JobPersonalNumber = allPhones.getJobPersonalNumber();
		int HomeNumber = allPhones.getHomeNumber();

		Object[] test = new Object[7];
		String emailtest = "Email1";
		String emailtest2 = "Email2";
		String emailtest3 = "Email3";
		String Phonetest1 = "Nº 1";
		String Phonetest2 = "Nº 2";
		String Phonetest3 = "Nº 3";
		String Phonetest4 = "Nº 4";

		if (email1 != null) {
			test[0] = (Object) emailtest;
		}
		if (email2 != null) {
			test[1] = (Object) emailtest2;
		}
		if (email3 != null) {
			test[2] = (Object) emailtest3;
		}
		if (PersonalNumber != 0) {
			test[3] = (Object) Phonetest1;
		}
		if (JobNumber != 0) {
			test[4] = (Object) Phonetest2;
		}
		if (JobPersonalNumber != 0) {
			test[5] = (Object) Phonetest3;
		}
		if (HomeNumber != 0) {
			test[6] = (Object) Phonetest4;
		}

		list_emailphone.setListData(test);
    }//GEN-LAST:event_list_contactsMouseClicked

    private void remove_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remove_buttonMouseClicked
		int index = list_contacts.locationToIndex(evt.getPoint());
		Person = allcontacts.get(index);
		Email email = null;
		String selected_emailphone = list_emailphone.getSelectedValue().
			toString();
		Person contact = null;
		if (selected_emailphone.equals("Email1")) {
			email = Person.getEmail();
			Person.insertEmails(null);
			EmailPhonectr.editContact(contact, index);

			JOptionPane.showMessageDialog(null, "Email Removed");
			dispose();

		}
		if (selected_emailphone.equals("Nº 1")) {
			PhoneNumber pn = Person.getPhoneNumber();
			Person.insertPhoneNumbers(null);
			EmailPhonectr.editContact(contact, index);

			EmailPhonectr.removePhoneNumber(pn);
			JOptionPane.showMessageDialog(null, "Phone Numbers Removed");
			dispose();

		}
    }//GEN-LAST:event_remove_buttonMouseClicked

    private void list_emailphoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_emailphoneMouseClicked

    }//GEN-LAST:event_list_emailphoneMouseClicked

    private void removeAll_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAll_buttonActionPerformed

    }//GEN-LAST:event_removeAll_buttonActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.
				getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.
				getLogger(RemoveMenuEmailandPhoneNumber.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.
				getLogger(RemoveMenuEmailandPhoneNumber.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.
				getLogger(RemoveMenuEmailandPhoneNumber.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.
				getLogger(RemoveMenuEmailandPhoneNumber.class.getName()).
				log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new RemoveMenuEmailandPhoneNumber().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label_emailphonelist;
    private javax.swing.JList list_contacts;
    private javax.swing.JList list_emailphone;
    private javax.swing.JButton removeAll_button;
    private javax.swing.JButton remove_button;
    // End of variables declaration//GEN-END:variables
}
