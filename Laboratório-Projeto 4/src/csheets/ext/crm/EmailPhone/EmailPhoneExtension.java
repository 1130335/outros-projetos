/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.EmailPhone;

import csheets.ext.Extension;
import csheets.ext.crm.EmailPhone.ui.UIExtensionEmailPhone;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Miguel
 */
public class EmailPhoneExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Email/PhoneNumber";

	/**
	 * Creates a new Example extension.
	 */
	public EmailPhoneExtension() {
		super(NAME);
	}

	/*
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionEmailPhone(this, uiController);
	}

}
