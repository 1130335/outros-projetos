/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.ImportExport;

import csheets.ext.crm.domain.Person;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author André Garrido - 1101598
 */
public interface Export {

	public void exportAddress_one(UIController uicontroller, Person contact,
								  ArrayList<Boolean> fields);

	public void exportAddress_all(UIController uicontroller,
								  List<Person> allContacts,
								  ArrayList<Boolean> fields);

	public void exportAll(UIController uicontroller);

}
