/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.ImportExport;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.crm.application.AddressController;
import csheets.ext.crm.domain.Email;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.domain.PhoneNumber;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulo
 */
public class Export_EmailPhone implements Export {

	AddressController addrctr;
	List<Person> allcontacts;

	public Export_EmailPhone() {
		addrctr = new AddressController();
		allcontacts = addrctr.getallContacts();
	}

	@Override
	public void exportAddress_one(UIController uicontroller, Person contact,
								  ArrayList<Boolean> fields) {
		try {
			Spreadsheet ss = uicontroller.getActiveSpreadsheet();
			/**
			 * Obtain the current active cell position.
			 */
			int col = uicontroller.getActiveCell().getAddress().getColumn();
			int row = uicontroller.getActiveCell().getAddress().getRow();

			int act_col = col + 2;
			Cell cl;
			cl = ss.getCell(col, row);
			cl.setContent(contact.getFirstName());
			cl = ss.getCell(col + 1, row);
			cl.setContent(contact.getLastName());

			String email1 = "";
			String email2 = "";
			String email3 = "";
			if (contact.getEmail() != null) {
				//Person's emails
				Email email = contact.getEmail();
				email1 = email.getEmail1();
				email2 = email.getEmail2();
				email3 = email.getEmail3();

			} else {
				JOptionPane.
					showMessageDialog(null, "There are no mail available", "Error", JOptionPane.ERROR_MESSAGE);
			}
			int phone1 = 0;
			int phone2 = 0;
			int phone3 = 0;
			int phone4 = 0;
			if (contact.getPhoneNumber() != null) {
				//Person's phone numbers
				PhoneNumber phone = contact.getPhoneNumber();
				phone1 = phone.getPersonalNumber();
				phone2 = phone.getJobPersonalNumber();
				phone3 = phone.getHomeNumber();
				phone4 = phone.getJobNumber();
			} else {
				JOptionPane.
					showMessageDialog(null, "There are no phone available", "Error", JOptionPane.ERROR_MESSAGE);
			}
			String[] emailPhoneInfo = new String[]{email1, email2, email3, "" + phone1, "" + phone2, "" + phone3, "" + phone4};

			/**
			 * Loop selected collumns to export just the information selected *
			 */
			for (int i = 0; i < emailPhoneInfo.length; i++) {
				cl = ss.getCell(act_col + i, row);
				if (fields.get(i)) {
					cl.setContent(emailPhoneInfo[i]);
				}
			}

		} catch (FormulaCompilationException ex) {
			return;
		}
	}

	@Override
	public void exportAddress_all(UIController uicontroller,
								  List<Person> allContacts,
								  ArrayList<Boolean> fields
	) {
		try {
			Spreadsheet ss = uicontroller.getActiveSpreadsheet();
			/**
			 * Obtain the current active cell position.
			 */
			int col = uicontroller.getActiveCell().getAddress().getColumn();
			int row = uicontroller.getActiveCell().getAddress().getRow();
			/**
			 * Loop all contacts.
			 */
			for (int j = 0; j < allcontacts.size(); j++) {
				Person person = allcontacts.get(j);

				int act_col = col + 2;
				Cell cl;
				cl = ss.getCell(col, row + j);
				cl.setContent(person.getFirstName());
				cl = ss.getCell(col + 1, row + j);
				cl.setContent(person.getLastName());

				String email1 = "";
				String email2 = "";
				String email3 = "";
				if (person.getEmail() != null) {
					//Person's emails
					Email email = person.getEmail();
					email1 = email.getEmail1();
					email2 = email.getEmail2();
					email3 = email.getEmail3();

				} else {
					JOptionPane.
						showMessageDialog(null, "There are no mail available", "Error", JOptionPane.ERROR_MESSAGE);
				}
				int phone1 = 0;
				int phone2 = 0;
				int phone3 = 0;
				int phone4 = 0;
				if (person.getPhoneNumber() != null) {
					//Person's phone numbers
					PhoneNumber phone = person.getPhoneNumber();
					phone1 = phone.getPersonalNumber();
					phone2 = phone.getJobPersonalNumber();
					phone3 = phone.getHomeNumber();
					phone4 = phone.getJobNumber();
				} else {
					JOptionPane.
						showMessageDialog(null, "There are no phone available", "Error", JOptionPane.ERROR_MESSAGE);
				}
				String[] emailPhoneInfo = new String[]{email1, email2, email3, "" + phone1, "" + phone2, "" + phone3, "" + phone4};

				/**
				 * Loop selected collumns to export just the information
				 * selected *
				 */
				for (int i = 0; i < emailPhoneInfo.length; i++) {
					cl = ss.getCell(act_col + i, j + row);
					if (fields.get(i)) {
						cl.setContent(emailPhoneInfo[i]);
					}
				}

			}

		} catch (FormulaCompilationException ex) {
			return;
		}
	}

	@Override
	public void exportAll(UIController uicontroller
	) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
