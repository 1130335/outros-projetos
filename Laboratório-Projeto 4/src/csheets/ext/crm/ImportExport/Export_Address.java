/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.ImportExport;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.crm.application.AddressController;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Person;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author André Garrido - 1101598
 */
public class Export_Address implements Export {

	public Export_Address(UIController uicontroller) {
		this.uicontroller = uicontroller;
	}

	AddressController addrctr = new AddressController();
	List<Person> allcontacts = addrctr.getallContacts();
	Person contact = new Person();

	private UIController uicontroller;
	private Spreadsheet ss;
	Cell cl;
	int col, row;

	/**
	 * Method to export addresses from all contacts with the selected fields.
	 *
	 * @param uicontroller
	 * @param allContacts
	 * @param fields
	 *
	 */
	public void exportAddress_all(UIController uicontroller,
								  List<Person> allContacts,
								  ArrayList<Boolean> fields) {
		try {
			this.uicontroller = uicontroller;
			this.ss = this.uicontroller.getActiveSpreadsheet();
			/**
			 * Obtain the current active cell position.
			 */
			col = uicontroller.getActiveCell().getAddress().getColumn();
			row = uicontroller.getActiveCell().getAddress().getRow();
			/**
			 * Loop all contacts.
			 */
			for (int j = 0; j < allcontacts.size(); j++) {
				Person person = allcontacts.get(j);
				Address mainAddr = person.getMainAddress();
				Address secAddr = person.getSecAddress();

				int act_col = col + 2;
				cl = ss.getCell(col, row + j);
				cl.setContent(person.getFirstName());
				cl = ss.getCell(col + 1, row + j);
				cl.setContent(person.getLastName());

				/**
				 * n_cols =10 - max number of fields to export per contact
				 * n_cols=5 if contact doesn't have secondary address.
				 */
				int n_cols = 10;
				if (secAddr == null) {
					n_cols = 5;
				}
				/**
				 * Loop selected collumns to export Switch is used to map the
				 * information selected to export
				 *
				 * fields={0-street1 , 1-area1 , 2-zipcode1 , 3-city1 ,
				 * 4-country1 , 5-street2 , 6-area2 , 7-zipcode2 , 8-city2 ,
				 * 9-country2}.
				 */
				if (mainAddr != null) {
					for (int i = 0; i < n_cols; i++) {
						if (fields.get(i) == true) {
							switch (i) {
								case 0:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(mainAddr.getStreet());
									break;
								case 1:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(mainAddr.getArea());
									break;
								case 2:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(mainAddr.getZipCode());
									break;
								case 3:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(mainAddr.getCity());
									break;
								case 4:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(mainAddr.getCountry());
									break;
								case 5:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(secAddr.getStreet());
									break;
								case 6:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(secAddr.getArea());
									break;
								case 7:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(secAddr.getZipCode());
									break;
								case 8:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(secAddr.getCity());
									break;
								case 9:
									cl = ss.getCell(act_col, row + j);
									cl.setContent(secAddr.getCountry());
									break;

							}
							act_col++;
						}
					}
				}
			}
		} catch (FormulaCompilationException ex) {
			Logger.getLogger(Export_Address.class.getName()).
				log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Method to export addresses from one contact with the selected fields.
	 *
	 * @param uicontroller
	 * @param contact
	 * @param fields
	 *
	 */
	public void exportAddress_one(UIController uicontroller, Person contact,
								  ArrayList<Boolean> fields) {
		try {
			this.uicontroller = uicontroller;
			this.ss = this.uicontroller.getActiveSpreadsheet();
			/**
			 * Obtain the current active cell position.
			 */
			col = uicontroller.getActiveCell().getAddress().getColumn();
			row = uicontroller.getActiveCell().getAddress().getRow();

			Address mainAddr = contact.getMainAddress();
			Address secAddr = contact.getSecAddress();

			int act_col = col + 2;
			cl = ss.getCell(col, row);
			cl.setContent(contact.getFirstName());
			cl = ss.getCell(col + 1, row);

			cl.setContent(contact.getLastName());

			/**
			 * n_cols =10 - max number of fields to export per contact n_cols=5
			 * if contact doesn't have secondary address.
			 */
			int n_cols = 10;
			if (secAddr == null) {
				n_cols = 5;
			}
			/**
			 * Loop selected collumns to export Switch is used to map the
			 * information selected to export
			 *
			 * fields={0-street1 , 1-area1 , 2-zipcode1 , 3-city1 , 4-country1 ,
			 * 5-street2 , 6-area2 , 7-zipcode2 , 8-city2 , 9-country2}.
			 */
			for (int i = 0; i < n_cols; i++) {
				if (fields.get(i) == true) {
					switch (i) {
						case 0:
							cl = ss.getCell(act_col, row);
							cl.setContent(mainAddr.getStreet());
							break;
						case 1:
							cl = ss.getCell(act_col, row);
							cl.setContent(mainAddr.getArea());
							break;
						case 2:
							cl = ss.getCell(act_col, row);
							cl.setContent(mainAddr.getZipCode());
							break;
						case 3:
							cl = ss.getCell(act_col, row);
							cl.setContent(mainAddr.getCity());
							break;
						case 4:
							cl = ss.getCell(act_col, row);
							cl.setContent(mainAddr.getCountry());
							break;
						case 5:
							cl = ss.getCell(act_col, row);
							cl.setContent(secAddr.getStreet());
							break;
						case 6:
							cl = ss.getCell(act_col, row);
							cl.setContent(secAddr.getArea());
							break;
						case 7:
							cl = ss.getCell(act_col, row);
							cl.setContent(secAddr.getZipCode());
							break;
						case 8:
							cl = ss.getCell(act_col, row);
							cl.setContent(secAddr.getCity());
							break;
						case 9:
							cl = ss.getCell(act_col, row);
							cl.setContent(secAddr.getCountry());
							break;

					}
					act_col++;
				}
			}
		} catch (FormulaCompilationException ex) {
			Logger.getLogger(Export_Address.class.getName()).
				log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void exportAll(UIController uicontroller) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
