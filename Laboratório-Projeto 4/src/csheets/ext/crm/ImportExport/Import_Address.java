/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.ImportExport;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.crm.domain.Address;
import csheets.ext.crm.domain.Person;
import csheets.ext.crm.persistence.JPAAddressRepository;
import csheets.ext.crm.persistence.JPAPersonRepository;
import csheets.ext.crm.persistence.Persistence;
import csheets.ui.ctrl.UIController;
import java.util.List;

/**
 *
 * @author André Garrido - 1101598
 */
public class Import_Address implements Import {

	private UIController uicontroller;
	private Spreadsheet ss;
	Cell cl;
	JPAPersonRepository rep_cont = Persistence.getJPARepositoryFactory().
		getJPAPersonRepository();
	JPAAddressRepository rep_addr = Persistence.getJPARepositoryFactory().
		getJPAAddressRepository();

	public Import_Address(UIController uicontroller) {
		this.uicontroller = uicontroller;
	}

	public void ImportContactAddress(List<Person> allcontacts) {
		for (int i = 0; i < allcontacts.size(); i++) {
			Person contact = allcontacts.get(i);
			Address mainAddr = contact.getMainAddress();
			Address secAddr = contact.getSecAddress();
			if (secAddr == null) {
				rep_addr.save(mainAddr);
				rep_cont.addContact(contact);
			} else {
				rep_addr.save(mainAddr);
				rep_addr.save(secAddr);
				rep_cont.addContact(contact);
			}
		}

	}

	public void ImportContacts(List<Person> allcontacts) {

		for (Person contact : allcontacts) {
			rep_cont.addContact(contact);
		}

	}

	public void ImportOneContact(Person contact) {

		rep_cont.addContact(contact);
	}
}
