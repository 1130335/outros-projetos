/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.tasks;

import csheets.ext.crm.domain.Contact;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Aria
 */
@Entity
public class task implements Serializable {
    @Id
    @GeneratedValue
    private String name;
    private String description;
    private int priority;
    private int execution;
    private Contact contact;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date;
    
    public task() {
    }
    
    public task(String name, String d, int p, int e, Contact contact, Date data) {
        this.name = name;
        this.description = d;
        this.priority = p;
        this.execution = e;
        this.contact = contact;
        this.date = data;
      
    }

    /**
     * @return the name
     */
    public task UpdateTast(String name, String d, int p, int e, Contact contact, Date data) {
        this.name = name;
        this.description = d;
        this.priority = p;
        this.execution = e;
        this.contact = contact;
        this.date = data;
        return this;
      
    }
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the execution
     */
    public int getExecution() {
        return execution;
    }

    /**
     * @param execution the execution to set
     */
    public void setExecution(int execution) {
        this.execution = execution;
    }

    /**
     * @return the contact
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
    public String toString (){
      
       return this.name;         
    }
    public String getDatas (){
        String s;
        s="name: "+this.name+"\n"+
         "description: "+this.description+"\n"+
          "Contact: "+this.contact.getContact_id()+"\n"+
          "priority: " +this.priority+"\n"+
          "execution: "+this.execution+"\n"+
           "date: "+this.date.getDay()+"/"+this.date.getMonth()+"/"+this.date.getYear();
       return s;         
    }
}
