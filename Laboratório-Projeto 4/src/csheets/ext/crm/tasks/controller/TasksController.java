/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.tasks.controller;

import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.JPARepositoryTasks;
import csheets.ext.crm.tasks.task;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aria
 */
public class TasksController {
    JPARepositoryTasks repo = new JPARepositoryTasks();
    JPAContactRepository repocont = new JPAContactRepository();
    
    
    public void create(String name, String d, int p, int e, Contact contact, Date data){
        task t = new task(name ,d,p,e,contact,data);
        repo.add(t);
    }
    public List<Contact> getListContact (){
       List<Contact> l = repocont.all();
       return l;
    }
    public void edit ( task t,String name, String d, int p, int e, Contact contact, Date data) {
    
     t.UpdateTast(name, d, p, e, contact, data);
     repo.updateTask(t);
        
       
       
        
    }
    public void delete (task t){
        repo.delete(t);
    }
    public List<task> AllTasks(){
        return repo.all();
    }
    public task[] orderByPriority() {
        List<task> list = this.AllTasks();
        Collections.sort(list, new Comparator<task>() {
            @Override
            public int compare(task task1, task task2) {
                String int1 = "" + task1.getPriority();
                String int2 = "" + task2.getPriority();
                return int1.compareTo(int2);
            }
        });
        task[] temp = new task[list.size()];
        for (int i = 0; i < list.size(); i++) {
            temp[i] = list.get(i);
        }
        return temp;
    }
    public task[] orderByDate() {
        List<task> list = this.AllTasks();
        Collections.sort(list, new Comparator<task>() {
            @Override
            public int compare(task task1, task task2) {

                return task1.getDate().compareTo(task2.getDate());
            }
        });
        task[] temp = new task[list.size()];
        for (int i = 0; i < list.size(); i++) {
            temp[i] = list.get(i);
        }
        return temp;
    }
    public task[] filterByExecution(task[] array, int execution) {
        List<task> temp = new ArrayList<task>();
        for (task array1 : array) {
            if (array1.getExecution() == execution) {
                temp.add(array1);
            }
        }
        task[] toRet = new task[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            toRet[i] = temp.get(i);
        }
        return toRet;
    }

    public task[] filterByContact(task[] array, String c) {
        Contact contact=getContact(c); 
        List<task> temp = new ArrayList<task>();
        for (task array1 : array) {
            if (array1.getContact().equals(contact)) {
                temp.add(array1);
            }
        }
        task[] toRet = new task[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            toRet[i] = temp.get(i);
        }
        return toRet;
    }

    private Contact getContact(String c) {
    List<Contact> l=repocont.all();
    Contact  cont = null;
    for(int i =0;i<l.size();i++){
        if(l.get(i).toString()==c){
            cont =l.get(i);
        }
    }
    return cont;
    }

    
}
