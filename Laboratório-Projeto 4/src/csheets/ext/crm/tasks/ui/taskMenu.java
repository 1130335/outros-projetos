/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.tasks.ui;


import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Aria
 */
public class taskMenu extends JMenu {
    public taskMenu(UIController uiController){
        super("Tasks");
        setMnemonic(KeyEvent.VK_L);
        
        add(new TaskAction(uiController));
        
    }
    
}
