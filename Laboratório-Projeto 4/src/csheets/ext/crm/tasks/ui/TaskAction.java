/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.tasks.ui;


import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Aria
 */
public class TaskAction extends BaseAction {
  private UIController uiController;

    /**
     * 
     * @param uiController 
     */
    public TaskAction(UIController uiController){
        this.uiController=uiController;
    }
    
    /**
     * 
     * @return Action name
     */
    @Override
    protected String getName() {
        return "TasksMenu";
    }

    /**
     * 
     */
    protected void defineProperties() {
	}
    
    /**
     * makes the window of edtion of alerts visible
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
       MenuTask m=new MenuTask();
       m.setVisible(true);
    }
}

