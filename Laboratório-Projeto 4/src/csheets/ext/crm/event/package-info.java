/**
 *
 * <p>
 * <b>Extension Contacts</b>
 *
 *
 *
 * An extension is a class that extends the Event Extension. CleanSheets
 * dynamically loads all the extensions that it finds declared in the following
 * property files: res/extensions.props and extensions.props. For the Contacts
 * extension to be loaded at startup a line with the fully qualified name of the
 * simple extension class must be present in one of the property files. The
 * fully qualified name of the simple extension is:
 * csheets.ext.simple.ExtensionExample
 * <p>
 * <p>
 * <b>Event Contacts</b>
 * *
 * <p>
 * All the extensions are loaded dynamically by the ExtensionManager at
 * application startup. <br/><br/>
 *
 * <p>
 * All the UI extensions are loaded by the UIController at application startup.
 * <br/><br/>
 *
 * <p>
 * All the menu extensions are loaded by the MenuBar at application startup.
 * <br/><br/>
 *
 *
 *
 * @author Andre Garrido 1101598
 *
 */
package csheets.ext.crm.event;
