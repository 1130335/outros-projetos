/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.event.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author daniel
 */
public class AccessInfoEvent {

    public AccessInfoEvent() {

    }

    public String getActiveContactType() {
        String strClassName = "";
        System.out.println("");
        Object Path = this.getClass().getResource("events.config");

        String path = Path.toString();
        String sub_path = path.substring(5, path.length());
        File configFile = new File(sub_path);
      
        try {
            FileReader url = new FileReader(configFile);
            Properties props = new Properties();
            Properties p = new Properties();
            p.load(url);
            strClassName = p.getProperty("EventActiveContact");
        } catch (Exception ex) {

        }
        return strClassName;

    }

    public void setActiveContactType(String type) {

        Properties prop = new Properties();
        OutputStream output = null;
        try {

            Object Path = this.getClass().getResource("events.config");
            String path = Path.toString();
            String sub_path = path.substring(5, path.length());
            output = new FileOutputStream(sub_path);
            // set the properties value
            prop.setProperty("EventActiveContact", type);

            // save properties to project root folder
            prop.store(output, type);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
