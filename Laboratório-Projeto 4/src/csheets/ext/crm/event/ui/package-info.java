/**
 * Provides the UI for the Events Extension
 *
 * @author AndreGarrido 1101598
 */
package csheets.ext.crm.event.ui;
