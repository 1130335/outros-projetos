/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.event.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author André Garrido<1101598>
 */
public class EventMenu extends JMenu {

	public EventMenu(UIController uiController) {
		super("Events");
		setMnemonic(KeyEvent.VK_E);

		add(new EventsAction(uiController));
	}
}
