/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.toolbarbuttonsui;

import javax.swing.JComponent;
import javax.swing.JToolBar;

/**
 *
 * @author Francisco Lopes 1130576
 */
public class ToolbarButtonsExtensionController {
    
    JToolBar toolBar;

    public ToolbarButtonsExtensionController() {
    }
    
    public JToolBar getToolbarUI(){
        return this.toolBar;
    }
    
    public void setToolbarUI(JComponent jtb){
        this.toolBar = (JToolBar) jtb;
    }
    
}
