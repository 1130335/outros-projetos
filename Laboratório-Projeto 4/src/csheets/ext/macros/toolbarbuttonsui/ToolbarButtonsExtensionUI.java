/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.toolbarbuttonsui;

import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.Extension;
import csheets.ext.macros.MacrosWindow.ui.MacrosWindow;
import csheets.ext.macros.toolbarbuttons.ToolbarButtonsExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Francisco Lopes 1130576
 */
public class ToolbarButtonsExtensionUI extends UIExtension{
    
    /**
	 * A side bar that provides editing of comments
	 */
	private JComponent sideBar;
        
                  private JToolBar toolBar;

	private ToolbarButtonsExtensionController controller;

	/**
	 * The menu of the extension
	 */
        
	public ToolbarButtonsExtensionUI(Extension extension, UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return null;
	}

	/**
	 * Returns an instance of a class that implements JMenu. In this simple case
	 * this class only supplies one menu option.
	 *
	 * @see ExampleMenu
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		return null;
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	public JToolBar getToolBar() {
		ToolbarButtons tbb = new ToolbarButtons();
                                    toolBar = tbb;
                                    return toolBar;
	}

	/**
	 * Returns a side bar that provides editing of comments.
	 *
	 * @return a side bar
	 */
	public JComponent getSideBar() {
		if (sideBar == null) {

			sideBar = new JPanel(new GridLayout(2, 1));
			sideBar.setName(ToolbarButtonsExtension.NAME);
                        
                                                      JPanel buttonPanel = new JPanel();

			// Creates components
                                                      JButton addButton = new JButton("Add Buttons");
                                                      JButton enableButton = new JButton("Enable Buttons");
                                                      JButton disableButton = new JButton("Disable Buttons");
                                                      
                                                      ButtonEvent addButtonEx = new ButtonEvent();
                                                      addButton.addActionListener(addButtonEx);
                                                      
                                                      ButtonEvent enableButtonEx = new ButtonEvent();
                                                      enableButton.addActionListener(enableButtonEx);
                                                      
                                                      ButtonEvent disableButtonEx = new ButtonEvent();
                                                      disableButton.addActionListener(disableButtonEx);

                                                      //adds buttons to panel
                                                      buttonPanel.add(addButton);
                                                      buttonPanel.add(enableButton);
                                                      buttonPanel.add(disableButton);
                                                      
			controller = new ToolbarButtonsExtensionController();

			// Creates side bar
                                                      sideBar.add(buttonPanel);
		}
		return sideBar;
	}

	public ToolbarButtonsExtensionController getController() {
		return controller;
	}

	public void setSideBarNull() {
		this.sideBar = null;
	}
    
                  private class ButtonEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("Add Buttons".equals(e.getActionCommand())) {
                                                            controller.setToolbarUI(toolBar);
                                                            executeAdd();
			}
                                                     else if("Enable Buttons".equals(e.getActionCommand())) {
                                                             executeEnable();
                                                     }
                                                     else if("Disable Buttons".equals(e.getActionCommand())){
                                                         executeDisable();
                                                     }
		}
	}
                   
                  private void executeAdd(){
                      addButtonUI addBUI = new addButtonUI(controller);
                      addBUI.setLocationRelativeTo(null);
                  }
                   
                  private void executeEnable(){
                      enableButtonUI enBUI = new enableButtonUI(controller);
                      enBUI.setLocationRelativeTo(null);
                  }
                  
                  private void executeDisable(){
                      DisableButtonUI disBUI = new DisableButtonUI(controller);
                      disBUI.setLocationRelativeTo(null);
                  }
                  
}
