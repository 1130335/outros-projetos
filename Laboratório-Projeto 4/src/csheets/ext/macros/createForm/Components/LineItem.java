/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.Component;
import java.io.Serializable;

/**
 *
 * @author Paulo
 */
public interface LineItem extends Serializable {

	public String DEFAULT_NAME = "Not defined";

	public Component create();

	public enum ITEM_TYPE {

		BUTTON, STATIC_TEXT_BOX, TEXT_BOX
	};

	public String getName();

	public String getText();

	public void setName(String name);

	public void setText(String text);
}
