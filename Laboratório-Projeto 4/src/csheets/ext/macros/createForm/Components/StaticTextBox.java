/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.Component;
import javax.swing.JTextField;

/**
 *
 * @author Paulo
 */
public class StaticTextBox implements LineItem {

	String text = DEFAULT_NAME;
	String name = DEFAULT_NAME;

	@Override
	public Component create() {
		JTextField jtextfield = new JTextField();
		jtextfield.setText(text);
		jtextfield.setName(name);
		jtextfield.setEnabled(false);
		return jtextfield;
	}

	public String toString() {
		return ITEM_TYPE.STATIC_TEXT_BOX.toString();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

}
