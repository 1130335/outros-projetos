/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author Paulo
 */
public class Button implements LineItem {

	private String name = DEFAULT_NAME;

	@Override
	public Component create() {
		JButton button = new JButton();
		button.setName(name);
		button.setText(name);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
			}
		}
		);
		return button;
	}

	public String toString() {
		return ITEM_TYPE.BUTTON.toString();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getText() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setText(String text) {
	}

}
