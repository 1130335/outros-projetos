/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import csheets.ext.macros.createForm.UI.ChangeButtonDialog;
import csheets.ext.macros.createForm.UI.ChangeTextBoxDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;

/**
 *
 * @author Paulo
 */
public class ChangePropertiesButton extends JButton {

	private JComboBox jcombobox;

	public ChangePropertiesButton(JComboBox jcombobox) {
		super();
		this.jcombobox = jcombobox;
		this.setName("Change Properties");
		this.setText("Change Properties");
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				LineItem item = (LineItem) ChangePropertiesButton.this.jcombobox.
					getSelectedItem();
				if (item instanceof Button) {
					new ChangeButtonDialog(null, true, item);
				} else {
					new ChangeTextBoxDialog(null, true, item);
				}
			}
		});

	}
}
