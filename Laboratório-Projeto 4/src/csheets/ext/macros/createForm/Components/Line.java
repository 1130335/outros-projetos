/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author Paulo
 */
public class Line extends JPanel {

	public static int NUM_COL = 4;
	public static int NUM_ROWS = 1;

	public Line() {
		super(new GridLayout(NUM_ROWS, NUM_COL));
		addComponents();
	}

	public void addComponents() {

		List<LineItem> line = new ArrayList<>();
		line.add(new Button());
		line.add(new TextBox());
		line.add(new StaticTextBox());
		JComboBox jcbox = new JComboBox(line.toArray());
		jcbox.setSelectedIndex(0);
		this.add(jcbox);
		JButton button = new ChangePropertiesButton(jcbox);
		this.add(button);

		List<LineItem> line2 = new ArrayList<>();
		line2.add(new Button());
		line2.add(new TextBox());
		line2.add(new StaticTextBox());
		JComboBox jcbox2 = new JComboBox(line2.toArray());
		jcbox2.setSelectedIndex(0);
		this.add(jcbox2);

		JButton button2 = new ChangePropertiesButton(jcbox2);
		this.add(button2);

//		for (Component listaItem : listaItems) {
		//	this.add(listaItem);
		//}
	}

}
