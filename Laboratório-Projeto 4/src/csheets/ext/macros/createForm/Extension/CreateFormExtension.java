/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Extension;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Paulo
 */
public class CreateFormExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Create Form";

	/**
	 * Creates a new create form extension.
	 */
	public CreateFormExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UICreateFormExtension(this, uiController);
	}

}
