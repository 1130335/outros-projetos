/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Extension;

import csheets.ext.Extension;
import csheets.ext.macros.createForm.Controller.CreateFormController;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author Paulo 1130313
 */
public class UICreateFormExtension extends UIExtension {

	/**
	 * The icon to display with the extension's name
	 */
	private Icon icon;
	/**
	 * SideBar where the information will appears
	 */
	private JComponent sideBar;
	/**
	 * The controller of this class
	 */
	private CreateFormController controller;

	/**
	 * The menu of the extension
	 */
	private CreateFormMenu menu;

	/**
	 * Creates a new Conditional Formatting Single Cell Extension UI
	 *
	 * @param extension The extension to apply this UI
	 * @param uiController The interface controller
	 */
	public UICreateFormExtension(Extension extension,
								 UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Returns an instance of a class that implements JMenu.
	 *
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		if (menu == null) {
			menu = new CreateFormMenu(uiController);
		}
		return menu;
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	public JToolBar getToolBar() {
		return null;
	}

	/**
	 * Returns a side bar that gives access to extension-specific functionality.
	 *
	 * @return a component, or null if the extension does not provide one
	 */
	public JComponent getSideBar() {
		return sideBar;
	}

	public CreateFormController getController() {
		return new CreateFormController(uiController);
	}

}
