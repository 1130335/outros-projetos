/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Extension;

import csheets.ext.macros.createForm.UI.CreateFormWindow;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo 1130313
 */
@SuppressWarnings("serial")
public class CreateFormExtensionAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new align bottom action.
	 *
	 * @param uiController the user interface controller
	 */
	public CreateFormExtensionAction(
		UIController uiController) {
		this.uiController = uiController;

	}

	/**
	 * Return the name of this action
	 *
	 * @return the name
	 */
	protected String getName() {
		return "Create Form";
	}

	/**
	 * Properties of this action
	 */
	protected void defineProperties() {
	}

	/**
	 * An action that presents a new window, in which the user can do operations
	 *
	 * @param event the event that was fired
	 */
	public void actionPerformed(ActionEvent event) {

		CreateFormWindow window = null;
		try {
			window = new CreateFormWindow(uiController);
		} catch (IOException ex) {
			Logger.getLogger(CreateFormExtensionAction.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(CreateFormExtensionAction.class.getName()).log(Level.SEVERE, null, ex);
		}
		window.setVisible(true);
	}
}
