/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Extension;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Paulo 1130313
 */
public class CreateFormMenu extends JMenu {

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created.
	 *
	 * @param uiController the user interface controller
	 */
	public CreateFormMenu(UIController uiController) {
		super("Create Form");
		setMnemonic(KeyEvent.VK_C);

		// Adds font actions
		add(new CreateFormExtensionAction(uiController));
		//add(new ConditionalFormattingRangeAction(uiController));
	}
}
