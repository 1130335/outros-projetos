package csheets.ext.macros.createForm.Controller;

import csheets.core.variable.Variable;
import csheets.core.variable.VariableList;
import csheets.ext.macros.createForm.Components.LineItem;
import csheets.ext.macros.createForm.Components.StaticTextBox;
import csheets.ext.macros.createForm.Components.TextBox;
import csheets.ui.ctrl.UIController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

/**
 * A controller for updating the user-specified comment of a cell.
 *
 * @author Paulo 1130313
 */
public class CreateFormController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	private List<LineItem> list;

	/**
	 * Creates a new Create Form controller.
	 *
	 * @param uiController the user interface controller
	 *
	 */
	public CreateFormController(UIController uiController) {
		this.uiController = uiController;
		list = new ArrayList<>();

	}

	public void setForm(JFrame form) {
		int cont = 0;
		for (LineItem l : list) {
			if (l instanceof TextBox || l instanceof StaticTextBox) {

				Variable v = VariableList.newVariable("@" + l.getName(), l.getText(), 0);
			}
			cont++;
		}

		this.uiController.getActiveSpreadsheet().addForm(form);
	}

	public JFrame getForm(String n) {
		return this.uiController.getActiveSpreadsheet().getFormByName(n);
	}

	public void setListLines(List<LineItem> list) {
		this.list = list;
	}

	public void saveForm() throws IOException {
		LineItem[] l = new LineItem[list.size()];
		int i = 0;
		for (LineItem l1 : list) {
			l[i] = l1;
			i++;
		}
		PersistForm eF = new PersistForm();
		eF.persistForm(l);
	}

	public void loadForm() throws IOException, ClassNotFoundException {
		PersistForm eF = new PersistForm();
		LineItem[] l = new LineItem[list.size()];
		l = eF.getPersistedForms();
		for (int i = 0; i < l.length; i++) {
			list.add(l[i]);
		}

	}

	public List<LineItem> getList() {
		return list;
	}

}
