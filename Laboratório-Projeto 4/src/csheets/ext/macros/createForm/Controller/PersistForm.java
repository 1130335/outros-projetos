/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Controller;

import csheets.ext.macros.createForm.Components.LineItem;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Utilizador
 */
class PersistForm {

	public PersistForm() {
	}

	public void persistForm(LineItem[] listForm) throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
			"../Binary/teste.dat"));
		oos.writeObject(listForm);
		oos.close();

	}

	public LineItem[] getPersistedForms() throws IOException, ClassNotFoundException {
		LineItem[] l;
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
			"../Binary/teste.dat"));
		l = (LineItem[]) (ois.readObject());

		return l;
	}
}
