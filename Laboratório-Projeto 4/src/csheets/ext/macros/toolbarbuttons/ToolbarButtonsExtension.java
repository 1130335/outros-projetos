/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.toolbarbuttons;

import csheets.ext.Extension;
import csheets.ext.macros.toolbarbuttonsui.ToolbarButtonsExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Francisco Lopes 1130576
 */
public class ToolbarButtonsExtension extends Extension {
    
    /**
	 * The name of the extension
	 */
	public static final String NAME = "Button Adder";

	/**
	 * Creates a new Navigator extension
	 */
	public ToolbarButtonsExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		if (this.uiExtension == null) {
			this.uiExtension = new ToolbarButtonsExtensionUI(this, uiController);
		}
		return this.uiExtension;
	}

	public void renewStatusInfo() {
		ToolbarButtonsExtensionUI uiExt;
                                    uiExt = (ToolbarButtonsExtensionUI) this.uiExtension;
		uiExt.setSideBarNull();
		uiExt.getSideBar();
	}
    
}
