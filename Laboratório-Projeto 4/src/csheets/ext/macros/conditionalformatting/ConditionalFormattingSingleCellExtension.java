/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.conditionalformatting;

import csheets.ext.Extension;
import csheets.ext.macros.conditionalformatting.ui.UIConditionalFormattingSingleCellExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Sergio 1130711
 */
public class ConditionalFormattingSingleCellExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Conditional Formatting Single Cell";

	/**
	 * Creates a new Conditional Formatting Single Cell extension.
	 */
	public ConditionalFormattingSingleCellExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIConditionalFormattingSingleCellExtension(this, uiController);
	}

}
