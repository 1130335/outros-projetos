/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.conditionalformatting.SideBar;

import csheets.ext.Extension;
import csheets.ext.macros.conditionalformatting.ConditionalFormattingSingleCellExtension;
import csheets.ext.macros.conditionalformatting.ui.ConditionalFormattingSingleCellController;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Sergio 1130711
 */
public class UIConditionalFormattingSingleCellSideBarExtension extends UIExtension {

	/**
	 * The icon to display with the extension's name
	 */
	private Icon icon;
	/**
	 * The sideBar of Conditional Formatting Single Cell SideBar Extension
	 */
	private JComponent sideBar;

	/**
	 * The controller Conditional Formatting Single Cell Controller,to controll
	 * the actions
	 */
	private ConditionalFormattingSingleCellController controller;

	/**
	 * Create a new Conditional Formatting Single Cell SideBar Extension UI
	 *
	 * @param extension The extension to apply this UI
	 * @param uiController The interface controller
	 */
	public UIConditionalFormattingSingleCellSideBarExtension(Extension extension,
															 UIController uiController) {
		super(extension, uiController);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Returns an instance of a class that implements JMenu.
	 *
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		return null;
	}

	/**
	 * Returns a toolbar that gives access to extension-specific functionality.
	 *
	 * @return a JToolBar component, or null if the extension does not provide
	 * one
	 */
	public JToolBar getToolBar() {
		return null;
	}

	/**
	 * Returns a side bar that gives access to extension-specific functionality.
	 *
	 * @return a component, or null if the extension does not provide one
	 */
	public JComponent getSideBar() {
		if (sideBar == null) {

			sideBar = new JPanel(new GridLayout(2, 1));
			sideBar.setName(ConditionalFormattingSingleCellExtension.NAME);

			// Creates components
			JTextArea last = new JTextArea();

			controller = new ConditionalFormattingSingleCellController(uiController);

			controller.setText(last);

			JScrollPane activePane = new JScrollPane(last);

			// Adds borders
			TitledBorder border = BorderFactory.
				createTitledBorder("Cell Formatted(Conditional");
			border.setTitleJustification(TitledBorder.CENTER);
			activePane.setBorder(border);
			// Creates side bar
			sideBar.add(activePane);
		}
		return sideBar;
	}

	/**
	 * Set the sideBar of this UI to null
	 */
	public void setSideBarNull() {
		this.sideBar = null;
	}

}
