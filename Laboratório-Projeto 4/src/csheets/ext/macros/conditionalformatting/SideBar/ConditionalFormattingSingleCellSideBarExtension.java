/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.conditionalformatting.SideBar;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Sergio 1130711
 */
public class ConditionalFormattingSingleCellSideBarExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Conditional Formatted";

	/**
	 * Creates a new Conditional Formatting Single Cell SideBar extension.
	 */
	public ConditionalFormattingSingleCellSideBarExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		if (this.uiExtension == null) {
			this.uiExtension = new UIConditionalFormattingSingleCellSideBarExtension(this, uiController);
		}
		return this.uiExtension;
	}

	/**
	 * Update the side bar that show the last cell formatted by Conditional
	 * Formattion
	 */
	public void renewStatusInfo() {
		UIConditionalFormattingSingleCellSideBarExtension uiExt = (UIConditionalFormattingSingleCellSideBarExtension) this.uiExtension;
		uiExt.setSideBarNull();
		uiExt.getSideBar();
	}
}
