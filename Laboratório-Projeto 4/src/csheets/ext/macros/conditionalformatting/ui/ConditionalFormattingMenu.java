/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.conditionalformatting.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author Sergio 1130711
 */
public class ConditionalFormattingMenu extends JMenu {

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created.
	 *
	 * @param uiController the user interface controller
	 */
	public ConditionalFormattingMenu(UIController uiController) {
		super("Conditional Formatting Cell");
		setMnemonic(KeyEvent.VK_F);

		// Adds font actions
		add(new ConditionalFormattingSingleCellExtensionAction(uiController));
		//add(new ConditionalFormattingRangeAction(uiController));
	}
}
