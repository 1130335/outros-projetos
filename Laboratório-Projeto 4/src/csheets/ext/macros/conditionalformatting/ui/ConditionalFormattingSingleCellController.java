package csheets.ext.macros.conditionalformatting.ui;

import csheets.ui.ctrl.UIController;
import javax.swing.JTextArea;

/**
 * A controller for updating the user-specified comment of a cell.
 *
 * @author Sergio 1130711
 */
public class ConditionalFormattingSingleCellController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new Conditional Formatting Single Cell controller.
	 *
	 * @param uiController the user interface controller
	 * @param uiPanel the user interface panel
	 */
	public ConditionalFormattingSingleCellController(UIController uiController) {
		this.uiController = uiController;

	}

	/**
	 * Set the text in sideBar,with the information of the last conditional
	 * formatted cell
	 *
	 * @param active where the information should be display
	 */
	public void setText(JTextArea active) {

		String strLast = "";
		if (uiController.getLastCFormattedCell() != null) {
			strLast = uiController.getLastCFormattedCell().
				getAddress().toString();
		} else {
			strLast = "";

		}
		active.setText(strLast);
	}
}
