/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.conditionalformatting.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Sergio
 */
@SuppressWarnings("serial")
public class ConditionalFormattingSingleCellExtensionAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new align bottom action.
	 *
	 * @param uiController the user interface controller
	 */
	public ConditionalFormattingSingleCellExtensionAction(
		UIController uiController) {
		this.uiController = uiController;

	}

	/**
	 * Return the name of this action
	 *
	 * @return the name
	 */
	protected String getName() {
		return "Single Cell";
	}

	/**
	 * Properties of this action
	 */
	protected void defineProperties() {
	}

	/**
	 * An action that presents a new window, in which the user can do operations
	 *
	 * @param event the event that was fired
	 */
	public void actionPerformed(ActionEvent event) {

		ConditionalFormattingSingleCellWindow window = new ConditionalFormattingSingleCellWindow(uiController);
		window.setVisible(true);
		window.setLocation(500, 200);
	}
}
