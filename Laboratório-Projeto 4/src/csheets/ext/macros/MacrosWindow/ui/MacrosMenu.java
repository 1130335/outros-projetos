package csheets.ext.macros.MacrosWindow.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author 1130486
 */
public class MacrosMenu extends JMenu {

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created. A menu
	 * option is an action (in this case
	 * {@link csheets.ext.simple.ui.ExampleAction})
	 *
	 * @param uiController the user interface controller
	 */
	public MacrosMenu(UIController uiController) {
		super("Macros' Window");
		setMnemonic(KeyEvent.VK_E);

		// Adds font actions
		add(new MacrosAction(uiController));
	}
}
