package csheets.ext.macros.MacrosWindow.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 * An action that allows the user to interact with the system and write macros.
 *
 * @author 1130486
 */
public class MacrosAction extends BaseAction {

	/**
	 * The user interface controller
	 */
	private final UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public MacrosAction(UIController uiController) {
		this.uiController = uiController;
	}

	@Override
	protected String getName() {
		return "Macros' Window";
	}

	protected void defineProperties() {
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		//Present the main menu for contacts
		MacrosWindow mWind = new MacrosWindow();
	}

}
