/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.MacrosWindow.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author 1130486
 */
public class MacrosWindow extends JFrame {

	/**
	 * WidthFrame Variable
	 */
	private static final int widthFrame = 450;
	/**
	 * HeightFrame Variable
	 */
	private static final int heightFrame = 250;

	/**
	 * GridLayout Variable
	 */
	private GridLayout grid;
	/**
	 * JPanel Variables
	 */
	private JPanel mainPanel, upPanel, downPanel;
	/**
	 * Label Variable
	 */
	private JLabel label;

	/**
	 * Text Area Variable
	 */
	private JTextArea textA;
	/**
	 * JButton variable - option for the user
	 */
	private JButton executeButton;
	/**
	 * ButtonEvent - when the user presses the button something happens
	 */
	private ButtonEvent execute;

	public MacrosWindow() {
		super("Macros' Window");

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screensize = toolkit.getScreenSize();
		int widthEcra = ((int) screensize.getWidth() / 2);
		int heightEcra = ((int) screensize.getHeight() / 2);
		widthEcra -= (widthFrame / 2);
		heightEcra -= (heightFrame / 2);
		setLocation(widthEcra, heightEcra);

		grid = new GridLayout(2, 1);
		mainPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		//Label, TextArea e JScrollBar
		label = new JLabel("You can write your desired macros down below:");
		textA = new JTextArea(5, 30);
		textA.setLineWrap(true);
		JScrollPane scr = new JScrollPane(textA, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		//Button
		execute = new ButtonEvent();
		executeButton = new JButton("Execute");
		executeButton.addActionListener(execute);

		//Adicionar
		mainPanel.setLayout(grid);
		upPanel.add(label, BorderLayout.EAST);
		downPanel.add(scr, BorderLayout.CENTER);
		downPanel.add(executeButton);
		mainPanel.add(upPanel);
		mainPanel.add(downPanel);
		add(mainPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setVisible(true);
	}

	private class ButtonEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("Execute".equals(e.getActionCommand())) {
				String macro = textA.getText();
				if (!macro.isEmpty()) {
					try {
						execute(macro);
					} catch (FormulaCompilationException ex) {
						Logger.getLogger(MacrosWindow.class.getName()).
							log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}

	private void execute(String message) throws FormulaCompilationException {
		textA.setText(null);
		Workbook w = new Workbook(2);
		Spreadsheet ss = w.getSpreadsheet(0);
		Cell c = ss.getCell(0, 0);
		c.setContent(message);
		JOptionPane.
			showMessageDialog(null, c.getValue().toString(), "Macro's Result", 1);
	}
}
