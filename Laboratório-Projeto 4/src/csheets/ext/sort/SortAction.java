/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sort;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * ss
 *
 * @author IvoTeixeira
 */
public class SortAction extends FocusOwnerAction {

	int row;

	/**
	 * Controller
	 */
	private UIController uiController;

	/**
	 * Range of cells
	 */
	public Cell[][] range;

	/**
	 * Constructor
	 *
	 * @param controller
	 */
	public SortAction(UIController controller) {
		uiController = controller;

	}

	/**
	 * Constructor without parameters
	 */
	public SortAction() {

	}

	/**
	 * Method that returns the name
	 *
	 * @return name
	 */
	@Override
	protected String getName() {
		return "Sort";
	}

	/**
	 * Method that defines the properties of the class
	 */
	@Override
	protected void defineProperties() {
		setEnabled(true);
		putValue(SMALL_ICON, new ImageIcon(CleanSheets.class.
				 getResource("res/img/sort.gif")));

	}

	/**
	 * Method that veryfies that a valid range of cells where selected and calls the SortSellectedCells Method
	 *
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//returns the selected cells of the spreadsheet
		range = focusOwner.getSelectedCells();

		//verifies that at least a range of cells is selected
		if (allCellsAreEmpty(range) == true) {
			JOptionPane.
				showMessageDialog(null, "Please select a range of cells", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		//verifies that only one collumn is selected
		if (verifyCollumnCount(range) == true) {
			SortGroupOfCellsUI dialogG = new SortGroupOfCellsUI(new javax.swing.JFrame(), true);

			int value = dialogG.getValue();

			row = dialogG.getRow();

			if (value == 1) {
				try {
					sortGroupDescending(range);
				} catch (FormulaCompilationException ex) {
					Logger.getLogger(SortAction.class.getName()).
						log(Level.SEVERE, null, ex);
				}

			} else {
				try {
					sortGroupAscending(range);
				} catch (FormulaCompilationException ex) {
					Logger.getLogger(SortAction.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}

		} else {

			SortSelectedCells dialog = new SortSelectedCells(new javax.swing.JFrame(), true);

			//returns the option selected ('1' foir descending and '2' for ascending)
			int value = dialog.getValue();

			if (value == 1) {
				try {
					sortDescending(range);
				} catch (FormulaCompilationException ex) {
					Logger.getLogger(SortAction.class.getName()).
						log(Level.SEVERE, null, ex);
				}

			} else {
				try {
					sortAscending(range);
				} catch (FormulaCompilationException ex) {
					Logger.getLogger(SortAction.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	// returns true if all the cells are empty and false if at least one cell have a value
	public boolean allCellsAreEmpty(Cell[][] range) {
		for (int i = 0; i < range.length; i++) {
			for (int j = 0; j < range[i].length; j++) {
				if (range[i][j].getContent().isEmpty()) {
				} else {
					return false;
				}
			}

		}
		return true;
	}

	//returns true if the number of selected cells is higher than 1 and false if lower
	public boolean verifyCollumnCount(Cell[][] range) {
		if (range[0].length > 1) {
			return true;
		}
		return false;
	}

	//sorts the range of cells by ascending order
	public void sortAscending(Cell[][] range) throws FormulaCompilationException {
		int i = 0;
		int j = 0;

		boolean flag = true;
		String temp;

		while (flag) {
			flag = false;
			for (j = 0; j < range.length - 1; j++) {
				if (range[j][0].getValue().compareTo(range[j + 1][0].
					getValue()) > 0) {
					temp = range[j][0].getContent();
					range[j][0].setContent(range[j + 1][0].getContent());
					range[j + 1][0].setContent(temp);
					flag = true;
				}
			}
		}
	}

	//sorts the range of cells by descending order
	public void sortDescending(Cell[][] range) throws FormulaCompilationException {

		int i = 0;
		int j = 0;

		boolean flag = true;
		String temp;

		while (flag) {
			flag = false;
			for (j = 0; j < range.length - 1; j++) {
				if (range[j][0].getValue().compareTo(range[j + 1][0].
					getValue()) < 0) {
					temp = range[j][0].getContent();
					range[j][0].setContent(range[j + 1][0].getContent());
					range[j + 1][0].setContent(temp);
					flag = true;              //shows a swap occurred
				}
			}
		}
	}

	//sorts the group of cells by ascending order
	public void sortGroupAscending(Cell[][] range) throws FormulaCompilationException {

		int j = 0;

		boolean flag = true;
		String temp;

		while (flag) {
			flag = false;
			for (j = 0; j < range.length - 1; j++) {
				if (range[j][row].getValue().compareTo(range[j + 1][row].
					getValue()) > 0) {
					temp = range[j][row].getContent();
					range[j][row].setContent(range[j + 1][row].getContent());
					range[j + 1][row].setContent(temp);
					flag = true;
				}
			}
		}
	}

	//sorts the group of cells by descending order
	public void sortGroupDescending(Cell[][] range) throws FormulaCompilationException {

		int j = 0;

		boolean flag = true;
		String temp;

		while (flag) {
			flag = false;
			for (j = 0; j < range.length - 1; j++) {
				if (range[j][row].getValue().compareTo(range[j + 1][row].
					getValue()) < 0) {
					temp = range[j][row].getContent();
					range[j][row].setContent(range[j + 1][row].getContent());
					range[j + 1][row].setContent(temp);
					flag = true;              //shows a swap occurred
				}
			}
		}
	}

}
