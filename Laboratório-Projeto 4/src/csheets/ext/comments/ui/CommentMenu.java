package csheets.ext.comments.ui;

import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author 1121248DiogoVieira
 */
public class CommentMenu extends JMenu {

	public CommentMenu(UIController uiController) {
		super("Comments");
		add(new CommentAction(uiController));
	}
}
