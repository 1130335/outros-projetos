package csheets.ext.comments.ui;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.Spreadsheet;
import csheets.ext.CellExtension;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.MyLinkedMap;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

/**
 * A new window navigator for all comments in the current sheet
 *
 * @author 1121248DiogoVieira
 */
public final class CommentNavigatorJDialog extends JDialog {

	private final JButton btnExit, btnSearch;
	private final JTextField txtSearch;
	private final JTextArea searchResult = new JTextArea();
	private final JTree tree;
	private final ArrayList<CommentableCell> cellTrees;
	private final JScrollPane scrollbar;
	private static final ImageIcon find = new ImageIcon(CleanSheets.class.
		getResource("res/img/findBooks.png"));

	/**
	 * The window constructor for the navigator
	 *
	 * @param window
	 * @param uiController
	 */
	public CommentNavigatorJDialog(JFrame window, UIController uiController) {
		super(window, "Comment Navigator");

		this.setLayout(new BorderLayout());

		/* Defines the main root for the JTree, in this case is the name of the current spread sheet */
		Spreadsheet currentSheet = uiController.getActiveSpreadsheet();
		DefaultMutableTreeNode rootSystem = new DefaultMutableTreeNode(currentSheet.
			getTitle());
		tree = new JTree(rootSystem);
		/* Makes sure that only one item can be selected at a time since by default you can select multiple of them */
		tree.getSelectionModel().
			setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		/* Function that returns all the commented cells in the current spreadSheet */
		cellTrees = getCommentedCells(currentSheet);
		if (cellTrees.size() < 0) {
			dispose();
		}
		MyLinkedMap map;
		for (CommentableCell cellTree : cellTrees) {
			map = cellTree.getMap();
			DefaultMutableTreeNode childTree = addANode(cellTree.getAddress().
				toString(), rootSystem);
			for (int i = 0; i < map.size(); i++) {
				addANode(map.getEntry(i).getKey().toString(), childTree);
			}
		}

		/* North Panel containing the search fucntion */
		JPanel pNorth = new JPanel(new BorderLayout());
		JPanel pNorth1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel lblIconSearch = new JLabel();
		lblIconSearch.setIcon(find);
		pNorth1.add(lblIconSearch);
		txtSearch = new JTextField(50);
		pNorth1.add(txtSearch);
		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String result = searchComment(txtSearch.getText());
				if (result.isEmpty() || result.equals("")) {
					JOptionPane.
						showMessageDialog(rootPane, "No results were found!", "404 - Error", ERROR_MESSAGE);
				} else {
					searchResult.setText(result);
				}

			}
		});
		pNorth1.add(btnSearch);
		pNorth.add(pNorth1, BorderLayout.NORTH);

		JScrollPane pNorth2 = new JScrollPane(searchResult);
		pNorth.add(pNorth2, BorderLayout.CENTER);

		/* Center Panel which has the JTree and / or the result of the search */
		scrollbar = new JScrollPane(tree);
		scrollbar.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		/* South Panel */
		JPanel pSouth = new JPanel(new FlowLayout(FlowLayout.CENTER));

		btnExit = new JButton("Cancel");
		btnExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		pSouth.add(btnExit);

		add(pNorth, BorderLayout.NORTH);
		add(scrollbar, BorderLayout.CENTER);
		add(pSouth, BorderLayout.SOUTH);

		setSize(750, 450);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Function that returns a CommentableCell array of all the commented cells
	 * in the current spread sheet.
	 *
	 * @param s the spread sheet that is currently selected
	 * @return CommentableCell[] all the commented cells in the spreadsheet
	 */
	public ArrayList<CommentableCell> getCommentedCells(Spreadsheet s) {

		ArrayList<CommentableCell> aux = new ArrayList();
		int collums = s.getColumnCount();
		for (int i = 0; i < collums; i++) {
			Cell[] aux2 = s.getColumn(i);
			for (int j = 0; j < aux2.length; j++) {
				CellImpl p = (CellImpl) aux2[j];
				Map<String, CellExtension> map2 = p.getExtensions();
				for (Map.Entry<String, CellExtension> entry : map2.entrySet()) {

					if (entry.getKey().equals("Comments")) {

						CommentableCell m2 = (CommentableCell) entry.getValue();
						int comment = m2.getComments().split(";")[0].length();
						if (comment > 0) {
							aux.add(m2);
						}
					}
				}
			}
		}
		return aux;
	}

	/**
	 * Method which adds a component and adds it to a new node in the tree / to
	 * a new child in the parent node
	 *
	 * @param name
	 * @param parentFolder
	 * @return newFile returns a new Node for the tree
	 */
	private DefaultMutableTreeNode addANode(String name,
											DefaultMutableTreeNode parentFolder) {
		/* Creates a new node for the tree */
		DefaultMutableTreeNode newFile = new DefaultMutableTreeNode(name);

		/* Add attaches a name to the node */
		parentFolder.add(newFile);

		return newFile;
	}

	public String searchComment(String text) {
		String aux = "";
		for (CommentableCell commentableCell : cellTrees) {
			String comment = commentableCell.getComments().split(";")[0];
			if (comment.contains(text)) {
				if (aux.length() != 0) {
					aux += "\n search-" + text + " | " + commentableCell.
						toString() + " - " + commentableCell.getComments().
						split(";")[0];
				} else {
					aux += "search-" + text + " | " + commentableCell.toString() + " - " + commentableCell.
						getComments().split(";")[0];
				}
			}
		}
		return aux;
	}
}
