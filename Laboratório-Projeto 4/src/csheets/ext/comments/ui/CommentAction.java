package csheets.ext.comments.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 1121248DiogoVieira
 */
public class CommentAction extends BaseAction {

	private CommentNavigatorJDialog cn;

	/**
	 * The user interface controller
	 */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 *
	 * @param uiController the user interface controller
	 */
	public CommentAction(UIController uiController) {
		this.uiController = uiController;
	}

	@Override
	protected String getName() {
		return "Comment Navigator";
	}

	@Override
	protected void defineProperties() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		cn = new CommentNavigatorJDialog(null, uiController);
	}

}
