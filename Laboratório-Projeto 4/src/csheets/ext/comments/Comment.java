/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments;

/**
 * Represents a comment on a commentable cell
 *
 * @author Daniela Maia
 */
public class Comment {

	/**
	 * the comment on a commentable cell
	 */
	private String comment = "";

	/**
	 * the user who creates the comment
	 */
	private String user = "";

	public Comment(String comment, String user) {
		this.comment = comment;
		this.user = user;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

}
