package csheets.ext.comments;

import csheets.core.Cell;
import csheets.core.graphics.Graphic;
import csheets.ext.CellExtension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

/**
 * An extension of a cell in a spreadsheet, with support for comments.
 *
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
public class CommentableCell extends CellExtension {

	/**
	 * The unique version identifier used for serialization
	 */
	private static final long serialVersionUID = 1L;
	private final MyLinkedMap<String, String> commentMap = new MyLinkedMap();
	/**
	 * The cell's user-specified comment
	 */
	private List<Comment> comments = new ArrayList<>();

	/**
	 * The listeners registered to receive events from the comentable cell
	 */
	private transient List<CommentableCellListener> listeners
		= new ArrayList<CommentableCellListener>();

	/**
	 * Creates a comentable cell extension for the given cell.
	 *
	 * @param cell the cell to extend
	 */
	CommentableCell(Cell cell) {
		super(cell, CommentsExtension.NAME);
	}


	/*
	 * DATA UPDATES
	 */
//	public void contentChanged(Cell cell) {
//	}
	/*
	 * COMMENT ACCESSORS
	 */
	public MyLinkedMap<String, String> getMap() {

		return commentMap;
	}

	/**
	 * Get the cell's user comment.
	 *
	 * @return The user supplied comment for the cell or <code>null</code> if no
	 * user supplied comment exists.
	 */
	public String getComments() {
		String allComments = "";
		for (Comment c : comments) {
			allComments += c.getComment() + "; User: " + c.
				getUser() + "; ";
		}
		return allComments;
	}

	/**
	 * Returns whether the cell has a comment.
	 *
	 * @return true if the cell has a comment
	 */
	public boolean hasComment() {
		return !comments.isEmpty();
	}

	/*
	 * COMMENT MODIFIERS
	 */
	/**
	 * Sets the user-specified comment for the cell.
	 *
	 * @param comment the user-specified comment
	 */
	public void setComment(String comment) {
		Comment c = new Comment(comment, null);
		comments.clear();
		comments.add(c);
		setUser(comment);
		// Notifies listeners
		fireCommentsChanged();
	}

	/**
	 * Sets the user-specified comment for the cell.
	 *
	 * @param user the user who created the comment
	 */
	public void setUser(String user) {

	}

	/*
	 * EVENT LISTENING SUPPORT
	 */
	/**
	 * Registers the given listener on the cell.
	 *
	 * @param listener the listener to be added
	 */
	public void addCommentableCellListener(CommentableCellListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the given listener from the cell.
	 *
	 * @param listener the listener to be removed
	 */
	public void removeCommentableCellListener(CommentableCellListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies all registered listeners that the cell's comments changed.
	 */
	protected void fireCommentsChanged() {
		for (CommentableCellListener listener : listeners) {
			listener.commentChanged(this);
		}
	}

	/**
	 * Customizes serialization, by recreating the listener list.
	 *
	 * @param stream the object input stream from which the object is to be read
	 * @throws IOException If any of the usual Input/Output related exceptions
	 * occur
	 * @throws ClassNotFoundException If the class of a serialized object cannot
	 * be found.
	 */
	private void readObject(java.io.ObjectInputStream stream)
		throws java.io.IOException, ClassNotFoundException {
		stream.defaultReadObject();
		listeners = new ArrayList<CommentableCellListener>();
	}

	@Override
	public void createGraphic(String name, SortedSet<Cell> chosenCells) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Graphic getGraphic() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
