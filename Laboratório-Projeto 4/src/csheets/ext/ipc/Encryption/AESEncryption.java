/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.Encryption;

import csheets.ext.ipc.domain.SimplerCell;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;

/**
 *
 * @author Pedro
 */
public class AESEncryption {

    private static final int DEFAULT_KEY_SIZE = 128;
    
    public static SecretKey generateAESKey(int key_size) throws NoSuchAlgorithmException {
        KeyGenerator KeyGen = KeyGenerator.getInstance("AES");
        KeyGen.init(key_size);
        return KeyGen.generateKey();
    }
    
    public static byte[] encrypt(SimplerCell cell, SecretKey sec_key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException{
        Cipher AesCipher = Cipher.getInstance("AES");

        AesCipher.init(Cipher.ENCRYPT_MODE, sec_key);
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        outputStream.writeObject(cell);
        return AesCipher.doFinal(os.toByteArray());
    }
    
    public static SimplerCell decrypt(byte[] encr_cell, SecretKey sec_key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException, ClassNotFoundException{
        
        Cipher AesCipher = Cipher.getInstance("AES");

        AesCipher.init(Cipher.DECRYPT_MODE, sec_key);
        byte[] decrypted_info = AesCipher.doFinal(encr_cell);
        
        ByteArrayInputStream is = new ByteArrayInputStream(decrypted_info);
        ObjectInputStream InputStream = new ObjectInputStream(is);
        
        return (SimplerCell) InputStream.readObject();
    }

    public static int getDefaultKeySize(){return DEFAULT_KEY_SIZE;}
}
