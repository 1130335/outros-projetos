/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;

/**
 *
 * @author Utilizador
 */
public class SearchOnAnotherInstance extends FocusOwnerAction {

	private static final String TITLE = "Initiate the Search";

	/**
	 * Creates a new action.
	 */
	public SearchOnAnotherInstance() {

	}

	@Override
	protected String getName() {
		return TITLE;
	}

	@Override
	protected void defineProperties() {
	}

	/**
	 * @param event the event that was fired
	 */
	@Override
	public void actionPerformed(ActionEvent event) {

		new SearchInstance(focusOwner);
	}
}
