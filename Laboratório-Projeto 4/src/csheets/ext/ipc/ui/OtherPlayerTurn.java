/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_07.GamesController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class OtherPlayerTurn extends JFrame {

    private static final int widthFrame = 300;
    private static final int heightFrame = 100;

    private GridLayout grid;
    private JPanel centralPanel, upPanel, downPanel;
    private JLabel info;

    private JPanel centralPanelMy_Turn, upPanelMy_Turn, downPanelMy_Turn;
    private JLabel infoMy_Turn;
    private JButton ReceivePlayButtonOther_Turn;

    final GamesController controller;
    final CreateGameRoomUI pf1;
    final SearchPlayersUI pf2;

    public OtherPlayerTurn(CreateGameRoomUI ParentFrame1, SearchPlayersUI ParentFrame2, GamesController cont, final SpreadsheetTable focusOwner) {
        this.controller = cont;
        this.pf1 = ParentFrame1;
        this.pf2 = ParentFrame2;

        grid = new GridLayout(2, 1);

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("OtherPlayerTurn");

        ReceivePlayButtonOther_Turn = new JButton("Send Play");

        ReceivePlayButtonOther_Turn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                controller.startTCPServer();
                controller.updateCells(focusOwner.getSpreadsheet());
                if (pf1 != null) {
                    pf1.setMyturn(true);
                } else {
                    pf2.setMyturn(true);
                }
                dispose();
            }
        }
        );

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("GameRoom");

        centralPanel.setLayout(grid);

        upPanel.add(info, BorderLayout.CENTER);

        downPanel.add(ReceivePlayButtonOther_Turn);

        centralPanel.add(upPanel);
        centralPanel.add(downPanel);

        add(centralPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
