/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui.findworkbooks;

import csheets.ext.ipc.impl_02.SearchLocalWorkbooksThread;
import static csheets.ext.ipc.impl_realtimeworkbooksearch.AddFolderPanel.RTController;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Paulo
 */
public class SearchLocalWorkbooksMenu extends JFrame {

	private int numberOfFolders = 0;
	private static final int widthFrame = 500;
	private static final int heightFrame = 100;
	private final GridLayout grid;
	private final JPanel centralPanel, upPanel, downPanel;
	private final JLabel info;
	private final JButton IniciateLocalWorkbooksSearchButton, AdvancedWorkbookResearchButton, RealtimeListWorkbooksButton;
	private final Observer sideBar;
	UIController uiController;

	public SearchLocalWorkbooksMenu(Observer sideBar) {
		super("Search Local Workbooks");
		this.sideBar = sideBar;
		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		IniciateLocalWorkbooksSearchButton = new JButton("Find Workbooks");
		AdvancedWorkbookResearchButton = new JButton("Advanced Workbook Research ");
		RealtimeListWorkbooksButton = new JButton("Realtime List of Workbooks");

		IniciateLocalWorkbooksSearchButton.
			addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent ae) {
					SearchLocalWorkbooksThread search_thread = new SearchLocalWorkbooksThread();
					search_thread.
					addObserver(SearchLocalWorkbooksMenu.this.sideBar);
					Thread t = new Thread(search_thread);
					t.start();
					dispose();
				}
			}
			);

		AdvancedWorkbookResearchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				SearchLocalWorkbooksThread search_thread = new SearchLocalWorkbooksThread();
				search_thread.
					addObserver(SearchLocalWorkbooksMenu.this.sideBar);
				Thread t = new Thread(search_thread);
				t.start();
				dispose();
			}
		}
		);
		RealtimeListWorkbooksButton.setEnabled(true);

		RealtimeListWorkbooksButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {

				List<File> finalFileList = new ArrayList<>();
				finalFileList = RTController.getFoldersToSearch();

				numberOfFolders = finalFileList.size();
				if (finalFileList.isEmpty()) {
					JOptionPane.
						showMessageDialog(centralPanel, "Please add a new folder for the real time search");
					return;
				}

				for (File fileToSearch : finalFileList) {
					SearchLocalWorkbooksThread search_thread = new SearchLocalWorkbooksThread(fileToSearch);

					search_thread.
						addObserver(SearchLocalWorkbooksMenu.this.sideBar);
					Thread t = new Thread(search_thread);
					t.start();
				}
				finalFileList.clear();

			}

			//dispose();
		}
		);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(IniciateLocalWorkbooksSearchButton);
		downPanel.add(AdvancedWorkbookResearchButton);
		downPanel.add(RealtimeListWorkbooksButton);

		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		pack();
		setResizable(false);

		setLocationRelativeTo(null);
		setVisible(true);

	}
}
