package csheets.ext.ipc.ui.findworkbooks;

import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 * A controller for updating the user-specified comment of a cell.
 *
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
public class SearchWorkbooksPanelController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new comment controller.
	 *
	 * @param uiController the user interface controller
	 */
	public SearchWorkbooksPanelController(UIController uiController) {
		this.uiController = uiController;
	}

	public boolean openWorkbook(File f) {
		return this.uiController.openWorkbook(f);

	}
}
