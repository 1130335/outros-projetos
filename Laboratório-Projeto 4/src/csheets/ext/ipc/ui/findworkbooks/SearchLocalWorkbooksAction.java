/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui.findworkbooks;

import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.util.Observer;

/**
 *
 * @author Paulo
 */
public class SearchLocalWorkbooksAction extends FocusOwnerAction {

	private static final String TITLE = "Search Local Workbooks";

	private final Observer sidebar;

	/**
	 * Creates a new action.
	 *
	 * @param sidebar
	 */
	public SearchLocalWorkbooksAction(Observer sidebar) {
		this.sidebar = sidebar;
	}

	@Override
	protected String getName() {
		return TITLE;
	}

	@Override
	protected void defineProperties() {
	}

	/**
	 * @param event the event that was fired
	 */
	@Override
	public void actionPerformed(ActionEvent event) {

		new SearchLocalWorkbooksMenu(sidebar);

	}

}
