/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_07.GamesController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CreateGameRoomUI extends JFrame {

    private String game;
    private GamesController controller;

    private static final int widthFrame = 300;
    private static final int heightFrame = 100;

    private GridLayout grid;
    private JPanel centralPanel, upPanel, downPanel;
    private JLabel info;
    private JButton ConnectPlayerButton, CloseGameRoomButton;

    private boolean playing = true;
    private boolean my_turn = true;
    private boolean buttonNotPressed = true;


    public CreateGameRoomUI(String game, final SpreadsheetTable focusOwner) {
        super("GameRoom");
        this.game = game;
        this.controller = new GamesController();
        
        this.controller.setGame(this.game);

        grid = new GridLayout(2, 1);

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("GameRoom");

        ConnectPlayerButton = new JButton("Connect Players");
        CloseGameRoomButton = new JButton("Quit Game");

        if (game.equals("BattleShip")) {
            ConnectPlayerButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    controller.startUDPServer();
                    try {
                        playBattleShip(focusOwner);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(CreateGameRoomUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dispose();
                }
            }
            );
        } else {
            ConnectPlayerButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    controller.startUDPServer();
                    try {
                        playDraughts_Checkers(focusOwner);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(CreateGameRoomUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dispose();
                }
            }
            );
        }

        CloseGameRoomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        }
        );

        centralPanel.setLayout(grid);

        upPanel.add(info, BorderLayout.CENTER);

        downPanel.add(ConnectPlayerButton);
        downPanel.add(CloseGameRoomButton);

        centralPanel.add(upPanel);
        centralPanel.add(downPanel);

        add(centralPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void playBattleShip(final SpreadsheetTable focusOwner) throws InterruptedException {
        while (this.playing) {
            if (this.my_turn) {

                PlayerTurnUI ui= new PlayerTurnUI(this,null,controller,focusOwner);
                
                while(buttonNotPressed){};

            } else {

                OtherPlayerTurn ui = new OtherPlayerTurn(this, null, controller, focusOwner);
                
                setButtonNotPressed(true);
            }
        }
        JOptionPane.showMessageDialog(null, "You won!!!");
        this.controller.removeGameFromList();
    }

    public void playDraughts_Checkers(final SpreadsheetTable focusOwner) throws InterruptedException {
        while (this.playing) {
            if (this.my_turn) {

                PlayerTurnUI ui = new PlayerTurnUI(this,null,controller,focusOwner);
                
                while(buttonNotPressed){};

            } else {

                OtherPlayerTurn ui = new OtherPlayerTurn(this, null, controller, focusOwner);
                
                setButtonNotPressed(true);
            }
        }
        JOptionPane.showMessageDialog(null, "You won!!!");
    }
    
    public void setMyturn(boolean f)
    {
        this.my_turn = f;
    }
    
    public void setPlaying(boolean f)
    {
        this.playing=f;
    }
    
    public void setButtonNotPressed(boolean f)
    {
        this.buttonNotPressed = f;
    }
}
