/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_01.AutomaticUpdateController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Rita
 */
public class CellSharingMenu extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 100;

	private final GridLayout grid;
	private final JPanel centralPanel, upPanel, downPanel;
	private final JLabel info;
	private final JButton IniciateSharingButton, UpdateSharingButton, SeveralSharesButton;

	public CellSharingMenu(final SpreadsheetTable focusOwner) {
		super("Cells Sharing");

		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		IniciateSharingButton = new JButton("Initiate Cell Sharing");
		UpdateSharingButton = new JButton("Automatic Sharing Update");
		SeveralSharesButton = new JButton("Several Shares");

		IniciateSharingButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				new MenuInitiate(focusOwner);
				dispose();
			}
		}
		);

		UpdateSharingButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				AutomaticUpdateController auc = new AutomaticUpdateController(focusOwner);
				new ChooseInstancesWindow(auc);
				dispose();
			}
		}
		);
		//Here should be added the other UC
		SeveralSharesButton.setEnabled(false);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(IniciateSharingButton);
		downPanel.add(UpdateSharingButton);
		

		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		setVisible(true);

	}

}
