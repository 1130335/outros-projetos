/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

public class PlayGamesMenu extends JMenu {

    private static final String NAME = "Games";

    /**
     * Creates a new simple menu. This constructor creates and adds the menu
     * options. In this simple example only one menu option is created. A menu
     * option is an action (in this case
     * {@link csheets.ext.ipc.ui.InitiateSharingAction})
     *
     * @param uiController the user interface controller
     */
    public PlayGamesMenu(UIController uiController) {
        super(NAME);
        setMnemonic(KeyEvent.VK_G);
        
        add(new PlayGamesAction());
    }
}
