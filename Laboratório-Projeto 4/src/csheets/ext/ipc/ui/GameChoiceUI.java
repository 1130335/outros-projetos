/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GameChoiceUI extends JFrame {

    private static final int widthFrame = 500;
    private static final int heightFrame = 100;

    private final GridLayout grid;
    private final JPanel centralPanel, upPanel, downPanel;
    private final JLabel info;
    private final JButton CreateGameRoomButton, SearchPlayersButton, SelectGameButton;
    private String game;
    

    public GameChoiceUI(final SpreadsheetTable focusOwner) {
        super("GameMenu");

        grid = new GridLayout(2, 1);

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("Choose one of the following options: ");

        CreateGameRoomButton = new JButton("Create GameRoom");
        SearchPlayersButton = new JButton("Search Players");
        SelectGameButton = new JButton("Select Game");

        CreateGameRoomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new CreateGameRoomUI(getGameSelected(),focusOwner);
                dispose();
            }
        }
        );

        SearchPlayersButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new SearchPlayersUI(focusOwner);
                dispose();
            }
        }
        );

        SelectGameButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String[] list = {"BattleShip","Draughts/Checkers"};
                JComboBox listGames = new JComboBox(list);
                JOptionPane.showConfirmDialog(null, listGames, "Select a Game", JOptionPane.OK_OPTION);
                setGameSelected((String) listGames.getSelectedItem());
            }
        }
        );

        centralPanel.setLayout(grid);

        upPanel.add(info, BorderLayout.CENTER);

        downPanel.add(CreateGameRoomButton);
        downPanel.add(SearchPlayersButton);
        downPanel.add(SelectGameButton);

        centralPanel.add(upPanel);
        centralPanel.add(downPanel);

        add(centralPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public String getGameSelected()
    {
        return this.game;
    }
    
    public void setGameSelected(String game)
    {
        this.game=game;
    }
}
