/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui.findnetworkworkbooks;

import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.util.Observer;

/**
 *
 * @author Paulo
 */
public class SearchNetworkWorkbooksAction extends FocusOwnerAction {

	private static final String TITLE = "Search The Local Network For Workbooks";

	private final Observer sidebar;

	/**
	 * Creates a new action.
	 *
	 */
	public SearchNetworkWorkbooksAction(Observer sidebar) {
		this.sidebar = sidebar;
	}

	@Override
	protected String getName() {
		return TITLE;
	}

	@Override
	protected void defineProperties() {
	}

	/**
	 * @param event the event that was fired
	 */
	@Override
	public void actionPerformed(ActionEvent event) {

		new SearchNetworkWorkbooksMenu(sidebar);

	}

}
