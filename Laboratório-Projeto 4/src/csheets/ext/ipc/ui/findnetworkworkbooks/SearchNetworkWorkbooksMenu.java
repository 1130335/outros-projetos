/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui.findnetworkworkbooks;

import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.protocols.NetworkFactory;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Paulo
 */
public class SearchNetworkWorkbooksMenu extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 100;

	private final GridLayout grid;
	private final JPanel centralPanel, upPanel, downPanel;
	private final JLabel info;
	private final JButton IniciateWorkbooksSearchButton, ListenForRequestsButton;
	private final Observer sideBar;

	public SearchNetworkWorkbooksMenu(Observer sideBar) {
		super("Search Local Workbooks");
		this.sideBar = sideBar;
		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		IniciateWorkbooksSearchButton = new JButton("Find Workbooks");
		ListenForRequestsButton = new JButton("Listen for Requests");

		IniciateWorkbooksSearchButton.
			addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent ae) {
					/**
					 * implementar um cliente udp que vai enviar o nome do
					 * workbook a pesquisar na rede local
					 */

					List<NetworkInstance> instances = new ArrayList<>();

					Thread t = new Thread(NetworkFactory.
						getUDPClientImpl_03(instances));
					t.start();
					try {
						t.join();
					} catch (InterruptedException ex) {
						Logger.getLogger(SearchNetworkWorkbooksMenu.class.
							getName()).
						log(Level.SEVERE, null, ex);
					}

				}
			}
			);

		ListenForRequestsButton.
			addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent ae) {
					//implementar um servidor udp à escuta de pedidos...
					Thread t = new Thread(NetworkFactory.getUDPServerImpl());
					try {
						t.join();
					} catch (InterruptedException ex) {
						Logger.getLogger(SearchNetworkWorkbooksMenu.class.
							getName()).
						log(Level.SEVERE, null, ex);
					}
				}
			}
			);

		/*Add to this buttons the action performed*/
		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(IniciateWorkbooksSearchButton);
		downPanel.add(ListenForRequestsButton);

		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		pack();
		setResizable(false);

		setLocationRelativeTo(null);
		setVisible(true);

	}
}
