package csheets.ext.ipc.ui.findnetworkworkbooks;

import csheets.ui.ctrl.UIController;
import java.io.File;

/**
 *
 *
 * @author Einar Pehrson
 */
public class SearchNetworkWorkbooksPanelController {

	/**
	 * The user interface controller
	 */
	private UIController uiController;

	/**
	 * Creates a new comment controller.
	 *
	 * @param uiController the user interface controller
	 */
	public SearchNetworkWorkbooksPanelController(UIController uiController) {
		this.uiController = uiController;
	}

	public boolean openWorkbook(File f) {
		return this.uiController.openWorkbook(f);

	}
}
