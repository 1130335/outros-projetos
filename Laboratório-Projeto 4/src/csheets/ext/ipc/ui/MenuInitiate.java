/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.impl_01.ReceiveCellsController;
import csheets.ext.ipc.impl_01.SendSelCellsController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Rita
 */
public class MenuInitiate extends JFrame {

	private static final int widthFrame = 300;
	private static final int heightFrame = 100;
	private static int option;

	private GridLayout grid;
	private JPanel centralPanel, upPanel, downPanel;
	private JLabel info;
	private JButton SendButton, ReceiveButton;

	public MenuInitiate(final SpreadsheetTable focusOwner) {
		super("Initiate Sharing");

		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		SendButton = new JButton("Send Cells");
		ReceiveButton = new JButton("Receive Cells");

		SendButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
                            new PedirPorta(focusOwner);
                            dispose();
			}
		}
		);

		ReceiveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
                            new ReceberPorta(focusOwner);
                             dispose();
			
                            
                            
			}
		}
		);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(SendButton);
		downPanel.add(ReceiveButton);

		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		setVisible(true);

	}

}
