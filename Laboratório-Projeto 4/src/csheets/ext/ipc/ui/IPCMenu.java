/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.ui.findnetworkworkbooks.SearchNetworkWorkbooksAction;
import csheets.ext.ipc.ui.findworkbooks.SearchLocalWorkbooksAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import java.util.Observer;
import javax.swing.JMenu;

/**
 *
 * @author Rui
 */
public class IPCMenu extends JMenu {

	private static final String NAME = "IPC";

	/**
	 * Creates a new simple menu. This constructor creates and adds the menu
	 * options. In this simple example only one menu option is created. A menu
	 * option is an action (in this case
	 * {@link csheets.ext.ipc.ui.InitiateSharingAction})
	 *
	 * @param uiController the user interface controller
	 */
	public IPCMenu(UIController uiController, Observer sidebar) {
		super(NAME);
		setMnemonic(KeyEvent.VK_I);

		// Adds IPC actions
		add(new InitiateSharingAction());
		add(new SearchOnAnotherInstance());
		add(new InitiateSendMessageAction());
		add(new SearchLocalWorkbooksAction(sidebar));
		add(new SearchNetworkWorkbooksAction(sidebar));

	}
}
