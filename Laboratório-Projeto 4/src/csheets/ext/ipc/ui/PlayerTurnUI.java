/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_07.GamesController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PlayerTurnUI extends JFrame {

    private static final int widthFrame = 300;
    private static final int heightFrame = 100;

    private GridLayout grid;
    private JPanel centralPanel, upPanel, downPanel;
    private JLabel info;

    private JPanel centralPanelMy_Turn, upPanelMy_Turn, downPanelMy_Turn;
    private JLabel infoMy_Turn;
    private JButton SendPlayButtonMy_Turn, QuitGameButtonMy_Turn;

    final GamesController controller;
    final CreateGameRoomUI pf1;
    final SearchPlayersUI pf2;

    public PlayerTurnUI(CreateGameRoomUI ParentFrame1, SearchPlayersUI ParentFrame2, GamesController cont, final SpreadsheetTable focusOwner) {

        this.controller = cont;
        this.pf1 = ParentFrame1;
        this.pf2 = ParentFrame2;

        grid = new GridLayout(2, 1);

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("PlayerTurn");

        SendPlayButtonMy_Turn = new JButton("Send Play");
        QuitGameButtonMy_Turn = new JButton("Quit Game");

        SendPlayButtonMy_Turn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.setCells(focusOwner);
                controller.sendCells();
                if (pf1 != null) {
                    pf1.setMyturn(false);
                    pf1.setButtonNotPressed(false);
                } else {
                    pf2.setMyturn(false);
                    pf2.setButtonNotPressed(false);
                }
                dispose();
            }
        }
        );

        QuitGameButtonMy_Turn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae
            ) {
                if (pf1 != null) {
                    pf1.setPlaying(false);
                    pf1.setButtonNotPressed(false);
                } else {
                    pf2.setPlaying(false);
                    pf2.setButtonNotPressed(false);
                }
                dispose();
            }
        }
        );

        centralPanelMy_Turn = new JPanel();
        upPanelMy_Turn = new JPanel();
        downPanelMy_Turn = new JPanel();

        infoMy_Turn = new JLabel("GameRoom");

        centralPanelMy_Turn.setLayout(grid);

        upPanelMy_Turn.add(infoMy_Turn, BorderLayout.CENTER);

        downPanelMy_Turn.add(SendPlayButtonMy_Turn);

        downPanelMy_Turn.add(QuitGameButtonMy_Turn);

        centralPanelMy_Turn.add(upPanelMy_Turn);

        centralPanelMy_Turn.add(downPanelMy_Turn);

        add(centralPanelMy_Turn);

        setResizable(false);
        setSize(widthFrame, heightFrame);

        setLocationRelativeTo(null);
        setVisible(true);
    }

}
