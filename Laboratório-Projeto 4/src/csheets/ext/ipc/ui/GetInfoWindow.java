/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_03.SearchOnAnotherInstanceController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Utilizador
 */
public class GetInfoWindow extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 150;
	private static int option;

	private GridLayout grid;
	private JPanel centralPanel, upPanel, downPanel;
	private JLabel info;
	private JButton ReceiveButton;
	private JTextField add;
	private JTextField ipA;
	private JLabel text;
	private JLabel text2;
	private SearchOnAnotherInstanceController sAIC;
	String address;
	String ip;
	int port;

	public GetInfoWindow() {
		sAIC = new SearchOnAnotherInstanceController();
	}

	public GetInfoWindow(final SpreadsheetTable focusOwner) {
		super("Initiate Sharing");
		port = 23455;

		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");
		text = new JLabel("Introduce an address:");
		text2 = new JLabel("Introduce an IP:");
		add = new JTextField();
		ipA = new JTextField();
		address = add.getText();
		ip = ipA.getText();

		ReceiveButton = new JButton("Receive Info");

		ReceiveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {

				address = add.getText();
				sAIC = new SearchOnAnotherInstanceController(address, ip, focusOwner);
				sAIC.getInfo();

				dispose();
			}
		}
		);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(ReceiveButton);
		centralPanel.add(text);
		centralPanel.add(add);
		centralPanel.add(text2);
		centralPanel.add(ipA);
		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		setVisible(true);

	}
}
