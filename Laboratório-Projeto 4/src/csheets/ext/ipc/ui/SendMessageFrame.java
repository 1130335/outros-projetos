/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.SendMessage.UI.SendMessageUI;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author João Cabral
 */
public class SendMessageFrame extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 500;

	private final GridLayout grid;
	private final JPanel centralPanel;

	public SendMessageFrame(final SpreadsheetTable focusOwner) {
		super("Send Message");

		grid = new GridLayout(1, 1);

		centralPanel = new JPanel();

		centralPanel.add(new SendMessageUI());

		centralPanel.setLayout(grid);
		centralPanel.setEnabled(true);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		setVisible(true);
		pack();

	}

}
