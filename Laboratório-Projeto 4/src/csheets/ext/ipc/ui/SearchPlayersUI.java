/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.impl_07.GamesController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SearchPlayersUI extends JFrame {

    private GamesController controller;

    private static final int widthFrame = 300;
    private static final int heightFrame = 100;

    private GridLayout grid;
    private JPanel centralPanel, upPanel, downPanel;
    private JLabel info;
    private JButton SearchPlayerButton, CloseGameRoomButton;

    private boolean playing = true;
    private boolean my_turn = false;
    private boolean buttonNotPressed = true;

    public SearchPlayersUI(final SpreadsheetTable focusOwner) {
        super("GameRoom");
        this.controller = new GamesController();


        grid = new GridLayout(2, 1);

        centralPanel = new JPanel();
        upPanel = new JPanel();
        downPanel = new JPanel();

        info = new JLabel("Search Player");

        SearchPlayerButton = new JButton("Search Players");
        CloseGameRoomButton = new JButton("Quit Game");

        SearchPlayerButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.searchListOfPlayers();
                List<NetworkInstance> netList = controller.getListOfPlayers();

                if (!netList.isEmpty()) {
                    //String[] list = controller.getGameList(); ----------------> To be Fixed
                    String[] list = controller.listToStringArray();
                    JComboBox listGames = new JComboBox(list);
                    JOptionPane.showConfirmDialog(null, listGames, "Select a Player", JOptionPane.OK_CANCEL_OPTION);
                    controller.setInstance(listGames.getSelectedIndex());
                    try {
                        playBattleShip(focusOwner);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SearchPlayersUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(SearchPlayersUI.this, "No instances found!", getTitle(), JOptionPane.ERROR_MESSAGE);
                }
                dispose();
            }
        }
        );

        CloseGameRoomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        }
        );

        centralPanel.setLayout(grid);

        upPanel.add(info, BorderLayout.CENTER);

        downPanel.add(SearchPlayerButton);
        downPanel.add(CloseGameRoomButton);

        centralPanel.add(upPanel);
        centralPanel.add(downPanel);

        add(centralPanel);
        setResizable(false);
        setSize(widthFrame, heightFrame);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void playBattleShip(final SpreadsheetTable focusOwner) throws InterruptedException {
        while (this.playing) {
            if (this.my_turn) {

                new PlayerTurnUI(null, this, controller, focusOwner);

                while (buttonNotPressed){};

            } else {

                new OtherPlayerTurn(null, this, controller, focusOwner);

                setButtonNotPressed(true);
            }
        }
        JOptionPane.showMessageDialog(null, "You won!!!");
    }

    public void playDraughts_Checkers(final SpreadsheetTable focusOwner) throws InterruptedException {
        while (this.playing) {
            if (this.my_turn) {

                new PlayerTurnUI(null, this, controller, focusOwner);

                while (buttonNotPressed) {
                };

            } else {

                new OtherPlayerTurn(null, this, controller, focusOwner);

                setButtonNotPressed(true);
            }
        }
        JOptionPane.showMessageDialog(null, "You won!!!");
    }

    public void setMyturn(boolean f) {
        this.my_turn = f;
    }

    public void setPlaying(boolean f) {
        this.playing = f;
    }

    public void setButtonNotPressed(boolean f) {
        this.buttonNotPressed = f;
    }

}
