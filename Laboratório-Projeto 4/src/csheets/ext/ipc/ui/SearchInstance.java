/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui;

import csheets.ext.ipc.impl_03.SearchOnAnotherInstanceController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Utilizador
 */
public class SearchInstance extends JFrame {

	private static final int widthFrame = 500;
	private static final int heightFrame = 100;

	private final GridLayout grid;
	private final JPanel centralPanel, upPanel, downPanel;
	private final JLabel info;
	private final JButton SearchInstance, OnLineInstance;
	private int port;

	public SearchInstance(final SpreadsheetTable focusOwner) {
		super("Search On Another Instance");

		grid = new GridLayout(2, 1);

		centralPanel = new JPanel();
		upPanel = new JPanel();
		downPanel = new JPanel();

		info = new JLabel("Choose one of the following options: ");

		SearchInstance = new JButton("Search Instance");

		OnLineInstance = new JButton("OnLine Instance");

		SearchInstance.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {

				GetInfoWindow infoWindow = new GetInfoWindow(focusOwner);
				infoWindow.setVisible(true);
				port = infoWindow.port;
				dispose();

			}
		}
		);

		OnLineInstance.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {

				SearchOnAnotherInstanceController ctr = new SearchOnAnotherInstanceController();
				try {
					ctr.startTCPServer("asf");
				} catch (IOException ex) {
					Logger.getLogger(SearchInstance.class.getName()).log(Level.SEVERE, null, ex);
				} catch (InterruptedException ex) {
					Logger.getLogger(SearchInstance.class.getName()).log(Level.SEVERE, null, ex);
				}
				dispose();

			}
		}
		);

		centralPanel.setLayout(grid);

		upPanel.add(info, BorderLayout.CENTER);

		downPanel.add(SearchInstance);
		downPanel.add(OnLineInstance);
		centralPanel.add(upPanel);
		centralPanel.add(downPanel);

		add(centralPanel);
		setResizable(false);
		setSize(widthFrame, heightFrame);
		setLocationRelativeTo(null);
		setVisible(true);

	}

}
