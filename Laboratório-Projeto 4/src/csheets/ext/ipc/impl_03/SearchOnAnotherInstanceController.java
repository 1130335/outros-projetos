/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_03;

import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.protocols.NetworkFactory;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Utilizador
 */
public class SearchOnAnotherInstanceController {

	private String address;
	private String ip;
	List<NetworkInstance> availableInstances;
	SpreadsheetTable f;

	public SearchOnAnotherInstanceController() {

		availableInstances = new ArrayList<>();
	}

	public SearchOnAnotherInstanceController(String address, String ip, SpreadsheetTable f) {
		this.address = address;
		this.ip = ip;
		availableInstances = new ArrayList<>();
		this.f = f;

	}

	public void getInfo() {
		try {
			Thread t = new Thread(NetworkFactory.getTCPClientImpl_03(address));
			t.start();

			t.join();
		} catch (InterruptedException ex) {
		}
	}

	/**
	 * Starts a new thread with a TCP Server implementation. Join done.
	 */
	public void startTCPServer(String add) throws IOException, InterruptedException {
		String s = "";
		Thread t;
		TCP_ServerImpl_03 tS;
		tS = NetworkFactory.getTCPServerImpl_03("CleanSheets");
		t = new Thread(tS);
		t.start();
		s = tS.sendInfo();
		System.out.println("Info:" + s);
		t.join();
	}
}
