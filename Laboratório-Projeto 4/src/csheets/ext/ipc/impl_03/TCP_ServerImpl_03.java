/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_03;

import csheets.core.Cell;
import csheets.ext.ipc.protocols.TCP_Connection;
import csheets.ext.sharingFiles.TcpServer;
import csheets.ui.sheet.SpreadsheetTable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Utilizador
 */
public class TCP_ServerImpl_03 extends TCP_Connection {

	String s = "";
	SpreadsheetTable f;
	Thread thread;
	ServerSocket serverSocket;

	/**
	 *
	 * @param f
	 */
	public TCP_ServerImpl_03(String address) {
		super(address, true);
		this.f = f;
		thread = new Thread();

	}

	public void setS(SpreadsheetTable f) {
		this.f = f;
	}

	public void start(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		thread.start();
	}

	public String sendInfo() {

		thread = new Thread(new Runnable() {
			@Override
			public void run() {

				while (true) {

					Socket socket = new Socket();
					try {
						socket = serverSocket.accept();

						int i = 0;
						while (f.getUIController().getActiveWorkbook().iterator().hasNext()) {
							s += "" + f.getUIController().getActiveWorkbook().getSpreadsheet(i).getTitle();

						}
						Cell n[][] = f.getUIController().getActiveSpreadsheet().getFirstCellsWithContent();

						for (int j = 0; j < n.length; j++) {
							for (int k = 0; k < n[0].length; k++) {
								s += n[j][k].getContent();
							}

						}
					} catch (IOException ex) {
						Logger.getLogger(TcpServer.class.getName()).log(Level.SEVERE, null, ex);
					}

				}
			}

		});
		return s;
	}

	void close() {
		thread.interrupt();
	}

	@Override
	public boolean sendData(List<Object> objList) {
		return false;
	}

	@Override
	public List<Object> receiveData() {
		return null;
	}

	@Override
	public void run() {
		if (initConnection()) {
			s = sendInfo();
			endConnection();
		}
	}
}
