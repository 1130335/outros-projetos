/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_03;

import csheets.ext.ipc.protocols.TCP_Connection;
import java.util.List;

/**
 *
 * @author Utilizador
 */
public class TCP_ClientImpl_03 extends TCP_Connection {

	public TCP_ClientImpl_03(String address) {
		super(address, false);

	}

	@Override
	public boolean sendData(List<Object> objToSend) {
		return true;

	}

	@Override
	public List<Object> receiveData() {
		//não é necessário implementar
		return null;
	}

	public String getInfo() {
		return null;
	}

	@Override
	public void run() {
		if (initConnection()) {
			System.out.println("info aqui");

			endConnection();
		}
	}
}
