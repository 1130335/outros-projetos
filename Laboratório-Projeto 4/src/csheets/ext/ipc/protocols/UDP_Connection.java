/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.protocols;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Properties;

/**
 * UDP_Connection is an abstract class that represents a network protocol (UDP)
 * To create an implementation to a specific functionality a new class must
 * extend this one.
 *
 * @author Rui
 */
public abstract class UDP_Connection implements NetworkConnection, Runnable {

	private static final String properties_location = ".\\src-resources\\csheets\\res\\ipc.props";
	protected static final String NETWORK_ID = "cleansheets";

	protected InetAddress IP;
	protected DatagramSocket sock;
	protected int port;
	protected int TIMEOUT_TIME;

	private boolean broadcast;
	private boolean is_server;
        private boolean load_port;

	public UDP_Connection(boolean broadcast, boolean is_server) {
		this.broadcast = broadcast;
		this.is_server = is_server;
                this.load_port=true;
	}
        public UDP_Connection(boolean broadcast, boolean is_server,int port) {
		this.broadcast = broadcast;
		this.is_server = is_server;
                this.port=port;
                this.load_port=false;
	}

	private UDP_Connection() {
	}

	/**
	 *
	 * @return
	 */
@Override
	public final boolean initConnection() {
		if (!loadProperties()) {
			return false;
		}
		try {
			if (is_server) {
				this.sock = new DatagramSocket(this.port);
			} else {
				this.sock = new DatagramSocket();
			}
			this.sock.setBroadcast(broadcast);
			this.sock.setSoTimeout(TIMEOUT_TIME);
			return true;
		} catch (SocketException ex) {
			return false;
		}
	}

	@Override
	public final void endConnection() {
		this.sock.close();
	}

	private boolean loadProperties() {
		try {
			Properties prop = new Properties();
			InputStream input = new FileInputStream(properties_location);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			if(load_port)this.port = Integer.parseInt(prop.getProperty("udp_port"));
			this.IP = InetAddress.getByName(prop.
				getProperty("ip_broadcast"));
			if (is_server) {
				this.TIMEOUT_TIME = Integer.
					parseInt(prop.getProperty("timeout_udp_server"));
			} else {
				this.TIMEOUT_TIME = Integer.
					parseInt(prop.getProperty("timeout_udp_client"));
			}
			return true;
		} catch (FileNotFoundException ex) {
			return false;
		} catch (IOException ex) {
			return false;
		} catch (NumberFormatException ex) {
			return false;
		}

	}
}
