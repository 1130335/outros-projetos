/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.protocols;

import csheets.core.Cell;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.impl_01.TCP_ClientImpl;
import csheets.ext.ipc.impl_01.TCP_ServerImpl;
import csheets.ext.ipc.impl_01.UDP_ClientImpl;
import csheets.ext.ipc.impl_01.UDP_ServerImpl;
import csheets.ext.ipc.impl_03.TCP_ClientImpl_03;
import csheets.ext.ipc.impl_03.TCP_ServerImpl_03;
import csheets.ext.ipc.impl_07.TCP_GameImpl;
import csheets.ext.ipc.ui.findnetworkworkbooks.UDP_ClientImpl_03;
import java.util.List;

/**
 * NetworkFactory is responsible for creating new connections objects. To create
 * a new connection the appropriate method must be called.
 *
 * @author Rui
 */
public class NetworkFactory {

	public static TCP_ClientImpl getTCPClientImpl(String address,
												  List<Cell> objToSend) {
		return new TCP_ClientImpl(address, objToSend);
	}
	public static TCP_ClientImpl getTCPClientImpl(String address,
												  List<Cell> objToSend,int port) {
		return new TCP_ClientImpl(address, objToSend,port);
	}

	public static TCP_ServerImpl getTCPServerImpl(String address,
												  List<SimplerCell> objToReceive) {
		return new TCP_ServerImpl(address, objToReceive);
	}
	public static TCP_ServerImpl getTCPServerImpl(String address,
												  List<SimplerCell> objToReceive,int port) {
		return new TCP_ServerImpl(address, objToReceive,port);
	}

	public static TCP_ClientImpl_03 getTCPClientImpl_03(String address) {
		return new TCP_ClientImpl_03(address);
	}

	public static TCP_ServerImpl_03 getTCPServerImpl_03(String address) {
		return new TCP_ServerImpl_03(address);
	}

	public static TCP_GameImpl getTCPServerGameImpl(String address,
													boolean flag,
													List<SimplerCell> objListRecebido,
													List<Cell> objListEnviar) {
		return new TCP_GameImpl(address, flag, objListRecebido, objListEnviar);
	}

	public static UDP_ClientImpl getUDPClientImpl(
		List<NetworkInstance> instances) {
		return new UDP_ClientImpl(instances);
	}
	public static UDP_ClientImpl getUDPClientImpl(
		List<NetworkInstance> instances,int port) {
		return new UDP_ClientImpl(instances,port);
	}

	public static UDP_ServerImpl getUDPServerImpl() {
		return new UDP_ServerImpl();
	}
	public static UDP_ServerImpl getUDPServerImpl(int port) {
		return new UDP_ServerImpl(port);
	}

	public static Runnable getUDPClientImpl_03(List<NetworkInstance> instances) {
		return new UDP_ClientImpl_03(instances);
	}

	
}
