/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.protocols;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * TCP_Connection is an abstract class that represents a network protocol (TCP)
 * To create an implementation to a specific functionality a new class must
 * extend this one.
 *
 * @author Rui
 */
public abstract class TCP_Connection implements NetworkConnection, Runnable {

	private static final String properties_location = ".\\src-resources\\csheets\\res\\ipc.props";
	protected int TIMEOUT_TIME;
	private int port;
	private InetAddress IP;
	protected Socket socket;
	protected ServerSocket server_socket;
	private String address;
	protected boolean is_server;
	protected boolean havePort;

	private TCP_Connection() {

	}

	public TCP_Connection(String address, boolean flag) {
		this.address = address;
		is_server = flag;
	}
	public TCP_Connection(String address, boolean flag,int port) {
		this.address = address;
		is_server = flag;
                this.port=port;
                this.havePort=true;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public final boolean initConnection() {
		if (!loadProperties()) {
			return false;
		}
		try {
			if (is_server) {
				this.server_socket = new ServerSocket(this.port);
                                this.server_socket.setSoTimeout(TIMEOUT_TIME);
			} else {
				this.IP = InetAddress.getByName(address);
				this.socket = new Socket(this.IP, this.port);
                                this.socket.setSoTimeout(TIMEOUT_TIME);
			}

			return true;
		} catch (UnknownHostException ex) {
                    System.out.println("false ex");
			return false;
		} catch (IOException ex) {
                    System.out.println("false ex1");
			return false;
		}
	}

	@Override
	public void endConnection() {
		try {
			if (is_server) {
				this.server_socket.close();
			} else {
				this.socket.close();
			}
		} catch (IOException ex) {
			return;
		}
	}

	private boolean loadProperties() {
		try {
			Properties prop = new Properties();
			InputStream input = new FileInputStream(properties_location);

			// load a properties file
			prop.load(input);

			if(!this.havePort){this.port = Integer.parseInt(prop.getProperty("tcp_port"));}
                        if(is_server)
                            this.TIMEOUT_TIME = Integer.parseInt(prop.getProperty("timeout_tcp_server"));
                        else
                            this.TIMEOUT_TIME = Integer.parseInt(prop.getProperty("timeout_tcp_client"));
                        
			return true;
		} catch (FileNotFoundException ex) {
                    System.out.println("file not found");
			return false;
		} catch (IOException ex) {
			return false;
		} catch (NumberFormatException ex) {
			return false;
		}

	}

}
