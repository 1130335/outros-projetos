/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.protocols;

import java.util.List;

/**
 * NetworkConnection is an abstract class that represents a network connection. 
 * Any class that represents a network connection protocol should implement this one.
 * @author Rui
 */
public interface NetworkConnection {
    
    public boolean initConnection();
    public void endConnection();
    public boolean sendData(List<Object> objList);
    public List<Object> receiveData();
}
