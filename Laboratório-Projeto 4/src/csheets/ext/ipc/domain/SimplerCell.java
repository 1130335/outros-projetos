/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.domain;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellListener;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.formula.Formula;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.graphics.Graphic;
import java.io.Serializable;
import java.util.SortedSet;

/**
 * An simpler implementation of a Cell with few attributes. Just have an address
 * and an content. Its purpose is a temporary use.
 *
 * @author Rui
 */
public class SimplerCell implements Cell, Serializable {

	/* Address of the cell. */
	private final Address address;

	/* Content of the cell */
	private String content;

	/**
	 *
	 * @param add Address
	 * @param cont Content
	 */
	public SimplerCell(Address add, String cont) {
		address = add;
		content = cont;
	}

	/**
	 * Creates a new SimplerCell from a regular Cell. It copies the address and
	 * the content from it.
	 *
	 * @param complexCell A normal Cell
	 */
	public SimplerCell(Cell complexCell) {
		this.address = complexCell.getAddress();
		this.content = complexCell.getContent();
	}

	@Override
	public Spreadsheet getSpreadsheet() {
		return null;
	}

	@Override
	public Address getAddress() {
		return address;
	}

	@Override
	public Value getValue() {
		return null;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public Formula getFormula() {
		return null;
	}

	@Override
	public void setContent(String content) throws FormulaCompilationException {
		this.content = content;
	}

	@Override
	public void clear() {
	}

	@Override
	public SortedSet<Cell> getPrecedents() {
		return null;
	}

	@Override
	public SortedSet<Cell> getDependents() {
		return null;
	}

	@Override
	public void copyFrom(Cell source) {
	}

	@Override
	public void moveFrom(Cell source) {
	}

	@Override
	public void addCellListener(CellListener listener) {
	}

	@Override
	public void removeCellListener(CellListener listener) {
	}

	@Override
	public CellListener[] getCellListeners() {
		return null;
	}

	@Override
	public int compareTo(Cell o) {
		return -1;
	}

	@Override
	public Cell getExtension(String name) {
		return null;
	}

	/**
	 * Equals method that checks to see if the content of a string and its
	 * address are the same to see if they are equal.
	 *
	 * @param other_cell the cell to compare
	 * @return true if they are equal
	 */
	@Override
	public boolean equals(Object other_cell) {
		SimplerCell oc = (SimplerCell) other_cell;
		return (oc.sameAddress(this.address) && oc.sameContent(this.content));
	}

	/**
	 * Method to verify if the address of this cell passed by parameter is the
	 * same as the one passed by paramether. Only implemented because of equals
	 * method.
	 *
	 * @param adr Address to compare to.
	 * @return true if they are equal.
	 */
	private boolean sameAddress(Address adr) {
		return this.address.equals(adr);
	}

	/**
	 * Method to verify if the content of this cell passed by parameter is the
	 * same as the one passed by paramether. Only implemented because of equals
	 * method.
	 *
	 * @param adr Address to compare to.
	 * @return true if they are equal.
	 */
	private boolean sameContent(String cont) {
		return this.content.equals(cont);
	}

	@Override
	public void createGraphic(String name, SortedSet<Cell> chosenCells) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Graphic getGraphic() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
