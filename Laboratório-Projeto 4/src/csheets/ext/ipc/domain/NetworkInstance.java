/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.domain;

//import javax.crypto.SecretKey;

/**
 * NetworkInstance represents a instance in the local network.
 *
 * @author Rui
 */
public class NetworkInstance {

    /**
     * IP address
     */
    private final String ipAddress;

    /**
     * Port
     */
    private final int port;

    /**
     * Secret key used for AES encryption needed to encrypt and decrypt the
     * information communicated.
     */
    //private final SecretKey aes_key;
    
    public NetworkInstance() {
        ipAddress = "";
        port = 0;
        //aes_key = null;
    }

    public NetworkInstance(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        //this.aes_key = sec_key;
    }

    /**
     * Returns IP address
     *
     * @return
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Returns the port
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    
    
    @Override
    public String toString() {
        return ipAddress + ":" + port;
    }
    
    @Override
    public boolean equals(Object obj){
        NetworkInstance net = (NetworkInstance) obj;
        return (net.getIpAddress().equals(this.ipAddress) && net.getPort()==this.port);
    }
}
