/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_02;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import javax.swing.JOptionPane;

/**
 *
 * @author Paulo
 */
public abstract class FileSearch extends Observable implements Runnable {

	private File fileFound;
	private File folderAdded;
	private boolean realtime;
	String[] extensionss = new String[]{".cls"};

	public FileSearch() {
		this.realtime = false;
	}

	public FileSearch(File folderToSearch) {
		this.folderAdded = folderToSearch;
		this.realtime = true;
	}

	public File getFileFound() {
		return this.fileFound;
	}

	public void changeMessage(File fileFound) {
		this.fileFound = fileFound;
		setChanged();
		notifyObservers(fileFound);
	}

	public void updateMessage(String updated) {
		setChanged();
		notifyObservers(updated);
	}

	public List<File> procura(String[] extensions) {

		if (realtime == false) {
			List<File> files_founded = new ArrayList<>();
			File[] roots = File.listRoots();
			FileFilterImpl fileFilter = new FileFilterImpl(extensions);
			for (File root : roots) {
				if (root.isDirectory()) {
					procuraRecursiva(root, files_founded, fileFilter);
				}
			}
			return files_founded;
		} else {

			System.out.println("Realtime true!");

			List<File> files_founded = new ArrayList<>();
			//	File[] roots = File.listRoots();
			File[] myRoots = folderAdded.listFiles();
			FileFilterImpl fileFilter = new FileFilterImpl(extensions);
			for (File root : myRoots) {
				if (root.isDirectory()) {
					procuraRecursiva(root, files_founded, fileFilter);
				}
			}
			System.out.println("Acabou a procura na pasta" + folderAdded.
				toString());
			Path folderToWatch = Paths.get(folderAdded.getPath());

			try {
				WatchService watcher = folderToWatch.getFileSystem().
					newWatchService();
				folderToWatch.
					register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
							 StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
				WatchKey watckKey = watcher.take();

				List<WatchEvent<?>> events = watckKey.pollEvents();

				for (WatchEvent event : events) {
					JOptionPane.
						showMessageDialog(null, "The folder located at " + folderAdded.
										  getPath() + "was modified!\n Updating the file's list!");
					updateMessage(folderAdded.getPath());

					procura(extensionss);

				}

//				SearchWorkbooksPanel panel = (SearchWorkbooksPanel) ExtensionManager.
//					getInstance().getExtension("Initiate Sharing").
//					getUIExtension(null).getSideBar();
//
//
//				if (panel != null) {
//					panel.updateWorkbookList();
//				}
				//updateMessage("List Changed");
			} catch (IOException | InterruptedException | HeadlessException e) {
			}
			return files_founded;
		}

	}

	/**
	 * Recursive method to find all the files_inside that match a fileFilter
	 *
	 * @param file file where the search begin
	 * @param files_founded files_founded files founded
	 * @param fileFilter filtro
	 */
	public void procuraRecursiva(File file, List<File> files_founded,
								 FileFilterImpl fileFilter) {

		File[] files_inside = file.listFiles();

		int j = 0;
		if (files_inside == null) {
			System.out.println("pasta estava vazia");
			return;
		}
		for (int i = 0; i < files_inside.length; i++) {

			File f = files_inside[i];
			if (f == null) {
				continue;
			}
			//System.out.println(f.getAbsolutePath());
			if (f.isDirectory()) {
				procuraRecursiva(f, files_founded, fileFilter);
			} else {
				if (fileFilter.accept(f)) {
					files_founded.add(f);
					System.out.println("Encontrei!");
					this.changeMessage(f);
				}
			}

		}

	}
}
