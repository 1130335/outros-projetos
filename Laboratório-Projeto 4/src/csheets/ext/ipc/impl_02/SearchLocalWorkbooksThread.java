/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_02;

import csheets.ext.ipc.ui.findworkbooks.SearchWorkbooksPanel;
import java.io.File;

/**
 *
 * @author Paulo
 */
public class SearchLocalWorkbooksThread extends FileSearch {

	File folderToSearch;
	SearchWorkbooksPanel swp = new SearchWorkbooksPanel();

	public SearchLocalWorkbooksThread() {
		super();
	}

	public SearchLocalWorkbooksThread(File folderToSearch) {
		super(folderToSearch);
		this.folderToSearch = folderToSearch;
	}

	@Override
	public void run() {
		String[] extensions = new String[]{".cls"};
		super.procura(extensions);
	}

}
