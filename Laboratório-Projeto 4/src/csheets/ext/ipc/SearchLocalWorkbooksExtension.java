/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc;

import csheets.ext.Extension;
import csheets.ext.ipc.ui.UIExtensionIPC;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Paulo
 */
public class SearchLocalWorkbooksExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Search Local Workbooks";

	public SearchLocalWorkbooksExtension() {
		super(NAME);
	}

	/**
	 * Returns the user interface extension of this extension (an instance of
	 * the class {@link  csheets.ext.ipc.ui.UIExtensionIPC}). <br/>
	 * In this extension example we are only extending the user interface.
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionIPC(this, uiController);
	}
}
