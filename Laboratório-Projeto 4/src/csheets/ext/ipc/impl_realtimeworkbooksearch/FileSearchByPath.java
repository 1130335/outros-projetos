/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_realtimeworkbooksearch;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

public abstract class FileSearchByPath extends Observable implements Runnable {

	private File fileFound;
	private File folderAdded;

	public FileSearchByPath(File folderToSearch) {
		this.folderAdded = folderToSearch;
	}

	public File getFileFound() {
		return this.fileFound;
	}

	public void changeMessage(File fileFound) {
		this.fileFound = fileFound;
		setChanged();
		notifyObservers(fileFound);
	}

	public List<File> searchForFiles(String[] extensions) {

		List<File> files_founded = new ArrayList<>();

//		FileFilterImpl fileFilter = new FileFilterImpl(extensions);
//		File[] roots = folderAdded.listFiles(fileFilter);
//
//		for (File root : roots) {
//			if (root.isDirectory()) {
//				recursiveSearch(root, files_founded, fileFilter);
//			}
//		}
		File[] roots = File.listRoots();
		Arrays.toString(roots);
		System.out.println("não parou");
		FileFilterImpl fileFilter = new FileFilterImpl(extensions);
		for (File root : roots) {
			if (root.isDirectory()) {
				recursiveSearch(root, files_founded, fileFilter);
			}
		}
		return files_founded;
	}

	/**
	 * Recursive method to find all the files_inside that match a fileFilter
	 *
	 * @param file file where the search begin
	 * @param files_founded files_founded files founded
	 * @param fileFilter filtro
	 */
	public void recursiveSearch(File file, List<File> files_founded,
								FileFilterImpl fileFilter) {

		File[] files_inside = file.listFiles();

		int j = 0;
		if (files_inside == null) {
			return;
		}
		for (int i = 0; i < files_inside.length; i++) {

			File f = files_inside[i];
			if (f == null) {
				continue;
			}
			//System.out.println(f.getAbsolutePath());
			if (f.isDirectory()) {
				recursiveSearch(f, files_founded, fileFilter);
			} else {
				if (fileFilter.accept(f)) {
					files_founded.add(f);

					this.changeMessage(f);
				}
			}

		}

	}
}
