/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_realtimeworkbooksearch;

import java.io.File;

public class SearchLocalWorkbooksThread extends FileSearchByPath {

	public SearchLocalWorkbooksThread(File folderToSearch) {
		super(folderToSearch);
	}

	@Override
	public void run() {
		String[] extensions = new String[]{".cls"};
		super.searchForFiles(extensions);
	}

}
