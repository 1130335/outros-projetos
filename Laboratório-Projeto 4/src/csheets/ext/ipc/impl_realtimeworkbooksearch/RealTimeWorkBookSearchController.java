/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_realtimeworkbooksearch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RICARDLEITE
 */
public class RealTimeWorkBookSearchController {

	List<File> foldersToSearch = new ArrayList<>();

	public RealTimeWorkBookSearchController() {
	}

	public List<File> getFoldersToSearch() {
		return this.foldersToSearch;
	}

	public void addFolderToList(File newFile) {
		this.foldersToSearch.add(newFile);
	}

}
