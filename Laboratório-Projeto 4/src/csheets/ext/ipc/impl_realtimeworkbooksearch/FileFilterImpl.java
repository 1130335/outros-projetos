/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_realtimeworkbooksearch;

import java.io.File;
import java.io.FileFilter;

public class FileFilterImpl implements FileFilter {

	private final String[] okFileExtensions;

	public FileFilterImpl(final String[] extensions) {
		this.okFileExtensions = extensions;
	}

	@Override
	public boolean accept(File file) {
		for (String extension : okFileExtensions) {
			if (file.getName().toLowerCase().endsWith(extension)) {
				return true;
			}
		}
		return false;
	}
}
