/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage.UI;

import csheets.ext.ipc.SendMessage.SendMessage_StreamMessage;

/**
 *
 * @author João Cabral
 */
interface IntSendMessageUI {

	/**
	 * Receive a SendMessage_StreamMessage.
	 *
	 * @param message the message to receive
	 */
	void receiveMessage(SendMessage_StreamMessage message);

}
