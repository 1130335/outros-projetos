/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage.UI;

import csheets.ext.ipc.SendMessage.SendMessageController;
import csheets.ext.ipc.SendMessage.SendMessage_MessageFactory;
import csheets.ext.ipc.SendMessage.SendMessage_StreamMessage;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author João Cabral
 */
public class SendMessageUI extends JPanel implements IntSendMessageUI {

	/**
	 * This JTextArea displays received/sent messages.
	 */
	private final JTextArea inbox;

	/**
	 * This JTextField keeps the name sent in the messages.
	 */
	private final JTextField name;

	/**
	 * Keeps the ip address to send messages.
	 */
	private final JTextField ip;

	/**
	 * The outbox to send a message.
	 */
	private final JTextField outbox;

	/**
	 * Send button.
	 */
	private final JButton send;

	/**
	 * Online, offline Buttons.
	 */
	private final JRadioButton online, offline;

	/**
	 * *
	 * Button group for the online and offline buttons.
	 */
	private final ButtonGroup radioGroup = new ButtonGroup();

	/**
	 * Date format to print date.
	 */
	private final SimpleDateFormat dateFormat;

	/**
	 * The SendMessageController.
	 */
	private SendMessageController controller;

	/**
	 * Instantiate a new SendMessageUI.
	 */
	public SendMessageUI() {
		name = new JTextField();
		inbox = new JTextArea();
		ip = new JTextField();
		send = new JButton("send");
		outbox = new JTextField();
		dateFormat = new SimpleDateFormat("hh:mm:ss");
		online = new JRadioButton("online", true);
		online.setSelected(true);
		offline = new JRadioButton("offline", false);
		radioGroup.add(online);
		radioGroup.add(offline);

		createGUI();
		try {
			controller = new SendMessageController(this);

		} catch (IOException e) {
			outbox.setEditable(true);
			outbox.setEnabled(true);
			name.setEditable(true);
			name.setEnabled(true);
			send.setEnabled(true);
			ip.setEnabled(true);
			ip.setEditable(true);
			online.setEnabled(true);
			offline.setEnabled(true);
                        
		}
	}

	/**
	 * Receive and display a SendMessage_StreamMessage message.
	 *
	 * @param message the message to receive
	 */
	public void receiveMessage(SendMessage_StreamMessage message) {
		synchronized (inbox) {
			if (message.getConversation() == null || message.getConversation().
				isEmpty() == true) {
				inbox.append("Name: " + message.getName() + " " + dateFormat.
					format(message.getDate()) + "\n");
				inbox.append(message.getInetAddressHostName() + "@" + message.
					getINetAddressString() + "\n");
				inbox.append(message.getMessage() + "\n\n");
			} else {
				conversationProcess(message);
			}
		}
	}

	public void conversationProcess(SendMessage_StreamMessage message) {
		if (controller.getStatusOrAdd(message.getConversation()) == -1) {
			int n = JOptionPane.
				showConfirmDialog(new csheets.ext.ipc.SendMessage.UI.SendMessageUI(),
								  "Would you like to join the conversation\"" + message.
								  getConversation() + "\"?",
								  "Join conversation",
								  JOptionPane.YES_NO_OPTION);
			if (n == 0) { //joined
				controller.changeStatus(message.getConversation(), true);
				String convName = message.getConversation();
				String userName = newUsername();
				if (userName != null) {
					if ((userName.length() <= 0)) {
						JOptionPane.showMessageDialog(null,
													  "A username must be inserted to proceed",
													  "Missing name error.",
													  JOptionPane.ERROR_MESSAGE);
					} else {
						// TBD ..
					}
				}
			}
		}
	}

	/**
	 * Setup and display all the components.
	 */
	private void createGUI() {
		JPanel tempPanel = new JPanel();
		tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.Y_AXIS));
		JLabel label = new JLabel("Name:");
		tempPanel.add(label, 0);
		tempPanel.add(name, 1);

		add(tempPanel, BorderLayout.PAGE_START);

		inbox.setLineWrap(true);
		inbox.setWrapStyleWord(true);
		inbox.setEditable(false);
		JScrollPane pane = new JScrollPane(inbox);

		TitledBorder border = BorderFactory.createTitledBorder("Inbox");
		border.setTitleJustification(TitledBorder.CENTER);
		pane.setBorder(border);
		add(pane, BorderLayout.CENTER);

		tempPanel = new JPanel(new GridLayout(6, 1, 3, 5));

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		JPanel panel6 = new JPanel();
		JPanel panel5 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.LINE_AXIS));
		panel4.setLayout(new BoxLayout(panel4, BoxLayout.LINE_AXIS));
		panel6.setLayout(new BoxLayout(panel6, BoxLayout.LINE_AXIS));
		panel5.setLayout(new BoxLayout(panel5, BoxLayout.LINE_AXIS));
		JLabel label_ip = new JLabel("IP:");
                
		//action listener for the Send Button
		send.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {

				if (ip.getText().length() == 0) {
					outbox.setText("You must set an IP!");
				} else if (name.getText().length() == 0) {
					outbox.setText("You must set a name!");
				} else {
					SendMessage_MessageFactory factory = new SendMessage_MessageFactory();
					try {
						SendMessage_StreamMessage message = factory.
							getSendMessage_StreamMessage(name.getText(), outbox.getText(), null, ip. getText());
                                                                                                            if(controller == null){
                                                                                                                JOptionPane.showMessageDialog(null, "Impossible to send message!", "Warning", JOptionPane.WARNING_MESSAGE );
                                                                                                                return;
                                                                                                            }
						controller.sendMessage(message);
						synchronized (inbox) {
							inbox.append("Name: " + message.getName() + " " + dateFormat.format(message.getDate()) + "\n");
							inbox.append(message.getMessage() + "\n\n");
						}
					} catch (IOException e) {
						outbox.setText("Error: " + e.getMessage());
					}
					outbox.setText(null);
				}
			}

		});

		panel1.add(label_ip, 0);
		panel1.add(ip, 1);
		panel2.add(online, 0);
		panel2.add(offline, 1);
		outbox.setText("Message Here");
		panel3.add(outbox);
		panel5.add(send);
		tempPanel.add(panel2);
		tempPanel.add(panel4);
		tempPanel.add(panel6);
		tempPanel.add(panel1);
		tempPanel.add(panel3);
		tempPanel.add(panel5);

		//add the outbox
		add(tempPanel, BorderLayout.PAGE_END);
	}

	public String newConversationName() {
		String convName = JOptionPane.
			showInputDialog(null, "Insert a conversation's name:\n", "Conversation's Name", JOptionPane.QUESTION_MESSAGE);

		return convName;
	}

	public String newUsername() {
		String Name = JOptionPane.
			showInputDialog(null, "Insert your name:\n", "User Name", JOptionPane.QUESTION_MESSAGE);

		return Name;
	}

	/**
	 * SendMessageUI tester.
	 *
	 * @param args the command line arguments.
	 */
	public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.add(new csheets.ext.ipc.SendMessage.UI.SendMessageUI(), 0);
		frame.pack();
		frame.setVisible(true);
	}
}
