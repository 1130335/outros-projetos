package csheets.ext.ipc.SendMessage.UI;

import csheets.ext.ipc.SendMessage.SendMessageExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author João Cabral
 */
public class SendMessageExtensionUI extends UIExtension {

	/**
	 * The icon to display with the extension's name
	 */
	private Icon icon;

	/**
	 * A panel in which the chat window is displayed
	 */
	private JPanel sideBar;

	/**
	 * Creates a new user interface extension.
	 *
	 * @param extension the extension for which components are provided.
	 * @param uiController the user interface controller.
	 */
	public SendMessageExtensionUI(SendMessageExtension extension,
								  UIController uiController) {
		super(extension, uiController);
	}

	/**
	 * Returns an icon to display with the extension's name.
	 *
	 * @return an icon text boxes
	 */
	@Override
	public Icon getIcon() {
		if (icon == null) {
		}
		return icon;
	}

	/**
	 * Returns a SendMessageUI
	 *
	 * @return a panel where a chat can take place.
	 */
	@Override
	public JComponent getSideBar() {
		if (sideBar == null) {
			sideBar = new SendMessageUI();
		}
		return sideBar;
	}

}
