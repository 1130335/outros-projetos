/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage;

import java.util.ArrayList;

/**
 *
 * @author João Cabral
 */
public class SendMessageConversations {

	private ArrayList<String> conversations;

	private ArrayList<Boolean> conversationsStatus;

	public SendMessageConversations() {
		conversations = new ArrayList<>();
		conversationsStatus = new ArrayList<>();
	}

	public void addConversation(String conv, Boolean status) {
		conversations.add(conv);
		conversationsStatus.add(status);
	}

	public ArrayList<String> getConversations() {
		return conversations;
	}

	public ArrayList<Boolean> getConversationsStatus() {
		return conversationsStatus;
	}

	/**
	 *
	 * @param conv
	 * @param status
	 * @return true if conversation exists and is changed, false if doesn't
	 * exist.
	 */
	public boolean changeStatus(String conv, Boolean status) {
		if (conversations.contains(conv) == true) {
			for (String conversation : conversations) {
				if (conv.equals(conversation)) {
					conversationsStatus.remove(conversations.
						indexOf(conversation));
					conversationsStatus.
						add(conversations.indexOf(conversation), status);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 *
	 * @param conv object of conversation
	 * @return -1 if not found, 0 for false, 1 for true
	 */
	public int getStatusOrAdd(String conv) {
		if (conversations.contains(conv) == true) {
			for (String conversation : conversations) {
				if (conv.equals(conversation)) {
					if (conversationsStatus.get(conversations.
						indexOf(conversation)) == true) {
						return 1;
					} else {
						return 0;
					}
				}
			}
		} else {
			addConversation(conv, false);
		}
		return -1;
	}

	/**
	 * Used to remove unstarted conversations
	 *
	 * @param conv conversation name
	 */
	public void removeConversation(String conv) {
		if (conversations.contains(conv) == true) {
			for (String conversation : conversations) {
				if (conv.equals(conversation)) {
					conversations.remove(conv);
					conversationsStatus.remove(conversations.
						indexOf(conversation));
				}
			}
		}
	}
}
