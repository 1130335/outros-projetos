/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;

/**
 *
 * @author João Cabral
 */
public class SendMessage_MessageFactory {

	/**
	 * The calendar used to get the time.
	 */
	private static Calendar cal = Calendar.getInstance();

	/**
	 * Instantiate a new SendMessage_Message.
	 *
	 * @param name the name of the user.
	 * @param message the message text.
	 * @return the SendMessage_Message instance.
	 */
	private SendMessage_Message getSendMessage_Message(String name, String message,
													String conversation) {
		return new SendMessage_Message(name, message, cal.getTime(), conversation);
	}

	/**
	 * Instantiate a new SendMessage_StreamMessage.
	 *
	 * @param name the name of the user.
	 * @param message the message text.
	 * @param conversation the conversation related
	 * @param ip the ip address of the receiver.
	 * @return the SendMessage_StreamMessage instance.
	 * @throws UnknownHostException if the ip address could not be resolved.
	 */
	public SendMessage_StreamMessage getSendMessage_StreamMessage(String name,
															   String message,
															   String conversation,
															   String ip) throws UnknownHostException {
		return new SendMessage_StreamMessage(getSendMessage_Message(name, message, conversation), InetAddress.
											 getByName(ip));
	}

	/**
	 * Instantiate a new SendMessage_StreamMessage.
	 *
	 * @param message the SendMessage_Message concrete component.
	 * @param ip the inet_address of the user.
	 * @return the SendMessage_StreamMessage instance.
	 */
	public SendMessage_StreamMessage getSendMessage_StreamMessage(
		SendMessage_Message message, InetAddress ip) {
		return new SendMessage_StreamMessage(message, ip);
	}
}
