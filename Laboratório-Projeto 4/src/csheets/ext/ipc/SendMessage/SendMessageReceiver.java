package csheets.ext.ipc.SendMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 *
 * @author João Cabral
 */
public class SendMessageReceiver implements Runnable {

	/**
	 * The listen socket.
	 */
	private final ServerSocket socket;
	/**
	 * The sheet talk engine used to pass incoming messages.
	 */
	private final SendMessageEngine engine;

	/**
	 * Instantiate a new SheetTalkMessage Receiver.
	 *
	 * @param engine The engine associated with this receiver.
	 * @param port The port used for listening.
	 * @throws IOException if any errors occur during listen socket bind.
	 */
	public SendMessageReceiver(SendMessageEngine engine, int port) throws IOException {
		this.engine = engine;
		socket = new ServerSocket(port);
		socket.setSoTimeout(500);
	}

	/**
	 * Listen for connections algorithm.
	 * <p/>
	 * This algorithm listens on a server port for connections. Accepts them and
	 * parses the embedded message to the SheetTalkEngine. The algorithm is
	 * meant to be used in a multi-threaded environment, and has timeout safe
	 * checking so that it does not get hanged in a dropped request forever.
	 * <p/>
	 * Our messages are received over TCP and de-serialized a SheetTalkMessage
	 * object.
	 */
	public void listen() {

		Socket clientSocket = null;

		while (!Thread.interrupted()) {
			try {
				clientSocket = socket.accept();
				clientSocket.setSoTimeout(2000);

				ObjectInputStream stream = new ObjectInputStream(clientSocket.
					getInputStream());
				SendMessage_Message streamMessage = (SendMessage_Message) stream.
					readObject();

				SendMessage_MessageFactory factory = new SendMessage_MessageFactory();
				SendMessage_StreamMessage message = factory.
					getSendMessage_StreamMessage(streamMessage, clientSocket.
												 getInetAddress());

				engine.passMessageToController(message);
			} catch (SocketTimeoutException ignored) {
			} catch (IOException | ClassNotFoundException e) {
			} finally {
				if (clientSocket != null) {
					try {
						clientSocket.close();
					} catch (IOException e) {
					}
				}
			}
		}

		try {
			socket.close();
		} catch (IOException e) {
		}

	}

	/**
	 * Runnable override for multi-threading.
	 */
	@Override
	public void run() {
		listen();
	}
}
