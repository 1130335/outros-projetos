package csheets.ext.ipc.SendMessage;

import java.net.InetAddress;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author João Cabral
 */
public class SendMessage_StreamMessage {

	/**
	 * The SendMessageMessage being decorated.
	 */
	private SendMessage_Message SendMessage_Message;
	/**
	 * The inet adress of the user.
	 */
	private InetAddress inetAddress;

	/**
	 * Instantiate a new SendMessage_StreamMessage.
	 *
	 * @param message the SendMessage_Message object.
	 * @param inet_address The InetAddress of the target.
	 */
	public SendMessage_StreamMessage(SendMessage_Message message,
									 InetAddress inet_address) {
		this.SendMessage_Message = message;
		this.inetAddress = inet_address;
	}

	/**
	 * Get the message.
	 *
	 * @return the message.
	 */
	public SendMessage_Message getSheetTalkMessage() {
		return SendMessage_Message;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name.
	 */
	public String getName() {
		return SendMessage_Message.getName();
	}

	/**
	 * Gets the message
	 *
	 * @return the message.
	 */
	public String getMessage() {
		return SendMessage_Message.getMessage();
	}

	/**
	 * gets the message date stamp.
	 *
	 * @return the date.
	 */
	public Date getDate() {
		return SendMessage_Message.getDate();
	}

	/**
	 * Gets the conversation name.
	 *
	 * @return the conversation name.
	 */
	public String getConversation() {
		return SendMessage_Message.getConversation();
	}

	/**
	 * Get the senders IP address in a string form/
	 *
	 * @return sender address
	 */
	public String getINetAddressString() {
		return inetAddress.getHostAddress();
	}

	/**
	 * Get the senders localhost.
	 *
	 * @return localhost
	 */
	public String getInetAddressHostName() {
		return inetAddress.getHostName();
	}

	/**
	 * Gets the InetAddress object.
	 *
	 * @return the InetAddress
	 */
	public InetAddress getInetAddress() {
		return inetAddress;
	}

	/**
	 * Compares this SendMessage_StreamMessage message with another object for
	 * equality.
	 *
	 * @param o the object being compared.
	 * @return if the objects are equal.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SendMessage_StreamMessage)) {
			return false;
		}

		SendMessage_StreamMessage that = (SendMessage_StreamMessage) o;

		if (!inetAddress.equals(that.inetAddress)) {
			return false;
		}
		if (!SendMessage_Message.equals(that.SendMessage_Message)) {
			return false;
		}

		return true;
	}

	/**
	 * Returns this SendMessage_StreamMessage hash code representation.
	 *
	 * @return this SendMessage_StreamMessage hash code representation.
	 */
	@Override
	public int hashCode() {
		int result = SendMessage_Message.hashCode();
		result = 31 * result + inetAddress.hashCode();
		return result;
	}

	/**
	 * Converts this object into a string.
	 *
	 * @return string representing the object.
	 */
	@Override
	public String toString() {
		return "SheetTalkStreamMessage{"
			+ "message=" + SendMessage_Message
			+ ", inetAddress=" + inetAddress
			+ '}';
	}
}
