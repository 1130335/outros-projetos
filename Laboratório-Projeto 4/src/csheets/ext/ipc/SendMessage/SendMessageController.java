/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage;

import csheets.ext.ipc.SendMessage.SendMessageState.State;
import csheets.ext.ipc.SendMessage.UI.SendMessageUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author João Cabral
 */
public class SendMessageController {

	/**
	 * The User Interface this controller serves.
	 */
	private SendMessageUI ui;

	/**
	 * The engine of this controller.
	 */
	private SendMessageEngine engine;

	/**
	 * checks if this controller has been closed.
	 */
	private boolean closed;

	/**
	 * sets the state of the chat.
	 */
	private SendMessageState state;

	/**
	 * list of available conversations
	 */
	private SendMessageConversations conversations;

	/**
	 * Instantiate a new SendMessageController.
	 *
	 * @param ui the user interface to notify.
	 * @throws IOException if the SendMessageEngine could not bind to service
	 * port.
	 */
	public SendMessageController(SendMessageUI ui) throws IOException {
		state = SendMessageState.getInstance();
		state.setOnline();
		this.ui = ui;
		engine = new SendMessageEngine(this);
		conversations = new SendMessageConversations();
	}

	/**
	 * Send a SendMessageSender over the network.
	 *
	 * @param message the message to be sent.
	 * @throws IOException if an IO operation fails.
	 */
	public void sendMessage(SendMessage_StreamMessage message) throws IOException {
		engine.sendMessage(message);
	}

	/**
	 * Parse message to the UI.
	 * <p/>
	 * This method is package restricted so that it is not used by mistake
	 * outside.
	 *
	 * @param message the message to be parsed.
	 */
	void parseMessage(SendMessage_StreamMessage message) {
		if (state.getState() == State.online) {
			ui.receiveMessage(message);
		} else {
			SendMessage_MessageFactory factory = new SendMessage_MessageFactory();
			try {
				SendMessage_StreamMessage Amessage = factory.
					getSendMessage_StreamMessage("Automatic Message", "User is offline", null, message.
												 getINetAddressString());
				sendMessage(Amessage);
			} catch (IOException ex) {
				Logger.getLogger(SendMessageController.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		}

	}

	/**
	 * Close the engine and clean up all resources associated with it.
	 */
	public void close() {
		if (!closed) {
			state.setOffline();
			engine.close();
			closed = true;
		}
	}

	public void setOnline() {
		state.setOnline();
		engine.connect();

	}

	public void setOffline() {

		state.setOffline();
		engine.interrupt();

	}

	public State getState() {
		return state.getState();
	}

	public void addConversation(String conv, Boolean status) {
		conversations.addConversation(conv, status);
	}

	/**
	 *
	 * @param conv
	 * @param status
	 * @return true if found and changed, false for not found
	 */
	public boolean changeStatus(String conv, Boolean status) {
		return conversations.changeStatus(conv, status);
	}

	/**
	 *
	 * @param conv
	 * @return -1 if not found, 0 for false, 1 for true
	 */
	public int getStatusOrAdd(String conv) {
		return conversations.getStatusOrAdd(conv);
	}

	public void removeConversation(String conv) {
		conversations.removeConversation(conv);
	}

}
