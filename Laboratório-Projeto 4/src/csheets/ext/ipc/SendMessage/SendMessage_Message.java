/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage;

import java.util.Date;

/**
 *
 * @author João Cabral
 */
public class SendMessage_Message {

	/**
	 * The name of user who sent the message.
	 */
	private final String name;

	/**
	 * The message contents.
	 */
	private final String message;

	/**
	 * The time the message was sent.
	 */
	private final Date date;

	/**
	 * The name of the conversation, if any.
	 */
	private final String conversation;

	/**
	 * Builds a SendMessage_Message.
	 *
	 * @param name the name of the user.
	 * @param message the message text.
	 * @param date the date.
	 */
	public SendMessage_Message(String name, String message, Date date) {
		this.name = name;
		this.message = message;
		this.date = date;
		this.conversation = null;
	}

	/**
	 * Builds a SendMessage_Message.
	 *
	 * @param name the name of the sender.
	 * @param message the message text.
	 * @param date the date.
	 * @param conversation the conversation to be destined.
	 */
	public SendMessage_Message(String name, String message, Date date,
							   String conversation) {
		this.name = name;
		this.message = message;
		this.date = date;
		this.conversation = conversation;
	}

	/**
	 * Get this senders Name.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets this Message message.
	 *
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the date this message was received.
	 *
	 * @return the date.
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Returns the conversation's name.
	 *
	 * @return the conversation.
	 */
	public String getConversation() {
		return conversation;
	}

	/**
	 * Checks if this instance is equal to another one.
	 *
	 * Compares date, inet_address, message, and name.
	 *
	 * @param o the object
	 * @return boolean
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SendMessage_Message)) {
			return false;
		}

		SendMessage_Message that = (SendMessage_Message) o;

		if (!date.equals(that.date)) {
			return false;
		}
		if (!message.equals(that.message)) {
			return false;
		}
		if (!name.equals(that.name)) {
			return false;
		}
		if (!conversation.equals(that.conversation)) {
			return false;
		}

		return true;
	}

	/**
	 * Returns a hash representation of this object.
	 *
	 * @return hash
	 */
	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + message.hashCode();
		result = 31 * result + date.hashCode();
		return result;
	}

	/**
	 * Returns a string representation of this object.
	 *
	 * @return a standard message
	 */
	@Override
	public String toString() {
		if (conversation == null) {
			return "SendMessage_Message{"
				+ "name = '" + name + '\''
				+ ", message = '" + message + '\''
				+ ", date = " + date
				+ '}';
		} else {
			return "SendMessage_Message{"
				+ "name = '" + name + '\''
				+ ", message = '" + message + '\''
				+ ", date =" + date
				+ ", conversation =" + conversation + '\''
				+ '}';
		}
	}
}
