/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.SendMessage;

/**
 *
 * @author João Cabral
 */
public class SendMessageState {

	public enum State {

		online, offline, no_Chat;
	}

	private static SendMessageState instance = new SendMessageState(State.no_Chat);
	State state;

	public SendMessageState(State state) {
		this.state = state;
	}

	public SendMessageState() {
		state = State.no_Chat;
	}

	public void setOnline() {
		state = State.online;
	}

	public void setOffline() {
		state = State.offline;
	}

	public State getState() {
		return state;
	}

	public static SendMessageState getInstance() {
		return instance;
	}

}
