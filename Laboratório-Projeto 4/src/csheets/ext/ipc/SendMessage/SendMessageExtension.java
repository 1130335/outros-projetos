package csheets.ext.ipc.SendMessage;

import csheets.ext.Extension;
import csheets.ext.ipc.SendMessage.UI.SendMessageExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author João Cabral
 */
public class SendMessageExtension extends Extension {

	/**
	 * The name of the extension
	 */
	public static final String NAME = "Send Message";

	/**
	 * Instantiates a new SendMessageExtension.
	 */
	public SendMessageExtension() {
		super(NAME);
	}

	/**
	 * Returns a user interface extension for dependency trees.
	 *
	 * @param uiController the user interface controller
	 * @return a user interface extension for dependency trees
	 */
	@Override
	public UIExtension getUIExtension(UIController uiController) {
		if (this.uiExtension == null) {
			this.uiExtension = new SendMessageExtensionUI(this, uiController);
		}
		return this.uiExtension;
	}

	public String getBroadCastMessage() {
		return SendMessageState.getInstance().getState().toString();
	}
}
