package csheets.ext.ipc.SendMessage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author João Cabral
 */
public class SendMessageSender {

	/**
	 * The socket used to send messages.
	 */
	private final Socket socket;

	/**
	 * The message to be sent
	 */
	private final SendMessage_StreamMessage message;

	/**
	 * Instantiate a new SendMessageSender.
	 *
	 * @param message the message to be sent.
	 * @param port the port the message should be sent to.
	 * @throws IOException when any errors occur when binding to the socket.
	 */
	public SendMessageSender(SendMessage_StreamMessage message, int port) throws IOException {
		socket = new Socket(message.getInetAddress(), port);
		this.message = message;
	}

	/**
	 * Send the message to the respective address.
	 *
	 * @throws IOException if any IO errors occur.
	 */
	public void sendMessage() throws IOException {
		ObjectOutputStream stream = new ObjectOutputStream(socket.
			getOutputStream());
		stream.writeObject(message.getSheetTalkMessage());
		stream.flush();
		stream.close();
		socket.close();
	}

}
