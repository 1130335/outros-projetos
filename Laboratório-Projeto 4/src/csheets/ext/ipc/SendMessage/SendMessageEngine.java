package csheets.ext.ipc.SendMessage;

import java.io.IOException;

/**
 *
 * @author João Cabral
 */
public class SendMessageEngine {

	/**
	 * The server port to be used.
	 */
	private static final int SOCKET_PORT = 1598;
	/**
	 * The Thread responsible for receiving messages.
	 */
	private final Thread receiver;

	private volatile boolean running = true;

	/**
	 * The controller used to parse messages back to the interface.
	 */
	private final SendMessageController controller;

	/**
	 * Instantiate a new SendMessageEngine.
	 *
	 * @param controller the controller
	 * @throws IOException if SendMessageReceiver could not bind to the port.
	 */
	public SendMessageEngine(SendMessageController controller) throws IOException {
		this.controller = controller;
		receiver = new Thread(new SendMessageReceiver(this, SOCKET_PORT));
		//receiver.start();
	}

	/**
	 * Send a SendMessage_StreamMessage message.
	 *
	 * @param message the message to be send.
	 * @throws IOException if an IO error occurs.
	 */
	public void sendMessage(SendMessage_StreamMessage message) throws IOException {
		new SendMessageSender(message, SOCKET_PORT).sendMessage();
	}

	/**
	 * Pass a message to the controller
	 *
	 * @param message the message to be passed.
	 */
	public void passMessageToController(SendMessage_StreamMessage message) {
		controller.parseMessage(message);
	}

	/**
	 * Close this engine and clean up all resources associated with it.
	 */
	public void close() {
		receiver.interrupt();
		try {
			receiver.join();
		} catch (InterruptedException e) {
		}
	}

	public void interrupt() {
		running = false;
	}

	public void connect() {
		running = true;
	}

	protected boolean getRunning() {
		return this.running;
	}
}
