/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.networkexplorer.ui;

import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.util.Observer;

/**
 *
 * @author Paulo silva
 */
public class networkExplorerAction extends FocusOwnerAction{

    private static final String TITLE="Network Explorer";
    private final Observer sidebar;
    
    public networkExplorerAction(Observer sidebar){
        this.sidebar=sidebar;
        
    }
    
    @Override
    protected String getName() {
        return TITLE;
    }

    @Override
	protected void defineProperties() {
	}
        
    @Override
    public void actionPerformed(ActionEvent event) {
        new networkExplorerMenu(sidebar);
    }
    
}
