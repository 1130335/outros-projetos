/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.networkexplorer.ui;

import csheets.ext.Extension;
import csheets.ext.ipc.ui.IPCMenu;
import csheets.ext.ipc.ui.findworkbooks.SearchWorkbooksPanel;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.util.Observer;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author Paulo silva
 */
public class UIExtensionNetworkExplorer extends UIExtension {

    /**
     * The icon to display with the extension's name
     */
    private Icon icon;
    
    private IPCMenu menu;

    /**
     * A side bar that provides that display the cleansheets instances
     */
    private JComponent sidebar;

    public UIExtensionNetworkExplorer(Extension extension, UIController uiController) {
        super(extension, uiController);

    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns a cell decorator that visualizes the data added by the extension.
     *
     * @return a cell decorator, or null if the extension does not provide one
     */
    public CellDecorator getCellDecorator() {
        return null;
    }

    /**
     * Returns a table decorator that visualizes the data added by the
     * extension.
     *
     * @return a table decorator, or null if the extension does not provide one
     */
    public TableDecorator getTableDecorator() {
        return null;
    }

    /**
     * Returns a toolbar that gives access to extension-specific functionality.
     *
     * @return a JToolBar component, or null if the extension does not provide
     * one
     */
    public JToolBar getToolBar() {
        return null;
    }
    
    /**
	 * Returns an instance of a class that implements JMenu. In this simple case
	 * this class only supplies one menu option.
	 *
	 * @see ExampleMenu
	 * @return a JMenu component
	 */
	public JMenu getMenu() {
		return null;
	}

    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    @Override
    public JComponent getSideBar() {
        if (sidebar == null) {
            sidebar = new networkExplorerPanel(uiController);
        }
        return sidebar;
    }

}
