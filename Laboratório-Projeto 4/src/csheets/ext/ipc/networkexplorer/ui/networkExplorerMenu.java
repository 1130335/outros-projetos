/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.networkexplorer.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Paulo silva
 */
public class networkExplorerMenu extends JFrame {

    private static final int widthFrame = 300;
    private static final int hightFrame = 100;

    private final GridLayout gl;
    private final JPanel central, up, down;
    private final JButton findInstances;
    private final Observer sidebar;

    public networkExplorerMenu(Observer sidebar) {
        super("Cleansheets instances");
        this.sidebar = sidebar;
        gl = new GridLayout(1, 1);

        central = new JPanel();
        up = new JPanel();
        down = new JPanel();

        findInstances = new JButton("Find Instances");

        findInstances.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

            }

        });
        central.setLayout(gl);
        down.add(findInstances);

        central.add(down);
        add(central);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
