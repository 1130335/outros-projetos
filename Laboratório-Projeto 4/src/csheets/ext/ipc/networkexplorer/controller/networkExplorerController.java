/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.networkexplorer.controller;

import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;

/**
 *
 * @author Paulo silva
 */
public class networkExplorerController {
    
    /**
	 * The user interface controller
	 */
    private UIController uiController;

/**
 * 
 */
public networkExplorerController(){

}

/**
 * Creates a new comment controller.
 * @param uiController 
 */
public networkExplorerController(UIController uiController){
    this.uiController=uiController;
}

public Workbook activeWorkbook(){
    return this.uiController.getActiveWorkbook();
}

public Spreadsheet activeSpreadsheet(){
    return this.uiController.getActiveSpreadsheet();
}

}