/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.ext.ipc.protocols.UDP_Connection;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rui
 */
public class UDP_ServerImpl extends UDP_Connection {

	private boolean is_cleansheet;
        private int port;

	public UDP_ServerImpl(int port) {
		super(false, true);
                this.port=port;
	}
	public UDP_ServerImpl() {
		super(false, true);
                
	}

	@Override
	public boolean sendData(List<Object> objList) {
		DatagramPacket send = new DatagramPacket(NETWORK_ID.getBytes(), NETWORK_ID.getBytes().length, this.IP, this.port);
		try {
			this.sock.send(send);
			return true;
		} catch (IOException ex) {
			return false;
		}
	}

	@Override
	public List<Object> receiveData() {
		List<Object> obj_list = new ArrayList<>();
		try {
			int size = NETWORK_ID.getBytes().length;
			byte[] data = new byte[size];
			DatagramPacket reply = new DatagramPacket(data, size);
			this.sock.receive(reply);

			this.IP = reply.getAddress();
			System.out.println("IP: " + IP);
			this.port = reply.getPort();
			System.out.println("port: " + port);
			this.is_cleansheet = new String(data, 0, reply.getLength()).
				equals(NETWORK_ID);

			return obj_list;
		} catch (IOException ex) {
			return obj_list;
		}

	}

	@Override
	public void run() {
		if (initConnection()) {
			try {
				this.sock.setSoTimeout(TIMEOUT_TIME);
				receiveData();

				if (this.is_cleansheet) {
					sendData(null);
				}
				endConnection();
			} catch (SocketException ex) {
				endConnection();
			}
		}
	}

}
