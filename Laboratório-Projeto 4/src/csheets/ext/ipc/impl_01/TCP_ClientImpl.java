/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.core.Cell;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.TCP_Connection;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 *
 * @author Rui
 */
public class TCP_ClientImpl extends TCP_Connection {

	private List<Cell> objToSend;
        private int port;

	public TCP_ClientImpl(String address, List<Cell> objList) {
		super(address, false);
		objToSend = objList;
	}
	public TCP_ClientImpl(String address, List<Cell> objList,int port) {
		super(address, false);
		objToSend = objList;
                this.port=port;
	}

	@Override
	public boolean sendData(List<Object> objToSend) {
		ObjectOutputStream outputStream = null;
		try {

			outputStream = new ObjectOutputStream(this.socket.getOutputStream());
			outputStream.writeInt(objToSend.size());

			for (Object obj : objToSend)
                        {
                            SimplerCell tmp = new SimplerCell((Cell) obj);
                            outputStream.writeObject(tmp);
                        }
                        outputStream.close();
		} catch (IOException ex) {
			return false;
		} finally {
			try {
                            if(outputStream != null)
				outputStream.close();
				return true;
			} catch (IOException ex) {
				return true;
			}
		}
	}

	@Override
	public List<Object> receiveData() {
		//não é necessário implementar
		return null;
	}

	@Override
	public void run() {
		if(initConnection())
                {
                    //do stuff
                    sendData((List<Object>) (List<?>) objToSend);

                    endConnection();
                }
	}

}
