/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.protocols.UDP_Connection;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rui
 */
public class UDP_ClientImpl extends UDP_Connection {

	private List<NetworkInstance> foundInstances;

	public UDP_ClientImpl(List<NetworkInstance> instancesList) {
		super(true, false);
		foundInstances = instancesList;
	}
        public UDP_ClientImpl(List<NetworkInstance> instancesList,int port) {
		super(true, false);
		foundInstances = instancesList;
                this.port=port;
	}

	@Override
	public boolean sendData(List<Object> objList) {

		DatagramPacket send_datagram = new DatagramPacket(NETWORK_ID.getBytes(), NETWORK_ID.
														  getBytes().length, IP, this.port);
		try {
			this.sock.send(send_datagram);
			return true;
		} catch (IOException ex) {
			return false;
		}
	}

	@Override
	public List<Object> receiveData() {
		List<NetworkInstance> instancesList = new ArrayList<>();
		while (true) {
			byte data[] = new byte[NETWORK_ID.getBytes().length];
			DatagramPacket reply = new DatagramPacket(data, NETWORK_ID.
													  getBytes().length);
			try {
				this.sock.receive(reply);
				String id = new String(data, 0, data.length);
				if (id.equals(NETWORK_ID)) {
					NetworkInstance tmp = new NetworkInstance(reply.getAddress().
						getHostAddress(), reply.getPort());
					System.out.println(tmp.getIpAddress() + " : " + tmp.
						getPort());
					instancesList.add(tmp);
				}
			} catch (SocketTimeoutException ex) {
				break;
			} catch (IOException ex) {
				break;
			}
		}
		System.out.println("list size inside thread " + instancesList.size());
		//TODO
		return (List<Object>) (List<?>) instancesList;
	}

	@Override
	public void run() {
                    if (initConnection()) {
			try {
				this.sock.setBroadcast(true);
				this.sock.setSoTimeout(TIMEOUT_TIME);
				if (sendData(null)) {
					foundInstances.
						addAll((List<NetworkInstance>) (List<?>) receiveData());
					System.out.
						println("Size list inside thread 2 " + foundInstances.
							size());
				}
			} catch (SocketException ex) {
				endConnection();
			}
		}
	}
        

}
