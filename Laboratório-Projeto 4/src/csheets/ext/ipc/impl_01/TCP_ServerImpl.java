/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.TCP_Connection;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rui
 */
public class TCP_ServerImpl extends TCP_Connection {

	private List<SimplerCell> objToReceive;
        private int port ;

	public TCP_ServerImpl(String address, List<SimplerCell> objList) {
		super(address, true);

		objToReceive = objList;
	}
	public TCP_ServerImpl(String address, List<SimplerCell> objList,int port) {
		super(address, true);
		objToReceive = objList;
                this.port=port;
	}

	@Override
	public boolean sendData(List<Object> objToSend) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Object> receiveData() {

		ObjectInputStream inStream = null;
		List<SimplerCell> tmp = new ArrayList<>();
		try {
			Socket s = this.server_socket.accept();

			inStream = new ObjectInputStream(s.getInputStream());
			int num_obj = inStream.readInt();

			for (int i = 0; i < num_obj; i++) {
				tmp.add((SimplerCell) inStream.readObject());
			}

			//TODO
			inStream.close();
			return (List<Object>) (List<?>) tmp;
		} catch (ClassNotFoundException | IOException ex) {
			System.out.println("ex1 eyh");
			tmp.clear();
			return (List<Object>) (List<?>) tmp;

		} finally {
			try {
				if (inStream != null) {
					inStream.close();
				}
			} catch (IOException ex) {
				System.out.println("ex2");
				return (List<Object>) (List<?>) tmp;
			}
		}
	}

	@Override
	public void run() {
		if (initConnection()) {
			objToReceive.addAll((List<SimplerCell>) (List<?>) receiveData());
			endConnection();
		}
	}

}
