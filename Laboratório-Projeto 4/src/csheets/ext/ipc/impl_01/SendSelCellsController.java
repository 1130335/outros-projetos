/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.core.Cell;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.protocols.NetworkFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Rui
 */
public class SendSelCellsController {

	/* List of available instances found */
	private List<NetworkInstance> availableInstances;
	/* List of cells to send */
	private final List<Cell> selCells;

	private boolean bool_port;
	private int port;

	/**
	 * Constructor
	 *
	 * @param cellsToSend
	 */
	public SendSelCellsController(Cell[][] cellsToSend) {

		selCells = cellArrayToList(cellsToSend);
		bool_port = true;
	}

	public SendSelCellsController(Cell[][] cellsToSend, int port) {

		selCells = cellArrayToList(cellsToSend);
		bool_port = true;
		this.port = port;
	}

	/**
	 * A new thread is created: it is responsible for seeking the available
	 * instances (using a udp connection)
	 */
	public void searchAvailableInstances() {
		try {
			availableInstances = new ArrayList<>();
			Thread t;
			if (!bool_port) {
				t = new Thread(NetworkFactory.
					getUDPClientImpl(availableInstances));
			} else {
				t = new Thread(NetworkFactory.
					getUDPClientImpl(availableInstances, port));
			}
			t.start();
			t.join();
		} catch (InterruptedException ex) {
			availableInstances.clear();
		}
	}

	/**
	 * A new thread is created: it is responsible for sending a list of cells to
	 * the specific destination (using a tcp connection)
	 *
	 * @param destination
	 * @param cells
	 */
	public void sendCells(NetworkInstance destination) {
		try {
                    Thread t;
			if(!bool_port){ t = new Thread(NetworkFactory.getTCPClientImpl(destination.
				getIpAddress(), selCells));}
                        else{ t = new Thread(NetworkFactory.getTCPClientImpl(destination.
				getIpAddress(), selCells,port));
                            
                        }
                        
			t.start();
			t.join();
		} catch (InterruptedException ex) {
		}
	}

	/**
	 * Creates a String array from the the available instances list.
	 *
	 * @return
	 */
	public String[] listToStringArray() {
		String[] vec = new String[availableInstances.size()];

		for (int i = 0; i < availableInstances.size(); i++) {
			vec[i] = availableInstances.get(i).toString();
		}
		return vec;
	}

	/**
	 * Creates a List of cells from the bidimensional array passed by parameter.
	 *
	 * @param mat
	 * @return
	 */
	private List<Cell> cellArrayToList(Cell[][] mat) {
		List<Cell> tmp = new ArrayList<>();
		for (Cell[] vec : mat) {
			tmp.addAll(Arrays.asList(vec));
		}
		return tmp;
	}

	public List<NetworkInstance> getAvailableInstances() {
		return availableInstances;
	}

	public List<Cell> getSelCells() {
		return selCells;
	}
}
