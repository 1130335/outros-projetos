/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.NetworkFactory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rui
 */
public class ReceiveCellsController {

	/* List of the cells to receive. */
	private final List<SimplerCell> cellsReceived;
	private int port;

	/**
	 *
	 */
	public ReceiveCellsController() {
		cellsReceived = new ArrayList<>();
	}

	public ReceiveCellsController(int port) {
		cellsReceived = new ArrayList<>();
		this.port = port;
	}

	/**
	 * Starts a new thread with a UDP Server implementation. No join done.
	 */
	public void startUDPServer() {
		Thread t = new Thread(NetworkFactory.getUDPServerImpl(this.port));
		t.start();
	}

	/**
	 * Starts a new thread with a TCP Server implementation. Join done.
	 */
	public void startTCPServer() {
		try {
			Thread t = new Thread(NetworkFactory.
				getTCPServerImpl(null, cellsReceived, port));
			t.start();
			t.join();
		} catch (InterruptedException ex) {
			cellsReceived.clear();
		}
	}

	/**
	 * Returns the list of the received cells
	 *
	 * @return
	 */
	public List<SimplerCell> getCellsReceived() {
		return cellsReceived;
	}

	/**
	 * Update the list of received cells in the Spreadsheet
	 *
	 * @param sheet The spreadsheet to be updated.
	 */
	public void updateCells(Spreadsheet sheet) {
		for (Cell c : cellsReceived) {
			try {
				sheet.getCell(c.getAddress()).setContent(c.getContent());
			} catch (FormulaCompilationException ex) {
				break;
			}
		}
	}

	public void updateCells(Spreadsheet sheet, int port) {
		for (Cell c : cellsReceived) {
			try {
				sheet.getCell(c.getAddress()).setContent(c.getContent());
			} catch (FormulaCompilationException ex) {
				break;
			}
		}
	}
}
