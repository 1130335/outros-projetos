/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_01;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.NetworkFactory;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Rita
 */
public class AutomaticUpdateController {

	/* List of available instances of the cleansheets */
	private List<NetworkInstance> availableInstances;
	/* List of received sentCells */
	private List<SimplerCell> receivedCells;
	/* List of sentCells to send */
	private List<Cell> sentCells;
	/* FocusOwner to be used in the controller */
	private SpreadsheetTable myTable;

	/**
	 * Controller's constructor
	 */
	public AutomaticUpdateController() {

	}

	/**
	 * Controller's constructor with focusOwner
	 *
	 * @param focusOwner
	 */
	public AutomaticUpdateController(final SpreadsheetTable focusOwner) {
		this.myTable = focusOwner;
		startUDPServer();
		sentCells = new ArrayList<>();
		receivedCells = new ArrayList<>();
	}

	/**
	 * Same method used in the IPC 01.01 to search for available instances of
	 * the cleansheets
	 */
	public void searchAvailableInstances() {
		try {
			availableInstances = new ArrayList<>();
			Thread t = new Thread(NetworkFactory.
				getUDPClientImpl(availableInstances));
			t.start();
			t.join();
		} catch (InterruptedException ex) {
			availableInstances.clear();
		}
	}

	/**
	 * Return a list with available instances of the cleansheets
	 *
	 * @return available instances
	 */
	public List<NetworkInstance> getAvailableInstances() {
		return availableInstances;
	}

	/**
	 * Same method used in the IPC 01.01 to start the UDP Server
	 */
	public void startUDPServer() {
		Thread t = new Thread(NetworkFactory.getUDPServerImpl());
		t.start();
	}

	/**
	 * Transforms the list of available instances of the cleansheets into an
	 * array of String to be showned by the combo box
	 *
	 * @return String vector
	 */
	public String[] listToStringArray() {
		String[] vec = new String[availableInstances.size()];

		for (int i = 0; i < availableInstances.size(); i++) {
			vec[i] = availableInstances.get(i).toString();
		}
		return vec;
	}

	/**
	 * Same method used in the IPC 01.01 to start the TCP Server
	 */
	public void startTCPServer() {
		try {
			Thread t = new Thread(NetworkFactory.
				getTCPServerImpl(null, receivedCells));
			t.start();
			t.join();
		} catch (InterruptedException ex) {

		}
	}

	/**
	 * Selects all the cells in the active spreadsheet and creates a list with
	 * all the cells
	 *
	 * @return list with cells
	 */
	public List<Cell> getCells() {
		List<Cell> tmp = new ArrayList<>();
		this.myTable.selectAll();
		tmp = cellArrayToList(this.myTable.getSelectedCells());
		return tmp;
	}

	/**
	 * Creates a List of cells from the bidimensional array passed by parameter.
	 *
	 * @param mat
	 * @return tmp list with all selected cells
	 */
	private List<Cell> cellArrayToList(Cell[][] mat) {
		List<Cell> tmp = new ArrayList<>();
		for (Cell[] vec : mat) {
			tmp.addAll(Arrays.asList(vec));
		}
		return tmp;
	}

	/**
	 * @param destination - Instance of the cleansheets to which the update will
	 * be sent
	 * @param cells
	 */
	public void sendCells(NetworkInstance destination, List<Cell> cells) {
		try {
			Thread t = new Thread(NetworkFactory.getTCPClientImpl(destination.
				getIpAddress(), cells));
			t.start();
			t.join();
		} catch (InterruptedException ex) {
		}
	}

	/**
	 * Update the list of received cells in the Spreadsheet
	 *
	 * @param sheet The spreadsheet to be updated.
	 */
	public void updateCells(Spreadsheet sheet) {
		for (Cell c : receivedCells) {
			try {
				sheet.getCell(c.getAddress()).setContent(c.getContent());
			} catch (FormulaCompilationException ex) {
				break;
			}
		}
	}

	public List<SimplerCell> getReceivedCells() {
		return receivedCells;
	}

	public Spreadsheet getSpreadsheet() {
		return this.myTable.getSpreadsheet();
	}
}
