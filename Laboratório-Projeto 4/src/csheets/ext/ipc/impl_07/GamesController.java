/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_07;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.NetworkFactory;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GamesController {

    private List<SimplerCell> listCellRecebido;
    private List<Cell> listCellEnviar;
    private List<NetworkInstance> availableInstances;
    private String address;
    private List<String> game = new ArrayList<>();

    public GamesController() {
        this.listCellRecebido = new ArrayList<>();
        this.listCellEnviar = new ArrayList<>();
        this.address = "";
    }

    public void startUDPServer() {
        Thread t = new Thread(NetworkFactory.getUDPServerImpl());
        t.start();
    }
    
    public void searchListOfPlayers()
    {
        try {
            availableInstances = new ArrayList<>();
            Thread t = new Thread(NetworkFactory.getUDPClientImpl(availableInstances));
            t.start();
            t.join();
        } catch (InterruptedException ex) {
            availableInstances.clear();
        }
    }
    
    public List<NetworkInstance> getListOfPlayers() {
        return availableInstances;
    }

    public String[] listToStringArray()
    {
        String[] vec = new String[availableInstances.size()];
        
        for(int i=0; i < availableInstances.size(); i++)
            vec[i] = availableInstances.get(i).toString();    
        return vec;
    }
    
    public void setInstance(int index)
    {
        String[] list = listToStringArray();
        this.address=list[index];
    }
    
    //To be Fixed
    public String[] getGameList()
    {
        //String[] temp = listToStringArray();
        String[] list = new String[availableInstances.size()];
        
        /*for (int i = 0; i < availableInstances.size(); i++) {
            list[i] = temp[i] + " - " + game.get(i);
        }*/
        
        return list;
    }
    
    public void startTCPServer() {
        try {
            Thread t = new Thread(NetworkFactory.getTCPServerGameImpl(this.address, true, this.listCellRecebido, this.listCellEnviar));
            t.start();
            t.join();
        } catch (InterruptedException ex) {
            this.listCellRecebido.clear();
            this.listCellEnviar.clear();
        }
    }
    
    public void setCells(final SpreadsheetTable focusOwner)
    {
        focusOwner.selectAll();
        this.listCellEnviar = cellArrayToList(focusOwner.getSelectedCells());
    }
    
    public void sendCells()
    {
        try {
            Thread t = new Thread(NetworkFactory.getTCPServerGameImpl(this.address, false, this.listCellRecebido, this.listCellEnviar));
            t.start();
            t.join();
        } catch (InterruptedException ex) {
            
        }
    }
    
    private List<Cell> cellArrayToList(Cell[][] mat)
    {
        List<Cell> tmp = new ArrayList<>();
        for(Cell[] vec : mat)
            tmp.addAll(Arrays.asList(vec));
        return tmp;
    }
    
    public void updateCells(Spreadsheet sheet)
    {
        for(Cell c : listCellRecebido)
            try {
                sheet.getCell(c.getAddress()).setContent(c.getContent());
            } catch (FormulaCompilationException ex) {
                break;
            }
    }
    
    public void setGame(String name)
    {
        game.add(name);
    }
    
    public void removeGameFromList()
    {
        game.clear();
    }
    
}
