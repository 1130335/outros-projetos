/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_07;

import csheets.core.Cell;
import csheets.ext.ipc.domain.SimplerCell;
import csheets.ext.ipc.protocols.TCP_Connection;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TCP_GameImpl extends TCP_Connection {

    private List<SimplerCell> objListRecebido;
    private List<Cell> objListEnviar;

    public TCP_GameImpl(String address, boolean flag, List<SimplerCell> objListRecebido, List<Cell> objListEnviar) {
        super(address, flag);
        this.objListRecebido = objListRecebido;
        this.objListEnviar = objListEnviar;
    }

    @Override
    public boolean sendData(List<Object> objToSend) {
        ObjectOutputStream outputStream = null;
        try {

            outputStream = new ObjectOutputStream(this.socket.getOutputStream());
            outputStream.writeInt(objToSend.size());

            for (Object obj : objToSend) {
                SimplerCell tmp = new SimplerCell((Cell) obj);
                outputStream.writeObject(tmp);
            }
            outputStream.close();
        } catch (IOException ex) {
            return false;
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                return true;
            } catch (IOException ex) {
                return true;
            }
        }
    }

    @Override
    public List<Object> receiveData() {
        ObjectInputStream inStream = null;
        List<SimplerCell> tmp = new ArrayList<>();
        try {
            Socket s = this.server_socket.accept();

            inStream = new ObjectInputStream(s.getInputStream());
            int num_obj = inStream.readInt();

            for (int i = 0; i < num_obj; i++) {
                tmp.add((SimplerCell) inStream.readObject());
            }

            inStream.close();
            return (List<Object>) (List<?>) tmp;
        } catch (ClassNotFoundException | IOException ex) {
            tmp.clear();
            return (List<Object>) (List<?>) tmp;

        } finally {
            try {
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException ex) {
                return (List<Object>) (List<?>) tmp;
            }
        }
    }

    @Override
    public void run() {
        if (this.is_server) {
            if (initConnection()) {
                objListRecebido.addAll((List<SimplerCell>) (List<?>) receiveData());
                endConnection();
            }
        } else {
            if (initConnection()) {
                sendData((List<Object>) (List<?>) objListEnviar);
                endConnection();
            }
        }
    }

}
