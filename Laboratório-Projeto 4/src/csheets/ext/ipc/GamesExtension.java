/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc;

import csheets.ext.Extension;
import csheets.ext.ipc.ui.UIExtensionGames;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to initiate playing games between instances in the same local network.
 * An extension must extend the Extension abstract class.
 * The class that implements the Extension is the "bootstrap" of the extension.
 * @see Extension
 */
public class GamesExtension extends Extension{

    /** The name of the extension */
    public static final String NAME = "Games";
    
    
    public GamesExtension() {
        super(NAME);
    }
    
    /**
     * Returns the user interface extension of this extension (an instance of the class {@link  csheets.ext.ipc.ui.UIExtensionGames}). <br/>
     * @param uiController
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
            return new UIExtensionGames(this, uiController);
    }
    
}
