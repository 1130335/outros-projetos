/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.version.ui;

import csheets.io.VersionsFileInput;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 * Version Chooser Class
 *
 * @author RICARDLEITE
 */
public class VersionChooser extends JFrame {

	final JPanel panel1 = new JPanel(); //panel for exit and confirmation button
	final JPanel panel2 = new JPanel(); //panel for the versions list
	final JPanel panel3 = new JPanel(); //panel for title

	VersionsFileInput readFile = new VersionsFileInput();
	String listContent = ""; //all lines read from the file

	public VersionChooser() {
	}

	/**
	 * Pops up a window for the user to select the extensions version to load
	 *
	 * @param name Name of the extension
	 * @return name of the version to load
	 */
	public String newVersionChooser(String name) {
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});

		BorderLayout layoutb = new BorderLayout();
		setLayout(layoutb);
		ArrayList<String> versionsList = readFile.inputFile(name);
		final JList<String> lista;	//list with all versions avaiable

		setTitle("Version Chooser");

		final JButton confirmation = new JButton("Confirm");

		final JButton exit = new JButton("Exit");

		JLabel title = new JLabel("Please select the version: ");

		if (versionsList.isEmpty()) {
			return listContent;
		} else {
			lista = new JList<String>(versionsList.
				toArray(new String[versionsList.size()]));
		}
		lista.setLayoutOrientation(JList.VERTICAL_WRAP);
		lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane listScroller = new JScrollPane(lista);
		listScroller.setPreferredSize(new Dimension(95, 95));

		panel1.add(confirmation);

		panel1.add(exit);
		add(panel1, BorderLayout.SOUTH);
		panel2.add(lista);
		add(panel2, BorderLayout.CENTER);
		panel2.setBackground(Color.ORANGE);
		panel3.add(title);
		panel3.setBackground(Color.gray);
		add(panel3, BorderLayout.NORTH);

		pack();
		setResizable(true);
		setLocationRelativeTo(null);
		setVisible(true);

		confirmation.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (lista.getSelectedValuesList().isEmpty()) {
					confirmation.setSelected(true);
				} else {
					listContent = lista.getSelectedValue();
					confirmation.setSelected(true);
				}

			}
		});

		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exit.setSelected(true);

			}
		});

		while (!exit.isSelected() && !confirmation.isSelected()) {
			//wait for user selection
			try {
				sleep(2);
			} catch (InterruptedException ex) {
				Logger.getLogger(VersionChooser.class.getName()).
					log(Level.SEVERE, null, ex);
			}

		}

		dispose();
		return listContent;
	}

}
