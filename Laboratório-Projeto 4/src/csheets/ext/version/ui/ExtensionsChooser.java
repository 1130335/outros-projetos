/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.version.ui;

/**
 *
 * @author RICARDLEITE
 */
import csheets.ext.Extension;
import csheets.ext.ExtensionManager;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.SortedMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Pops up the Extensions Chooser Window
 *
 */
public class ExtensionsChooser extends JFrame {

	private boolean toContinue = false;

	public ExtensionsChooser(JPanel extensionsPanel) {
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});

		BorderLayout bl = new BorderLayout();

		setTitle("Choose Extensions");
		setLocationRelativeTo(null);
		final JButton confirmation = new JButton("Confirm");

		JScrollPane listScroller = new JScrollPane(extensionsPanel);
		listScroller.setPreferredSize(new Dimension(250, 100));
		extensionsPanel.setBounds(61, 11, 81, 140);
		//extensionsPanel.setLayout(new BoxLayout(extensionsPanel, BoxLayout.Y_AXIS));
		extensionsPanel.setLayout(new GridLayout(7, 3));
		add(extensionsPanel, BorderLayout.CENTER);
		add(confirmation, BorderLayout.SOUTH);

		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - extensionsPanel.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - extensionsPanel.getHeight()) / 2);
		extensionsPanel.setLocation(x, y);
		pack();

		setResizable(true);
		setVisible(true);

		confirmation.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.
					showMessageDialog(rootPane, "The extensions will be now loaded! \nThe application may not respond if you deactivated Style Extension!");
				toContinue = true;
				confirmation.setSelected(true);

			}
		});

		while (!confirmation.isSelected()) {
			try {
				sleep(2);
			} catch (InterruptedException ex) {
				Logger.getLogger(ExtensionsChooser.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		}
		dispose();
	}

	/**
	 * Constructor of the Extensions Chooser class
	 */
	public ExtensionsChooser() {
	}

	/**
	 * Allows the user to select which extensions will be loaded
	 *
	 * @param extensionsVersionsPanel Versions Panel
	 * @param checkBoxList	CheckBox list with all extensions to load
	 * @param extensionMap Application ExtensionsMap with all extensions to load
	 * @param extensionList	Extension List with all extensions from the file
	 * extensions.props
	 */
	public void extensionsChooser(JPanel extensionsVersionsPanel,
								  ArrayList<JCheckBox> checkBoxList,
								  SortedMap<String, Extension> extensionMap,
								  ArrayList<Extension> extensionList) {
		int length = 0;
		ExtensionsChooser ec = new ExtensionsChooser(extensionsVersionsPanel);

		while (!ec.getContinueStatus()) {
			try {
				sleep(2);
			} catch (InterruptedException ex) {
				Logger.getLogger(ExtensionManager.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		}
		while (length < checkBoxList.size()) {
			if (checkBoxList.get(length).isSelected()) {
				extensionMap.
					put(extensionList.get(length).getName(), extensionList.
						get(length));
			}
			length++;

		}
	}

	/**
	 * Returns the continue status
	 *
	 * @return true, if the user confirmed or exited the Extensions loader
	 */
	public boolean getContinueStatus() {
		return toContinue;
	}

}
