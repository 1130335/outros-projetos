/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.wizard.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;

/**
 *
 * @author Rita
 */
public class GraphicController {

	private SpreadsheetTable myFocusOwner;
	private Cell graphicCell;

	public GraphicController(final SpreadsheetTable focusOwner) {
		this.myFocusOwner = focusOwner;
	}

	public void createNewGraphic(String name, String beginRangeString,
								 String endRangeString) {
		this.myFocusOwner.selectAll();
		Cell[][] aux = this.myFocusOwner.getSelectedCells();
		Address beginRange = null;
		Address endRange = null;
		List<Cell> tmp = new ArrayList<>();
		for (Cell[] vec : aux) {
			tmp.addAll(Arrays.asList(vec));
		}

		for (int i = 0; i < tmp.size(); i++) {
			if (!tmp.get(i).getContent().isEmpty()) {
				if (tmp.get(i).getAddress().toString().
					equalsIgnoreCase(beginRangeString)) {
					beginRange = tmp.get(i).getAddress();
				}
				if (tmp.get(i).getAddress().toString().
					equalsIgnoreCase(endRangeString)) {
					endRange = tmp.get(i).getAddress();
				}
			}
		}
		Spreadsheet spreadsheet = this.myFocusOwner.getSpreadsheet();
		SortedSet<Cell> cellsToUse = spreadsheet.getCells(beginRange, endRange);
		cellsToUse.first().createGraphic(name, cellsToUse);
		this.graphicCell = cellsToUse.first();
	}

	public Cell getCellwithGraphic() {
		return this.graphicCell;
	}

	public SpreadsheetTable getFocusOwner() {
		return this.myFocusOwner;
	}
}
