package csheets.ext.wizard.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

public class WizardAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public WizardAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Wizard";
    }

    @Override
    protected void defineProperties() {
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        new WizardWindow(null, uiController);
    }

    /**
     * This method exchanges the old parameter for the new one.
     *
     * @param stringParamIntroduced String with the parameters inserted by the
     * user
     * @param newParam New parameter that the user wants to insert
     * @param size Number of parameters that the function has
     * @param index The index of the new parameter (the index in the string is
     * calculated based on the split by the ';')
     * @return A string with the new parameter inserted by the user added to the
     * string with all the parameters inserted.
     */
    public static String checkStringParamIntroduced(String stringParamIntroduced, String newParam, int size, int index) {

        String returnString = "";

        String parameters[] = new String[size];

        String initialCharacters = stringParamIntroduced.substring(0, stringParamIntroduced.indexOf('(') + 1);

        stringParamIntroduced = stringParamIntroduced.substring(stringParamIntroduced.indexOf('(') + 1);

        stringParamIntroduced = stringParamIntroduced.substring(0, stringParamIntroduced.length() - 1);
        
        if (size != 0) {
            for (int i = 0; i < size; i++) {
                parameters[i] = stringParamIntroduced.split(";")[i];
            }

            for (int i = 0; i < size; i++) {
                if (i == index) {
                    if (!newParam.isEmpty()) {
                        parameters[i] = newParam;
                    } else {
                        parameters[i] = " ";
                    }
                }
            }
            
            returnString = initialCharacters;

            for (int i = 0; i < size; i++) {
                if (i != size - 1) {
                    returnString += parameters[i] + ";";
                } else {
                    returnString += parameters[i] + ")";
                }
            }
        } else {
            returnString = initialCharacters;
            returnString += newParam + ")";
        }
        return returnString;
    }

    /**
     * 
     * This method verifies if the user has inserted a parameter on a certain
     * index.
     *
     * @param paramIntroduced String with the type of the parameters that the
     * user needs to enter
     * @param input The new parameter that the user is inserting
     * @param size Number of parameters that the function has
     * @param index The index of the new parameter
     * @return True if the user is inserting a new parameter or return false if
     * the user is inserting an empty string or the type of the parameter that
     * he needed to insert.
     */
    public static boolean verifyParamChanged(String paramIntroduced, String input, int size, int index) {
        /**
         * Array with all the parameters
         */
        String parameters[] = new String[size];
        /**
         * This string will be added in the end to the return string
         */
        paramIntroduced.substring(0, paramIntroduced.indexOf('(') + 1);

        /* Here, we remove all the characters before '(' (including) */
        paramIntroduced = paramIntroduced.substring(paramIntroduced.indexOf('(') + 1);
        /* This line removes the last ')' */
        paramIntroduced = paramIntroduced.substring(0, paramIntroduced.length() - 1);

        for (int i = 0; i < size; i++) {
            parameters[i] = paramIntroduced.split(";")[i];
        }

        for (int i = 0; i < size; i++) {
            if (i == index && !parameters[i].equals(input) && !input.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}