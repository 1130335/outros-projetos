/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.wizard.ui;

import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;

/**
 *
 * @author Rita
 */
public class GraphicWizardAction extends FocusOwnerAction {

	public GraphicWizardAction() {

	}

	@Override
	protected String getName() {
		return "Graphic Wizard";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		new GraphicDataWindow(focusOwner);
	}

}
