package csheets.ext.wizard.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 * Representes the UI extension menu of the Wizard extension.
 */
public class WizardMenu extends JMenu {

	/**
	 * Creates a new Wizard menu. This constructor creates and adds the Wizard
	 * funcinality to the menu options.
	 *
	 * @param uiController
	 */
	public WizardMenu(UIController uiController) {
		super("Wizard");
		setMnemonic(KeyEvent.VK_W);

		// Adds font actions
		add(new GraphicWizardAction());
		add(new WizardAction(uiController));
	}
}
