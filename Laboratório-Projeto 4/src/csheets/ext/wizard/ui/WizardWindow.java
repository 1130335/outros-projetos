package csheets.ext.wizard.ui;

import csheets.core.Cell;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.lang.Language;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class WizardWindow extends JDialog {

	/**
	 * Label with the sintax
	 */
	private final JLabel lblSyntax;

	/**
	 * Label with string 'Function'
	 */
	private final JLabel lblFunction;
	/**
	 * Label withe the stirng 'Result'
	 */
	private final JLabel lblResult;
	/**
	 * Label with the string 'Inserted Parameters'
	 */
	private final JLabel lblInsertedParam;
	/**
	 * TextField with the function's syntax+parameter setted
	 */
	private final JTextField txtInsertedParam;
	/**
	 * Label which will have the result or an error message
	 */
	private JTextField txtResult;
	/**
	 * Combobox with all the functions available
	 */
	private final JComboBox cmbFunctions;
	/**
	 * Label with the string 'Parameters'
	 */
	private final JLabel lblParameters;
	/**
	 * Combobox with all the parameters of a function
	 */
	private final JComboBox cmbParameters = new JComboBox();
	/**
	 * Insert, OK, Exit and Help buttons
	 */
	private final JButton btnOk, btnExit, btnInserir, btnHelp;
	/**
	 * Textfield with the function's syntax
	 */
	private final JTextField txtSyntax;
	/**
	 * Textfield editable with the parameters setted
	 */
	private final JTextField txtEditBoxParam;
	/**
	 * List with the type of each parameter
	 */
	private final JList jlDescription;
	/**
	 * Array with all the functions available
	 */
	final Function[] functions = Language.getInstance().getFunctions();
	/**
	 * Array that will be filled in with all the parameters of a function
	 */
	FunctionParameter funcP[];
	/**
	 * Array that will be filled in with all the parameters of a Binary
	 */
	List<BinaryOperator> operadores = Language.getInstance().
		getBinaryOperators();
	/**
	 * List that will be filled in with 'FunctionParameter' arrays
	 */
	private List parameters = new ArrayList<FunctionParameter[]>();
	/**
	 * Auxiliar variable for control, which represents the index of the function
	 * on the comboBox
	 */
	private int controlAux;
	/**
	 * List that will be filled with the parameters inserted by the user
	 */
	private List inputParam = new ArrayList<String>();
	/**
	 * Auxiliar variable for function
	 */
	private Function funcAux;
	/**
	 * Array to check if parameters were inserted by the user
	 */
	private boolean parametersChanged[];

	/**
	 * The checkbox that enables/disables the abstract syntax tree
	 */
	private final JCheckBox checkbox = new JCheckBox("Abstract Syntax Tree", true);

	/**
	 * The text that will shown on the variables to edit.
	 */
	String variableType = "";

	/**
	 * Class constructor Opens the Wizard which helps the user to select the
	 * functions available to use.
	 *
	 * @param window the frame window
	 * @param uiController the user interface controller
	 */
	public WizardWindow(JFrame window, final UIController uiController) {

		super(window, "Wizard");
		this.setLayout(new BorderLayout());

		//Button for the south panel
		btnHelp = new JButton("Help");

		/* pNorth */
		JPanel pNorth = new JPanel(new BorderLayout());
		pNorth.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));

		JPanel pNorth2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pNorth2.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));

		JPanel treePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		treePanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));
		treePanel.setVisible(true);

		JPanel checkboxPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		checkboxPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));
		checkboxPanel.setVisible(true);
		checkbox.setVisible(true);
		checkboxPanel.add(checkbox);

		JPanel ASTPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		ASTPanel.setVisible(true);
		ASTPanel.setBackground(Color.white);
		ASTPanel.setForeground(Color.yellow);

		treePanel.add(checkbox);
		treePanel.add(ASTPanel);

		lblSyntax = new JLabel("Sintax: ");
		txtSyntax = new JTextField(25);

		txtSyntax.setEditable(
			false);

		pNorth2.add(lblSyntax);

		pNorth2.add(txtSyntax);

		JPanel pNorth1 = new JPanel(new FlowLayout(FlowLayout.CENTER));

		pNorth1.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 20));

		lblFunction = new JLabel("Function: ");

		pNorth1.add(lblFunction);

		jlDescription = new JList();
		cmbFunctions = new JComboBox();
		List<BinaryOperator> operadores = Language.getInstance().
			getBinaryOperators();
		for (BinaryOperator operadore : operadores) {
			cmbFunctions.addItem(operadore.toString());
		}

		for (Function function : functions) {
			cmbFunctions.addItem(function.getIdentifier());
			FunctionParameter[] parametersAux = function.getParameters();
			parameters.add(parametersAux);
		}

		cmbFunctions.setSelectedIndex(
			-1);
		cmbFunctions.addActionListener(
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e
				) {
					variableType = "";
					/**
					 * This variable will be incremented as the functions array
					 * is iterated
					 */

					int control = 0;
					txtResult.setText("");
					txtInsertedParam.setText("");

					for (Function function : functions) {
						if (function.getIdentifier().equals(cmbFunctions.
							getSelectedItem())) { //verify if the identifier=selected item from cmbox
							funcAux = function;
							while (!inputParam.isEmpty()) {
								inputParam.remove(0);
							}
							controlAux = control; //index da função na cmbox
							funcP = function.getParameters(); //filling funcP[] with all the paramaters of functions
							int size = funcP.length;
							parametersChanged = new boolean[size];
							if (size > 0) {
								variableType = funcP[0].getValueType().
								toString();
								for (int i = 1; i < size; i++) {
									variableType += ";" + funcP[i].
									getValueType().
									toString();

								}
								txtSyntax.
								setText("=" + function.getIdentifier() + "(" + variableType + ")");
								txtInsertedParam.setText(txtSyntax.getText());
								jlDescription.setListData(function.
									getParameters());

							} else {
								txtSyntax.
								setText("=" + function.getIdentifier() + "()");
								txtInsertedParam.setText(txtSyntax.getText());
								jlDescription.setListData(function.
									getParameters());
							}

							cmbParameters.removeAllItems();
							/**
							 * Array with a certain parameter of a certain
							 * function
							 */
							FunctionParameter par[] = (FunctionParameter[]) parameters.
							get(control);
							for (FunctionParameter func : par) {
								cmbParameters.addItem(func.getName());
							}
							cmbParameters.setSelectedIndex(-1);
							cmbParameters.
							addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									/**
									 * Array with a certain parameter of a
									 * certain function
									 */
									FunctionParameter par[] = (FunctionParameter[]) parameters.
									get(controlAux);
									for (FunctionParameter func : par) {
										if (func.getName().equals(cmbParameters.
											getSelectedItem())) {
											if (inputParam.size() == 0) {
												for (FunctionParameter func2 : par) {
													inputParam.add("");
												}
											}
											txtEditBoxParam.
											setText((String) inputParam.
												get(cmbParameters.
													getSelectedIndex()));
										}
									}
								}
							});
						}
						control++;
					}
				}
			}
		);

		//Layout nPanel
		pNorth1.add(cmbFunctions, BorderLayout.NORTH);

		pNorth1.add(jlDescription, BorderLayout.CENTER);

		pNorth2.add(
			new JScrollPane(jlDescription));

		pNorth.add(pNorth1, BorderLayout.NORTH);

		pNorth.add(pNorth2, BorderLayout.CENTER);

		pNorth.add(treePanel, BorderLayout.SOUTH);

		/* pCenter */
		JPanel pCenter = new JPanel(new BorderLayout());

		JPanel pCenter1 = new JPanel(new FlowLayout(FlowLayout.CENTER));

		pCenter1.setBorder(
			new EmptyBorder(10, 0, 0, 0));
		lblParameters = new JLabel("Parameters: ");
		btnInserir = new JButton("Insert");

		btnInserir.addActionListener(
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e
				) {
					boolean allParamChanged = true;
					/* Here we check if the function asks for parameters or not */
					if (funcAux.getParameters().length == 0) {
						String parameterIntroduced = txtEditBoxParam.getText();
						allParamChanged = WizardAction.
						verifyParamChanged(txtSyntax.
							getText(), txtEditBoxParam.getText(), 1, 0);
						txtInsertedParam.setText(WizardAction.
							checkStringParamIntroduced(txtInsertedParam.
								getText(), parameterIntroduced, 1, 0));
					} else {
						if (cmbParameters.getSelectedIndex() > -1) {
							inputParam.
							set(cmbParameters.getSelectedIndex(), txtEditBoxParam.
								getText());
							parametersChanged[cmbParameters.getSelectedIndex()] = WizardAction.
							verifyParamChanged(txtSyntax.getText(), txtEditBoxParam.
											   getText(), funcAux.
											   getParameters().length, cmbParameters.
											   getSelectedIndex());
							txtInsertedParam.setText(WizardAction.
								checkStringParamIntroduced(txtInsertedParam.
									getText(), txtEditBoxParam.getText(), funcAux.
														   getParameters().length, cmbParameters.
														   getSelectedIndex()));
							/* Automaticaly update the JTextField with the result */
							for (int i = 0; i < parametersChanged.length; i++) {
								if (parametersChanged[i] == false) {
									allParamChanged = false;
								}
							}
						} else {
							txtResult.
							setText("Invalid expression/Parameters missing");
						}
					}
					/**
					 * Formula type variable used to 'calculate' the final
					 * result
					 */
					Formula form = null;

					if (allParamChanged) {
						try {
							form = FormulaCompiler.getInstance().
							compile(null, txtInsertedParam.getText());
						} catch (Exception ex) {
							txtResult.
							setText("Invalid expression/Parameters missing");
						}
					} else {
						txtResult.
						setText("Invalid expression/Parameters missing");
					}

					if (allParamChanged) {
						try {
							txtResult.setText(form.evaluate().toString());
						} catch (Exception ex) {
							txtResult.
							setText("Invalid expression/Parameters missing");
						}
					} else {
						txtResult.
						setText("Invalid expression/Parameters missing");
					}
				}
			}
		);

		//Layout pCenter
		pCenter1.add(lblParameters);

		pCenter1.add(cmbParameters);

		txtEditBoxParam = new JTextField(10);

		pCenter1.add(txtEditBoxParam);

		pCenter1.add(btnInserir);

		JPanel pCenter2 = new JPanel(new FlowLayout(FlowLayout.CENTER));

		pCenter2.setBorder(
			new EmptyBorder(10, 0, 0, 0));
		lblInsertedParam = new JLabel("Inserted Parameters: ");

		pCenter2.add(lblInsertedParam);
		txtInsertedParam = new JTextField(25);

		txtInsertedParam.setEditable(
			false);
		pCenter2.add(txtInsertedParam);

		JPanel pCenter3 = new JPanel(new FlowLayout(FlowLayout.CENTER));

		pCenter3.setBorder(
			new EmptyBorder(10, 0, 0, 0));
		lblResult = new JLabel("Result: ");

		pCenter3.add(lblResult);
		txtResult = new JTextField(20);

		txtResult.setEditable(
			false);
		pCenter3.add(txtResult);

		pCenter.add(pCenter1, BorderLayout.NORTH);

		pCenter.add(pCenter2, BorderLayout.CENTER);

		pCenter.add(pCenter3, BorderLayout.SOUTH);


		/* pSouth */
		JPanel pSouth = new JPanel(new FlowLayout(FlowLayout.CENTER));

		btnOk = new JButton("OK");

		btnOk.addActionListener(
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e
				) {
					try {
						uiController.getActiveCell().
						setContent(txtInsertedParam.
							getText());
						dispose();
					} catch (FormulaCompilationException ae) {
						JOptionPane.
						showMessageDialog(rootPane, "Invalid / Empty String input!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		);
		btnExit = new JButton("Exit");

		btnExit.addActionListener(
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e
				) {
					dispose();
				}
			}
		);

		/**
		 * Action that gets all the function's descriptions and shows it to the
		 * user.
		 */
		btnHelp.addActionListener(
			new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e
				) {

					for (Function function : functions) {
						if (function.getIdentifier().
						equals(cmbFunctions.
							getSelectedItem())) {
							JOptionPane.
							showMessageDialog(rootPane, function.
											  getFunctionDescription(), "Descritpion", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}
			}
		);

		checkbox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				if (checkbox.isSelected()) {

					Cell cell = uiController.getActiveCell();
					String formulaSrc = cell.getFormula().toString();
					try {
						Expression expression = FormulaCompiler.getInstance().
							compile(cell, formulaSrc).getExpression();
					} catch (FormulaCompilationException ex) {
						Logger.getLogger(WizardWindow.class.getName()).
							log(Level.SEVERE, null, ex);
					}

				}
			}
		});

		//Layout pSouth
		pSouth.add(btnOk, BorderLayout.NORTH);

		pSouth.add(btnExit, BorderLayout.NORTH);

		pSouth.add(btnHelp, BorderLayout.NORTH);

		this.add(pNorth, BorderLayout.NORTH);

		this.add(pCenter, BorderLayout.CENTER);

		this.add(pSouth, BorderLayout.SOUTH);

		this.setVisible(
			true);

		this.setResizable(
			true);

		this.setSize(
			450, 450);

		this.setAlwaysOnTop(
			true);

		this.setLocationRelativeTo(
			null);
	}

}
