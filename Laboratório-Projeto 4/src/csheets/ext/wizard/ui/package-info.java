/**
 * Provides the UI for the Wizard extension.
 * @author 1130079
 */
package csheets.ext.wizard.ui;