package csheets.ext.wizard;

import csheets.ext.Extension;
import csheets.ext.wizard.ui.UIWizard;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author 1130709
 */
public class ExtensionWizard extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Wizard";

    /**
     * Creates a new Wizard extension.
     */
    public ExtensionWizard() {
        super(NAME);
    }

    /**
     * Returns the user interface extension of this extension.
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIWizard(this, uiController);
    }
}
