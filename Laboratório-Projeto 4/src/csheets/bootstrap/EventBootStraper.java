/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.bootstrap;

import csheets.ext.crm.application.EventsController;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.persistence.JPAContactRepository;
import csheets.ext.crm.persistence.JPARepositoryFactory;
import java.util.Date;
import java.util.List;

/**
 *
 * @author daniel
 */
public class EventBootStraper {

	public void execute() {
		EventsController event_controller = new EventsController();
		JPAContactRepository repo = new JPARepositoryFactory().
			getJPAContactRepository();
		List<Contact> contacts = repo.AllContacts();
		String event_string = "event";
		int event_num = 0;
		Date d = new Date();
		for (Contact contact : contacts) {
			Event event = new Event(contact, d, 0, 0, event_string + event_num, null,d,0,0);
			event_num++;
			event_controller.addEvent(event);
		}

	}
}
