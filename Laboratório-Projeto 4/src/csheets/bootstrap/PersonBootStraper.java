/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.bootstrap;

import csheets.ext.crm.application.CompanyController;
import csheets.ext.crm.application.PersonController;
import csheets.ext.crm.domain.Company;
import csheets.ext.crm.domain.Person;

/**
 *
 * @author daniel
 */
public class PersonBootStraper {

	public void execute() {
		PersonController person_controller = new PersonController();
		CompanyController company_controller = new CompanyController();

		Person p1, p2;
		byte[] img = null;
		Company comp = new Company("Company", img);
		company_controller.saveContactDB(comp);

		p1 = person_controller.
			NewContact("Daniel", "Oliveira", img, "Bombeiro", comp, null, null);
		p2 = person_controller.
			NewContact("Manuel", "Oliveira", img, "Doutor", comp, null, null);

		person_controller.saveContactDB(p1);
		person_controller.saveContactDB(p2);

	}
}
