/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.bootstrap;

/**
 *
 * @author Daniel Oliveira
 */
public class BootStraper {

	public void execute() {
        PersonBootStraper personBoot = new PersonBootStraper();
        
        personBoot.execute();
        
        EventBootStraper eventBoot = new EventBootStraper();
        
        eventBoot.execute();
	}
}
