grammar Formula;

options {
	language=Java;
	output=AST;
}


@parser::header {
package csheets.core.formula.compiler;
}

@lexer::header {
package csheets.core.formula.compiler;
}

// Alter code generation so catch-clauses get replace with
// this action.
@rulecatch {
	catch (RecognitionException e) {
		reportError(e);
		throw e;
	}
}

@members {
	protected void mismatch(IntStream input, int ttype, BitSet follow)
		throws RecognitionException
	{
    	throw new MismatchedTokenException(ttype, input);
	}

	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
		throws RecognitionException
	{
		throw e;
	}

	@Override
  	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    	throw new MismatchedTokenException(ttype, input);
 	}
}

/*Macros 06.01 - Teria que se criar uma nova gramática de acordo com esta mas não consegui adicionar uma nova ao projecto sem que se criasse conflitos. Adicionei nos ficheiros a nova gramática assim como no projecto e mesmo assim não fui bem sucedido.
Fica aqui a minha pequena alteração para se for necessário no futuro.
1130486

//A nova gramática começaria pelo inicio onde permitia ; para comentários e a existência ou não do '=' permitia executar cálculos.

inicio : ';' comment
		| expression
		| EQ! expression;

//expression
//	: comparison EOF!
//	| attribution EOF!
//	;

comment
	:  ( ('a'..'z'|'A'..'Z')+ | ('0'..'9')+  )+
	;

*/

expression
	: EQ! comparison EOF!
	| EQ! attribution EOF!
	| HASH! money_text LBRA! concatenation_money RBRA! EOF!
	;
//	OLD EXPRESSIONS
//	- EQ! FUNCTION! LBRA! comparison (SEMI! comparison)* RBRA! EOF!
//      - EQ! comparison (attribution comparison)? EOF!


attribution
	: reference ATRB^ (attribution | comparison)
	;
comparison
	: concatenation
		( ( EQ^ | NEQ^ | GT^ | LT^ | LTEQ^ | GTEQ^ ) concatenation )?
	;

concatenation
	: arithmetic_lowest
		( AMP^ arithmetic_lowest )*
	;

arithmetic_lowest
	:	arithmetic_low
		( ( PLUS^ | MINUS^ ) arithmetic_low )*
	;

arithmetic_low
	:	arithmetic_medium
		( ( MULTI^ | DIV^ ) arithmetic_medium )*
	;

arithmetic_medium
	:	arithmetic_high
		( POWER^ arithmetic_high )?
	;

arithmetic_high
	:	arithmetic_highest ( PERCENT^ )?
	;
concatenation_money
	: arithmetic_low_money
		( ( PLUS^ | MINUS^ ) arithmetic_low_money )*
	;

arithmetic_low_money
	: arithmetic_highest_money
		( ( MULTI^ | DIV^ ) arithmetic_highest_money )*;

arithmetic_highest_money
	:	( MINUS^ )? atom_money
	;

atom_money
	:	money_number
	;


arithmetic_highest
	:	( MINUS^ )? atom
	;

atom
	:	function_call
	|	reference
	|	literal
	|	LPAR! comparison RPAR!
	;

function_call
	:	FUNCTION^ LPAR!
		( comparison ( SEMI! comparison )* )?
		RPAR!
		| block
		| FOR^ block
	;

block	:	LBRA^ (attribution | comparison) (SEMI! (attribution | comparison))* RBRA!
	;

reference
	:	CELL_REF
		( ( COLON^ ) CELL_REF )?
        |       VAR_REF
	;

literal
	:	NUMBER
	|	STRING
	;

/*  USE CASE MACROS 01_2 RELATED GRAMMAR    */

money_number
	: NUMBER money_reference
	;

money_reference
	: DOLLAR | EURO | POUND;

DOLLAR:'$';
EURO:'€';
POUND:'£';

money_text
	:       DOLLARTXT
	|	EUROTXT
	|	POUNDTXT;

DOLLARTXT:'dollar' | 'DOLLAR';
EUROTXT:'euro' | 'EURO';
POUNDTXT:'pound' | 'POUND';

fragment LETTER: ('a'..'z'|'A'..'Z') ;

FOR	: 'FOR' | 'for';

FUNCTION :
	  ( LETTER )+
	;

 CELL_REF
	:
		( ABS )? LETTER ( LETTER )?
		( ABS )? ( DIGIT )+
	;

 VAR_REF
        : AT (LETTER) ( NUMBER | LETTER) * | AT (LETTER) ( NUMBER | LETTER)* LBRACK (NUMBER)+ RBRACK
        ;

/* String literals, i.e. anything inside the delimiters */

STRING	:	QUOT
		(options {greedy=false;}:.)*
		QUOT  { setText(getText().substring(1, getText().length()-1)); }
	;

QUOT: '"'
	;

/* Numeric literals */
NUMBER: ( DIGIT )+ ( COMMA ( DIGIT )+ )? ;

fragment
DIGIT : '0'..'9' ;

/* Comparison operators */
EQ		: '=' ;
HASH	: '#' ;
NEQ		: '<>' ;
LTEQ	: '<=' ;
GTEQ	: '>=' ;
GT		: '>' ;
LT		: '<' ;

/* Text operators */
AMP		: '&' ;

/* Attribution Operator */
ATRB	: ':=';
/* Arithmetic operators */
PLUS	: '+' ;
MINUS	: '-' ;
MULTI	: '*' ;
DIV	: '/' ;
POWER	: '^' ;
PERCENT : '%' ;

/* Reference operators */
fragment ABS : '$' ;
fragment EXCL:  '!'  ;
COLON	: ':' ;
AT      :'@';

/* Miscellaneous operators */
COMMA	: ',' ;
SEMI	: ';' ;
LPAR	: '(' ;
RPAR	: ')' ;
LBRA	: '{';
RBRA	: '}';
LBRACK  : '[';
RBRACK  : ']';

/* White-space (ignored) */
WS: ( ' '
	| '\r' '\n'
	| '\n'
	| '\t'
	) {$channel=HIDDEN;}
	;


