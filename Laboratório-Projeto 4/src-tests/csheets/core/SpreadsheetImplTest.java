/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author IvoTeixeira
 */
public class SpreadsheetImplTest {

	public SpreadsheetImplTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of writeMatrixFromCell method, of class SpreadsheetImpl.
	 */
	@Test
	public void testWriteMatrixFromCell() {
		System.out.println("writeMatrixFromCell");
		Workbook w = new Workbook(1);
		Spreadsheet s = w.getSpreadsheet(0);
		Address address = new Address(0, 0);
		Cell cell = s.getCell(address);
		String content = "{5;4; 3;6}";
		SpreadsheetImpl instance = null;
		instance.writeMatrixFromCell(cell, content);
		assertEquals(instance.getCell(0, 0).getContent(), 5);
	}

}
