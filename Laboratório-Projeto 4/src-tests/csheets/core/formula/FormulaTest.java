/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author IvoTeixeira
 */
public class FormulaTest {

	public FormulaTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of setIsArray method, of class Formula.
	 */
	@Test
	public void testSetandGetIsArray() {
		System.out.println("setIsArray");
		Expression exp = null;
		Workbook w = new Workbook(1);
		Spreadsheet s = w.getSpreadsheet(0);
		Address address = new Address(0, 0);
		Cell cell = s.getCell(address);
		Formula instance = new Formula(cell, exp);
		instance.setIsArray(true);
		boolean flag = instance.getIsArray();
		assertEquals(flag, true);

	}

}
