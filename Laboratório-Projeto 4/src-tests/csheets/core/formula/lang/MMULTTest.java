/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.FunctionParameter;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Miguel
 */
public class MMULTTest {

	public MMULTTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getIdentifier method, of class MMULT.
	 */
	@Test
	public void testGetIdentifier() {
		System.out.println("getIdentifier");
		MMULT instance = new MMULT();
		String expResult = "";
		String result = instance.getIdentifier();
		assertEquals(expResult, result);

	}

	/**
	 * Test of applyTo method, of class MMULT.
	 */
	@Test
	public void testApplyTo() throws Exception {
		System.out.println("applyTo");
		Expression[] arguments = null;
		MMULT instance = new MMULT();
		Value expResult = null;
		Value result = instance.applyTo(arguments);
		assertEquals(expResult, result);

	}

	/**
	 * Test of getParameters method, of class MMULT.
	 */
	@Test
	public void testGetParameters() {
		System.out.println("getParameters");
		MMULT instance = new MMULT();
		FunctionParameter[] expResult = null;
		FunctionParameter[] result = instance.getParameters();
		assertArrayEquals(expResult, result);
	}

	/**
	 * Test of isVarArg method, of class MMULT.
	 */
	@Test
	public void testIsVarArg() {
		System.out.println("isVarArg");
		MMULT instance = new MMULT();
		boolean expResult = false;
		boolean result = instance.isVarArg();
		assertEquals(expResult, result);

	}

	/**
	 * Test of multiply method, of class MMULT.
	 */
	@Test
	public void testMultiply() {
		System.out.println("multiply");
		Value[][] Matrix1 = null;
		Value[][] Matrix2 = null;
		MMULT instance = new MMULT();
		Value expResult = null;
		Value result = instance.multiply(Matrix1, Matrix2);
		assertEquals(expResult, result);

	}

	/**
	 * Test of getFunctionDescription method, of class MMULT.
	 */
	@Test
	public void testGetFunctionDescription() {
		System.out.println("getFunctionDescription");
		MMULT instance = new MMULT();
		String expResult = "";
		String result = instance.getFunctionDescription();
		assertEquals(expResult, result);

	}

}
