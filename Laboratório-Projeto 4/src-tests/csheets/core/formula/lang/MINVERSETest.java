/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.FunctionParameter;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Miguel
 */
public class MINVERSETest {

	public MINVERSETest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getIdentifier method, of class MINVERSE.
	 */
	@Test
	public void testGetIdentifier() {
		System.out.println("getIdentifier");
		MINVERSE instance = new MINVERSE();
		String expResult = "";
		String result = instance.getIdentifier();
		assertEquals(expResult, result);

	}

	/**
	 * Test of applyTo method, of class MINVERSE.
	 */
	@Test
	public void testApplyTo() throws Exception {
		System.out.println("applyTo");
		Expression[] arguments = null;
		MINVERSE instance = new MINVERSE();
		Value expResult = null;
		Value result = instance.applyTo(arguments);
		assertEquals(expResult, result);

	}

	/**
	 * Test of invert method, of class MINVERSE.
	 */
	@Test
	public void testInvert() {
		System.out.println("invert");
		Value[][] matrix = null;
		MINVERSE instance = new MINVERSE();
		Value expResult = null;
		Value result = instance.invert(matrix);
		assertEquals(expResult, result);

	}

	/**
	 * Test of gaussian method, of class MINVERSE.
	 */
	@Test
	public void testGaussian() {
		System.out.println("gaussian");
		Value[][] a = null;
		int[] index = null;
		MINVERSE instance = new MINVERSE();
		instance.gaussian(a, index);

	}

	/**
	 * Test of getParameters method, of class MINVERSE.
	 */
	@Test
	public void testGetParameters() {
		System.out.println("getParameters");
		MINVERSE instance = new MINVERSE();
		FunctionParameter[] expResult = null;
		FunctionParameter[] result = instance.getParameters();
		assertArrayEquals(expResult, result);

	}

	/**
	 * Test of isVarArg method, of class MINVERSE.
	 */
	@Test
	public void testIsVarArg() {
		System.out.println("isVarArg");
		MINVERSE instance = new MINVERSE();
		boolean expResult = false;
		boolean result = instance.isVarArg();
		assertEquals(expResult, result);

	}

	/**
	 * Test of getFunctionDescription method, of class MINVERSE.
	 */
	@Test
	public void testGetFunctionDescription() {
		System.out.println("getFunctionDescription");
		MINVERSE instance = new MINVERSE();
		String expResult = "";
		String result = instance.getFunctionDescription();
		assertEquals(expResult, result);

	}

}
