/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.variable;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Rita
 */
public class VariableTest {

	public VariableTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class Variable.
	 */
	@Test
	public void testSetGetName() {
		System.out.println("getName");
		Variable instance = new Variable("Name");
		instance.setName("Name");
		String expResult = "Name";
		String result = instance.getName();
		assertEquals(expResult, result);;
	}

	/**
	 * Test of getValue method, of class Variable.
	 */
	@Test
	public void testSetGetValue() {
		System.out.println("getValue");
		int position = 0;
		Variable instance = new Variable("Name");
		instance.setValue(String.valueOf(1), position);
		String expResult = "1.0";
		String result = instance.getValue(position);
		assertEquals(expResult, result);
	}

}
