/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sharingFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pedro
 */
public class DownloadControllerTest {

	public DownloadControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Even though this is a Controller Method I thought it was important to
	 * test because it's the core of this Funcionality. Test of
	 * ContinueDownloadWithoutBar method, of class DownloadController.
	 */
	@Test
	public void testContinueDownloadWithoutBar() throws Exception {
		System.out.println("ContinueDownloadWithoutBar");
		File newFile = new File("Desktop\\CMYK.png");
		File oldFile = new File("Ipc08.02 Avaliacao\\CMYK.png");

		newFile.getParentFile().mkdirs();
		newFile.createNewFile();
		FileOutputStream f = new FileOutputStream(newFile);
		FileInputStream fileInputStream = new FileInputStream(oldFile);
		int maxBuffer = 512;
		int remainingBytes = fileInputStream.available(); //remaining bytes to be read
		int bufferSize = Math.min(remainingBytes, maxBuffer);
		byte[] FileBytes = new byte[bufferSize]; //size in bytes

		//Read File
		int bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);

		//until there's no more bytes to read (fills the progress bar with percentage)
		while (bytesRead > 0) {
			f.write(FileBytes, 0, bytesRead);
			remainingBytes = fileInputStream.available();
			bufferSize = Math.min(remainingBytes, maxBuffer);
			FileBytes = new byte[bufferSize];
			bytesRead = fileInputStream.read(FileBytes, 0, bufferSize);
		}
		//Close stream
		fileInputStream.close();
		f.flush();
		f.close();
		boolean result;
		if (newFile.exists()) {
			result = true;
			System.out.println(newFile.getPath());
		} else {
			result = false;
		}

		boolean expResult = true;
		assertEquals(expResult, result);
	}

}
