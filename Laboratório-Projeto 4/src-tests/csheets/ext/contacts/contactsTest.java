///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.contacts;
//
//import csheets.ext.crm.application.PersonController;
//import csheets.ext.crm.domain.Person;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import static org.junit.Assert.assertEquals;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
///**
// *
// * @author Miguel Teixeira
// */
//public class contactsTest {
//
//    public contactsTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    //Test to method getContactFirstName to validate the results and check for errors in the method structure.
//    @Test
//    public void getContactFirstName() {
//
//        String firstName = "Daniel";
//        String lastName = "Teixeira";
//        System.out.println("Get Contact Name");
//        Person first = new Person(firstName, lastName);
//        String result = first.getFirstName();
//        assertEquals(firstName, result);
//
//    }
//
//    //Test to method getContactLastName to validate the results and check for errors in the method structure.
//    @Test
//    public void getContactLastName() {
//
//        String firstName = "Daniel";
//        String lastName = "Teixeira";
//        System.out.println("Get Contact Name");
//        Person first = new Person(firstName, lastName);
//        String result = first.getLastName();
//        assertEquals(lastName, result);
//
//    }
//
//    //Test to method setContactLastName to validate the results and check for errors in the method structure.
//    @Test
//    public void setContactLastName() {
//        System.out.println("Set Contact Name");
//        Person last = new Person("Miguel", "Teixeira");
//        String lastname = ("Miguel");
//
//        last.setLastName(lastname);
//        String result = last.getLastName();
//        assertEquals(lastname, result);
//
//    }
//
//    //Test to method getImage to validate the results and check for errors in the method structure.
//    @Test
//    public void GetImage() {
//        System.out.println("getProfileImage");
//        byte[] buf = new byte[1024];
//        Person instance = new Person("", "", buf, "");
//        byte[] result = instance.getImage();
//        Assert.assertArrayEquals(buf, result);
//
//    }
//
//    //Test to method EditContact to validate the results and check for errors in the method structure.
//    @Test
//    public void EditContact() {
//        System.out.println("EditContact");
//        byte[] image = new byte[1024];
//        PersonController instance = new PersonController();
//        Person profile = new Person("fn", "ln", image, null, null, null, null);
//        instance.editContact(profile, "fn2", "fn1", image, null, null);
//
//    }
//
//    //Test to method removeContact to validate the results and check for errors in the method structure.
//    @Test
//    public void RemoveContact() {
//        System.out.println("Remove Contact");
//        PersonController instance = new PersonController();
//        Person contact = new Person("Daniel", "Oliveira");
//        instance.saveContactDB(contact);
//        instance.removeContact(0);
//        int result = instance.getallContacts().size();
//        int expResult = 0;
//        assertEquals(expResult, result);
//
//    }
//
//    @Test
//    public void testConfirm() {
//        System.out.println("Confirm");
//    }
//
//}
