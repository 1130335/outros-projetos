/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.Component;
import javax.swing.JButton;

/**
 *
 * @author Paulo
 */
public class LineItemImpl implements LineItem {

	private String name;
	private String text;

	/**
	 * Example
	 *
	 * @return new jbutton with name and text set
	 */
	public Component create() {
		JButton j = new JButton();
		j.setText(text);
		j.setName(name);
		return j;
	}

	public String getName() {
		return this.name;
	}

	public String getText() {
		return this.text;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setText(String text) {
		this.text = text;
	}
}
