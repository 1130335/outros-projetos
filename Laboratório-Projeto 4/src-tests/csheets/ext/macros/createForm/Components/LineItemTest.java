/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.createForm.Components;

import java.awt.Component;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo
 */
public class LineItemTest {

	public LineItemTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of create method, of class LineItem.
	 */
	@Test
	public void testCreate() {
		System.out.println("create");
		LineItem instance = new LineItemImpl();
		instance.setName("teste");
		instance.setText("teste2");
		Component result = instance.create();
		System.out.println("New Component");
		System.out.println("Name : " + result.getName());
		assertNotNull(result);
	}

	/**
	 * Test of getName method, of class LineItem.
	 */
	@Test
	public void testGetName() {
		System.out.println("getName");
		LineItem instance = new LineItemImpl();
		String expResult = "nome";
		instance.setName(expResult);
		instance.setText(expResult);
		String result = instance.getName();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getText method, of class LineItem.
	 */
	@Test
	public void testGetText() {
		System.out.println("getText");
		LineItem instance = new LineItemImpl();
		String expResult = "textTeste";
		instance.setText(expResult);
		instance.setName(expResult);
		String result = instance.getText();
		assertEquals(expResult, result);
	}

}
