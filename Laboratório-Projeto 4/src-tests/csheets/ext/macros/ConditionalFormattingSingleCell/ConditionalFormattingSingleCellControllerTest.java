/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ConditionalFormattingSingleCell;

import csheets.CleanSheets;
import csheets.core.Address;
import csheets.core.CellImpl;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sergio
 */
public class ConditionalFormattingSingleCellControllerTest {

	public ConditionalFormattingSingleCellControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test to check if the information in sidebar is the most correct (with the
	 * information of the last conditional formatted cell).
	 *
	 * Also, test if the methods setLastCFormattedCell and getLastCFormattedCell
	 * in UIController are correct.
	 *
	 * The dificult to test this methods is that the operations inside his are
	 * based in actions in user interface
	 *
	 * @param active where the information should be display
	 */
	@Test
	public void setText() throws FormulaCompilationException {
		System.out.println("setTextTest");

		String expResult = "";
		String result = null;
		CleanSheets app = null;
		CellImpl lastCFormattedCell = null;

		Address adress = new Address(2, 2);
		UIController instance = new UIController(app);
		instance.setLastCFormattedCell(lastCFormattedCell);

		if (instance.getLastCFormattedCell() != null) {
			result = instance.getLastCFormattedCell().getContent().toString();
		} else {
			result = "";

		}
		assertEquals(expResult, result);

	}
}
