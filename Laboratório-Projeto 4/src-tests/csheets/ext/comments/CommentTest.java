/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniela Maia
 */
public class CommentTest {

	public CommentTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getComment method, of class Comment.
	 */
	@Test
	public void testSetGetComment() {
		System.out.println("getComment");
		Comment instance = new Comment("comment", "user");
		String expResult = "comment";
		instance.setComment(expResult);
		String result = instance.getComment();
		assertEquals(expResult, result);

	}

	/**
	 * Test of getUser method, of class Comment.
	 */
	@Test
	public void testSetGetUser() {
		System.out.println("getUser");
		Comment instance = new Comment("comment", "user");
		String expResult = "user";
		instance.setUser(expResult);
		String result = instance.getUser();
		assertEquals(expResult, result);

	}

}
