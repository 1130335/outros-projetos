/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Address;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author André Garrido - 1101598
 */
public class JPAAddressRepositoryTest {

	public JPAAddressRepositoryTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of saveAddress method, of class JPAAddressRepository.
	 */
	@Test
	public void testSaveAddress() {
		System.out.println("SAVE ADDRESS");
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		Address addr = new Address(Street, Area, ZipCode, City, Country);

		JPAAddressRepository repo = Persistence.getJPARepositoryFactory().
			getJPAAddressRepository();
		repo.saveAddress(addr);
		List<Address> allAddress = repo.listAll();
		String result = "", expected = addr.toString();
		for (Address a : allAddress) {
			if (a.toString().equals(expected)) {
				result = a.toString();
			}
		}
		assertEquals(expected, result);
	}

	/**
	 * Test of updateAddress method, of class JPAAddressRepository.
	 */
//	@Test
//	public void testUpdateAddress() {
//		System.out.println("updateAddress");
//		Address a = null;
//		JPAAddressRepository instance = new JPAAddressRepository();
//		instance.updateAddress(a);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
	/**
	 * Test of deleteAddress method, of class JPAAddressRepository.
	 */
	@Test
	public void testDeleteAddress() {
		System.out.println("DELETE ADDRESS");
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		Address addr = new Address(Street, Area, ZipCode, City, Country);

		JPAAddressRepository repo = Persistence.getJPARepositoryFactory().
			getJPAAddressRepository();
		repo.saveAddress(addr);
		repo.deleteAddress(addr);
		List<Address> allAddress = repo.listAll();
		String result = "", expected = addr.toString();
		for (Address a : allAddress) {
			if (a.toString().equals(expected)) {
				result = a.toString();
			}
		}
		assertEquals(expected, result);
		fail("Address was successfully removed");
	}

	/**
	 * Test of listAll method, of class JPAAddressRepository.
	 */
	@Test
	public void testListAll() {
		System.out.println("LIST ALL ADDRESSES");
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		Address addr1 = new Address(Street, Area, ZipCode, City, Country);
		Address addr2 = new Address("Street2", "Area2", "ZipCode2", "City2", "Country2");

		JPAAddressRepository repo = Persistence.getJPARepositoryFactory().
			getJPAAddressRepository();
		repo.saveAddress(addr1);
		repo.saveAddress(addr2);
		List<Address> result = Collections.emptyList();
		List<Address> expected = repo.listAll();

		assertEquals(expected, result);
		fail("Fails if expected==null");
	}

}
