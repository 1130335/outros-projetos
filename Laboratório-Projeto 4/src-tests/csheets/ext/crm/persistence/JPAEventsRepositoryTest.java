/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;
import csheets.ext.crm.domain.Contact;
import csheets.ext.crm.domain.Event;
import csheets.ext.crm.domain.Person;
import java.awt.Color;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class JPAEventsRepositoryTest {

    JPAPersonRepository instance_person;
    JPAEventsRepository instance;
    Person p;
    Event event_aux;

    public JPAEventsRepositoryTest() {
        
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addEvent method, of class JPAEventsRepository.
     */
    @Test
    public void testAddEvent() {
        System.out.println("addEvent");
        instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        instance = Persistence.getJPARepositoryFactory().getJPAEventsRepository();
         p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        event_aux = new Event(p, new Date(), 0, 0, "event1", null, new Date(),0,0);
        instance.addEvent(event_aux);
        Event expResult=event_aux;
        Event result = instance.listAllEvents().get(0);
        instance.delete(event_aux);
        instance_person.delete(p);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateEvents method, of class JPAEventsRepository.
     */
    @Test
    public void testUpdateEvents() {
        System.out.println("updateEvents");
        instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        instance = Persistence.getJPARepositoryFactory().getJPAEventsRepository();
        p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        event_aux = new Event(p, new Date(), 0, 0, "event1", null, new Date(),0,0);
        instance.addEvent(event_aux);
        String expResult="Calendar";
        Calenda c = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance_calendar = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance_calendar.addCalendar(c);
        event_aux.setCalendar(c);
        instance.update(event_aux);
        String result=event_aux.getCalendar().getName();
        instance.delete(event_aux);
        instance_calendar.delete(c);
        instance_person.delete(p);
        assertEquals(expResult, result);
    }


    

    /**
     * Test of AllEventsByContact method, of class JPAEventsRepository.
     */
    @Test
    public void testAllEventsByContact() {
        System.out.println("AllEventsByContact");
        instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        instance = Persistence.getJPARepositoryFactory().getJPAEventsRepository();
         p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        event_aux =new Event(p, new Date(), 0, 0, "event1", null, new Date(),0,0);
        instance.addEvent(event_aux);
        List<Event> result = instance.AllEventsByContact(p);
        instance.delete(event_aux);
        instance_person.delete(p);
        assertEquals(event_aux, result.get(0));
        
    }

    

    /**
     * Test of AllEventsOfCalendar method, of class JPAEventsRepository.
     */
    @Test
    public void testAllEventsOfCalendar() {
        System.out.println("AllEventsOfCalendar");
        instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        instance = Persistence.getJPARepositoryFactory().getJPAEventsRepository();
        p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        Calenda c = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance_calendar = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance_calendar.addCalendar(c);
        event_aux = new Event(p, new Date(), 0, 0, "event1", null, new Date(),0,0);
        instance.addEvent(event_aux);
        
        
        
        List<Event> expResult= new ArrayList<>();
        expResult.add(event_aux);
        List<Event> result = instance.AllEventsOfCalendar(c);
        
        instance.delete(event_aux);
        instance_calendar.delete(c);
        instance_person.delete(p);
        assertEquals(expResult, result);
      
    }

    

}
