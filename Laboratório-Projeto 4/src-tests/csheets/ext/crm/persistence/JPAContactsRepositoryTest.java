/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.crm.persistence;
//
//import csheets.ext.crm.domain.Person;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import static org.junit.Assert.assertEquals;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
///**
// *
// * @author Miguel
// */
//public class JPAContactsRepositoryTest {
//
//	public JPAContactsRepositoryTest() {
//	}
//
//	@BeforeClass
//	public static void setUpClass() {
//	}
//
//	@AfterClass
//	public static void tearDownClass() {
//	}
//
//	@Before
//	public void setUp() {
//	}
//
//	@After
//	public void tearDown() {
//	}
//
//	/**
//	 * Test of addContact method, of class JPAContactsRepository.
//	 */
//	@Test
//	public void testAddContact() {
//		System.out.println("addContact");
//		Person c = new Person("Miguel", "Teixeira");
//		JPAPersonRepository repo = new JPAPersonRepository();
//		repo.addContact(c);
//		List<Person> contacts = repo.AllContacts();
//		String result = "";
//		String expected = c.toString();
//		for (Person contact : contacts) {
//			if ((contact.toString()).equals(c.toString())) {
//				result = contact.toString();
//			}
//		}
//
//		assertEquals(expected, result);
//
//	}
//
//	/**
//	 * Test of editContact method, of class JPAContactsRepository.
//	 */
//	@Test
//	public void testEditContact() {
//		System.out.println("editContact");
//		Contacts c = new Contacts("Miguel", "Teixeira", "/caminho");
//		Contacts c_new = new Contacts("Migueli", "Teixeira", "/caminho");
//		JPAContactsRepository repo = new JPAContactsRepository();
//		repo.addContact(c);
//		repo.editContact(c_new, 0);
//		JPAContactsRepository repo2 = new JPAContactsRepository();
//		List<Contacts> contacts = repo2.AllContacts();
//		String result = contacts.get(0).toString();
//
//		String expected = c_new.toString();
//
//		assertEquals(expected, result);
//
//	}
//
//	/**
//	 * Test of removeContact method, of class JPAContactsRepository.
//	 */
//	@Test
//	public void testRemoveContact() {
//		System.out.println("removeContact");
//		Contacts c = new Contacts("Miguel", "Teixeira", "/caminho");
//		JPAContactsRepository repo = new JPAContactsRepository();
//		repo.addContact(c);
//		repo.removeContact(0);
//		List<Contacts> contacts = repo.AllContacts();
//		String result = "";
//		String expected = c.toString();
//		for (Contacts contact : contacts) {
//			if ((contact.toString()).equals(c.toString())) {
//				result = contact.toString();
//			}
//		}
//
//		assertEquals(expected, result);
//	}
//
//	/**
//	 * Test of AllContacts method, of class JPAContactsRepository.
//	 */
//	@Test
//	public void testAllContacts() {
//		System.out.println("AllContacts");
//		JPAContactsRepository instance = new JPAContactsRepository();
//		List<Contacts> expResult = null;
//		List<Contacts> result = instance.AllContacts();
//		assertEquals(expResult, result);
//
//	}
//
//}
