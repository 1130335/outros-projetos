/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.persistence;

import csheets.ext.crm.domain.Calenda;

import csheets.ext.crm.domain.Person;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class JPACalendarRepositoryTest {

    public JPACalendarRepositoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addCalendar method, of class JPACalendarRepository.
     */
    @Test
    public void testAddCalendar() {
        System.out.println("addCalendar");
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        Calenda expResult = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance.addCalendar(expResult);
        Calenda result = instance.getCalendarByIndex(0);
        instance.removeCalendar(expResult);
         instance_person.removeContact(0);
        assertEquals(expResult, result);

    }

    /**
     * Test of updateCalendar method, of class JPACalendarRepository.
     */
    @Test
    public void testUpdateCalendar() {
        System.out.println("updateCalendar");
        String expResult = "Calendar new";
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        Calenda calendar = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance.addCalendar(calendar);
        calendar.setName("Calendar new");
        String result = calendar.getName();
        instance.removeCalendar(calendar);
        instance_person.removeContact(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCalendarByIndex method, of class JPACalendarRepository.
     */
    @Test
    public void testGetCalendarByIndex() {
        System.out.println("getCalendarByIndex");
        int index = 0;
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        Calenda expResult = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance.addCalendar(expResult);
        Calenda result = instance.getCalendarByIndex(index);
        instance.removeCalendar(expResult);
        instance_person.removeContact(0);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeCalendar method, of class JPACalendarRepository.
     */
    @Test
    public void testRemoveCalendar() {
        System.out.println("removeCalendar");
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        Calenda calendar = new Calenda("Calendar", "new", p, Color.yellow);
        JPACalendarRepository instance = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        instance.addCalendar(calendar);
        instance.removeCalendar(calendar);
        instance_person.removeContact(0);
        int expResult = 0;
        int result = instance.AllCalendars().size();
        assertEquals(expResult, result);

    }

    /**
     * Test of AllCalendars method, of class JPACalendarRepository.
     */
    @Test
    public void testAllCalendars() {
        System.out.println("AllCalendars");
        JPACalendarRepository instance = new JPACalendarRepository();
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        List<Calenda> expResult = new ArrayList<>();
        Calenda calendar1 = new Calenda("Calendar", "new", p, Color.yellow);
        instance.addCalendar(calendar1);
        Calenda calendar2 = new Calenda("Calendar2", "new", p, Color.yellow);
        instance.addCalendar(calendar2);
        expResult.add(calendar1);
        expResult.add(calendar2);
        List<Calenda> result = instance.AllCalendars();
        instance.removeCalendar(calendar1);
        instance.removeCalendar(calendar2);
        instance_person.removeContact(0);
        assertEquals(expResult, result);

    }

    /**
     * Test of AllCalendarsByContact method, of class JPACalendarRepository.
     */
    @Test
    public void testAllCalendarsByContact() {
        System.out.println("AllCalendarsByContact");
        JPACalendarRepository instance = Persistence.getJPARepositoryFactory().getJPACalendarRepository();
        JPAPersonRepository instance_person = Persistence.getJPARepositoryFactory().getJPAPersonRepository();
        Person p = new Person("Daniel", "Oliveira");
        instance_person.addContact(p);
        List<Calenda> expResult = new ArrayList<>();
        Calenda calendar = new Calenda("Calendar", "new", p, Color.yellow);
        instance.addCalendar(calendar);
        Calenda calendar2 = new Calenda("Calendar2", "new", p, Color.yellow);
        instance.addCalendar(calendar2);
        expResult.add(calendar);
        expResult.add(calendar2);
        List<Calenda> result = instance.AllCalendarsByContact(p);
        instance.removeCalendar(calendar);
        instance.removeCalendar(calendar2);
        instance_person.removeContact(0);
        assertEquals(expResult, result);

    }

}
