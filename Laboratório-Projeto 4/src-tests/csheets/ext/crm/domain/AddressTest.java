/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author André Garrido - 1101598
 */
public class AddressTest {

	public AddressTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getid method, of class Address.
	 */
	/**
	 * Test of getStreet method, of class Address.
	 */
	@Test
	public void testGetStreet() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("Get Street Name");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String result = addr.getStreet();
		assertEquals(Street, result);
	}

	/**
	 * Test of getArea method, of class Address.
	 */
	@Test
	public void testGetArea() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("Get Area Name");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String result = addr.getArea();
		assertEquals(Area, result);
	}

	/**
	 * Test of getZipCode method, of class Address.
	 */
	@Test
	public void testGetZipCode() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("Get ZipCode");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String result = addr.getZipCode();
		assertEquals(ZipCode, result);
	}

	/**
	 * Test of getCity method, of class Address.
	 */
	@Test
	public void testGetCity() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("Get City Name");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String result = addr.getCity();
		assertEquals(City, result);
	}

	/**
	 * Test of getCountry method, of class Address.
	 */
	@Test
	public void testGetCountry() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("Get Country Name");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String result = addr.getCountry();
		assertEquals(Country, result);
	}

	/**
	 * Test of toString method, of class Address.
	 */
	@Test
	public void testToString() {
		String Street = "Street1";
		String Area = "Area1";
		String ZipCode = "4405-555";
		String City = "City1";
		String Country = "Country1";
		System.out.println("TO STRING TEST");
		Address addr = new Address(Street, Area, ZipCode, City, Country);
		String expResult = "Street: " + Street + "Area: " + Area + "ZipCode: " + ZipCode + "City: " + City + "Country: " + Country;
		String result = addr.toString();
		assertEquals(expResult, result);
	}

}
