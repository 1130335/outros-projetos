/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Miguel
 */
public class PhoneNumberTest {

	public PhoneNumberTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getPersonalNumber method, of class PhoneNumber.
	 */
	@Test
	public void testGetPersonalNumber() {
		int PersonalNumber = 919490172;
		int HomeNumber = 220151525;
		int JobPersonalNumber = 939490172;
		int JobNumber = 229490172;

		System.out.println("Get PhoneNumbers List");
		PhoneNumber allPhones = new PhoneNumber(PersonalNumber, HomeNumber, JobPersonalNumber, JobNumber);
		int result = allPhones.getPersonalNumber();
		assertEquals(PersonalNumber, result);
	}

	/**
	 * Test of getJobNumber method, of class PhoneNumber.
	 */
	@Test
	public void testGetJobNumber() {
		int PersonalNumber = 919490172;
		int HomeNumber = 220151525;
		int JobPersonalNumber = 939490172;
		int JobNumber = 229490172;

		System.out.println("Get PhoneNumbers List");
		PhoneNumber allPhones = new PhoneNumber(PersonalNumber, HomeNumber, JobPersonalNumber, JobNumber);
		int result = allPhones.getJobNumber();
		assertEquals(JobNumber, result);

	}

	/**
	 * Test of getJobPersonalNumber method, of class PhoneNumber.
	 */
	@Test
	public void testGetJobPersonalNumber() {
		int PersonalNumber = 919490172;
		int HomeNumber = 220151525;
		int JobPersonalNumber = 939490172;
		int JobNumber = 229490172;

		System.out.println("Get PhoneNumbers List");
		PhoneNumber allPhones = new PhoneNumber(PersonalNumber, HomeNumber, JobPersonalNumber, JobNumber);
		int result = allPhones.getJobPersonalNumber();
		assertEquals(JobPersonalNumber, result);
	}

	/**
	 * Test of getHomeNumber method, of class PhoneNumber.
	 */
	@Test
	public void testGetHomeNumber() {
		int PersonalNumber = 919490172;
		int HomeNumber = 220151525;
		int JobPersonalNumber = 939490172;
		int JobNumber = 229490172;

		System.out.println("Get PhoneNumbers List");
		PhoneNumber allPhones = new PhoneNumber(PersonalNumber, HomeNumber, JobPersonalNumber, JobNumber);
		int result = allPhones.getHomeNumber();
		assertEquals(HomeNumber, result);
	}
}
