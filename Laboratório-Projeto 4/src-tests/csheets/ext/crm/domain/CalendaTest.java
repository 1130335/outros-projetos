/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import csheets.ext.crm.persistence.JPACalendarRepository;
import java.awt.Color;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class CalendaTest {

    public CalendaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setColor method, of class Calenda.
     */
    @Test
    public void testSetColor() {
        System.out.println("setColor");
        Color color = new Color(255, 255, 255);
        Calenda instance = new Calenda("calendar", "new", new Contact(), color);
        Color expected = new Color(0, 0, 0);
        instance.setColor(expected);
        Color result = instance.getColor();
        assertEquals(expected, result);
    }

    /**
     * Test of setDescription method, of class Calenda.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "old";
        Calenda instance = new Calenda("calendar", description, new Contact(), new Color(255, 255, 255));
        String expected = "new";
        instance.setDescription(expected);
        String result = instance.getDescription();
        assertEquals(expected, result);
    }

    /**
     * Test of setName method, of class Calenda.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "old";
        Calenda instance = new Calenda(name, "new", new Contact(), new Color(255, 255, 255));
        String expected = "new";
        instance.setName(expected);
        String result = instance.getName();
        assertEquals(expected, result);
    }

    

    /**
     * Test of getColor method, of class Calenda.
     */
    @Test
    public void testGetColor() {
        System.out.println("getColor");
        Color expected = new Color(255, 255, 255);
        Calenda instance = new Calenda("calendar", "new", new Contact(), expected);
        Color result = instance.getColor();
        assertEquals(expected, result);
    }

    /**
     * Test of getDescription method, of class Calenda.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        String expected = "old";
        Calenda instance = new Calenda("calendar", expected, new Contact(), new Color(255, 255, 255));
        String result = instance.getDescription();
        assertEquals(expected, result);
    }

    /**
     * Test of getName method, of class Calenda.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expected = "old";
        Calenda instance = new Calenda(expected, "new", new Contact(), new Color(255, 255, 255));
        String result = instance.getName();
        assertEquals(expected, result);
    }

    /**
     * Test of getContact method, of class Calenda.
     */
    @Test
    public void testGetContactPerson() {
        System.out.println("getContactPerson");
        Person expected_person = new Person("new","person");
        Calenda instance = new Calenda("Calendar", "new", expected_person, new Color(255, 255, 255));
        Person result_person = (Person) instance.getContact();
        assertEquals(expected_person, result_person);
       
    }
    
    /**
     * Test of getContact method, of class Calenda.
     */
    @Test
    public void testGetContactCompany() {
        System.out.println("getContactCompany");
        Company expected_company = new Company("new",new byte[2]);
        Calenda instance = new Calenda("Calendar", "new", expected_company, new Color(255, 255, 255));
        Company result_company= (Company) instance.getContact();
        assertEquals(expected_company, result_company);
       
    }
    
    /**
     * Test of getId method, of class Calenda.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        JPACalendarRepository jpa= new JPACalendarRepository();
        Calenda instance = new Calenda();
        jpa.add(instance);
        Long expResult = (long) 1;
        Long result = instance.getId();
        assertEquals(expResult, result);
        
    }


   

}
