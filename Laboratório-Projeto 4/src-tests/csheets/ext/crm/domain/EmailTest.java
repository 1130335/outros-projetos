/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.domain;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Miguel
 */
public class EmailTest {

	public EmailTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getEmail1 method, of class Email.
	 */
	@Test
	public void testGetEmail() {
		String email1 = "miguel@hotmail.com";
		String email2 = "miguel@gmail.com";
		String email3 = "miguel@sapo.pt";
		System.out.println("Get Emails List");
		Email allEmails = new Email(email1, email2, email3);
		String result = allEmails.getEmail1();
		assertEquals(email1, result);
	}

	/**
	 * Test of getEmail2 method, of class Email.
	 */
	@Test
	public void testGetEmail2() {
		String email1 = "miguel@hotmail.com";
		String email2 = "miguel@gmail.com";
		String email3 = "miguel@sapo.pt";
		System.out.println("Get Emails List");
		Email allEmails = new Email(email1, email2, email3);
		String result = allEmails.getEmail2();
		assertEquals(email2, result);
	}

	/**
	 * Test of getEmail3 method, of class Email.
	 */
	@Test
	public void testGetEmail3() {
		String email1 = "miguel@hotmail.com";
		String email2 = "miguel@gmail.com";
		String email3 = "miguel@sapo.pt";
		System.out.println("Get Emails List");
		Email allEmails = new Email(email1, email2, email3);
		String result = allEmails.getEmail3();
		assertEquals(email3, result);
	}

}
