/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.crm.alerts;

import java.sql.Timestamp;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo silva
 */
public class AlertTest {
   

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}
        
        /**
     * Test of getId method, of class Alert.
     */
        @Test
        public void createTimestamp(){
            System.out.println("Creating Process");
            Date date=new Date("22/08/2015");
            int hours=2;
            int minutes=23;
            date.setHours(hours);
            date.setMinutes(minutes);
            Alerts a=new Alerts();
            Timestamp time=new Timestamp(date.getTime());
            Timestamp expresult=time;
            Timestamp result=a.createTimestamp(date, hours, minutes);
            assertEquals(expresult,result);
        }
        
          /**
     * Test of validDate method, of class Alert.
     */
    @Test
    public void testValidateDate() {
      
            System.out.println("validDate");
         
            String dateDay = "20/10/2005";
    
            Alerts a = new Alerts();
            //mes e dia invertidos devido a formataçao da data
            Date expResult = new Date("10/20/2005");
            Date result = a.validateDate(dateDay);

            assertEquals(expResult, result);
            
     
        
    }
}
