/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.Email.domain;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.ext.CellExtension;
import csheets.ext.ipc.domain.SimplerCell;
import javax.mail.AuthenticationFailedException;
import javax.swing.JOptionPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class contains test for the class Email (for email communication) and
 * also test the EmailConfigurationProperties class.
 *
 * @author coelho
 */
public class EmailTest {

    private static final String[] emai_info = {"teste@gmail.com", "password", "smtp.gmail.com", "587"};

    private final Cell[][] active_cells;
    
    public EmailTest() {
        this.active_cells = new Cell[1][1];
        this.active_cells[0][0] = new SimplerCell(new Address(1, 1), "Email Test Message");
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of the import and export methods in one method. First exports the
     * String[], so he can then import the file and compare them.
     */
    @Test
    public void testImportEmailPropertiesFile() {
        System.out.println("importEmailPropertiesFile");
        Email instance = new Email();
        String[] expResult = emai_info;

        instance.exportEmailPropertiesFile(emai_info);

        String[] result = instance.importEmailPropertiesFile();
        
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of sendTestEmail method in EmailSMTP class.
     */
    @Test
    public void testSendTestEmail() throws AuthenticationFailedException{
        System.out.println("importEmailPropertiesFile");
        Email instance = new Email();
        
        String[] teste = new String[4];
        teste[0] = "Pedro.n.coelho.5@gmail.com";
                //JOptionPane.showInputDialog("Enter Email");
        teste[1] = JOptionPane.showInputDialog("Enter Password");
        teste[2] = "smtp.gmail.com";
                //JOptionPane.showInputDialog("Enter Host");
        teste[3] = "587";
                //JOptionPane.showInputDialog("Enter Port");
        
        
        instance.exportEmailPropertiesFile(teste);

        teste = instance.importEmailPropertiesFile();
        
        instance.sendTestEmail(active_cells);
        
        String[] clean = {"","","",""};
        instance.exportEmailPropertiesFile(clean);
    }

}
