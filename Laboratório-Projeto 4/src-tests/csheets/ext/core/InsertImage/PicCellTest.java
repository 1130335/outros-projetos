/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.InsertImage;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pedro
 */
public class PicCellTest {

	public PicCellTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of hasImage method, of class PicCell.
	 */
	@Test
	public void testSetAndHasImage() {
		System.out.println("SetAndHasImage");
		Workbook wb = new Workbook(3);
		Spreadsheet spread = wb.getSpreadsheet(0);
		Cell cell = spread.getCell(0, 0);

		PicCell pic = new PicCell(cell);
		File image = new File("src-resources\\csheets\\res\\img\\sheet.gif");
		pic.setUserImage(image);

		boolean expResult = true;
		boolean result = pic.hasImage();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getImage method, of class PicCell.
	 */
	@Test
	public void testGetImage() {
		System.out.println("SetAndHasImage");
		Workbook wb = new Workbook(3);
		Spreadsheet spread = wb.getSpreadsheet(0);
		Cell cell = spread.getCell(0, 0);

		PicCell pic = new PicCell(cell);
		File image = new File("src-resources\\csheets\\res\\img\\sheet.gif");
		pic.setUserImage(image);
		boolean expResult = true;
		boolean result;
		if (pic.getImage() != null) {
			result = true;
		} else {
			result = false;
		}
		assertEquals(expResult, result);

	}

}
