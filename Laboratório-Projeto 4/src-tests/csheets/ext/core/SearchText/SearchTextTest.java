/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.core.SearchText;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author André Garrido - 1101598
 */
public class SearchTextTest {

	public SearchTextTest() {

	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getSearchResult method, of class SearchText.
	 *
	 * @throws csheets.core.formula.compiler.FormulaCompilationException
	 */
	@Test
	public void testGetSearchResult1() throws FormulaCompilationException {
		System.out.println("Search Result 1");

		Workbook wb = new Workbook(2);
		Spreadsheet ss = wb.getSpreadsheet(0);
		Cell cl;

		cl = ss.getCell(3, 3);
		cl.setContent("");
		cl = ss.getCell(1, 1);
		cl.setContent("tesit");
		cl = ss.getCell(2, 2);
		cl.setContent("test");
		cl = ss.getCell(2, 4);
		cl.setContent("testass");
		cl = ss.getCell(2, 1);
		cl.setContent("test");

		String text = "test";
		SearchText instance = new SearchText();
		instance.Search(ss, text);

		ArrayList<Cell> result = instance.getSearchResult();
		int expResult = 2;
		int nresults = result.size();
		assertEquals(expResult, nresults);
	}

	/**
	 * Test of getSearchResult method, of class SearchText.
	 *
	 * @throws csheets.core.formula.compiler.FormulaCompilationException
	 */
	@Test
	public void testGetSearchResult2() throws FormulaCompilationException {
		System.out.println("Search Result 2");

		Workbook wb = new Workbook(2);
		Spreadsheet ss = wb.getSpreadsheet(0);
		Cell cl;

		cl = ss.getCell(3, 3);
		cl.setContent("a");
		cl = ss.getCell(1, 1);
		cl.setContent("tatata");
		cl = ss.getCell(2, 2);
		cl.setContent("tttaaaa");
		cl = ss.getCell(2, 4);
		cl.setContent("");
		cl = ss.getCell(2, 1);
		cl.setContent("tata");

		String text = "(ta)+";
		SearchText instance = new SearchText();
		instance.Search(ss, text);

		ArrayList<Cell> result = instance.getSearchResult();
		int expResult = 2;
		int nresults = result.size();
		assertEquals(expResult, nresults);
	}

	/**
	 * Test of getSearchResult method, of class SearchText.
	 *
	 * @throws csheets.core.formula.compiler.FormulaCompilationException
	 */
	@Test
	public void testGetSearchResult3() throws FormulaCompilationException {
		System.out.println("Search Result 3");

		Workbook wb = new Workbook(2);
		Spreadsheet ss = wb.getSpreadsheet(0);
		Cell cl;

		cl = ss.getCell(3, 3);
		cl.setContent("a");
		cl = ss.getCell(1, 1);
		cl.setContent("teste1");
		cl = ss.getCell(2, 2);
		cl.setContent("teste2");
		cl = ss.getCell(2, 4);
		cl.setContent("");
		cl = ss.getCell(2, 1);
		cl.setContent("ttest");

		String text = "hello";
		SearchText instance = new SearchText();
		instance.Search(ss, text);

		ArrayList<Cell> result = instance.getSearchResult();
		int expResult = 0;
		int nresults = result.size();
		assertEquals(expResult, nresults);
		fail("This test is supposed to fail");
	}
}
