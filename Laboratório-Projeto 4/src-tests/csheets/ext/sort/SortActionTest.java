/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.sort;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.ext.ipc.domain.SimplerCell;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author IvoTeixeira
 */
public class SortActionTest {

	private Cell[][] mat;
	private Cell[][] mat2;

	public SortActionTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of allCellsAreEmpty method, of class SortAction.
	 */
	@Test
	public void testAllCellsAreEmpty() {
		System.out.println("allCellsAreEmpty");
		Cell[][] range = {};
		SortAction instance = new SortAction();
		boolean expResult = true;
		boolean result = instance.allCellsAreEmpty(range);
		assertEquals(expResult, result);

	}

	/**
	 * Test of verifyCollumnCount method, of class SortAction.
	 */
	@Test
	public void testVerifyCollumnCount() {
		System.out.println("verifyCollumnCount");

		Cell[][] range = null;
		SortAction instance = new SortAction();
		boolean expResult = true;
		boolean result = instance.verifyCollumnCount(range);
		assertEquals(expResult, result);
		fail("The test case is a prototype.");
	}

	/**
	 * Test of sortAscending method, of class SortAction.
	 */
	@Test
	public void testSortAscending() throws Exception {
		System.out.println("sortAscending");
		SimplerCell c1 = new SimplerCell(new Address(0, 0), "1");
		SimplerCell c2 = new SimplerCell(new Address(0, 0), "2");
		mat = new Cell[2][1];
		mat[0][0] = c2;
		mat[0][1] = c1;
		mat2 = new Cell[2][1];
		mat2[0][0] = c1;
		mat2[0][1] = c2;
		SortAction instance = new SortAction();
		instance.sortAscending(mat);
		boolean flag = false;
		boolean expResult = true;
		if (mat.equals(mat2)) {
			flag = true;
		}
		assertEquals(flag, expResult);

	}

	/**
	 * Test of sortDescending method, of class SortAction.
	 */
	@Test
	public void testSortDescending() throws Exception {
		System.out.println("sortDescending");
		Cell[][] range = null;
		SortAction instance = new SortAction();
		instance.sortDescending(range);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

}
