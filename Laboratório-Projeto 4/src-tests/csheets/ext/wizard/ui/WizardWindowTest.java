/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.wizard.ui;

import csheets.core.Cell;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.ui.ctrl.UIController;
import javax.swing.JCheckBox;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniela Maia
 */
public class WizardWindowTest {

	private UIController uiController;

	public WizardWindowTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testEnabledDisabledCheckboxExpression() throws FormulaCompilationException {
		JCheckBox checkbox = new JCheckBox("Show Abstract Syntax Tree");
		String result;

		Cell cell = uiController.getActiveCell();
		String formulaSrc = uiController.getActiveCell().getFormula().
			toString();
		Expression expression = FormulaCompiler.getInstance().
			compile(cell, formulaSrc).getExpression();
		if (checkbox.isSelected()) {
			result = expression.toString();
		} else {
			result = null;
		}
		Assert.assertEquals(result, null);

	}

}
