/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.wizard;

import csheets.ext.wizard.ui.WizardAction;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author 1130709
 */
public class WizardTest {
    

    @Test
    public void testCheckStringParamIntroduced() {
        System.out.println("checkStringParamIntroduced");
        String stringParamIntroduzidos = "=MIN(NUMERIC;NUMERIC)";
        String newParam = "50";
        int size = 2;
        int index = 1;
        String expResult = "=MIN(NUMERIC;50)";
        String result = WizardAction.checkStringParamIntroduced(stringParamIntroduzidos, newParam, size, index);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testVerifyParamChanged() {
        System.out.println("verifyParamChanged");
        String paramIntroduced = "=MIN(NUMERIC;NUMERIC)";
        String input = "20";
        int size = 2;
        int index = 0;
        boolean expResult = true;
        boolean result = WizardAction.verifyParamChanged(paramIntroduced, input, size, index);
        assertEquals(expResult, result);
    }

    
}
