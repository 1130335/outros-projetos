/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_03;

import csheets.ext.ipc.impl_01.*;
import csheets.core.Cell;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.domain.SimplerCell;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Rita
 */
public class AutomaticUpdateControllerTest {

	public AutomaticUpdateControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of searchAvailableInstances method, of class
	 * AutomaticUpdateController.
	 */
	@Test
	public void testSearchAvailableInstances() {
		System.out.println("searchAvailableInstances");
		AutomaticUpdateController instance = new AutomaticUpdateController();
		instance.startUDPServer();
		instance.searchAvailableInstances();
		int n_conections = 0;
		n_conections = instance.getAvailableInstances().size();
		assertEquals(instance.getAvailableInstances().size(), n_conections);
	}

	/**
	 * Test of getAvailableInstances method, of class AutomaticUpdateController.
	 */
	@Test
	public void testGetAvailableInstances() throws UnknownHostException {
		System.out.println("getAvailableInstances");
		AutomaticUpdateController instance = new AutomaticUpdateController();
		instance.startUDPServer();
		instance.searchAvailableInstances();
		List<NetworkInstance> expResult = new ArrayList<>();
		expResult.add(new NetworkInstance(InetAddress.getLocalHost().
			getHostAddress(), 40001));
		List<NetworkInstance> result = instance.getAvailableInstances();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getCells method, of class AutomaticUpdateController.
	 */
	@Test
	public void testGetCells() {
		System.out.println("getCells");
		AutomaticUpdateController instance = new AutomaticUpdateController();
		List<Cell> expResult = null;
		List<Cell> result = instance.getCells();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getReceivedCells method, of class AutomaticUpdateController.
	 */
	@Test
	public void testGetReceivedCells() {
		System.out.println("getReceivedCells");
		AutomaticUpdateController instance = new AutomaticUpdateController();
		List<SimplerCell> expResult = null;
		List<SimplerCell> result = instance.getReceivedCells();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
	}

}
