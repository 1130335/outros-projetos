/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.ui.findnetworkworkbooks;

import csheets.ext.ipc.protocols.NetworkFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniela Maia
 */
public class SearchNetworkWorkbooksMenuTest {

	public SearchNetworkWorkbooksMenuTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void clientUDP() {
		Thread t = new Thread(NetworkFactory.getUDPServerImpl());
		t.start();
		try {
			t.join();
		} catch (InterruptedException ex) {
			Logger.getLogger(SearchNetworkWorkbooksMenu.class.
				getName()).
				log(Level.SEVERE, null, ex);
		}

	}

}
