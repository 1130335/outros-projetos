/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.Encryption;

import csheets.core.Address;
import csheets.ext.ipc.domain.SimplerCell;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.*;

/**
 *
 * @author Pedro
 */
public class EncryptionTest {

    public EncryptionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    /**
     * Test that test encryption and decryption on a Cell
     * Compares the original Cell with the decrypted Cell and sees if theyre equal.
     */
    @Test
    public void EncryptionAndDecryptionCell() {
        
        Address address = new Address(5, 5);
        SimplerCell inicial_cell = new SimplerCell(address, "5");
        SimplerCell final_cell = null;
        
        try {
            SecretKey key = AESEncryption.generateAESKey(128);
            byte[] encrypted_cell = AESEncryption.encrypt(inicial_cell, key);
            final_cell = AESEncryption.decrypt(encrypted_cell, key);
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IOException | IllegalBlockSizeException | BadPaddingException | ClassNotFoundException ex) {
            Logger.getLogger(EncryptionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertEquals(final_cell, inicial_cell);
        
    }
}
