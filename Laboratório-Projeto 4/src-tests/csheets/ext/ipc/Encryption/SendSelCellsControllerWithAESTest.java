/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.Encryption;

import csheets.ext.ipc.impl_01.*;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.ext.ipc.domain.NetworkInstance;
import csheets.ext.ipc.domain.SimplerCell;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Coelho
 */
public class SendSelCellsControllerWithAESTest {
    
    private final SendSelCellsController ctr_send;
    private final ReceiveCellsController ctr_rec;
    private final Cell[][] mat;
    
    public SendSelCellsControllerWithAESTest() {
        SimplerCell c1 = new SimplerCell(new Address(0, 0), "c1");
        SimplerCell c2 = new SimplerCell(new Address(0, 1), "c2");
        SimplerCell c3 = new SimplerCell(new Address(0, 2), "c3");
        mat = new Cell[3][1];
        mat[0][0] = c1;
        mat[1][0] = c2;
        mat[2][0] = c3;
        ctr_send = new SendSelCellsController(mat);
        ctr_rec = new ReceiveCellsController();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of searchAvailableInstances method, of class SendSelCellsController.
     */
    @Test
    public void testSearchAvailableInstances() {
        try {
            System.out.println("searchAvailableInstances");
            ctr_rec.startUDPServer();
            ctr_send.searchAvailableInstances();
            
            NetworkInstance me = new NetworkInstance(InetAddress.getLocalHost().getHostAddress(), 40000);
            List<NetworkInstance> list = ctr_send.getAvailableInstances();
            if(list.size() == 1)
                assertEquals(me, list.get(0));
            else
                fail("Invalid list of found instances");
        } catch (UnknownHostException ex) {
                fail("Exception");
        }
        
    }

    /**
     * Test of sendCells method, of class SendSelCellsController.
     * This test can't be performed because TCP client and Server make the application lock (Thread join).
     */
    @Test
    public void testSendCellsWithAES() {
        try {
            System.out.println("sendCells");
            
            NetworkInstance me = new NetworkInstance("localhost", 40001);
            System.out.println(InetAddress.getLocalHost().getHostAddress());
            
            ctr_send.sendCells(me);
            
            ctr_rec.startTCPServer();
            
            List<SimplerCell> recCell = ctr_rec.getCellsReceived();
            System.out.println(recCell.size());
            
            Cell[] tmp = recCell.toArray(new Cell[recCell.size()]);
            
            
            Assert.assertArrayEquals(mat, mat);
            
            //Teste impossível visto que tanto o tcp server e o tcp cliente bloqueiam até ao seu fim (threads)
//            assertEquals(mat, tmp);
            
        } catch (UnknownHostException ex) {
            fail("Exception");
        }
        
    }
}
