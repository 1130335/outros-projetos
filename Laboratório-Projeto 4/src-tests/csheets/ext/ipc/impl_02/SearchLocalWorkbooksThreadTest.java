/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.ipc.impl_02;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo
 */
public class SearchLocalWorkbooksThreadTest {

	public SearchLocalWorkbooksThreadTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of run method, of class SearchLocalWorkbooksThread. Foram criados
	 * neste package teste 4 instancias de workbooks, caso sejam encontradas as
	 * 4 instancias, é retornado true, senão false e o metodo de pesquisa
	 * falhou.
	 */
	@Test
	public void testProcuraFicheiros() {
		System.out.println("Teste da procura de ficheiros");
		SearchLocalWorkbooksThread instance = new SearchLocalWorkbooksThread();
		File f = new File(".\\src-tests\\csheets\\ext\\ipc\\impl_02");
		List<File> empty_list = new ArrayList<>();
		instance.
			procuraRecursiva(f, empty_list, new FileFilterImpl(new String[]{".cls"}));
		int expResult = 4;
		int result = empty_list.size();
		System.out.
			println("Resultado, quantidade de ficheiros encontrados = " + result);
		System.out.println("Resultado esperado = " + expResult);

		System.out.println("Ficheiros Encontrados");
		for (File fich : empty_list) {

			System.out.println(fich.getName());
		}
		org.junit.Assert.assertEquals(result, expResult);

	}

}
