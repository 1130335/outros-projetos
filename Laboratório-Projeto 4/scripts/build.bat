@echo off
rmdir /S /Q  ..\tmp-build 
mkdir ..\tmp-build
REM del /S /Q ..\tmp-build\*.class >nul
dir /B /S /O:N ..\src\*.java > c.lst
dir /B /S /O:N ..\build\generated-sources\antlr-output\*.java >> c.lst
mkdir META-INF
copy ..\src\META-INF\persistence.xml META-INF\persistence.xml
mkdir ..\tmp-build\META-INF
copy ..\src\META-INF\persistence.xml ..\tmp-build\META-INF\persistence.xml
javac -cp ..\src;..\lib\antlr-3.5.2-complete.jar;..\lib\javax.mail.jar;..\lib\eclipselink.jar;..\lib\org.eclipse.persistence.jpa.jpql_2.5.1.v20130918-f2b9fc5.jar;..\lib\bsh-2.0b4.jar;..\lib\itextpdf-5.5.6.jar;..\lib\javax.persistence_2.1.0.v201304241213.jar -d ..\tmp-build @c.lst %1 %2 %3
rmdir /S /Q META-INF
del c.lst