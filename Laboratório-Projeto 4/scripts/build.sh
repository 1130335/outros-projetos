#!/bin/sh
if [ -e ../tmp-build ]   &&  [ -f ../tmp-build ]
then  
	rm ../tmp-build 
fi 

if [ ! -d ../tmp-build ]   
then 
	mkdir ../tmp-build 
fi 

mkdir META-INF
cp ../src/META-INF/persistence.xml META-INF/persistence.xml
mkdir ../tmp-build/META-INF
cp ../src/META-INF/persistence.xml ../tmp-build/META-INF/persistence.xml
find ../src ../build/generated-sources/antlr-output -name "*.java" | xargs javac -cp ../src:`find ../lib -iname \*.jar | tr "\n" ":"` -d ../tmp-build
rm -rf META-INF

