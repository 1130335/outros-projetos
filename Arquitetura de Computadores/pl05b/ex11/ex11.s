/* Exercício 11 */

.section .data
	var1:
	.int 30
	var2:
	.int 10

.section .text

.global _start

_start:
	movl $0, %ecx
	movl $0, %edx
	movl var1, %ecx
	movl var2, %edx
	
	cmpl %ecx, %edx
	jg jmp_e_maior
	jl jmp_e_menor
	
jmp_e_maior:
	movl var2, %ebx
	jmp fim

jmp_e_menor:
	movl var1, %ebx
	jmp fim

fim:	
	movl $1, %eax
	int $0x80
