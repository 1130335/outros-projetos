.section .data
num:
.int 4

.section .text
.global _start
_start:

movl $0, %edx
movl $0, %eax
movl $0, %ebx
movl $0, %ecx

movl num, %eax
movl $2, %ebx

div %ebx

cmpl %edx, %ecx
jg jmp_e_par
jl jmp_e_impar

jmp_e_par:
	movl $1, %ebx
	jmp fim
	
jmp_e_impar:
	movl $0, %ebx
	jmp fim
	
fim:

movl $1, %eax
int $0x80 
