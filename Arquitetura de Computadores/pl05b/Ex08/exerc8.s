.section .data
base:
.int 10
altura:
.int 2

.section .text
.global _start
_start:

movl $0, %eax
movl $0, %ebx
movl $0, %ecx

movl base, %ebx
movl altura, %eax

mull %ebx

movl $2, %ecx

div %ecx

movl %eax, %ebx
movl $1, %eax
int $0x80 
