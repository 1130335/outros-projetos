/* Exercício 13: alínea a) */

.section .data

.section .text
.global _start
	i:
	.int 3
	j:
	.int 3

_start:
	movl $0, %eax
	movl $0, %ebx
	movl $0, %ecx
	
	movl i, %eax
	movl j, %ecx
	
	cmpl %eax, %ecx
	je jmp_e_igual
	jl jmp_e_menor
	jg jmp_e_maior
	
jmp_e_igual:
	subl %eax, %ecx
	movl %ecx, %ebx
	jmp fim

jmp_e_menor:
	addl %eax, %ecx
	movl %ecx, %ebx
	jmp fim

jmp_e_maior:
	addl %eax, %ecx
	movl %ecx, %ebx
	jmp fim


fim:
	movl $1, %eax
	int $0x80
