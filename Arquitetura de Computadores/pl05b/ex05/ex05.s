/* Exercício 5 */

.section .data
	n1:
	.int 5
	n2:
	.int 5
	n3:
	.int 2	

.section .text

.global _start

_start:
	movl $0, %eax
	movl $0, %ecx
	movl $0, %edx
	
	movl n1, %eax
	movl n2, %ecx
	movl n3, %edx
	
	addl %eax, %ecx
	subl %edx, %ecx
	
	movl %ecx, %ebx
	movl $1, %eax

	int $0x80
