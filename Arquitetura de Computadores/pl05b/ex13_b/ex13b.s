#PL4 - Exercício 13b: 

.section .data
	i:
		.int -2
	j:
		.int -7
		
.section .text
.global _start
_start:
	movl i, %eax
	movl j, %esi

	cmpl %eax, %esi
	je ret_if
	jl ret_if
	jg ret_else

ret_if:
	addl $1, %eax
	jmp exit

ret_else:
	addl $1, %esi
	jmp exit

exit:
	imull %esi
	movl %eax, %ebx
	movl $1, %eax
	int $0x80
