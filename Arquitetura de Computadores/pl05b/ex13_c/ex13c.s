#PL4 - Exercício 13c

.section .data
	i:
		.int 4
	j:
		.int 45
		
.section .text
.global _start
_start:
    movl i, %edx
    movl j, %ecx
    
    cmpl %ecx, %edx #i>=j
    jg ret_if
    je ret_if
    jl ret_else
    
ret_if:
    #h = i+j
    movl %edx, %eax
    addl %ecx, %eax
    
    #g = i+1
    movl %edx, %ebx
    addl $1, %ebx
    jmp exit
    
ret_else:
    #g = i + j + 2
    movl %edx, %ebx
    addl %ecx, %ebx
    addl $2, %ebx
    
    #h  = i * j
    movl %ecx, %eax
    imull %edx
    jmp exit

exit:
    #r = h/g
    movl $0, %edx
    idivl %ebx
    movl %eax, %ebx
	
	movl $1, %eax
	int $0x80
