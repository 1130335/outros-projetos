.section .data
g_dividendo:
.int 25
g_divisor:
.int 4

.section .text
.global _start
_start:

movl $0, %edx
movl $0, %eax
movl $0, %ebx

movl g_dividendo, %eax
movl g_divisor, %ebx

div %ebx

movl %edx, %ebx
movl $1, %eax
int $0x80 
