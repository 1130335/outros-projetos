/* Exercício 6 */

.section .data
	g_dividendo:
	.int 10
	g_divisor:
	.int 5

.section .text

.global _start

_start:
	movl $0, %edx
	movl $0, %ebx
	movl $0, %eax
	
	movl g_dividendo, %eax
	movl g_divisor, %ebx
	idiv %ebx
		
	movl %eax, %ebx
	movl $1, %eax

	int $0x80
