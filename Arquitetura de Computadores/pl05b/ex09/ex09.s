/* Exercício 9 */

.section .data
	var1:
	.int 450
	var2:
	.int 550

.section .text

.global _start

_start:
	movl $0, %ecx
	movl $0, %edx
	
	movl var1, %ecx
	movl var2, %edx
	
	addl %ecx, %edx

	jc output_com_carry
	movl $0, %ebx
	jmp fim
	
output_com_carry:
	movl $1, %ebx
	
	jo output_com_overflow
	movl $0, %ebx
	jmp fim
output_com_overflow:
	movl $1, %ebx
	
fim:
	movl $1, %eax
	int $0x80
