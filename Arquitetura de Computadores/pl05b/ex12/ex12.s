/* Exercício 12 */

.section .data
	v1:
	.int 7
	v2:
	.int 257
	v3:
	.int 6
	
.section .text
.global _start

_start:
	movl $0, %eax
	movl $0, %ebx
	movl $0, %ecx
	movl $0, %edx
	
	movl v1, %eax
	movl v2, %ecx
	movl v3, %edx
	
	cmpl %eax, %ecx
	jg jmp_2_maior_1
	jl jmp_1_maior_2
	
jmp_1_maior_2:
	cmpl %eax, %edx
	jg jmp_3_maior_1
	jl jmp_1_maior_3
	
jmp_1_maior_3:
	movl v1, %ebx
	jmp fim
	
jmp_3_maior_1:
	movl v3, %ebx
	jmp fim
	
jmp_2_maior_1:
	cmpl %ecx, %edx
	jg jmp_3_maior_2
	jl jmp_2_maior_3

jmp_2_maior_3:
	movl v2, %ebx
	jmp fim
	
jmp_3_maior_2:
	movl v3, %ebx
	jmp fim
	
fim:
	movl $1, %eax
	int $0x80
