# ia32impl.s
#
# Implementacao em assembly dos comandos do interpretador.
#
# Este ficheiro contem a implementacao das intrucoes suportados pelo interpretador.
#
# Autor: Alexandre Braganca (ATB)
# Autor: Nuno Pereira (NAP)
# Data: Nov 11, 2014

# Dados globais
.data
#	para ja nao temos nada

		
# codigo
.text
# definicao das funcoes e etiquetas
.global func_mov_const_eax, func_mov_const_ebx, func_mov_const_ecx, func_mov_const_edx, func_mov_const_cl
.global func_mov_eax_ebx, func_mov_eax_ecx, func_mov_eax_edx
.global func_shl_cl_eax, func_shl_cl_ebx, func_shl_cl_edx
.global func_add_const_eax, func_add_const_ebx, func_add_const_ecx, func_add_const_edx
.global func_add_eax_eax, func_add_eax_ebx, func_add_eax_ecx, func_add_ebx_eax, func_add_ebx_ebx, func_add_ebx_ecx, func_add_ebx_edx, func_add_ecx_eax, func_add_ecx_ebx, func_add_ecx_ecx, func_add_ecx_edx, func_add_edx_eax, func_add_edx_ebx, func_add_edx_ecx, func_add_edx_edx 
.global func_adc_const_eax, func_adc_const_ebx, func_adc_const_ecx, func_adc_const_edx, global func_adc_eax_eax, func_adc_eax_ebx, func_adc_eax_ecx, func_adc_ebx_eax, func_adc_ebx_ebx, func_adc_ebx_ecx, func_adc_ebx_edx, func_adc_ecx_eax, func_adc_ecx_ebx, func_adc_ecx_ecx, func_adc_ecx_edx, func_adc_edx_eax, func_adc_edx_ebx, func_adc_edx_ecx, func_adc_edx_edx 
.global func_sub_const_eax, func_sub_const_ebx, func_sub_const_ecx, func_sub_const_edx, global func_sub_eax_eax, func_sub_eax_ebx, func_sub_eax_ecx, func_sub_ebx_eax, func_sub_ebx_ebx, func_sub_ebx_ecx, func_sub_ebx_edx, func_sub_ecx_eax, func_sub_ecx_ebx, func_sub_ecx_ecx, func_sub_ecx_edx, func_sub_edx_eax, func_sub_edx_ebx, func_sub_edx_ecx, func_sub_edx_edx 
.global func_sub_const_eax, func_sbb_const_ebx, func_sbb_const_ecx, func_sbb_const_edx, global func_sbb_eax_eax, func_sbb_eax_ebx, func_sbb_eax_ecx, func_sbb_ebx_eax, func_sbb_ebx_ebx, func_sbb_ebx_ecx, func_sbb_ebx_edx, func_sbb_ecx_eax, func_sbb_ecx_ebx, func_sbb_ecx_ecx, func_sbb_ecx_edx, func_sbb_edx_eax, func_sbb_edx_ebx, func_sbb_edx_ecx, func_sbb_edx_edx 
.global func_imul_eax_eax, func_imul_ebx_eax, func_imul_ecx_eax, func_imul_edx_eax, func_imul_const_eax, func_imul_const_ebx, func_imul_const_ecx, func_imul_const_edx
.global func_idiv_eax_eax, func_idiv_ebx_eax, func_idiv_ecx_eax, func_idiv_edx_eax, func_idiv_const_eax, func_idiv_const_ebx, func_idiv_const_ecx, func_idiv_const_edx
.global func_incl_eax, func_incl_ebx, func_incl_ecx, func_incl_edx
.global func_decl_eax, func_decl_ebx, func_decl_ecx, func_decl_edx
.global func_cmp_const_eax, func_cmp_const_ebx, func_cmp_const_ecx, func_cmp_const_edx
.global func_test_const_eax, func_test_const_ebx, func_test_const_ecx, func_test_const_edx
.global func_sal_cl_eax, func_sal_cl_ebx, func_sal_cl_edx
.global func_sar_cl_eax, func_sar_cl_ebx, func_sar_cl_edx
.global func_rcl_cl_eax, func_rcl_cl_ebx, func_rcl_cl_edx
.global func_rcr_cl_eax, func_rcr_cl_ebx, func_rcr_cl_edx
.global func_rol_cl_eax, func_rol_cl_ebx, func_rol_cl_edx
.global func_ror_cl_eax, func_ror_cl_ebx, func_ror_cl_edx
.global func_negl_eax, func_negl_ebx, func_negl_ecx, func_negl_edx
.global func_notl_eax, func_notl_ebx, func_notl_ecx, func_notl_edx
.global func_and_const_eax, func_and_const_ebx, func_and_const_ecx, func_and_const_edx, func_and_eax_ebx, func_and_eax_ecx, func_and_eax_edx, func_and_ebx_eax, func_and_ebx_ecx, func_and_ebx_edx, func_and_ecx_eax, func_and_ecx_ebx, func_and_ecx_edx, func_and_edx_eax, func_and_edx_ebx, func_and_edx_ecx
.global func_or_const_eax, func_or_const_ebx, func_or_const_ecx, func_or_const_edx, func_or_eax_ebx, func_or_eax_ecx, func_or_eax_edx, func_or_ebx_eax, func_or_ebx_ecx, func_or_ebx_edx, func_or_ecx_eax, func_or_ecx_ebx, func_or_ecx_edx, func_or_edx_eax, func_or_edx_ebx, func_or_edx_ecx
.global func_xor_const_eax, func_xor_const_ebx, func_xor_const_ecx, func_xor_const_edx, func_xor_eax_ebx, func_xor_eax_ecx, func_xor_eax_edx, func_xor_ebx_eax, func_xor_ebx_ecx, func_xor_ebx_edx, func_xor_ecx_eax, func_xor_ecx_ebx, func_xor_ecx_edx, func_xor_edx_eax, func_xor_edx_ebx, func_xor_edx_ecx
.global func_shrl_cl_eax, func_shrl_cl_ebx, func_shrl_cl_edx
.global func_pushl_eax, func_pushl_ebx, func_pushl_ecx, func_pushl_edx
.global func_popl_eax, func_popl_ebx, func_popl_ecx, func_popl_edx
.global funcao_executa

# A funcao funcao_executa() vai executar o codigo que se encontra na respetiva
# etiqueta da instrucao a executar.
funcao_executa:

	pushl %ebp
	movl %esp, %ebp
	
	# o primeiro parametro e o endereco da instrucao a executar
	movl 8(%ebp), %esi
	
repor_registos:
	movl reg_eax, %eax
	movl reg_ebx, %ebx
	movl reg_ecx, %ecx
	movl reg_edx, %edx
	
executa_instrucao:	
	# colocamos em esi o endereco da instrucao a executar
	# movl $func_mov_const_eax, %esi
	jmp *%esi
	
fim_executa_instrucao:

guardar_flags:
	pushf
	popl %edi
	movl %edi, reg_eflags

guardar_registos:
	movl %eax, reg_eax
	movl %ebx, reg_ebx
	movl %ecx, reg_ecx
	movl %edx, reg_edx
	
funcao_executa_fim:	
	movl %ebp, %esp
	popl %ebp
	ret

# MOV	
func_mov_const_eax:
	movl arg_constante, %eax
	jmp fim_executa_instrucao

func_mov_const_ebx:
	movl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_mov_const_ecx:
	movl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_mov_const_edx:
	movl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_mov_const_cl:
	movb arg_constante, %cl
	jmp fim_executa_instrucao
	
func_mov_eax_ebx:
	movl %eax, %ebx
	jmp fim_executa_instrucao

func_mov_eax_ecx:
	movl %eax, %ecx
	jmp fim_executa_instrucao
	
func_mov_eax_edx:
	movl %eax, %edx
	jmp fim_executa_instrucao
	
func_mov_ebx_eax:
	movl %ebx, %eax
	jmp fim_executa_instrucao
	
func_mov_ebx_ecx:
	movl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_mov_ebx_edx:
	movl %ebx, %edx
	jmp fim_executa_instrucao
	
func_mov_ecx_eax:
	movl %ecx, %eax
	jmp fim_executa_instrucao
	
func_mov_ecx_ebx:
	movl %ecx, %ebx
	jmp fim_executa_instrucao
	
func_mov_ecx_edx:
	movl %ecx, %edx
	jmp fim_executa_instrucao
	
func_mov_edx_eax:
	movl %edx, %eax
	jmp fim_executa_instrucao
	
func_mov_edx_ebx:
	movl %edx, %ebx
	jmp fim_executa_instrucao
	
func_mov_edx_ecx:
	movl %edx, %ecx
	jmp fim_executa_instrucao
	
	
# ADD
func_add_const_eax:
	addl arg_constante, %eax
	jmp fim_executa_instrucao

func_add_const_ebx:
	addl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_add_const_ecx:
	addl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_add_const_edx:
	addl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_add_eax_eax:
	addl %eax, %eax
	jmp fim_executa_instrucao

func_add_eax_ebx:
	addl %eax, %ebx
	jmp fim_executa_instrucao
	
func_add_eax_ecx:
	addl %eax, %ecx
	jmp fim_executa_instrucao
	
func_add_eax_edx:
	addl %eax, %edx
	jmp fim_executa_instrucao
	
func_add_ebx_eax:
	addl %ebx, %eax
	jmp fim_executa_instrucao
	
func_add_ebx_ebx:
	addl %ebx, %ebx
	jmp fim_executa_instrucao
	
func_add_ebx_ecx:
	addl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_add_ebx_edx:
	addl %ebx, %edx
	jmp fim_executa_instrucao
	
func_add_ecx_eax:
	addl %ecx, %eax
	jmp fim_executa_instrucao
	
func_add_ecx_ebx:
	addl %ecx, %ebx
	jmp fim_executa_instrucao
	
func_add_ecx_ecx:
	addl %ecx, %ecx
	jmp fim_executa_instrucao
	
func_add_ecx_edx:
	addl %ecx, %edx
	jmp fim_executa_instrucao
	
func_add_edx_eax:
	addl %edx, %eax
	jmp fim_executa_instrucao
	
func_add_edx_ebx:
	addl %edx, %ebx
	jmp fim_executa_instrucao
	
func_add_edx_ecx:
	addl %edx, %ecx
	jmp fim_executa_instrucao
	
func_add_edx_edx:
	addl %edx, %edx
	jmp fim_executa_instrucao
	
# ADC
func_adc_const_eax:
	adcl arg_constante, %eax
	jmp fim_executa_instrucao

func_adc_const_ebx:
	adcl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_adc_const_ecx:
	adcl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_adc_const_edx:
	adcl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_adc_eax_eax:
	adcl %eax, %eax
	jmp fim_executa_instrucao

func_adc_eax_ebx:
	adcl %eax, %ebx
	jmp fim_executa_instrucao
	
func_adc_eax_ecx:
	adcl %eax, %ecx
	jmp fim_executa_instrucao
	
func_adc_eax_edx:
	adcl %eax, %edx
	jmp fim_executa_instrucao
	
func_adc_ebx_eax:
	adcl %ebx, %eax
	jmp fim_executa_instrucao
	
func_adc_ebx_ebx:
	adcl %ebx, %ebx
	jmp fim_executa_instrucao
	
func_adc_ebx_ecx:
	adcl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_adc_ebx_edx:
	adcl %ebx, %edx
	jmp fim_executa_instrucao
	
func_adc_ecx_eax:
	adcl %ecx, %eax
	jmp fim_executa_instrucao
	
func_adc_ecx_ebx:
	adcl %ecx, %ebx
	jmp fim_executa_instrucao
	
func_adc_ecx_ecx:
	adcl %ecx, %ecx
	jmp fim_executa_instrucao
	
func_adc_ecx_edx:
	adcl %ecx, %edx
	jmp fim_executa_instrucao
	
func_adc_edx_eax:
	adcl %edx, %eax
	jmp fim_executa_instrucao
	
func_adc_edx_ebx:
	adcl %edx, %ebx
	jmp fim_executa_instrucao
	
func_adc_edx_ecx:
	adcl %edx, %ecx
	jmp fim_executa_instrucao
	
func_adc_edx_edx:
	adcl %edx, %edx
	jmp fim_executa_instrucao
	
# SUB
func_sub_const_eax:
	subl arg_constante, %eax
	jmp fim_executa_instrucao

func_sub_const_ebx:
	subl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_sub_const_ecx:
	subl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_sub_const_edx:
	subl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_sub_eax_eax:
	subl %eax, %eax
	jmp fim_executa_instrucao

func_sub_eax_ebx:
	subl %eax, %ebx
	jmp fim_executa_instrucao
	
func_sub_eax_ecx:
	subl %eax, %ecx
	jmp fim_executa_instrucao
	
func_sub_eax_edx:
	subl %eax, %edx
	jmp fim_executa_instrucao
	
func_sub_ebx_eax:
	subl %ebx, %eax
	jmp fim_executa_instrucao
	
func_sub_ebx_ebx:
	subl %ebx, %ebx
	jmp fim_executa_instrucao
	
func_sub_ebx_ecx:
	subl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_sub_ebx_edx:
	subl %ebx, %edx
	jmp fim_executa_instrucao
	
func_sub_ecx_eax:
	subl %ecx, %eax
	jmp fim_executa_instrucao
	
func_sub_ecx_ebx:
	subl %ecx, %ebx
	jmp fim_executa_instrucao
	
func_sub_ecx_ecx:
	subl %ecx, %ecx
	jmp fim_executa_instrucao
	
func_sub_ecx_edx:
	subl %ecx, %edx
	jmp fim_executa_instrucao
	
func_sub_edx_eax:
	subl %edx, %eax
	jmp fim_executa_instrucao
	
func_sub_edx_ebx:
	subl %edx, %ebx
	jmp fim_executa_instrucao
	
func_sub_edx_ecx:
	subl %edx, %ecx
	jmp fim_executa_instrucao
	
func_sub_edx_edx:
	subl %edx, %edx
	jmp fim_executa_instrucao
	
# SBB
func_sbb_const_eax:
	sbbl arg_constante, %eax
	jmp fim_executa_instrucao

func_sbb_const_ebx:
	sbbl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_sbb_const_ecx:
	sbbl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_sbb_const_edx:
	sbbl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_sbb_eax_eax:
	sbbl %eax, %eax
	jmp fim_executa_instrucao

func_sbb_eax_ebx:
	sbbl %eax, %ebx
	jmp fim_executa_instrucao
	
func_sbb_eax_ecx:
	sbbl %eax, %ecx
	jmp fim_executa_instrucao
	
func_sbb_eax_edx:
	sbbl %eax, %edx
	jmp fim_executa_instrucao
	
func_sbb_ebx_eax:
	sbbl %ebx, %eax
	jmp fim_executa_instrucao
	
func_sbb_ebx_ebx:
	sbbl %ebx, %ebx
	jmp fim_executa_instrucao
	
func_sbb_ebx_ecx:
	sbbl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_sbb_ebx_edx:
	sbbl %ebx, %edx
	jmp fim_executa_instrucao
	
func_sbb_ecx_eax:
	sbbl %ecx, %eax
	jmp fim_executa_instrucao
	
func_sbb_ecx_ebx:
	sbbl %ecx, %ebx
	jmp fim_executa_instrucao
	
func_sbb_ecx_ecx:
	sbbl %ecx, %ecx
	jmp fim_executa_instrucao
	
func_sbb_ecx_edx:
	sbbl %ecx, %edx
	jmp fim_executa_instrucao
	
func_sbb_edx_eax:
	sbbl %edx, %eax
	jmp fim_executa_instrucao
	
func_sbb_edx_ebx:
	sbbl %edx, %ebx
	jmp fim_executa_instrucao
	
func_sbb_edx_ecx:
	sbbl %edx, %ecx
	jmp fim_executa_instrucao
	
func_sbb_edx_edx:
	sbbl %edx, %edx
	jmp fim_executa_instrucao
	
# MUL
func_imul_eax_eax:
	imul %eax, %eax
	jmp fim_executa_instrucao

func_imul_ebx_eax:
	imul %ebx, %eax
	jmp fim_executa_instrucao
	
func_imul_ecx_eax:
	imul %ecx, %eax
	jmp fim_executa_instrucao
	
func_imul_edx_eax:
	imul %edx, %eax
	jmp fim_executa_instrucao
	
func_imul_const_eax:
	imul arg_constante, %eax
	jmp fim_executa_instrucao

func_imul_const_ebx:
	imul arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_imul_const_ecx:
	imul arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_imul_const_edx:
	imul arg_constante, %edx
	jmp fim_executa_instrucao
	
# DIV
func_idiv_eax_eax:
	idiv %eax, %eax
	jmp fim_executa_instrucao

func_idiv_ebx_eax:
	idiv %ebx, %eax
	jmp fim_executa_instrucao
	
func_idiv_ecx_eax:
	idiv %ecx, %eax
	jmp fim_executa_instrucao
	
func_idiv_edx_eax:
	idiv %edx, %eax
	jmp fim_executa_instrucao
	
func_idiv_const_eax:
	idiv arg_constante, %eax
	jmp fim_executa_instrucao

func_idiv_const_ebx:
	idiv arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_idiv_const_ecx:
	idiv arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_idiv_const_edx:
	idiv arg_constante, %edx
	jmp fim_executa_instrucao
	
# INC
	
func_incl_eax:
	incl %eax
	jmp fim_executa_instrucao

func_incl_ebx:
	incl %ebx
	jmp fim_executa_instrucao
	
func_incl_ecx:
	incl %ecx
	jmp fim_executa_instrucao
	
func_incl_edx:
	incl %edx
	jmp fim_executa_instrucao
	
# DEC
	
func_decl_eax:
	decl %eax
	jmp fim_executa_instrucao

func_decl_ebx:
	decl %ebx
	jmp fim_executa_instrucao
	
func_decl_ecx:
	decl %ecx
	jmp fim_executa_instrucao
	
func_decl_edx:
	decl %edx
	jmp fim_executa_instrucao
	
# CMP
	
func_cmp_const_eax:
	cmpl arg_constante, %eax
	jmp fim_executa_instrucao

func_cmp_const_ebx:
	cmpl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_cmp_const_ecx:
	cmpl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_cmp_const_edx:
	cmpl arg_constante, %edx
	jmp fim_executa_instrucao
	
# TEST
	
func_test_const_eax:
	testl arg_constante, %eax
	jmp fim_executa_instrucao

func_test_const_ebx:
	testl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_test_const_ecx:
	testl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_teste_const_edx:
	testl arg_constante, %edx
	jmp fim_executa_instrucao
	
# sall

func_sal_cl_eax:
	sall %cl, %eax
	jmp fim_executa_instrucao

func_sal_cl_ebx:
	sall %cl, %ebx
	jmp fim_executa_instrucao
	
func_sal_cl_edx:
	sall %cl, %edx
	jmp fim_executa_instrucao
	

# sarl

func_sar_cl_eax:
	sarl %cl, %eax
	jmp fim_executa_instrucao

func_sar_cl_ebx:
	sarl %cl, %ebx
	jmp fim_executa_instrucao
	
func_sar_cl_edx:
	sarl %cl, %edx
	jmp fim_executa_instrucao
	

# rcll

func_rcl_cl_eax:
	rcll %cl, %eax
	jmp fim_executa_instrucao

func_rcl_cl_ebx:
	rcll %cl, %ebx
	jmp fim_executa_instrucao
	
func_rcl_cl_edx:
	rcll %cl, %edx
	jmp fim_executa_instrucao
	

# rcrl

func_rcr_cl_eax:
	rcrl %cl, %eax
	jmp fim_executa_instrucao

func_rcr_cl_ebx:
	rcrl %cl, %ebx
	jmp fim_executa_instrucao
	
func_rcr_cl_edx:
	rcrl %cl, %edx
	jmp fim_executa_instrucao
	
	
# roll

func_rol_cl_eax:
	roll %cl, %eax
	jmp fim_executa_instrucao

func_rol_cl_ebx:
	roll %cl, %ebx
	jmp fim_executa_instrucao
	
func_rol_cl_edx:
	roll %cl, %edx
	jmp fim_executa_instrucao
	
	
# rorl

func_ror_cl_eax:
	rorl %cl, %eax
	jmp fim_executa_instrucao

func_ror_cl_ebx:
	rorl %cl, %ebx
	jmp fim_executa_instrucao
	
func_ror_cl_edx:
	rorl %cl, %edx
	jmp fim_executa_instrucao
	
	
# negl

func_negl_eax:
	negl %eax
	jmp fim_executa_instrucao

func_negl_ebx:
	negl %ebx
	jmp fim_executa_instrucao
	
func_negl_ecx:
	negl %ecx
	jmp fim_executa_instrucao
	
func_negl_edx:
	negl %edx
	jmp fim_executa_instrucao
	
	
# notl

func_notl_eax:
	notl %eax
	jmp fim_executa_instrucao

func_notl_ebx:
	notl %ebx
	jmp fim_executa_instrucao
	
func_notl_ecx:
	notl %ecx
	jmp fim_executa_instrucao
	
func_notl_edx:
	notl %edx
	jmp fim_executa_instrucao
	
	
# AND
func_and_const_eax:
	andl arg_constante, %eax
	jmp fim_executa_instrucao

func_and_const_ebx:
	andl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_and_const_ecx:
	andl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_and_const_edx:
	andl arg_constante, %edx
	jmp fim_executa_instrucao
	
func_and_eax_ebx:
	andl %eax, %ebx
	jmp fim_executa_instrucao
	
func_and_eax_ecx:
	andl %eax, %ecx
	jmp fim_executa_instrucao
	
func_and_eax_edx:
	andl %eax, %edx
	jmp fim_executa_instrucao
	
func_and_ebx_eax:
	andl %ebx, %eax
	jmp fim_executa_instrucao
	
func_and_ebx_ecx:
	andl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_and_ebx_edx:
	andl %ebx, %edx
	jmp fim_executa_instrucao
	
func_and_ecx_eax:
	andl %ecx, %eax
	jmp fim_executa_instrucao
	
func_and_ecx_ebx:
	andl %ecx, %ebx
	jmp fim_executa_instrucao
	

func_and_ecx_edx:
	andl %ecx, %edx
	jmp fim_executa_instrucao
	
func_and_edx_eax:
	andl %edx, %eax
	jmp fim_executa_instrucao
	
func_and_edx_ebx:
	andl %edx, %ebx
	jmp fim_executa_instrucao
	
func_and_edx_ecx:
	andl %edx, %ecx
	jmp fim_executa_instrucao
	

# OR
func_or_const_eax:
	orl arg_constante, %eax
	jmp fim_executa_instrucao

func_or_const_ebx:
	orl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_or_const_ecx:
	orl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_or_const_edx:
	orl arg_constante, %edx
	jmp fim_executa_instrucao
	

func_or_eax_ebx:
	orl %eax, %ebx
	jmp fim_executa_instrucao
	
func_or_eax_ecx:
	orl %eax, %ecx
	jmp fim_executa_instrucao
	
func_or_eax_edx:
	orl %eax, %edx
	jmp fim_executa_instrucao
	
func_or_ebx_eax:
	orl %ebx, %eax
	jmp fim_executa_instrucao
	

func_or_ebx_ecx:
	orl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_or_ebx_edx:
	orl %ebx, %edx
	jmp fim_executa_instrucao
	
func_or_ecx_eax:
	orl %ecx, %eax
	jmp fim_executa_instrucao
	
func_or_ecx_ebx:
	orl %ecx, %ebx
	jmp fim_executa_instrucao
	

func_or_ecx_edx:
	orl %ecx, %edx
	jmp fim_executa_instrucao
	
func_or_edx_eax:
	orl %edx, %eax
	jmp fim_executa_instrucao
	
func_or_edx_ebx:
	orl %edx, %ebx
	jmp fim_executa_instrucao
	
func_or_edx_ecx:
	orl %edx, %ecx
	jmp fim_executa_instrucao
	
	
# XOR
func_xor_const_eax:
	xorl arg_constante, %eax
	jmp fim_executa_instrucao

func_xor_const_ebx:
	xorl arg_constante, %ebx
	jmp fim_executa_instrucao
	
func_xor_const_ecx:
	xorl arg_constante, %ecx
	jmp fim_executa_instrucao
	
func_xor_const_edx:
	xorl arg_constante, %edx
	jmp fim_executa_instrucao
	

func_xor_eax_ebx:
	xorl %eax, %ebx
	jmp fim_executa_instrucao
	
func_xor_eax_ecx:
	xorl %eax, %ecx
	jmp fim_executa_instrucao
	
func_xor_eax_edx:
	xorl %eax, %edx
	jmp fim_executa_instrucao
	
func_xor_ebx_eax:
	xorl %ebx, %eax
	jmp fim_executa_instrucao
	

func_xor_ebx_ecx:
	xorl %ebx, %ecx
	jmp fim_executa_instrucao
	
func_xor_ebx_edx:
	xorl %ebx, %edx
	jmp fim_executa_instrucao
	
func_xor_ecx_eax:
	xorl %ecx, %eax
	jmp fim_executa_instrucao
	
func_xor_ecx_ebx:
	xorl %ecx, %ebx
	jmp fim_executa_instrucao
	

func_xor_ecx_edx:
	xorl %ecx, %edx
	jmp fim_executa_instrucao
	
func_xor_edx_eax:
	xorl %edx, %eax
	jmp fim_executa_instrucao
	
func_xor_edx_ebx:
	xorl %edx, %ebx
	jmp fim_executa_instrucao
	
func_xor_edx_ecx:
	xorl %edx, %ecx
	jmp fim_executa_instrucao
	
	
# shll - o primeiro argumento é sempre o registo "cl". O segundo argumento é sempre um registo

func_shl_cl_eax:
	shll %cl, %eax
	jmp fim_executa_instrucao

func_shl_cl_ebx:
	shll %cl, %ebx
	jmp fim_executa_instrucao
	
func_shl_cl_edx:
	shll %cl, %edx
	jmp fim_executa_instrucao
	
	
# shrl 

func_shrl_cl_eax:
	shrl %cl, %eax
	jmp fim_executa_instrucao

func_shrl_cl_ebx:
	shrl %cl, %ebx
	jmp fim_executa_instrucao
	
func_shrl_cl_edx:
	shrl %cl, %edx
	jmp fim_executa_instrucao
	

#pushl
	
func_pushl_eax:
	pushl %eax
	jmp fim_executa_instrucao

func_pushl_ebx:
	pushl %ebx
	jmp fim_executa_instrucao
	
func_pushl_ecx:
	pushl %ecx
	jmp fim_executa_instrucao
	
func_pushl_edx:
	pushl %edx
	jmp fim_executa_instrucao
	
	
#popl
	
func_popl_eax:
	popl %eax
	jmp fim_executa_instrucao

func_popl_ebx:
	popl %ebx
	jmp fim_executa_instrucao
	
func_popl_ecx:
	popl %ecx
	jmp fim_executa_instrucao
	
func_popl_edx:
	popl %edx
	jmp fim_executa_instrucao
	
		
		
