/** @file parser.c
 *  @brief Implementacao do parser dos comandos do interpretador.
 *
 *  Este ficheiro ira conter as funcoes que implementam o parser 
 *  dos comandos do interpretador.
 *
 *  @author Alexandre Braganca (ATB)
 *  @author Nuno Pereira (NAP)
 *  @date Nov 11, 2014
 *  @bug Nenhum conhecido.
 */

/* -- Includes -- */
#include <string.h>
#include <stdlib.h> /* atoi */
#include "ia32state.h"
#include "ia32impl.h"
#include "commands.h"
#include "lexer.h"
#include "parser.h"
#include <stdio.h>

/* -- Implementacao -- */

/* Esta funcao tenta identificar se o comando e' um comando ou uma instrucao a ser
 * executada */
void *id_command(char parts[_MAX_CMD_PARTS][_MAX_CMD_BUFFER_SIZE], int n_parts, int* oper1, int* oper2)
{
	if (strcmp(parts[0], CMD_DISPLAY)==0) {
		return display;
	} else if (strcmp(parts[0], CMD_HISTORY)==0) {
		return history;
	} else if (strcmp(parts[0], CMD_TOP)==0) {
		return top;
	} else if (strcmp(parts[0], CMD_OPTION_BIN)==0) {
		return bin;
	} else if (strcmp(parts[0], CMD_OPTION_CLEAN)==0) {
		return clean;
	} else if (strcmp(parts[0], CMD_OPTION_DEC)==0) {
		return dec;
	} else if (strcmp(parts[0], CMD_OPTION_EXEC)==0) {
		return exec;
	} else if (strcmp(parts[0], CMD_OPTION_HEX)==0) {
		return hex;
	} else {
		printf("\nComando inválido!\n");
	}
	
	return 0;
}


/* Esta funcao retorna a "funcao" que vai executar a instrucao assembly identificada na string do comando (ja separado em partes) */
/* Esta funcao tambem trata de "preparar" os operandos da instrucao se for caso disso */
void *id_instruction(char parts[_MAX_CMD_PARTS][_MAX_CMD_BUFFER_SIZE], int n_parts, int* oper1, int* oper2) {
        if (strcmp(parts[0], INSTR_MOVL) == 0) {
            /* vamos descobrir os operandos */
            if (parts[1][0] == '%') {
                /* primeiro operando é um registo */
                if (strcmp(parts[1], REG_EAX) == 0) {
                    /* vamos ver o segundo parametro */
                    if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_mov_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_mov_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_mov_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_mov_ebx_eax;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_mov_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_mov_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_ECX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_mov_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_mov_ecx_ebx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_mov_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_EDX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_mov_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_mov_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_mov_edx_ecx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
                /* se o primeiro operando nao e' registo so pode ser constante */
                /* vamos colocar o valor da constante na variavel que o assembly usa */
                /* Isto deve ser feito de forma diferente para verificarmos possiveis erros */
                /* E' necessário suportar os formatos de escrita de valores constantes!! */
                int arg = atoi(parts[1]);
                arg_constante = arg;

                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_mov_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_mov_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_mov_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_mov_const_edx;
                } else if (strcmp(parts[2], REG_CL) == 0) {
                    return func_mov_const_cl;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }

        } else if (strcmp(parts[0], INSTR_SHLL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_shl_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_shl_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_shl_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }

        } else if (strcmp(parts[0], INSTR_ADDL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_add_eax_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_add_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_add_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_add_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_add_ebx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_add_ebx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_add_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_add_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_add_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_add_ecx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_add_ecx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_add_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_add_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_add_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_add_edx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_add_edx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_add_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_add_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_add_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_add_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_ADCL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_adc_eax_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_adc_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_adc_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_adc_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_adc_ebx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_adc_ebx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_adc_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_adc_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_adc_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_adc_ecx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_adc_ecx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_adc_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_adc_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_adc_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_adc_edx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_adc_edx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_adc_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_adc_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_adc_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_adc_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }

        } else if (strcmp(parts[0], INSTR_SUBL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sub_eax_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sub_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sub_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sub_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sub_ebx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sub_ebx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sub_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sub_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sub_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sub_ecx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sub_ecx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sub_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sub_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sub_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sub_edx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sub_edx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else {
                    printf("\nOperando inválido!\n");
                }
            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_sub_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_sub_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_sub_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_sub_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_SBBL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sbb_eax_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sbb_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sbb_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sbb_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sbb_ebx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sbb_ebx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sbb_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sbb_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sbb_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sbb_ecx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sbb_ecx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sbb_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_sbb_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_sbb_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_sbb_edx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_sbb_edx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_sbb_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_sbb_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_sbb_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_sbb_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_IMULL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_imul_eax_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_imul_ebx_eax;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_imul_ecx_eax;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_imul_edx_eax;
                } else {
                    printf("\nOperando inválido!\n");
                }
                
            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_imul_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_imul_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_imul_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_imul_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
		} else if (strcmp(parts[0], INSTR_IDIVL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_idiv_eax_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_idiv_ebx_eax;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_idiv_ecx_eax;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_idiv_edx_eax;
                } else {
                    printf("\nOperando inválido!\n");
                }
                
            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_idiv_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_idiv_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_idiv_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_idiv_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
		} else if (strcmp(parts[0], INSTR_INCL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_incl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_incl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_incl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_incl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				printf("\nOperando inválido!\n");
            }
		} else if (strcmp(parts[0], INSTR_DECL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_decl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_decl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_decl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_decl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				printf("\nOperando inválido!\n");
            }
            
		} else if (strcmp(parts[0], INSTR_CMPL) == 0) {

            if (parts[1][0] == '%') {
				int arg = atoi(parts[2]);
                arg_constante = arg;
				
                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_cmp_const_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    return func_cmp_const_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_cmp_const_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_cmp_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_cmp_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_cmp_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_cmp_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_cmp_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
		} else if (strcmp(parts[0], INSTR_TESTL) == 0) {

            if (parts[1][0] == '%') {
				int arg = atoi(parts[2]);
                arg_constante = arg;
				
                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_test_const_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
                    return func_test_const_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_test_const_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_test_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_test_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_test_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_test_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_test_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
		} else if (strcmp(parts[0], INSTR_SALL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_sal_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_sal_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_sal_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_SARL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_sar_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_sar_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_sar_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_RCLL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_rcl_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_rcl_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_rcl_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_RCRL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_rcr_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_rcr_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_rcr_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_ROLL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_rol_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_rol_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_rol_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_RORL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_ror_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_ror_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_ror_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_NEGL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_negl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_negl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_negl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_negl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				printf("\nOperando inválido!\n");
            } 
		} else if (strcmp(parts[0], INSTR_NOTL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_notl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_notl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_notl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_notl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				printf("\nOperando inválido!\n");
            } 
		} else if (strcmp(parts[0], INSTR_ANDL) == 0) {
            /* vamos descobrir os operandos */
            if (parts[1][0] == '%') {
                /* primeiro operando é um registo */
                if (strcmp(parts[1], REG_EAX) == 0) {
                    /* vamos ver o segundo parametro */
                    if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_and_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_and_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_and_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_and_ebx_eax;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_and_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_and_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_ECX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_and_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_and_ecx_ebx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_and_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_EDX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_and_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_and_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_and_edx_ecx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
				
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_and_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_and_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_and_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_and_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_ORL) == 0) {
            /* vamos descobrir os operandos */
            if (parts[1][0] == '%') {
                /* primeiro operando é um registo */
                if (strcmp(parts[1], REG_EAX) == 0) {
                    /* vamos ver o segundo parametro */
                    if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_or_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_or_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_or_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_or_ebx_eax;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_or_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_or_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_ECX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_or_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_or_ecx_ebx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_or_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_EDX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_or_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_or_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_or_edx_ecx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
				
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_or_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_or_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_or_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_or_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_XORL) == 0) {
            /* vamos descobrir os operandos */
            if (parts[1][0] == '%') {
                /* primeiro operando é um registo */
                if (strcmp(parts[1], REG_EAX) == 0) {
                    /* vamos ver o segundo parametro */
                    if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_xor_eax_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_xor_eax_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_xor_eax_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }

                } else if (strcmp(parts[1], REG_EBX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_xor_ebx_eax;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_xor_ebx_ecx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_xor_ebx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_ECX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_xor_ecx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_xor_ecx_ebx;
                    } else if (strcmp(parts[2], REG_EDX) == 0) {
                        return func_xor_ecx_edx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                } else if (strcmp(parts[1], REG_EDX) == 0) {

                    if (strcmp(parts[2], REG_EAX) == 0) {
                        return func_xor_edx_eax;
                    } else if (strcmp(parts[2], REG_EBX) == 0) {
                        return func_xor_edx_ebx;
                    } else if (strcmp(parts[2], REG_ECX) == 0) {
                        return func_xor_edx_ecx;
                    } else {
                        printf("\nOperando inválido!\n");
                    }
                }
            } else {
				
                int arg = atoi(parts[1]);
                arg_constante = arg;

                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_xor_const_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_xor_const_ebx;
                } else if (strcmp(parts[2], REG_ECX) == 0) {
                    return func_xor_const_ecx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_xor_const_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_SHRL) == 0) {
            /* primeiro operando é sempre o %cl */
            if (strcmp(parts[1], REG_CL) == 0) {
                /* vamos ver o segundo parametro */
                if (strcmp(parts[2], REG_EAX) == 0) {
                    return func_shrl_cl_eax;
                } else if (strcmp(parts[2], REG_EBX) == 0) {
                    return func_shrl_cl_ebx;
                } else if (strcmp(parts[2], REG_EDX) == 0) {
                    return func_shrl_cl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }
            }
        } else if (strcmp(parts[0], INSTR_POPL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_popl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_popl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_popl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_popl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				printf("\nOperando inválido!\n");
            }
            
		} else if (strcmp(parts[0], INSTR_PUSHL) == 0) {

            if (parts[1][0] == '%') {

                if (strcmp(parts[1], REG_EAX) == 0) {
					return func_pushl_eax;
                } else if (strcmp(parts[1], REG_EBX) == 0) {
					return func_pushl_ebx;
                } else if (strcmp(parts[1], REG_ECX) == 0) {
                    return func_pushl_ecx;
                } else if (strcmp(parts[1], REG_EDX) == 0) {
                    return func_pushl_edx;
                } else {
                    printf("\nOperando inválido!\n");
                }

            } else {
				int arg = atoi(parts[1]);
                arg_constante = arg;
                
                return func_pushl_const;
            }
            
		}
		
        return 0;
    }
