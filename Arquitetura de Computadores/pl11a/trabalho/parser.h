/** @file parser.h
 *  @brief Definicoes para o parser dos comandos do interpretador.
 *
 *  Cabecalhos de funcoes e outras definicoes necessarias para a 
 *  implementacao do parser dos comandos do interpretador.
 *
 *  @author Alexandre Braganca (ATB)
 *  @author Nuno Pereira (NAP)
 *  @date Nov 11, 2014
 *  @bug Nenhum conhecido.
 */

#ifndef PARSER_H_
#define PARSER_H_

/** @defgroup lexer Lexer/Parser.
 *  Parser dos comandos do interpretador
 *   
 *  @{
 */
 
/* -- definicoes das strings dos comandos -- */

/** string do comando display */
#define CMD_DISPLAY "display"

/** string do comando top */
#define CMD_TOP "top"

/** string do comando history */
#define CMD_HISTORY "history"

/* -- definicoes das strings das opcoes -- */
 
/** string da opcao bin */
#define CMD_OPTION_BIN "bin"
/** string da opcao hex */
#define CMD_OPTION_HEX "hex"
/** string da opcao dec */
#define CMD_OPTION_DEC "dec"
/** string da opcao clean */
#define CMD_OPTION_CLEAN "clean"
/** string da opcao exec */
#define CMD_OPTION_EXEC "exec"

/* -- definicoes das strings das instrucoes -- */

/** string da intrucao movl */
#define INSTR_MOVL "movl"
/** string da intrucao shll */
#define INSTR_SHLL "shll"
/** string da intrucao addl */
#define INSTR_ADDL "addl"
/** string da instrucao adcl */
#define INSTR_ADCL "adcl"
/** string da instrucao subl */
#define INSTR_SUBL "subl"
/** string da instrucao sbbl */
#define INSTR_SBBL "sbbl"
/** string da instrucao imull */
#define INSTR_IMULL "imull"
/** string da instrucao idivl */
#define INSTR_IDIVL "idivl"
/** string da instrucao incl */
#define INSTR_INCL "incl"
/** string da instrucao decl */
#define INSTR_DECL "decl"
/** string da instrucao cmpl */
#define INSTR_CMPL "cmpl"
/** string da instrucao testl */
#define INSTR_TESTL "testl"
/** string da instrucao sall */
#define INSTR_SALL "sall"
/** string da instrucao sarl */
#define INSTR_SARL "sarl"
/** string da instrucao rcll */
#define INSTR_RCLL "rcll"
/** string da instrucao rcrl */
#define INSTR_RCRL "rcrl"
/** string da instrucao roll */
#define INSTR_ROLL "roll"
/** string da instrucao rorl */
#define INSTR_RORL "rorl"
/** string da instrucao negl */
#define INSTR_NEGL "negl"
/** string da instrucao notl */
#define INSTR_NOTL "notl"
/** string da instrucao andl */
#define INSTR_ANDL "andl"
/** string da instrucao orl */
#define INSTR_ORL "orl"
/** string da instrucao xorl */
#define INSTR_XORL "xorl"
/** string da intrucao shrl */
#define INSTR_SHRL "shrl"
/** string da intrucao popl */
#define INSTR_POPL "popl"
/** string da intrucao pushl */
#define INSTR_PUSHL "pushl"

/* -- definicoes das strings dos registos -- */

/** string do registo eax */
#define REG_EAX "%eax"
/** string do registo ebx */
#define REG_EBX "%ebx"
/** string do registo ecx */
#define REG_ECX "%ecx"
/** string do registo edx */
#define REG_EDX "%edx"
/** string do registo cl */
#define REG_CL "%cl"


/** @brief Tenta identificar o comando como um comando a ser
 *         executado (e nao como uma instrucao), dado um vetor que contem as varias partes de um comando.
 *         Esta funcao deve devolver a "funcao" (endereco da funcao) que vai executar o comando que foi identificado.
 *  @param parts Vetor com as varias partes de um comando; este vetor e' obtido
 *         atraves da divisao da string de comando em partes (ver funcao split())
 *  @param n_parts Numero de elementos do vector
 *  @param oper1 Valor do primeiro operando do comando identificado (caso exista e seja necessario/util devolver)
 *  @param oper2 Valor do segundo operando do comando identificado (caso exista e seja necessario/util devolver)
 *  @return apontador para a funcao que vai executar o comando identificado.
 *  @see split()
 */
void *id_command(char parts[_MAX_CMD_PARTS][_MAX_CMD_BUFFER_SIZE], int n_parts, int* oper1, int* oper2);

/** @brief Esta funcao retorna a "funcao" (endereco da funcao) que vai executar a instrucao assembly
           identificada na string do comando (ja separado em partes).
           Esta funcao tambem trata de "preparar" os operandos da instrucao 
           se for caso disso.
 *  @param parts Vetor com as varias partes de um comando; este vetor e' obtido
 *         atraves da divisao da string de comando em partes (ver funcao split())
 *  @param n_parts Numero de elementos do vector
 *  @param oper1 Valor do primeiro operando da instrucao identificada (caso exista e seja necessario/util devolver)
 *  @param oper2 Valor do segundo operando da instrucao identificada (caso exista e seja necessario/util devolver)
 *  @return apontador para a funcao que vai executar a instrucao assembly
 */
void *id_instruction(char parts[_MAX_CMD_PARTS][_MAX_CMD_BUFFER_SIZE], int n_parts, int* oper1, int* oper2);

/** @} */ // fim do grupo/modulo

#endif /* PARSER_H_ */
