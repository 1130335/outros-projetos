/** @file ia32impl.h
 *  @brief Definicoes para a implementacao das instrucoes suportadas.
 *
 *  Cabecalhos de funcoes e outras definicoes necessarias para a 
 *  implementacao das instrucoes suportadas.
 *  A implementacao das intrucoes propriamente ditas e' feita em assembly no 
 *  ficheiro ia32impl.s
 *
 *  @author Alexandre Braganca (ATB)
 *  @author Nuno Pereira (NAP)
 *  @date Nov 11, 2014
 *  @bug Nenhum conhecido.
 */

#ifndef IA32IMPL_H_
#define IA32IMPL_H_

/** @defgroup ia32impl IA32Impl.
 *  Funcoes relativas 'a implementacao, em assembly, das instrucoes suportadas.
 *  A funcao funcao_executa() vai executar o codigo que se encontra na respetiva
 *  etiqueta da instrucao a executar (que corresponde ao endereco da funcao que ela
 *  recebe como parametro).
 *   
 *  @{
 */
 
/* -- execucao -- */

/** @brief Executa codigo que implementa uma instrucao 
 *  @param f apontador para o codigo a executar.
 *  @return void.
 */ 
void funcao_executa(void (*f)());

/* -- Instrucao MOV -- */
 
/** @brief Mover valor para o eax
 *  @return void.
 */
void func_mov_const_eax();

/** @brief Mover valor para o ebx
 *  @return void.
 */
void func_mov_const_ebx();

/** @brief Mover valor para o ecx
 *  @return void.
 */
void func_mov_const_ecx();

/** @brief Mover valor para o edx
 *  @return void.
 */
void func_mov_const_edx();

/** @brief Mover eax para ebx
 *  @return void.
 */
void func_mov_eax_ebx();

/** @brief Mover eax para ecx
 *  @return void.
 */
void func_mov_eax_ecx();

/** @brief Mover eax para edx
 *  @return void.
 */
void func_mov_eax_edx();

/* -- Instrucao SHL -- */
 
/** @brief shl cl, eax
 *  @return void.
 */ 
void func_shl_cl_eax();

/** @brief shl cl, ebx
 *  @return void.
 */ 
void func_shl_cl_ebx();

/** @brief shl cl, edx
 *  @return void.
 */ 
void func_shl_cl_edx();

/* -- Instrucao ADD -- */
 
/** @brief Adicionar valor constante a eax
 *  @return void.
 */ 
void func_add_const_eax();

/** @brief Adicionar valor constante a ebx
 *  @return void.
 */ 
void func_add_const_ebx();

/** @brief Adicionar valor constante a ecx
 *  @return void.
 */ 
void func_add_const_ecx();

/** @brief Adicionar valor constante a edx
 *  @return void.
 */ 
void func_add_const_edx();

/** @brief Adicionar valor constante a edx
 *  @return void.
 */ 
void func_add_const_edx();

/** @brief Adicionar valor constante a edx
 *  @return void.
 */ 
void func_add_eax_eax();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_eax_ebx();

/** @brief Adicionar registo eax a ebx
 *  @return void.
 */ 
void func_add_eax_ecx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_eax_edx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ebx_eax();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ebx_ebx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ebx_ecx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ebx_edx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ecx_eax();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ecx_ebx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ecx_ecx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_ecx_edx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_edx_eax();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_edx_ebx();

/** @brief Adicionar registo eax a eax
 *  @return void.
 */ 
void func_add_edx_ecx();

void func_adc_const_eax();
void func_adc_const_ebx();
void func_adc_const_ecx();
void func_adc_const_edx();
void func_adc_eax_eax();
void func_adc_eax_ebx();
void func_adc_eax_ecx();
void func_adc_ebx_eax();
void func_adc_ebx_ebx();
void func_adc_ebx_ecx();
void func_adc_ebx_edx();
void func_adc_ecx_eax();
void func_adc_ecx_ebx();
void func_adc_ecx_ecx();
void func_adc_ecx_edx();
void func_adc_edx_eax();
void func_adc_edx_ebx();
void func_adc_edx_ecx();
void func_adc_edx_edx(); 


void func_sub_const_eax();
void func_sub_const_ebx();
void func_sub_const_ecx();
void func_sub_const_edx();
void func_sub_eax_eax();
void func_sub_eax_ebx();
void func_sub_eax_ecx();
void func_sub_ebx_eax();
void func_sub_ebx_ebx();
void func_sub_ebx_ecx();
void func_sub_ebx_edx();
void func_sub_ecx_eax();
void func_sub_ecx_ebx();
void func_sub_ecx_ecx();
void func_sub_ecx_edx();
void func_sub_edx_eax();
void func_sub_edx_ebx();
void func_sub_edx_ecx();
void func_sub_edx_edx(); 


void func_sbb_const_eax();
void func_sbb_const_ebx();
void func_sbb_const_ecx();
void func_sbb_const_edx();
void func_sbb_eax_eax();
void func_sbb_eax_ebx();
void func_sbb_eax_ecx();
void func_sbb_ebx_eax();
void func_sbb_ebx_ebx();
void func_sbb_ebx_ecx();
void func_sbb_ebx_edx();
void func_sbb_ecx_eax();
void func_sbb_ecx_ebx();
void func_sbb_ecx_ecx();
void func_sbb_ecx_edx();
void func_sbb_edx_eax();
void func_sbb_edx_ebx();
void func_sbb_edx_ecx();
void func_sbb_edx_edx(); 


void func_imul_eax_eax();
void func_imul_ebx_eax();
void func_imul_ecx_eax();
void func_imul_edx_eax();
void func_imul_const_eax();
void func_imul_const_ebx();
void func_imul_const_ecx();
void func_imul_const_edx();


void func_idiv_eax_eax();
void func_idiv_ebx_eax();
void func_idiv_ecx_eax();
void func_idiv_edx_eax();
void func_idiv_const_eax();
void func_idiv_const_ebx();
void func_idiv_const_ecx();
void func_idiv_const_edx();


void func_incl_eax();
void func_incl_ebx();
void func_incl_ecx();
void func_incl_edx();


void func_decl_eax();
void func_decl_ebx();
void func_decl_ecx();
void func_decl_edx();


void func_cmp_const_eax();
void func_cmp_const_ebx();
void func_cmp_const_ecx();
void func_cmp_const_edx();


void func_test_const_eax();
void func_test_const_ebx();
void func_test_const_ecx();
void func_test_const_edx();


void func_sal_cl_eax();
void func_sal_cl_ebx();
void func_sal_cl_edx();


void func_sar_cl_eax();
void func_sar_cl_ebx();
void func_sar_cl_edx();


void func_rcl_cl_eax();
void func_rcl_cl_ebx();
void func_rcl_cl_edx();


void func_rcr_cl_eax();
void func_rcr_cl_ebx();
void func_rcr_cl_edx();


void func_rol_cl_eax();
void func_rol_cl_ebx();
void func_rol_cl_edx();


void func_ror_cl_eax();
void func_ror_cl_ebx();
void func_ror_cl_edx();


void func_negl_eax();
void func_negl_ebx();
void func_negl_ecx();
void func_negl_edx();


void func_notl_eax();
void func_notl_ebx();
void func_notl_ecx();
void func_notl_edx();


void func_and_const_eax();
void func_and_const_ebx();
void func_and_const_ecx();
void func_and_const_edx();
void func_and_eax_ebx();
void func_and_eax_ecx();
void func_and_ebx_eax();
void func_and_ebx_ecx();
void func_and_ebx_edx();
void func_and_ecx_eax();
void func_and_ecx_ebx();
void func_and_ecx_edx();
void func_and_edx_eax();
void func_and_edx_ebx();
void func_and_edx_ecx();


void func_or_const_eax();
void func_or_const_ebx();
void func_or_const_ecx();
void func_or_const_edx();
void func_or_eax_ebx();
void func_or_eax_ecx();
void func_or_ebx_eax();
void func_or_ebx_ecx();
void func_or_ebx_edx();
void func_or_ecx_eax();
void func_or_ecx_ebx();
void func_or_ecx_edx();
void func_or_edx_eax();
void func_or_edx_ebx();
void func_or_edx_ecx();


void func_xor_const_eax();
void func_xor_const_ebx();
void func_xor_const_ecx();
void func_xor_const_edx();
void func_xor_eax_ebx();
void func_xor_eax_ecx();
void func_xor_ebx_eax();
void func_xor_ebx_ecx();
void func_xor_ebx_edx();
void func_xor_ecx_eax();
void func_xor_ecx_ebx();
void func_xor_ecx_edx();
void func_xor_edx_eax();
void func_xor_edx_ebx();
void func_xor_edx_ecx();


void func_shl_cl_eax();
void func_shl_cl_ebx();
void func_shl_cl_edx();


void func_pushl_eax();
void func_pushl_ebx();
void func_pushl_ecx();
void func_pushl_edx();


void func_popl_eax();
void func_popl_ebx();
void func_popl_ecx();
void func_popl_edx();

/** @} */ // fim do grupo/modulo

#endif /* IA32IMPL_H_ */

