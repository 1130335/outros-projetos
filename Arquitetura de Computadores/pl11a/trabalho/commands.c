/** @file commands.c
 *  @brief Implementacao de funcoes dos comandos do proprio interpretador.
 *
 *  Este ficheiro contem funcoes que implementam os comandos suportados 
 *  pelo proprio interpretador.
 *
 *  @author Alexandre Braganca (ATB)
 *  @author Nuno Pereira (NAP)
 *  @date Nov 11, 2014
 *  @bug Nenhum conhecido.
 */

#include <stdio.h>
#include "ia32state.h"
#include "commands.h"

/* Converter um numero inteiro numa string contendo a sua representacao em binario */
char* conv_int_to_string_bin(long int x, char *s)
{
 	int  i=32;
 	s[i--]=0x00;   /* marcar o fim da string */
 	do
 	{ 	/* preencher a string da direita para a esquerda */
  		s[i--]=(x & 1) ? '1':'0';
  		x = x >> 1;         /* deslocar os bits uma "casa" para a direita */
 	} while( x > 0);

	while(i>=0) s[i--]='0';

 	return s;
}

/* Mostra o valor das eflags */
void display_eflags()
{
	/* Carry, Overflow, Sign, Zero, Parity */
	printf("EFLAGS=Carry(CF)=%d, Overflow(OF)=%d, Sign(SF)=%d, Zero(ZF)=%d, Parity(PF)=%d\n",
		reg_eflags & 0x00000001, (reg_eflags >> 11)& 0x00000001, (reg_eflags >> 7)& 0x00000001, (reg_eflags >> 6)& 0x00000001, (reg_eflags >> 2)& 0x00000001);
}

/* Funcao que implementa o comando 'display'. Ainda nao esta completa... */
void display()
{
	char buffer[_MAX_DIGITS_IN_NUMBER+1];

	conv_int_to_string_bin(reg_eax, buffer);
	printf("\nEAX=%s\n", buffer);
	conv_int_to_string_bin(reg_ebx, buffer);
	printf("EBX=%s\n", buffer);
	conv_int_to_string_bin(reg_ecx, buffer);
	printf("ECX=%s\n", buffer);
	conv_int_to_string_bin(reg_edx, buffer);
	printf("EDX=%s\n", buffer);
	display_eflags();
	
	
}

