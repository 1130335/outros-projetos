/* Exercício 2 para Avaliação */

#include <stdio.h>
#include <stdlib.h> 

/* Neste exercício pretende-se comparar 12 números inteiros e determinar o maior absoluto.
Resolvemos optar por ler os números pelo teclado no main para um vector de inteiros e chamar depois uma função
para verificar qual é o maior número absoluto (ou em módulo) e retornar o mesmo para o main, onde
será apresentado no ecrã. */

int maiorAbsoluto (int vec []) {
	int i,j,x=0;
	for (i=0; i<12; i++) {
		for(j=i+1;j<12;j++) {
			if(abs(vec[i]) > abs(vec[j])) {
				x=abs(vec[i]);
			} else if (abs(vec[i]) < abs(vec[j])){
				x=abs(vec[j]);
			}
	}
}
	return x;
}

int main () {
	int inteiros [12], i, num = 0;
	
	for (i=0; i<12; i++) {
		scanf("%d", &num);
		inteiros[i] = num;
	
	}
	
	printf("Maior absoluto: %d", maiorAbsoluto(inteiros));
	
	return 0;
}
