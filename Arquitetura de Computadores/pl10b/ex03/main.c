#include <stdio.h>
#include <stdlib.h>

struct stack{
	int numero;
	struct stack *proximo;	
};

static struct stack *topo = NULL;
static int tamanho = 5;

int st_count() {
	int count;
	count = 0;
	struct stack *temp;
	temp = topo;
	while(temp!=NULL) {
		temp = temp->proximo;
		count++;
	}
	return count;
}

void push (int i) {
	struct stack *temp;
	int count;
	temp = (struct stack*)malloc(sizeof(struct stack));
	count = st_count();
	if(count <= tamanho-1) {
		(temp->numero)=i;
		printf("Num: %d\n",(temp->numero));
		(temp->proximo) = topo;
		topo = temp;
	} else {
		tamanho = tamanho*2;
		(temp->numero) = i;
		(temp->proximo) = topo;
		printf("Num: %d\n",(temp->numero));
		topo = temp;
	}
}

int pop() {
	struct stack *temp;
	if(topo==NULL) {
		printf("\nA stack está vazia!\n");
		return 0;
	} else {
		temp = topo;
		printf("\nNúmero removido: %d\n", topo->numero);
		topo = topo->proximo;
		free(temp);
		return temp->numero;
	}
}

int main() {
	int escolha;
	int paragem = 1;
	while(paragem==1) {
		printf("\n0 que deseja fazer na stack?\n1) inserir um novo elemento\n2) remover um elemento \n3) terminar\n");
		printf("Introduza um número: ");
		scanf("%d", &escolha);
		
		if(escolha==1) {
			int ele = 0;
			printf("Introduza o número que deseja introduzir: ");
			scanf("%d", &ele);
			push(ele);
		} else if(escolha==2) {
			pop();
		} else if(escolha==3) {
			paragem = 0;
		} else {
			printf("\nEscolha errada!");
		}
	}
			
	return 0;
}
