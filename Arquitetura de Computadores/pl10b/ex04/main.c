#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

char * novaString (char vec []){
	
	char *res = (char*) malloc(strlen(vec) * sizeof(vec));
	
	int i=0;
	
	while (vec[i] != '\0'){
		char c = toupper(vec[i]);
		*(res+i) = c;
		i++;
	}
	*(res+i) = 0;
	return res;
}


int main (){
	
	char str[50];
	char *ap;
	
	int aux = read (1, str, 50);
	
	ap = novaString(str);
	
	write(1, "String introduzida pelo utilizador: \n", 36);	
	write (1, str, aux);	
	
	
	write (1, "String modificada: \n", 18);	
	write (1, ap, aux);
	
	free(ap);	

	return 0;
	
	
}
