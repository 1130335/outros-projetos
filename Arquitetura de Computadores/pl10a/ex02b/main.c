#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int numero;
	char nome [50];
} DADOS;

DADOS *reserva_espaco() {
	DADOS *ap = NULL;
	ap = (DADOS*) calloc(5, 54);
	return ap;
}

void insere_dados(DADOS *d, int n) {
	int i;
	for(i = 0; i<n; i++) {
		scanf("%d\n", &((&d[i])->numero));
		fgets((&d[i])->nome,50,stdin);
	}
}

int main() {
	DADOS *d = reserva_espaco();
	insere_dados(d,5);
	int i;
	for(i = 0; i<5; i++) {
		printf("\nAluno %d",i+1);
		printf("\nNumero: %d", (&d[i])->numero);
		printf("\nNome: %s", (&d[i])->nome);
	}
	free(d);
	return 0;
	
}
