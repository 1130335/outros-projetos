#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

char * novaString (char vec []){
	
	char *res = (char*) malloc(strlen(vec) * sizeof(vec));
	
	int i=0;
	
	while (vec[i] != '\0'){
		char c = toupper(vec[i]);
		*(res+i) = c;
		i++;
	}
	*(res+i) = 0;
	return res;
}


int main (){
	
	char str[] = "Arquitectura de Computadores\n";
	char *ap;
	
	ap = novaString(str);
	
	printf("String original: %s\n", str);
	
	printf("String modificada: %s\n", ap);
	
	free(ap);	

	return 0;
}



