#ex09.s
.global f

f:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#corpo da funcao
	pushl %ebx
	pushl %esi
	pushl %edi
	
	cmpl $100, 8(%ebp)
	jg n_maior_100
	jmp n_menor_igual_100
	
n_maior_100:
	movl 8(%ebp), %eax
	subl $10, %eax
	jg fim

n_menor_igual_100:	
	movl 8(%ebp), %ebx
	addl $11, %ebx
	
	pushl %ebx
	call f
	addl $4, %esp
	
	movl %eax, %ebx
	
	pushl %ebx
	call f
	addl $4, %esp
	
fim:
	#epilogo
	popl %edi
	popl %esi
	popl %ebx
	
	movl %ebp, %esp
	popl %ebp
	ret
	
