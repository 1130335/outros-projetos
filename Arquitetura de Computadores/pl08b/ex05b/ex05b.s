
.data 
    formatOutput:
    	.asciz "%c"

.text
.global imprime_string
imprime_string:
	#prologo
	push %ebp
	movl %esp, %ebp

	#backup de registos
	pushl %ebx
	pushl %esi
	pushl %edi

	movl 8(%ebp), %ebx 
    incl %ebx 
    
verifica_lastChar:
	movb (%ebx), %cl 
	cmpb $0, %cl 
	je inversa 

	
	pushl %ebx 
	call imprime_string 
	addl $4, %esp #repõe o topo da stack após chamada da função
	jmp inversa 

inversa:
    movl 8(%ebp), %esi 
	
	pushl (%esi) 
	pushl $formatOutput 
	call printf 
	addl $8, %esp 

	jmp end 

end:
	movl $0, %eax #coloca registo de retorno a 0

	#reposição de registos via backup na stack
	popl %edi
	popl %esi
	popl %ebx

	#epílogo
	movl %ebp, %esp
	popl %ebp
	ret
