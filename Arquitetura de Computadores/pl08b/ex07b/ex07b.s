.global maxVector
maxVector:
	#prólogo
	pushl %ebp
	movl %esp, %ebp

	#backup de registos
	pushl %ebx
	pushl %esi
	pushl %edi

	movl 8(%ebp), %ebx 
	movl (%ebx), %eax 
	
	cmpl $1, 12(%ebp) 
	je fim 

	movl 12(%ebp), %esi 
	decl %esi 

	
	pushl %esi #parâmetro n-1
	pushl 8(%ebp) #parâmetro *vec
	call maxVector 
	addl $8, %esp #reposição do topo da stack após chamada função

	movl 8(%ebp), %ebx 
	#multiplica o n-1 (topo actual do Vector) por o tamanho de cada elemento do vector (4 bytes) 
	imull $4, %esi, %esi
	addl %esi, %ebx #passa para o endereço (vec+n-1)

	cmpl %eax, (%ebx) 
	jg trocaMax 
	jmp fim 

trocaMax:
	movl (%ebx), %eax 
	jmp fim 

fim:
	#reposição de registos via backup existente na stack
	popl %edi
	popl %esi
	popl %edi

	#epílogo
	movl %ebp, %esp
	popl %ebp
	ret
