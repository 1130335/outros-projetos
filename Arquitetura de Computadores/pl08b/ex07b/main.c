#include "ex07b.h"
#include <stdio.h>
#define MAX 5

void printVector(int *vec, int n) {
    int i;
    for (i = 0; i < n; ++i)
    {
        printf("%2d ", *(vec+i));
    }
    printf("\n");
}

int main() {
    int vec[MAX] = {1, 2, 5, 4, 3};
    int n = MAX;
 
    printf("Vector Definido:\n");
    printVector(vec, n);
    printf("Elemento Máximo do Vector = %d\n", maxVector(vec, n));
 
    return 0;
}
