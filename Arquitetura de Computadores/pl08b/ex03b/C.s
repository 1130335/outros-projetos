.global C

C:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#backup de registos
	pushl %ebx
	pushl %ecx
	
	movl 8(%ebp), %eax		
	movl 12(%ebp), %ebx		
	
	cmpl %eax, %ebx			
	je maior				#jump se y igual que x
	cmpl $0,%ebx			#compara y com 0
	je maior				#jump se y igual a 0
	
	cmpl $0, %ebx    		#compara y com 0
	jl diferente      		#jump se 0 maior
	cmpl %eax, %ebx  		#compara x com y
	jg diferente     		#jump se y maior que x
	cmpl $1, %eax    		#compara x com 1
	jl diferente      		#jump se x menor que 1
	jmp ciclo
	
maior:
	movl $1, %eax	       #adiciona 1 a %eax
	jmp fim
	
diferente:
	movl $0, %eax          #move 0 para %eax
	jmp fim
	
ciclo:
	decl %eax				
	decl %ebx				
	pushl %ebx				
	pushl %eax				
	call C					
	addl $8, %esp			#repoe o topo da stack
	movl %eax, %ebx			
	
	movl 8(%ebp), %eax		
	decl %eax				
	movl 12(%ebp), %ecx		
	pushl %ecx				
	pushl %eax				
	call C					
	addl $8, %esp			#repoe o topo da stack
	
	addl %ebx, %eax			#adiciona o retorno anterior com o novo (C((x - 1), (y - 1)) + C((x - 1), y))

	
fim:
	#repor registos   
	popl %ebx
	popl %ecx
	
	#epilogo
	movl %ebp, %esp
	popl %ebp
	ret
