#include <stdio.h>

int C(int n1, int n2){
	if(n1==n2){	
		return 1;
	} else if (n2==0){	
		return 1;
	} else if((0<n2 && n2<n1) && n1>1) {	
		return (C((n1-1),(n2-1)) + C(n1-1, n2)); 
	}
	return 0;
}	
