#include <stdio.h>

int soma_vector (int *vec, int n) {
	if(n==0) {
		return 0;
	} else {
		int s;
		s = soma_vector(vec, n-1);
		if(vec[n-1]>0) {
			s= s + vec[n-1];
		}
		return s;
	}
}

int main(void) {
	int n = 6;
	int v[] = {1,2,3,4,5,6};
	int *vec;
	vec=v;
	printf("%d\n", soma_vector(vec,n));
	return 0;
}
