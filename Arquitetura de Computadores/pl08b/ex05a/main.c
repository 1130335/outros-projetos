
#include <stdio.h>
 
void imprime_string(char* str) {
    if(*(str+1) == 0) { 
        printf("%c", *str);
    } else {
        imprime_string((str+1)); /* Chama recursivamente até ao último caracter */
        printf("%c", *str); 
    }
}

int main() {
    char str[28] = "Arquitectura de computadores";
    imprime_string(str);
    printf("\n");
    return 0;
}
