#include <stdio.h>

int binario(int x) {
	if(x==0) {
		return 0;
	} else {
		return(x%2+10*binario(x/2));
	}
}

int main(void) {
	int x = 7;
	
	printf("%d",binario(x));
	printf("\n");
	
	return 0;
}
