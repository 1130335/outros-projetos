#ex06b.s
.global soma_vector
soma_vector:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	pushl %ebx
	pushl %esi
	pushl %edi
	
	#corpo da funcao
	
	movl $0, %eax
	cmpl $0, 12(%ebp)
	je fim
	
	movl 12(%ebp), %edi
	subl $1, %edi
	
	imull $4, %edi, %ecx
	
	movl 8(%ebp), %ebx
	addl %ecx, %ebx
	
	movl (%ebx), %esi
	
	pushl %edi
	pushl 8(%ebp)
	call soma_vector
	
	addl $8, %esp
	addl %esi, %eax
	
fim:
	#epilogo
	popl %edi
	popl %esi
	popl %ebx
	
	movl %ebp, %esp
	popl %ebp
	ret
