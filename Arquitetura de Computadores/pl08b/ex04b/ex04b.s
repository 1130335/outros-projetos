#ex04b.s
.global multiplica

multiplica:
	#prologo
	pushl %ebp        
    movl %esp, %ebp
    
    #corpo da função
    movl 8(%ebp), %ecx
    
    cmpl $1, %ecx
	je acaba
	decl %ecx
	
	pushl 12(%ebp)
	pushl %ecx
	call multiplica
	addl $8, %esp
	
	addl 12(%ebp), %eax
	jmp fim
	
acaba:
	movl 12(%ebp), %eax
	     
fim:   
	#epilogo
    movl %ebp, %esp
    popl %ebp
    
    ret
