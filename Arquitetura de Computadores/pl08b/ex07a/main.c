#include <stdio.h>
#define MAX 5

int maxVector(int *vec, int n) {
  if (n == 1) { 
        return *(vec);
    } else {
        int max = maxVector(vec, n-1); /** verifica o máximo do vector*/
         
        if(*(vec+n-1) > max) { 
            return *(vec+n-1); 
        } else {
            return max; /**retorna resultado obtido recursivamente **/
        }
    }
}


 
void printVector(int *vec, int n) {
    int i;
    for (i = 0; i < n; ++i)
    {
        printf("%2d ", *(vec+i));
    }
    printf("\n");
}
 
int main() {
    int vec[MAX] = {1, 2, 5, 4, 3};
    int n = MAX;
 
    printf("Vector Definido:\n");
    printVector(vec, n);
    printf("Elemento Máximo do Vector = %d\n", maxVector(vec, n));
 
    return 0;
}
