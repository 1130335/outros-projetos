#ex08b.s
.global binario

binario:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	movl $2, %edi
	
	#corpo da funcao
	pushl %ebx
	pushl %esi
	pushl %edi
	
	movl $1, %eax
	cmpl $1, 8(%ebp)
	je fim
	
	movl $0, %edx
	movl 8(%ebp), %eax
	idivl %edi
	movl %edx, %esi
	
	pushl %eax
	call binario
	addl $4, %esp
	
	imull $10, %eax, %eax
	addl %esi, %eax
	
fim:
	#epilogo
	popl %edi
	popl %esi
	popl %ebx
	
	movl %ebp, %esp
	popl %ebp
	ret
