#include <stdio.h>
#include "compacta_data.h"
#include "compatar.h"
#include <stdlib.h>

struct Data{
	int ano;
	int mes;
	int dia;
	int data;
};

int main() {
	struct Data* ap = NULL;
	ap = (struct Data*)malloc(16);
	
	struct Data d[5];
	
	d[0].ano = 2012;
	d[0].mes = 11;
	d[0].dia = 2;
	d[0].data = 0;
	
	d[1].ano = 1999;
	d[1].mes = 2;
	d[1].dia = 10;
	d[1].data = 0;
	
	d[2].ano = 1820;
	d[2].mes = 4;
	d[2].dia = 27;
	d[2].data = 0;
	
	d[3].ano = 1923;
	d[3].mes = 8;
	d[3].dia = 12;
	d[3].data = 0;
	
	d[4].ano = 1910;
	d[4].mes = 4;
	d[4].dia = 1;
	d[4].data = 0;
	
	ap = &d[0];
	
	int* ano;
	ano = &(ap->ano);
	int* mes;
	mes = &(ap->mes);
	int* dia;
	dia = &(ap->dia);
	int* data;
	data = &(ap->data);
	
	compacta_data(ano, mes, dia, data);
	printf("A data enviada compactada é igual a %d\n", ap->data);
	free(ap);
	
	Data* d2 = (Data*)calloc(4,64);
	int anos[] = {2012,1999,1820,1923,1910};
	int meses[] = {11,2,4,8,4};
	int dias[] = {2,10,27,12,1};
	int datas[5];
	
	return 0;
}
