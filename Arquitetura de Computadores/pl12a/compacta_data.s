.global compacta_data

compacta_data:
	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	# corpo da função
	movl 8(%esp), %ebx
	movl 12(%esp), %ecx
	movl 16(%esp), %edx
	movl 20(%esp), %eax

	movl (%ebx), %esi #move-se o ano
	sarl $16, %esi #move o ano para os 16 bits mais significativos
	andl $4294901760, %esi #garante que os últimos 16 bits estão a zero
	
	addl (%edx), %esi #adiciona o dia
	roll $24, %esi
	addl (%ecx), %esi #adiciona o mes
	roll $8, %esi
	
	movl %esi, %eax
	
	fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
