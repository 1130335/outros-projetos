.global converte_data

converte_data:
	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	# corpo da função
	movl 8(%esp), %ebx
	movl 12(%esp), %ecx
	movl 16(%esp), %edx
	movl 20(%esp), %eax
	movl $0, %esi
	
	ciclo:
	cmpl $5, %esi
	je fim
	
	pushl %eax
	pushl %edx
	pushl %ecx
	
	pushl (%ebx)
	pushl (%ecx)
	pushl (%edx)
	pushl (%eax)
	call compacta_data
	addl $20, %esp
	
	popl %eax
	popl %edx
	popl %ecx
	
	incl %esi
	addl $4, %ebx
	addl $4, %ecx
	addl $4, %edx
	addl $4, %eax
	
	fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
