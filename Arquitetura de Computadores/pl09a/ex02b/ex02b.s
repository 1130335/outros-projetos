#ex02b.s
.global menor_data

menor_data:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	#corpo da função
	movl $0, %eax
	
	#Comparação dos anos
	movl 8(%ebp), %ebx
	movl 12(%ebp), %ecx
	
	andl $65535, %ebx
	andl $65535, %ecx
	
	cmpl %ebx, %ecx
	jg data2_maior_data1
	jl data1_maior_data2
	
	#Se os anos forem iguais, compara meses
	movl 8(%ebp), %ebx
	movl 12(%ebp), %ecx
	
	sar $24, %ebx
	sar $24, %ecx
	
	andl $255, %ebx
	andl $255, %ecx
		
	cmpl %ebx, %ecx
	jg data2_maior_data1
	jl data1_maior_data2
	
	#Se os meses forem iguais, compara os dias
	movl 8(%ebp), %ebx
	movl 12(%ebp), %ecx
	
	sar $16, %ebx
	sar $16, %ecx
	
	andl $255, %ebx
	andl $255, %ecx
	
	cmpl %ebx, %ecx
	jg data2_maior_data1
	jl data1_maior_data2
	
data2_maior_data1:
	movl 8(%ebp), %eax
	jmp fim

data1_maior_data2:
	movl 12(%ebp), %eax
	jmp fim

fim:
	#restauro do registos
	popl %ebx
	popl %edi
	popl %esi
	
	#epilogo
	movl %ebp, %esp
	popl %ebp
	ret
