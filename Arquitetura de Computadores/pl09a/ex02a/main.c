#include <stdio.h>

int menor_data(int data1, int data2) {
	int menor = 0;
	//data1_binario = 0000 1100|0000 1011|0000 0111 1101 1110
	//data2_binario = 0000 1100|0000 1011|0000 0111 1101 1111
	
	//comparar anos
	//mascara_ano_data_binario= 0000 0000|0000 0000|1111 1111 1111 1111
	//mascara_ano_data_decimal = 65535
	int dano1 = data1 & 65535;
	int dano2 = data2 & 65535;
	if(dano1<dano2) {
		menor = data1;
	} else if(dano1>dano2) {
		menor = data2;
	} else {
		//comparar meses
		int dmes1 = data1>>24;
		int dmes2 = data2>>24;
		//mascara_mes_data_binario = 0000 0000|0000 0000|0000 0000|1111 1111
		//mascara_mes_data_decimal = 255
		dmes1 = dmes1 & 255;
		dmes2 = dmes2 & 255;
		if(dmes1<dmes2) {
			menor = data1;
		} else if(dmes1>dmes2) {
			menor = data2;
		} else {
			//comparar dias
			int ddia1 = data1>>16;
			int ddia2 = data2>>16;
			ddia1 = ddia1 & 255;
			ddia2 = ddia2 & 255;
			if(ddia1<ddia2) {
				menor = data1;
			} else {
				menor = data2;
			}
		}
	}
	return menor;
}

int main() {
	int data1 = 202049502;
	int data2 = 202049503;
	int menor = menor_data(data1, data2);
	printf("A menor data é %d.\n", menor);

	return 0;
}
