#include <stdio.h>


int rotate_left(int num, int nbits){
	
	num = (num << nbits) | (num >> (32-nbits));
	
	return num;
}

int rotate_right(int num, int nbits){
	
	num = (num >> nbits) | (num << (32-nbits));
	
	return num;
}


int main(void) {
	
	printf("%d",rotate_left(12, 1));
	printf("\n");
	printf("%d", rotate_right(12, 1));
	printf("\n");
	
	return 0;
}
