#Ficheiro Assembly para a função calcula_pontuacao_equipa

.global calcula_pontuacao_equipa

calcula_pontuacao_equipa:

	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	#corpo da função
	movl $0, %esi # limpa o registo esi
	movl $0, %ecx # limpa o registo ecx
	movb 8(%esp), %cl # move o código da equipa para ecx
	movl 12(%esp), %edx # move o vetor com os jogos para edx
	
	movl (%edx), %ebx # move o número de jogos para o ebx
	incl %edx # avança para o primeiro jogo
	
	ciclo:
	cmpl $0, %ebx
	je fim
	
	cmpb (%cl), %dl # compara o código recebido com o código da equipa que jogou em casa
	je jogou_em_casa # entra se forem iguais
	
	sarl $16, %edx # se não jogou em casa, faz-se shift à direita de 16 para ficar com a equipa que jogou fora
	andl $65535, %edx # aplica-se a máscara lógica para garantir que ficamos apenas com os dados necessários
	
	cmpb (%cl), %dl # compara o código recebido com o código da equipa que jogou fora
	je jogou_fora
	
	decl %ebx
	incl %edx
	
	jogou_em_casa:
	pushl %ecx
	pushl %edx
	
	pushl %edx
	call resultado_jogo
	addl $4, %esp
	addl %eax, %esi
	
	popl %eax
	popl %ecx
	popl %edx
	
	incl %edx
	decl %ebx
	jmp ciclo
	
	jogou_fora:
	# terminar e corrigir

	fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
