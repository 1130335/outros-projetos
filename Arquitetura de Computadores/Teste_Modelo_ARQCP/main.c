#include <stdio.h>
#include "resultado_jogo.h"
#include "calcula_pontuacao_equipa.h"

/*
	Equipa A (casa) - Equipa B (fora) / 3 - 0
	* Equipa A marcou 3 golos e a Equipa B marcou 2 golos = 37880641
	Equipa A (fora) - Equipa B (casa) / 3 - 0
	* Equipa A marcou 4 golos e a Equipa B marcou 1 golo = 21103681
	Equipa A (casa) - Equipa C (fora) / 1 - 1
	* Equipa A marcou 1 golo e a Equipa C marcou 1 golo = 21168449
	Equipa A (fora) - Equipa C (casa) / 0 - 3
	* Equipa A marcou 0 golos e a Equipa C marcou 1 golo = 21168193
	Equipa A (casa) - Equipa D (fora) / 0 - 3
	* Equipa A marcou 2 golos e a Equipa D marcou 3 golo = 54788673
	Equipa A (fora) - Equipa D (casa) / 3 - 0
	* Equipa A marcou 3 golos e a Equipa D marcou 1 golo = 21234497

	Equipa B (casa) - Equipa C (fora) / 1 - 1
	* Equipa B marcou 2 golos e a Equipa C marcou 2 golo = 37945922
	Equipa B (fora) - Equipa C (casa) / 1 - 1
	* Equipa B marcou 0 golos e a Equipa C marcou 0 golo = 4390978
	Equipa B (casa) - Equipa D (fora) / 3 - 0
	* Equipa B marcou 1 golo e a Equipa D marcou 0 golos = 4456770
	Equipa B (fora) - Equipa D (casa) / 1 - 1
	* Equipa B marcou 3 golos e a Equipa D marcou 3 golo = 54788930

	Equipa C (casa) - Equipa 4 (fora) / 3 - 0
	* Equipa C marcou 3 golos e a Equipa D marcou 0 golo = 4457283
	Equipa C (fora) - Equipa 4 (casa) / 0 - 3
	* Equipa C marcou 3 golos e a Equipa D marcou 4 golo = 71566147
	
	[---- ----] golos(fora) | [---- ----] equipa(fora) | [---- ----] golos(casa) | [---- ----] equipa(casa)
*/

int main() {
	int jogos[] = {12, 37880641, 21103681, 21168449, 21168193, 54788673, 21234497, 37945922, 4390978, 4456770, 54788930, 4457283, 71566147};
	int pontos = resultado_jogo(jogos[10]);
	printf("\nA equipa que jogou em casa no jogo 10 adquiriu %d pontos.\n", pontos);
	
	return 0;
}
