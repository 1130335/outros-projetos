#Ficheiro Assembly para a função resultado_jogo

.global resultado_jogo

resultado_jogo:

	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	# corpo da função
	movl 8(%ebp), %ecx
	
	movl %ecx, %edx # copia o jogo para o edx
	sarl $24, %edx # faz shift de 24 para a direita para ficar com os golos da equipa que jogou fora
	andl $255, %edx # aplica a máscara lógica para garantir que não há lixo antes do número pretendido
	sarl $8, %ecx # faz shift de 8 para a direita para ficar com os golos da equipa que jogou em casa
	andl $255, %ecx # aplica a máscara lógica para ficar apenas os golos da equipa que jogou em casa
	
	cmpl %edx, %ecx # compara os golos das duas equipas
	je empatados # se empataram, entra nesta label (1 ponto)
	jg ganhou_casa # se a equipa que jogou em casa ganhou, entra nesta label (3 pontos)
	
	movl $0, %eax # se não entrar em nenhuma label, a equipa que jogou em casa perdeu (zero pontos)
	jmp fim

empatados:	
	movl $1, %eax
	jmp fim
	
ganhou_casa:
	movl $3, %eax
	jmp fim	
	
fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
