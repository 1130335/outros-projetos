/* Resolução do exercicío 6 */

#include <stdio.h>

int lerInteiros () {
	int num = 0;
	scanf("%d", &num);
	return num;
}

int main () {
	int vec[5];
	int i = 0, j=0, x=0;
	
	while (i<5) {
		vec[i] = lerInteiros();
		i++;	
	}

	for (i=0;i<=3;i++) {
		for (j=i+1;j<=4;j++) {
			if (vec[i] > vec[j]) {
				x=vec[i];
				vec[i]=vec[j];
				vec[j] = x;
			}
		}
	}
	
	i = 0;
	printf("Vector ordenado:\n");
	while (i<5) {
		printf("%d\n", vec[i]);
		i++;	
	}
	
	return 0;
}
