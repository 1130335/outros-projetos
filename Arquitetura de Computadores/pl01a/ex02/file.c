/* resolucao exercicio 2 */

#include <stdio.h>

int string2inteiro(char s[]) {
	int num = 0;
	int a = s[0] - '0';
	int b = s[1] - '0';
	int c = s[2] - '0';	
	num = a*100 + b*10 + c;
	return num;
}

int main() {
	char vec[] = "123";
	int num = 0;
	num = string2inteiro(vec);
	printf("%d\n", num);
	return 0;
}
