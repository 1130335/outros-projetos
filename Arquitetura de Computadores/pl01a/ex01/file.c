/**
arqcp_PL_1-exc1.c

Grupo 04_Turma2DA
 
Faça um programa em C que apresente o tamanho dos tipos de dados 
base do C: char, int, float e double
 
 
**/
 
#include <stdio.h>
 
int main(void) {

    int i=1;
    printf("Int: %d\n", sizeof(i));
 
    char c='A';
    printf("Char: %d\n", sizeof(c));
     
    float f=2.1;
    printf("Float: %.d\n", sizeof(f));
     
    double d=0.2;
    printf("Double: %d\n", sizeof(d));
     
    return 0;
}
