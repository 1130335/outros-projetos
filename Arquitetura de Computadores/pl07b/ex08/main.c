#include <stdio.h>
#include "ex08.h"

void imprime_resultado(int op, int o1, int o2, int res) {
	printf("%d %c %d = %d\n", o1, op, o2, res);
}

int main(void) {
	int res = calcula(10,3);
	printf ("Res = %d\n", res);
	return 0;
}
