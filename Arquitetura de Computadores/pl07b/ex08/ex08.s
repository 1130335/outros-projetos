#ex08.s

.global calcula

calcula:
	#prologo
	pushl %ebp        
    movl %esp, %ebp
    pushl %edi
    pushl %esi
    
    #inicialização dos registos
    movl $0, %edi   
    movl $0, %esi
    
    #calculo da soma
    addl 8(%ebp), %edi
    addl 12(%ebp), %edi
    pushl %edi
    pushl 12(%ebp)
    pushl 8(%ebp)
    pushl $'+'
    call imprime_resultado
    
    #calculo da multiplicação
    movl 12(%ebp), %eax
    mull 8(%ebp)
    movl %eax, %esi
    pushl %esi
    pushl 12(%ebp)
    pushl 8(%ebp)
    pushl $'*'
    call imprime_resultado
    
    #calculo final
	movl $0, %eax
	subl %edi, %esi
	movl %esi, %eax
    
    #epilogo
    popl %esi
    popl %edi
    movl %ebp, %esp
    popl %ebp
    
    ret
