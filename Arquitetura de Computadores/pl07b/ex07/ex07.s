#ex07.s

.global conta_pares

conta_pares:
#prologo
	pushl %ebp
	movl %esp, %ebp
	pushl %ebx
	pushl %edi
	pushl %esi

#corpo da função
	movl $0, %ebx
	movl 8(%ebp), %ebx
	movl $2, %ecx
	movl $0, %eax
	movl $0, %edi
	movl $0, %esi
	
ciclo:
	movl $0, %edx
	cmpl %edi, 12(%ebp)
	je fim
	
	movl (%ebx), %eax
	divl %ecx
	cmpl $0, %edx
	je par
	
	incl %edi
	addl $4, %ebx
	movl $0, %eax
	jmp ciclo

par:
	incl %esi
	incl %edi
	addl $4, %ebx
	movl $0, %eax
	jmp ciclo

#epilogo
fim:
	movl %esi, %eax
	
	popl %esi
	popl %ebx
	popl %edi
	movl %ebp, %esp
	popl %ebp

	ret
