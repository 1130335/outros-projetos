/* main do ex07 */

#include <stdio.h>
#include "ex07.h"

int main(void) {
	int n = 4;
	int vec[] = {9,1,1,3};
	int *p_vec;
	p_vec = vec;
	int res = conta_pares(p_vec, n);
	printf("No vetor vec existem %d numeros pares\n", res);

	return 0;
}
