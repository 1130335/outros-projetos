#ex10.s

.global calc

calc:

# prologo
	pushl %ebp
	movl %esp, %ebp
	
	pushl %ebx
	pushl %esi
	pushl %edi
	
	
	movl 8(%ebp), %edi
	movl (%edi), %eax
	subl 12(%ebp), %eax
	
	imull 16(%ebp)
	
fim:

	popl %edi
	popl %esi
	popl %ebx

	#epilogo
	movl %ebp, %esp
	popl %ebp
ret
