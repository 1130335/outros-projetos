/* main do ex04*/

#include <stdio.h>
#include "ex04.h"

int main (void) {

	int num1 = 10;
	int num2 = 48;
	int num3;
	int *maior = &num3;
	
	printf("A soma dos dois números é: %d\n", soma_e_maior(num1, num2, maior));
	printf("O maior número é: %d\n", *maior);
	
	return 0;
	
}
