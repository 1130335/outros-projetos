#ex04.s

.global soma_e_maior

soma_e_maior:
# prologo
	pushl %ebp
	movl %esp, %ebp
	
# corpo da funcao
	movl 8(%ebp), %edx
	movl 12(%ebp), %ebx
	movl $0, %eax
	pushl %edx
	pushl %ebx
	
	addl %edx, %eax
	addl %ebx, %eax
	
	movl 16(%ebp), %ecx
	pushl %ecx
	
	cmpl %edx, %ebx
	jg e_maior	
	
	movl %edx, (%ecx)
	
	jmp fim
	
e_maior:
	
	movl %ebx, (%ecx)
	
	jmp fim
	
#epilogo

fim:
	
	pop %ebx
	pop %ecx
	pop %edx
	
	movl %ebp, %esp
	popl %ebp
	ret
