/* main do ex05 */

#include <stdio.h>
#include "ex05.h"

int main (void) {
	int x = 4;
	int v2 = 3;
	int *v1;
	v1 = &x;
	
	int quadrado = incrementa_e_quadrado(v1,v2);
	printf("O primeiro número incrementado é igual a %d\n", *v1);
	printf("O segundo número ao quadrado é igual a %d\n", quadrado);

	return 0;
}
