#ex05.s

.global incrementa_e_quadrado

incrementa_e_quadrado:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#resto da funcao
	movl $0, %eax
	movl $0, %ebx
	movl 8(%ebp), %ebx
	pushl %ebx
	movl 12(%ebp), %eax
	
	incl (%ebx)
	imul %eax
	
	#epilogo
fim:
	pop %ebx
	movl %ebp, %esp
	popl %ebp
	ret
