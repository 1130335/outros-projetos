/* main do ex06*/

#include <stdio.h>
#include "ex06.h"

int main (void) {
	
	char str1 [30] = "String 222222222222";
	char str2 [30] = "String 1";
	char *a = str1;
	char *b = str2;
	
	int i = testa_iguais(a, b);
	
	if (i == 0){
		printf("As strings não são iguais.");
	} else {
	printf("As strings são iguais.");
	}
	
	return 0;
}
