#ex06.s

.global testa_iguais

testa_iguais:

# prologo
	pushl %ebp
	movl %esp, %ebp
	pushl %ebx
	pushl %esi
	pushl %edi
	
	movl $0, %eax
	
	movl 8(%ebp), %edi
	movl 12(%ebp), %esi
	
ciclo:

	movb (%edi), %bl
	movb (%esi), %bh
	
	cmpb %bh, %bl
	jne fim
	
	cmpb $0, %bh
	je igual
	
	incl %esi
	incl %edi
	jmp ciclo
	
igual:

	cmpb $0, %bl
	jne fim
	movl $1, %eax
	jmp fim
	
	
fim:

#epilogo

	popl %esi
	popl %edi
	popl %ebx
	movl %ebp,%esp
	popl %ebp
	ret
