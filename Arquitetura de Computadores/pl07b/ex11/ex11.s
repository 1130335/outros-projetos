#ex11.s

.global soma_e_troca

soma_e_troca:
    pushl %ebp        
    movl %esp, %ebp
    pushl %ebx
    pushl %esi
    pushl %edi
    movl 8(%ebp), %ebx
    movl 12(%ebp), %esi
    movl $0, %ecx
    movl $0, %eax
    movl $0, %edx
    movl $0, %edi
        
ciclo:
	cmpl $5, %ecx
	je fim
	
	movl $0, %edx
	movl $0, %edi
	
	movl (%ebx), %edx
	addl %edx, %eax
	movl (%esi), %edi
	addl %edi, %eax
	movl %edx, (%esi)
	movl %edi, (%ebx)
	
	incl %ecx
	addl $4, %ebx
	addl $4, %esi
          
    jmp ciclo
    
fim:   
	popl %ebx
	popl %esi
	popl %edi	
    movl %ebp, %esp
    popl %ebp
    
    ret
