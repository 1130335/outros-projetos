#include <stdio.h>
#include "ex11.h"

int main(void) {
	int vec1[] = {1,2,3,4,5};
	int vec2[] = {6,7,8,9,10};
	int *v1;
	int *v2;
	v1 = vec1;
	v2 = vec2;
	
	printf ("A soma dos dois vetores é igual a %d.\n", soma_e_troca(v1,v2));
	return 0;
}
