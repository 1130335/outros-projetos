#ex09.s

.global soma_func

soma_func:
    pushl %ebp        
    movl %esp, %ebp
    pushl %ebx
    movl $1, %ecx   
    movl $0, %ebx
        
ciclo:
    cmpl $4, %ecx    
    jz fim            
    
    movl %ecx, %edx    
    incl %ecx
    
    pushl %ecx       
    pushl %edx 

    call func        
    addl %eax, %ebx 
      
    popl %ecx
    popl %edx
    incl %ecx        
    jmp ciclo
    
fim:
	movl $0, %eax 
    movl %ebx, %eax
    popl %ebx     
    movl %ebp, %esp
    popl %ebp
    
    ret
