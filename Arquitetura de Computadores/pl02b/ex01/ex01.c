#include <stdio.h>

double media(double , double );
double media_vetor(double [], int );

int main()
{
double v[2]={ 1, 2 };
double r=0;

r=media(v[0], v[1]);

printf("media =%f\n", r);
return 0;
}

double media(double n1, double n2)
{
return (n1+n2)/2;
}
/*media de elementos do vetor**/

double media_vetor(double v[], int n) 
{ 
int i;
double soma = 0;
for (i=0; i<n; i++)
{ soma = soma + v[i];
}
return soma/n;
}
