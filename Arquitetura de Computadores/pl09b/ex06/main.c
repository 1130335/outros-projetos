#include <stdio.h>

typedef struct {
	int bi;
	int num_fiscal;
	char nome[80];
	char morada[100];
}dados;

void insere_dados(dados *vec, int n) {
	int i;
	for (i=0;i<n;i++) {
		scanf("%d", &(vec[i].bi));
		scanf("%d", &(vec[i].num_fiscal));
		scanf("%s", (&vec[i])->nome);
		scanf("%s", (&vec[i])->morada);
	}
}

int main() {
	dados d[2];
	dados d1;
	dados d2;
	d[0] = d1;
	d[1] = d2;
	dados *vec;
	
	vec = d;
	
	insere_dados(vec,2);
	int i;
	for(i=0;i<2;i++) {
		printf("\nDados inseridos: \n%d\n", vec[i].bi);
		printf("%d\n", vec[i].num_fiscal);
		printf("%s\n", vec[i].nome);
		printf("%s\n", vec[i].morada);
	}
	
	return 0;
}
