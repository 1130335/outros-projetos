#ex04b.s
.global somabyte2

somabyte2:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	#corpo da função
	movl 8(%ebp), %eax
	movl 12(%ebp), %ecx
	movl 16(%ebp), %edx
	movl $1, %edi
	movl $0, %esi
	movl $0, %ebx
	
	movl (%ecx), %esi
	movl %esi, (%edx)
	addl $4, %ecx
	addl $4, %edx
	
ciclo:
	cmpl %edi, %esi
	je fim
	
	movl (%ecx), %ebx
	sar $8, %ebx
	andl $255, %ebx
	addl %eax, %ebx
	movl %ebx, (%edx)
	addl $4, %ecx
	addl $4, %edx
	incl %edi
	
	jmp ciclo
	
fim:
	#restauro do registos
	popl %ebx
	popl %edi
	popl %esi
	
	#epilogo
	movl %ebp, %esp
	popl %ebp
	ret
