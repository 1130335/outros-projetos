#include <stdio.h>

void somabyte2 (char x, int *vec1, int *vec2) {
	int i;
	int aux1 = 0;
	vec2[0] = vec1[0];
	for (i=1;i<vec1[0];i++) {
		aux1 = vec1[i];
		aux1 = aux1>>8;
		aux1 = aux1 & 255;
		aux1 = aux1 + x;
		vec2[i] = aux1;		
		printf("%d\n", vec2[i]);
	}
}

int main() {
	int v1[]= {3,65280,2};
	int *vec1 = v1;
	int v2[3];
	int *vec2 = v2;
	char x = 'a';
	
	somabyte2(x,vec1,vec2);
	
	return 0;
}
