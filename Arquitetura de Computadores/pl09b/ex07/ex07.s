#ex07.s
.global numero_fiscal

numero_fiscal:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	#corpo da função
	movl 8(%ebp), %ecx
	movl $0, %eax
	addl $4, %ecx
	movl (%ecx), %eax
	
fim:
	#restauro do registos
	popl %ebx
	popl %edi
	popl %esi
	
	#epilogo
	movl %ebp, %esp
	popl %ebp
	ret
