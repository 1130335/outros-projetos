#include <stdio.h>

void subtrai_unid (int *vec, int tamanho){
	 int i, aux, ultimo;
	 for (i = 0; i< tamanho; i++) {
		aux = *(vec+i);
		aux = aux & 0x0F00;
		ultimo = aux >> 8;	
		
		if ( ultimo > 3){
			aux = *(vec+i);
			ultimo= ultimo -1;
			ultimo = ultimo << 8;
			aux = aux & 0xFFFFF0FF; 
			aux = aux | ultimo;
			*(vec+i) = aux;
	 
		}	
	}  

}

int main(void) {
	
	int vec[] = {0x48,0x57,0x1452};
	int tamanho = 3;
	subtrai_unid(vec, tamanho);
	
	int i;
	
	for ( i= 0; i<tamanho; i++){
		printf("%X ", vec[i]);
	}
	printf("\n");
	
	return 0;
}

