.global subtrai_unid
subtrai_unid:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#backup registos
	pushl %ebx
	pushl %ecx
	pushl %edx
	
	movl 8(%ebp), %ebx 	
	movl 12(%ebp), %ecx 

ciclo:
	movl (%ebx), %eax 	
	andl $0x0F00, %eax	
	cmpb $3, %ah		
	jl maior			
	decb %ah			
	movl (%ebx), %edx 	
	andl $0xFFFFF0FF, %edx	
	orl %edx, %eax		#junta %edx a %eax
	movl %eax, (%ebx)	

maior:
	addl $4, %ebx	#passa à proxima posição do vetor
	loop ciclo		
	
	#repor registos
	popl %edx
	popl %ecx
	popl %ebx
	
	#epilogo
	movl %ebp, %esp
	popl %ebp

	ret
