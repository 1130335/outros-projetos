.section .data
	
str:
	.asciz "Arquitectura de Computadores"
	
.section .text
.global percorrer_string

percorrer_string:

	movl $0, %ebx

	movl $str, %ebx

ciclo:
	movb (%ebx), %cl
	cmpb $' ', %cl
	je tem_espaços
	cmpb $0, %cl
	jz fim
	
tem_espaços:
	
	incl %ebx
	jmp ciclo
	
fim:

ret
