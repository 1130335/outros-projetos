/* main do ex02 em C */

#include <stdio.h>
#include "ex02.h"

#define MAX_CHAR 20

char *ptr1, *ptr2;

int main (void) {
	char str1[MAX_CHAR] = "abcbdbebfb";
	char str2[MAX_CHAR];
	
	ptr1 = str1;
	ptr2 = str2;
	
	printf("str1 = %s \n", str1);
	str_copy_p();
	printf("str2 = %s \n", str2);
	
	return 0;
}
