/* Exercício 2 */

.global ptr1
.global ptr2

.global str_copy_p

str_copy_p:
	movl $0, %esi
	movl $0, %edi
	movl ptr1, %esi
	movl ptr2, %edi
	movl $0, %eax
	
ciclo:
	movb (%esi), %cl
	cmpb $0, %cl
	je e_zero
	
	cmpb $'b', %cl
	je e_b
	
	movb %cl, (%edi)
	incl %eax
	incl %esi
	incl %edi
	jmp ciclo
	
e_b:
	movb $'v', %cl
	movb %cl, (%edi)
	incl %eax
	incl %esi
	incl %edi
	jmp ciclo
	
e_zero:
	movb $0, (%edi)
	jz fim
	
fim:
	ret
