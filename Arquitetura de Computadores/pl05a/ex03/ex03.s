/* Exercício 3 */

.section .data
	val1:
	.int -3
	val2:
	.int 2

.section .text
.global _start
_start:
	movl $0, %eax
	movl $0, %ecx
	
	movl val1, %eax
	movl val2, %ecx
	
	imull %eax, %ecx
	
	movl %ecx, %ebx
	movl $1, %eax
	
	int $0x80
	
	
