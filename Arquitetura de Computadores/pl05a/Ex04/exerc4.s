# (3+2)*(5+4)*(4-1)

.section .text
.global _start
_start:

movl $0, %edx
movl $0, %ecx
movl $0, %eax

addl $3, %edx
addl $2, %edx
addl $5, %ecx
addl $4, %ecx
addl $4, %eax
subl $1, %eax

imull %eax, %ecx
imull %ecx, %edx

mov %edx, %ebx
movl $1, %eax
int $0x80 

