.global compactar

compactar:

	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	# corpo da função
	movl 8(%ebp), %esi
	movl 12(%ebp), %ebx
	movl 16(%ebp), %edx


	funcao:
	pushl %esi			# parametro ano
	pushl %ebx			# parametro mes
	pushl %edx		    # parametro dia
	call compacta_data	#chama a função
	movl %..., %eax		#move a data compactada para o registo de saída (%eax)
	addl $4, %esi		#passa à proxima posição do vec
	addl $4, %ebx		#passa à proxima posição do vec
	addl $4, %edx		#passa à proxima posição do vec
	
	
fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
