.global compacta_data

compacta_data:

	# prólogo
	pushl %ebp
	movl %esp, %ebp
	
	# backup dos registos
	pushl %ebx
	pushl %edi
	pushl %esi
	
	# corpo da função
	movl 8(%ebp), %ecx
	movl 12(%ebp), %ebx
	movl 16(%ebp), %edx
	
	sarl $8, %ebx #shift de 8 para a direita para guardar o mês
	andl $255, %ebx	#mascara utilizada para garantir que não existe lixo 
	sarl $16, %ecx #shift de 16 para a direita para guardar o ano
	andl $255, %ecx #mascara utilizada para garantir que não existe lixo 
	
	
fim:
	# restauro dos registos
	popl %ebx
	popl %edi
	popl %esi
	
	# epílogo
	movl %ebp, %esp
	popl %ebp
	ret
