#ex15.s

.global ptrvec1
.global ptrvec2
.global ptrvec3
.global comuns
.section .data
	j:					
	.int 0
	i:					
	.int 0

comuns:
	movl $0, %ebx
	movl $0, %ecx
	movl $0, %edx
	movl $0, %esi
	movl ptrvec1, %ebx	
	movl ptrvec2, %ecx	
	movl ptrvec3, %edx	
	movl $0, %eax		

for1:
	cmpl $3, i
	je fim
	
	movl (%ebx), %esi
	movl $0, j
	jmp for2
	
for2:
	cmpl $3, j
	je mudar_for
	
	cmpl (%ecx), %esi
	je jmp_iguais
	
	addl $1, j
	addl $4, %ecx
	jmp for2
	
mudar_for:
	addl $4, %ebx
	addl $1, i
	jmp for1
	
jmp_iguais:
	incl %eax
	movl %esi, %edx
	addl $4, %edx
	addl $4, %ebx
	addl $1, i
	jmp for1
			
fim:
	ret
