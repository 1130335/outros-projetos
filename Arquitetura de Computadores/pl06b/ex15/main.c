/* main do exercício 15 */

#include <stdio.h>
#include "ex15.h"

int *ptrvec1, *ptrvec2, *ptrvec3;

int main (void) {
	int vec1[] = {1,2,3};
	int vec2[] = {3,4,5};
	int vec3[3];
	ptrvec1 = vec1;
	ptrvec2 = vec2;
	ptrvec3 = vec3;
	int n = comuns();
	printf("Elementos no vetor: %d\n",n);
	int i;
	for (i=0; i<3; i++) {
		printf("Vec3_i = %d\n", vec3[i]);
	}
	return 0;
}
