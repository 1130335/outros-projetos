/* main do ex08 */
#include <stdio.h>
#include "ex08.h"

int *ptrvec;
int limite = 10;

int main(void) {
	int vec [] = {2,4,6,8,10,1,3,10,9,0};
	ptrvec = vec;
	int a = vec_inc();
	printf("Numero de elementos somados: %d \n", a);
	
	int i;
	for(i = 0; i<limite; i++) {
		printf("[ %d ] ", ptrvec[i]);
	}
	
	return 0;
}
