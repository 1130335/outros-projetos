#ex08.s

.global ptrvec
.global limite
.global vec_inc

vec_inc:
	movl limite, %ebx
	movl $0, %eax
	
	movl ptrvec, %edi

ciclo:
	cmpl %eax, %ebx
	je fim
	
	addl $1, %eax
	addl $1, (%edi)
	addl $4, %edi
	jmp ciclo
	
fim:
	ret
