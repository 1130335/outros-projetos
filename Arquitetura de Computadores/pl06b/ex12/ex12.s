#ex12.s

.global limite
.global ptrvec
.global vec_corta

vec_corta:
	movl limite, %ebx
	movl $0, %ecx
	movl ptrvec, %esi
	movl $0, %eax
	movl $1000, %edx
	
ciclo:
	cmpl %ecx, %ebx
	je fim
	
	cmpl (%esi), %edx
	jge maior_1000
	je maior_1000
	
	addl $4, %esi
	incl %ecx
	jmp ciclo
	
maior_1000:
	movl $0, (%esi)
	incl %eax
	addl $4, %esi
	incl %ecx
	jmp ciclo
	
fim:
	ret
