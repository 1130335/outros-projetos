# ex04.s

.global encripta
.global ptr1

encripta:
	movl $0, %esi
	movl $0, %eax
	movl ptr1, %esi
	
ciclo:
	movb (%esi), %cl
	cmpb $0, %cl
	jz fim
		
	cmpb $' ', %cl
	je espaco
	
	cmpb $'0', %cl
	je espaco
	
	incl %eax
	incl (%esi)
	incl %esi
	jmp ciclo

espaco:
	incl %esi
	jmp ciclo
	
fim:
	ret
