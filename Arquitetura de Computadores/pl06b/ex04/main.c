/* main.c */

#include <stdio.h>
#include "ex04.h"

#define MAX 25

char *ptr1;

int main(void) {
	char str[MAX] = "A Ana dormiu 000 bem";
	ptr1 = str;
	int alt = encripta();
	printf("Foram alterados %d carateres.", alt);
	printf("\n");
	printf("%s", ptr1);
	printf("\n");
	
	return 0;
	
}
