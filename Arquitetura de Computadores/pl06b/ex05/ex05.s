.global ptr1

.global desencripta
encripta:
	movl $0, %eax #limpar zona memória do retorno
	movl ptr1, %esi #endereço da frase 1 em ESI
	
ciclo:
	movb (%esi), %cl
	cmpb $0, %cl #se chegou ao fim da frase
	jz fimciclo #entao salta para return da funcao
	jmp go_desencript
	
go_desencript:
	cmpb $0x20, %cl #verifica se o caracter é espaço
	je end_desencript #se igual, entao nao encripta caracter
	subb $1, (%esi) #subtrai 1 ao byte actual
	incl %eax #conta o numero de caracteres alterado

end_desencript:
	incl %esi #passa ao proximo caracter
	jmp ciclo #passa à proxima iteração do ciclo

fimciclo:
	ret

