.global ptrvec
.global ptrnvec

.global vec_sum
vec_sum:
	movl $0, %eax
	movl ptrvec, %esi #endereço vector em ESI
	movl ptrnvec, %edi #número de elementos do vector em EDI
    
	movl $0, %ecx #variável contadora

ciclo:
    cmpl %ecx, %edi
	je fimciclo
	addl (%esi), %eax
	addl $4, %esi #adiciona 4 bytes ao endereço ESI para passar à próxima posição
	incl %ecx
	jmp ciclo
	
fimciclo:
	ret
