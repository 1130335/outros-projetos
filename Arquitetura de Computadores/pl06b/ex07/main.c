#include <stdio.h>
#include "ex07.h"
 
int *ptrvec, ptrnvec=10;
 
int main() {
    int soma = 0;
    int vec[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
 
    ptrvec = vec;
 
    soma = vec_sum();
    printf("Soma vector = %d\n", soma);
    return 0;
}
