#include <stdio.h>
#include "ex13.h"
#define MAX 10
 
int *ptrvec, ptrnvec;
 
void printVector(int* v, int n) {
    int i;
    for (i = 0; i < n; ++i)
    {
        printf("%2d ", *(v+i));
    }
    printf("\n");
}
 
int main() {
    int vec[MAX] = {10, -2, -5, 4, -8, 6, 7, 80, -9, 10};
 
    ptrvec = vec;
    ptrnvec = MAX;
 
    printf("Vector:\n");
    printVector(ptrvec, ptrnvec);
     
    int cont = 0;
    cont = preenche_vec();
 
    printf("Vector com elementos cortados:\n");
    printVector(ptrvec, ptrnvec);
    printf("Nr. Elementos alterados: %d\n", cont);
     
    return 0;
}
