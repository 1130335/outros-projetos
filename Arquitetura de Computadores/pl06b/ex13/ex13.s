.global ptrvec
.global ptrnvec

.global preenche_vec
preenche_vec:
	movl $0, %eax #numero elementos cortados em EAX 
	movl ptrvec, %esi #endereço vector em ESI
	movl $0, %ecx #variável contadora
    
ciclo:
    cmpl ptrnvec, %ecx
	je fimciclo
	
	cmpl $0, (%esi)
	jl putindex
	jmp contciclo
	
putindex:
	movl %ecx, (%esi)
    incl %eax
	jmp contciclo

contciclo:
	addl $4, %esi
	incl %ecx
	jmp ciclo

fimciclo:
	ret
	
