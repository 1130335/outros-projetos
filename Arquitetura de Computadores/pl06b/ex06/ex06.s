/* ex06.s */

.global ptr1
.global ptr2
.global ptr3

.global str_cat

str_cat:
	movl $0, %eax
	movl $0, %edi
	movl $0, %esi
	movl $0, %ebx
	movl ptr3, %ebx
	movl ptr1, %esi
	movl ptr2, %edi
	
primeiro_apontador:
	movb (%esi), %cl
	cmpb $0, %cl
	je segundo_apontador
	
	movb %cl, (%ebx)
	
	incl %eax
	incl %ebx
	incl %esi
	jmp primeiro_apontador
	
segundo_apontador:
	movb (%edi), %cl
	cmpb $0, %cl
	jz fim
	
	movb %cl, (%ebx)
	incl %eax
	incl %ebx
	incl %edi
	jmp segundo_apontador

fim:
	movb $0, (%ebx)
	ret
