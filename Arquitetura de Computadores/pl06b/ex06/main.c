/* main.c */
#include <stdio.h>
#include "ex06.h"
#define MAX 40

char *ptr1, *ptr2, *ptr3;

int main (void) {
	char str1[MAX] = "A Maria comeu ";
	ptr1 = str1;
	char str2[MAX] = "um bolo de chocolate";
	ptr2 = str2;
	char str3[MAX*2];
	ptr3 = str3;
	str_cat();
	printf("%s", str3);
	return 0;
}
