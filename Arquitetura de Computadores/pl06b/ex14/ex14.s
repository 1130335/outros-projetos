#ex14.s

.global ptrvec1
.global ptrvec2
.global ptrvec3
.global diferentes
.section .data
	j:					
	.int 0
	i:					
	.int 0
	aux1:				
	.int 0
	aux2:				
	.int 0

diferentes:
	movl $0, %ebx
	movl $0, %ecx
	movl $0, %edx
	movl ptrvec1, %ebx	
	movl ptrvec2, %ecx	
	movl ptrvec3, %edx	
	movl $5, %eax		

for1:
	cmpl $5, i
	je fim
	movl %ebx, aux2
	movl aux1, %edx
	movl $0, j
	jmp for2
	
for2:
	cmpl $5, j
	je mudar_for
	
	cmpl aux2, %ecx
	je iguais
	
	incl j
	addl $4, %ecx
	jmp for2
	
iguais:
	decl %eax
	jmp mudar_for
	
mudar_for:
	addl $4, %ebx
	movl aux2, %edx
	addl $4, %edx
	incl i
	jmp for1
		
fim:
	ret
