/* main do ex14 */
#include <stdio.h>
#include "ex14.h"

int *ptrvec1, *ptrvec2, *ptrvec3;

int main(void) {
	int vec1 [] = {10,2,4,6,8};
	int vec2 [] = {1,10,5,2,9};
	int vec3 [5];
	
	ptrvec1 = vec1;
	ptrvec2 = vec2;
	ptrvec3 = vec3;
	
	int dif = diferentes();

	printf ("Nº Elementos Diferentes: %d\n", dif);
	int i;
	for (i=0; i<5; i++) {
		printf("Vec3_i = %d\n", vec3[i]);
	}

	return 0;
}
