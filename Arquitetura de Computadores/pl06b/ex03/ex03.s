#Assembly - func.s - PL6 - Exercício 3: str_copy_p2()
.global ptr1
.global ptr2

.global str_copy_p2 #cabeçalho da função int str_copy_p(); 
str_copy_p2:
	movl $0, %eax
	movl ptr1, %esi
	movl ptr2, %edi

ciclo:
	movb (%esi), %cl 
	cmpb $0, %cl
	jz fimciclo

	cmpb $0x62, (%esi) #se o caracter actual for B
	je swapB #escreve V 
	cmpb $0x76, (%esi)
	je swapV
	movb %cl,(%edi) #copia caracter
	jmp continuaciclo

swapB:
	movb $0x76,(%edi) #coloca V em EDI
	incl %eax #incrementamos o numero de swaps 
	jmp continuaciclo

swapV:
	movb $0x62,(%edi) #coloca B em EDI
	incl %eax #incrementamos o numero de swaps 
	jmp continuaciclo

continuaciclo:	
	incl %esi #passa para o proximo caracter de ptr1
	incl %edi #passa para o proximo caracter de ptr2
	jmp ciclo

fimciclo:
	movl $0, (%edi)
	ret
	
