#ex10.s

.global ptrvec
.global limite
.global num
.global vec_search

vec_search:
	movl $0, %ebx
	movl ptrvec, %ebx
	movl $0, %ecx
	movl num, %ecx
	movl $0, %edx
	movl limite, %edx
	movl $0, %eax
	movl $0, %edi
	
ciclo:
	cmpl %edi, %edx
	je fim
	
	cmpl (%ebx), %ecx
	je jmp_encontrou
	
	incl %edi
	addl $4, %ebx
	jmp ciclo

jmp_encontrou:
	movl $2, %ebx
	jmp fim
	
fim:
	ret
