.global ptrvec
.global ptrnvec

.global vecsum_par
vecsum_par:
	movl $0, %ebx #soma índices pares em EBX 
	movl ptrvec, %esi #endereço vector em ESI
	movl $0, %ecx #variável contadora
    movl $2, %edi #divisor
    
ciclo:
    cmpl ptrnvec, %ecx
	je fimciclo
	
	movl $0, %edx #limpar zona memoria EDX do Resto
    movl (%esi), %eax
    idivl %edi
    cmpl $0, %edx
	je somapar
	jmp contciclo
	
somapar:
    addl (%esi), %ebx
	jmp contciclo

contciclo:
	addl $4, %esi
	incl %ecx
	jmp ciclo

fimciclo:
	movl %ebx, %eax
	ret
	
