.global ptrvec
.global ptrnvec

.global vec_pos
vec_pos:
	movl $0, %eax #soma índices pares em EBX 
	movl ptrvec, %esi #endereço vector em ESI
	movl $0, %ecx #variável contadora
    
ciclo:
    cmpl ptrnvec, %ecx
	je fimciclo
	
	cmpl $10, (%esi)
	jge maior10
	jmp contciclo
	
maior10:
    addl $1, %eax
	jmp contciclo

contciclo:
	addl $4, %esi
	incl %ecx
	jmp ciclo

fimciclo:
	ret
	
