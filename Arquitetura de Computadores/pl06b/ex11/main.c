#include <stdio.h>
#include "ex11.h"
#define MAX 10
 
int *ptrvec, ptrnvec;
 
void printVector(int* v, int n) {
    int i;
    for (i = 0; i < n; ++i)
    {
        printf("%2d ", *(v+i));
    }
    printf("\n");
}
 
int main() {
    int vec[MAX] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
 
    ptrvec = vec;
    ptrnvec = MAX;
 
    printf("Vector:\n");
    printVector(ptrvec, ptrnvec);
     
    int cont = 0;
    cont = vec_pos();
 
    printf("Números maiores que 10: %d\n", cont);
     
    return 0;
}
