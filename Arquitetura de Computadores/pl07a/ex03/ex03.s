#ex03.s

.global menor

menor:
	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#resto da funcao
	movl 8(%ebp), %eax
	pushl %eax	
	movl 12(%ebp), %ebx
	pushl %ebx
	movl 16(%ebp), %ecx
	pushl %ecx
	
	cmpl %eax, %ebx
	jl b_menor_a
	
	cmpl %eax, %ecx
	jl c_menor_a
		
	pop %edx
	pop %ebx
	pop %ecx
	
b_menor_a:
	cmpl %ecx, %ebx
	jl b_menor_c
	movl %ecx, %eax
	jmp fim

b_menor_c:
	movl %ebx, %eax
	jmp fim
	
c_menor_a:
	movl %ecx, %eax
	jmp fim
	
	#epilogo
fim:
	movl %ebp, %esp
	popl %ebp
	ret
