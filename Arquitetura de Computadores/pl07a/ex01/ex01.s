#ex01.h

.global quadrado

quadrado:

	#prologo
	pushl %ebp
	movl %esp, %ebp
	
	#resto da funcao
	pushl %ebx
	movl 8(%ebp), %eax
	imul %eax
	
	#epilogo
	movl %ebp, %esp
	popl %ebp
	ret
		
	
