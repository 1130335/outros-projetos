.global somatorio_n

somatorio_n:

#prologo

pushl %ebp
movl %esp, %ebp

#corpo da função

movl 8(%ebp), %edx
pushl %edx

movl $0, %eax
movl $1, %ebx

ciclo: 
	
	cmpl %ebx, %edx
	je fim
	jmp somatorio
	
somatorio:
	
	addl %ebx, %eax
	incl %ebx
	jmp ciclo
	
#epilogo

fim: 

pop %ebx
movl %ebp, %esp
popl %ebp
ret
