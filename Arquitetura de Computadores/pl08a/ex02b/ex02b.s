#ex02b.s
.global primo

primo:
	#prologo
	pushl %ebp        
    movl %esp, %ebp
    pushl %esi
    
    #corpo da função
    movl 8(%ebp), %eax
	movl %eax, %esi
	movl $0, %edx
	
	movl $2, %ecx
	idivl %ecx
	movl %eax, -4(%ebp)
	
divisao:
	movl 8(%ebp), %eax
	movl -4(%ebp), %ecx
	cmpl $1, %ecx
	je e_primo
	movl $0, %edx
	idivl %ecx
	cmpl $0, %edx
	je nao_primo
	
	subl $1, %ecx
	movl %ecx, -4(%ebp)
	pushl %esi
	call divisao
	
	addl $4, %esp

e_primo:
	movl $1, %eax
	jmp fim
	
nao_primo:
	movl $0, %eax
	jmp fim

fim:   
	#epilogo
	popl %esi
    movl %ebp, %esp
    popl %ebp
    
    ret
