#ex01b.s

.section .data
	str:
		.asciz "\n n = %d \n"
		
.section .text
.global count

count:

	pushl %ebp				
	movl %esp, %ebp			
	pushl %ebx	
	pushl %esi
	pushl %edi		
				
	
	movl 8(%ebp), %edi		
	cmpl $0, %edi			
	jz fim	


continua:
	
	pushl %edi
	pushl $str
	call printf
	addl $8, %esp
	
	decl %edi
	
	pushl %edi
	call count
	addl $4, %esp
	
fim:

	popl %esi
	popl %edi
	popl %ebx				
	movl %ebp, %esp			
	popl %ebp				
	ret	
