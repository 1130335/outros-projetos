#include <stdio.h>

void auxprimo(int n, int j, int i) {
	if(j>n/2 || i>1) {
		if (i==1) {
			printf("O número é primo.\n");
		} else {
			printf("O número não é primo.\n");
		}
	} else {
		if(n%j==0) {
			i=j;
		}
	return auxprimo(n, j+1, i);
	}
}

int primo(int n) {
	auxprimo(n,1,0);
	return 0;
}

int main(void) {
	primo(13);
	return 0;
}
