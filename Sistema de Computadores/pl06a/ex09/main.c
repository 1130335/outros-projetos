/* PL05A - Memória Partilhada
9.Implemente um programa que simule um jogo de apostas entre dois processos que comunicam através de uma zona de memória partilhada.
	O processo pai deve:
	* Criar a zona de memória com espaço suficiente para cada processo filho efectuar 3 apostas (valores entre 1 e 20);
	* Criar 5 novos processos;
	* Esperar que todos os processos filho efectuem as suas apostas;
	* Gerar um valor aleatório (entre 1 e 20);
	* Verificar quantas apostas acertaram nesse valor e imprimir essa informação;
	* Eliminar a zona de memória partilhada.
	Cada processo filho deve:
	* Gerar 3 valores aleatórios (entre 1 e 20);
	* Escrever os valores nas posições respectivas na zona de memória partilhada.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <math.h>



#define NR_APOS 3
#define NR_PROC 5

typedef struct{
	
int apostas[NR_PROC][NR_APOS];
int new_data;

}aposta;

int main(int argc, char *argv[]) {
	/* iniciar o gerador */
	
	/* gerar numero aleatorio */
	int r;
	pid_t pid;
	time_t t;
	int fd, data_size = sizeof(aposta);
	aposta *shared_data;
	
	fd = shm_open("/Aposta", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (aposta*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	int i;
	for(i=0; i<NR_PROC; i++) {
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
		
	if(pid == 0) {
		int j;
		srand((unsigned) time(&t)-10*i+1);
		for(j=0; j<NR_APOS; j++) {
				
				shared_data->apostas[i][j]=rand()% 20+ 1;
				printf("Processo %d aposta %d .\n",i,shared_data->apostas[i][j]);
		}
		
	}
		
	if(pid>0) {
		srand((unsigned) time(&t));
		int j,i;
		for(j=0; j<NR_PROC; j++) {
			wait(NULL);
		}
		int cont=0;
		
		int valor=rand()% 20 + 1;
		printf("Valor %d:.\n",valor);
		for(i=0; i<NR_PROC; i++) {
			for(j=0; j<NR_APOS; j++) {
			if(shared_data->apostas[i][j]==valor){
				printf("Processo %d acertou na aposta %d .\n",i,shared_data->apostas[i][j]);
				cont++;
			}
		}
	}
		printf("Acertou %d vezes.\n",cont);
		
		r = munmap(shared_data, data_size); /* desfaz mapeamento */
		if (r < 0) exit(1); /* verifica erro */
		r = shm_unlink("/Aposta"); /* remove do sistema */
		if (r < 0) exit(1); /* verifica erro */
		exit(0);
		
	}
	

	return 0;

}
