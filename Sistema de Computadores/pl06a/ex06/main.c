/* PL05A - Memória Partilhada
Exercício 6
O processo pai deve:
– Criar uma zona de memória partilhada de acordo com a struct aluno, indicada a seguir, e as necessárias variáveis de sincronização;
– Preencher a zona de memória partilhada de acordo com a informação introduzida pelo utilizador;
– Criar um novo processo;
– Esperar que o processo filho termine;
– Eliminar a zona de memória partilhada.

O processo filho deve:
– Aguardar pelos dados do aluno;
– Imprimir a informação no ecrã.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>


#define STR_SIZE 50

typedef struct {
	int numero;
	char nome[STR_SIZE];
	int new_data;
}aluno;

int main(int argc, char *argv[]) {
	/* iniciar o gerador */
	srand((unsigned) time(NULL));
	/* gerar numero aleatorio */
	
	
	int r;
	pid_t pid;
	int fd, data_size = sizeof(aluno);
	aluno *shared_data;
	
	fd = shm_open("/aluno", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (aluno*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	
	
	shared_data->new_data = 0; /*false*/
	pid = fork();
	
	if(pid < 0) {
		exit(-1);
	}
	
		
	
	if(pid>0) {
		int aux1;
		char aux2[80];
		printf("Insira o numero por favor: ");
		scanf("%d", &aux1);
		shared_data->numero = aux1;
		printf("Insira o nome por favor: ");
		scanf("%s", aux2);
		printf("\n");
		strcpy(shared_data->nome, aux2);
		shared_data->new_data = 1;
		wait(NULL);
		
		printf("Iniciando remoção do sistema . . .\n");
		r = munmap(shared_data, data_size); /* desfaz mapeamento */
		if (r < 0) exit(1); /* verifica erro */
		r = shm_unlink("/aluno"); /* remove do sistema */
		if (r < 0) exit(1); /* verifica erro */
		exit(0);
		
	}
	else { 
		while(!shared_data->new_data);
		printf("Nome: %s\n", shared_data->nome);
		printf("Numero: %d\n", shared_data->numero);
	}
	

	return 0;
}
