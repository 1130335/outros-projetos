/* PL05A - Memória Partilhada
10.IAltere o programa anterior de forma a suportar "rondas". Assim que todos os processos
filho terminem cada uma das rondas, o processo pai começa de imediato a
verificar as apostas efectuadas.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>



#define NR_APOS 3
#define NR_PROC 5

typedef struct{
	
int apostas[NR_PROC][NR_APOS];
int new_data;
int jogado;

}aposta;

int main(int argc, char *argv[]) {
	/* iniciar o gerador */
	srand((unsigned) time(NULL));
	/* gerar numero aleatorio */
	int r;
	pid_t pid;
	int fd, data_size = sizeof(aposta);
	aposta *shared_data;
	
	fd = shm_open("/Aposta", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (aposta*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	int i;
	for(i=0; i<NR_PROC; i++) {
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
		shared_data->new_data=0;
	if(pid == 0) {
		
			while(shared_data->new_data);
			shared_data->apostas[i]=rand()% 1000+ 1;
			printf("Processo %d aposta %d .\n",i,shared_data->apostas[i]);
			shared_data->new_data=1;
		
		
	}
		
	if(pid>0) {
		while(!shared_data->new_data);
		
		int j,i;
		int cont=0;
		int valor=rand()% 1000 + 1;
		printf("Valor %d:.\n",valor);
		for(i=0; i<NR_PROC; i++) {
		
			if(shared_data->apostas[i]==valor){
				printf("Processo %d acertou na aposta %d .\n",i,shared_data->apostas[i]);
				cont++;
			}
		}
		shared_data->new_data=0;
		}else{
		shared_data->new_data=0;
		}
		for(j=0; j<NR_PROC; j++) {
			wait(NULL);
		}
		
	
		printf("Acertou %d vezes.\n",cont);
		
		r = munmap(shared_data, data_size); /* desfaz mapeamento */
		if (r < 0) exit(1); /* verifica erro */
		r = shm_unlink("/Aposta"); /* remove do sistema */
		if (r < 0) exit(1); /* verifica erro */
		exit(0);
		
	}
	

	return 0;

}
