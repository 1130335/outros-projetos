#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define MAX_COLOR_VALUE     	255 
#define BUFFER_LENGTH     	50

typedef struct {
  	unsigned char red, green, blue;
} PPMPixel;

typedef struct {
	char format[2];
  	int x, y;
	unsigned int rgb_comp;
  	PPMPixel *data;
} PPMImage;


PPMImage *readPPM(const char *filename)
{
	char buff[BUFFER_LENGTH];
	PPMImage *img 	= NULL;
	FILE *fp	= NULL;
	int c;
	//open PPM file for reading
	fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		exit(1);
	}
	//read image format
	if (!fgets(buff, BUFFER_LENGTH, fp)) {
		perror(filename);
		exit(1);
	}
	//check the image format
	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		exit(1);
	}
	
	//alloc memory form image
	img = (PPMImage *)malloc(sizeof(PPMImage));
	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}
	img->format[0] = 'P';
	img->format[1] = '6';
	//check for comments
	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n') ;
		c = getc(fp);
	}
	//'ungetc' pushes the character char (an unsigned char) onto the specified stream so that the this is available for the next read operation.
	ungetc(c, fp);
	
	//read image size information
	if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		exit(1);
	}
	//read rgb component
	if (fscanf(fp, "%d", &img->rgb_comp) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
		exit(1);
	}
	//check rgb component depth
	if (img->rgb_comp != MAX_COLOR_VALUE) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		exit(1);
	}
	//
	while (fgetc(fp) != '\n') ;

	//memory allocation for pixel data
	img->data = (PPMPixel *) malloc( sizeof(PPMPixel) * img->x * img->y);
	if (!img->data) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}
	//read pixel data from file
	if (fread(img->data, sizeof(PPMPixel),img->x * img->y, fp) != img->x*img->y) {
		fprintf(stderr, "Error loading image '%s'\n", filename);
		exit(1);
	}
	fclose(fp);

	return img;
}

void writePPM(const char *filename, PPMImage *img)
{
	FILE *fp = NULL;
	//open file for output
	fp = fopen(filename, "wb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		exit(1);
	}
	//write the header file
	//image format
	fprintf(fp, "%c%c\n",img->format[0],img->format[1]);
	//image size
	fprintf(fp, "%d %d\n",img->x,img->y);
	// rgb component depth
	fprintf(fp, "%d\n",img->rgb_comp);
	// pixel data
	fwrite(img->data, sizeof(PPMPixel),img->x * img->y, fp);
	fclose(fp);
}

PPMImage * createPPM(int x, int y, int rgb_comp)
{
	int i;
	PPMImage *img 	= NULL;
	if (x < 0 || y < 0) {
		fprintf(stderr, "Invalid image size\n");
		exit(1);
	}
	//check rgb component depth
	if (rgb_comp != MAX_COLOR_VALUE) {
		fprintf(stderr, "Invalid rgb components\n");
		exit(1);
	}
	img = (PPMImage *)malloc(sizeof(PPMImage));
	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}
	//set image format
	img->format[0] = 'P';
	img->format[1] = '6';
	//set image size
	img->x 		= x;
	img->y 		= y;
	//set rgb component depth
	img->rgb_comp 	= rgb_comp;

	//memory allocation for pixel data
	img->data = (PPMPixel *) malloc( sizeof(PPMPixel) * img->x * img->y);
	if (!img->data) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}
	 for(i = 0; i < img->x *img->y;i++){
		img->data[i].red 	= rand() % (MAX_COLOR_VALUE + 1);
		img->data[i].green    	= rand() % (MAX_COLOR_VALUE + 1);
		img->data[i].blue 	= rand() % (MAX_COLOR_VALUE + 1);
	 }	

	return img;
}
void printPPM(PPMImage *img)
{
	int i, j;
	fprintf(stdout, "Format:%c%c\n",img->format[0],img->format[1]);
	fprintf(stdout, "X:%d:Y:%d\n",img->x,img->y);
	// rgb component depth
	fprintf(stdout, "RGB COMPONENT:%d\n",img->rgb_comp);
	// pixel data
	for(i = 0; i < img->x; i++){
		for(j = 0; j < img->y; j++){
			// Mapping matrix notation in a vector: index of line * Number of colmuns + index of colmun				
			fprintf(stdout, "(%u,%u,%u)\t",img->data[i*img->y+j].red, img->data[i*img->y+j].green, img->data[i*img->y+j].blue);
		}
		fprintf(stdout, "\n");
	}
}
PPMPixel * getPixelPPM(int x, int y, PPMImage *img)
{
	if((x >=0 && x < img->x) && (y >=0 && y < img->y)){
		return &img->data[x*img->y+y];
	}
	return NULL;	
	
}
int menu()
{
	int op;
	do{
		fprintf(stdout,"\nMenu\n");
		fprintf(stdout,"1 - Create a PPM image\n");
		fprintf(stdout,"2 - Print a PPM image\n");
		fprintf(stdout,"3 - Print one pixel\n");
		fprintf(stdout,"\n0 - Exit\n");
		fprintf(stdout,"Choose:");
		fscanf(stdin, "%d", &op);
	}while(op < 0 || op > 3);
	return op;
}
int main(int argc, char *argv[]) {

	char filename[BUFFER_LENGTH];
	PPMImage *img 	= NULL;
	PPMPixel *pixel	= NULL;
	int x, y;
	int op;
	do{
		op = menu();
		switch(op){
			case 0:
			break;
			case 1:
				fflush(stdin);
				fprintf(stdout,"\nFile name (PPM extension):");
				fscanf(stdin, "%s", filename);
				fflush(stdin);

				fprintf(stdout,"\nImage size (x):");
				fscanf(stdin, "%d", &x);
				fflush(stdin);

				fprintf(stdout,"\nImage size (y):");
				fscanf(stdin, "%d", &y);
				fflush(stdin);

				img = createPPM(x, y,MAX_COLOR_VALUE);
				if(img != NULL){
					writePPM(filename, img);
				}

			break;
			case 2:
				fflush(stdin);
				fprintf(stdout,"\nFile name (PPM extension):");
				fscanf(stdin, "%s", filename);
				fflush(stdin);

				img = readPPM(filename);
				if(img != NULL){
					printPPM(img);
				}

			break;
			case 3:
				fflush(stdin);
				fprintf(stdout,"\nFile name (PPM extension):");
				fscanf(stdin, "%s", filename);
				fflush(stdin);

				

				img = readPPM(filename);
				if(img != NULL){
					fprintf(stdout,"\nImage x coordinate:");
					fscanf(stdin, "%d", &x);
					fflush(stdin);

					fprintf(stdout,"\nImage y coordinate:");
					fscanf(stdin, "%d", &y);
					fflush(stdin);
					
					pixel = getPixelPPM(x, y, img);
					if(pixel){
						fprintf(stdout, "(%d,%d)%d(%u,%u,%u)\t",x,y, x*img->y+y,pixel->red,pixel->green, pixel->blue);
					}
					
				}

			break;

		}
	}while(op != 0);
	if(img != NULL){
		if(img->data != NULL)
			free(img->data);
		free(img);
	}
	return 0;
}

