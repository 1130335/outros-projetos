/* PL05A - Memória Partilhada
Exercício 12 - Jogador1
Implemente um programa que permita jogar o "Jogo do Galo"entre dois processos
usando memória partilhada não relacionados hierarquicamente.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#define DIM 3

typedef struct {
	int matriz[DIM][DIM];
	int play;
	int games;
	int vencedor;
} shared_data_type;

void mostrarTabela(int matriz[DIM][DIM]){
	 /*mostrar tabela do jogo*/
	int i, j;
    for(i = 0; i < DIM; i++){
        for (j = 0; j < DIM; j++){
			if(matriz[i][j]==1){
			 printf("| x |");
			}
			else if(matriz[i][j]==2){
			 printf("| o |");
			}
			else{
			 printf("|   |");
			}	
		}
		printf("\n");
     }
}

void criarTabela(int matriz[DIM][DIM]){
	int i,j;
	for(i = 0; i < DIM; i++){
			for (j = 0; j < DIM; j++){
				matriz[i][j] = 0;
			 }
        }
        
}

int verificaVitoriaHorizontal(int matriz[DIM][DIM]){
	int i,j, cont;
	int p;
	for(i = 0; i < DIM; i++){
		cont=0;
		p=matriz[i][0];
		for (j=0; j < DIM; j++){
			
			if(p==matriz[i][j] && matriz[i][j]!=0){
				cont++;
				
			 }
        }
        if(cont==3){
			i=3;
		}
	}
	if(cont==3){
		return 1;
	}else{
	return 0;
	}  
}

int verificaVitoriaVertical(int matriz[DIM][DIM]){
	int i,j, cont;
	int p;
	for(j = 0; j < DIM; j++){
		cont=0;
		p=matriz[0][j];
		for (i=0; i < DIM; i++){
			
			if(p==matriz[i][j] && matriz[i][j]!=0){
				cont++;
				
			 }
        }
        if(cont==3){
			j=3;
		}
	}
	if(cont==3){
		return 1;
	}else{
	return 0;
	}  
}

int verificaVitoriaDiagonal(int matriz[DIM][DIM]){
	int p,l;
	p=matriz[0][0];
	l=matriz[0][3];
	if(((p==matriz[1][1] && p==matriz[2][2]) || (l==matriz[1][1] && l==matriz[3][0])) && (p!=0 && l!=0)){
		return 1;
	
	}else{
		return 0;
	}  
}	
int main() {
	
	int r, cont=0;
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/jogo8", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	criarTabela(shared_data->matriz);
	
	shared_data->games=0;
	shared_data->play=0;
	shared_data->vencedor=0;
	
	  
	/*Iniciar jogo*/
	printf("\nJogador1:\n");
	do{
		while(shared_data->play);
		if(shared_data->vencedor!=1 && shared_data->vencedor!=2){
		do{
	
			int x, y;
			printf("É a sua vez!\n");
			mostrarTabela(shared_data->matriz);
			printf("Insira a coordenada X: ");
			scanf("%d", &x);
    
			printf("\n\nInsira a coordenada Y: ");
			scanf("%d", &y);
    
			x--;
			y--;

			if (shared_data->matriz[x][y] > 0 ){
				printf("\n Posição Ocupada!\n");
			}
			else{
				shared_data->matriz[x][y] = 1;
				cont=1;
				shared_data->play=1;
				shared_data->games++;

				mostrarTabela(shared_data->matriz);
				int h=verificaVitoriaHorizontal(shared_data->matriz);
				int v=verificaVitoriaVertical(shared_data->matriz);
				int d=verificaVitoriaDiagonal(shared_data->matriz);
				
				if(!h && !v && !d){
					
					printf("\nEsperando pelo jogador 2\n");	
				}else{
					
					shared_data->vencedor=1;
				}
				
				}
    
		}while(cont==0 && shared_data->vencedor!=1 && shared_data->vencedor!=2);
	}
	}while(shared_data->games<9 && shared_data->vencedor!=1 && shared_data->vencedor!=2);

	sleep(1);
	if(shared_data->vencedor!=0){
		printf("\nVencedor: Jogador %d\n",shared_data->vencedor);
	}else{
		printf("\nEmpate!\n");
	}
	printf("\nIniciando remoção do sistema . . .\n");
	r = munmap(shared_data, data_size); /* desfaz mapeamento */
	if (r < 0) exit(1); /* verifica erro */
	r = shm_unlink("/jogo8"); /* remove do sistema */
	if (r < 0) exit(1); /* verifica erro */
	exit(0);
	return 0;
}


