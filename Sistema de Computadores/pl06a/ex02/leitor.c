/* PL05A - Memória Partilhada
Exercício 2 - Leitor
Implemente um programa que permita partilhar um vector de 10 inteiros entre dois processos não relacionados hierarquicamente, um escritor e um leitor.
	* O escritor deve criar uma zona de memória partilhada, gerar 10 números aleatórios entre 1 e 20 e escrevê-los na zona de memória partilhada.
	* O leitor deve ler os 10 valores, calcular e imprimir a sua média.
Nota: o escritor deve ser executado antes do leitor.
*/

#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

typedef struct {
	
	int vec[20];
} shared_data_type;

int main(int argc, char *argv[]) {
	int i=0;
	int media=0;
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/vector", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	printf("SOU O LEITOR\n");
	
	for(i=0; i<20;i++){
		media=media+ shared_data->vec[i];
		
	}
	media=media/20;
	printf("Media: %d\n", media);
	
	return 0;
}
