// PL03A - Processos e Funções Exec
// Exercício 19

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>

int main() {
	pid_t pid;
	int ret;
	pid = fork();
	if(pid == -1) {
		printf("Erro na criação do processo!");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0) {
		wait(NULL);
		ret = execlp("cp", "cp", "main.c", "makefile", "backup", (char*) NULL);
	}
	
	if(pid == 0) {
		ret = execlp("mkdir", "mkdir", "backup", (char*) NULL);
	}
	
	return 0;
}
