// PL03A - Processos e Funções Exec
// Exercício 17

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>

int main() {
	int ret;
	char * inst [20];
	printf("Que instrução deseja executar?\n");
	scanf("%s", &inst);
	
	pid_t pid;
	pid = fork();
	
	if(pid == -1) {
		printf("Erro na criação de processo!");
		exit(EXIT_FAILURE);
	}
	
	if(pid == 0) {
		ret = execlp(inst, inst, (char*) NULL);
	}
	
	return 0;
}
