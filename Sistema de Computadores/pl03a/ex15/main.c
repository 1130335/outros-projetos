// PL03A - Processos e Funções Exec
// Exercício 15

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>

void executa(char* c1, char* c2, char* c3) {
	pid_t pids[3];
	char *argumentos[] = {c1,c2,c3,0};
	int i, ret;
	for(i = 0; i<3; i++) {
		pids[i] = fork();
		
		if(pids[i] == -1) {
			printf("Erro na criação do processo!");
			exit(EXIT_FAILURE);
		}
		
		if(pids[i] == 0) {
			break;
		}
	}
	
	if(pids[i] > 0) {
		wait(NULL);
	}
	
	if(pids[i] == 0 && i == 0) {
		//ret = execvp(c1, argumentos);
		ret = execlp(c1, c1, (char*) NULL);
	}
	
	if(pids[i] == 0 && i == 1) {
		ret = execlp(c2, c2, (char*) NULL);
		//ret = execvp(c2, argumentos);
	}
	
	if(pids[i] == 0 && i == 2) {
		ret = execlp(c3, c3, (char*) NULL);
		//ret = execvp(c3, argumentos);
	}
}

int main() {
	char* c1;
	char c1aux [] = {'p','s', 0};
	c1 = c1aux; 
	char* c2;
	char c2aux [] = {'l','s', 0};
	c2 = c2aux; 
	char* c3;
	char c3aux [] = {'w','h','o', 0};
	c3 = c3aux;
	
	executa(c1, c2, c3);
	
	return 0;
}
