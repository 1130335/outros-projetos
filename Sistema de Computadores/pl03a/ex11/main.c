// PL03A - Processos e Funções Exec
// Exercício 11

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
// #define NUM_F 2
#define NUM_F 6

/*
char cria_gemeos(pid_t lista[NUM_F]) {
	int i;
	for(i = 0; i<NUM_F; i++) {
		lista[i] = fork();
		if(lista[i] == -1) {
			perror("A criação do processo falhou");
			exit(EXIT_FAILURE);
		}
		
		if(lista[i] == 0) {
			break;
		}
	}
	
	if(lista[0] == 0) {
		return 'a';
	} else {
		return 'p';
	}
	
	if(lista[1] == 0) {
		return 'b';
	} else {
		return 'p';
	}		
}

int main() {
	pid_t lista[2];
	char p;
	p = cria_gemeos(lista);
	printf("Char: %c\n", p);
	return 0;
}
*/

// PL03A - Processos e Funções Exec
// Exercício 11a

void printaNum(int num){
	int i;
	for(i=num;i<num + 20;i++){
		printf("%d \n", i);
	}
}

void cria_gemeos(pid_t lista[NUM_F]) {
	int i, estado;
	for(i = 0; i<NUM_F; i++) {
		lista[i] = fork();
		if(lista[i] == -1) {
			perror("A criação do processo falhou");
			exit(EXIT_FAILURE);
		}
		
		if(lista[i] == 0) {
			break;
		}
	}
	
	if(lista[i] == -1) {
		printf("Erro!\n");
		exit(EXIT_FAILURE);
	}
	
	if(lista[i] > 0) {
		wait(NULL);
		printf("Eu sou o pai\n");
	}
	
	
	if(lista[0] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(100);
	}
	
	if(lista[1] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(80);
	}
	
	if(lista[2] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(60);
	}
	
	if(lista[3] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(40);
	}
	
	if(lista[4] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(20);
	}
	
	if(lista[5] == 0) {
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(0);
	}
	
	exit(EXIT_SUCCESS);
}

int main() {
	pid_t lista[NUM_F];
	cria_gemeos(lista);
	
	return 0;
}
