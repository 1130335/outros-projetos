/* PL08B - Semáforos 
   Exercício 5
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

int main(int argc, char *argv[]) {
	pid_t pid;
	sem_t *semaforo;
	
	if(!semaforo) {
		if((semaforo = sem_open("ex05", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
			perror("Impossível criar o semáforo");
			exit(EXIT_FAILURE);
		}
	}
	
	pid = fork();
	
	if(pid < 0) {
		perror("Erro na clonagem do processo");
		exit(EXIT_FAILURE);
	}
	
	if(pid == 0) {
		sem_wait(semaforo);
		printf("Eu sou o filho.\n");
		sem_post(semaforo);
		sem_unlink("ex05");
	}
	
	if(pid > 0) {
		printf("Eu sou o pai.\n");
		sem_post(semaforo);
	}
	
	return 0;
}

