/* PL08B - Semáforos 
   Exercício 7 - Cliente
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	int bilhete;
} shared_data_type;

int main(int argc, char *argv[]) {
	
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/ex07", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	sem_t *semaforo;
	if((semaforo = sem_open("ex07", O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	time_t t;
	srand((unsigned) time(&t));
	sleep(rand() % 10 + 1);
	int valor_sem = 0;
	while(valor_sem == 0) {
		sleep(rand() % 10 + 1);
		sem_getvalue(semaforo, &valor_sem);
	}
	printf("Vou ser atentido agora!\n");
	sem_wait(semaforo);
	printf("Este é o meu bilhete: %d\n", shared_data->bilhete);
	
	return 0;
}
