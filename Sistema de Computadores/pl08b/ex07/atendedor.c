/* PL08B - Semáforos 
   Exercício 7 - Atendedor
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	int bilhete;
} shared_data_type;

int main(int argc, char *argv[]) {
		
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
		
	sem_unlink("ex07");
	munmap(shared_data, sizeof(shared_data_type));
	close(fd);
	shm_unlink("/ex07");
		
	fd = shm_open("/ex07", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	shared_data->bilhete = 1;
	
	sem_t *semaforo;
	if((semaforo = sem_open("ex07", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
		
	int sair = 0;
	while(sair == 0) {
		int valor_sem = 1;
		while(valor_sem == 1) {
			sem_getvalue(semaforo, &valor_sem);
		}
		
		printf("Cliente %d atendido\n", shared_data->bilhete);
		shared_data->bilhete++;
		printf("Próximo cliente...\n");
		sem_post(semaforo);
		printf("Sair? 1 = Sim | 0 = Não\n");
		scanf("%d", &sair);	
	}
		
	return 0;
}
