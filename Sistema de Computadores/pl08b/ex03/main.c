/* PL08B - Semáforos 
   Exercício 3
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

int main(int argc, char *argv[]) {
	char *ficheiro = argv[1];
	printf("Ficheiro: %s\n", ficheiro);
	FILE *file;
	sem_t *semaforo;
	
	if(!semaforo) {
		if((semaforo = sem_open("sem1", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
			perror("Impossível criar o semáforo");
			exit(EXIT_FAILURE);
		}
	}
	
	// printf("ESTOU AQUI\n");
	sem_wait(semaforo);
	// printf("ESTOU AQUI\n");
	file = fopen(ficheiro, "a");
	fprintf(file, "Eu sou o processo com o PID %d\n", getpid());
	fclose(file);
	sleep(2);
	// printf("ESTOU AQUI\n");
	sem_post(semaforo);
	// printf("ESTOU AQUI\n");
	
	sem_unlink("sem1");
	// printf("ESTOU AQUI\n");
	return 0;
}

