#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>


void * count(void * k);

pthread_mutex_t mutex;

int vec[10000];
int counter[10];


int main(void) {

	pthread_mutex_init(&mutex, NULL);
	int i;
	time_t t;
   

   
   /* Intializes random number generator */
   srand((unsigned) time(&t));
	
	for(i=0;i<10000;i++) { // preencher vetor
		vec[i] = rand() % 10;
	}
	//reset aos counters
	for(i=0; i<10; i++){
		counter[i] = 0;
	}
	


	//criar as 10 threads
	pthread_t threads[10];
	int starts[10];	//vetor onde sao guardados os start points de pesquisa
	for(i=0;i<10;i++) {

		starts[i] = i*1000;	//define o start point da pesquisa
		pthread_create(&threads[i], NULL, count, (void *)&starts[i]);
	}
	
	/* esperar que todas as threads terminem */
	for(i=0;i<5;i++) {
		pthread_join(threads[i], NULL);
	}
	printf("\nTodas as threads terminaram\n");

	for (i = 0; i < 10; ++i)
	{
		printf("Numero de ocorrencias da chave %d: %d\n", i, counter[i]);
	}

	pthread_mutex_destroy(&mutex);
	return 0;
}

void * count(void * k) {


	int i = *((int *)k);

	int j;
	for(j=i;j<i+1000;j++) {

		pthread_mutex_lock(&mutex);
		counter[vec[j]]++;
		pthread_mutex_unlock(&mutex);
			   

	}
	
	
	pthread_exit((void*)NULL); //exit da thread
}
