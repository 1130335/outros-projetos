#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>

#define NUM_VIAGENS 2


void * percursoAD(void * k) ;

void * percursoCA(void * k) ;

char* hora(){
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    return asctime (timeinfo);
}

pthread_mutex_t mutexAB;
pthread_mutex_t mutexBD;
pthread_mutex_t mutexBC;	



int main(void) {

	//inicia os mutexs
	pthread_mutex_init(&mutexAB, NULL);
	pthread_mutex_init(&mutexBD, NULL);
	pthread_mutex_init(&mutexBC, NULL);

   
 	int i;


	//criar as 2 threads para cada um dos percursos
	pthread_t threadsAD[NUM_VIAGENS];
	pthread_t threadsCA[NUM_VIAGENS];
	int starts[2];
	for(i=0;i<NUM_VIAGENS;i++) {
		starts[i] = i;
		pthread_create(&threadsAD[i], NULL, percursoAD, (void *)&starts[i]);
		pthread_create(&threadsCA[i], NULL, percursoCA, (void *)&starts[i]);
	}

	
	/* esperar que todas as threads terminem */

	for(i=0;i<2;i++) {
		pthread_join(threadsAD[i], NULL);
		pthread_join(threadsCA[i], NULL);
	}
	printf("\nTodas as threads terminaram\n");

	
	//elimina os mutexs
	pthread_mutex_destroy(&mutexAB);
	pthread_mutex_destroy(&mutexBD);
	pthread_mutex_destroy(&mutexBC);
	return 0;
}

void * percursoAD(void * k) {
	//funçao para a execuçao do percurso CidadeA - CidadeD
	int numComboio = *((int *)k);
	char* horaSaida = hora();
	pthread_mutex_lock(&mutexAB);
	printf("Comboio AD%d\n", numComboio);
	printf("Procedente de CidadeA e com destino a CidadeD\n");
	printf("Encontra-se na linha AB\n\n");
	sleep(2);
	pthread_mutex_unlock(&mutexAB);

	pthread_mutex_lock(&mutexBD);
	printf("Comboio AD%d\n", numComboio);
	printf("Procedente de CidadeA e com destino a CidadeD\n");
	printf("Encontra-se na linha BD\n\n");
	sleep(2);
	pthread_mutex_unlock(&mutexBD);

	char* horaChegada = hora();

	printf("Comboio CA%d\n", numComboio);
	printf("Procedente de CidadeC e com destino a CidadeA\n");
	printf("Teve saída as %s e chegada às %s\n", horaSaida, horaChegada);
	
	
	pthread_exit((void*)NULL); //exit da thread
}

void * percursoCA(void * k) {
	//funçao para a execuçao do percurso CidadeC - CidadeA
	int numComboio =*((int *)k);
	char* horaSaida = hora();

	pthread_mutex_lock(&mutexBC);
	printf("Comboio CA%d\n", numComboio);
	printf("Procedente de CidadeC e com destino a CidadeA\n");
	printf("Encontra-se na linha BC\n\n");
	sleep(2);
	pthread_mutex_unlock(&mutexBC);

	pthread_mutex_lock(&mutexBD);
	printf("Comboio CA%d\n", numComboio);
	printf("Procedente de CidadeC e com destino a CidadeA\n");
	printf("Encontra-se na linha AB\n\n");
	sleep(2);
	pthread_mutex_unlock(&mutexBD);

	char* horaChegada = hora();

	printf("Comboio CA%d\n", numComboio);
	printf("Procedente de CidadeC e com destino a CidadeA\n");
	printf("Teve saída as %s e chegada às %s\n", horaSaida, horaChegada);
	
	
	pthread_exit((void*)NULL); //exit da thread
}
