#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#define NUM_THREADS 3
#define NUM_RUNS 9

pthread_mutex_t mutex[3]; // 4 - 5 | 8 - 9 | 9 - 10

int tempos[15][16] = { 
	{0, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 6, 4, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 5, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0}
};
int nextPos[NUM_THREADS][NUM_RUNS] = { 
	{1, 4, 5, 6, 8, 9, 10, 11, 15},
	{2, 4, 5, 7, 8, 9, 13, 14, 15},
	{3, 4, 5, 7, 9, 9, 10, 12, 15}
};

typedef struct {
	int id;
	int pos;
	int next;
	int tempo;
} Caracol;

Caracol caracois[NUM_THREADS]; 

void* run(void *arg){
	Caracol* c = (Caracol*) arg;
	int mutexID = 0;
	while(c->pos != 15) {
		if(c->pos == 4 || c->pos == 8 || c->pos == 9){ //mutex com conflito de contacto de caracois
			mutexID = (c->pos == 4 ? 0 : c->pos == 8 ? 1 : c->pos == 9 ? 2 : 0); //posição do mutex
			pthread_mutex_lock(&mutex[mutexID]);
		}
		time_t inicio = time(NULL);
		int times = tempos[c->pos][nextPos[c->id][c->next]];
		tempos[c->pos][nextPos[c->id][c->next]]*=2; //viscosidade
		printf("%d - %d\n", c->id, times);
		sleep(times);
		if(c->pos == 4 || c->pos == 8 || c->pos == 9) 
			pthread_mutex_unlock(&mutex[mutexID]);
		c->pos = nextPos[c->id][c->next];
		c->next++;
		time_t fim = time(NULL);
		c->tempo += difftime(fim, inicio);
		// c->tempo += times;
	}
	pthread_exit(NULL);
}



int main(int argc, char** argv){
	/*=============== Threads ================*/
	pthread_mutex_init(&mutex[0], NULL);
	pthread_mutex_init(&mutex[1], NULL);
	pthread_mutex_init(&mutex[2], NULL);
	pthread_t threads[NUM_THREADS];
	/*========================================*/
	Caracol* c[NUM_THREADS];
	int i = 0;
	for(i = 0; i < NUM_THREADS; i++) {
		c[i] = malloc(sizeof(Caracol));
		c[i]->id = i;
		c[i]->pos = 0;
		c[i]->tempo = 0;
		pthread_create(&threads[i], NULL, run, c[i]);
	}
	for(i = 0; i < NUM_THREADS; i++)
		pthread_join(threads[i], NULL);	
	
	for(i = 0; i < NUM_THREADS; i++)
		printf("Tempo do caracol %d : %d\n", c[i]->id+1, c[i]->tempo);
	
	pthread_mutex_destroy(&mutex[0]);
	pthread_mutex_destroy(&mutex[1]);
	pthread_mutex_destroy(&mutex[2]);

	return 0;
}