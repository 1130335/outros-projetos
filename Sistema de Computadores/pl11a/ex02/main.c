#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

#define NUM_THREADS 5
#define VAL 200

typedef struct {
	int pos;
	int num;
	int* vec;
} args;

void* threadFunction(void *arg){
	args* a = (args*) arg;
	int i;
	for(i = a->pos; i < a->pos + VAL ; i++){
		if(a->num == a->vec[i]){
			printf("Foi encontrado o numero %d na posicao %d", a->num, i);
			int* threadID = malloc(4);
			*threadID = a->pos/VAL;
			pthread_exit(threadID);
		}
	}
	pthread_exit((void*)NULL);
}

int main(int argc, char** argv){
	pthread_t threads[NUM_THREADS];
	int vec[1000];
	int i, num = 0;
	printf("Qual é o valor a encontrar?");
	scanf("%d", &num);
	args a[5];
	for(i = 0; i < NUM_THREADS*VAL; i++)	vec[i] = i;
	for(i = 0; i < NUM_THREADS; i++){
		a[i].pos = i*VAL;
		a[i].num = num;
		a[i].vec = vec;
		pthread_create(&threads[i], NULL, threadFunction, (void*) &a[i]);	
	}
	for(i = 0; i < NUM_THREADS; i++){
		int* threadID;
		pthread_join(threads[i], (void**)&threadID);
		
		if(threadID != NULL){
			int t = *threadID;
			printf("\n[FUNCAO PRINCIPAL]: %d\n", t);
			break;
		}		
	}
	return 0;
}