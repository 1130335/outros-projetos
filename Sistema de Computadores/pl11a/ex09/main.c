/* PL11A - Threads
   Exercício 9
   DÁ ERROS AO CORRER - VALORES INVÁLIDOS
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

/* Estrutura Prova */
typedef struct {
	int numeroAluno;
	int notaG1;
	int notaG2;
	int notaG3;
	int notaFinal;
}Prova;

/* Variável Global - Mutex Notas Finais */
pthread_mutex_t mux1;
/* Variável Global - Mutex Notas Positivas*/
pthread_mutex_t mux2;
/* Variável Global - Mutex Notas Negativas*/
pthread_mutex_t mux3;
/* Variável Global - Contador */
int contador;
/* Variável Global - Provas de 300 alunos */
Prova* provas;
/* Variável Global - Número de Positivas */
int n_positivas;
/* Variável Global - Número de Negativas */
int n_negativas;

int geraNota() {
	int nota = 0;
	nota = rand() % 101;
	return nota;
}

void* cria_provas(void *arg) {
	Prova* provas = (Prova*)malloc(sizeof(Prova)*300);
	int i;
	for(i = 0; i<300; i++) {
		provas[i].numeroAluno = i + 1;
		provas[i].notaG1 = geraNota();
		provas[i].notaG2 = geraNota();
		provas[i].notaG3 = geraNota();
		provas[i].notaFinal = 0;
	}	
	pthread_exit((void*)provas);
}

void* soma_positivas(void *arg) {
	n_positivas++;
	pthread_exit((void*)NULL);
}

void* soma_negativas(void *arg) {
	n_negativas++;
	pthread_exit((void*)NULL);
}

void* calcula_finais(void *arg) {
	pthread_mutex_lock(&mux1);
	if(contador < 300) {
		provas[contador].notaFinal = ((provas[contador].notaG1 + provas[contador].notaG2 + provas[contador].notaG3)/3);
		if(provas[contador].notaFinal >= 50) {
			pthread_mutex_lock(&mux2);
			pthread_t c_positivas;
			pthread_create(&c_positivas, NULL, soma_positivas, (void*)NULL);
			pthread_join(c_positivas,(void*) NULL);
			pthread_mutex_unlock(&mux2);
		} else {
			pthread_mutex_lock(&mux3);
			pthread_t c_negativas;
			pthread_create(&c_negativas, NULL, soma_negativas, (void*)NULL);
			pthread_join(c_negativas,(void*) NULL);
			pthread_mutex_unlock(&mux3);
		}
		contador++;
	}
	pthread_mutex_unlock(&mux1);
	pthread_exit((void*)NULL);
}

int main(void) {
	srand((unsigned)time(NULL));
	n_positivas = 0;
	n_negativas = 0;	
	contador = 0;

	/* Criação da Thread Criadora */
	void *retorno;
	pthread_t criadora;
	pthread_create(&criadora, NULL, cria_provas, (void*) NULL);
	pthread_join(criadora, (void*)&retorno);
	provas = ((Prova*)retorno);
	
	/* Criação das Threads Calculadoras */
	pthread_mutex_init(&mux1, NULL);
	pthread_mutex_init(&mux2, NULL);
	pthread_mutex_init(&mux3, NULL);
	
	pthread_t calculadora1;
	pthread_t calculadora2;
		
	while(contador < 300) {
		pthread_create(&calculadora1, NULL, calcula_finais, (void*)NULL);
		pthread_create(&calculadora2, NULL, calcula_finais, (void*)NULL);
	
		pthread_join(calculadora1,(void*) NULL);
		pthread_join(calculadora2,(void*) NULL);
	}
	
	pthread_mutex_destroy(&mux1);
	pthread_mutex_destroy(&mux2);
	pthread_mutex_destroy(&mux3);
	
	/* Apresentação dos Dados */
	int p_positivas = ((n_positivas*100)/300);
	int p_negativas = ((n_negativas*100)/300);
	printf("### Estatísticas ###\n");
	printf("Percentagem de Positivas: %d%\n", p_positivas);
	printf("Percentagem de Negativas: %d%\n\n", p_negativas);
	printf("### Dados das Provas ###\n");
	int i;
	for(i = 0; i < 300; i++) {
		printf("Número do Aluno: %d\n", provas[i].numeroAluno);
		printf("Nota Final: %d\n", provas[i].notaFinal);
		printf("-----------------------\n");
	}
	
	free(retorno);	
	return 0;
}
