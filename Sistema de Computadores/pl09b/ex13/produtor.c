/* PL09B - Semáforos 
   Exercício 13
   PRODUTOR
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

#define N 3			

/* memória partilhada */
typedef struct {
	int p1[N];		/* prioridade mais alta */
	int p2[N];
	int p3[N];
	int p4[N];		/* prioridade mais baixa */
}shm;

int main(void) {
	sem_t *cheio;		/* indica se o buffer está cheio */
	sem_t *vazio;		/* indica se o buffer está vazio */
	sem_t *exclusao;	/* garante exclusão mútua no acesso à memória partilhada */
	int in = 0;
	int ler;			/* número a ser inserido no buffer */
	time_t t;
	
	/* criação dos semáforos */
	
	if((cheio = sem_open("ex13_cheio", O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	if((vazio = sem_open("ex13_vazio", O_EXCL, 0644, N)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	if((exclusao = sem_open("ex13_exclusao", O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	/* criação da zona de memória partilhada */
	int fd, data_size = sizeof(shm);
	shm *mybuffer;
		
	fd = shm_open("/ex13", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	mybuffer = (shm*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	
	srand((unsigned) time(&t));
	
	printf("===== PRODUTOR =====\n");
	ler = rand() % 4 + 1;
	sem_wait(vazio);
	sem_wait(exclusao);
	switch(ler) {
		case 1:
			mybuffer->p1[in] = ler;
			in= (in + 1) % N;
			break;
		case 2:
			mybuffer->p2[in] = ler;
			in= (in + 1) % N;
			break;
		case 3:
			mybuffer->p3[in] = ler;
			in= (in + 1) % N;
			break;
		case 4:
			mybuffer->p4[in] = ler;
			in= (in + 1) % N;
			break;
		default:
			printf("Erro: número gerado não é válido!\n");
			break;
	}
	sem_post(exclusao);
	sem_post(cheio);	
	
	return 0;
}
