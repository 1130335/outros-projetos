// SCOMP PL01 - Revisões de C
// Exercício 1

#include <stdio.h>

int soma_pares(int *vec, int n) {

	int soma;
	soma = 0;
	int i;
	for(i = 0; i<n; i++) {
		if((vec[i])%2 == 0) {
			soma = soma + (vec[i]);
		}
	}
	return soma;
	
}

int main(void) {

	int vec[] = {1,2,3,4,5};
	int *p_vec;
	p_vec = vec;
	int soma;
	soma = soma_pares(p_vec,5);
	printf("A soma dos elementos pares do vetor é igual a %d\n", soma);
	return 0;
	
}
