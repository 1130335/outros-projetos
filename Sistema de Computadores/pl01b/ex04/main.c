// SCOMP PL01 - Revisões de C
// Exercício 4

#include <stdio.h>

void subtrai_recursiva(int *vec, int n, int v) {
	
	if(n>0) {
		vec[n-1] = vec[n-1] - v;
		subtrai_recursiva(vec,n-1,v);
	}
}

int main(void) {
	
	int vec[] = {1,2,3,4,5};
	int *p_vec = vec;
	subtrai_recursiva(p_vec,5,1);
	int i;
	for(i=0;i<5; i++) {
		printf("Novo valor: %d\n", p_vec[i]);
	}
	return 0;
	
}
