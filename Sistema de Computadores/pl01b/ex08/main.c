// SCOMP PL02 - Revisões de C
// Exercício 8

#include <stdio.h>

void somabyte2(char x, int *vec1, int *vec2) {
	int tam_vec1 = vec1[0];
	int tam_vec2 = vec2[0];
	
	if(tam_vec1 != tam_vec2) {
		printf("Não é possível efetuar a soma dado que os vetores não têm o mesmo tamanho.\n");
	}
	
	int aux1;
	int cop_aux; 
	int i;
	for(i=1; i<tam_vec1; i++) {
		aux1 = vec1[i];
		cop_aux = aux1;
		aux1 = aux1 >> 8;
		aux1 = aux1 & 255;
		aux1 = aux1 + x;
		aux1 = aux1 << 8;
		aux1 = aux1 & 65280;
		cop_aux = cop_aux & 4294902015;
		cop_aux = cop_aux | aux1;
		vec2[i] = cop_aux;
		printf("Novo número: %d\n", vec2[i]);
		
	}
}

int main(void) {
	int vec1[] = {3, 1, 2};
	int vec2[3];
	vec2[0] = 3;
	int *v1 = vec1;
	int *v2 = vec2;
	char x = 'a';
	somabyte2(x,v1,v2);
	return 0;
	
}
