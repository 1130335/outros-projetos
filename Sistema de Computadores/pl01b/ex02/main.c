// SCOMP PL01 - Revisões de C
// Exercício 2

#include <stdio.h>

void subtrai_valor(int *vec, int n, int v) {
	int i;
	for(i=0; i<n; i++) {
		vec[i] = vec[i] - v;
	}
}

int main(void) {
	
	int vec[] = {1,2,3,4,5};
	int *p_vec = vec;
	subtrai_valor(p_vec,5,1);
	int i;
	for(i = 0; i<5; i++) {
		printf("Novo valor: %d\n",p_vec[i]);
	}
	return 0;
	
}
