// SCOMP PL01 - Revisões de C
// Exercício 3

#include <stdio.h>

int soma_pares_recursiva(int *vec, int n, int soma) {
	
	if (n<=0) {
		return soma;
	}
	
	if (n>0) {
		if(vec[n-1]%2 == 0) {
			soma = soma + vec[n-1];
		}
		soma_pares_recursiva(vec,n-1,soma);
	}
}

int main(void) {
	
	int vec[] = {1,2,3,4,5};
	int *p_vec = vec;
	int soma = 0;
	soma = soma_pares_recursiva(p_vec,5,soma);
	printf("A soma dos números pares é igual a %d\n", soma);
	return 0;
	
}
