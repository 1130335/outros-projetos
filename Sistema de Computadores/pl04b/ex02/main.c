// PL04B- PIPES
// Exercício 2

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 
#include <string.h>


#define BUFFER_SIZE 80

	
int main (void) {
	struct data{
		int numero;
		char nome[BUFFER_SIZE];
	};
	
	struct data b;
	char read_msg[BUFFER_SIZE];
	char write_msg[BUFFER_SIZE];
	int fd[2];
	int fd1[2];
	pid_t pid;
	
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		perror("Pipe failed");
		return 1;
	}
	if(pipe(fd1) == -1){
		perror("Pipe failed");
		return 1;
	}
	pid = fork();
	
	
	
	if(pid > 0){
		/* fecha a extremidade não usada */
		close(fd[0]);
		close(fd1[0]);
		/* escreve no pipe */
		gets(b.nome);
		gets(b.numero);
		write(fd[1],b.nome,strlen(b.nome)+1);	
		write(fd1[1],b.numero,strlen(b.numero)+1);		
		/* fecha a extremidade de escrita */
		close(fd[1]);
		close(fd1[1]);
	} 
	
	
	else{
		/* fecha a extremidade não usada */
		close(fd[1]);
		close(fd1[1]);
		/* lê dados do pipe */
		read(fd[0], read_msg, BUFFER_SIZE);
		printf("Filho leu: %s \n", read_msg);
		read(fd1[0], read_msg, BUFFER_SIZE);
		printf("Filho leu: %s", read_msg);
		/* fecha a extremidade de leitura */
		close(fd[0]);
		close(fd1[0]);
	}
	return 0;
}
