/* PL04B - Pipes
Exercício 10
...
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#define NUM_P 6

int main( void) {

	srand(time(NULL));
	int fd[NUM_P][2];
	int i;
	int numeroMaior;
	int numero;
	pid_t pid;
	int write_msg;
	
	for(i=0; i<NUM_P;i++){
			if( pipe(fd[i]) == -1) {
				perror ("Pipe failed");
				return 1;
			}
	}
	
	for(i=1;i<NUM_P;i++){
		
		pid = fork ();
		
		if (pid == -1){
			printf("Erro a criar o processo!\n");
			exit(EXIT_FAILURE);
		}
		
		if(pid == 0){
			break;
		}
	}
	
	if(pid > 0){
		
		close (fd[0][0]);
		write_msg= rand() % 50;
		printf("PAI : %d NOVO NUMERO : %d\n", getpid() , write_msg);
		write ( fd[0][1] , &write_msg , sizeof(write_msg));
		close (fd[0][1]);
		int status;
		int k;
		for(k=0;k<NUM_P-1;k++){
			wait(&status);
		}
		read ( fd[NUM_P-1][0] , &numeroMaior, sizeof(int));
		printf("MAIOR: %d \n", numeroMaior);
		exit(EXIT_SUCCESS);
	}
	
	int j, n;
	for( j=1; j < NUM_P; j++){
	
		if( pid == 0){
			
			if(j == i){
				
				n = rand() % 50-1;
				read ( fd[j-1][0] , &numero, sizeof(int));
				printf( "FILHO:%d NUMERO HERDADO: %d\n", j, numero);
				printf(" PID : %d NOVO NUMERO: %d\n", getpid() , n);
			
				if( numero > n ){
					write( fd[j][1], &numero, sizeof(numero));
				
				}else if(n >= numero){
					write ( fd[j][1], &n, sizeof(n));
				}
				close (fd[j-1][0]);
				close (fd[j][1]);
				
			
				exit(EXIT_SUCCESS);
			}
			
		}
	}
	return 0;
}
