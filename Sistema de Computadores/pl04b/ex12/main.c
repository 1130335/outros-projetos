/* PL04B - Pipes
Exercício 12
...
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>

#define BUFFER_SIZE 2000000

int main(void) { 
	int fd[2];
	pid_t pid;
	
	if( pipe(fd) == -1) {
		perror ("Pipe failed");
		return 1;
	}
	pid = fork ();
	
	if (pid == -1){

		printf("Erro a criar o processo!\n");
		exit(EXIT_FAILURE);
	}
	
	if( pid > 0 ){
		
		close(fd[0]);
		
		FILE *file;
		char txt[BUFFER_SIZE];
		file=fopen("fx.txt", "r");
		fgets(txt, sizeof(txt), file);
		fclose(file);
		
		write( fd[1], &txt, strlen(txt) +1);
		
		close(fd[1]);
		int status;
		wait(&status);
		
	}
	if( pid ==0){
		close(fd[1]);
		dup2(fd[0], 0);
		close(fd[0]);
		int ret = execlp ("od", "fx.txt", (char*) NULL);
		exit(ret);
				
	}
	
	
	
return 0;	
}
