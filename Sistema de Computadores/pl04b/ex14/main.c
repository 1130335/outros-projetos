// PL04B- PIPES
// Exercício testes

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 
#include <string.h>


#define BUFFER_SIZE 100
#define NUM_PROCESSOS 2
#define MAX_REGISTOS 10
#define STEP MAX_REGISTOS/NUM_PROCESSOS

typedef struct {
	int codigo_cliente;
	int codigo_produto;
	int quantidade;
	
} registo;

int main (void) {
	
	
	 
	int dados[NUM_PROCESSOS][2], i,j;
	
	registo registos[MAX_REGISTOS];
	
	registo read_registos[MAX_REGISTOS];
	
	registo write_maiores[MAX_REGISTOS];
	
	int write_contador, read_contador;
	
	
	pid_t pid;
	
	for(i=0;i<MAX_REGISTOS;i++){
		registos[i].quantidade=15+i;
	}
	
	for(i=0; i<NUM_PROCESSOS; i++) {
		pipe(dados[i]);
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
		
	if(pid == 0) {
		for(j=0; j<i; j++) {
			close(dados[j][0]);
			close(dados[j][1]);
		}
		close(dados[i][0]);
		write_contador=0;
		for(j=STEP*i; j < STEP*i+STEP; j++) {
			if(registos[j].quantidade>20){
			
			write_maiores[write_contador].quantidade=registos[j].quantidade;
			
			write_contador++;
			}
		}
		write(dados[i][1], &write_contador, sizeof(int));
		write(dados[i][1], &write_maiores, sizeof(registo)*write_contador);
		close(dados[i][1]);
	}
		
	if(pid>0) {
		for(j=0; j<NUM_PROCESSOS; j++) {
			close(dados[j][1]);
			read(dados[j][0], &read_contador, sizeof(int));
			read(dados[j][0], &read_registos, sizeof(registo)*read_contador);
			for(i=0; i<read_contador; i++) {
				printf("Maiores:%d\n", read_registos[i].quantidade);
			}
			
			close(dados[j][0]);
		}
		
		for(j=0; j<NUM_PROCESSOS; j++) {
			wait(NULL);
		}
		
		
	}
	
	return 0;
		
	
	
	return 0;
}
