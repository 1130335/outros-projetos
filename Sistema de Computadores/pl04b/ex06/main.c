/* PL04B - Pipes
Exercício 6
...
*/

/* PL04B - Pipes
Exercício 6
...
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_P 5
#define MAX_U 1000
#define STEP MAX_U/NUM_P

int main(void) {
	/* ... */
	int dados[NUM_P][2];
	
	for(j=0; j<MAX_U; j++) {
		/* ... */
	}
	
	for(i=0; i<NUM_P; i++) {
		pipe(dados[i]);
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
		
	if(pid == 0) {
		for(j=0; j<i; j++) {
			close(dados[j][0]);
			close(dados[j][1]);
		}
		close(dados[i][0]);
		for(j=STEP*i; j < STEP*i+STEP; j++) {
			res[j] = vec[j] + vec1[j];
			write(dados[i][1], &res[j], sizeof(int));
		}
		close(dados[i][1]);
	}
		
	if(pid>0) {
		for(j=0; j<NUM_P; j++) {
			close(dados[j][1]);
			for(i=0; i<STEP; i++) {
				read(dados[j][0], &res[STEP*j+i], sizeof(int));
			}
			close(dados[j][0]);
		}
		
		for(j=0; j<NUM_P; j++) {
			wait(NULL);
		}
		
		for(j=0; j<MAX_U; j++) {
			printf(...);
		}
	}
	
	return 0;
}
