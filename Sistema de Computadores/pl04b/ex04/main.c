// PL04B- PIPES
// Exercício 4

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 
#include <string.h>


#define BUFFER_SIZE 20

	
int main (void) {
	char url[]="file.txt";
	char ch;
	FILE *arq;
	int cont=0;
	char read_msg[BUFFER_SIZE];
	char write_msg[BUFFER_SIZE];
	int fd[2];
	pid_t pid;
	
	
	/* cria o pipe */
	if(pipe(fd) == -1){
		perror("Pipe failed");
		return 1;
	}
	
	pid = fork();
	
	
	
	if(pid > 0){
		/* fecha a extremidade não usada */
		close(fd[0]);
		/* fecha a extremidade não usada */
		arq = fopen(url, "r");
		if(arq == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
		else
			while((ch=fgetc(arq))!= EOF ){
			write_msg[cont]=ch;
			cont++;
			}
			fclose(arq);
		/* escreve no pipe */
		
		write(fd[1],write_msg,strlen(write_msg)+1);	
		
		/* fecha a extremidade de escrita */
		close(fd[1]);
		
	} 
	
	
	else{
		/* fecha a extremidade não usada */
		close(fd[1]);
		
		/* lê dados do pipe */
		read(fd[0], read_msg, BUFFER_SIZE);
		printf("Filho leu: %s \n", read_msg);
		
		/* fecha a extremidade de leitura */
		close(fd[0]);
	
	}
	return 0;
}
