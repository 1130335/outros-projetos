/* PL04A - Pipes
Exercício 5
Dados dois vectores de inteiros, de 1000 elementos cada, implemente um programa que crie 5 novos processos.
	* Cada um dos filhos é responsável por somar 200 posições (vec1[i] + vec2[i]), 
	enviando de seguida o resultado da soma parcial ao pai através de um pipe.
	* O pai dele ler do pipe as 5 somas parciais e calcular o resultado final.
Utiliza apenas um pipe, partilhado entre o pai e os 5 filhos. 
Tenha atenção que os cálculos dever ser efectuados de forma concorrente por todos os filhos.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	pid_t pid[5];
	int fd[2];
	
	/* Cria-se o pipe */
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return 1;
	}

	/* Cria-se os vetores necessários, com números aleatórios */
	int vec1[1000];
	int vec2[1000];
	int elem = 0;
	int i;
	
	for(i = 0; i<1000; i++) {
		vec1[i] = elem;
		elem++;
		vec2[i] = elem;
	}
	
	/* Cria-se os 5 processos */
	for(i = 0; i<5; i++) {
		pid[i] = fork();
		
		if(pid[i] == -1) {
			perror("A criação do processo falhou!");
			exit(EXIT_FAILURE);
		}
		
		if(pid[i] == 0) {
			break;
		}
	}
	
	if(pid[i] == 0) {
	
	int soma = 0;
	int j;
	
	/* Antes de começar as somas, verifica-se quais dos processos entrou para que não se faça a mesma conta
	 * cinco vezes seguidas e para que os cálculos sejam seguidos (ex. [0-200], [200-400], etc.).
	 */
	 
	if(i==0) {
		for(j=0; j<200; j++) {
		soma = soma + (vec1[j] + vec2[j]);
		}
	}
	
	if(i==1) {
		for(j=200; j<400; j++) {
		soma = soma + (vec1[j] + vec2[j]);
		}
	}
	
	if(i==2) {
		for(j=400; j<600; j++) {
		soma = soma + (vec1[j] + vec2[j]);
		}
	}
	
	if(i==3) {
		for(j=600; j<800; j++) {
		soma = soma + (vec1[j] + vec2[j]);
		}
	}
	
	if(i==4) {
		for(j=800; j<1000; j++) {
		soma = soma + (vec1[j] + vec2[j]);
		}
	}
	
	/* Escreve-se o resultado no pipe */
	write(fd[1], &soma, sizeof(int));
	
	exit(1);
	
	} 
	
	if(pid[0] > 0) {
		int soma_total = 0;
		int aux;
		int k;
		for(k=0; k<5; k++) {
			read(fd[0], &aux, sizeof(int));
			soma_total = soma_total + aux;
		}
		printf("A soma total é igual a %d\n", soma_total);
		
		/* Fecha-se as extremidades */
		close(fd[0]);
		close(fd[1]);
	}
		
	return 0;
}
