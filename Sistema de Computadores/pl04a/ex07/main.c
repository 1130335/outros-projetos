/* PL04A - Pipes
Exercício 7
Implemente um programa que cria 10 novos processos para simular um jogo "Corrida ao pipe".
Deverá existir um único pipe partilhando entre o pai e os 10 processos filho.
	* O pai escreve, em intervalos de 2 segundos, uma estrutura com a mensagem "Win!" e o número da ronda (1 a 10) no pipe.
	* Cada um dos filhos está a tentar ler os dados enviados do pipe. O processo que o conseguir imprime a mensagem e o seu pid, 
	terminando de seguida com um valor igual ao nº da ronda em que conseguiu ganhar o acesso ao pipe.
	* Os restantes processos continuam a tentar ler a mensagem do pipe.
O pai, depois de enviar 10 mensagens, deve imprimir o número da ronda em que cada um dos filhos ganhou o acesso ao pipe.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct gameround {
	char* res;
	int num_round;
}gameround;

int main(void) {
	int fd[2];
	pid_t pid[10];
	gameround round[10];
	
	/* Cria-se o pipe */
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return 1;
	}
	
	/* Criam-se os 10 processos */
	int i;
	for(i = 0; i < 10; i++) {
		round[i].res = "Win!";
		round[i].num_round = i+1;
			
		pid[i] = fork();
		
		if(pid[i] == -1) {
			perror("Erro na criação do processo!");
			exit(EXIT_FAILURE);
		}
		
		if(pid[i] == 0) {
			break;
		}
	}
	
	if(pid[i] > 0) {
		int j;
		
		/* Processa-se os dados para escrever no pipe */
		for(j=0; j < 10; j++) {
			/* Fecha-se a extremidade de leitura */
			close(fd[0]);
			
			write(fd[1], &round, sizeof(round));
			
			/* Fecha-se a extremidade de escrita */
			close(fd[1]);
			sleep(2);
		}
				
		int status;
		pid_t pid_filho;
		printf("\n");
		for(j=0; j<10; j++) {
			pid_filho = wait(&status);
			printf("O filho %d conseguiu ler na ronda %d.\n", pid_filho, WEXITSTATUS(status));
		}
	}
	
	if(pid[i]==0) {
		/* Fecha-se a extremidade de escrita */
		close(fd[1]);
		
		/* Lê-se os dados do pipe */
		gameround myround;
		read(fd[0],&myround,8);
		
		/* Fecha-se a extremidade de leitura */
		close(fd[0]);
		
		printf("Ronda %d: O meu PID é igual a %d e a mensagem é %s\n", myround.num_round, getpid(), myround.res);
		exit(myround.num_round);
	}
			
	return 0;
}
