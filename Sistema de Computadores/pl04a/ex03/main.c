/* PL04A - Pipes
Exercício 3
Implemente um programa que crie um novo processo e estabelece com ele uma ligação usando um pipe.
O pai deve enviar duas mensagens, "Hello World!" e "Goodbye!", terminando em seguida a ligação.
Por sua vez, o processo filho deverá imprimir no ecrã as mensagens à medida que as for recebendo, 
terminando assim que o processo pai feche a ligação.
O pai deve aguardar que o filhe termine antes de ele próprio terminar.
Não se esqueça de, em cada um dos processos, fechar os descritores que não vão ser usados.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 80

int main(void) {
	pid_t pid;
	int fd[2];
	
	char read_msg1[BUFFER_SIZE];
	char read_msg2[BUFFER_SIZE];
	char write_msg1[] = "Hello World!";
	char write_msg2[] = "Goodbye!";
	
	/* Cria-se o pipe */
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return 1;
	}
	
	/* Cria-se um novo processo */
	pid = fork();
	
	/* Verifica-se se houve erros na criação do processo */
	if(pid == -1) {
		perror("A criação do processo falhou!");
		exit(EXIT_FAILURE);
	}
	
	/* Processo Pai */
	if(pid > 0) {
		int tam_msg1;
		int tam_msg2;
	
		/* Verifica-se o tamanho de cada mensagem */
		tam_msg1 = strlen(write_msg1) + 1;
		tam_msg2 = strlen(write_msg2) + 1;
		
		/* Fecha-se a extremidade de leitura */
		close(fd[0]);
		
		/* Escreve-se no pipe o que se pretende enviar ao filho, enviando primeiro o tamanho da mensagem */
		write(fd[1], &tam_msg1, sizeof(int));
		write(fd[1], &write_msg1, tam_msg1);
		write(fd[1], &tam_msg2, sizeof(int));
		write(fd[1], &write_msg2, tam_msg2);
		
		/* Fecha-se a extremidade de escrita */
		close(fd[1]);
		
		wait(NULL);
	}
	
	/* Processo Filho */
	if(pid == 0) {
		int aux;
		
		/* Fecha-se a extremidade de escrita */
		close(fd[1]);
		
		/* Lê-se o conteúdo do pipe, primeiro sabendo o tamanho da mensagem a ler*/
		read(fd[0], &aux, sizeof(int));
		read(fd[0], &read_msg1, aux);
		read(fd[0], &aux, sizeof(int));
		read(fd[0], &read_msg2, aux);
		printf("1ª Mensagem: %s\n", read_msg1);
		printf("2ª Mensagem: %s\n", read_msg2);
		
		/* Fecha-se a extremidade de leitura */
		close(fd[0]);
		exit(0);
	}

	return 0;
}
