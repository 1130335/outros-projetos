/* PL04A - Pipes
Exercício 1
Implemente um programa que crie um novo processo e estabelece com ele uma ligação usando um pipe. 
O pai começa por imprimir o seu pid, enviando-o de seguida ao seu filho através do pipe. 
O processo filho deve ler do pipe o pid enviado e apresentá-lo no ecrã.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	pid_t pid;
	int fd[2];
	int read_msg;
	int write_msg;
	
	/* Cria-se o pipe */
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return 1;
	}
	
	/* Cria-se um novo processo */
	pid = fork();
	
	/* Verifica-se se houve erros na criação do processo */
	if(pid == -1) {
		perror("A criação do processo falhou!");
		exit(EXIT_FAILURE);
	}
	
	/* Processo Pai */
	if(pid > 0) {
		/* Fecha-se a extremidade que não se vai usar, neste caso a de leitura */
		close(fd[0]);
		/* Escreve-se no pipe o que se pretende enviar ao filho */
		write_msg = getppid();
		printf("PAI: O PID do pai é igual a %d\n", write_msg);
		write(fd[1], &write_msg, sizeof(int));
		/* Quando se acabou de escrever, fecha-se a extremidade de escrita */
		close(fd[1]);
	}
	
	/* Processo Filho */
	if(pid == 0) {
		/* Fecha-se a extremidade de escrita */
		close(fd[1]);
		/* Lê-se o conteúdo do pipe */
		read(fd[0], &read_msg, sizeof(int));
		printf("FILHO: O PID do pai é igual a %d\n", read_msg);
		
		/* Fecha-se a extremidade de leitura */
		close(fd[0]);
	}

	return 0;
}
