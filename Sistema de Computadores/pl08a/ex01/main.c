/* PL08A - Semáforos 
   Exercício 1
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

#define NUM_FILHOS 5

int main(int argc, char *argv[]) {
	pid_t pid[NUM_FILHOS];
	FILE *file;
	file = fopen("ex01.txt", "a");
	sem_t *semaforo;
	/* cria o semáforo */
	if((semaforo = sem_open("semaforo", O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	int i;
	/* cria os 5 filhos */
	for(i = 0; i<NUM_FILHOS; i++) {
		pid[i] = fork();
		if(pid[i] < 0) {
			perror("Erro na clonagem do processo");
			exit(EXIT_FAILURE);
		} else if(pid[i] == 0) {
			break;
		}
	}
	
	if(pid[i] == 0) {
		// printf("Filho %d: Entrei!\n", i);
		sem_wait(semaforo);
		// printf("Filho %d: Estou aqui!\n", i);
		int j;
		for(j = 1; j < 201; j++) {
			fprintf(file, "%d\n", j);
		}
		sem_post(semaforo);
		// printf("Filho %d: Saí!\n", i);
		exit(EXIT_SUCCESS);
	}
	
	if(pid[0] > 0) {
		int j;
		for(j = 0; j<NUM_FILHOS; j++) {
			wait(NULL);
		}
		fclose(file);
		int num = 0;
		char line[100];
		file = fopen("ex01.txt", "rt");
		while(fgets(line, 100, file) != NULL) {
			sscanf(line, "%d", &num);
			printf("%d\n", num);
		}
		fclose(file);
	}
	
	return 0;
}

