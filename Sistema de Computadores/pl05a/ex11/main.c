/* PL05A - Pipes
Exercício 11
Cada vez mais hipermercados têm à disposição dos seus clientes dispositivos que permitem saber o preço dum dado produto pelo seu código de barras.
Os dispositivos estão distribuídos pela superfície do hipermercado, têm um leitor de códigos de barras e um mostrador.
O cliente passa o código de barras do produto no leitor e é mostrado o nome do produto e respectivo preço.
Simule o funcionamento destes dispositivos usando processos que comunicam através de pipes.
	* O pai tem acesso aos códigos de barras, nomes e preços de todos os produtos.
	* O pai cria um pipe para o qual todos os processos filho escrevem.
	* O pai, depois de obter a informação sobre o produto em causa, envia-a ao filho que a solicitou usando um pipe que partilha apenas com ele.
Nota: Tenha em atenção a quantidade de informação que é necessária enviar pelo filho para que o pai lhe possa responder.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hiper {
	int codigo;
	char* nome;
	double preco;
}hiper;

int main(void)  {
	hiper hipermercado[2];
	hipermercado[0].codigo = 1;
	hipermercado[0].nome = "Carne";
	hipermercado[0].preco = 2.50;
	hipermercado[1].codigo = 2;
	hipermercado[1].nome = "Peixe";
	hipermercado[1].preco = 5.00;
	pid_t pid[2];
	int fd[2];
	int i;
	
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return -1;
	}
	
	int fd1[2];
	if(pipe(fd1) == -1) {
		perror("A criação do pipe falhou!");
		exit(EXIT_FAILURE);
	}
	
	int fd2[2];
	if(pipe(fd2) == -1) {
		perror("A criação do pipe falhou!");
		exit(EXIT_FAILURE);
	}
	
	for(i = 0; i<2; i++) {
		pid[i] = fork();
		
		if(pid[i] == -1) {
			perror("A criação do processo falhou!");
			exit(EXIT_FAILURE);
		}
		
		if(pid[i] == 0) {
			break;
		}
	}
	
	if(i == 0) {
		close(fd1[1]);
		close(fd2[0]);
		close(fd2[1]);
		
		int cod1 = 1;
		write(fd[1], &cod1, sizeof(int));
		hiper prod1;
		read(fd1[0], &prod1, sizeof(hiper));
		printf("Informação do produto:\nCódigo de barras: %d\nNome: %s\nPreço: %f €\n", prod1.codigo, prod1.nome, prod1.preco);
		close(fd1[0]);
	}
	
	if(i == 1) {
		close(fd1[0]);
		close(fd1[1]);
		close(fd2[1]);
		
		int cod2 = 2;
		write(fd[1], &cod2, sizeof(int));
		hiper prod2;
		read(fd2[0], &prod2, sizeof(hiper));
		printf("Informação do produto:\nCódigo de barras: %d\nNome: %s\nPreço: %f €\n", prod2.codigo, prod2.nome, prod2.preco);	
		close(fd2[0]);
	}
	
	if(i == 2) {
		close(fd[1]);
		close(fd1[0]);
		close(fd2[0]);
		int j, cod;
		for(j=0; j<2; j++) {
			read(fd[0], &cod, sizeof(int));
			if(cod == hipermercado[0].codigo) {
				write(fd1[1], &hipermercado[0], sizeof(hiper));
				close(fd1[1]);
			}
			
			if(cod == hipermercado[1].codigo) {
				write(fd2[1], &hipermercado[1], sizeof(hiper));
				close(fd2[1]);
			}
		}
	}
	
	return 0;
}
