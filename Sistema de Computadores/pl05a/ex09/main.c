/* PL05A - Pipes
Exercício 9
Implemente um programa que simula um sistema de apostas. Assuma que o processo filho começa o jogo com 20 euros.

	* O pai lê do teclado um número inteiro (entre 1 e 5).
	* De seguida, informa ao filho, usando um pide, de que: (i) pode tentar acertar no número, enviando-lhe o valor 1; 
	ou (ii) o jogo terminou, enviando-lhe o valor 0.
	* O filho aguarda instruçoes do pai para realizar a sua aposta ou terminar. 
	Para apostar, deve gerar um número aleatório entre 1 e 5 e enviá-lo ao pai usando um pipe.
	* O pai aguarda pela aposta do filho e determina se esta é ou não acertada. 
	Se acertou, aumenta em 10 euros o saldo actual. Caso contrário, retira 5 euros. De seguida, envia o saldo actual ao filho.
	
Nota 1: Considere que o programa irá funcionar de forma ciclica até que não exista saldo ou o utilizador insira o número -1.
Nestes casos deve ser enviado o valor 0 ao filho, informando-o que o jogo terminou. 
O pai deve esperar que o filho termine antes de ele próprio terminar.
Nota 2: Tenha em atenção que para este exercício um pipe não será suficiente. 
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	pid_t pid;
	int fd_pai[2];
	int fd_filho[2];
	int saldo = 20;
	
	if(pipe(fd_pai) == -1) {
		perror("A criação do pipe falhou!");
		return -1;
	}
	
	if(pipe(fd_filho) == -1) {
		perror("A criação do pipe falhou!");
		return -1;
	}
	
	pid = fork();
	
	if(pid == -1) {
		perror("A criação do processo falhou!");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0) {
		close(fd_pai[0]);
		close(fd_filho[1]);
		
		int num_escolhido = 0;
		int jogo = 1;
		while(num_escolhido!=-1 && saldo != 0) {
			printf("\nNúmero: ");
			scanf("%d", &num_escolhido);
			printf("Número escolhido: %d\n", num_escolhido);
			if(num_escolhido < 0 && num_escolhido > -2) {
				jogo = 0;
				write(fd_pai[1], &jogo, sizeof(int));
				close(fd_pai[1]);
				close(fd_filho[0]);
			} else {
				jogo = 1;
				write(fd_pai[1], &jogo, sizeof(int));
				int num_af;
				read(fd_filho[0], &num_af, sizeof(int));
				if(num_af == num_escolhido) {
					printf("O processo acertou!\n");
					saldo = saldo + 10;
					write(fd_pai[1], &saldo, sizeof(int));
				} else {
					printf("O processo não acertou!\n");
					saldo = saldo - 5;
					write(fd_pai[1], &saldo, sizeof(int));
				}
			}
		}
	}
	
	if(pid == 0) {
		close(fd_pai[1]);
		close(fd_filho[0]);
		
		int jogo;
		read(fd_pai[0], &jogo, sizeof(int));
		
		if(jogo == 0) {
			close(fd_pai[0]);
			close(fd_filho[1]);
			exit(EXIT_SUCCESS);
		}
		
		int aposta = (rand()%5) + 1;
		printf("O filho aposta %d\n", aposta);
		write(fd_filho[1], &aposta, sizeof(int));
		
		read(fd_pai[0], &saldo, sizeof(int));
		printf("O meu saldo: %d\n", saldo);
		
	}
	
	
	return 0;
}
