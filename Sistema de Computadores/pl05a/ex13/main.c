/* PL05A - Pipes
Exercício 13
Implemente um programa que crie um novo processo.
	* O filho executa o comando sort fx.txt e envia o resultado ao processo pai.
	* O pai lê o output produzido pelo filho e apresenta o ficheiro ordenado no ecrã.
Como resultado, espera-se obter o mesmo comando sort fx.txt executado na shell.
Sugestão: redireccione o Standart Output do processo filho para escrever dados do pipe.
Utilize as funções exex para executar o comando sort.
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	pid_t pid;
	int fd[2];
	
	if(pipe(fd) == -1) {
		perror("A criação do pipe falhou!");
		return -1;
	}
	
	pid = fork();
	
	if(pid == -1) {
		perror("A criação do processo falhou!");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0) {
		wait(NULL);
		close(fd[0]);
		close(fd[1]);		
	}
	
	if(pid == 0) {
		close(fd[0]);
		dup2(fd[1], 1);
		close(fd[1]);
		
		execlp("sort", "sort", "fx.txt", (char*) NULL);
	}
	return 0;
}
