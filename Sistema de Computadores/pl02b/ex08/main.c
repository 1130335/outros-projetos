// PL02B- Processos e Funções Exec
// Exercício 8

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 
int main (void) {
	int a, b,c,d;
	
	a=0;
	b=fork();
	printf("first fork ");
	b=fork();
	printf("second fork ");
	b=fork();
	printf("third fork ");
	c=getpid();
	d=getppid();
	a=a+5;
	printf("\na=%d, b=%d, c=%d d=%d\n", a, b,c,d);
	
}
