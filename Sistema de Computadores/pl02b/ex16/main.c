// PL02B- Processos e Funções Exec
// Exercício 16


#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	char buffer[80];
	int estado;
	pid_t p;

	while (strcmp("sair", buffer) != 0) {
		printf("Introduza um comando: ");
		scanf("%s", buffer);

		p = fork();
		if (p == 0 && strcmp("ls-la", buffer) == 0) {
			execlp("ls", "ls","-la",(char*)NULL);
			exit(0);
		}
		if (p == 0) {
			execlp(buffer, buffer, (char*)NULL);
			exit(0);
		} else if (p < 0) {
			printf("ERRO!");
			exit(-1);
		} else {
			if (wait(&estado)) {
				if (WIFEXITED(estado)) {
					if (WEXITSTATUS(estado) == 0) {
						printf("\nO comando %s foi processado correctamente!\n");
					} else {
						printf("O comando %s nao foi processado correctamente!\n");
					}
				}
			}
		}
	}

	return 0;
}

