// PL02B- Processos e Funções Exec
// Exercício 10


#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	pid_t p[5];
	int vec[100000];
	int i;
	int j;
	int num = 50000;
	int estado;
	
	for (i = 0; i < 100000; i++) { //PREENCHE O VECTOR
		vec[i] = i+1;
	}
	
	for (i = 0; i < 5; i++) {
		p[i] = fork();
		
		if (p[i] == 0) { //FILHO
			for (j = 20000*i; j < 20000+20000*i; j++) {
				if (num == vec[j]) {
					printf("O numero encontra-se na posicao %d\n", (j+1));
					
					exit(i+1);
				}
			}
			
			exit(0);
		} else if (p[i] < 0) {
			printf("ERRO!");
			exit(-1);
		}
	}
	
	for (i = 0; i < 5; i++) {
		waitpid(p[i], &estado, 0);
		
		if (WIFEXITED(estado)) {
			if (WEXITSTATUS(estado) != 0) {
				printf("O numero foi encontrado no processo %d\n", WEXITSTATUS(estado));
			}
		}
	}
	
	return 0;
}
