// PL02B- Processos e Funções Exec
// Exercício 9

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 

void printaNum(int num){
	int i;
	for(i=num;i<num + 20;i++){
		printf("%d \n", i);
	}
}

int main(void) {
	int pid, estado; 
	
	pid=fork();
	
	
	if(pid>0){
		wait(&estado);
		printf("Processo: %d \n", getpid());
		printaNum(100);
		
	}else{
		if(pid==0){
			pid=fork();
		
			if(pid>0){
				wait(&estado);
				printf("Processo: %d \n", getpid());
				printaNum(80);	
			}else{
				if(pid==0){
					pid=fork();
		
					if(pid>0){
						wait(&estado);
						printf("Processo: %d \n", getpid());
						printaNum(60);
					}else{
						if(pid==0){
							pid=fork();
		
							if(pid>0){
								wait(&estado);
								printf("Processo: %d \n", getpid());
								printaNum(40);
							}else{
								if(pid==0){
									pid=fork();
		
									if(pid>0){
										wait(&estado);
										printf("Processo: %d \n", getpid());
										printaNum(20);
									}else{
										if(pid==0){
											pid=fork();
		
											if(pid>0){
												wait(&estado);
												printf("Processo: %d \n", getpid());
												printaNum(0);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
		
}

