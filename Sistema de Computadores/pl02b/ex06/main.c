// PL02B- Processos e Funções Exec
// Exercício 6

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 
int main () {
	int dados[100];
	int resultado[100];
	int i;
	int estado;
	for (i = 0; i < 100; i++) { //CADA PROCESSO CALCULA 50000 POSICOES
			dados[i]=1/4;
	}


	pid_t p = fork();

	if (p > 0) { //PAI
		for (i = 0; i < 50; i++) { //CADA PROCESSO CALCULA 50000 POSICOES
			resultado[i] = dados[i]*4 + 20;
			printf("%d \n", resultado[i]);
		}

		wait(&estado); //ESPERA PELO PROCESSO-FILHO

		for (i = 0; i < 50; i++) { //CADA PROCESSO PRINTA 50000 POSICOES
			printf("%d ", resultado[i]);
		}
	} else if (p == 0) {
		for (i = 50; i < 100; i++) { //CADA PROC. CALCULA 50000 POSICOES
			resultado[i] = dados[i]*4 + 21;
			printf("%d \n", resultado[i]);
		}

		for (i = 50; i < 100; i++) { //CADA PROC. PRINTA 50000 POSICOES
			printf("%d ", resultado[i]);
		}
	} else if (p < 0) {
		printf("ERRO!");
		exit(-1);
	}

	return 0;
}
