// PL02A - Processos e Funções Exec
// Exercício 1

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main(void) {
	pid_t p, a;
	p = fork();
	
	if (p == -1) {
		perror("O fork p falhou");
		exit(EXIT_FAILURE);
	}
	
	a = fork();
		
	if (a == -1) {
		perror("O fork a falhou");
		exit(EXIT_FAILURE);
	}
	
	printf("Sistemas de Computadores\n");
	return 0;
}
