// PL02A - Processos e Funções Exec
// Exercício 2

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(void) {
	pid_t f1, f2;
	int pid_pai;
	int status;
	pid_pai = getppid();
	
	printf("Eu sou o pai\n");
	printf("PID = %d\n", pid_pai);
	f1 = fork();
	
	if (f1 == -1) {
		perror("O fork f1 falhou");
		exit(EXIT_FAILURE);
	}
	
	if (f1 == 0) {
		printf("Eu sou o 1º filho\n");
		printf("PID = %d\n", getpid());
		sleep(5);
		exit(1);
	}
	
	waitpid(f1, &status, 0);
	if(WIFEXITED(status)) {
		printf("Pai: o filho %d retornou o valor: %d\n", f1, WEXITSTATUS(status));
	}
	f2 = fork();
		
	if (f2 == -1) {
		perror("O fork f2 falhou");
		exit(EXIT_FAILURE);
	}
		
	if (f2 == 0) {
		printf("Eu sou o 2º filho\n");
		printf("PID = %d\n", getpid());
		exit(2);
	}
	
	waitpid(f2, &status, 0);
	if(WIFEXITED(status)) {
		printf("Pai: o filho %d retornou o valor: %d\n", f2, WEXITSTATUS(status));
	}
	
	return 0;
}
