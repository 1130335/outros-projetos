// PL02A - Processos e Funções Exec
// Exercício 4

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>

// Alterações da alínea d) : modificação do for para criar apenas 3 filhos
// Alterações da alínea e) : tratamento das situações de erro
// Alterações da alínea f) : o pai espera pelo término dos filhos

/*
int main(void) {
	pid_t pid;
	pid = getpid();
	int	f;
	int status;
	for(f=0; f<3; f++) {
		if(pid > 0) {
			pid = fork();
			wait(&status);
			printf("Eu sou o PAI\n");
		}
			
		if(pid==-1) {
			perror("A criação do fork falhou\n");
			exit(EXIT_FAILURE);
		}
			
		if(pid==0) {
			printf("Eu sou filho\n");
			exit(2);
		}
	}
	return 0;
}
*/

// Alterações da alínea g) : cada filho devolve o número de término ao pai e o seu PID
// Alterações da alínea h) : o pai espera apenas pelo segundo filho, sem bloquear

int main(void) {
	pid_t f1, f2, f3;
	int status;
	f1 = fork();
	
	if(f1 == -1) {
		perror("A criação do filho falhou\n");
		exit(EXIT_FAILURE);
	}
	
	if(f1 == 0) {
		exit(1);
	}
	
	f2 = fork();

	if(f2== -1) {
		perror("A criação do filho falhou\n");
		exit(EXIT_FAILURE);
	}
	
	if(f2 == 0) {
		exit(2);
	}
	
	f3 = fork();
			
	if(f3 == -1) {
		perror("A criação do filho falhou\n");
		exit(EXIT_FAILURE);
	}
	
	if(f3 == 0) {
		exit(3);
	}
	
	/*
	int i;
	pid_t aux;
	for(i=0; i<3; i++) {
		aux = wait(&status);
		if(WIFEXITED(status)) {
			printf("\nFilho PID: %d \nNúmero de Ordem: %d \n", aux, WEXITSTATUS(status));
		}
	} */
	
	waitpid(f2, &status, WNOHANG);
	printf("Eu sou o pai\n");
	
	return 0;
}

