/* PL10A - Threads
   Exercício 1
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	int numero;
	char* nome;
	char* morada;
}posicao_vetor;

void* thread_func(void *arg) {
	posicao_vetor *posicao = (posicao_vetor*)arg;
	printf("Número: %d\nNome: %s\nMorada: %s\n\n", (*posicao).numero, (*posicao).nome, (*posicao).morada);
	pthread_exit((void*)NULL);
}

int main(void) {
	posicao_vetor vetor[5];
	pthread_t threads[5];
	
	/* Dados do Vetor */
	vetor[0].numero = 1;
	vetor[0].nome = "Nome1";
	vetor[0].morada = "Morada1";
	
	vetor[1].numero = 2;
	vetor[1].nome = "Nome2";
	vetor[1].morada = "Morada2";
	
	vetor[2].numero = 3;
	vetor[2].nome = "Nome3";
	vetor[2].morada = "Morada3";
	
	vetor[3].numero = 4;
	vetor[3].nome = "Nome4";
	vetor[3].morada = "Morada4";
	
	vetor[4].numero = 5;
	vetor[4].nome = "Nome5";
	vetor[4].morada = "Morada5";
	
	/* Criação das Threads */
	int i;
	for(i = 0; i<5; i++) {
		pthread_create(&threads[i], NULL, thread_func, (void*) &vetor[i]);
		pthread_join(threads[i], NULL);
	}
	return 0;
}
