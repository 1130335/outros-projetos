/* PL07A - Memória Partilhada
Exercício 3 
...
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

typedef struct {
	int numero;
} shared_data_type;

int main(int argc, char *argv[]) {
	int fd, data_size = sizeof(shared_data_type);
	pid_t pid;
	shared_data_type *shared_data;
	if(fd == NULL) {
		fd = shm_open("/numero", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
		ftruncate(fd, data_size);
		shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	}
		
	shared_data->numero = 100;
	
	pid = fork();
	
	if(pid == -1) {
		perror("Erro na clonagem do processo!");
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0) {
		int i;
		for(i = 0; i<1000; i++) {
			shared_data->numero = shared_data->numero + 1;
		}
		
		for(i = 0; i<1000; i++) {
			shared_data->numero = shared_data->numero - 1;
		}
		
		for(i = 0; i<1000; i++) {
			printf("\n%d. Pai_Número: %d", i+1, shared_data->numero);
		}
		
		
	} else {
		int i;
		for(i = 0; i<1000; i++) {
			shared_data->numero = shared_data->numero + 1;
		}
		
		for(i = 0; i<1000; i++) {
			shared_data->numero = shared_data->numero - 1;
		}
		
		for(i = 0; i<1000; i++) {
			printf("\n%d. Filho_Número: %d", i+1, shared_data->numero);
		}
		
		exit(1);
	}
	
	return 0;
}
