/* PL05A - Memória Partilhada
Exercício 4 

*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>


#define NUM_PROCESSOS 10
#define MAX_REGISTOS 1000
#define STEP MAX_REGISTOS/NUM_PROCESSOS

typedef struct {
	int vector[NUM_PROCESSOS];
} shared_data_type;

int main(int argc, char *argv[]) {
	/* iniciar o gerador */
	srand((unsigned) time(NULL));
	/* gerar numero aleatorio */
	
	int i=0;
	int j, r;
	int vec[1000];
	pid_t pid;
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	
	fd = shm_open("/vector", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	for(i=0;i<1000;i++){
		vec[i]=rand()% 1000 + 1;
	}
	
	for(i=0; i<NUM_PROCESSOS; i++) {
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
		
	if(pid == 0) {
		int maior=0;
		for(j=STEP*i; j < STEP*i+STEP; j++) {
			if(maior<vec[j]){
			maior=vec[j];
			}
		}
		printf("Maior local do processo filho %d: %d\n",i,maior);
		shared_data->vector[i]=maior;
	}
		
	if(pid>0) {
		for(j=0; j<NUM_PROCESSOS; j++) {
			wait(NULL);
		}
		
		int maior=0;
		for(j=0; j<NUM_PROCESSOS; j++) {
			if(maior<shared_data->vector[j]){
			maior=shared_data->vector[j];
			}
		}
		printf("Maior global: %d\n",maior);
		r = munmap(shared_data, data_size); /* desfaz mapeamento */
		if (r < 0) exit(1); /* verifica erro */
		r = shm_unlink("/vector"); /* remove do sistema */
		if (r < 0) exit(1); /* verifica erro */
		exit(0);
		
	}
	

	return 0;
}
