/* PL05A - Memória Partilhada
Exercício 1 - Leitor
Implemente um programa que permita comunicar o número e o nome de um aluno entre dois processos não relacionados hierarquicamente, um escritor e um leitor.
	* O escritor deve criar uma zona de memória partilhada, ler os dados a partir do teclado e escrevê-los na memória partilhada.
	* O leitor deve le⁻los da zona de memória partilhada e imprimi-los no ecrã.
Nota: o escritor deve ser executado antes do leitor.
*/

#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

typedef struct {
	int numero;
	char nome[80];
} shared_data_type;

int main(int argc, char *argv[]) {
	
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/data_aluno", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	printf("SOU O LEITOR\n");
	printf("Número Lido: %d\n", shared_data->numero);
	printf("Nome Lido: %s\n", shared_data->nome);
	
	return 0;
}
