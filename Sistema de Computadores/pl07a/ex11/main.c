/* PL07A - Memória Partilhada
Exercício 11
...
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

#define NP 10

typedef struct {
	char* caminho[NP];
	char* palavra[NP];
	int num_ocorrencias[NP];
} shared_data_type;

int main(void) {
	int fd, data_size = sizeof(shared_data_type);
	pid_t pid;
	shared_data_type *shared_data;
	fd = shm_open("/contagem", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	
	int i;
	for(i = 0; i<NP; i++) {
		shared_data->caminho[i] = "/media/partilha/scomp14152dbg03/pl07a/ex11/texto.txt";
		shared_data->palavra[i] = "texto";
		shared_data->num_ocorrencias[i] = 0;
		pid = fork();
		
		if(pid == -1) {
			perror("Erro na criação do processo");
			exit(EXIT_FAILURE);
		}
		
		if(pid == 0) {
			break;
		}
	}
	
	if(pid > 0) {
		int j;
		for(j = 0; j < NP; j++) {
			wait(NULL);
		}
		
		for(j = 0; j < NP; j++) {
			printf("%d. Número de ocorrências: %d\n", j+1, shared_data->num_ocorrencias[j]);
		}
		
		munmap(shared_data, data_size);
		close(fd);
		shm_unlink("/contagem");
	}
	
	if(pid == 0) {
		FILE *sf;
		sf = fopen(shared_data->caminho[i], "r");
		if(!sf) {
			perror("Não foi possível abrir o ficheiro.");
			exit(EXIT_FAILURE);
		}
		
		char str[6];
		while(fgets(str, 6, sf)) {
			printf("%s\n", str);
			if(strcmp(str, shared_data->palavra[i])){
				shared_data->num_ocorrencias[i] = shared_data->num_ocorrencias[i] + 1;
			}
		}
		fclose(sf);
		
	}
			
	return 0;
}
