/* PL07A - Memória Partilhada
Exercício 5 
...
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

#define NUM_PROCESSOS 5
#define STR_SIZE 100

typedef struct {
	char frase[STR_SIZE];
	int new[NUM_PROCESSOS-1];
} shared_data_type;

int main(void) {
	int fd, data_size = sizeof(shared_data_type);
	pid_t pid;
	shared_data_type *shared_data;
	fd = shm_open("/msg_cooperativa", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	shared_data->new[0] == 0;
	shared_data->new[1] == 0;
	shared_data->new[2] == 0;
	shared_data->new[3] == 0;
	
	int i;
	for(i=0; i<NUM_PROCESSOS; i++) {
		pid = fork();
		
		if(pid == -1) {
			perror("A criação do processo falhou!");
			exit(EXIT_FAILURE);
		}
		
		if(pid == 0) {
			break;
		}
	}
	
	if(pid == 0) {
		while(i == 1 && shared_data->new[0] == 0);
		while(i == 2 && shared_data->new[1] == 0);
		while(i == 3 && shared_data->new[2] == 0);
		while(i == 4 && shared_data->new[3] == 0);
		
		char aux[STR_SIZE];
		printf("\nIntroduza uma palavra: ");
		scanf("%[^\n]s", aux);
		strcat(shared_data->frase, aux);
		
		if(i == 0) {
			shared_data->new[0] = 1;
		}
		
		if(i == 1) {
			shared_data->new[1] = 1;
		}
		
		if(i == 2) {
			shared_data->new[2] = 1;
		}
		
		if(i == 3) {
			shared_data->new[3] = 1;
		}
		
		exit(1);
	}
	
	if(pid > 0) {
		int j;
		for(j = 0; j < NUM_PROCESSOS; j++) {
			wait(NULL);
		}
		
		printf("\nA frase formada foi: %s\n", shared_data->frase);
		munmap(shared_data, data_size);
		close(fd);
		shm_unlink("/msg_cooperativa");
	}
		
	return 0;
}
