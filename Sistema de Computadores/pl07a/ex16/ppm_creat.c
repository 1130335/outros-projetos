#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MAX_COLOR_VALUE     	255 
#define BUFFER_LENGTH     	50
#define NUM_PROCESSOS 3

typedef struct {
  	unsigned char red, green, blue;
} PPMPixel;

typedef struct {
	char format[2];
  	int x, y;
	unsigned int rgb_comp;
  	PPMPixel *data;
} PPMImage;

typedef struct {
	int new_data;
	int p;
	PPMImage *img;
	char filename[BUFFER_LENGTH];
	int ppm_paint;
	int ppm_save;
	int ppm_creat;
} shared_data_type;



void writePPM(const char *filename, PPMImage *img)
{
	FILE *fp = NULL;
	//open file for output
	fp = fopen(filename, "wb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		exit(1);
	}
	//write the header file
	//image format
	fprintf(fp, "%c%c\n",img->format[0],img->format[1]);
	//image size
	fprintf(fp, "%d %d\n",img->x,img->y);
	// rgb component depth
	fprintf(fp, "%d\n",img->rgb_comp);
	// pixel data
	fwrite(img->data, sizeof(PPMPixel),img->x * img->y, fp);
	fclose(fp);
}

PPMImage * createPPM(int x, int y, int rgb_comp)
{
	int i;
	PPMImage *img 	= NULL;
	if (x < 0 || y < 0) {
		fprintf(stderr, "Invalid image size\n");
		exit(1);
	}
	//check rgb component depth
	if (rgb_comp != MAX_COLOR_VALUE) {
		fprintf(stderr, "Invalid rgb components\n");
		exit(1);
	}
	img = (PPMImage *)malloc(sizeof(PPMImage));
	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}
	//set image format
	img->format[0] = 'P';
	img->format[1] = '6';
	//set image size
	img->x 		= x;
	img->y 		= y;
	//set rgb component depth
	img->rgb_comp 	= rgb_comp;

	//memory allocation for pixel data
	img->data = (PPMPixel *) malloc( sizeof(PPMPixel) * img->x * img->y);
	if (!img->data) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}

	return img;
}


int main() {
	
	pid_t pid;
	
	
	int x, y;
	
	//int r, cont=0;

	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/pintar", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	printf("%d\n", fd);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	fflush(stdin);
	fprintf(stdout,"\nFile name (PPM extension):");
	fscanf(stdin, "%s", shared_data->filename);
	fflush(stdin);
	fprintf(stdout,"\nImage size (x):");
	fscanf(stdin, "%d", &x);
	fflush(stdin);

	fprintf(stdout,"\nImage size (y):");
	fscanf(stdin, "%d", &y);
	fflush(stdin);
	
	shared_data->img = createPPM(x, y,MAX_COLOR_VALUE);
	
	shared_data->ppm_creat=0;


	pid=fork();
	if(pid>0){
		printf("escreve\n");
		int i;
		for(i = 0; i < (shared_data->img)->x *(shared_data->img)->y;i++){
		(shared_data->img)->data[i].red 	= 255;
		(shared_data->img)->data[i].green    	= 255;
		(shared_data->img)->data[i].blue 	= 255;
		}	
		printf("id image: %d",*(shared_data->img));
		shared_data->ppm_creat=1;
		wait(NULL);
		
		
		if((shared_data->img) != NULL){
			printf("\nFinal color red: %d\n",(shared_data->img)->data[0].red);
			writePPM(shared_data->filename, (shared_data->img));
			
			
		}
		
	}else{
		
		while(!shared_data->ppm_creat);
		int i;
		for(i = 0; i < (shared_data->img)->x *(shared_data->img)->y;i++){
		(*(shared_data->img)).data[i].red= 0;
		(*(shared_data->img)).data[i].green = 0;
		(*(shared_data->img)).data[i].blue = 0;
		}	
		printf("id image: %d",*(shared_data->img));
		printf("color red: %d",(shared_data->img)->data[0].red);
		
		
	}












	/*PPMImage *img 	= NULL;
	
	int x, y;
	pid_t pid;
	
	//int r, cont=0;
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/pintar", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	fflush(stdin);
	fprintf(stdout,"\nFile name (PPM extension):");
	fscanf(stdin, "%s", shared_data->filename);
	fflush(stdin);
	fprintf(stdout,"\nImage size (x):");
	fscanf(stdin, "%d", &x);
	fflush(stdin);

	fprintf(stdout,"\nImage size (y):");
	fscanf(stdin, "%d", &y);
	fflush(stdin);
	
	


	
	
	int i;
	for(i=0; i< NUM_PROCESSOS; i++) {
		pid = fork();
		if(pid == 0) {
			
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
		
	}
	
	
	
	if(pid>0) {
		
		printf("Cria uma vez");
		shared_data->img=img;
		shared_data->img = createPPM(x, y,MAX_COLOR_VALUE);
		shared_data->ppm_paint=1;
		for(i=0; i< NUM_PROCESSOS; i++) {
			wait(NULL);
		}
		if(img != NULL){
		if(img->data != NULL)
			free(img->data);
		free(img);
		}
	}else{
		
		if(i==0){
		printf("\nole");
		while(!shared_data->ppm_paint);
		int j;
		for(j = 0; j < (shared_data->img)->x *(shared_data->img)->y;j++){
			
			
		(shared_data->img)->data[j].red 	= rand() % (MAX_COLOR_VALUE + 1);
		(shared_data->img)->data[j].green   = rand() % (MAX_COLOR_VALUE + 1);
		(shared_data->img)->data[j].blue 	= rand() % (MAX_COLOR_VALUE + 1);
		}
		
		printf("RED%d\n" , 		(shared_data->img)->data[1].red);
		shared_data->ppm_save=1;
		
		printf("Entrou aqui primeiro%d\n" , getpid());
		
		}
		
		if(i==1){
		printf("\nole2");
		while(!shared_data->ppm_save);
		
		printf("Entrou aqui segundo RED%d\n" , 	(shared_data->img)->data[1].red);
		if((shared_data->img) != NULL){
			writePPM(shared_data->filename, (shared_data->img));
			printf("E por fim%d\n" , getpid());
		}
		}
		printf("Depois alterou isto%d\n" , getpid());
	
	}*/

				/*
				img = readPPM(filename);
				if(img != NULL){
					printPPM(img);
				}
				*/
				
				/*
				fflush(stdin);
				fprintf(stdout,"\nFile name (PPM extension):");
				fscanf(stdin, "%s", filename);
				fflush(stdin);

				

				img = readPPM(filename);
				if(img != NULL){
					fprintf(stdout,"\nImage x coordinate:");
					fscanf(stdin, "%d", &x);
					fflush(stdin);

					fprintf(stdout,"\nImage y coordinate:");
					fscanf(stdin, "%d", &y);
					fflush(stdin);
					
					pixel = getPixelPPM(x, y, img);
					if(pixel){
						fprintf(stdout, "(%d,%d)%d(%u,%u,%u)\t",x,y, x*img->y+y,pixel->red,pixel->green, pixel->blue);
					}
					
				}
				*/
	
	return 0;
}
