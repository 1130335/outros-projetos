/* PL05A - Memória Partilhada
8. Altere o exercício anterior para permitir a troca de informação de uma turma,
constituída por 20 alunos, um de cada vez. Tenha em atenção a necessária sincronização
de leituras e escritas dos dados partilhados.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>


#define STR_SIZE 50
#define NR_DISC 10


typedef struct{
int numero;
char nome[STR_SIZE];
int disciplinas[NR_DISC];
int new_data;
int cont;
}aluno;

int main(int argc, char *argv[]) {
	
	int r;
	pid_t pid;
	int fd, data_size = sizeof(aluno);
	aluno *shared_data;
	
	fd = shm_open("/aluno7", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (aluno*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	
	
	shared_data->new_data = 0; /*false*/
	pid = fork();
	
	if(pid < 0) {
		exit(-1);
	}
	
		
	
	if(pid>0) {
		do{
		while(shared_data->new_data);
		printf("\nNovo aluno: ");
		int aux1;
		char aux2[80];
		printf("Insira o numero por favor: ");
		scanf("%d", &aux1);
		shared_data->numero = aux1;
		printf("Insira o nome por favor: ");
		scanf("%s", aux2);
		printf("\n");
		strcpy(shared_data->nome, aux2);
		int i;
		for(i=0;i<NR_DISC;i++){
			printf("Insira nota disciplina nº%d: ", i+1);
			scanf("%d", &aux1);
			shared_data->disciplinas[i]=aux1;
			
			
		}
		
		shared_data->cont++;
		
		shared_data->new_data = 1;
	}while(shared_data->cont<20);
		wait(NULL);
		
		printf("Iniciando remoção do sistema . . .\n");
		r = munmap(shared_data, data_size); /* desfaz mapeamento */
		if (r < 0) exit(1); /* verifica erro */
		r = shm_unlink("/aluno7"); /* remove do sistema */
		if (r < 0) exit(1); /* verifica erro */
		exit(0);
		
	}
	else {
		do{
		while(!shared_data->new_data);
		int maior=0, menor=20, soma=0,media=0;
		int i;
		for(i=0;i<NR_DISC;i++){
			if(shared_data->disciplinas[i]>=maior){
				maior=shared_data->disciplinas[i];
			}else if(shared_data->disciplinas[i]<=menor){
				menor=shared_data->disciplinas[i];
			}
			soma=soma+shared_data->disciplinas[i];
		}
		media=soma/NR_DISC;
		printf("Nome: %s\n", shared_data->nome);
		printf("Numero: %d\n", shared_data->numero);
		printf("Nota mais alta: %d\n", maior);
		printf("Nota mais baixa: %d\n", menor);
		printf("Media: %d\n", media);
		shared_data->new_data = 0;
	}while(shared_data->cont<20);
	}
	

	return 0;
}
