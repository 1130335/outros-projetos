/* PL05A - Memória Partilhada
Exercício 14
	Implemente um programa que cria um novo processo. Um será o produtor e outro o consumidor. 
	Entre eles deverá ser criado um buffer circular para armazenar
	10 inteiros e as necessárias variáveis de sincronização numa zona de memória partilhada.
	O produtor coloca valores crescentes no buffer, que devem ser impressos pelo consumidor. 
	Admita que são trocados 30 valores entre eles.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

#define SIZE 10
#define NUMBERS 30


typedef struct {
    int size;
    int start;
    int end;
    int count;
    int element[SIZE];  
    int value; 
    int new_data; 
    int cont;
}buffer_t;
 
void init(buffer_t *buffer) {
	buffer->size = SIZE;
    buffer->start = 0;
    buffer->count = 0;
    buffer->cont=0;
	buffer->value=0; 
	buffer->new_data = 0; /*false*/
     
}
 
int empty(buffer_t *buffer) {
    if (buffer->count == 0) {
        return 1;
    } else {
        return 0;
    }
}
void full(buffer_t *buffer) {
    if (buffer->count == buffer->size) { 
         buffer->start = 0;
		 buffer->count = 0;
    } 
}

     
void push(buffer_t *buffer, int data) {
    int index;
    full(buffer);
    index = buffer->start + buffer->count++;
    if (index >= buffer->size) {
		index = index - buffer->size;
	}
    buffer->element[index] = data;
    
}


int main(int argc, char *argv[]) {
	
	
	int r;
	pid_t pid;
	
	buffer_t *shared_data;
	int fd, data_size = sizeof(buffer_t);

	
    
	
	fd = shm_open("/buffer", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	
	shared_data = (buffer_t*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	init(shared_data);
	
	
	pid = fork();
	
	if(pid < 0) {
		exit(-1);
	}
	
		
	
	if(pid>0) {
		do{
		while(!shared_data->new_data);
		int i;
			for(i=0; i< SIZE; i++){
				printf("Print: %d\n", shared_data->element[i]);
			}
		shared_data->new_data = 0;
		if(shared_data->cont==NUMBERS){
			printf("Iniciando remoção do sistema . . .\n");
			r = munmap(shared_data, data_size); 
			if (r < 0) exit(1);
				r = shm_unlink("/buffer"); 
			if (r < 0) exit(1); 
				exit(0);
		}
	}while(shared_data->cont<NUMBERS);
	}
	else { 
		do{
		while(shared_data->new_data);
			int i;
			for(i=0; i< SIZE; i++){
				shared_data->value++;
				push(shared_data, shared_data->value);
				printf("push: %d\n", shared_data->value);
				shared_data->cont++;
				shared_data->new_data = 1;
			}
		}while(shared_data->cont<NUMBERS);
	}

	return 0;
}
