/* PL05A - Memória Partilhada
10.IAltere o programa anterior de forma a suportar "rondas". Assim que todos os processos
filho terminem cada uma das rondas, o processo pai começa de imediato a
verificar as apostas efectuadas.
*/

/* PL05A - Memória Partilhada
10
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <math.h>



#define NR_ROND 3
#define NR_PROC 5

typedef struct{
	
int apostas[NR_PROC];
int new_data;
int nr_apostas;
int apostou[NR_PROC];
}aposta;

int main(int argc, char *argv[]) {

	//int r;
	pid_t pid;
	time_t t;
	int fd, data_size = sizeof(aposta);
	aposta *shared_data;
	
	fd = shm_open("/Aposta",O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (aposta*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	int i;
	
	for(i=0; i<NR_PROC; i++) {
		pid = fork();
		if(pid == 0) {
			break;
		}
		
		if(pid < 0) {
			exit(-1);
		}
	}
	
	shared_data->new_data=0;	
	if(pid == 0) {
		srand((unsigned) time(&t)-10*i+1);
		
		printf("Começou filho %d\n", i);
		int cont=0;
		do{
			
		while(!shared_data->new_data);
		if(shared_data->apostou[i]==1){
		
		shared_data->apostas[i]=rand()% 20+ 1;
		
		shared_data->apostou[i]=0;
		cont++;
		shared_data->new_data=0;
		}
		}while(cont<NR_ROND);
		
		
	}
		
	if(pid>0) {
		
		int nr_rondas=0;
		
		int i;
		for(i=0; i<NR_PROC;i++){
			shared_data->apostou[i]=1;
		}
		
		printf("Começou pai"); 
		printf("\nRonda %d:\n",nr_rondas);
		do{
			while(shared_data->new_data);

			int n=0;
			if(nr_rondas!=NR_ROND){
			for(i=0; i<NR_PROC;i++){
				if(shared_data->apostou[i]==0){
					printf("Processo %d apostou.\n",i);
					n++;
				}else{
					n=0;
					printf("Processo %d não apostou.\n",i);
				}
			}
			}
			if(n==NR_PROC){
				n=0;
				srand((unsigned) time(&t)-10*nr_rondas+1);
				
				int valor=rand()% 20 + 1;
				
				printf("\nNumero aleatorio: %d:\n",valor);
				for(i=0; i<NR_PROC;i++){
					printf("Aposta do %d º processo: %d\n",i,shared_data->apostas[i]);
					if(shared_data->apostas[i]==valor){
						printf("Processo %d ganhou!\n",i);
					}
				shared_data->apostou[i]=1;
				}
				
				
				nr_rondas++;
				if(nr_rondas!=NR_ROND){
				printf("\nRonda %d:\n",nr_rondas);
				shared_data->new_data=1;
				}
				
			}else{
				if(nr_rondas!=NR_ROND){
				printf(" \n");
				shared_data->new_data=1;
				}
			}
			
		}while(nr_rondas!=NR_ROND);
		
		
		int j;
		for(j=0; j<NR_PROC; j++) {
			wait(NULL);
		}
		
		
		
	}
	

	return 0;

}
