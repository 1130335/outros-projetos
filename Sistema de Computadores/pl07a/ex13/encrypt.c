/* PL07A - Memória Partilhada
Exercício 13
Encrypt
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>

typedef struct {
	char msg[100];
	int valor;
} shared_data_type;

int main(void) {
	
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/mensagem", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	srand((unsigned)time(NULL));
	shared_data->valor = (rand() % 5) + 1;
	printf("Valor: %d\n", shared_data->valor);
	
	int i;
	for(i = 0; i< strlen(shared_data->msg); i++) {
		shared_data->msg[i] = shared_data->msg[i] + shared_data->valor;
	}
	
	printf("Mensagem encriptada: %s\n", shared_data->msg);
	execl("display", "display", (char*)NULL);
			
	return 0;
}
