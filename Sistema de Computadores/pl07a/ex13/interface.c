/* PL07A - Memória Partilhada
Exercício 13
Interface
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

typedef struct {
	char msg[100];
	int valor;
} shared_data_type;

int main(void) {
	
	int fd, data_size = sizeof(shared_data_type);
	shared_data_type *shared_data;
	fd = shm_open("/mensagem", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	shared_data = (shared_data_type*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	printf("Introduza uma mensagem: ");
	scanf("%[^\n]s", shared_data->msg);
	
	execl("encrypt", "encrypt", (char*)NULL);
			
	return 0;
}
