/* PL09A - Semáforos 
   Exercício 9
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

#define N 10			/* posições disponíveis no buffer */
#define NUM_FILHOS 2	/* número de processos filhos criados */

/* memória partilhada */
typedef struct {
	int buffer[N];		/* buffer circular */
}shm;

int main(void) {
	sem_t *cheio;		/* indica se o buffer está cheio */
	sem_t *vazio;		/* indica se o buffer está vazio */
	sem_t *exclusao;	/* garante exclusão mútua no acesso à memória partilhada */
	int in = 0;
	int out = 0;
	int numero;			/* número a ser inserido no buffer */
	int lido;
	
	/* criação dos semáforos */
	
	if((cheio = sem_open("ex09_cheio", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	if((vazio = sem_open("ex09_vazio", O_CREAT|O_EXCL, 0644, N)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	if((exclusao = sem_open("ex09_exclusao", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	/* criação da zona de memória partilhada */
	int fd, data_size = sizeof(shm);
	shm *mybuffer;
		
	fd = shm_open("/ex09", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, data_size);
	mybuffer = (shm*)mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	
	numero = 0;
	
	/* criação dos processos */
	pid_t pid[NUM_FILHOS];
	int i;
	for(i = 0; i < NUM_FILHOS; i++) {
		pid[i] = fork();
		if(pid[i] == -1) {
			perror("Erro na clonagem do processo");
			exit(EXIT_FAILURE);
		}
		
		if(pid[i] == 0) {
			break;
		}
	}
	
	if(pid[i] == 0) {
		printf("===== FILHO PRODUTOR %d INICIOU =====\n", i);
		int j;
		for(j = 0; j < 30; j++) {
			sem_wait(vazio);
			sem_wait(exclusao);
			mybuffer->buffer[in] = numero;
			in= (in + 1) % N;
			numero++;
			sem_post(exclusao);
			sem_post(cheio);
		}
		printf("===== FILHO PRODUTOR %d ACABOU =====\n", i);
		exit(EXIT_SUCCESS);
	}
	
	if(pid[0] > 0) {
		printf("===== PAI CONSUMIDOR =====\n");
		int j;
		for(j = 0; j < 60; j++) {
			sem_wait(cheio);
			sem_wait(exclusao);
			lido = mybuffer->buffer[out];
			printf("Valor lido: %d\n", lido);
			out = (out + 1) % N;
			sem_post(exclusao);
			sem_post(vazio);
		}
	}
	
	sem_unlink("ex09_cheio");
	sem_unlink("ex09_vazio");
	sem_unlink("ex09_exclusao");
	munmap(mybuffer, sizeof(shm));
	close(fd);
	shm_unlink("/ex09");
		
	return 0;
}
