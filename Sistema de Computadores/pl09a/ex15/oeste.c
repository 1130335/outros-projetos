/* PL09A - Semáforos
   OESTE
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

int main(void) {
	sem_t *este;
	sem_t *oeste;
	
	if((oeste = sem_open("ex15_oeste", O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	if((este = sem_open("ex15_este", O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("Impossível criar o semáforo");
		exit(EXIT_FAILURE);
	}
	
	int v1 = 0;
	sem_getvalue(este, &v1);
	while(v1 == 1) {
		sem_getvalue(este, &v1);
	}
	
	sem_post(oeste);
	sleep(5);
	printf("Passei!\n");
	sem_wait(oeste);
	
	return 0;
}
