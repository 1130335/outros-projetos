/* PL10B - Threads
   Exercício 7
   CORRIGIR E ACABAR - ESTOU NAS PRIMEIRAS THREADS
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	char* vec[15]
	char* vec1[5];
	char* vec2[5];
	char* vec3[5];
}hipers;

/* Variável Global */
int hiper_barato;

char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

void* thread_func1(void *arg) {
	hipers* val = (hipers*) arg;
	int i;
	for(i = 0; i < 15; i++) {
		char* hiper = str_split(val->vec[i], ',');
		printf("%s\n", hiper);
	}
	
	pthread_exit((void*)NULL);
}

void* thread_func2(void *arg) {
	hipers* resultados = (hipers*) arg;
	
	
	pthread_exit((void*)NULL);
}

int main(void) {
	
	/* Inicialização dos dados da estrutura e Criação do vetor */
	hipers valores;
	valores.vec[0] = "{h1,p1,1}";
	valores.vec[1] = "{h1,p2,1}";
	valores.vec[2] = "{h1,p3,1}";
	valores.vec[3] = "{h1,p4,1}";
	valores.vec[4] = "{h1,p5,1}";
	valores.vec[5] = "{h2,p1,2}";
	valores.vec[6] = "{h2,p2,2}";
	valores.vec[7] = "{h2,p3,2}";
	valores.vec[8] = "{h2,p4,2}";
	valores.vec[9] = "{h2,p5,2}";
	valores.vec[10] = "{h3,p1,0}";
	valores.vec[11] = "{h3,p2,0}";
	valores.vec[12] = "{h3,p3,0}";
	valores.vec[13] = "{h3,p4,0}";
	valores.vec[14] = "{h3,p5,0}";
	
	/* Criação das Threads */
	pthread_t primeiras[3];
	pthread_t segundas[3];
	int i;
	for(i = 0; i<3; i++) {
		pthread_create(&primeiras[i], NULL, thread_func1, (void*) &valores);
		pthread_join(primeiras[i], NULL);
	}
	
	for(i = 0; i<3; i++) {
		pthread_create(&segundas[i], NULL, thread_func2, (void*) &valores);
		pthread_join(segundas[i], NULL);
	}
	
	printf("O hipermercado com os preço total dos produtos mais baratos é %s\n", hiper_barato);
	return 0;
}
