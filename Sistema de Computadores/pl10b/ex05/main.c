/* PL10B - Threads
   Exercício 5
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	int resultado[1000];
	int atual;
	int proximo;
	int dados[1000];
}result;

void* thread_func(void *arg) {
	result* resultados = (result*) arg;
	int i;
	for(i = resultados->atual; i < resultados->proximo; i++) {
		resultados->resultado[i] = resultados->dados[i]*2+10;
		printf("Resultado %d = %d\n", i+1, resultados->resultado[i]);
	}
	pthread_exit((void*)NULL);
}

int main(void) {
	/* Inicialização dos dados da estrutura e Criação do vetor */
	result res[5];
	int j;
	res[0].atual = 0;
	res[0].proximo = 200;
	for(j = 0; j < 5; j++) {
		if(j != 0) {
			res[j].atual = res[j-1].atual + 200;
			res[j].proximo = res[j-1].proximo + 200;
		}
		int i;
		for(i = 0; i < 1000; i++) {
			res[j].dados[i] = i + 1;
		}
	}
	
	/* Criação das Threads */
	pthread_t threads[5];
	int i;
	for(i = 0; i<5; i++) {
		pthread_create(&threads[i], NULL, thread_func, (void*) &res[i]);
		pthread_join(threads[i], NULL);
	}
		
	return 0;
}
