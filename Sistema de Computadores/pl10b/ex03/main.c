/* PL10B - Threads
   Exercício 3
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct {
	int vetor[1000];
	int atual;
	int proximo;
	int total;
}banco;

void* thread_func(void *arg) {
	banco* contas = (banco*) arg;
	int i;
	for(i = contas->atual; i < contas->proximo; i++) {
		contas->total = contas->total + contas->vetor[i];
	}
	pthread_exit((void*)NULL);
}

int main(void) {
	/* Inicialização dos dados da estrutura e Criação do vetor */
	banco contas[5];
	int esperado = 0;
	int j;
	contas[0].atual = 0;
	contas[0].proximo = 200;
	for(j = 0; j < 5; j++) {
		contas[j].total = 0;
		if(j != 0) {
			contas[j].atual = contas[j-1].atual + 200;
			contas[j].proximo = contas[j-1].proximo + 200;
		}
		int i;
		for(i = 0; i < 1000; i++) {
			contas[j].vetor[i] = i + 1;
			if(j == 4) {
				esperado = esperado + contas[j].vetor[i];
			}
		}
	}
	
	printf("O valor calculado deverá ser igual a: %d\n", esperado);
	printf("A calcular... \n");
	int total = 0;
	
	/* Criação das Threads */
	pthread_t threads[5];
	int i;
	for(i = 0; i<5; i++) {
		pthread_create(&threads[i], NULL, thread_func, (void*) &contas[i]);
		pthread_join(threads[i], NULL);
	}
	
	for(i = 0; i<5; i++) {
		total = total + contas[i].total;
	}
	printf("Cálculo terminado...\n");
	printf("Valor calculado = %d\n", total);
	
	return 0;
}
