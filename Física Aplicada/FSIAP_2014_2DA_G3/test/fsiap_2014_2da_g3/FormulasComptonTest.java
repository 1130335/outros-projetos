package fsiap_2014_2da_g3;

import fsiap.model.FormulasCompton;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class FormulasComptonTest {

    public FormulasComptonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCalculosTotais() {
        System.out.println("calculosTotais");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResultCO = 1.0000000000020552;
        double resultCO = instance.getCompOndaFin();
        assertEquals(expResultCO, resultCO, 0.0);
        double expResultEF = 1.9878207899959142E-25;
        double resultEF = instance.getEnergiaFinal();
        assertEquals(expResultEF, resultEF, 0.0);
        double expResultEI = 1.9878207899999998E-25;
        double resultEI = instance.getEnergiaInicial();
        assertEquals(expResultEI, resultEI, 0.0);
        double expResultEEle = 4.085531635328634E-37;
        double resultEEle = instance.getEnergiaEletrao();
        assertEquals(expResultEEle, resultEEle, 0.0);
        
    }

    @Test
    public void testGetEnergiaEletrao() {
        System.out.println("getEnergiaEletrao");
        FormulasCompton instance = new FormulasCompton();
        double com = 1.0;
        double ang = 30.0;
        double expResult = 4.085531635328634E-37;
        instance.calculosTotais(com, ang);
        double result = instance.getEnergiaEletrao();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetAngulo() {
        System.out.println("getAngulo");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResult = 30.0;
        double result = instance.getAngulo();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetEnergiaFinal() {
        System.out.println("getEnergiaFinal");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResult = 1.9878207899959142E-25;
        double result = instance.getEnergiaFinal();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetEnergiaInicial() {
        System.out.println("getEnergiaInicial");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResult = 1.9878207899999998E-25;
        double result = instance.getEnergiaInicial();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetCompOndaIni() {
        System.out.println("getCompOndaIni");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResult = 1.0;
        double result = instance.getCompOndaIni();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetCompOndaFin() {
        System.out.println("getCompOndaFin");
        double com = 1.0;
        double ang = 30.0;
        FormulasCompton instance = new FormulasCompton();
        instance.calculosTotais(com, ang);
        double expResult = 1.0000000000020552;
        double result = instance.getCompOndaFin();
        assertEquals(expResult, result, 0.0);
    }

}
