package fsiap.model;

import java.io.Serializable;

/**
 * @author FSIAP_2014_2DA_G3
 */

/**
 * Introdução dos componentes dos dados para a aplicação
 */
public class FormulasCompton implements Serializable{

    private final double H; //Constante de Planck
    private final double C; //Velocidade da Luz, no Vazio
    private final double COMPTON; //Comprimento de onda de Compton
    private double compOndaIni; //Comprimento de Onda Inicial
    private double compOndaFin; //Comprimento de Onda Final
    private double angulo; //Ângulo de Dispersão do Fotão
    private double energiaIni; //Energia Inicial do Fotão
    private double energiaFin; //Energia Final do Fotão
    private double energiaEle; //Energia do Eletrão
    private boolean estado; //Estado de cálculo

    /**
     *Preparação dos dados para o cálculo, nomeadamnete as constantes da fórmula de Compton
     */
    public FormulasCompton() {
        H = 6.6260693 * (Math.pow(10, -34));
        C = 3.00 * (Math.pow(10, 8));
        COMPTON = 2.43*(Math.pow(10,-12));
        compOndaIni = 0;
        compOndaFin = 0;
        angulo = 0;
        energiaFin = 0;
        energiaEle = 0;
        estado = false;
    }
    
    /**
     *Finalização dos cálculos do Efeito de Compton
     * @param com - comnprimento de onda inicial
     * @param ang - ângulo de dispersão
     */
    public void calculosTotais(double com, double ang) {
        compOndaIni = com;
        angulo = ang;
        
        compOndaFin = COMPTON * (1 - Math.cos(angulo)) + compOndaIni;
        
        if (compOndaFin != 0) {
            energiaFin = ((H * C) / (compOndaFin));
        }
        
        if (compOndaIni != 0) {
            energiaIni = ((H * C) / (compOndaIni));
        }
        
        energiaEle = energiaIni - energiaFin;
        
        estado = true;
    }

    /**
     *Devolve a energia do eletrão, após a colisão
     * @return energiaEle
     */
    public double getEnergiaEletrao() {
        return energiaEle;
    }

    /**
     *Devolve o ângulo de dispersão
     * @return angulo
     */
    public double getAngulo() {
        return angulo;
    }

    /**
     *Devolve a Energia Final do Fotão, após a colisão
     * @return energiaFin
     */
    public double getEnergiaFinal() {
        return energiaFin;
    }

    /**
     *Devolve a Energia Inicial, antes da colisão
     * @return energiaIni
     */
    public double getEnergiaInicial() {
        return energiaIni;
    }

    /**
     * Devolve o comprimento de onda Inicial do fotão
     * @return oompOndaIni
     */
    public double getCompOndaIni() {
        return compOndaIni;
    }

    /**
     *Devolve o comprimento de onda final do fotão
     * @return compOndaFin
     */
    public double getCompOndaFin() {
        return compOndaFin;
    }
}
