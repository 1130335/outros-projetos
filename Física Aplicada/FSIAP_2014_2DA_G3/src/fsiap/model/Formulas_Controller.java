package fsiap.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Formatter;

    /**
     * Classe de processamento dos Dados de informação, entre a UI
     * e FormulasCompton.
     */

public class Formulas_Controller {

    private ArrayList<FormulasCompton> colisoes;
    private CodificadorHTML m_cHTML;
    private Formatter m_out;
    
    /**
     * Contrutor deFormulas_Controller que inicializa os objetos necessários
     */
    public Formulas_Controller() throws FileNotFoundException {
        colisoes = new ArrayList<FormulasCompton>(); //guarda todas as colisões da aplicação
        m_cHTML = new CodificadorHTML(); //objeto codificador de HTML
        m_out = new Formatter(new File("resultados.html")); //objeto de ficheiro de HTML
        m_cHTML.MontarFicheiro(m_out);  //inicializa o ficheiro de HTML
    }
        
    /**
     * Cria uma nova colisão
     * @return m_fc retorna a nova colisão
     */
    public FormulasCompton novaColisao() {
        FormulasCompton m_fc = new FormulasCompton();
        return m_fc;
    }
    
    /**
     * Efetua os cálculos para a colisão, passada por parâmetro
     * @param m_fc colisão para a qual são feitos os cálculos
     * @param com comprimento de onda inicial do fotão
     * @param ang ângulo de dispersão
     */
    public void fazerCalculos(FormulasCompton m_fc, double com, double ang) {
            m_fc.calculosTotais(com, ang);
            colisoes.add(m_fc);
    }
    
    /**
     * Escreve os resultados, em Inglês
     * @param m_fc colisão para a qual foram feitos os cálculos
     * @return s
     */
    public String escreverENG(FormulasCompton m_fc) {
        String s;
        s = "\nRESULTS AFTER THE COLLISION:\n"
                + "Photon's Dispersion Angle: " + m_fc.getAngulo() + "º\n"
                + "Photon's Initial Wave-Length: " + m_fc.getCompOndaIni() + " m\n"
                + "Photon's Final Wave-Length: " + m_fc.getCompOndaFin() + " m\n"
                + "Photon's Initial Energy: " + m_fc.getEnergiaInicial() + " J\n"
                + "Photon's Final Energy: " + m_fc.getEnergiaFinal() + " J\n"
                + "Electron's Energy: " + m_fc.getEnergiaEletrao() + " J\n";
        System.out.println(s);
        return s;
    }
    
    /**
     * Escreve os resultados, em Português
     * @param m_fc colisão para a qual foram feitos os cálculos
     * @return s
     */
    public String escrever(FormulasCompton m_fc) {
        String s;
        s = "\nRESULTADOS APÓS A COLISÃO:\n"
                + "Ângulo de Dispersão do Fotão: " + m_fc.getAngulo() + "º\n"
                + "Comprimento de Onda Inicial do Fotão: " + m_fc.getCompOndaIni() + " m\n"
                + "Comprimento de Onda Final do Fotão: " + m_fc.getCompOndaFin() + " m\n"
                + "Energia Inicial do Fotão: " + m_fc.getEnergiaInicial() + " J\n"
                + "Energia Final do Fotão: " + m_fc.getEnergiaFinal() + " J\n"
                + "Energia do Eletrão: " + m_fc.getEnergiaEletrao() + " J\n";
        System.out.println(s);
        return s;
    }
   
    /**
     * Insere a nova colisão numa tabela de HTML
     * @param m_fc colisão para a qual foram feitos os cálculos
     */
    public void guardarHTML(FormulasCompton m_fc) {
        m_cHTML.InserirDadosColisao(m_fc.getAngulo(), m_fc.getCompOndaIni(), m_fc.getCompOndaFin(), m_fc.getEnergiaInicial(), m_fc.getEnergiaFinal(), m_fc.getEnergiaEletrao(), m_out);
    }
    
    /**
     * Finaliza os ficheiros binários e de HTML, quando a aplicação é encerrada
     */
    public void finalizarGravacao() throws IOException {
        FileOutputStream fileOut = new FileOutputStream("binario.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        m_cHTML.FinalizarFicheiro(m_out);
        out.writeObject(colisoes); 
        out.close();
        fileOut.close();
    }

}
