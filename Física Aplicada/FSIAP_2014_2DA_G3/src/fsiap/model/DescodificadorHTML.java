package fsiap.model;

import java.io.*;
import java.util.*;

/**
 * Importa os dados para HTML
 *
 * @author Rita i130511
 */
public class DescodificadorHTML {

    public static void lerFicheiro(String[] parametros, String ficheiro) throws FileNotFoundException {

        Scanner input = new Scanner(new File(ficheiro));
        String aux;
        int i = 0;

        // Guarda as linhas do ficheiro HTML num vetor(parametros), eliminando as tags
        while (input.hasNext()) {
            aux = input.nextLine().trim();
            if (aux.equals("") == false) {
                aux = aux.replaceAll("\\<.*?\\>", "");
            } else {
                if (aux.equals("") == true) {
                    continue;
                }
            }

            if (aux.equals("") == false) {
                if (aux.equalsIgnoreCase(ficheiro) == false) {
                    parametros[i] = aux.trim();
                    i++;
                }
            }
        }

        input.close();
    }

    public static int contaLinhas(String ficheiro) throws FileNotFoundException {

        Scanner input = new Scanner(new File(ficheiro));
        String linha;
        int cont = 0;

        // Conta as linhas do ficheiro HTML, eliminando as tags
        while (input.hasNext()) {
            linha = input.nextLine().trim();
            if (linha.equals("") == false) {
                linha = linha.replaceAll("\\<.*?\\>", "");
            } //else não é necessário
            else {
                if (linha.equals("") == true) {
                    continue;
                }
            }

            //ver se este segmento faz sentido
            if (linha.equals("") == false) {
                if (linha.equalsIgnoreCase(ficheiro) == false) {
                    cont++;
                }
            }
        }
        return cont;
    }

     public static void criaMatriz(String[] parametros, String[][] matriz, int num) {
        int cont = 0;
        int i, a;
        String str1 = parametros[0];
        String str2 = parametros[1];
        for (a = 0; a < num; a++) {
            for (i = 0; i < matriz[0].length; i++) {
                matriz[a][i] = parametros[a + cont + 2];
                cont++;
            }
            cont--;
        }
    }

     //método para testes
    private static void listarMatriz(String[][] matriz, int cont) {
        System.out.printf("MATRIZ: \n");
        for (int i = 0; i < cont; i++) {
            System.out.printf("%5s%15s%15s%15s%15s%15s%15s%n", matriz[i][0], matriz[i][1], matriz[i][2], matriz[i][3], matriz[i][4], matriz[i][5], matriz[i][6]);
        }
    }

}
