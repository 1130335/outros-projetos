package fsiap.model;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Formatter;

/**
 *Exporta os dados para HTML
 * @author FSIAP_2014_2DA_G3
 */
public class CodificadorHTML {

    private int nColisao;

    /**
     *Inicializa o número de colisões, da aplicação
     */
    public CodificadorHTML() {
        nColisao = 1;
    }

    /**
     *Devolve o cabeçalho da página HTML
     * @return cabecalho
     */
    public String Cabecalho() {
        String cabecalho;
        cabecalho = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Resultados das Colisões</title>\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                + "</head>"
                + "<body>"
                + "<h1>FSIAP_2014_2DA_G3</h1>";
        return cabecalho;
    }

    /**
     *Prepara a tabela para as colisões e devolve o início do corpo da página HTML
     * @return corpo
     */
    public String Corpo() {
        String corpo;
        corpo = "<h2>Resultados após Colisões</h2><p>\n"
                + "<table border = \"1\">\n"
                + "<tr>\n"
                + "<td><b> NºColisão </b></td>\n"
                + "<td><b> Ângulo (Graus)</b></td>\n"
                + "<td><b> Comprimento de Onda Inicial (m) </b></td>\n"
                + "<td><b> Comprimento de Onda Final (m)</b></td>\n"
                + "<td><b> Energia Inicial do Fotão (J)</b></td>\n"
                + "<td><b> Energia Final do Fotão (J)</b></td>\n"
                + "<td><b> Energia do Eletrão (J)</b></td>\n"
                + "</tr>\n";

        return corpo;
    }

    /**
     *O método irá inserir os dados para a tabela
     * @param angulo - ângulo de dispersão do fotão;
     * @param coIni - comprimento de onda inicial;
     * @param coFin - comprimento de onda final;
     * @param eneIni - energia inicial do fotão;
     * @param eneFin - energia final do fotão;
     * @param eneEle - energia do eletrão;
     * @param out - página html
     */
    public void InserirDadosColisao(double angulo, double coIni, double coFin, double eneIni, double eneFin, double eneEle, Formatter out) {
        String novosDados;
        novosDados = "<tr>"
                + "<td>" + nColisao + "</td>\n"
                + "<td>" + angulo + "</td>\n"
                + "<td>" + coIni + "</td>\n"
                + "<td>" + coFin + "</td>\n"
                + "<td>" + eneIni + "</td>\n"
                + "<td>" + eneFin + "</td>\n"
                + "<td>" + eneEle + "</td>\n"
                + "</tr>\n";
        nColisao++;
        out.format(novosDados);

    }

    /**
     *Devolve o fim da página HTML
     * @return fim
     */
    public String Fim() {
        String fim;
        fim = "</table>\n"
                + "</body>\n"
                + "</html>\n";
        return fim;
    }

    /**
     *Inicializa a página em HTML
     * @param out - pagina html;
     * @throws FileNotFoundException
     */
    public void MontarFicheiro(Formatter out) throws FileNotFoundException {

        out.format(this.Cabecalho());
        out.format(this.Corpo());
    }

    /**
     *Finaliza a página HTML
     * @param out - pagina html
     */
    public void FinalizarFicheiro(Formatter out) {
        out.format(this.Fim());
        out.close();
    }
}
