package fsiap.model;

import fsiap.GUI.Window;
import fsiap.ui.FSIAP_UI;
import fsiap.ui.Janela;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Interface Gráfica criada inicialmente
        // (Com erros de compilação - GUI apresentada no protótipo final)
        Janela j = new Janela();

        // Consola - Apenas em Português
        // FSIAP_UI ui = new FSIAP_UI();
        // ui.lerBinario();
        // ui.run(); 
        
        // Interface Gráfica completamente funcional criada após a última milestone 
        // (alternativa - não presente no protótipo final)
        // new Window().setVisible(true);
    }
}
