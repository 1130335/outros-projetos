package fsiap.ui;

import fsiap.model.FormulasCompton;
import fsiap.model.Formulas_Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Apresenta os resultados finais, em Inglês
 * @author aria
 */
public class resultadosENG  extends JFrame {
    String s ;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel().getPreferredSize();
    Formulas_Controller fc;
    

public resultadosENG(Frame pai) throws FileNotFoundException, IOException {
    super( "result "); 
    FormulasCompton m_fc = fc.novaColisao();
    fc = new Formulas_Controller();  
     s= fc.escreverENG(m_fc);
        JPanel p1 = resultados();
        JPanel p2 = ok();
        
         add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
         
         pack();
        setResizable(false);
        setLocation(DIALOGO_DESVIO_X,DIALOGO_DESVIO_Y );
        setVisible(true);

    }
resultadosENG() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


private JPanel resultados () {
        JLabel lbl = new JLabel(this.s, JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
         final int CAMPO_LARGURA = 20;
         JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
         p.add(lbl);
         return p;

}
private JPanel ok() {
     JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //Janela.this.setVisible(false);
                //DadosENG eng = new DadosENG(Janela.this);
            }
        });
        

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btn);
       

        return p;
    }

}
