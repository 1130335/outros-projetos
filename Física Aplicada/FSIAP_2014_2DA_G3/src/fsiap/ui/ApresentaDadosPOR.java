package fsiap.ui;

import fsiap.model.Formulas_Controller;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.*;

/**
 *
 * @author Rita i130511
 */
public class ApresentaDadosPOR extends JDialog {

    private final Frame MAINFRAME;

    private Formulas_Controller formController;

    /**
     *
     * @param pai
     * @param emp
     */
    public ApresentaDadosPOR(Frame pai) throws FileNotFoundException, IOException {

        super(pai, "....", true);
        this.MAINFRAME = pai;
        formController = new Formulas_Controller();

        JPanel p = new JPanel();

        run();

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    public void run() {
        
            JTextArea infoEvento = new JTextArea(formController.toString());
            infoEvento.setEditable(false);
            int i = JOptionPane.showConfirmDialog(MAINFRAME, infoEvento, "Confirmar", 0, JOptionPane.QUESTION_MESSAGE);
            if (i == 1) {
                dispose();
            } else {
                JOptionPane.showMessageDialog(MAINFRAME, "Evento registado com sucesso!", "Evento criado", JOptionPane.INFORMATION_MESSAGE);
            }
            dispose();
        }
}
