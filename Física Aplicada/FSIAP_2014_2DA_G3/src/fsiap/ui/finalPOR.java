/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap.ui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class finalPOR extends JDialog  {
    private JTextField txtTitular, txtDeposito;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel().getPreferredSize();
    public finalPOR(Dialog pai)throws FileNotFoundException, IOException {

        super(pai, "FINAL", true);
        
        JPanel p2 = text();
        JPanel p3 = Botoes();
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        
    
}
    finalPOR() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    private JPanel text(){
        JLabel lbl = new JLabel("You wanna try it again?",JLabel.RIGHT);
        final int CAMPO_LARGURA = 20;
        txtTitular = new JTextField(CAMPO_LARGURA);
        txtTitular.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        return p;
        
        
    }
    private JPanel Botoes(){
        JButton btnYES = criarBotaoYES();
        JButton btnNO = criarBotaoNO();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnYES);
        p.add(btnNO);

        return p;
    }
    private JButton criarBotaoYES(){
        JButton btn = new JButton("SIM");
        btn.addActionListener((ActionEvent e) -> {
            finalPOR.this.setVisible(false);
            
            Janela port;
            port = new Janela();
        });
        return btn;
    }
        
    private JButton criarBotaoNO(){
        JButton btn = new JButton("NÃO");
        btn.addActionListener((ActionEvent e) -> {
            dispose();
        });
        return btn;
    }
 }       






