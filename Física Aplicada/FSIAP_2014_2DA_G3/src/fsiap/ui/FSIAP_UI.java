package fsiap.ui;

import fsiap.model.FormulasCompton;
import fsiap.model.Formulas_Controller;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Interface de consola
 */

public class FSIAP_UI {

    private Formulas_Controller m_fc;

    /**
     * Inicializa o objeto Formulas_Controller
     */
    public FSIAP_UI() throws FileNotFoundException, IOException {
        m_fc = new Formulas_Controller();
    }
    
    /**
     * Pede os dados ao Utilizador e comunica com a classe Formulas_Controller.
     * É chamado recursivamente, até que não seja necessário criar uma nova colisão.
     */
    public void run() throws FileNotFoundException, IOException {
       
        double com = 0;
        double ang = 0;
        Scanner in = new Scanner(System.in);
        while (com <= 0) {
            System.out.println("Introduza comprimento do onda inicial:");
            com = in.nextDouble();
            if (com <= 0) {
                System.out.println("Comprimento do onda inicial nao pode ser zero ou negativo");
            }
        }

        System.out.println("Introduza ângulo de dispersão do fotão:");
        ang = in.nextDouble();

        FormulasCompton fc = m_fc.novaColisao();
        m_fc.fazerCalculos(fc,com, ang);
        m_fc.escrever(fc);
        int resposta;
        System.out.println("\nDeseja realizar uma nova colisão? \n0) Sim\n1) Nao");
        resposta = in.nextInt();
        while(resposta>1||resposta<0) {
           System.out.println("\nResposta inválida!");
           System.out.println("\nDeseja realizar uma nova colisão? \n0) Sim\n1) Nao");
            resposta = in.nextInt();
        }
        if (resposta == 0) {
            m_fc.guardarHTML(fc);
            run();
        } else if (resposta == 1) {
            m_fc.guardarHTML(fc);
            m_fc.finalizarGravacao();
        }
    }
}
