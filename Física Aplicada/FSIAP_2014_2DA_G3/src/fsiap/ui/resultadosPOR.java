/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap.ui;

import fsiap.model.FormulasCompton;
import fsiap.model.Formulas_Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Apresenta os resultados finais, em Português
 * @author aria
 */
public class resultadosPOR  extends JFrame {
    String s ;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel().getPreferredSize();
    Formulas_Controller fc;
    

public resultadosPOR(Formulas_Controller f) throws FileNotFoundException {
    super("Resultados ");  
    FormulasCompton m_fc = fc.novaColisao();
    fc = f; 
     s= fc.escrever(m_fc);
        JPanel p1 = resultados();
        JPanel p2 = ok();
        
         add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
         
        pack();
        setResizable(false);
        setLocation(DIALOGO_DESVIO_X,DIALOGO_DESVIO_Y );
        setVisible(true);

    }



private JPanel resultados () {
        JLabel lbl = new JLabel(this.s, JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
         final int CAMPO_LARGURA = 20;
         JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
         p.add(lbl);
         return p;

}
private JPanel ok() {
     JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                //Janela.this.setVisible(false);
                //DadosENG eng = new DadosENG(Janela.this);
            }
        });
        

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btn);
       

        return p;
    }

}
