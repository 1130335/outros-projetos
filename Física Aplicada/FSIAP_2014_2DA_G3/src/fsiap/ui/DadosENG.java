package fsiap.ui;

import fsiap.model.FormulasCompton;
import fsiap.model.Formulas_Controller;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Apresentação do pedido de dados, em Inglês
 * @author aria
 */
public class DadosENG extends JFrame {

    private JTextField txtComp, txtAng;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Scattering angle:    ").getPreferredSize();
    private String cmp,ang;
    Formulas_Controller fc;
    public DadosENG(Frame p) {
        super("Insert the values");
        JPanel p1 = criarPainelcomp();
        JPanel p2 = criarPainelangulo();
        JPanel p3 = criarPainelBotoes();

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setLocation(DIALOGO_DESVIO_X,DIALOGO_DESVIO_Y );
        setVisible(true);
    }

    DadosENG() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private JPanel criarPainelcomp() {
        JLabel lbl = new JLabel("Wavelength:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        final int CAMPO_LARGURA = 20;
        txtComp = new JTextField(CAMPO_LARGURA);
        txtComp.requestFocus();
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtComp);

        return p;
    }

    private JPanel criarPainelangulo() {
        JLabel lbl = new JLabel("Scattering angle:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 10;
        txtAng = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtAng);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
              int s = Integer.parseInt(cmp);
              int p = Integer.parseInt(ang);
              FormulasCompton m_fc = fc.novaColisao();
              fc.fazerCalculos(m_fc,s,p);
              DadosENG.this.setVisible(false);
                try {
                    resultadosENG eng = new resultadosENG(null);
                    
                    //Janela.this.setVisible(false);
                    //DadosENG eng = new DadosENG(Janela.this);
                } catch (IOException ex) {
                    Logger.getLogger(DadosPOR.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            
            
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
