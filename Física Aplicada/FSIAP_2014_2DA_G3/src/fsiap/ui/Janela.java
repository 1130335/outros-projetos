package fsiap.ui;

import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Janela principal da aplicação
 * @author aria
 */
public class Janela extends JFrame {

    private static final int JANELA_POSICAO_X = 200, JANELA_POSICAO_Y = 200;

    public Janela() {
        super("FSIAP");
        JPanel p1 = escolherLinguagem();
        JPanel p2 = Botoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setLocation(JANELA_POSICAO_X, JANELA_POSICAO_Y);
        setVisible(true);
        
    }
    public Janela(Dialog p) {
        super("FSIAP");
        JPanel p1 = escolherLinguagem();
        JPanel p2 = Botoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setLocation(JANELA_POSICAO_X, JANELA_POSICAO_Y);
        setVisible(true);
        
    }

    private JPanel escolherLinguagem() {
        JLabel lbl = new JLabel("Choice the language:", JLabel.RIGHT);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);

        return p;

    }

    private JPanel Botoes() {
        JButton ENG = criarBotaoENG();
        JButton POR = criarBotaoPOR();
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(ENG);
        p.add(POR);

        return p;

    }

    private JButton criarBotaoENG() {
        JButton btn = new JButton("English");
         btn.addActionListener(new ActionListener() {

           @Override
            public void actionPerformed(ActionEvent e) {
                Janela.this.setVisible(false);
                DadosENG eng = new DadosENG(null);
            }
        });

        return btn;
    }

    private JButton criarBotaoPOR() {
        JButton btn = new JButton("Portugues");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Janela.this.setVisible(false);
               
                try {
                    DadosPOR port = new DadosPOR();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Janela.class.getName()).log(Level.SEVERE, null, ex);
                
              
            }   catch (IOException ex) {
                    Logger.getLogger(Janela.class.getName()).log(Level.SEVERE, null, ex);
                }}
        });

        return btn;
    }

    private void fechar() {
        String[] opSimNao = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(this,
                "Deseja fechar a aplicação?",
                "...",
                0,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opSimNao,
                opSimNao[1]);

        final int SIM = 0;
        if (resposta == SIM) {
            dispose();
        }
    }

}