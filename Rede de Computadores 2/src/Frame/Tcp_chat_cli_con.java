/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JTextArea;

/**
 *
 * @author Sergio
 */
public class Tcp_chat_cli_con implements Runnable {

	private Socket s;
	private DataInputStream sIn;
	JTextArea chatBox;
	ChatController m_controller;

	public Tcp_chat_cli_con(Socket tcp_s, JTextArea chBox,
							ChatController controller) {
		s = tcp_s;
		chatBox = chBox;
		m_controller = controller;
	}

	public void run() {
		int nChars;
		byte[] data = new byte[300];
		String frase;

		try {
			sIn = new DataInputStream(s.getInputStream());
			while (true) {
				nChars = sIn.read();
				if (nChars == 0) {
					break;
				}
				sIn.read(data, 0, nChars);
				frase = new String(data, 0, nChars);
				chatBox.append(frase + "\n");
			}
		} catch (IOException ex) {
			//System.out.println("Ligacao TCP terminada.");
		}
	}

}
