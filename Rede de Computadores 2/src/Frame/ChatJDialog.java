/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Sergio
 */
public class ChatJDialog extends JDialog {

	ChatController m_controller;
	String frase;
	JButton sendMessage;
	JTextField messageBox;
	JTextArea chatBox;
	JTextField usernameChooser;
	JFrame preFrame;

	ChatJDialog(ChatController controller) {
		m_controller = controller;
	}

	void run() throws IOException, InterruptedException {
		definicoesJanela();
		do {
			for (Ligacao l : m_controller.getM_listaLigados()) {

				Thread serverConn = new Thread(new Tcp_chat_cli_con(l.getSock(), chatBox, m_controller));
				serverConn.start();

				serverConn.join();
			}
		} while (true);

	}

	public void definicoesJanela() {
		this.setTitle("CHAT RCOMP");
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());

		JPanel southPanel = new JPanel();
		southPanel.setBackground(Color.BLUE);
		southPanel.setLayout(new GridBagLayout());

		messageBox = new JTextField(30);
		messageBox.requestFocusInWindow();

		sendMessage = new JButton("Send Message");
		sendMessage.addActionListener(new sendMessageButtonListener());

		chatBox = new JTextArea();
		chatBox.setEditable(false);
		chatBox.setFont(new Font("Serif", Font.PLAIN, 15));
		chatBox.setLineWrap(true);

		mainPanel.add(new JScrollPane(chatBox), BorderLayout.CENTER);

		GridBagConstraints left = new GridBagConstraints();
		left.anchor = GridBagConstraints.LINE_START;
		left.fill = GridBagConstraints.HORIZONTAL;
		left.weightx = 512.0D;
		left.weighty = 1.0D;

		GridBagConstraints right = new GridBagConstraints();
		right.insets = new Insets(0, 10, 0, 0);
		right.anchor = GridBagConstraints.LINE_END;
		right.fill = GridBagConstraints.NONE;
		right.weightx = 1.0D;
		right.weighty = 1.0D;

		southPanel.add(messageBox, left);
		southPanel.add(sendMessage, right);

		mainPanel.add(BorderLayout.SOUTH, southPanel);

		this.add(mainPanel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(470, 300);
		this.setVisible(true);
	}

	class sendMessageButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			if (messageBox.getText().length() < 1) {
				// nao faz nada
			} else if (messageBox.getText().equals(".clear")) {
				messageBox.setText("");
			} else {
				try {
					enviaMensagem();
				} catch (IOException ex) {
					Logger.getLogger(ChatJDialog.class.getName()).
						log(Level.SEVERE, null, ex);
				}
				messageBox.setText("");
			}
			messageBox.requestFocusInWindow();
		}
	}

	public void enviaMensagem() throws IOException {
		for (Ligacao l : m_controller.getM_listaLigados()) {

			byte[] data = new byte[300];

			DataOutputStream sOut = new DataOutputStream(l.getSock().
				getOutputStream());

			while (true) {
				frase = messageBox.getText();
				frase = "(" + m_controller.getNickname() + ") " + frase;
				data = frase.getBytes();
				sOut.write((byte) frase.length());
				sOut.write(data, 0, (byte) frase.length());
			}

		}

	}

}
