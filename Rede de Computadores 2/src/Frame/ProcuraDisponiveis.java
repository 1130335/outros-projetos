/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class ProcuraDisponiveis implements Runnable {

	private List<Ligacao> m_listaPesquisar;
	private List<Ligacao> m_listaDisponiveis;

	public ProcuraDisponiveis(List<Ligacao> listaPesquisar) {
		m_listaPesquisar = listaPesquisar;
	}

	@Override
	public void run() {
		while (true) {
			for (Ligacao l : m_listaPesquisar) {

				try {

					DatagramSocket c = new DatagramSocket(40001);
					c.setSoTimeout(3000);
					byte[] sendData = "DISPONIVEL?".getBytes();

					try {

						DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.
																	   getByName(l.
																		   getNome()), 40001);

						c.send(sendPacket);
					} catch (Exception e) {
					}

					//Wait for a response
					byte[] recvBuf = new byte[15000];

					DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);

					try {
						c.receive(receivePacket);
						String message = new String(receivePacket.getData()).
							trim();
						Socket soc = new Socket(receivePacket.getAddress(), 40000);
						l.setIPdestino(receivePacket.getAddress());
						l.setSock(soc);
						l.setNome(message);
						soc.close();
					} catch (SocketTimeoutException ex) {
						//ex.printStackTrace();
					}
					//Close the port!
					c.close();
				} catch (IOException ex) {
				}
			}
		}
	}

	public List<Ligacao> getDisponiveis() {
		return m_listaDisponiveis;
	}
}
