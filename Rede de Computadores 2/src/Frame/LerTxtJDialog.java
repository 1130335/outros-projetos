/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Sergio
 */
public class LerTxtJDialog extends JDialog {

	ChatController m_controller;
	private JTextField txtField;

	LerTxtJDialog(ChatController controller) {
		m_controller = controller;
	}

	public void run() {

		this.setTitle("Ler servidores pesquisáveis");

		setLayout(new BorderLayout());

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 2));
		p1.add(criarPainelTempExt());

		JPanel p2 = new JPanel();
		p2.setLayout(new FlowLayout(FlowLayout.CENTER));
		p2.add(criarPanelBotoes());

		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setModal(true);
		setVisible(true);
	}

	private JPanel criarPainelTempExt() {
		JPanel p = new JPanel();
		JLabel lbl;

		lbl = new JLabel("Nome do ficheiro(.txt):");

		txtField = new JTextField(15);
		p.add(lbl);
		p.add(txtField);
		return p;
	}

	private JPanel criarPanelBotoes() {
		JButton btnok = criarBotaoOk();
		JButton btncancelar = criarBotaoCancelar();

		JPanel p = new JPanel();
		p.add(btnok);
		p.add(btncancelar);

		return p;

	}

	private JButton criarBotaoOk() {
		JButton btnok = new JButton("OK");
		btnok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String txt = txtField.getText();

				try {
					m_controller.lerTxt(txt);
				} catch (FileNotFoundException ex) {
					Logger.getLogger(LerTxtJDialog.class.getName()).
						log(Level.SEVERE, null, ex);
				}

				dispose();
			}
		});
		return btnok;

	}

	private JButton criarBotaoCancelar() {
		JButton btncancelar;

		btncancelar = new JButton("Cancelar");

		btncancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		return btncancelar;

	}

}
