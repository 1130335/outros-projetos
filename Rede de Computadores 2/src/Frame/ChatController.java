/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Sergio
 */
public class ChatController {

	private List<Ligacao> m_listaPesquisar;
	private List<Ligacao> m_listaDisponiveis;
	private List<Ligacao> m_listaLigados;
	private String nickname;

	public ChatController() {
		m_listaPesquisar = new ArrayList();
		m_listaDisponiveis = new ArrayList();
		m_listaLigados = new ArrayList();
	}

	public List<Ligacao> getListaServidoresDisponiveis() throws IOException {
		ProcuraDisponiveis pd = null;
		new Thread(new ProcuraDisponiveis(m_listaPesquisar)).start();
		return pd.getDisponiveis();
	}

	void fecharSocketsLigados(List<Ligacao> listaLigados) throws IOException {
		for (Ligacao l : listaLigados) {
			l.getSock().close();
			listaLigados.remove(l);

		}
	}

	boolean ligarServidor(int escolha) throws IOException, InterruptedException {
		Ligacao l = this.m_listaDisponiveis.get(escolha);
		while (true) {
			String frase;
			byte[] data = new byte[300];
			DataOutputStream sOut = new DataOutputStream(l.getSock().
				getOutputStream());

			//ENVIAR NICK PARA O SERVIDOR REGISTAR
			frase = "#registar: " + this.nickname;
			data = frase.getBytes();
			sOut.write((byte) frase.length());
			sOut.write(data, 0, (byte) frase.length());
			//caso na funçao recebePermissaoLigacao a resposta do servidor for sucesso o cliente obtem ligaçao
			if (recebePermissaoLigacao(l.getSock())) {
				//System.out.println("Ligado com sucesso");
			} else {
				//System.out.println("Insucesso");
			}
		}
	}

	List<Ligacao> getDisponiveisNaoLigados() throws IOException {
		List<Ligacao> disponiveisNLigados = new ArrayList();
		for (Ligacao l : getListaServidoresDisponiveis()) {
			if (!m_listaLigados.contains(l)) {
				disponiveisNLigados.add(l);
			}

		}

		return disponiveisNLigados;
	}

	boolean recebePermissaoLigacao(Socket s) throws IOException {

		int nChars;
		byte[] data = new byte[300];
		String frase = null;
		DataInputStream sIn;
		while (!frase.equalsIgnoreCase("sucesso") && !frase.
			equalsIgnoreCase("insucesso")) {
			sIn = new DataInputStream(s.getInputStream());

			nChars = sIn.read();
			sIn.read(data, 0, nChars);
			frase = new String(data, 0, nChars);
			if (frase.equalsIgnoreCase("sucesso")) {
				return true;
			} else {

			}
		}
		s.close();
		return false;
	}

	void lerTxt(String txt) throws FileNotFoundException {
		Scanner fTxt = new Scanner(new File(txt), "UTF-8");
		List<Ligacao> ips = new ArrayList<Ligacao>();

		while (fTxt.hasNextLine()) {
			Ligacao l = new Ligacao();
			String linha = fTxt.nextLine().trim();

			l.setNome(linha);

			ips.add(l);
		}

		fTxt.close();

		m_listaPesquisar = ips;
	}

	void alterarRecebe(int escolha) {
		m_listaLigados.get(escolha).setEstadoRecebe();
	}

	void alterarEnvia(int escolha) {
		m_listaLigados.get(escolha).setEstadoEnvia();
	}

	void terminarLigacao(int escolha) throws IOException {
		DataOutputStream sOut = new DataOutputStream(m_listaLigados.get(escolha).
			getSock().getOutputStream());
		sOut.write(0);
		m_listaLigados.get(escolha).getSock().close();
		m_listaLigados.remove(escolha);
	}

	/**
	 * @return the m_listaPesquisar
	 */
	public List<Ligacao> getM_listaPesquisar() {
		return m_listaPesquisar;
	}

	/**
	 * @param m_listaPesquisar the m_listaPesquisar to set
	 */
	public void setM_listaPesquisar(
		List<Ligacao> m_listaPesquisar) {
		this.m_listaPesquisar = m_listaPesquisar;
	}

	/**
	 * @param m_listaDisponiveis the m_listaDisponiveis to set
	 */
	public void setM_listaDisponiveis(
		List<Ligacao> m_listaDisponiveis) {
		this.m_listaDisponiveis = m_listaDisponiveis;
	}

	/**
	 * @return the m_listaLigados
	 */
	public List<Ligacao> getM_listaLigados() {
		return m_listaLigados;
	}

	/**
	 * @param m_listaLigados the m_listaLigados to set
	 */
	public void setM_listaLigados(
		List<Ligacao> m_listaLigados) {
		this.m_listaLigados = m_listaLigados;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
