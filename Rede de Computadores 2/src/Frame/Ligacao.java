/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 *
 * @author Sergio
 */
public class Ligacao {

	private InetAddress IPdestino;
	private int porto;
	private String nome;
	private boolean estadoEnvia;
	private boolean estadoRecebe;
	private Socket sock;

	public Ligacao() {
		estadoEnvia = true;
		estadoRecebe = true;
		nome = new String();
	}

	public boolean verificaDisponibilidade() {
		// Find the server using UDP broadcast

		try {
			//Open a random port to send the package
			DatagramSocket c = new DatagramSocket();
			c.setSoTimeout(3000);
			byte[] sendData = "DISPONIVEL?".getBytes();

			try {

				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.
															   getByName(this.nome), 40001);

				c.send(sendPacket);
			} catch (Exception e) {
			}

			//Wait for a response
			byte[] recvBuf = new byte[15000];

			DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);

			try {
				System.out.println("A verificar");
				c.receive(receivePacket);
				System.out.println("passou");
				String message = new String(receivePacket.getData()).trim();
				Ligacao liga = new Ligacao();
				Socket soc = new Socket(receivePacket.getAddress(), 40000);
				this.setIPdestino(receivePacket.getAddress());
				this.setSock(soc);
				this.setNome(message);
				System.out.println("Mensagem recebida");
				return true;
			} catch (SocketTimeoutException ex) {
				//ex.printStackTrace();
				System.out.println("O servidor nao respondeu");
				//System.out.println("O servidor não respondeu");
			}
			//Close the port!
			c.close();
		} catch (IOException ex) {
		}
		return false;
	}

	/**
	 * @return the IPdestino
	 */
	public InetAddress getIPdestino() {
		return IPdestino;
	}

	/**
	 * @param IPdestino the IPdestino to set
	 */
	public void setIPdestino(InetAddress IPdestino) {
		this.IPdestino = IPdestino;
	}

	/**
	 * @return the porto
	 */
	public int getPorto() {
		return porto;
	}

	/**
	 * @param porto the porto to set
	 */
	public void setPorto(int porto) {
		this.porto = porto;
	}

	/**
	 * @return the Nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param Nome the Nome to set
	 */
	public void setNome(String Nome) {
		this.nome = Nome;
	}

	/**
	 * @return the estadoEnvia
	 */
	public boolean getEstadoEnvia() {
		return estadoEnvia;
	}

	/**
	 * @param estadoEnvia the estadoEnvia to set
	 */
	public void setEstadoEnvia() {
		if (estadoEnvia == true) {
			this.estadoEnvia = false;
		} else {
			this.estadoEnvia = true;
		}

	}

	/**
	 * @return the estadoRecebe
	 */
	public boolean getEstadoRecebe() {
		return estadoRecebe;
	}

	/**
	 * @param estadoRecebe the estadoRecebe to set
	 */
	public void setEstadoRecebe() {
		if (estadoRecebe == true) {
			this.estadoRecebe = false;
		} else {
			this.estadoRecebe = true;
		}
	}

	/**
	 * @return the sock
	 */
	public Socket getSock() {
		return sock;
	}

	/**
	 * @param sock the sock to set
	 */
	public void setSock(Socket sock) {
		this.sock = sock;
	}

	public String toString() {

		return "IP:" + this.IPdestino.getAddress().toString();
	}
}
