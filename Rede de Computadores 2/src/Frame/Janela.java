/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 *
 * @author Sergio
 */
public class Janela extends JFrame {

	JFrame principal = new JFrame("CHAT RCOMP");
	JPanel aux_principal = new JPanel();
	JFrame log = new JFrame("LOGIN");
	ChatController m_controller;

	public Janela(ChatController controller) {
		m_controller = controller;
	}

	public void run() {
		nickname();
	}

	public void principal() {
		aux_principal.setPreferredSize(new Dimension(400, 400));
		log.setVisible(false);
		JMenuBar menubar = new JMenuBar();
		menubar.add(criarMenuOperacoes());
		principal.setJMenuBar(menubar);

		//principal.add(new JLabel(new ImageIcon("ISEP_marca_cor.png")));
//		principal.addWindowListener(new WindowAdapter() {
//			@Override
//			public void windowClosing(WindowEvent e) {
//				fechar();
//			}
//		});
		// principal.setSize(400, 400);
		principal.add(aux_principal);
		principal.setVisible(true);
		principal.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		principal.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				fechar();
			}
		});
		principal.pack();
		principal.setLocationRelativeTo(null);
	}

	public void nickname() {
		principal.setVisible(false);

		JTextField username = new JTextField(15);
		JLabel usernameLabel = new JLabel("Digite username:");
		JButton login = new JButton("Login");
		login.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (username.getText().length() <= 15 && username.getText().
					length() > 0) {
					principal();
				}
			}
		});

		JPanel prePanel = new JPanel(new GridBagLayout());

		GridBagConstraints preRight = new GridBagConstraints();
		preRight.insets = new Insets(0, 0, 0, 10);
		preRight.anchor = GridBagConstraints.EAST;
		GridBagConstraints preLeft = new GridBagConstraints();
		preLeft.anchor = GridBagConstraints.WEST;
		preLeft.insets = new Insets(0, 10, 0, 10);
		preRight.fill = GridBagConstraints.HORIZONTAL;
		preRight.gridwidth = GridBagConstraints.REMAINDER;

		prePanel.add(usernameLabel, preLeft);
		prePanel.add(username, preRight);
		log.add(BorderLayout.CENTER, prePanel);
		log.add(BorderLayout.SOUTH, login);
		log.setSize(400, 300);
		log.setVisible(true);
		log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		log.pack();
		log.setLocationRelativeTo(null);

	}

	private JMenu criarMenuOperacoes() {
		JMenu menu;
		menu = new JMenu("Menu");
		menu.add(lerTxt());
		menu.add(alterarEstadosEnvia());
		menu.add(alterarEstadosRecebe());
		menu.add(desligarServidor());
		menu.add(ligarServidor());
		menu.add(chat());
		menu.add(sair());
		return menu;
	}

	private JMenuItem sair() {
		JMenuItem itemSair;
		itemSair = new JMenuItem("Sair");
		itemSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fechar();
			}
		});

		return itemSair;
	}

	private JMenuItem alterarEstadosEnvia() {
		JMenuItem item;

		item = new JMenuItem("Alterar o estado ENVIA a servidor(es)");

		item.setMnemonic(KeyEvent.VK_E);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AlterarEnviaJDialog aejd = new AlterarEnviaJDialog(m_controller);
				aejd.run();
			}
		});

		return item;
	}

	private JMenuItem alterarEstadosRecebe() {
		JMenuItem item;

		item = new JMenuItem("Alterar o estado RECEBE a um servidor(es)");

		item.setMnemonic(KeyEvent.VK_R);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AlterarRecebeJDialog arjd = new AlterarRecebeJDialog(m_controller);
				arjd.run();
			}
		});

		return item;
	}

	private JMenuItem desligarServidor() {
		JMenuItem item;

		item = new JMenuItem("Desligar servidor(es)");

		item.setMnemonic(KeyEvent.VK_D);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl D"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DesligarServidorJDialog dsjd = new DesligarServidorJDialog(m_controller);
				dsjd.run();
			}
		});

		return item;
	}

	private JMenuItem ligarServidor() {
		JMenuItem item;
		item = new JMenuItem("Ligar a servidor disponivel");

		item.setMnemonic(KeyEvent.VK_L);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl L"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LigarServidorJDialog lsjd = new LigarServidorJDialog(m_controller);
				try {
					if (!m_controller.getDisponiveisNaoLigados().isEmpty()) {
						lsjd.run();
					}
				} catch (IOException ex) {
					Logger.getLogger(Janela.class.getName()).
						log(Level.SEVERE, null, ex);
				}

			}
		});

		return item;
	}

	private JMenuItem chat() {
		JMenuItem item;
		item = new JMenuItem("Chat");

		item.setMnemonic(KeyEvent.VK_C);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl C"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChatJDialog chat = new ChatJDialog(m_controller);
				if (!m_controller.getM_listaLigados().isEmpty()) {
					try {
						chat.run();
					} catch (IOException ex) {
						Logger.getLogger(Janela.class.getName()).
							log(Level.SEVERE, null, ex);
					} catch (InterruptedException ex) {
						Logger.getLogger(Janela.class.getName()).
							log(Level.SEVERE, null, ex);
					}
				}

			}
		});

		return item;
	}

	private JMenuItem lerTxt() {
		JMenuItem item;
		item = new JMenuItem("Carregar Servidores de ficheiro");

		item.setMnemonic(KeyEvent.VK_F);
		item.setAccelerator(KeyStroke.getKeyStroke("ctrl F"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LerTxtJDialog txt = new LerTxtJDialog(m_controller);
				txt.run();
			}
		});

		return item;
	}

	private void fechar() {

		String[] opSimNao = {"Sim", "Não"};
		int resposta = JOptionPane.showOptionDialog(this,
													"Deseja fechar a aplicação?",
													"Chat RCOMP",
													0,
													JOptionPane.QUESTION_MESSAGE,
													null,
													opSimNao,
													opSimNao[1]);
		final int SIM = 0;
		if (resposta == SIM) {
			System.exit(0);
			// dispose();
		}
	}

}
