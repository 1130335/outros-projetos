/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Sergio
 */
public class DesligarServidorJDialog extends JDialog {

	ChatController m_controller;
	int escolha;

	DesligarServidorJDialog(ChatController controller) {
		m_controller = controller;
	}

	void run() {

		this.setTitle("Desligar conexão");

		setLayout(new BorderLayout());

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 2));
		p1.add(criarPaineEscolha());
		JPanel p2 = new JPanel();
		p2.setLayout(new FlowLayout(FlowLayout.CENTER));

		p2.add(criarPanelBotoes());

		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setModal(true);
		setVisible(true);
	}

	private JPanel criarPaineEscolha() {
		JPanel p = new JPanel();
		JLabel lbl = new JLabel("Connexão:");
		p.add(lbl);

		p.add(criarComboBoxServidores());
		return p;
	}

	private JComboBox criarComboBoxServidores() {
		ArrayList<String> p = new ArrayList<>();
		for (Ligacao l : m_controller.getM_listaLigados()) {
			p.add(l.toString());
		}
		String[] a = new String[m_controller.getM_listaLigados().size()];
		p.toArray(a);
		JComboBox servidoresSelect = new JComboBox(a);

		servidoresSelect.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				escolha = servidoresSelect.getSelectedIndex();
			}

		});
		return servidoresSelect;
	}

	private JPanel criarPanelBotoes() {
		JButton btnok = criarBotaoOk();
		JButton btncancelar = criarBotaoCancelar();

		JPanel p = new JPanel();
		p.add(btnok);
		p.add(btncancelar);
		return p;

	}

	private JButton criarBotaoOk() {
		JButton btnok = new JButton("OK");
		btnok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					m_controller.terminarLigacao(escolha);
				} catch (IOException ex) {
					Logger.getLogger(DesligarServidorJDialog.class.getName()).
						log(Level.SEVERE, null, ex);
				}
				dispose();

			}
		});
		return btnok;

	}

	private JButton criarBotaoCancelar() {
		JButton btncancelar;
		btncancelar = new JButton("Cancelar");

		btncancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		return btncancelar;

	}

}
