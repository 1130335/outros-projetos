:- dynamic vendedor/4.

% Nota: verificar disponibilidade de imoveis com a dos clientes e
% vendedores. N�o verifiquei isso, mas se for preciso poder� se criar
% mais imoveis, clientes e vendedores.
% Zonas
zona(a, "Hospital Sao Joao").
zona(b, "Areosa").
zona(c, "Prelada").
zona(d, "Paranhos").
zona(e, "Contumil").
zona(f, "Boavista").
zona(g, "Antas").
zona(h, "Fonte da Moura").
zona(i, "Foz do Douro").
zona(j, "Campanha").
zona(l, "Massarelos").
zona(m, "Lordelo do Ouro").
zona(n, "Campo 24 Agosto").
zona(o, "Azevedo").
zona(p, "Vitoria").
zona(q, "Freixo").
zona(r, "Fontainhas").

% Dist�ncias (em minutos)
distancia(a,b,7).
distancia(b,a,7).
distancia(a,c,8).
distancia(c,a,8).
distancia(a,d,5).
distancia(d,a,5).
distancia(a,e,12).
distancia(b,e,6).
distancia(d,e,7).
distancia(c,d,8).
distancia(c,f,13).
distancia(c,i,18).
distancia(d,f,15).
distancia(f,h,3).
distancia(f,m,4).
distancia(f,i,6).
distancia(i,m,5).
distancia(e,f,12).
distancia(e,g,2).
distancia(e,j,5).
distancia(g,h,16).
distancia(g,l,18).
distancia(g,j,6).
distancia(h,m,4).
distancia(h,n,15).
distancia(h,l,3).
distancia(j,l,16).
distancia(j,o,3).
distancia(l,n,18).
distancia(l,o,20).
distancia(m,n,15).
distancia(m,p,10).
distancia(n,p,11).
distancia(n,r,12).
distancia(o,n,3).
distancia(o,q,5).
distancia(p,r,4).

% Coordenadas (x,y)
coordenadas(a,45,95).
coordenadas(b,90,95).
coordenadas(c,15,85).
coordenadas(d,40,80).
coordenadas(e,70,80).
coordenadas(f,25,65).
coordenadas(g,65,65).
coordenadas(h,45,55).
coordenadas(i,5,50).
coordenadas(j,80,50).
coordenadas(l,65,45).
coordenadas(m,25,40).
coordenadas(n,55,30).
coordenadas(o,80,30).
coordenadas(p,25,15).
coordenadas(q,80,15).
coordenadas(r,55,10).

% imoveis(id,zona,tamanhoCasa,valorCasa,Se tem ou nao chave,
% horariosdis, minutos necess�rios para a visita

% Im�veis (disponibilizade (0, 0:00, 0:00) significa que ele pode ser
% visitado a qualquer hora porque o vendedor tem a chave)
imovel(id01, d, "T2", 25000, n, [(2, 9, 19),(4, 14, 19), (6, 9, 13)], 60).
imovel(id02, r, "T1", 10000, s, [(0, 0, 0)],45).
imovel(id03, i, "Moradia", 500000, s, [(0, 0, 0)],120).
imovel(id04, p, "T3", 150000, n, [(2, 10, 18),(3, 10, 18),(5, 14, 18)],90).
imovel(id05, a, "T2", 30000, n, [(1, 14, 17),(2, 9.3, 18), (3, 8, 16), (4, 16, 18),(5, 9, 18), (6, 9, 13),60],45).
imovel(id06,f, "T1", 20000, s, [(0, 0, 0)],45).

% Clientes
% cliente(idCliente,nome,horario)
cliente(c01, "Fernanda Gomes", [(2, 9, 16), (3, 14, 19),(7, 14, 18)]).
cliente(c02, "Daniel Sousa", [(7, 10, 12),(7, 14, 18)]).
cliente(c03, "Sergio Ramos", [(1, 15, 19), (2, 9, 10), (3, 14, 19)]).

% Rela��es entre clientes e imoveis
% cliente_imovel(idCliente,listaImoveis)
cliente_imovel(c01, [id02, id06]).
cliente_imovel(c02, [id01, id02, id03, id04, id06]).
cliente_imovel(c03, [id01, id03, id05]).

% Vendedores
% vendedor(idvendedor, nome, horario,zona onde est�)
vendedor(v01, "Antonio Tomas", [(2, 8, 18), (3, 8, 18), (4, 13, 18), (5, 8, 18), (6, 8, 17)], a).
vendedor(v02, "Luisa Marques", [(1, 10, 16), (7, 10, 17)], b).
vendedor(v03, "Ana Paula Soares", [(1, 9, 13),(2, 9, 16),(5, 9, 12), (5, 14, 16), (6, 9, 16), (7,14, 16)], c).




casasDis(_,[],[]).
casasDis(Hc,[Id|Ids1],[Id|Ids2]):-
	compativel(Hc,Id),!,
	casasDis(Hc,Ids1,Ids2).
casasDis(Hc,[_|Ids1],Ids2):-
	casasDis(Hc,Ids1,Ids2).

compativel(_,Id):-
	imovel(Id,_,_,_,_,HI,_),
	member((0,0,0),HI).
compativel(Hc,Id):-
	imovel(Id,_,_,_,_,HI,TV),
	member((X,Hic,Hfc),Hc),
	member((X,Hii,Hfi),HI),
	intersecao([Hii,Hfi],[Hic,Hfc],[I|F]),
	H is (F-I)*60,
	TV=<H,!.
	%Hii<Hic,Hfc<Hfi.  %Mudar para intersec��o / Aten��o ao tempo de visita / Aten��o � agenda

%CASAS

intersecao([],[],_).
intersecao([Hii,Hfi],[Hic,Hfc],Ht):-
	intersecao1([Hii,Hfi],[Hic,Hfc],Ht),!.

intersecao([_|T],L,Ht):-
	intersecao(T,L,Ht).

 intersecao1([Hii,Hfi],[Hic,Hfc],[I,F]):- (((Hic=0,Hfc=0), I is Hii, F
 is Hfi);(Hii=0,Hfi=0), I is Hic, F is Hfc).


intersecao1([Hii,Hfi],[Hic,Hfc],_):-
	(Hic>Hfi;Hii>Hfc),!,fail.


intersecao1([Hii,Hfi],[Hic,Hfc],[I,F]):-
	max(Hii,Hic,I),
	min(Hfi,Hfc,F).








% hORARIOS

intersecao3([],[],_).
intersecao3([Hii,Hfi],[Hic,Hfc],Ht):-
	intersecao4([Hii,Hfi],[Hic,Hfc],Ht),!.

intersecao3([_|T],L,Ht):-
	intersecao3(T,L,Ht).

 intersecao4([Hii,Hfi],[Hic,Hfc],[I,F]):- (((Hic=0,Hfc=0), I is Hii, F
 is Hfi);(Hii=0,Hfi=0), I is Hic, F is Hfc).


intersecao4([Hii,Hfi],[Hic,Hfc],_):-
	(Hic>=Hfi;Hii>=Hfc),!,fail.


intersecao4([Hii,Hfi],[Hic,Hfc],[I,F]):-
	max(Hii,Hic,I),
	min(Hfi,Hfc,F).




max(I1,I2,I1):-I1>=I2,!.
max(_,I2,I2).
min(F1,F2,F1):-F1=<F2,!.
min(_,F2,F2).

% melhorCaminho(idCliente,IdVendedor,ListaMelhorCaminho) -->
% D� uma lista ordenada com as casas disponiveis
melhorCaminho(Idc,Idv,Lmc,LO,TV):-cliente(Idc,_,H), cliente_imovel(Idc,Lc),
       vendedor(Idv,_,_,Z),
       casasDis(H,Lc,L),
       ordenarCasas(L,Lmc,LO,TV,Z),!.

ordenarCasas([],[],[],[],_).
ordenarCasas(L,[Id|Ids],[LC|T],[TV|TV1],Z):-
	melhorCasa(L,Id,Z),
	delete(L,Id,L1),
	imovel(Id,ZI,_,_,_,_,_),
	primeiroOMelhor(Z,ZI,LC,TV),
	ordenarCasas(L1,Ids,T,TV1,ZI).

melhorCasa([],_,_).
melhorCasa([Id1|Ids],Id,ZV):-melhorCasa2(Ids,Id1,Id,ZV).

melhorCasa2([Id1|Ids],Id2,Id,ZV):-
	imovel(Id1,ZI1,_,_,_,_,_),
	imovel(Id2,ZI2,_,_,_,_,_),
	primeiroOMelhor(ZV,ZI1,_,C1),
	primeiroOMelhor(ZV,ZI2,_,C2),
	(C1<C2,melhorCasa2(Ids,Id1,Id,ZV);
	C2=<C1,melhorCasa2(Ids,Id2,Id,ZV)).
melhorCasa2([],Id2,Id2,_).



%Primeiro o Melhor
primeiroOMelhor(Origem,Destino,Solucao,Custo):-
		primeiroOMelhor2(Destino,[Origem],Solucao,Custo).


primeiroOMelhor2(Destino,[Destino|T],Solucao,0):-
				reverse([Destino|T],Solucao).

primeiroOMelhor2(Destino,[H|T],Solucao,Custo):-
			Destino\==H,
			findall((CE,CX,[X,H|T]),(distancia(H,X,CX),\+ member(X,[H|T]),estimativa(X,Destino,CE)),Novos),
			sort(Novos,LS),
			LS=[(_,CMX,Melhor)|_],
			primeiroOMelhor2(Destino,Melhor,Solucao,C),
			Custo is C+CMX.


estimativa(N1,N2,Valor):-
			coordenadas(N1,X1,Y1),
			coordenadas(N2,X2,Y2),
			distancia1(X1,Y1,X2,Y2,Valor).

distancia1(X1,Y1,X2,Y2,Distancia):-
			Distancia is sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)).


agenda([],_).
agenda([LCli|LCli2],IdVendedor):-
	melhorCaminho(LCli,IdVendedor,LCO,_,TV),

	write(IdVendedor),
	write('>'),
	write(LCli),
	write('\n'),
	verificarHorario(LCO,IdVendedor,LCli,TV,LCP),
	write(LCP),
	write('\n'),
	alterarHorarioVendedor(IdVendedor,LCli),
	vendedor(IdVendedor,_,HVend,_),
	write('Novo Hor�rio do Vendedor: '),
	write(HVend),
	write('\n'),
	write('\n'),
	agenda(LCli2,IdVendedor),!.


verificarHorario(LCO,IdVendedor,IdCliente,TVC, LCP):-
	cliente(IdCliente,_,HC),
	vendedor(IdVendedor,_,HV,_),
	compativel2(HC,HV,LF),
	apagaListasVazias(LF,LHF),
	% VER TEMPO DISPONIVEL TD is (F-I)*60,
	verificarHorario1(LCO,TVC,LHF,LCP),!.

apagaListasVazias([],[]).
apagaListasVazias([L1|L2],[L1|L3]):-
	length(L1,T),
	T\=0,
	apagaListasVazias(L2,L3).
apagaListasVazias([L1|L2],L3):-
	apagaListasVazias(L2,L3).


verificarHorario1(_,_,[],[]).
verificarHorario1(LCO,TVC,[PH|OH],[H|T]):-
	verificarHorario4(LCO,TVC,PH,H),
	apagarImoveisVisitados(LCO,H,TVC,NLTV,NLI),
	verificarHorario1(NLI,NLTV,OH,T).
verificarHorario1(LCO,TVC,[PH|OH],[H|T]):-
	verificarHorario4(LCO,TVC,PH,H),
	verificarHorario1(LCO,TVC,OH,T).

apagarImoveisVisitados(LCO,(_,L),TV,NTV,NL):-
	apagarAux(LCO,L,TV,NTV,NL).

apagarAux(_,[],_,[],[]).
apagarAux(LCO,[ID|IDS],TV,NTV,NL):-
       apagarAux2(LCO,ID,TV,NTV,NL),
       apagarAux(NL,IDS,NTV,NTV1,NL1).

apagarAux2([],_,[],[],[]).
apagarAux2([LCO|LCS],ID,[TV|TVS],[TV|LTV],[LCO|L]):-
	LCO\=ID,
	apagarAux2(LCS,ID,TVS,LTV,L).
apagarAux2([_|LCS],ID,[_|TVS],NL,L):-apagarAux2(LCS,ID,TVS,NL,L).

verificarHorario4(_,_,[],[]).
verificarHorario4(LCO, TVC, [(D,[I,F])],(D,LC)):-
	TD is (F-I)*60,
	verificarHorario2(LCO,TD,TVC,LC).

verificarHorario2([],_,[],[]).
verificarHorario2([],0,_,[]).
verificarHorario2([LCO|LOS],TD,[TV|TVS],[LCO|L]):-
	verificarHorario3(LCO,TD,TV,TD1),
	verificarHorario2(LOS,TD1,TVS,L).

verificarHorario2([LCO|LOS],TD,[TV|TVS],L):-
	verificarHorario2(LOS,TD,TVS,L).



verificarHorario3(LCO,TD,TV,TD1):-
	imovel(LCO,_,_,_,_,_,TVI),
	TD2 is TVI+TV,
	TD>=TD2,
	TD1 is TD-TD2.

compativel2(_,[],[]).
compativel2(HC,[HV|HV1],[HLivre|HLivre2]):-
	compativel3(HC,HV,HLivre),
	compativel2(HC,HV1,HLivre2),
	!.

compativel3([],_,[]).

compativel3([HC|HC1],HV,[HLivre|HLivre2]):-compativel4(HC,HV,HLivre),!,compativel3(HC1,HV,HLivre2).

compativel3([HC|HC1],HV,HLivre):-compativel3(HC1,HV,HLivre).




compativel4((X,HCI,HCF),(X1,HVI,HVF),(X,HLivre)):-X=X1,intersecao([HCI,HCF],[HVI,HVF],HLivre).
compativel4((X,HCI,HCF),(X1,HVI,HVF),(X,HLivre)):-(X\=0,X1=0),intersecao([HCI,HCF],[HVI,HVF],HLivre).
compativel4((X,HCI,HCF),(X1,HVI,HVF),(X1,HLivre)):-(X=0,X1\=0),intersecao([HCI,HCF],[HVI,HVF],HLivre).


alterarHorarioVendedor(IdVendedor,IdCliente):-
	vendedor(IdVendedor,_,HV,_),
	cliente(IdCliente,_,HC),
	compativel2(HC,HV,HLivre),
	apagaListasVazias(HLivre,HLivreNova),
	alterarHorarioVendedor2(IdVendedor,HLivreNova,HV).

alterarHorarioVendedor2(_,[],_).
alterarHorarioVendedor2(IdVendedor,[[H1]|Outros],HV):-
	alterarHorarioVendedor3(IdVendedor,H1,HV),
	vendedor(IdVendedor,_,HVnovo,_),
	alterarHorarioVendedor2(IdVendedor,Outros,HVnovo).

alterarHorarioVendedor3(_,_,[]).
alterarHorarioVendedor3(IdVend,(Dia1,[HorarioInc2,HorarioFim2]),[(Dia,HorarioInc1,HorarioFim1)|Outros]):-
	Dia=Dia1,
	intersecao2([HorarioInc2,HorarioFim2],[HorarioInc1,HorarioFim1],HorarioLivre),
	guardarFacto(IdVend,Dia,HorarioLivre),
	alterarHorarioVendedor3(IdVend,(Dia1,[HorarioInc2,HorarioFim2]),Outros).


alterarHorarioVendedor3(IdVend,(Dia1,[HorarioInc2,HorarioFim2]),[(Dia,_,_)|Outros]):-
	Dia\=Dia1,
	alterarHorarioVendedor3(IdVend,(Dia1,[HorarioInc2,HorarioFim2]),Outros).

guardarFacto(IdVend,Dia,HorarioLivre):-
	vendedor(IdVend,NomeVend,HorarioAntigo,ZonaVend),
	retract(vendedor(IdVend,NomeVend,HorarioAntigo,ZonaVend)),
	modificaHorario(Dia,HorarioLivre,HorarioAntigo,LNova),
	assertz(vendedor(IdVend,NomeVend,LNova,ZonaVend)).

modificaHorario(Dia,HorarioLivre,HorarioAntigo,LNova):-
	delete(HorarioAntigo,(Dia,_,_),LN),
	length(HorarioLivre,T),
	T=0,
	modificaHorario2(Dia,HorarioLivre,Li2),
	uniao(LN,Li2,LNova).


modificaHorario(Dia,[Horario1|Outros],HorarioAntigo,LNova):-
	delete(HorarioAntigo,(Dia,_,_),LN),
	modificaHorario2(Dia,Horario1,Li2),
	uniao(Li2,LN,LNova1),
	modificaHorario3(Outros,Outros2),
	modificaHorario2(Dia,Outros2,LN2),
	uniao(LN2,LNova1,LNova).


modificaHorario(Dia,HorarioLivre,HorarioAntigo,LNova):-
	delete(HorarioAntigo,(Dia,_,_),LN),
	modificaHorario2(Dia,HorarioLivre,Li2),
	uniao(LN,Li2,LNova).





modificaHorario3([O1|_],O1).

modificaHorario2(Dia,[HoraInc1,HoraFim1],[(Dia,HoraInc1,HoraFim1)]).

intersecao2([],_,[]).
intersecao2([X,Y],[X1,Y1],[[X1,X2],[Y2,Y1]]):-intersecao3([X,Y],[X1,Y1],[X2,Y2]),
	X2>X1, Y2<Y1,!.
intersecao2([X,Y],[X1,Y1],[Y2,Y1]):-intersecao3([X,Y],[X1,Y1],[X2,Y2]),
	X2=X1, Y2<Y1,!.
intersecao2([X,Y],[X1,Y1],[X1,X2]):-intersecao3([X,Y],[X1,Y1],[X2,Y2]),
	X2>X1, Y2=Y1,!.
intersecao2([X,Y],[X1,Y1],[]):-true,!.





uniao([ ],L,L).
uniao([X|L1],L2,LU):-member(X,L2),!,uniao(L1,L2,LU).
uniao([X|L1],L2,[X|LU]):-uniao(L1,L2,LU).














