/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.observer;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseTypeEntry;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.RepositoryFactory;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Pedro_Coelho_1130353
 */
public class ExpenseAlertUC4O implements ExpenseAlertInterface{

    private final int xPercent = 104;
    
    //Colocar metodo no Budget para retornar o budget atual (budget do presente mês).
    
    @Override
    public void validateExpense(Expense exp) {
        RepositoryFactory repfac = Persistence.getRepositoryFactory();
        BudgetRepository budfac = repfac.getBudgetRepository();
        Iterator<Budget> itr = budfac.iterator(xPercent);
        
        Budget thisMonthsBudget = null;
        BigDecimal entryValue = new BigDecimal(0);
        BigDecimal budgetPercent = new BigDecimal(0);
        Calendar c = Calendar.getInstance();
        int presentYear = c.get(Calendar.YEAR), presentMonth = c.get(Calendar.MONTH);
        
        while(itr.hasNext()) {
           Budget budget = itr.next();
           
           if(budget.getMonth() == presentMonth+1 && budget.getYear()== presentYear+1){
               thisMonthsBudget = budget;
           }
        }
        
        List<Entry> listOfEntrys = thisMonthsBudget.getEntries();
        
        for(Entry e : listOfEntrys)
        {
            if(e instanceof ExpenseTypeEntry)
            {
                if( ((ExpenseTypeEntry)e).getExpenseType() == exp.getType() ){
                    entryValue = e.getValue();
                }
            }
        }
        
        budgetPercent = entryValue.multiply(new BigDecimal(xPercent));
        
        if(exp.getValue() > budgetPercent.doubleValue()){
            exp.addAlert(this);
        }
    }

    @Override
    public String alertRaised() {
        return "Warning!!! The register despense has surpassed the " + xPercent + "established for its expense type!";
    }
    
}
