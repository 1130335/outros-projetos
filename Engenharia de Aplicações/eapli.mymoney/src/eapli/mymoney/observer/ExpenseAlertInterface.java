/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.observer;

import eapli.mymoney.domain.Expense;

/**
 *
 * @author Pedro_Coelho_1130353
 */
public interface ExpenseAlertInterface {

	//Method that validates if an expense os valid or not. Each expense alert implements this method accordingly.
	void validateExpense(Expense exp);

	//Method that returns the text that the raised alert wants to be displayed.
	String alertRaised();
}
