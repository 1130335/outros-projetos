/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.DomainEntity;
import eapli.util.Validations;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class ExpenseType implements DomainEntity {

	@Id
	private String text;
	private boolean active;

	protected ExpenseType() {
	}

	public ExpenseType(final String text) {
		if (Validations.isNullOrWhiteSpace(text)) {
			throw new IllegalArgumentException();
		}
		this.text = text;
		this.active = true;
	}

	/**
	 * returns the description text of this expense type
	 *
	 * this method is only provided for user output
	 *
	 * @return
	 */
	public String description() {
		return text;
	}

	/**
	 * Method to return the flag status of the Expense Type
	 *
	 * @return true, if active
	 */
	public boolean getFlag() {

		return this.active;
	}

	/**
	 * Method to return the flag status of the Expense Type
	 *
	 * @return true, if active
	 */
	public void setFlag(boolean val) {

		this.active = val;
	}

	public void setDescription(String str) {
		text = str;
	}

	public boolean equals(ExpenseType other) {
//		if (other == null) {
//			return false;
//		}
//		if (other == this) {
//			return true;
//		}
//		if (!(other instanceof ExpenseType)) {
//			return false;
//		}
//		ExpenseType etOther = (ExpenseType) other;
//		return this.description().equals(etOther.description());
		return this.text.equalsIgnoreCase(other.toString());
	}

	@Override
	public String toString() {
		return text;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + (this.text != null ? this.text.hashCode() : 0);
		return hash;
	}

	@Override
	public Object id() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
