/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author RICARDLEITE
 */
public class DeleteExpenseTypeValidation {

	/**
	 * Method to validate if it is possible to remove the expense type
	 *
	 * @param expenseTypeRepository Expense Type Repository
	 * @param expenseTypeToRemove Expense Type to remove
	 * @return true, if possible to delete the expense type
	 */
	public boolean validate(ExpenseType expenseTypeToRemove) {

		ExpenseRepository expenseRepository = Persistence.getRepositoryFactory().
			getExpenseRepository();
		ExpenseTypeRepository expenseTypeRepository = Persistence.
			getRepositoryFactory().getExpenseTypeRepository();

		List<Expense> list = expenseRepository.all();

		if (!list.isEmpty()) {
			for (Expense exp : list) {
				if (exp.getType().equals(expenseTypeToRemove)) {

					expenseTypeRepository.inactive(expenseTypeToRemove);
					return false;
				} else {
					expenseTypeRepository.remove(expenseTypeToRemove);
					return true;
				}
			}
		} else {
			expenseTypeRepository.remove(expenseTypeToRemove);
			return true;
		}
		return false;
	}

}
