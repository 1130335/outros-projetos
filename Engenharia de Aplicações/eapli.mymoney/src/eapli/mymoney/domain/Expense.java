/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.DomainEntity;
import eapli.mymoney.domain.Payment.PaymentMethod;
import eapli.mymoney.observer.ExpenseAlertInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Paulo Silva
 * @author Ivo Teixeira
 * @author Paulo Andrade
 */
@Entity
public class Expense implements DomainEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String comment;
    private double value;
    //List of raised alerts during the register of this expense.
    private List<ExpenseAlertInterface> raisedAlerts;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    @ManyToOne(cascade = CascadeType.MERGE)
    private ExpenseType type;

    public Expense() {
        this.raisedAlerts = new ArrayList<>();
    }

    public Expense(String comment, double value, Date date, ExpenseType type, ExpenseGroup eg, PaymentMethod p) {
        this.raisedAlerts = new ArrayList<>();
        this.comment = comment;
        if (value >= 0) {
            this.value = value;
        } else {
            System.out.println("Error: negative value not valid\n");
        }
        this.date = date;
        this.type = type;

    }

    public String getComment() {
        return comment;
    }

    public double getValue() {
        return value;
    }

    public Date getDate() {
        return date;
    }

    public ExpenseType getType() {
        return type;
    }

    public void addAlert(ExpenseAlertInterface alert){
        this.raisedAlerts.add(alert);
    }
    
    @Override
    public String toString() {
        String str = "EXPENSE DATA: ";
        str += "\nComment: " + getComment();
        str += "\nValue: " + getValue();
        str += "\nDate: " + getDate();
        str += "\nExpense Type: " + getType();
        return str;
    }

    public Long getId() {
        return id;
    }

    @Override
    public Object id() {
        return this.id;
    }
}
