/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.mymoney.strategy.ExportExpenses;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rita
 */
public class ExportExpensesToCSV implements ExportExpenses {

	private List<Expense> lExpenses;

	public ExportExpensesToCSV() {

	}

	@Override
	public void export() {
		final String header = "ID, Comment, Value, Date, Type\n";
		Formatter out = null;
		try {
			out = new Formatter("C:\\Users\\Rita\\Desktop\\ISEP\\EAPLI\\eapli-2015-2db\\expensegroup.csv");
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ExportExpensesToCSV.class.getName()).
				log(Level.SEVERE, null, ex);
		}
		out.format("%s", header);
		for (Expense e : this.lExpenses) {
			out.format("%s, %s, %s, %s, %s\n", String.valueOf(e.getId()), e.
					   getComment(), e.getValue(), e.getDate(), e.getType().
					   description());
		}
		out.close();
	}

	public void setList(List<Expense> lex) {
		this.lExpenses = lex;
	}

}
