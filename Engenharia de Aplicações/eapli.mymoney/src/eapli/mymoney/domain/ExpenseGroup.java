/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.DomainEntity;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Utilizador
 */
@Entity
@Table(name = "EXPENSEGROUP")
public class ExpenseGroup implements DomainEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idExpenseGroup;

	private String name;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date initialDate;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date endDate;
	private List<ExpenseType> typesList;
	private float totalValue;
	private float somatorio;
	private boolean active = true; //true active / false inactive

	public ExpenseGroup() {

	}

	public ExpenseGroup(String nd) {

	}

	public ExpenseGroup(String name, Date initialDate, Date endDate) {
		this.name = name;
		this.initialDate = initialDate;
		this.endDate = endDate;
		this.totalValue = 0;
		this.somatorio = 0;

	}

	public ExpenseGroup(ExpenseGroup expenseGroup) {
		this.endDate = expenseGroup.endDate;
		this.idExpenseGroup = expenseGroup.idExpenseGroup;
		this.name = expenseGroup.name;
		this.totalValue = expenseGroup.totalValue;
		this.typesList = expenseGroup.typesList;
		this.initialDate = expenseGroup.initialDate;
	}

	public void setData(String name, Date beginD, Date endD) {
		this.name = name;
		this.initialDate = beginD;
		this.endDate = endD;
	}

	public ExpenseGroup clone() {
		ExpenseGroup exp = new ExpenseGroup();
		exp.name = this.name;
		exp.initialDate = this.initialDate;
		exp.endDate = this.endDate;
		exp.totalValue = this.totalValue;
		return exp;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return this.name;
	}

	public Date getInitialDate() {
		return this.initialDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public boolean hasExpense() {

		return this.getTypesList().size() > 0;
	}

	public boolean validate(String name, Date beginD, Date endD) {

		return beginD.before(endD);
	}

	public boolean validateOptionalData(float value) {
		if (value > 0) {
			return true;
		}
		return false;
	}

	/**
	 * @return the typesList
	 */
	public List<ExpenseType> getTypesList() {
		return typesList;
	}

	/**
	 * @param typesList the typesList to set
	 */
	public void setTypesList(
		List<ExpenseType> typesList) {
		this.typesList = typesList;
	}

	public void addExpenseType(ExpenseType xtype) {
		getTypesList().add(xtype);
	}

	public void setOptionalData(float value) {
		this.totalValue = value;
	}

	public float getTotalValue() {
		return this.totalValue;
	}

	@Override
	public Object id() {
		return idExpenseGroup;
	}

	public void cancelExpenseGroup(boolean val) {
		this.active = val;
	}

	public boolean getState() {
		return this.active;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ExpenseGroup other = (ExpenseGroup) obj;
		if (!Objects.equals(this.idExpenseGroup, other.idExpenseGroup)) {
			return false;
		}
		return Objects.equals(this.name, other.name);
	}

	public String toString() {
		return "Expense Group: " + name + ",initial date: " + this.initialDate.
			toString() + ",end date: " + this.endDate.toString() + ",total value: " + totalValue;
	}

	public float getSumVal() {
		return somatorio;
	}
}
