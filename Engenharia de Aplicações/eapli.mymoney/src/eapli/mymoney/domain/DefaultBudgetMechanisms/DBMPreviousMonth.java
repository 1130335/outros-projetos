/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.DefaultBudgetMechanisms;

import eapli.framework.patterns.AggregateRoot;
import eapli.framework.patterns.DomainEntity;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author João Cabral
 */
@Entity
public class DBMPreviousMonth implements DefaultBudgetMechanismInterface, Serializable, AggregateRoot<Long>, DomainEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Budget budget;
    private BudgetRepository budgetRepo;

    public DBMPreviousMonth() {
        budget = new Budget();
        budgetRepo = null;
    }


    @Override
    public Budget createBudget(BudgetRepository br) {
        budget = new Budget();
        this.budgetRepo = br;
        
        long numberBudgets = budgetRepo.size();
        Budget lastMonthsBudget = budgetRepo.getLastMonthsBudget();

        if (lastMonthsBudget == null) {
            return lastMonthsBudget;
        }

        List<Entry> tmpList = lastMonthsBudget.getEntries();
        for (Entry tmpList1 : tmpList) {
            budget.addEntry(tmpList1);
        }

        return budget;
    }

    @Override
    public Long id() {
        return this.id;
    }

}
