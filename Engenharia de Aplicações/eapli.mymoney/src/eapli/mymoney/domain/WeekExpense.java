/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Daniela Maia
 */
public class WeekExpense {

	public Double getWeekExpense(List<Expense> rep) {

		List<Expense> rep1 = null;
		Date d;

		// get today and clear time of day
		Calendar cal = Calendar.getInstance();

		int inicio = cal.getFirstDayOfWeek();
		int date = Calendar.DATE;
		int month = Calendar.MONTH;
		int year = Calendar.YEAR;
		for (int i = 0; i < rep.size(); i++) {

			if (date < inicio) {
				if (month == 1) {
					year--;
					month = 12;
				} else {
					month--;
				}
				d = new Date(inicio, month, year);
			} else {

				d = new Date(inicio, month, year);
			}
			if (rep.get(i).getDate().after(d)) {
				rep1.add(rep.get(i));
			}

		}
		Double l = getValue(rep1);
		return l;

	}

	private Double getValue(List<Expense> rep1) {
		Double l = 0.0;
		for (int i = 0; i < rep1.size(); i++) {
			l = l + rep1.get(i).getValue();
		}
		return l;
	}

}
