/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.AggregateRoot;
import eapli.framework.patterns.DomainEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Paulo
 */
@Entity
public class Budget implements Serializable, AggregateRoot<Long>, DomainEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	//Month range: 1 - 12
	private int month;
	//Year range: 1 - 12
	private int year;

	private boolean active = true; //true active / false inactive

	@OneToMany(cascade = CascadeType.ALL)
	private List<Entry> entries;

	public Budget() {
		this.entries = new ArrayList<>();
	}

	public Budget(int mes, int ano) {
		this.month = mes;
		this.year = ano;
		this.entries = new ArrayList<>();
	}

	public Budget(int mes, int ano, List<Entry> entries) {
		this.month = mes;
		this.year = ano;
		this.entries = entries;
	}

	public Budget(Budget bud) {
		this.month = bud.month;
		this.year = bud.year;
		this.active = bud.active;
		this.entries = new ArrayList<>(bud.entries);
	}

	public List<Entry> getEntries() {
		return this.entries;
	}

	public void setEntries(List<Entry> entries) {
		this.entries = entries;
	}

	public int getMonth() {
		return this.month;
	}

	public int getYear() {
		return this.year;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void cancelBudget(boolean val) {
		this.active = val;
	}

	public boolean getState() {
		return this.active;
	}

	public boolean wasMadeLastMonth() {

		Calendar c = Calendar.getInstance();
		int presentMonth = c.get(Calendar.MONTH);

		return presentMonth == (month - 1);
	}

	public Entry newExpenseTypeEntry(ExpenseType expType, BigDecimal value) {
		return new ExpenseTypeEntry(expType, value);
	}

	public boolean addEntry(Entry entry) {

		if (!this.entries.contains(entry)) {
			return this.entries.add(entry);
		} else {
			return false;
		}
	}

	@Override
	public Long id() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ID: " + id;
	}

//	public String info() {
//		return "Month: " + month + "\n Year: " + year + "\nState: " + active;
//	}
}
