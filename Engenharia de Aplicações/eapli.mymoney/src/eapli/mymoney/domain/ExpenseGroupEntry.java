/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.math.BigDecimal;
import javax.persistence.Entity;

/**
 *
 * @author Paulo
 */
@Entity
public class ExpenseGroupEntry extends Entry {

	private ExpenseGroup expGroup;

	protected ExpenseGroupEntry() {

	}

	public ExpenseGroupEntry(ExpenseGroup expGroup, BigDecimal value) {
		super(value);
		this.expGroup = expGroup;
	}

	@Override
	public String toString() {
		return "Expense Group\n" + expGroup.toString() + "\nvalue = " + this.
			getValue().intValue() + "\n";
	}
        public ExpenseGroup getExpenseGroup(){
            return this.expGroup;
        }

}
