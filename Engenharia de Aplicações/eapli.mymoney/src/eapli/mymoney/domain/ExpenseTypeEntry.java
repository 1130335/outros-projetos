/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.math.BigDecimal;
import javax.persistence.Entity;

/**
 *
 * @author Paulo
 */
@Entity
public class ExpenseTypeEntry extends Entry {

	private ExpenseType expType;

	protected ExpenseTypeEntry() {

	}

	public ExpenseTypeEntry(ExpenseType expType, BigDecimal value) {
		super(value);
		this.expType = expType;
	}
        public ExpenseType getExpenseType()
        {
            return this.expType;
        }
}
