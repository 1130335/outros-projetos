/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.ExpenseGroupEntry;
import eapli.mymoney.domain.ExpenseTypeEntry;
import eapli.mymoney.persistence.Persistence;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.List;

/**
 *
 * @author Aria
 */
public class BudgetXMLController {

public List<Budget> getListBudgets() {

		return Persistence.getRepositoryFactory().getBudgetRepository().all();

	}

	public void ExportBudget(Budget b) throws FileNotFoundException {
		Formatter out = new Formatter("C:\\Users\\Aria\\Documents\\NetBeansProjects\\eapli-2015-2db\\Budget.xml");
		out.format("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		out.format("\n <Budget ID=" + b.id() + ">");
		out.format("\n  <Month>" + b.getMonth() + "</Month>");
		out.format("\n  <Year>" + b.getYear() + "</Year>");
		out.format("\n  <Entries>");
		for (int i = 0; i < b.getEntries().size(); i++) {
			out.format("\n   <Entry ID=" + b.getEntries().get(i).id() + ">");
            if (b.getEntries().get(i).getClass().getSimpleName().equals("ExpenseTypeEntry")){
                out.format("\n    <ExpenseType>"+((ExpenseTypeEntry)b.getEntries().get(i)).getExpenseType()+"</ExpenseType>");
                        }
                        if (b.getEntries().get(i).getClass().getSimpleName().equals("ExpenseGroupEntry")){
                out.format("\n    <ExpenseGroup>"+((ExpenseGroupEntry)b.getEntries().get(i)).getExpenseGroup()+"</ExpenseGroup>");
                        }
			out.format("\n    <value>" + b.getEntries().get(i).getValue() + "</Value>");
			out.format("\n   </Entry>");
		}
		out.format("\n  </Entries>");
		out.format("\n </Budget>");
                out.close();
	}

}