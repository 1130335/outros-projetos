/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Pedro&Ricardo
 */
public class CancelBudgetController {

	/**
	 * Method that returns all budgets
	 *
	 * @return allBudgets list of all budgets
	 */
	public List<Budget> getAll() {
		List<Budget> allBudgets = Persistence.getRepositoryFactory().
			getBudgetRepository().all();
		return allBudgets;
	}

	/**
	 * Method that changes the Budget State
	 *
	 * @param sb Budget to cancel
	 */
	public void cancelBudget(Budget sb) {
		BudgetRepository repo = Persistence.getRepositoryFactory().
			getBudgetRepository();
		repo.cancelBudget(sb);
	}
}
