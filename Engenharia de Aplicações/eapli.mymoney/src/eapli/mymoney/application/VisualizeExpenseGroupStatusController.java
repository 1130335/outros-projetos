package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.jpa.ExpenseRepositoryImpl;
import java.util.ArrayList;
import java.util.List;

public class VisualizeExpenseGroupStatusController {

	public VisualizeExpenseGroupStatusController() {

	}

	public List<Expense> processExpenses(ExpenseGroup eg) {
		List<Expense> le = new ExpenseRepositoryImpl().all();
		List<Expense> le_g = new ArrayList<>();
		for (Expense e : le) {
//			if ((e.getGroup()).equals(eg)) {
//				le_g.add(e);
//			}
			le_g.add(e);
		}
		return le_g;
	}

	public double calculateValue(List<Expense> le) {
		double value = 0;
		for (Expense e : le) {
			value = value + e.getValue();
		}
		return value;
	}

}
