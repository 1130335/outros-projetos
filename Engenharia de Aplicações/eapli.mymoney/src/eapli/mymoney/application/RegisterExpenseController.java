/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.Payment.PaymentMethod;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Date;

/**
 *
 * @author Paulo Silva
 * @author Ivo Teixeira
 * @author Paulo Andrade
 */
public class RegisterExpenseController {

	public RegisterExpenseController() {

	}

	public void ExpenseRegister(String c, double t, Date d, ExpenseType et,
								ExpenseGroup eg, PaymentMethod pm) {
		Expense expense = new Expense(c, t, d, et, eg, pm);
		ExpenseRepository repo = Persistence.getRepositoryFactory().
			getExpenseRepository();

		repo.add(expense);

	}

}
