/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class RemoveBudgetController {

	private Budget sb;

	public List<Budget> getAllBudgets() {
		List<Budget> Budgetlist = Persistence.getRepositoryFactory().
			getBudgetRepository().all();
		return Budgetlist;
	}

	public void removeB(Budget sb) {
		BudgetRepository tmp_bud = Persistence.getRepositoryFactory().
			getBudgetRepository();
		tmp_bud.remove(sb);
	}
}
