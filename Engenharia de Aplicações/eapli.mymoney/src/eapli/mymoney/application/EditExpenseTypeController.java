/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author i130303
 */
public class EditExpenseTypeController {
    
    private ExpenseType et;
    private String newDescription;
    
    /**
     * All expense types available
     * @return List of all expense types available
    */
    public List<ExpenseType> getAllExpenseTypes()
    {
        ExpenseTypeRepository repo = Persistence.getRepositoryFactory().
                getExpenseTypeRepository();
        return repo.all();
    }

    /**
     * Defines the expense type to edit
     * @param et Expense type object
    */
    public void setExpenseType(ExpenseType et)
    {
        this.et = et;
    }
    
    /**
     * Set new description (validates)
     * @param d New description
     * @return True if new description is valid
     */
    public boolean setDescription(String d)
    {
        if(validate(d))
        {
            newDescription = d;
            return true;
        } else
        {
            System.out.println("Expense type already exists!");
            return false;
        }
    }
    
    /*
    * Validates new description
    * @param d Validates if the string is empty or already exists a expense type with that description
    * @return True if the string is valid (no empty and don't exists)
    */
    private boolean validate(String d)
    {
        return (!d.isEmpty() && !exists(d));
    }
    
    /*
    * Validates if a already exists a expense type with that description (s)
    * @param s Description
    * @return  True if already exists a expense type with that description
    */
    private boolean exists(String s)
    {
        for(ExpenseType etp: getAllExpenseTypes())
        {
            if(etp.description().equals(s))
                return true;
        }
        return false;
    }

    /**
     * Edits the expense type description
     * @return True if the expense type was edited on the repo
     */
    public boolean editExpenseType()
    {
        ExpenseTypeRepository repo = Persistence.getRepositoryFactory().
                getExpenseTypeRepository();
        
        return repo.updateExpenseType(et, newDescription);
    }
}
