/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.DeleteExpenseTypeValidation;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Ricardo Leite 1130522 & Pedro Tavares 1130486
 */
public class DeleteExpenseTypeController {

	private ExpenseTypeRepository ExpenseTypeRepository;
	private ExpenseType expenseTypeToRemove;

	// demo purposes
	public Iterator<ExpenseType> iterator() {
		ExpenseTypeRepository = Persistence.getRepositoryFactory().
			getExpenseTypeRepository();
		// using a pagesize=1 for DEBUG ONLY
		return ExpenseTypeRepository.iterator(1);
	}

	public void expenseTypeToDelete(int option) {

		int aux = option - 1;
		List<ExpenseType> list = ExpenseTypeRepository.all();

		expenseTypeToRemove = list.get(aux);
	}

	public String getExpenseTypeToRemoveName() {

		return expenseTypeToRemove.toString();
	}

	/**
	 * Method to validate if it is possible to remove de expense type
	 *
	 * @return true, if possible
	 */
	public boolean validate() {

		DeleteExpenseTypeValidation validation = new DeleteExpenseTypeValidation();
		boolean result = validation.validate(expenseTypeToRemove);

		return result;
	}
}
