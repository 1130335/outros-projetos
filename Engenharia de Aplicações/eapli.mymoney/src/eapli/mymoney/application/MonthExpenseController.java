/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.Persistence;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Daniela Maia
 */
public class MonthExpenseController {

	public double getThisMonthExpenses() {
        
		return Persistence.getRepositoryFactory().
			getExpenseRepository().getExpenseMonthValue();

	}
        
        public List<Double> getMonthsExpenses(int year) {

		return Persistence.getRepositoryFactory().
			getExpenseRepository().getExpenseMonthsValues(year);

	}
        
        public Map<String,Double> getMonthsExpensesByType(int month,List<ExpenseType> listExpensesType) {
            
                return Persistence.getRepositoryFactory().
			getExpenseRepository().getMonthsExpensesByType(month,listExpensesType);

	}
        
        

}
