/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Date;

/**
 *
 * @author Utilizador
 */
public class CreateExpenseGroupController {

	private ExpenseGroup eg;

	public CreateExpenseGroupController() {

	}

	public void createGroup(String name, Date beginD, Date endD) {
		eg = new ExpenseGroup();
		if (eg.validate(name, beginD, endD)) {
			eg.setData(name, beginD, endD);
		}
	}

	public void setOptionalData(float value) {
		if (eg.validateOptionalData(value)) {

			eg.setOptionalData(value);
		}
	}

	public void add() {
		ExpenseGroupRepository egr = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		egr.add(eg);
	}

}
