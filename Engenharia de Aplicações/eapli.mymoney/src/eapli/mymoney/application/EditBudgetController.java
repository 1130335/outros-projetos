/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rafael
 */
public class EditBudgetController {

	private Budget bud;
	private List<Entry> budgetEntries = new ArrayList<>();
	private Entry entry;
	private BigDecimal value;

	public List<Budget> getAllBudgets() {
		BudgetRepository repo = Persistence.getRepositoryFactory().
			getBudgetRepository();
		return repo.all();
	}

	public void setBudget(Budget b) {
		bud = b;
		budgetEntries.addAll(b.getEntries());
		//budgetEntries = b.getEntries();
	}

	public Budget getBudget() {
		return bud;
	}

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

	public boolean setValue(double value) {
		for (Entry data1 : budgetEntries) {
			if (data1.equals(entry)) {
				BigDecimal GD = new BigDecimal(value);
				data1.setValue(GD);
				return true;
			}
		}

		return false;
	}

	public List<ExpenseType> getExpenseTypeList() {

		final ExpenseTypeRepository repo = Persistence.getRepositoryFactory().
			getExpenseTypeRepository();
		return repo.all();

	}

	public void newEntry(ExpenseType expType, BigDecimal value) throws IllegalArgumentException {
		if (expType == null || value == null) {
			throw new IllegalArgumentException();
		}
		Entry new_entry = this.bud.newExpenseTypeEntry(expType, value);
		budgetEntries.add(new_entry);
	}

	public void addEntry() throws IllegalArgumentException {
		for (Entry e : budgetEntries) {
			if (e.equals(entry)) {
				e.setValue(value);
			}
		}
	}

	public void setValue(int valor) {
		BigDecimal value = new BigDecimal(valor);
		this.value = value;
	}

	//é aqui que acontece a magia supostamente a variavel bud ainda nao tem as entries la dentro,
	//envio para meter o resto é fazer trace ao código
	public boolean editBudget() {
		BudgetRepository repo = Persistence.getRepositoryFactory().
			getBudgetRepository();

		return repo.update(bud, budgetEntries);
	}
}
