/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class SetExpenseGroupController {

	ExpenseGroup expGroup;
	ExpenseGroup expGroupClone;
	ExpenseGroupRepository expGroupRepo;

	public SetExpenseGroupController() {
		expGroupClone = new ExpenseGroup();
	}

	public List<ExpenseGroup> getExpenseGroupList() {
		ExpenseGroupRepository repoGroup = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		return repoGroup.all();
	}

	public List<ExpenseGroup> getExpenseGroupList(Date date) {
		ExpenseGroupRepository repoGroup = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		return repoGroup.getActiveExpenseGroup(date);
	}

	public void setExpenseGroup(ExpenseGroup expenseGroup) {
		this.expGroup = expenseGroup;
	}

	public String getExpenseGroupData() {
		return expGroup.toString();
	}

	public ExpenseGroup getExpenseGroup() {

		return this.expGroup;
	}

	public ExpenseGroup getExpenseGroupClone() {

		return this.expGroupClone;
	}

	public void setData(String name, Date iniDate, Date endDate, float value) {
		this.expGroupClone.setData(name, iniDate, endDate);
		expGroupClone.setOptionalData(value);

	}

	public boolean confirm() {
		this.expGroupRepo.updateExpenseGroup(expGroup, expGroupClone);
		return false;
	}

}
