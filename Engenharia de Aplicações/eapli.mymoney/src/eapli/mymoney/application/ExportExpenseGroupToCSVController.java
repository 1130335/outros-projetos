/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.strategy.ExportExpenses;
import eapli.mymoney.domain.ExportExpensesToCSV;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author Rita
 */
public class ExportExpenseGroupToCSVController {

	public ExportExpenseGroupToCSVController() {

	}

	public void export(List<Expense> lexpense) throws FileNotFoundException {
		ExportExpenses ee = new ExportExpensesToCSV();
		((ExportExpensesToCSV) ee).setList(lexpense);
		ee.export();
		System.out.println("CSV file was created successfully!\n");
	}
}
