/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.DefaultBudgetMechanismRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class DefaultBudgetController {

	private DefaultBudgetMechanismInterface dbMechanism;
	private Budget budget;
	private RepositoryFactory repoFactory;
	private DefaultBudgetMechanismRepository dbmRepo;
	private DefaultBudgetMechanismInterface dbMec;
	private List<DefaultBudgetMechanismInterface> listMec;

	/* Default constructor. */
	public DefaultBudgetController() {
	}

	/* Method that returns the list of existing mechanisms. */
	public List<DefaultBudgetMechanismInterface> getListMechanisms() {
		repoFactory = Persistence.getRepositoryFactory();
		dbmRepo = repoFactory.getDefaultBudgetRepository();
		try {
			listMec = dbmRepo.all();
		} catch (Exception e) {
		}

		return listMec;
	}

	/* Method that creates a budget. As parameter receives a mechanism.*/
	public Budget createBudget(DefaultBudgetMechanismInterface mec) {
		RepositoryFactory rf = Persistence.getRepositoryFactory();
		BudgetRepository br = rf.getBudgetRepository();
		return mec.createBudget(br);
	}
}
