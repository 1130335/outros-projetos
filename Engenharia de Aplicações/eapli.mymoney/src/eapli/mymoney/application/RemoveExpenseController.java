/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;


/**
 *
 * @author Paulo Silva
 */
public class RemoveExpenseController {

	public RemoveExpenseController() {

	}
        
        public List<Expense> ListExpenses() {
		
		List<Expense> list=Persistence.getRepositoryFactory().
			getExpenseRepository().all();
		
               return list;
	}

	public void ExpenseRemover(int index) {
                ExpenseRepository repo = Persistence.getRepositoryFactory().
			getExpenseRepository();
            
		Expense expense = repo.all().get(index);
                
               repo.delete(expense);
	}

}
