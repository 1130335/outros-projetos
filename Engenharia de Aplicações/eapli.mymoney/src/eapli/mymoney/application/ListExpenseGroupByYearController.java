/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.*;
import eapli.mymoney.persistence.*;
import java.util.*;

/**
 *
 * @author Francisco Lopes 1130576
 */
public class ListExpenseGroupByYearController {
    
    private int year;
    
    /**
     * Returns all expense groups available
     * @return List of the expense groups available
     */
    public List<ExpenseGroup> getAllExpenseGroups() {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().getExpenseGroupRepository();
		return repo.all();
    }
    
    /**
     * Set end year
     * @param year 
     */
    public void setEndYear(int year){
        this.year = year;
    }
    
    /**
     * Returns the list of expense groups with the end year matching the defined one
     * @return List of expense group filtered by the end year
     */
    public List<ExpenseGroup> getExpenseGroupsPerYear(){
        List<ExpenseGroup> ExpenseGroupByYearList = new ArrayList<ExpenseGroup>();
        for(ExpenseGroup eg : getAllExpenseGroups()){
            if(eg.getEndDate().getYear() == this.year){
                ExpenseGroupByYearList.add(eg);
            }
        }
        return ExpenseGroupByYearList;
    }
    
    /**
     * Validates the year 
     * @param year
     * @return True if it's a valid year
     */
    public boolean validateYear(int year){
        List<ExpenseGroup> ExpenseGroups = getAllExpenseGroups();
        for(ExpenseGroup eg : ExpenseGroups){
            if(eg.getEndDate().getYear() == year)
                return true;
        }
        return false;
    }
    
    /**
     * Allow to calculate the total real value of the expenses that belong in the expense group
     * @param eg
     * @return Total real value
     */
    public float getRealValueByExpenseGroup (ExpenseGroup eg){
        float cont = 0;
        ExpenseRepository repo = Persistence.getRepositoryFactory().getExpenseRepository();
        
        for(ExpenseType et : eg.getTypesList())
            for(Expense e: repo.all())
                if(e.getType().equals(et))
                    cont += e.getValue();
            
        return cont;
    }
}
