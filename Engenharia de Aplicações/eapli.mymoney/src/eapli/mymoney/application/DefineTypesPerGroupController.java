/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class DefineTypesPerGroupController {

	private ExpenseGroup expGroup;
	private ExpenseGroup expGroupAux;
	private ExpenseRepository expRepo;
	private ExpenseGroupRepository repoGroup;
	private ExpenseTypeRepository repoType;

	public DefineTypesPerGroupController() {
		expGroup = new ExpenseGroup();
		expGroupAux = new ExpenseGroup();
		expRepo = Persistence.getRepositoryFactory().
			getExpenseRepository();
		repoGroup = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		repoType = Persistence.getRepositoryFactory().
			getExpenseTypeRepository();
	}

	public List<ExpenseGroup> getExpenseGroupList() {

		return repoGroup.all();
	}

	public List<ExpenseType> getExpenseTypeList() {
		return repoType.all();
	}

	public void addExpenseType(ExpenseType xtype) {
		this.expGroupAux.addExpenseType(xtype);
	}

	public ExpenseGroup getExpenseGroupAux() {
		return this.expGroupAux;
	}

	public ExpenseGroup getExpenseGroup() {
		return this.expGroup;
	}

	public void setExpenseGroup(ExpenseGroup xgroup) {
		this.expGroup = xgroup;
		expGroupAux = expGroup.clone();

	}

	public boolean confirm() {

		List<Expense> expList = expRepo.all();
		List<ExpenseType> expTypeGroupList = this.expGroup.getTypesList();
		List<ExpenseType> expTypeGroupAuxList = this.expGroupAux.getTypesList();

		for (int i = 0; i < expTypeGroupList.size(); i++) {

			if (!expTypeGroupAuxList.contains(expTypeGroupList.get(i))) {

				for (int j = 0; j < expList.size(); j++) {
					if (expList.get(j).getType() == expTypeGroupList.get(i)) {
						return false;
					}
				}
				repoGroup.updateExpenseGroup(this.expGroup, this.expGroupAux);

			}
		}
		return true;
	}
}
