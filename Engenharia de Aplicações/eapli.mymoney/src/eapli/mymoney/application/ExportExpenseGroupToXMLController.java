/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.Persistence;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 *
 * @author rafael
 */
public class ExportExpenseGroupToXMLController {

	PrintWriter writer;
	private ExpenseGroup expenseGroup;

	public List<ExpenseGroup> getAllExpenseGroups() {
		final ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		return repo.all();
	}

	public void setGroup(ExpenseGroup expenseGroup) {
		this.expenseGroup = expenseGroup;
	}

	public void doExport() throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter(expenseGroup.getName() + ".xml", "UTF-8");
		List<ExpenseType> typesList = expenseGroup.getTypesList();
		writer.println("<ExpenseGroup>");
		writer.println("\t<id>" + expenseGroup.id() + "</id>");
		writer.println("\t<name>" + expenseGroup.getName() + "</name>");
		writer.println("\t<InitialDate>" + expenseGroup.getInitialDate().
			toString() + "</InitialDate>");
		writer.
			println("\t<EndDate>" + expenseGroup.getEndDate().toString() + "</EndDate>");
		writer.
			println("\t<TotalValue>" + expenseGroup.getTotalValue() + "</TotalValue>");
		writer.println("\t<sumVal>" + expenseGroup.getSumVal() + "</sumVal>");
		writer.println("\t<TypeList>");
		for (ExpenseType et : typesList) {
			writer.println("\t\t<ExpenseType>");
			writer.println("\t\t\t<id>" + et.id() + "</id>");
			writer.println("\t\t\t<name>" + et.description() + "</name>");
			writer.println("\t\t</ExpenseType>");
		}
		writer.println("\t</TypeList>");
		writer.println("</ExpenseGroup>");
		writer.close();
	}

	public ExpenseGroup getGroup() {
		return this.expenseGroup;
	}
}
