/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Entry;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.strategy.ExportProjectStatus;
import eapli.mymoney.strategy.ExportProjectStatusFactory;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Daniela Maia
 */
public class ExportProjectStatusController {

	public boolean export(List<Entry> le, int month, int year,Map<String, Double> map) {
            ExportProjectStatusFactory fabrica =  ExportProjectStatusFactory.getInstance();
            ExportProjectStatus estrategia = fabrica.getEstrategia();
            estrategia.export(le, month,  year, map);

		return true;

	}

}
