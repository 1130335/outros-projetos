/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.persistence.Persistence;

/**
 *
 * @author Daniela Maia
 */
public class WeeksExpenseController {

	public Double getThisWeeksExpenses() {

		return Persistence.getRepositoryFactory().
			getExpenseRepository().getExpenseWeekValue();

	}

}
