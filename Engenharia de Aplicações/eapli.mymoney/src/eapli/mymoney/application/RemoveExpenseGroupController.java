/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author i120311
 */
public class RemoveExpenseGroupController {
    public List<ExpenseGroup> getAllExpenseGroups() {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		return repo.all();
	}
    
     public boolean hasExpenses(ExpenseGroup eg) {
         ExpenseRepository repo = Persistence.getRepositoryFactory().getExpenseRepository();
		       for (Expense exp : repo.all()) {
                           //if(eg.equals(exp.getGroup()){
                               return true;
                           //}
                       }
		return false;
	}
     
     public boolean deleteExpenseGroup(ExpenseGroup eg) {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
                if(hasExpenses(eg)){
                    return false;
                }
		repo.remove(eg);
                return true;
	}

// demo purposes
    public Iterator<ExpenseGroup> iterator() {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		// using a pagesize=1 for DEBUG ONLY
		return repo.iterator(1);
	}
}
