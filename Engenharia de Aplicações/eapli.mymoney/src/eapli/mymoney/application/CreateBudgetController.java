/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.BudgetRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author IvoTeixeira
 */
public class CreateBudgetController {

	Budget newBudget;

	public CreateBudgetController() {
		newBudget = new Budget();
	}

	public void setBudgetValues(int month, int year) {
		newBudget.setMonth(month);
		newBudget.setYear(year);
	}

	/**
	 * metodo que obtem todos os tipos de despesa através do repositorio de
	 * despesas
	 *
	 * @return lista dos tipos de despesa
	 */
	public List<ExpenseType> getExpenseTypeList() {

		final ExpenseTypeRepository repo = Persistence.getRepositoryFactory().
			getExpenseTypeRepository();
		return repo.all();

	}

	public boolean anyBudgetIn(int month, int year) {

		final BudgetRepository repo = Persistence.getRepositoryFactory().
			getBudgetRepository();
		return repo.anyBudgetIn(month, year);

	}

	public void newEntry(ExpenseType expType, BigDecimal value) throws IllegalArgumentException {
		if (expType == null || value == null) {
			throw new IllegalArgumentException();
		}
		Entry new_entry = this.newBudget.newExpenseTypeEntry(expType, value);
		this.newBudget.addEntry(new_entry);
	}

	public void persist() {

		BudgetRepository b = Persistence.getRepositoryFactory().
			getBudgetRepository();
		b.add(newBudget);

	}

}
