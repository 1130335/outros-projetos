/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.Persistence;
import java.util.List;
import java.util.Map;

/**
 *
 * @author André Garrido<1101598>
 */
public class ControlBudgetReportController {

	public List<Budget> getAllBudgets() {

		List<Budget> Budgetlist = Persistence.getRepositoryFactory().
			getBudgetRepository().all();
		return Budgetlist;
	}

	public List<ExpenseType> getAllExpenseTypes() {

		List<ExpenseType> ExpenseTypelist = Persistence.getRepositoryFactory().
			getExpenseTypeRepository().all();
		return ExpenseTypelist;
	}

	public Map<String, Double> getMonthsExpensesByType(int month,
													   List<ExpenseType> listExpensesType) {

		return Persistence.getRepositoryFactory().
			getExpenseRepository().
			getMonthsExpensesByType(month, listExpensesType);
	}
}
