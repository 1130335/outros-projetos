/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author rafael
 */
public class CancelExpenseGroupController {

	private ExpenseGroup expenseGroup;

	public List<ExpenseGroup> getAllExpenseGroups() {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		return repo.all();
	}

	public ExpenseGroup getExpenseGroup() {
		return expenseGroup;
	}

	public void setExpenseGroup(ExpenseGroup expenseGroup) {
		this.expenseGroup = expenseGroup;
	}

	public boolean cancel() {
		ExpenseGroupRepository repo = Persistence.getRepositoryFactory().
			getExpenseGroupRepository();
		repo.cancelExpenseGroup(expenseGroup);
		//CRIAR OS METODOS EM:
		//BudgetRepositoryImpl inMemory e JPA
		return true;
	}
}
