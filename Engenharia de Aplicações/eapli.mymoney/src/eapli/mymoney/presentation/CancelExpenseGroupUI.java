/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.CancelExpenseGroupController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author rafael
 */
public class CancelExpenseGroupUI {

	CancelExpenseGroupController controller = new CancelExpenseGroupController();

	public boolean doShow() {
		List<ExpenseGroup> ExpenseGroupList = controller.getAllExpenseGroups();
		if (ExpenseGroupList.isEmpty()) {
			System.out.
				println("Error! You have to create an expense group first!\n");
			return false;
		}
		int n;
		boolean flag = false;
		while (!flag) {
			n = 0;
			for (ExpenseGroup b : ExpenseGroupList) {
				if (b.getState()) {
					System.out.
						println(n + " - " + b.toString() + " " + b.getState());
					n++;
				} else {
					ExpenseGroupList.remove(b);
				}
			}
			flag = select(ExpenseGroupList);
			if (!flag) {
				System.out.println("Invalid number!");
			}
		}
		if (controller.cancel()) {
			System.out.println("Expense Group canceled sucessfully!");
			return true;
		}
		return false;
	}

	private boolean select(List<ExpenseGroup> all) {
		int tmp;
		tmp = Console.readInteger("Enter number:");

		if (tmp >= 0 && tmp < all.size()) {
			controller.setExpenseGroup(all.get(tmp));
			return true;
		}
		return false;
	}
}
