package eapli.mymoney.presentation;

import eapli.mymoney.application.ListExpenseTypesController;
import eapli.mymoney.application.RegisterExpenseController;
import eapli.mymoney.application.SetExpenseGroupController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.Payment.PaymentMethod;
import eapli.util.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Rita
 * @author Ivo Teixeira
 * @author Paulo Andrade
 */
public class RegisterExpenseUI extends BaseUI {

	private RegisterExpenseController rec = new RegisterExpenseController();
	ExpenseType expenseType;
	ExpenseGroup expenseGroup;

	public RegisterExpenseUI() {

	}

	@Override
	protected boolean doShow() {
		String comment;
		Date date = new Date();
		String aux_date;
		double total = 0;
		comment = Console.readLine("Enter expense name or comment » ");
		aux_date = Console.
			readLine("Enter the date of the expense (dd-mm-yyyy) » ");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = formatter.parse(aux_date);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		total = Console.readDouble("Enter the total of the expense » ");

		final List<ExpenseType> expenseTypes = new ListExpenseTypesController().
			getAllExpenseTypes();

		if (expenseTypes.isEmpty()) {
			System.out.println("There is no expense Types yet!");
			return false;
		}
		System.out.println("\nExpenses Types: ");
		int i = 1;
		for (ExpenseType et : expenseTypes) {
			if (et.getFlag() == true) {
				System.out.println(i + " - " + et.description());
			}
			i++;
		}
		int option;
		do {
			option = Console.
				readInteger("Enter the number of the expense type you desired » ");
		} while (option < 1 || option > i - 1);
		expenseType = expenseTypes.get(option - 1);

		final List<ExpenseGroup> expenseGroups = new SetExpenseGroupController().
			getExpenseGroupList(date);
		String ans;
		if (expenseGroups.isEmpty()) {
			System.out.println("There is no Expense Groups available!");
			return false;

		}
		System.out.println("\nExpenses groups:");

		i = 1;
		for (ExpenseGroup eg : expenseGroups) {

			System.out.println(i + " - " + eg.getName());

			i++;
		}
		int option_group;
		do {
			option_group = Console.
				readInteger("Enter the number of the expense group you desired » ");
		} while (option_group < 1 || option_group > i - 1);
		expenseGroup = expenseGroups.get(option_group - 1);

		System.out.println("\nPlease select a payment method:");
		int j = 1;
		for (PaymentMethod tp : PaymentMethod.values()) {
			System.out.println(j + " - " + tp);
			j++;
		}
		int k;
		do {
			k = Console.
				readInteger("Enter the number of the payment method you desired » ");

		} while (k < 1 || k > j - 1);
		PaymentMethod p = PaymentMethod.values()[k - 1];
		expenseGroup.addExpenseType(expenseType);
		rec.ExpenseRegister(comment, total, date, expenseType, expenseGroup, p);

		showExpenseData();
		return true;

	}

	public void showExpenseData() {
		System.out.println("\nExpense recorded!");
	}

	@Override
	public String headline() {
		return "REGISTER AN EXPENSE";
	}
}
