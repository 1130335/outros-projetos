/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.CreateBudgetController;
import eapli.mymoney.domain.ExpenseType;
import eapli.util.Console;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author IvoTeixeira
 */
public class RegisterBudgetUI extends BaseUI {

	private CreateBudgetController controller = new CreateBudgetController();

	/**
	 * This method shows a error message and returns false if the list of
	 * Expenses Types is empty and if not when all the entrys are added it shows
	 * a clear message of well-succedded and returns true
	 *
	 * @return true or false
	 */
	@Override
	protected boolean doShow() {

		boolean flag = false;
		int mes;
		int ano;
		do {
			do {
				mes = Console.readInteger("Insert the budget's month");
			} while (mes < 1 || mes > 12);

			do {
				ano = Console.readInteger("Insert the budget's year");
			} while (ano < 0 || ano > 9999);

			flag = this.controller.anyBudgetIn(mes, ano);

			if (flag) {
				System.out.
					println("There is a budget already created for month " + mes + " and year " + ano);
			}
		} while (flag);

		controller.setBudgetValues(mes, ano);

		List< ExpenseType> all = controller.getExpenseTypeList();

		if (all.isEmpty()) {
			System.out.
				println("There is no Expenses Types yet! Please define Expenses Types first!");

			return false;
		}
		showList(all);

		System.out.
			println("You have defined the values for the Expenses Types!");

		controller.persist();
		return true;
	}

	/**
	 *
	 * This method shows the list of Expenses Types and the user selects a value
	 * for each. It also adds a new entry with the expense type and the value.
	 *
	 * @param all
	 * @return true or false
	 */
	protected boolean showList(List<ExpenseType> all) {

		int option;
		double value;

		int i = 1;

		System.out.println("Expenses Types List: ");
		for (ExpenseType et : all) {

			System.out.println(i + " - " + et.description());
			i++;
		}

		do {
			option = Console.readInteger("Please select an option:");
		} while (option < 1 || option > all.size());
		value = Console.
			readDouble("Please select the value of that Expense Type: ");
		controller.newEntry(all.get(option - 1), new BigDecimal(value));
		System.out.println();
		System.out.
			println("It was added a new entry with the following information:\n");
		System.out.print("Expense Type: ");
		System.out.println(all.get(option - 1));
		System.out.print("Value: ");
		System.out.print(value);
		System.out.println("\n");

		all.remove(option - 1);

		if (all.isEmpty()) {
			return true;
		} else {
			String ans;
			do {

				ans = Console.
					readLine("Do you want to define the value for a new Expense type? (yes/no)");
				if ("yes".equalsIgnoreCase(ans)) {
					showList(all);
				}

			} while (!("yes".equalsIgnoreCase(ans)) && !("no".
				equalsIgnoreCase(ans)));

			return true;
		}
	}

	@Override
	public String headline() {

		return "CREATE BUDGET";
	}

}
