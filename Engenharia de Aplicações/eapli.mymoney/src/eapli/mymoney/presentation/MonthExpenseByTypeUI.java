/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.MonthExpenseController;
import eapli.mymoney.application.ListExpenseTypesController;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseType;
import eapli.util.Console;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthExpenseByTypeUI extends BaseUI {

    private final MonthExpenseController theController = new MonthExpenseController();
    private final ListExpenseTypesController theControllerTypes = new ListExpenseTypesController();

    @Override
    protected boolean doShow() { 
        
       
        int month = Console.readInteger("Insert the month >> ");
        if(month>0 && month<13){
        List<ExpenseType> listExpensesType=theControllerTypes.getAllExpenseTypes();
        
        Map<String,Double> map= theController.getMonthsExpensesByType( month,listExpensesType);
        System.out.println("Month: "+ month);
        for (Map.Entry<String,Double> entry : map.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            System.out.println("Expense Type " + key+": "+value);
        }
        }else{
            System.out.println("Invalid Month");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Expense of a Month by Type:";
    }
}
