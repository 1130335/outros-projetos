/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.ControlBudgetReportController;
import eapli.mymoney.application.ExportProjectStatusController;
import eapli.mymoney.application.MonthExpenseController;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.util.Console;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Daniel
 */
public class ExportProjectStatusUI extends BaseUI{
        private ControlBudgetReportController controller = new ControlBudgetReportController();
	private final MonthExpenseController controller_monthExpense = new MonthExpenseController();
	private final ExportProjectStatusController controller_exp = new ExportProjectStatusController();

	

    @Override
    protected boolean doShow() {
        //show all and select Budgets
	showAll();
	//removes and inform of sucess
	report();
	return true;
        
    }
    
    private void showAll() {
		List<Budget> all = controller.getAllBudgets();

		if (all.isEmpty()) {
			System.out.println("No Budgets to Show!");
			System.exit(0);
		} else {
			int n = 0;
			System.out.println("Budgets");
			for (Budget sb : all) {
				System.out.println("Budget " + n + " id= " + sb.id());
				n++;
			}
		}
		//System.out.println("Invalid Selection!");

	}
    
    
    public void report() {

		final List<Budget> allbudgets = controller.getAllBudgets();
		final List<ExpenseType> allExpenseTypes = controller.
			getAllExpenseTypes();
		int tmp;
		
		tmp = Console.readInteger("Choose a Budget:");

		Budget sb = allbudgets.get(tmp);

		/* Codigo para calculo da estimativa gasta ate ao final do mes*/
		int month = sb.getMonth();  //get the month of the selected budget
		int year = sb.getYear();



		/* ----------------------------------------------------------------------*/
		//get months expenses by ExpenseType
		Map<String, Double> map = controller_monthExpense.
			getMonthsExpensesByType(month, allExpenseTypes);

		List<Entry> le = sb.getEntries();
                controller_exp.export( le, month,  year, map);
                
    }
    @Override
    public String headline() {
        return "List Expense Groups";
    }
}
