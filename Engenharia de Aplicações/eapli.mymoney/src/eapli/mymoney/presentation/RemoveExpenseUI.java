/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.RemoveExpenseController;
import eapli.mymoney.domain.Expense;
import eapli.util.Console;
import java.util.List;
import java.util.ArrayList;


/**
 *
 * @author Paulo Gandra Sousa
 */
public class RemoveExpenseUI extends BaseUI {

    private final RemoveExpenseController theController = new RemoveExpenseController();

    @Override
    protected boolean doShow() {
        
        List<Expense> list = new ArrayList<Expense>();
        list=theController.ListExpenses();
        int cont=0;
        int index;
        for (Expense list1 : list) {
            cont++;
            System.out.println("Expense "+cont+": "+list1);
            
        }
        if(!list.isEmpty()){
        System.out.println("Cancel Expense elimination >> 0");
        index = Console.readInteger("Choose the expense number to remove >> ");
        if(index!=0){
        if(index>0 && index<=list.size()){
            theController.ExpenseRemover(index-1);
            System.out.println("Expense removed");
        }else{
            System.out.println("Choose a valid Expense");
        }
        }
        }else{
            System.out.println("Your expenses list is empty");
        
        }
        return true;
    }

    @Override
    public String headline() {
        return "Expense of current month:";
    }
}
