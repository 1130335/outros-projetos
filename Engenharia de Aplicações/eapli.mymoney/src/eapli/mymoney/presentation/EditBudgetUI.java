/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.EditBudgetController;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.ExpenseTypeEntry;
import eapli.util.Console;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author rafael
 */
public class EditBudgetUI extends BaseUI {

	EditBudgetController controller = new EditBudgetController();
	Scanner sc = new Scanner(System.in);
	private int ano;
	private int mes;

	public EditBudgetUI() {

	}

	@Override
	public boolean doShow() {

		if (!showAll()) {
			return false;
		}
		System.out.println("1. Add entry");
		System.out.println("2. Edit entry's value");
		int option;
		while (true) {
			option = Console.readInteger("Enter number:");
			if (option == 1 || option == 2) {
				break;
			}
			System.out.println("Impossible");
		}
//
		if (option == 1) {
			if (!addEntry()) {
				return false;
			}
		}
		if (option == 2) {
			if (!showAllEntries()) {
				return false;
			}
			controller.addEntry();
		}

		if (controller.editBudget()) {
			System.out.println("\nBudget edited!");
			return true;
		} else {
			System.out.println("\nBudget not edited!");
			return false;
		}
	}

	private boolean showAll() {
		List<Budget> budgets = controller.getAllBudgets();
		if (budgets.isEmpty()) {
			System.out.
				println("Error! You have to create a budget first!\n");
			return false;
		}
		System.out.println("1. January");
		System.out.println("2. February");
		System.out.println("3. March");
		System.out.println("4. April");
		System.out.println("5. May");
		System.out.println("6. June");
		System.out.println("7. July");
		System.out.println("8. August");
		System.out.println("9. September");
		System.out.println("10. October");
		System.out.println("11. November");
		System.out.println("12. December");
		while (!selectMonth()) {
			System.out.println("Invalid Month");
		}
		int n;
		boolean flag = false;
		List<Budget> shownBudgets = new ArrayList<Budget>();
		while (!flag) {
			n = 0;
			for (Budget b : budgets) {

				if (b.getMonth() == mes && b.getYear() == ano && b.getState()) {

					System.out.println(n + " - " + b.toString());
					shownBudgets.add(b);
				}
				n++;
			}
			if (shownBudgets.isEmpty()) {
				System.out.
					println("Error! You have to create a budget first!\n");
				return false;
			}
			flag = select(shownBudgets);
			if (!flag) {
				System.out.println("Invalid Budget!");
			}
		}
		return true;
	}

	private boolean selectMonth() {
		int mes = Console.readInteger("Enter number:");
		if (mes < 1 || mes > 12) {
			return false;
		}
		this.mes = mes;
		while (!selectYear()) {
			System.out.println("Invalid Year");
		}
		return true;
	}

	private boolean selectYear() {

		int ano = Console.readInteger("Enter year:");
		if (ano > 9999) {
			return false;
		}
		this.ano = ano;
		return true;
	}

	private boolean select(List<Budget> all) {
		int tmp;
		tmp = Console.readInteger("Enter number:");

		if (tmp >= 0 && tmp < all.size()) {
			controller.setBudget(all.get(tmp));
			return true;
		}
		return false;
	}

	private boolean showAllEntries() {
		List<Entry> entries = controller.getBudget().getEntries();
		if (entries.isEmpty()) {
			System.out.println("Error! You have to create an entry first!\n");
			return false;
		}
		int n;
		boolean flag = false;
		while (!flag) {
			n = 0;
			for (Entry b : entries) {
				if (b.getClass().getSimpleName().equals("ExpenseTypeEntry")) {
					ExpenseType expType = ((ExpenseTypeEntry) b).
						getExpenseType();
					System.out.
						println(n + " - " + expType.description() + " - " + b.
							getValue());
					n++;
				} else {
					entries.remove(b);
				}
			}
			flag = selectEntry(entries);
			if (!flag) {
				System.out.println("Invalid number!");
			}
		}
		controller.setValue(Console.readInteger("Enter new Value - "));
		return true;
	}

	private boolean selectEntry(List<Entry> entries) {
		int tmp;
		tmp = Console.readInteger("Enter number:");

		if (tmp >= 0 && tmp < entries.size()) {
			controller.setEntry(entries.get(tmp));
			return true;
		}
		return false;
	}

	private boolean addEntry() {
		List< ExpenseType> all = controller.getExpenseTypeList();

		if (all.isEmpty()) {
			System.out.
				println("There is no Expenses Types yet! Please define Expenses Types first!");

			return false;
		}
		showList(all);
		return true;
	}

	protected boolean showList(List<ExpenseType> all) {

		int option;
		double value;

		int i = 1;

		System.out.println("Expenses Types List: ");
		for (ExpenseType et : all) {

			System.out.println(i + " - " + et.description());
			i++;
		}

		do {
			option = Console.readInteger("Please select an option:");
		} while (option < 1 || option > all.size());
		value = Console.
			readDouble("Please select the value of that Expense Type: ");
		controller.newEntry(all.get(option - 1), new BigDecimal(value));
		System.out.println();
		System.out.
			println("It was added a new entry with the following information:\n");
		System.out.print("Expense Type: ");
		System.out.println(all.get(option - 1));
		System.out.print("Value: ");
		System.out.print(value);
		System.out.println("\n");

		all.remove(option - 1);

		if (all.isEmpty()) {
			return true;
		} else {
			String ans;
			do {

				ans = Console.
					readLine("Do you want to define the value for a new Expense type? (yes/no)");
				if ("yes".equalsIgnoreCase(ans)) {
					showList(all);
				}

			} while (!("yes".equalsIgnoreCase(ans)) && !("no".
				equalsIgnoreCase(ans)));

			return true;
		}
	}

	private void submit() {
		while (!controller.setValue(Console.readDouble(("Enter new value:"))));
	}

	@Override
	public String headline() {
		return "EDIT BUDGET";
	}
}
