/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.MonthExpenseController;
import eapli.mymoney.domain.Expense;
import eapli.util.Console;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthsExpensesUI extends BaseUI {

    private final MonthExpenseController theController = new MonthExpenseController();

    @Override
    protected boolean doShow() {
        int year = Console.readInteger("Insert the year >> ");
        if(year>0 && year<2015){
        List<Double> list = new ArrayList<Double>();
        list=theController.getMonthsExpenses(year);
        int cont=0;
        for (Double list1 : list) {
            cont++;
            System.out.println("Month"+cont+": "+list1);
        }
        
        }else{
            System.out.println("Invalid year");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Expense of current month:";
    }
}
