/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.DeleteExpenseTypeController;
import eapli.mymoney.domain.ExpenseType;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Ricardo Leite 1130522 & Pedro Tavares 1130486
 *
 */
public class DeleteExpenseTypeUI extends BaseUI {

	private final DeleteExpenseTypeController controller = new DeleteExpenseTypeController();

	/**
	 * Method to run the use case
	 */
	public void run() {
		boolean show;

		if (doShow()) {
			choose();
		} else {
			System.out.
				println("\nError! You have to create a new expense type first!\n");
		}

	}

	/**
	 * Method to choose the expense type to delete
	 *
	 * @return true if the operation was well succeeded
	 */
	public boolean choose() {
		String opt;
		int continua = 0;
		int range;
		Scanner read = new Scanner(System.in);

		while (continua == 0) {
			System.out.
				println("Which expense type do you wish to remove?\n Select the number:");

			opt = read.nextLine();

			if (opt.matches("[0-9]+")) {
				range = listRange();
				int option = Integer.valueOf(opt);

				if (option > 0 && option <= range) {

					controller.expenseTypeToDelete(option);
					continua++;
				} else {
					System.out.
						println("You can only choose between 1 and " + range);
				}

			} else {
				System.out.println("Please choose a number from the list!");

			}
		}

		continua = 0;

		System.out.
			println("Are you sure you want to remove this expense type? (Y/N)");
		while (continua == 0) {
			opt = read.nextLine();
			if (opt.matches("[Y|y|N|n]")) {

				if (opt.equals("n") || opt.equals("N")) {
					System.out.println("Operation canceled");
					break;
				} else {

					boolean value = controller.validate();

					if (value == true) {

						System.out.
							println("The expense type was sucessfuly removed!");
						break;
					} else {

						System.out.
							println("The expense type was blocked. \n It was impossible to remove due to existent expenses with " + controller.
								getExpenseTypeToRemoveName() + " type!"
							);
						break;
					}
				}

			} else {
				System.out.
					println("Please chose \"Y\" for Yes or \"N\" for No!");
			}

		}

		return false;
	}

	/**
	 * Method to return the number of available expense types
	 *
	 * @return number of expense types
	 */
	public int listRange() {
		final Iterator<ExpenseType> expenseTypes = controller.iterator();
		int x = 0;

		while (expenseTypes.hasNext()) {
			ExpenseType et = expenseTypes.next();
			x++;
		}

		return x;
	}

	@Override
	protected boolean doShow() {

		final Iterator<ExpenseType> expenseTypes = controller.iterator();
		int numero = 1;
		int cont = 0;
		while (expenseTypes.hasNext()) {
			ExpenseType et = expenseTypes.next();
			if (et.getFlag() == true) {
				System.out.println(numero + ": " + et.description());
				numero++;
				cont++;
			}

		}
		return cont > 0;
//        final List<ExpenseType> expenseTypes = controller.getAllExpenseTypes();
//        for (ExpenseType et : expenseTypes) {
//            System.out.println(et.description());
//        }
	}

	@Override
	public String headline() {
		return "Delete Expense Types";
	}

}
