/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.ExportExpenseGroupToCSVController;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.persistence.jpa.ExpenseRepositoryImpl;
import eapli.util.Console;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rita
 */
public class ExportExpenseGroupToCSVUI extends BaseUI {

	private ExportExpenseGroupToCSVController eegc = new ExportExpenseGroupToCSVController();

	public ExportExpenseGroupToCSVUI() {

	}

	@Override
	protected boolean doShow() {
		String expenseGroup = " ";
		boolean validate = false;
		while (validate == false) {
			expenseGroup = Console.readLine("Enter expense group name » ");
//			List<ExpenseGroup> egl = new ExpenseGroupRepositoryImpl().all();
//			for (ExpenseGroup eg : egl) {
//				if (eg.getName().equals(expenseGroup)) {
//					validate = true;
//				}
//			}
			validate = true;
		}

		List<Expense> expenseList = new ExpenseRepositoryImpl().all();
		List<Expense> exportExpenses = new ArrayList<>();
		for (Expense e : expenseList) {
			/* Alterar quando o registar despesa com grupo de despesa estiver completo */
			if (/* e.getGroup().equals(expenseGroup) */true) {
				exportExpenses.add(e);
			}
		}

		try {
			eegc.export(exportExpenses);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ExportExpenseGroupToCSVUI.class.getName()).
				log(Level.SEVERE, null, ex);
		}

		System.out.println("Expenses exported!\n");
		return true;
	}

	@Override
	public String headline() {
		return "EXPORT EXPENSE GROUP TO CSV";
	}

}
