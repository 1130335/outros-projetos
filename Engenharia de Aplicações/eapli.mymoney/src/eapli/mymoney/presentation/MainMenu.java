/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.util.Console;

/**
 * The application's main menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu {

	public void mainLoop() {
		showMainMenu();
	}

	private boolean showMainMenu() {
		int option = -1;
		while (option != 0) {
			System.out.println("=============================");
			System.out.println("  myMoney - EXPENSE MANAGER  ");
			System.out.println("=============================\n");
			System.out.println("--- master tables ---");
			System.out.println("100. Register an expense type");
			System.out.println("101. List expense types");
			System.out.println("102. Delete an expense type");
			System.out.println("103. Edit a expense type");
			System.out.println("104. Visualize current month expense");
			System.out.println("105. Register Budget");
			System.out.println("106. Register an expense");
			System.out.println("107. Export expense group to CSV");
			System.out.println("108. Export budget to XML");
			System.out.println("109. RemoveExpense");
			System.out.println("201. Edit Budget");
			System.out.println("202. Create Default Budget");
			System.out.println("203. Cancel Budget");
			System.out.println("205. Visualize expenses of month by Type");
			System.out.println("206. Visualize year expenses by month");
			System.out.println("210. Remove Budget");
			System.out.println("301. Define expense group");
			System.out.println("302. Set expense group");
			System.out.println("303. Cancel expense group");
			System.out.println("304. Export expense group to XML");
			System.out.println("305. List expense group by year");
			System.out.println("306. Define expense types per group");
			System.out.println("308. Generate Control Budget Report");
			System.out.println("307. Visualize expense group status");
			System.out.println("310. Remove expense group");
			System.out.println("311. map budget control to XML");
			System.out.println("---------------------");
			System.out.println("0. Exit\n\n");
			option = Console.readInteger("Please choose an option");
			switch (option) {
				case 0:
					System.out.println("bye bye ...");
					return true;
				case 100:
					final RegisterExpenseTypeUI uc01 = new RegisterExpenseTypeUI();
					uc01.show();
					break;
				case 101:
					final ListExpenseTypesUI uc01_L = new ListExpenseTypesUI();
					uc01_L.show();
					break;
				case 102:
					final DeleteExpenseTypeUI uc08_D = new DeleteExpenseTypeUI();
					uc08_D.run();
					break;
				case 103:
					final EditExpenseTypeUI uc_d_007 = new EditExpenseTypeUI();
					uc_d_007.show();
					break;
				case 104:
					final MonthExpenseUI uc_d_004 = new MonthExpenseUI();
					uc_d_004.show();
					break;
				case 105:
					final RegisterBudgetUI uc_o_007 = new RegisterBudgetUI();
					uc_o_007.show();
					break;
				case 106:
					final RegisterExpenseUI uc02 = new RegisterExpenseUI();
					uc02.show();
					break;
				case 107:
					final ExportExpenseGroupToCSVUI uc8g = new ExportExpenseGroupToCSVUI();
					uc8g.doShow();
					break;
				case 108:
					final BudgetXMLUI uc6 = new BudgetXMLUI();
					uc6.run();
					break;
				case 109:
					final RemoveExpenseUI uc_009_d = new RemoveExpenseUI();
					uc_009_d.doShow();
					break;
				case 201:
					final EditBudgetUI uc_o_201 = new EditBudgetUI();
					uc_o_201.doShow();
					break;
				case 202:
					final DefaulBudgetUI uc_o_202 = new DefaulBudgetUI();
					uc_o_202.show();
					break;
				case 203:
					final CancelBudgetUI uc_o_011 = new CancelBudgetUI();
					uc_o_011.show();
					break;
				case 205:
					final MonthExpenseByTypeUI uc_d_005 = new MonthExpenseByTypeUI();
					uc_d_005.show();
					break;
				case 206:
					final MonthsExpensesUI uc_d_006 = new MonthsExpensesUI();
					uc_d_006.show();
					break;
				case 210:
					final RemoveBudgetUI uc_o_010 = new RemoveBudgetUI();
					uc_o_010.show();
					break;
				case 301:
					final CreateExpenseGroupUI uc_g_001 = new CreateExpenseGroupUI();
					uc_g_001.show();
					break;
				case 302:
					final SetExpenseGroupUI uc_g_002 = new SetExpenseGroupUI();
					uc_g_002.show();
					break;
				case 303:
					final CancelExpenseGroupUI uc_o_303 = new CancelExpenseGroupUI();
					uc_o_303.doShow();
					break;
				case 304:
					final ExportExpenseGroupToXMLUI uc_o_304 = new ExportExpenseGroupToXMLUI();
					uc_o_304.doShow();
					break;
				case 305:
					final ListExpenseGroupByYearUI uc_g_005 = new ListExpenseGroupByYearUI();
					uc_g_005.doShow();
					break;
				case 306:
					final DefineTypesPerGroupUI uc_g_006 = new DefineTypesPerGroupUI();
					uc_g_006.show();
					break;
				case 307:
					final VisualizeExpenseGroupStatusUI uc_g_004 = new VisualizeExpenseGroupStatusUI();
					uc_g_004.doShow();
					break;
				case 308:
					final ControlBudgetReportUI uc_o_003 = new ControlBudgetReportUI();
					uc_o_003.doShow();
					break;
				case 310:
					final RemoveExpenseGroupUI uc_g_010 = new RemoveExpenseGroupUI();
					uc_g_010.doShow();
					break;
				case 311:
					final ExportProjectStatusUI teste = new ExportProjectStatusUI();
					teste.doShow();
					break;
				default:
					System.out.println("option not recognized.");
					break;
			}
		}
		return false;
	}

}
