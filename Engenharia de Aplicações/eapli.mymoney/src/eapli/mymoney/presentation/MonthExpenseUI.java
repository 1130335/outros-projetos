/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.MonthExpenseController;
import eapli.mymoney.domain.Expense;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthExpenseUI extends BaseUI {

    private final MonthExpenseController theController = new MonthExpenseController();

    @Override
    protected boolean doShow() {
        
        final double exp = theController.getThisMonthExpenses();
      
            System.out.println(exp);
        
	
        return true;
    }

    @Override
    public String headline() {
        return "Expense of current month:";
    }
}
