/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.ExportExpenseGroupToXMLController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.util.Console;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class ExportExpenseGroupToXMLUI extends BaseUI {

	private ExportExpenseGroupToXMLController controller = new ExportExpenseGroupToXMLController();

	public ExportExpenseGroupToXMLUI() {

	}

	@Override
	protected boolean doShow() {
		List<ExpenseGroup> egl = controller.getAllExpenseGroups();
		String expenseGroup = "";
		boolean validate = false;
		while (!validate) {

			int n = 0;
			for (ExpenseGroup eg : egl) {
				if (eg.getState()) {
					System.out.println(n + " - " + eg.toString());
					n++;
				} else {
					egl.remove(eg);
				}
			}
			if (egl.isEmpty()) {
				System.out.
					println("Error! You must to create an Expense Group first!\n");
				return false;
			}
			validate = select(egl);
			if (!validate) {
				System.out.println("Invalid number!");
			}
		}
		try {
			controller.doExport();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ExportExpenseGroupToXMLUI.class.getName()).
				log(Level.SEVERE, null, ex);
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(ExportExpenseGroupToXMLUI.class.getName()).
				log(Level.SEVERE, null, ex);
		}
		return true;
	}

	private boolean select(List<ExpenseGroup> expenseGroup) {
		int tmp;
		tmp = Console.readInteger("Enter number:");

		if (tmp >= 0 && tmp < expenseGroup.size()) {
			controller.setGroup(expenseGroup.get(tmp));
			return true;
		}
		return false;
	}

	@Override
	public String headline() {
		return "EXPORT EXPENSE GROUP TO XML";
	}
}
