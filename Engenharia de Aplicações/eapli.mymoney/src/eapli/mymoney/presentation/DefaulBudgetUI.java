/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.DefaultBudgetController;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author i130353
 */
public class DefaulBudgetUI extends BaseUI {

	Scanner sc = new Scanner(System.in);
	private DefaultBudgetController dbController;
	private DefaultBudgetMechanismInterface dbMechanism;
	private Budget budget;
	private List<DefaultBudgetMechanismInterface> dbMList;

	public DefaulBudgetUI() {
	}

	public void run() {
		/*Create the controller*/
		dbController = new DefaultBudgetController();

		boolean i = false;

		/*While the user doesn't confirm the budget, the body of the while is repeated.*/
		while (i == false) {
			dbMList = dbController.getListMechanisms();

			if (dbMList == null) {
				System.out.
					println("There are no mechanism to create a budget!\n");
				i = true;
			} else {
				dbMechanism = selectMechanism(dbMList);

				budget = dbController.createBudget(dbMechanism);

				i = confirmOrcamento(budget);
			}
		}

		if (dbMList != null) {
			showSucess();
		}
	}

	@Override
	protected boolean doShow() {
		run();
		return true;
	}

	/**/
	@Override
	public String headline() {
		return "Create Default Budget";
	}

	/*Method used to confirm if the budget is accepted.*/
	private boolean confirmOrcamento(Budget budget) {
		String op;
		budget.toString();
		do {
			System.out.
				println("\nPlease confirm that you accept this budget (Y/N):\n");
			op = sc.next();
		} while (!(op.equalsIgnoreCase("Y") | op.equalsIgnoreCase("N")));

		return "Y".equalsIgnoreCase(op);
	}

	/*Method that informs the user that the budget was succesfully created.*/
	private void showSucess() {
		System.out.println("The budget was created succesfully.");
	}

	/*Method that shows the existing mechanisms for the user to choose one.*/
	private DefaultBudgetMechanismInterface selectMechanism(
		List<DefaultBudgetMechanismInterface> listaMec) {
		System.out.
			println("\nThe list of available mechanism:\nChoose one please.");

		for (int i = 0; i < listaMec.size(); i++) {
			System.out.println(i + ":" + listaMec.get(i).toString());
		}

		int op = sc.nextInt();
		return listaMec.get(op);
	}

}
