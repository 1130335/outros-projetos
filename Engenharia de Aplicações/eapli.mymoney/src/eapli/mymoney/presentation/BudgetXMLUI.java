/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.BudgetXMLController;
import eapli.mymoney.domain.Budget;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aria
 */
public class BudgetXMLUI {
    
    private BudgetXMLController controller = new BudgetXMLController() ;
    
    
    public void run(){
        List<Budget> listBudget = controller.getListBudgets();
        showBudgetList(listBudget);
        Budget b= selectBudget(listBudget);
        try {
            controller.ExportBudget(b);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BudgetXMLUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void showBudgetList(List<Budget> listBudget) {
        for (Budget b : listBudget) {
            System.out.println(b.toString());
        }
       
            
    }

    private Budget selectBudget(List<Budget> l) {
       
        System.out.println("Select the Budget, type id \n ");
        Scanner sc = new Scanner(System.in);
        long id = sc.nextLong();
        return getBudgetbyID(l,id);
        
         
    }

    private Budget getBudgetbyID(List<Budget> l, long id) {
      Budget budget =null;
        for (Budget b : l) {
            if(b.id()==id){
            budget=b;    
                
            }
        }
        return budget;
    }

   
    
    

    
    
}