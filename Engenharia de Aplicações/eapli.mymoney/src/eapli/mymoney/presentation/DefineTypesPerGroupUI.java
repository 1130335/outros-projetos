/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.DefineTypesPerGroupController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author Paulo
 */
public class DefineTypesPerGroupUI extends BaseUI {

	private DefineTypesPerGroupController controller;

	public DefineTypesPerGroupUI() {

		controller = new DefineTypesPerGroupController();
	}

	@Override
	protected boolean doShow() {
		String res;
		selectExpenseGroup();
		selectExpenseType();
		res = Console.readLine("Save alterations? Yes/No");
		if (res.equalsIgnoreCase("yes")) {
			if (controller.confirm()) {
				System.out.println("Data saved with success");
			} else {
				System.out.println("Already exits expense types with expenses");
			}
		} else {
			System.out.println("Data not saved!");
		}
		return true;
	}

	private void selectExpenseType() {
		String res;

		do {
			showExpenseType();
			res = Console.readLine("Select Expense Type");
			ExpenseType expensetype = controller.getExpenseTypeList().
				get(Integer.parseInt(res));
			controller.addExpenseType(expensetype);
			res = Console.readLine("Add more expense types?");

		} while (res.equalsIgnoreCase("yes"));

	}

	private void showExpenseType() {
		System.out.println("***Expense Types***");
		List<ExpenseType> list = controller.getExpenseTypeList();
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + " " + list.get(i).toString());
		}

	}

	private void showExpenseGroup() {
		System.out.println("***Expense Groups***");
		List<ExpenseGroup> list = controller.getExpenseGroupList();
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + " " + list.get(i).getName());
		}

	}

	private void selectExpenseGroup() {

		String res;

		showExpenseGroup();
		res = Console.readLine("Select Expense Group");
		ExpenseGroup expensegroup = controller.getExpenseGroupList().
			get(Integer.parseInt(res));
		controller.setExpenseGroup(expensegroup);
	}

	@Override
	public String headline() {
		return "Define Types Per Group";
	}

}
