/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.CancelBudgetController;
import eapli.mymoney.domain.Budget;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Pedro
 */
public class CancelBudgetUI extends BaseUI {

	private CancelBudgetController controller = new CancelBudgetController();

	/**
	 * Shows info to user
	 *
	 * @return true if success
	 */
	public boolean doShow() {
		int numBudgets = showBudgets();
		if (numBudgets > 0) {
			Budget bud = choose(numBudgets);
			endUC(bud);
		} else {
			System.out.println("There are no Budgets to cancel!");
		}
		return true;
	}

	/**
	 * Method that shows all the Budgets available
	 *
	 * @return cont Returns the number of Budgets available
	 */
	int showBudgets() {
		List<Budget> budgets = controller.getAll();
		int numero = 1, cont = 0;

		if (!budgets.isEmpty()) {
			for (Budget sb : budgets) {
				if (sb.getState() == true) {
					System.out.println(numero + "-> Budget ID: " + sb.id());
					numero++;
					cont++;
				}
			}
		}
		return cont;
	}

	/**
	 * Method that allows the user to choose one of the available Budgets
	 *
	 * @param numBudgets Number of Budgets available
	 * @return Budget Returns the Budget that will be cancelled
	 */
	public Budget choose(int numBudgets) {
		Scanner read = new Scanner(System.in);
		List<Budget> budgets = controller.getAll();
		Budget bud = new Budget();
		int continua = 0, option;
		String opt;

		while (continua == 0) {
			System.out.
				println("Which Budget do you wish to cancel?\n Select the number:");
			opt = read.nextLine();

			if (opt.matches("[0-9]+")) {
				option = Integer.valueOf(opt);

				if (option > 0 && option <= numBudgets) {
					bud = budgets.get(option - 1);
					break;
				} else {
					System.out.
						println("You can only choose between 1 and " + numBudgets);
				}
			} else {
				System.out.println("Please choose a number from the list!");

			}
		}
		return bud;
	}

	/**
	 * Last Method that allows the user to cancel completely the Budget
	 *
	 * @param bud Budget to cancel
	 */
	private void endUC(Budget bud) {
		Scanner read = new Scanner(System.in);
		int continua = 0;
		String opt;
		System.out.println("Are you sure you want to cancel this Budget? (Y/N)");
		while (continua == 0) {
			opt = read.nextLine();
			if (opt.matches("[Y|y|N|n]")) {
				if (opt.equals("n") || opt.equals("N")) {
					System.out.println("Operation canceled");
					break;
				} else {
					controller.cancelBudget(bud);
					System.out.println("The Budget was sucessfuly cancelled!");
					break;
				}
			} else {
				System.out.
					println("Please chose \"Y\" for Yes or \"N\" for No!");
			}
		}
	}

	@Override
	public String headline() {
		return "Cancel Expernse Type";
	}
}
