/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.CreateExpenseGroupController;
import eapli.util.DateTime;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Utilizador
 */
public class CreateExpenseGroupUI extends BaseUI {

	Scanner in = new Scanner(System.in);
	private CreateExpenseGroupController controllerCEG;

	@Override
	protected boolean doShow() {
		setBasicData();
		aditionalData();
		controllerCEG.add();
		return true;
	}

	@Override
	public String headline() {
		return "CREATE EXPENSE GROUP";
	}

	private void setBasicData() {
		String name;
		int year, month, day;

		controllerCEG = new CreateExpenseGroupController();
		System.out.println("Introduce the name: ");
		name = in.next();
		System.out.println("\n");
		System.out.println("Set Beginning Date:\n");
		year = eapli.util.Console.readInteger("Year:");
		month = eapli.util.Console.readInteger("Month:");
		day = eapli.util.Console.readInteger("Day:");

		Date beginD = DateTime.newCalendar(year, month, day).getTime();

		System.out.println("Set End Date:\n");
		year = eapli.util.Console.readInteger("Year:");
		month = eapli.util.Console.readInteger("Month:");
		day = eapli.util.Console.readInteger("Day:");

		Date endD = DateTime.newCalendar(year, month, day).getTime();
		controllerCEG.createGroup(name, beginD, endD);
	}

	private void aditionalData() {
		String resp;
		float value;
		System.out.println("Do you want to add the total estimate?(yes/no)\n");
		resp = in.next();

		if (resp.equals("yes")) {

			System.out.println("Introduce the value:");
			value = Float.parseFloat(in.next());
			controllerCEG.setOptionalData(value);
		}

	}

}
