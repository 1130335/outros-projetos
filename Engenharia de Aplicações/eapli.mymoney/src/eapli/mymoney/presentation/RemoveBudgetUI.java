/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.RemoveBudgetController;
import eapli.mymoney.domain.Budget;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class RemoveBudgetUI extends BaseUI {

	private RemoveBudgetController controller = new RemoveBudgetController();

	public boolean doShow() {

		//show all and select Budgets
		showAll();
		//removes and inform of sucess
		submit();
		return true;
	}

	private void showAll() {
		List<Budget> all = controller.getAllBudgets();

		if (all.isEmpty()) {
			System.out.println("No Budgets to Remove!");
			System.exit(0);
		} else {
			int n = 0;
			System.out.println("Budgets");
			for (Budget sb : all) {
				System.out.println("Budget " + n + " id= " + sb.id());
				n++;
			}
		}
	}

	public void submit() {
		final List<Budget> Budget = controller.getAllBudgets();
		int tmp;
		tmp = Console.readInteger("Choose the number to remove:");

		controller.removeB(Budget.get(tmp));

		System.out.println("\nBudget Removed!");

	}

	@Override
	public String headline() {
		return "REMOVE BUDGET FROM REPOSITORY!";
	}
}
