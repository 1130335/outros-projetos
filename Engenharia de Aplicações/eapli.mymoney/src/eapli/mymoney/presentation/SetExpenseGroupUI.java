/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.SetExpenseGroupController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.util.Console;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sergio
 */
public class SetExpenseGroupUI extends BaseUI {

	private SetExpenseGroupController controller;

	@Override
	protected boolean doShow() {
		showExpenseGroup();
		selectExpenseGroup();
		showExpenseGroupData();
		insertNewData();
		confirm();
		return true;
	}

	private void showExpenseGroup() {
		System.out.println("***Expense Groups***");
		List<ExpenseGroup> list = controller.getExpenseGroupList();
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + " " + list.get(i).getName());
		}

	}

	private void selectExpenseGroup() {

		String res;

		showExpenseGroup();
		res = Console.readLine("Select Expense Group");
		ExpenseGroup expensegroup = controller.getExpenseGroupList().
			get(Integer.parseInt(res));
		controller.setExpenseGroup(expensegroup);
	}

	public void showExpenseGroupData() {
		controller.getExpenseGroupData();
	}

	public void insertNewData() {

		String name;
		float value;
		name = Console.readLine("Insert new name: ");
		Date iniDate = insertDate(), endDate = insertDate();

		value = (float) Double.parseDouble(Console.
			readLine("Insert new total value for this expense group: "));
		controller.setData(name, iniDate, endDate, value);
		if (confirm()) {
			controller.confirm();
			System.out.println("Success!");
		} else {
			System.out.println("Canceled");
		}
	}

	public boolean confirm() {
		String confirm;
		confirm = Console.readLine("Confirm changes?(yes/no)");
		if (confirm.equalsIgnoreCase("yes")) {
			return true;
		}
		return false;
	}

	@Override
	public String headline() {
		return "Set Expense Group";
	}

	public Date insertDate() {
		int year, month, day;
		System.out.println("New initial date:");
		day = Integer.parseInt(Console.
			readLine("Insert new day for initial date: "));
		month = Integer.parseInt(Console.
			readLine("Insert new month for initial date: "));
		year = Integer.parseInt(Console.
			readLine("Insert new year for initial date: "));
		return new Date(year, month, day);

	}
}
