/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.EditExpenseTypeController;
import eapli.mymoney.domain.ExpenseType;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author i130303
 */
public class EditExpenseTypeUI extends BaseUI{
    private final EditExpenseTypeController controller = new EditExpenseTypeController();

    @Override
    public boolean doShow()
    {
        
        //show all and select expense type; if there are no expense types available a error message is shown and the uc exit.
        if(!showAll())
            return false;
        //asks for new description
        submit();
        //Update expense type on the repo
        if(controller.editExpenseType())
        {
            System.out.println("\nExpense type edited!");
            return true;
        } else
        {
            System.out.println("\nExpense type not edited!");
            return false;
        }
    }
    
    private boolean showAll()
    {
        List<ExpenseType> all = controller.getAllExpenseTypes();
        if(all.isEmpty())
        {
            System.out.println("Error! You have to create a new expense type first!\n");
            return false;
        }
        int n = 0; boolean flag = false;
        while(!flag)
        {
            n=0;
            for(ExpenseType et: all)
            {
                System.out.println(n + " - " + et.description());
                n++;
            }
            flag = select(all);
            if(!flag)
                System.out.println("Invalid number!");
        }
        return true;
        
    }
    
    private boolean select(List<ExpenseType> all)
    {
        int tmp;
        tmp = Console.readInteger("Enter number:");
        
        if(tmp >= 0 && tmp < all.size())
        {
            controller.setExpenseType(all.get(tmp));
            return true;
        }
        return false;
    }

    private void submit() 
    {
        while(!controller.setDescription(Console.readLine(("Enter new description:"))));
    }

    @Override
    public String headline() {
        return "EDIT AN EXPENSE TYPE";
    }
}
