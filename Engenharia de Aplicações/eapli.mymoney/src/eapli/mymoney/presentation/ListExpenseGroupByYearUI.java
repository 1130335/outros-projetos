/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

/**
 *
 * @author Francisco Lopes 1130576
 */

import eapli.mymoney.application.*;
import eapli.mymoney.domain.*;
import eapli.util.Console;
import java.util.*;

/**
 *
 * @author Rui
 */
public class ListExpenseGroupByYearUI extends BaseUI{

    private final ListExpenseGroupByYearController theController = new ListExpenseGroupByYearController();
    
    @Override
    protected boolean doShow() {
        boolean flag;
        do{
            flag = selectYear();
        }while(!flag);
        return showAll();
    }

    @Override
    public String headline() {
        return "List Expense Group By Year";
    }
    
    /**
    * Shows to the user the expense groups with the end year previous defined.
    * @return boolean True if there are expense groups available
    */
    private boolean showAll()
    {
        List<ExpenseGroup> all = theController.getAllExpenseGroups();
        if(all.isEmpty())
        {
            System.out.println("Error! You have to create a new expense group first!\n");
            return false;
        }
        
        int n = 0;
        
        List<ExpenseGroup> allByYear = theController.getExpenseGroupsPerYear();
        
        
        for(ExpenseGroup eg: allByYear)
        {
            System.out.println(n + " - " + eg.getName());
            System.out.println("> Current value: " + theController.getRealValueByExpenseGroup(eg));
            System.out.println("> Estimated value: " + eg.getTotalValue());
            System.out.println("> Diference:  " + (eg.getTotalValue() - theController.getRealValueByExpenseGroup(eg)));
            n++;
        }
        
        return true;
        
    }
    
    /**
    * Asks the user for the end year and validates it.
    * @return boolean True in case of a valid year
    */
    private boolean selectYear(){
        final List<ExpenseGroup> expenseGroups = theController.getAllExpenseGroups();
        int endYear;
        boolean flag;
        do{
            endYear = Console.readInteger("Please indicate the expense group end year:");
            theController.setEndYear(endYear);
            if(theController.validateYear(endYear))
                flag = true;
            else{
                flag = false;
                System.out.println("Error. Invalid year!");
            }
        }while(!flag);
        
        return flag;
    }
}
