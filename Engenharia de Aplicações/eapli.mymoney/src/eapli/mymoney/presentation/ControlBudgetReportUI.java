/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.ControlBudgetReportController;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.ExpenseTypeEntry;
import eapli.util.Console;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 * @author André Garrido<1101598>
 */
public class ControlBudgetReportUI extends BaseUI {

	private ControlBudgetReportController controller = new ControlBudgetReportController();

	public boolean doShow() {

		//show all and select Budgets
		showAll();
		//removes and inform of sucess
		report();
		return true;
	}

	private void showAll() {
		List<Budget> all = controller.getAllBudgets();

		if (all.isEmpty()) {
			System.out.println("No Budgets to Show!");
			System.exit(0);
		} else {
			int n = 0;
			System.out.println("Budgets");
			for (Budget sb : all) {
				System.out.println("Budget " + n + " id= " + sb.id());
				n++;
			}
		}
		//System.out.println("Invalid Selection!");

	}

	public void report() {

		final List<Budget> allbudgets = controller.getAllBudgets();
		final List<ExpenseType> allExpenseTypes = controller.
			getAllExpenseTypes();
		int tmp;
		Double forecast_expense;
		BigDecimal budgeted_value = new BigDecimal("0");
		Double spentvalue;
		tmp = Console.readInteger("Choose a Budget:");

		Budget sb = allbudgets.get(tmp);

		/* Codigo para calculo da estimativa gasta ate ao final do mes*/
		int month = sb.getMonth();  //get the month of the selected budget
		int year = sb.getYear();

		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
		int numberofdays = cal.getActualMaximum(Calendar.DAY_OF_MONTH); //get the total number of days in a selected month

		int currentday = Calendar.DAY_OF_MONTH; //get the current day    NOT WORKING!!!!!!!!!!!


		/* ----------------------------------------------------------------------*/
		//get months expenses by ExpenseType
		Map<String, Double> map = controller.
			getMonthsExpensesByType(month, allExpenseTypes);

		List<Entry> le = sb.getEntries();

		for (Entry ent : le) {
			if (ent.getClass().getSimpleName().equals("ExpenseTypeEntry")) {
				ExpenseType exptype = ((ExpenseTypeEntry) ent).getExpenseType();
				budgeted_value = ((ExpenseTypeEntry) ent).getValue();

				for (Map.Entry<String, Double> entry : map.entrySet()) {
					String key = entry.getKey();
					Double value = entry.getValue();

					if (key.equals(((ExpenseTypeEntry) ent).getExpenseType().
						description())) {
						spentvalue = value;
						forecast_expense = (spentvalue / currentday) * numberofdays;
						System.out.println("CurrentDay " + currentday);
						System.out.println("NumberofDays " + numberofdays);

						System.out.
							println("Expense:" + ((ExpenseTypeEntry) ent).
								getExpenseType().description() + "  " + "Budgeted Value: " + budgeted_value);
						System.out.println("Current Spent: " + spentvalue);
						System.out.println("Forecast: " + forecast_expense);

					}
				}
			}
		}

		System.out.println("\nNo more Expenses to show!");
	}

	public String headline() {
		return "CONTROL BUDGET REPORT";
	}
}
