/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.RemoveExpenseGroupController;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class RemoveExpenseGroupUI extends BaseUI{

	private final RemoveExpenseGroupController controller = new RemoveExpenseGroupController();

	

    @Override
    protected boolean doShow() {
        final List<ExpenseGroup> expenseGroups = controller.getAllExpenseGroups();
        int n=0;
        if(!expenseGroups.isEmpty()){
            for (ExpenseGroup eg : expenseGroups) {
                n++;
                System.out.println("Number of expense group" + n + ":" + eg.getName());
            }
            int tmp;
            tmp = Console.readInteger("Choose the group expense number:");
            if(tmp>0 && tmp<=expenseGroups.size()){
                boolean deleted=controller.deleteExpenseGroup(expenseGroups.get(tmp-1));
                if(!deleted){
                    System.out.println("Expense Group being used. Can't delete.");
                }else{
                    System.out.println("Deleted successfully");
                }
            }else{
                System.out.println("Invalid number to expensive group");
            }
        }else{
            System.out.println("Expense Group list empty");
        }
        return true;
    }
    @Override
    public String headline() {
        return "List Expense Groups";
    }
}
