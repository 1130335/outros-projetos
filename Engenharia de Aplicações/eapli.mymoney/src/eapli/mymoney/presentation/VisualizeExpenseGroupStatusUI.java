package eapli.mymoney.presentation;

import eapli.mymoney.application.VisualizeExpenseGroupStatusController;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.jpa.ExpenseGroupRepositoryImpl;
import eapli.util.Console;
import java.util.List;

public class VisualizeExpenseGroupStatusUI extends BaseUI {

	private VisualizeExpenseGroupStatusController vegsc = new VisualizeExpenseGroupStatusController();

	public VisualizeExpenseGroupStatusUI() {

	}

	@Override
	protected boolean doShow() {
		List<ExpenseGroup> egl = new ExpenseGroupRepositoryImpl().all();
		int i = 1;
		for (ExpenseGroup eg : egl) {
			System.out.println("## EXPENSE GROUPS ##\n");
			System.out.println(i + " - " + eg.getName() + "\n");
			i++;
		}
		i = Console.readInteger("Enter the number of the group » ");
		ExpenseGroup eg = egl.get(i - 1);
		List<Expense> le = vegsc.processExpenses(eg);
		double value = vegsc.calculateValue(le);
		String s = "## Expense Group Status ##\n"
			+ "Name: " + eg.getName()
			+ "\nValue: " + value
			+ "\nExpenses: ";
		System.out.println(s);
		i = 1;
		for (Expense e : le) {
			System.out.println("\n" + i + " - " + e.getComment());
		}
		System.out.println("\n");
		return true;
	}

	@Override
	public String headline() {
		return "VISUALIZE EXPENSE GROUP STATUS";
	}

}
