package eapli.mymoney.strategy;

import eapli.mymoney.domain.Entry;
import java.util.List;
import java.util.Map;

public interface ExportProjectStatus {

	boolean export(List<Entry> le, int month, int year,Map<String, Double> map);
}
