/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.strategy;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.persistence.BudgetRepository;

/**
 *
 * @author João Cabral
 */
public interface DefaultBudgetMechanismInterface {

	/* Method that creates a budget. */
	public Budget createBudget(BudgetRepository budgetRep);
}
