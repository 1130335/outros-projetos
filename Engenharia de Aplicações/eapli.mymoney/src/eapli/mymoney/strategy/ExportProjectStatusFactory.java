/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.strategy;

import eapli.mymoney.strategy.ExportProjectStatus;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Formatter;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;



// Desconto de 20% sobre o valor total
public class ExportProjectStatusFactory{
    private static ExportProjectStatusFactory fabrica = null;
    private ExportProjectStatusFactory() { } // Singleton
    public static synchronized ExportProjectStatusFactory getInstance() {
        if (fabrica == null) fabrica = new ExportProjectStatusFactory();
            return fabrica;
        }

    public ExportProjectStatus getEstrategia() {
    String strClassName;
   
    try {
       
        FileInputStream configFile = new FileInputStream("C:\\Users\\Daniel\\Documents\\NetBeansProjects\\eapli-2015-2db\\eapli.mymoney\\src\\eapli\\mymoney\\main.config");
        
        Properties p = new Properties();
        p.load(configFile);
        strClassName = p.getProperty("tipo_exportacao");
    } catch (Exception ex) { strClassName = "teapli.mymoney.strategy.ExportProjectStatusToXML"; }
    try {
        Class exportation = Class.forName(strClassName);
        ExportProjectStatus exp = (ExportProjectStatus) exportation.newInstance();
        return exp;
    } catch (Exception ex) { return null; }
    }


}
