/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.strategy;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.ExpenseTypeEntry;
import eapli.mymoney.strategy.ExportProjectStatus;
import eapli.util.Console;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ExportProjectStatusToXML implements ExportProjectStatus {
    @Override
    public boolean export(List<Entry> le, int month, int year,Map<String, Double> map){
     
		
		Double forecast_expense;
		BigDecimal budgeted_value = new BigDecimal("0");
		Double spentvalue;
		
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
		int numberofdays = cal.getActualMaximum(Calendar.DAY_OF_MONTH); //get the total number of days in a selected month
                int currentday = Calendar.DAY_OF_MONTH; //get the current day    NOT WORKING!!!!!!!!!!!


		/* ----------------------------------------------------------------------*/
		//get months expenses by ExpenseType
                try {
		PrintWriter writer = new PrintWriter("BudgetMap.xml", "UTF-8");
             
                   
                    writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    writer.println("\n <Entries>");
                    for (Entry ent : le) {
                        
			if (ent.getClass().getSimpleName().equals("ExpenseTypeEntry")) {
                            writer.println("\n <Entry>");
                            ExpenseType exptype = ((ExpenseTypeEntry) ent).getExpenseType();
                            budgeted_value = ((ExpenseTypeEntry) ent).getValue();

				for (Map.Entry<String, Double> entry : map.entrySet()) {
					String key = entry.getKey();
					Double value = entry.getValue();
                                       
					if (key.equals(((ExpenseTypeEntry) ent).getExpenseType().
						description())) {
                                                writer.println("\n <Expense description=" + ((ExpenseTypeEntry) ent).getExpenseType().description()+">");
						spentvalue = value;
						forecast_expense = (spentvalue / currentday) * numberofdays;
                                                writer.println("\n <CurrentDay>" + currentday +"</CurrentDay>");
                                                writer.println("\n <NumberofDays>" + numberofdays +"</NumberofDays>");
                                                writer.println("\n <BudgetedValue>" + budgeted_value +"</BudgetedValue>");
                                                writer.println("\n <Current Spent>" + spentvalue +"</Current Spent>");
                                                writer.println("\n <Forecast>" + forecast_expense +"</Forecast>");
						writer.println("\n </Expense>");

					}
				}
                           writer.println("\n </Entry>");
			}
		}
                writer.println("\n </Entries>");
                writer.close();
     } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ExportProjectStatusToXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExportProjectStatusToXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    return true;
    }
    

}
