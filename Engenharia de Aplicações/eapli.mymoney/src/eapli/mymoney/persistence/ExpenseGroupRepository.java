
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.ExpenseGroup;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Utilizador
 */
public interface ExpenseGroupRepository {

	boolean add(ExpenseGroup expense);

	void remove(ExpenseGroup expense);

	long size();

	List<ExpenseGroup> all();

	public Iterator<ExpenseGroup> iterator(int pagesize);

	boolean cancelExpenseGroup(ExpenseGroup expense);

	public boolean updateExpenseGroup(ExpenseGroup expGroupOld,
									  ExpenseGroup expGroupNew);

	public List<ExpenseGroup> getActiveExpenseGroup(Date dia_registo_despesa);
}
