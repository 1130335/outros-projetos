/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.observer.ExpenseAlertInterface;
import eapli.mymoney.persistence.ExpenseAlertRepository;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Pedro_Coelho_1130353
 */
public class ExpenseAlertRepositoryImpl implements ExpenseAlertRepository{

    List<ExpenseAlertInterface> expenseAlertList;
    
    @Override
    public boolean add(ExpenseAlertInterface alert) {
        return this.expenseAlertList.add(alert);
    }

    @Override
    public long size() {
        return expenseAlertList.size();
    }

    @Override
    public List<ExpenseAlertInterface> all() {
        return Collections.unmodifiableList(this.expenseAlertList);
    }
    @Override
    public Iterator<ExpenseAlertInterface> iterator(int pagesize) {
        return this.expenseAlertList.iterator();
    }
    
    @Override
    public void remove(ExpenseAlertInterface alert) {
        this.expenseAlertList.remove(alert);
    }
    
    @Override
    public void checkAlerts(Expense exp) {
        for(ExpenseAlertInterface alert : this.expenseAlertList){
            alert.validateExpense(exp);
        }
    }
    
}
