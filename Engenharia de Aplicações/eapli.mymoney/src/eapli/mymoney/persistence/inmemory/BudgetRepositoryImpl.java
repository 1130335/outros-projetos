/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.persistence.BudgetRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author André Garrido<1101598>
 */
public class BudgetRepositoryImpl implements BudgetRepository {

	private static final List<Budget> data = new ArrayList<>();

	@Override
	public boolean add(Budget budget) {
		if (budget == null) {
			throw new IllegalArgumentException();
		}
		if (data.contains(budget)) {
			//TODO rever se deviamos ter outra exceção mais significativa
			throw new IllegalStateException();
		}
		return data.add(budget);
	}

	@Override
	public void remove(Budget sb) {
		Iterator<Budget> data1 = data.iterator();
		while (data1.hasNext()) {
			if (data.equals(sb)) {
				data1.remove();
			}
		}
//        for (Budget data1 : data)
//            if (data1.equals(sb))

	}

	@Override
	public boolean update(Budget sb, List<Entry> entries) {
		for (Budget data1 : data) {
			if (data1.equals(sb)) {
				data1.setEntries(entries);
				return true;
			}
		}
		return false;
//		Iterator<Budget> data1 = data.iterator();
//		while (data1.hasNext()) {
//			Budget tmp = data1.next();
//			if (data.equals(sb)) {
//				List<Entry> entries = tmp.getEntries();
//				Iterator<Entry> data2 = entries.iterator();
//				while (data2.hasNext()) {
//					Entry eTmp = data2.next();
//					if (entries.equals(entry)) {
//
//					}
//				}
//			}
//		}
//        for (Budget data1 : data)
//            if (data1.equals(sb))

	}

	@Override
	public long size() {
		return data.size();
	}

	@Override
	public List<Budget> all() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public Iterator<Budget> iterator(int pagesize) {
		return data.iterator();
	}

	@Override
	public Budget getLastMonthsBudget() {

		for (int i = 0; i < data.size(); i++) {
			Budget budget = data.get(i);

//			if(budget.wasMadeLastMonth()){
//				return budget;
//			}
		}

		return null;
	}

	@Override
	public void cancelBudget(Budget sb) {
		if (sb == null) {
			throw new IllegalArgumentException();
		}
		for (Budget data1 : data) {
			if (data1.equals(sb)) {
				data1.cancelBudget(false);
			}
		}
	}

	public boolean anyBudgetIn(int month, int year) {
		return false;
	}
}
