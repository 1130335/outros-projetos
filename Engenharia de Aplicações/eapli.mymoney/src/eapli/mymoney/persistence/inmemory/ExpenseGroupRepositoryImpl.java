/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseGroupRepositoryImpl implements ExpenseGroupRepository {

	private static final List<ExpenseGroup> data = new ArrayList<ExpenseGroup>();

	@Override
	public boolean add(ExpenseGroup expenseGroup) {
		if (expenseGroup == null) {
			throw new IllegalArgumentException();
		}
		if (data.contains(expenseGroup)) {
			//TODO rever se deviamos ter outra exceção mais significativa
			throw new IllegalStateException();
		}
		return data.add(expenseGroup);
	}

	@Override
	public void remove(ExpenseGroup eg) {
		Iterator<ExpenseGroup> data1 = data.iterator();
		while (data1.hasNext()) {
			ExpenseGroup tmp = data1.next();
			if (data.equals(eg)) {
				data1.remove();
			}
		}

	}
	/*
	 * Replaces expense type (et) description for the new one (nd)
	 * @param et Existing expense type object
	 * @param nd New description
	 */

	@Override
	public boolean updateExpenseGroup(ExpenseGroup expGroupOld,
									  ExpenseGroup expGroupNew) {
		for (ExpenseGroup data1 : data) {
			if (data1.equals(expGroupOld)) {

				//SET THIS
				//  data1.setDescription(nd);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean cancelExpenseGroup(ExpenseGroup expenseGroup) {
		if (expenseGroup == null) {
			throw new IllegalArgumentException();
		}
		for (ExpenseGroup data1 : data) {
			if (data1.equals(expenseGroup)) {
				data1.cancelExpenseGroup(false);
				return true;
			}
		}
		return data.add(expenseGroup);
	}

	@Override
	public long size() {
		return data.size();
	}

	// TODO check if we realy need this method
	public boolean contains(ExpenseGroup group) {
		return data.contains(group);
	}

	@Override
	public List<ExpenseGroup> all() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public Iterator<ExpenseGroup> iterator(int pagesize) {
		return data.iterator();
	}

	public List<ExpenseGroup> getActiveExpenseGroup(Date dia_registo_despesa) {
		return Collections.unmodifiableList(data);
	}
}
