package eapli.mymoney.persistence.inmemory;

//import com.sun.javafx.scene.control.skin.VirtualFlow;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.RollbackException;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseRepositoryImpl implements ExpenseRepository {

	private static final List<Expense> data = new ArrayList<Expense>();

	@Override
	public boolean add(Expense expense) {
		if (expense == null) {
			throw new IllegalArgumentException();
		}
		if (data.contains(expense)) {
			//TODO rever se deviamos ter outra exceção mais significativa
			throw new IllegalStateException();
		}
		return data.add(expense);
	}

	@Override
	public void delete(Expense expense) {
		try {
			data.remove(expense);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
	}

	@Override
	public long size() {
		return data.size();
	}

	@Override
	public List<Expense> all() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public Iterator<Expense> iterator(int pagesize) {
		return data.iterator();
	}

	@Override
	public Double getExpenseWeekValue() {
		List<Expense> rep = Persistence.getRepositoryFactory().
			getExpenseRepository().all();
		if (rep == null) {
			return 0.0;
		} else {
			List<Expense> rep1 = null;
			Date d;

			// get today and clear time of day
			Calendar cal = Calendar.getInstance();

			int inicio = cal.getFirstDayOfWeek();
			int date = Calendar.DATE;
			int month = Calendar.MONTH;
			int year = Calendar.YEAR;
			for (int i = 0; i < rep.size(); i++) {

				if (date < inicio) {
					if (month == 1) {
						year--;
						month = 12;
					} else {
						month--;
					}
					d = new Date(year, month, inicio);
				} else {

					d = new Date(year, month, inicio);
				}
				if (rep.get(i).getDate().after(d)) {
					rep1.add(rep.get(i));
				}

			}
			Double l = getValue(rep1);
			if (l != 0 || l != null) {
				return 0.0;
			} else {
				return l;
			}
		}
	}

	private Double getValue(List<Expense> rep1) {
		Double l = 0.0;
		for (int i = 0; i < rep1.size(); i++) {
			l = l + rep1.get(i).getValue();
		}
		return l;
	}

	private double getValueMonth(List<Expense> rep1) {
		double l = 0.0;
		for (int i = 0; i < rep1.size(); i++) {
			l = l + rep1.get(i).getValue();
		}
		return l;
	}

	@Override
	public double getExpenseMonthValue() {
		List<Expense> rep = all();

		ArrayList<Expense> rep1 = new ArrayList<Expense>();

		Calendar cal = Calendar.getInstance();
		cal.setTime(cal.getTime());

		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		for (int i = 0; i < rep.size(); i++) {

			Calendar c = Calendar.getInstance();
			c.setTime(rep.get(i).getDate());

			if ((cal.get(Calendar.MONTH) + 1) == month && c.get(Calendar.YEAR) == year) {

				rep1.add(rep.get(i));
			}

		}
		double l = getValueMonth(rep1);

		return l;

	}

	@Override
	public List<Double> getExpenseMonthsValues(int year) {
		List<Expense> rep = Persistence.getRepositoryFactory().
			getExpenseRepository().all();
		List<Expense> rep1 = null;
		Date d;
		List<Double> monthsValues = new ArrayList<Double>();
		for (int i = 0; i < 12; i++) {
			monthsValues.add(0.0);

		}

		for (int i = 0; i < rep.size(); i++) {

			Calendar c = Calendar.getInstance();
			c.setTime(rep.get(i).getDate());
			if (c.get(Calendar.YEAR) == year) {
				int month = c.get(Calendar.MONTH) + 1;
				double aux = monthsValues.get(month) + rep.get(i).getValue();
				monthsValues.set(month - 1, aux);
			}

		}

		return monthsValues;

	}

	@Override
	public Map<String, Double> getMonthsExpensesByType(int month,
													   List<ExpenseType> listExpensesType) {

		return null;
	}

}
