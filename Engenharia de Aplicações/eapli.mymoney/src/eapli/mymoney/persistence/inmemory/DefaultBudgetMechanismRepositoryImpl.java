/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.persistence.DefaultBudgetMechanismRepository;
import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public class DefaultBudgetMechanismRepositoryImpl implements DefaultBudgetMechanismRepository {

	private static final List<DefaultBudgetMechanismInterface> data = new ArrayList<>();

	@Override
	public boolean add(DefaultBudgetMechanismInterface dbm) {
		if (dbm == null) {
			throw new IllegalArgumentException();
		}
		if (data.contains(dbm)) {
			throw new IllegalStateException();
		}
		return data.add(dbm);
	}

	@Override
	public long size() {
		return data.size();
	}

	@Override
	public List<DefaultBudgetMechanismInterface> all() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public Iterator<DefaultBudgetMechanismInterface> iterator(int pagesize) {
		return data.iterator();
	}

	@Override
	public boolean remove(DefaultBudgetMechanismInterface dbm) {
		Iterator<DefaultBudgetMechanismInterface> data1 = data.iterator();
		while (data1.hasNext()) {
			DefaultBudgetMechanismInterface tmp = data1.next();
			if (data.equals(dbm)) {
				data1.remove();
				return true;
			}
		}
		return false;
	}
}
