/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import javax.persistence.RollbackException;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTypeRepositoryImpl
	extends JpaRepository<ExpenseType, String>
	implements ExpenseTypeRepository {

	@Override
	public boolean add(ExpenseType expenseType) {
		try {
			super.add(expenseType);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
		return true;
	}

	/*
	 * Replaces expense type (et) description for the new one (nd)
	 * TODO Talvez tornar o retorno dependente de algo.
	 * @param et Existing expense type object
	 * @param nd New description
	 */
	@Override
	public boolean updateExpenseType(ExpenseType et, String nd) {
		super.delete(et);
		ExpenseType tmp = new ExpenseType(nd);
		super.add(tmp);
		return true;
	}

	/**
	 * Method to remove the expense type TODO: TEST!!
	 *
	 * @param et Expense type to remove
	 * @return true, if possible
	 */
	@Override
	public boolean remove(ExpenseType et) {

		super.delete(et);
		return true;
	}

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}

	@Override
	public void inactive(ExpenseType et) {
		et.setFlag(false);
		super.save(et);
	}
}
