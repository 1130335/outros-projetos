/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.RollbackException;

public class ExpenseRepositoryImpl
	extends JpaRepository<Expense, String>
	implements ExpenseRepository {

	@Override
	public boolean add(Expense expense) {
		try {
			super.add(expense);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
		return true;
	}

	@Override
	public void delete(Expense expense) {
		try {
			super.delete(expense);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
	}

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}

	@Override
	public Double getExpenseWeekValue() {

		List<Expense> rep = Persistence.getRepositoryFactory().
			getExpenseRepository().all();
		if (rep == null) {
			return 0.0;
		} else {

			List<Expense> rep1 = new ArrayList<>();
			Date d;

			// get today and clear time of day
			Calendar cal = Calendar.getInstance();

			int inicio = cal.getFirstDayOfWeek();
			int date = Calendar.DATE;
			int month = Calendar.MONTH;
			int year = Calendar.YEAR;
			for (int i = 0; i < rep.size(); i++) {

				if (date < inicio) {
					if (month == 1) {
						year--;
						month = 12;
					} else {
						month--;
					}
					d = DateTime.newCalendar(year, month, inicio).getTime();
				} else {

					d = DateTime.newCalendar(year, month, inicio).getTime();
				}
				if (rep.get(i).getDate().after(d)) {
					rep1.add(rep.get(i));
				}

			}

			Double l = getValue(rep1);
			return l;
		}
	}

	private Double getValue(List<Expense> rep1) {
		Double l = 0.0;
		if (rep1 == null) {
			return 0.0;
		} else {
			for (int i = 0; i < rep1.size(); i++) {

				l = l + rep1.get(i).getValue();

			}
			return l;
		}

	}

	private double getValueMonth(List<Expense> rep1) {
		double l = 0.0;
		for (int i = 0; i < rep1.size(); i++) {
			l = l + rep1.get(i).getValue();
		}
		return l;
	}

	@Override
	public double getExpenseMonthValue() {

		List<Expense> rep = Persistence.getRepositoryFactory().
			getExpenseRepository().all();

		ArrayList<Expense> rep1 = new ArrayList<Expense>();

		Calendar cal = Calendar.getInstance();
		cal.setTime(cal.getTime());

		int cont = 0;
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		for (int i = 0; i < rep.size(); i++) {

			Calendar c = Calendar.getInstance();
			c.setTime(rep.get(i).getDate());

			if ((c.get(Calendar.MONTH) + 1) == month && c.get(Calendar.YEAR) == year) {
				cont++;

				rep1.add(rep.get(i));
			}

		}
		Double l = getValueMonth(rep1);

		if (l != 0 || l != null) {
			return 0.0;
		} else {
			return l;
		}

	}

	@Override
	public List<Double> getExpenseMonthsValues(int year) {
		List<Expense> rep = Persistence.getRepositoryFactory().
			getExpenseRepository().all();
		List<Expense> rep1 = null;
		Date d;
		List<Double> monthsValues = new ArrayList<Double>();
		for (int i = 0; i < 12; i++) {
			monthsValues.add(0.0);

		}

		for (int i = 0; i < rep.size(); i++) {

			Calendar c = Calendar.getInstance();
			c.setTime(rep.get(i).getDate());
			if (c.get(Calendar.YEAR) == year) {
				int month = c.get(Calendar.MONTH) + 1;
				double aux = monthsValues.get(month) + rep.get(i).getValue();
				monthsValues.set(month - 1, aux);
			}

		}

		return monthsValues;

	}

	@Override
	public Map<String, Double> getMonthsExpensesByType(int month,
													   List<ExpenseType> listExpensesType) {
		List<Expense> Expenses = Persistence.getRepositoryFactory().
			getExpenseRepository().all();

		List<ExpenseType> ExpensesType = listExpensesType;

		Map<String, Double> result = new HashMap<String, Double>();

		for (ExpenseType Expt : ExpensesType) {
			result.put(Expt.toString(), 0.0);

		}

		for (int i = 0; i < Expenses.size(); i++) {

			Calendar c = Calendar.getInstance();
			c.setTime(Expenses.get(i).getDate());

			if ((c.get(Calendar.MONTH) + 1) == month) {
				if (Expenses.get(i).getType() != null) {
					Double old_value = result.get(Expenses.get(i).getType().
						toString());
					Double new_value = Expenses.get(i).getValue() + old_value;

					result.put(Expenses.get(i).getType().toString(), new_value);
				}
			}
		}

		return result;
	}

}
