
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.persistence.ExpenseGroupRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;

/**
 *
 * @author Sergio
 */
public class ExpenseGroupRepositoryImpl extends JpaRepository<ExpenseGroup, String> implements ExpenseGroupRepository {

	@Override
	public boolean add(ExpenseGroup expenseGroup) {
		try {
			super.add(expenseGroup);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
		return true;
	}

	@Override
	public void remove(ExpenseGroup eg) {
		super.delete(eg);
	}

	public boolean updateExpenseGroup(ExpenseGroup expGroupOld,
									  ExpenseGroup expGroupNew) {
		super.delete(expGroupOld);
		super.add(expGroupNew);
		return true;
	}

	public boolean cancelExpenseGroup(ExpenseGroup expenseGroup) {
		ExpenseGroup tmp = new ExpenseGroup(expenseGroup);
		tmp.cancelExpenseGroup(false);
		super.delete(expenseGroup);
		super.add(tmp);
		return true;
	}

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}

	public List<ExpenseGroup> getActiveExpenseGroup(Date dia_registo_despesa) {
		List<ExpenseGroup> groups = new ArrayList<>();
		EntityManager em = entityManager();
		//SimpleDateFormat formatter = new SimpleDateFormat("MMMMMMMMM dd, YYYY");
		String tableName = ExpenseGroup.class.getSimpleName();
		String query_string = "SELECT g "
			+ "FROM " + tableName + " g "
			+ "Where g.initialDate<=:dia_registo_despesa "
			+ "and g.endDate >=:dia_registo_despesa";

		Query q = em.createQuery(query_string);
		q.setParameter("dia_registo_despesa", dia_registo_despesa);
		groups = q.getResultList();

		return groups;
	}
}
