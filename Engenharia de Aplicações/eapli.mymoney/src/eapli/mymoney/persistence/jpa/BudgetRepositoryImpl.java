/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.persistence.BudgetRepository;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;

/**
 *
 * @author André Garrido<1101598>
 */
public class BudgetRepositoryImpl
	extends JpaRepository<Budget, Long>
	implements BudgetRepository {

	@Override
	public boolean add(Budget budget) {
		try {
			super.add(budget);
		} catch (RollbackException ex) {
			throw new IllegalStateException();
		}
		return true;
	}

	/*
	 * TODO: I could do better but I don't know how for now...
	 */
	@Override
	public void remove(Budget sb) {
		super.delete(sb);
	}

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}

	@Override
	public Budget getLastMonthsBudget() {
		List<Budget> tmpList = all();

		for (int i = 0; i < tmpList.size(); i++) {
			Budget budget = tmpList.get(i);

			if (budget.wasMadeLastMonth()) {
				return budget;
			}
		}

		return null;
	}

	@Override
	public boolean update(Budget sb, List<Entry> entries) {
		Budget tmp = new Budget(sb);
		tmp.setEntries(entries);
		super.delete(sb);
		super.add(tmp);
		return true;
	}

	@Override
	public void cancelBudget(Budget sb) {
		Budget tmp = new Budget(sb);
		tmp.cancelBudget(false);
		super.delete(sb);
		super.add(tmp);
	}

	public boolean anyBudgetIn(int month, int year) {
		List<Budget> groups = new ArrayList<>();
		EntityManager em = entityManager();
		//SimpleDateFormat formatter = new SimpleDateFormat("MMMMMMMMM dd, YYYY");
		String tableName = Budget.class.getSimpleName();
		String query_string = "SELECT b "
			+ "FROM " + tableName + " b "
			+ "Where b.month=" + month
			+ " and b.year=" + year;

		Query q = em.createQuery(query_string);
		groups = q.getResultList();

		return !groups.isEmpty();
	}
}
