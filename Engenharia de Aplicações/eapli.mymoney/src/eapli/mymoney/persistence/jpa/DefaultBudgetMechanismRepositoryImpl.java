/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.persistence.DefaultBudgetMechanismRepository;
import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.util.Iterator;
import java.util.List;
import javax.persistence.RollbackException;

/**
 *
 * @author 1130353 Pedro Coelho
 */
public class DefaultBudgetMechanismRepositoryImpl extends JpaRepository<DefaultBudgetMechanismInterface, Long> implements DefaultBudgetMechanismRepository {

    /* Adds a mechanism to the repository. */
    @Override
    public boolean add(DefaultBudgetMechanismInterface defBM) {
        try {
            super.add(defBM);
        } catch (RollbackException ex) {
            throw new IllegalStateException();
        }
        return true;
    }

    /* Method to remove a mechanism. */
    @Override
    public boolean remove(DefaultBudgetMechanismInterface defBM) {
        try {
            super.delete(defBM);
        } catch (RollbackException ex) {
            throw new IllegalStateException();
        }
        return true;
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

}
