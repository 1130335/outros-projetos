/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.strategy.DefaultBudgetMechanismInterface;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author João Cabral
 */
public interface DefaultBudgetMechanismRepository {

    /* Adds a mechanism to the repository. */
    boolean add(DefaultBudgetMechanismInterface budget);

    /* Returns the size of the repository. */
    long size();

    /* Returns a list with all the mechanisms in the repository. */
    List<DefaultBudgetMechanismInterface> all();

    /* Iterator to traverse the list of mechanisms. */
    public Iterator<DefaultBudgetMechanismInterface> iterator(int pagesize);

    /* Method to remove a mechanism. */
    boolean remove(DefaultBudgetMechanismInterface budget);
}
