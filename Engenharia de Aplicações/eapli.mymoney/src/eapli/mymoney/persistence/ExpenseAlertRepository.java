/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.observer.ExpenseAlertInterface;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Pedro_Coelho_1130353
 */
public interface ExpenseAlertRepository {
    
    boolean add(ExpenseAlertInterface alert);

    long size();

    List<ExpenseAlertInterface> all();

    public Iterator<ExpenseAlertInterface> iterator(int pagesize);

    void remove(ExpenseAlertInterface alert);
    
    void checkAlerts(Expense exp);
    
}
