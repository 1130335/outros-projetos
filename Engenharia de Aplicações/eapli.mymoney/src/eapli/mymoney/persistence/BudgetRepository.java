/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Paulo
 */
public interface BudgetRepository {

	boolean add(Budget budget);

	long size();

	List<Budget> all();

	public Iterator<Budget> iterator(int pagesize);

	void remove(Budget budget);

	public boolean update(Budget sb, List<Entry> entries);

	public Budget getLastMonthsBudget();

	public void cancelBudget(Budget sb);

	public boolean anyBudgetIn(int month, int year);
}
