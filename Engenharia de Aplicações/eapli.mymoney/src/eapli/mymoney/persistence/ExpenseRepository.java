/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.ExpenseType;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rita
 */
public interface ExpenseRepository {

	boolean add(Expense expense);

	void delete(Expense expense);

	long size();

	List<Expense> all();

	public Iterator<Expense> iterator(int pagesize);

	public Double getExpenseWeekValue();

	public double getExpenseMonthValue();

	public Map<String, Double> getMonthsExpensesByType(int month,
													   List<ExpenseType> listExpensesType);

	public List<Double> getExpenseMonthsValues(int year);

}
