/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.DefaultBudgetMechanisms;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.persistence.inmemory.BudgetRepositoryImpl;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author João
 */
public class DBMPreviousMonthTest {

    
    private BudgetRepositoryImpl br;
    private Budget expectedBudget;

    public DBMPreviousMonthTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Calendar c = Calendar.getInstance();
        
        int mes = c.get(Calendar.MONTH);
        int ano = c.get(Calendar.YEAR) + 1;
        List<Entry> listaEntradas = null;
        
        //listaEntradas.add(new Entry(new ExpenseType("teste"), BigDecimal.ZERO));
        
        expectedBudget = new Budget(mes + 1, ano, listaEntradas);
        
        br = new BudgetRepositoryImpl();
        br.add(expectedBudget);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createBudget method of class DBMPreviousMonth.
     */
    @Test
    public void testCreateBudget() {
        System.out.println("createBudget");
        DBMPreviousMonth dbm = new DBMPreviousMonth();
        Budget testResultBudget = dbm.createBudget(br);

        Assert.assertEquals(expectedBudget, testResultBudget);
        
    }
}
