/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Utilizador
 */
public class ExpenseGroupTest {

	public ExpenseGroupTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of setData method, of class ExpenseGroup.
	 */
	@Test
	public void testSetData() {
		System.out.println("setData");
		String name = "Os gastos com o meu carro";
		Date beginD = new Date(2015, 05, 02);
		Date endD = new Date(2016, 05, 02);
		ExpenseGroup instance = new ExpenseGroup();
		instance.setData(name, beginD, endD);

		String name2 = instance.getName();
		Date beginD2 = instance.getInitialDate();
		Date endD2 = instance.getEndDate();

		Assert.assertEquals(name, name2);
		Assert.assertEquals(beginD, beginD2);
		Assert.assertEquals(endD, endD2);
	}

	/**
	 * Test of setName method, of class ExpenseGroup.
	 */
	@Test
	public void testSetName() {
		System.out.println("setName");
		String name = "";
		ExpenseGroup instance = new ExpenseGroup();
		instance.setName(name);
	}

	/**
	 * Test of setInitialDate method, of class ExpenseGroup.
	 */
	@Test
	public void testSetInitialDate() {
		System.out.println("setInitialDate");
		Date initialDate = null;
		ExpenseGroup instance = new ExpenseGroup();
		instance.setInitialDate(initialDate);
	}

	/**
	 * Test of setEndDate method, of class ExpenseGroup.
	 */
	@Test
	public void testSetEndDate() {
		System.out.println("setEndDate");
		Date endDate = null;
		ExpenseGroup instance = new ExpenseGroup();
		instance.setEndDate(endDate);
	}

	

	



	/**
	 * Test of getName method, of class ExpenseGroup.
	 */
	@Test
	public void testGetName() {
		System.out.println("getName");
		ExpenseGroup instance = new ExpenseGroup();
		String expResult = "";
		String result = instance.getName();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of getInitialDate method, of class ExpenseGroup.
	 */
	@Test
	public void testGetInitialDate() {
		System.out.println("getInitialDate");
		ExpenseGroup instance = new ExpenseGroup();
		Date expResult = null;
		Date result = instance.getInitialDate();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of getEndDate method, of class ExpenseGroup.
	 */
	@Test
	public void testGetEndDate() {
		System.out.println("getEndDate");
		ExpenseGroup instance = new ExpenseGroup();
		Date expResult = null;
		Date result = instance.getEndDate();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of hasExpense method, of class ExpenseGroup.
	 */
	@Test
	public void testHasExpense() {
		System.out.println("hasExpense");
		ExpenseGroup instance = new ExpenseGroup();
		boolean expResult = false;
		boolean result = instance.hasExpense();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of validate method, of class ExpenseGroup.
	 */
	@Test
	public void testValidate() {
		System.out.println("validate");
		String name = "Os gastos com o meu carro";
		Date beginD = new Date(2015, 05, 02);
		Date endD = new Date(2016, 05, 02);
		ExpenseGroup instance = new ExpenseGroup();
		boolean expResult = true;
		boolean result = instance.validate(name, beginD, endD);
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of validateOptionalData method, of class ExpenseGroup.
	 */
	@Test
	public void testValidateOptionalData() {
		System.out.println("validateOptionalData");
		float value = 2.0F;
		ExpenseGroup instance = new ExpenseGroup();
		boolean expResult = true;
		boolean result = instance.validateOptionalData(value);
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of getTypesList method, of class ExpenseGroup.
	 */
	@Test
	public void testGetTypesList() {
		System.out.println("getTypesList");
		ExpenseGroup instance = new ExpenseGroup();
		List<ExpenseType> expResult = null;
		List<ExpenseType> result = instance.getTypesList();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of setTypesList method, of class ExpenseGroup.
	 */
	@Test
	public void testSetTypesList() {
		System.out.println("setTypesList");
		List<ExpenseType> typesList = null;
		ExpenseGroup instance = new ExpenseGroup();
		instance.setTypesList(typesList);
	}

	/**
	 * Test of addExpenseType method, of class ExpenseGroup.
	 */
	@Test
	public void testAddExpenseType() {
		System.out.println("addExpenseType");
		ExpenseType xtype = null;
		ExpenseGroup instance = new ExpenseGroup();
		instance.addExpenseType(xtype);
	}

	/**
	 * Test of setOptionalData method, of class ExpenseGroup.
	 */
	@Test
	public void testSetOptionalData() {
		System.out.println("setOptionalData");
		float value = 2.0F;
		ExpenseGroup instance = new ExpenseGroup();
		instance.setOptionalData(value);
		float value2 = instance.getTotalValue();
		Assert.assertEquals(value, value2, 0.0);
	}

	/**
	 * Test of getTotalValue method, of class ExpenseGroup.
	 */
	@Test
	public void testGetTotalValue() {
		System.out.println("getTotalValue");
		ExpenseGroup instance = new ExpenseGroup();
		float expResult = 0.0F;
		float result = instance.getTotalValue();
		Assert.assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of id method, of class ExpenseGroup.
	 */
	@Test
	public void testId() {
		System.out.println("id");
		ExpenseGroup instance = new ExpenseGroup();
		Object expResult = null;
		Object result = instance.id();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of getStatus method, of class ExpenseGroup.
	 */
	@Test
	public void testGetStatus() {
		System.out.println("getStatus");
		ExpenseGroup instance = new ExpenseGroup();
		int expResult = 0;
		int result = instance.getStatus();
		Assert.assertEquals(expResult, result);
	}

	/**
	 * Test of setStatus method, of class ExpenseGroup.
	 */
	@Test
	public void testSetStatus() {
		System.out.println("setStatus");
		int status = 0;
		ExpenseGroup instance = new ExpenseGroup();
		instance.setStatus(status);
	}

}
