/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo
 */
public class BudgetTest {

	public BudgetTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of newEntry method, of class Budget.
	 */
	@Test
	public void testNewEntry() {
		System.out.println("newEntry");
		ExpenseType expType = new ExpenseType();
		BigDecimal value = new BigDecimal(0);
		Budget instance = new Budget();
		Entry expResult = new ExpenseTypeEntry(expType, value);
		Entry result = instance.newExpenseTypeEntry(expType, value);
		assertEquals(expResult, result);
	}

	/**
	 * Test of addEntry method, of class Budget.
	 */
	@Test
	public void testAddEntry() {
		System.out.println("addEntry");

		Budget instance = new Budget();
		Entry entry = instance.
			newExpenseTypeEntry(new ExpenseType(), BigDecimal.ZERO);
		Entry expResult = entry;
		instance.addEntry(entry);
		List<Entry> lista = instance.getEntries();
		Entry result = lista.get(0);
		assertEquals(expResult, result);
	}
}
