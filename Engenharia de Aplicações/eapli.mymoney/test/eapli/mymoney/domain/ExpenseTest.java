/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.mymoney.domain.Payment.PaymentMethod;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo Silva
 * @author Ivo Teixeira
 * @authot Paulo Andrade
 */
public class ExpenseTest {

	public ExpenseTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testCreateExpense() throws ParseException {
		ExpenseType et = new ExpenseType("ET Teste");
		ExpenseGroup eg = new ExpenseGroup("EG Teste");
		PaymentMethod p = PaymentMethod.BANK_CHEQUE;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Expense expense = new Expense("T1", 100, formatter.parse("14-05-2015"), et, eg, p);
		if (expense != null) {
			System.out.println("Expense tested!\n");
		}

	}
}
