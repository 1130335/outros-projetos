/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import eapli.mymoney.domain.ExpenseType;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sergio
 */
public class DefineTypesPerGroupControllerTest {

	public DefineTypesPerGroupControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testSetExpenseGroup() {
		DefineTypesPerGroupController instance = new DefineTypesPerGroupController();
		ExpenseGroup group = new ExpenseGroup();
		Date dInicial = new Date(2015, 06, 21);
		Date dFinal = new Date(2015, 06, 24);
		group.setData("teste", dInicial, dFinal);
		group.setOptionalData(12);
		instance.setExpenseGroup(group);

		ExpenseGroup expResult = group;
		ExpenseGroup result = instance.getExpenseGroup();
		assertEquals(expResult, result);
	}

	@Test
	public void testAddExpenseType() {
		DefineTypesPerGroupController instance = new DefineTypesPerGroupController();
		ExpenseGroup group = new ExpenseGroup();
		Date dInicial = new Date(2015, 06, 21);
		Date dFinal = new Date(2015, 06, 24);
		group.setData("teste", dInicial, dFinal);
		group.setOptionalData(12);
		instance.setExpenseGroup(group);

		ExpenseType expType = new ExpenseType("Tipo de despesa test");
		instance.addExpenseType(expType);

		ExpenseType expResult = expType;
		ExpenseType result = instance.getExpenseGroupAux().getTypesList().get(0);
		assertEquals(expResult, result);
	}

	@Test
	public void testConfirm() {
		DefineTypesPerGroupController instance = new DefineTypesPerGroupController();
		ExpenseGroup group = new ExpenseGroup("grupo de despesa teste");
		ExpenseType expType = new ExpenseType("Tipo1");
		ExpenseType expT = new ExpenseType("Tipo2");
		List<ExpenseType> lista = null;
		lista.add(expT);
		group.setTypesList(lista);
		instance.setExpenseGroup(group);

		instance.addExpenseType(expType);

		boolean expResult = false;
		boolean result = instance.confirm();
		assertEquals(expResult, result);

	}
}
