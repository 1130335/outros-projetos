/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.ExpenseGroup;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo
 */
public class SetExpenseGroupControllerTest {

	public SetExpenseGroupControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	// TODO add test methods here.
	// The methods must be annotated with annotation @Test. For example:
	//
	// @Test
	// public void hello() {}
	@Test
	public void testGetExpenseGroupList() {
		System.out.println("getExpenseGroupList");
	}

	@Test
	public void testSetExpenseGroup() {
		System.out.println("setExpenseGroup");
		SetExpenseGroupController instance = new SetExpenseGroupController();
		ExpenseGroup group = new ExpenseGroup("grupo de despesa teste");
		instance.setExpenseGroup(group);

		ExpenseGroup expResult = group;
		ExpenseGroup result = instance.getExpenseGroup();
		assertEquals(expResult, result);

	}

	@Test
	public void getExpenseGroupData() {
		System.out.println("getExpenseGroupData");
		SetExpenseGroupController instance = new SetExpenseGroupController();
		ExpenseGroup group = new ExpenseGroup();
		Date dInicial = new Date(2015, 06, 21);
		Date dFinal = new Date(2015, 06, 24);
		group.setData("teste", dInicial, dFinal);
		group.setOptionalData(12);
		instance.setExpenseGroup(group);
		instance.setData("grupoClone", dFinal, dFinal, 10);

		String expResult = "Expense Group: " + group.getName() + ",initial date: " + group.
			getInitialDate().toString() + ",end date: " + group.getEndDate().
			toString() + ",total value: " + group.getTotalValue();
		String result = instance.getExpenseGroupData();
		assertEquals(expResult, result);

	}

	@Test
	public void testConfirm() {
		System.out.println("Confirm");
	}
}
