/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.ExpenseType;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author IvoTeixeira
 */
public class CreateBudgetControllerTest {

	public CreateBudgetControllerTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getExpenseTypeList method, of class CreateBudgetController.
	 */
	@Test
	public void testGetExpenseTypeList() {
		System.out.println("getExpenseTypeList");
		CreateBudgetController instance = new CreateBudgetController();
		List<ExpenseType> expResult = null;
		List<ExpenseType> result = instance.getExpenseTypeList();
		assertEquals(expResult, result);

	}

	/**
	 * Test of newEntry method, of class CreateBudgetController.
	 */
	@Test
	public void testNewEntry() {
		System.out.println("newEntry");
		Budget newBudget;

		ExpenseType expType = new ExpenseType("Vestuário");
		BigDecimal value = new BigDecimal(200);

		//Entry new_entry = newBudget.newEntry(expType, BigDecimal.ZERO);
		CreateBudgetController instance = new CreateBudgetController();
		instance.newEntry(expType, value);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of persist method, of class CreateBudgetController.
	 */
	@Test
	public void testPersist() {
		System.out.println("persist");
		CreateBudgetController instance = new CreateBudgetController();
		instance.persist();
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

}
