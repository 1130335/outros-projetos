/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Budget;
import eapli.mymoney.domain.Entry;
import eapli.mymoney.domain.ExpenseTypeEntry;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author rafael
 */
public class EditBudgetControllerTest {

    public EditBudgetControllerTest() {
	}
    @BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}
        
	@Test
        public void testSetBudget() {
        System.out.println("set budget");
        EditBudgetController instance = new EditBudgetController();
        Budget b = instance.getBudget();
        Budget expb = null;
        List<Entry> expbudgetEntries = null;
        List<Entry> budgetEntries = b.getEntries();
        instance.setBudget(b);  
        assertEquals(expbudgetEntries, budgetEntries);
        assertEquals(expb, b);
    }
        
        public void testGetBudget() {
        System.out.println("get budget");
        EditBudgetController instance = new EditBudgetController();
        Budget b = instance.getBudget();
        Budget expb = null;
	assertEquals(expb, b);
        }
        
        public void testGetEntry() {
		System.out.println("getEntry");
                EditBudgetController instance = new EditBudgetController();
		Entry expEntry = null;
		Entry Entry = instance.getEntry();
		assertEquals(expEntry, Entry);
	}
        
        public void testSetEntry() {
        System.out.println("set Entry");
        EditBudgetController instance = new EditBudgetController();
        Entry expEntry = null;
        Entry Entry = instance.getEntry();
        instance.setEntry(Entry);
        assertEquals(expEntry, Entry);
    }
}